<?php 
ini_set('display_errors', 'On');

include __dir__."/../apis/include/BackgroundModel.php";
include __dir__."/../apis/include/ContactModel.php";
include __dir__."/../apis/include/ProjectModel.php";
include __dir__."/../apis/include/OvertimeModel.php";
require_once __dir__."/../apis/core/constant.php";
// include '../apis/include/CaseModel.php';

Class BackgroundProcess {

	private $datetime;
	private $backgroundModel;

	public function __construct(){

		$this->backgroundModel = new BackgroundModel();
		$this->updateCloseSr();
		// $config = $this->backgroundModel->getConfig();
		
		//$hours = date("H");
		//$minutes = date("i");
		// echo $config['get_sla_remedy'];

		//$prefix = array("");

		//$this->processSendUpdateCaseToCCD();
		// $this->genPDF();

        ///////// Disable function: waiting for check process  1/26/2018 /////
		
//		foreach ($prefix as $key => $value) {
//			//value is prefix;
//			// if($minutes%55==0){
//            	// $this->updateCaseWorklog($value);
//        	// }
//        	// if($minutes%$config['get_sla_remedy']==0){
//			// $this->updateUserCanCreateDummyCompany();
//
//
//
//
//			$this->createDummyContract();
//        	if($minutes%3==0){
//        		$this->main($value);
//        		// $this->runExecuteRuleSla($value);
//        		// $this->updateTableSlaUrgency($value);
//        		// $this->updateContractTableSLA($value);
//        		// $this->updateInfoTableSLA($value);
//
//        	}
//
//			if($minutes%2==0){
//
//
//				// $this->sendMailPDF();
//				$this->sendMailOpenTicket();
//
//				$this->backgroundModel->processUpdateTaxifare();
//
//				$this->processOvertime($value);
//				$this->processTaxifare($value);
//				// $this->processClosedPM($value);
//			}
//
//			if($minutes%3==0){
//
//				$this->processCheckStandby($value);
//			}
//			if($minutes%5==0){
//				$this->getPmFromEmail();
//				// if(1==2){
//
//				// }
//				$this->updateCloseSr();
//
//				$this->projectProgress();
//				$this->runContact($value);
//			}
//		}
//
//		if($hours>8 && $hours<18){
//			if($minutes%59==0||$minutes%29==0){
//                // update contract empty not use?
//				$this->updateContractTableSLA($value);
//
//                // call web service flgupload ? not use ?
//        		$this->updateInfoTableSLA($value);
//
//                // update standby not use?
//        		$this->updateCheckPointStandby();
//
//			}
//			if($minutes%6==0){
//				$this->updateManDaysProject();
//
//				$this->processTaxifareTicket();
//
//			}
//		}


		// $this->mainMvg();
		// $this->runInsertTasklog();
	}

	public function processSendUpdateCaseToCCD(){
		$caseWaitSendToCCD = $this->backgroundModel->caseWaitSendToCCD();
		echo "<pre>";
		print_r($caseWaitSendToCCD);
		echo "</pre>";
		foreach ($caseWaitSendToCCD as $key => $value) {
			$this->backgroundModel->updateStatusCaseToRemedy($value['ticket_sid'], $value['create_by'], $value['worklog'], $value['expect_pending'], $value['case_status_sid'], $value['solution'], $value['solution_detail'], $value['sid'], $value);
		}
	}
	public function sendMailOpenTicket(){
		$this->backgroundModel->sendMailOpenTicket();
	}
	public function projectProgress(){
		$this->backgroundModel->calProgressAndUpdateProjectProgress();
	}
	public function genPDF(){
		$data = $this->backgroundModel->genPDF();
		foreach ($data as $key => $value) {
			// echo "<pre>";
			// print_r($value);
			// echo "</pre>";
			if(file_exists("../uploads/signature/".$value['file_name'])){
				echo "Have File Signature".$value['task_sid'];

				$url = fopen(HOST_NAME."/nodes/pdf/?task=".$value['task_sid']."&email=","r");
				$this->backgroundModel->updateWhenGenPDF($value['sid']);
				
			}else{
				echo "Have not file Signature ".$value['task_sid'];
			}
			// $url = fopen("http://vspace01.vspace.in.th/nodes/pdf/?task=".$value['task_sid']."&email=","r");
			// $url = fopen("http://vspace02.vspace.in.th/nodes/pdf/?task=".$value['task_sid']."&email=","r");
			// $url = fopen("http://vspace03.vspace.in.th/nodes/pdf/?task=".$value['task_sid']."&email=","r");		
			// $this->backgroundModel->updateWhenGenPDF($value['sid']);

			// file_exists(filename)
		}
	}
	public function sendMailPDF(){
		$data = $this->backgroundModel->listNewGenPDF();
		foreach ($data as $key => $value) {
			echo "<pre>";
			print_r($value);
			echo "</pre>";
			fopen(HOST_NAME."/api/sendmail/sendEmailAfterSigned/".$value['task_sid']."/", "r");

			$this->backgroundModel->updateWhenSendMailPDF($value['sid']);
		}
	}
	public function updateWhenGenPDF(){
		$data = $this->backgroundModel->updateWhenGenPDF();
	}
	public function updateCloseSr(){
		$this->backgroundModel->updateCloseSr();
	}

	public function updateCheckPointStandby(){
		$this->backgroundModel->updateCheckPointStandby();
	}

	private function createDummyContract(){
		$data = $this->backgroundModel->tableProjectNoDataContract();
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		foreach ($data as $key => $value) {
			$sales2 = $value['sales2'];
			$sales1 = $value['sales'];
			$prjName = str_replace(" ", "_", $value['name']);
			$maStart = $value['project_start'];
			$maEnd = $value['project_end'];
			$custNo = $value['customer'];
			$comp = $value['project_for_company'];
			$createBy = $value['empno'];
			echo $url = "http://flgupload.com/contractCreateDummyEbizWithSale.php?comp=".$comp."&sales1=".$sales1."&sales2=".$sales2."&prjName=".$prjName."&maStart=".$maStart."&createBy=".$createBy."&maEnd=".$maEnd."&remark=&category=77&custNo=".$custNo."&ref=&pm=";
		    $res = file_get_html($url)->plaintext;

		    $this->backgroundModel->updateDummyContract($value['sid'], $res);
		}
	}
	private function updateManDaysProject(){
		$obj = new ProjectModel();
    	$data = $obj->executeUpdateManDays();
	}
	private function runExecuteRuleSla($prefix){
		$this->backgroundModel->runExecuteRuleSla($prefix);
	}
	private function updateTableSlaUrgency($prefix){
		$this->backgroundModel->updateTableSlaUrgency($prefix);
	}
	private function updateInfoTableSLA($prefix){
		$this->backgroundModel->updateInfoTableSLA($prefix);
	}
	private function updateContractTableSLA($prefix){
		$this->backgroundModel->updateContractTableSLA($prefix);
	}
	private function processCheckStandby($prefix){
		// if($prefix==""){
			$this->backgroundModel->checkStandby("");
		// }
	}
	private function processClosedPM($prefix){
		// $this->backgroundModel = new BackgroundModel();
		$this->backgroundModel->runAutoClosePm($prefix);
	}
	private function processOvertime($prefix){
		$overtimeModel = new OvertimeModel();
		$overtimeModel->processOvertime($prefix);
	}
	private function processTaxifare($prefix){
		$overtimeModel = new OvertimeModel();
		$overtimeModel->processTaxifare($prefix);	
	}
	private function processTaxifareTicket(){
		$overtimeModel = new OvertimeModel();
		$overtimeModel->processTaxifareTicket();
	}
	private function runContact($prefix){
		$contactModel = New ContactModel();
		$contactModel->runContact($prefix);
	}
	public function updateCaseWorklog($prefix){
        // $this->backgroundModel = new BackgroundModel();
        $this->backgroundModel->updateCaseWorklog($prefix, '4');
        // $backgroundModel->updateCaseWorklog($prefix, '2');
    }
    public function runInsertTasklog(){
		// $backgroundModel = new BackgroundModel();
		// $backgroundModel->firstDayDecember();
	}
	private function updateUserCanCreateDummyCompany(){
		$backgroundModel = new BackgroundModel();
		$data = $backgroundModel->selectUserWithEmpno();
		foreach ($data as $key => $value) {
			$data[$key]['can_create_dummy_company'] = json_decode(file_get_html("http://flgupload.com/checkPermissionDummyContract.php?empno=".$value['empno'])->plaintext, true);
			$backgroundModel->insert_user_create_dummy_contract_company($data[$key]);
		}
	}

	public function main($prefix){
		$backgroundModel = new BackgroundModel();
		$res = $backgroundModel->newTicket($prefix);
		$prefix_ = str_replace("_", "", strtoupper($prefix));
		echo "Main start";
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		echo "Main End";
		$hd = "";
		foreach ($res as $key => $value) {
			if($hd!=""){
				$hd .= ",";
			}
			$hd .= $value['refer_remedy_hd'];

			$file = fopen("http://ws.flgupload.com/soap/testWebServiceRemedy/RemedySLA.php?id=".$value['refer_remedy_hd'], "r");
			// echo "Run";
			if($file){
				$stringSLA = "";
				while (!feof( $file)) {
					$stringSLA .= fgets( $file );	
				}
			}
			// echo $stringSLA;
			$backgroundModel->updateSLARemedy($value['refer_remedy_hd'], $stringSLA, $value['owner'], $value['sid'], $prefix);

			$notification_sid = 0;
			if($value['status']=="1"){
				$notification_sid = "1";
			}else{
				$notification_sid = "4";
			}

			if($value['status']=="1"){
				// $this->sendNotify(
				// 	$value['owner'], 
				// 	"Notify: ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$value['status_name']." ".", ".$value['subject'],
				// 	$notification_sid,
				// 	$value['sid']
				// );

				// $this->sendNotify(
				// 	"autsakorn.t@firstlogic.co.th",
				// 	"Notify ".$prefix_.": ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$value['status_name'].", ".$value['subject'],
				// 	$notification_sid,
				// 	$value['sid']
				// );

				// if($prefix==""){
				// 	$this->sendNotify(
				// 		"thanachot.w@firstlogic.co.th",
				// 		"Notify ".$prefix_.": ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$value['status_name'].", ".$value['subject'],
				// 		$notification_sid,
				// 		$value['sid']
				// 	);
				// }
			

			}else{
				$newDataCase = $backgroundModel->getTicket($prefix, $value['refer_remedy_hd']);
				if($value['status']=="4" && false){
					if($newDataCase['status']!=$value['status']){
						// $this->sendNotify(
						// 	$value['owner'],
						// 	"Notify: ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$newDataCase['status_name'].", ".$newDataCase['subject'].", ".$newDataCase['worklog'],
						// 	$notification_sid,
						// 	$value['sid']);
						// $this->sendNotify(
						// 	"autsakorn.t@firstlogic.co.th",
						// 	"Notify ".$prefix_.": ".(isset($newDataCase['no_ticket'])?$newDataCase['no_ticket']:$value['no_ticket'])." (".$newDataCase['refer_remedy_hd']."), ".$newDataCase['status_name'].", ".$newDataCase['subject'].", ".$newDataCase['worklog'].", ".$value['engname'],
						// 	$notification_sid,
						// 	$value['sid']);

						// if($prefix==""){
						// 	$this->sendNotify(
						// 		"thanachot.w@firstlogic.co.th",
						// 		"Notify ".$prefix_.": ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$value['status_name'].", ".$value['subject'],
						// 		$notification_sid,
						// 		$value['sid']
						// 	);
						// }
					}
				}else{
					// $this->sendNotify(
					// 	$value['owner'],
					// 	"Notify: ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$newDataCase['status_name'].", ".$newDataCase['subject'].", ".$newDataCase['worklog'],
					// 	$notification_sid,
					// 	$value['sid']);
					// $this->sendNotify(
					// 	"autsakorn.t@firstlogic.co.th",
					// 	"Notify ".$prefix_.": ".(isset($newDataCase['no_ticket'])?$newDataCase['no_ticket']:$value['no_ticket'])." (".$newDataCase['refer_remedy_hd']."), ".$newDataCase['status_name'].", ".$newDataCase['subject'].", ".$newDataCase['worklog'].", ".$value['engname'],
					// 	$notification_sid,
					// 	$value['sid']);

					// if($prefix==""){
					// 	$this->sendNotify(
					// 		"thanachot.w@firstlogic.co.th",
					// 		"Notify ".$prefix_.": ".$value['no_ticket']." (".$value['refer_remedy_hd']."), ".$value['status_name'].", ".$value['subject'],
					// 		$notification_sid,
					// 		$value['sid']
					// 	);
					// }
				
				}
			}
			// if($value['status']!="4" && $value['status']!="2"){
			// }
		}
		// if($hd!=""){
		// 	$this->sendNotify("autsakorn.t@firstlogic.co.th",$prefix." Has Update ".$hd);
		// }
		// close();
	}

	private function sendNotify($to,$message, $notification_sid=0,$ticket_sid=0){
		$this->datetime = date("d m Y H:i:s");
		$dataPost = array (
	        'to' => $to,
	        'message'=>$message,
	        'notification_sid'=>$notification_sid,
	        'ticket_sid'=>$ticket_sid
		);
		$dataPost = http_build_query($dataPost);
		$context_options = array (
			'http' => array (
		    'method' => 'POST',
		    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
		    	. "Content-Length: " . strlen($dataPost) . "\r\n",
		        'content' => $dataPost
		    )
		);
		$context = stream_context_create($context_options);
		$url = HOST_NAME."/apis/v1/incident/sendNotification";
		if($message!=""){
			$html = file_get_html($url, false, $context)->plaintext;
		}
	}

	public function mainMvg(){
		$backgroundModel = new BackgroundModel();
		$res = $backgroundModel->newTicket("mvg_");
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		$hd = "";
		foreach ($res as $key => $value) {
			if($hd!=""){
				$hd .= ",";
			}
			$hd .= $value['refer_remedy_hd'];

			$file = fopen("http://ws.flgupload.com/soap/testWebServiceRemedy/RemedySLA.php?id=".$value['refer_remedy_hd'], "r");
			$stringSLA = "";
			while (!feof( $file)) {
				$stringSLA .= fgets( $file );	
			}
			echo $value['refer_remedy_hd'];
			echo $stringSLA;
			$backgroundModel->updateSLARemedy($value['refer_remedy_hd'], $stringSLA, $value['owner'], $value['sid']);

		}
		// $this->datetime = date("d m Y h:i");
		$dataPost = array (
	        'to' => 'autsakorn.t@firstlogic.co.th',
	        'message'=>'Case New MVG, '.$hd,
		);
		$dataPost = http_build_query($dataPost);
		$context_options = array (
			'http' => array (
		    'method' => 'POST',
		    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
		    	. "Content-Length: " . strlen($dataPost) . "\r\n",
		        'content' => $dataPost
		    )
		);
		$context = stream_context_create($context_options);
		$url = HOST_NAME."/apis/v1/incident/sendNotification";
		if($hd!=""){
			$html = file_get_html($url, false, $context)->plaintext;
		}
		// close();
	}

	public function getPmFromEmail(){
		$file = fopen("http://flgupload.com/cronjob/GetPMFromMail.php", "r");
	}
}
// if(isset($_GET['manual']) && $_GET['manual']=="1"){
	$backgroundProcess = new BackgroundProcess();
// }
?>