<?php
ini_set('display_errors', 'On');
include "../simple_html_dom.php";
include "../apis/include/BackgroundModel.php";

class UpdateProjectProgress{
	public function run(){
		$obj = new BackgroundModel();
		$project = $obj->calProgressAndUpdateProjectProgress();
	}
}
$obj = new UpdateProjectProgress();
$obj->run();
?>