<?php
require_once __dir__."/../apis/include/BackgroundAutoCloseModel.php";
/**
 *
 */
class BackgroundAutoclose
{
  private $AutoCloseModel;
  public function __construct()
  {
    $this->AutoCloseModel = new BackgroundAutoCloseModel();
    $this->autoClose();
  }

  function autoClose()
  {
     $data = $this->AutoCloseModel->listResolveAfter15days();
     // echo "<pre>";
     // print_r($data);
     // echo "</pre>";
     foreach ($data as $key => $value) {
       $this->AutoCloseModel->updateCloseTicket($value['sid']);
     }

  }
}
$backgroundAutoClose = new BackgroundAutoclose();
 ?>
