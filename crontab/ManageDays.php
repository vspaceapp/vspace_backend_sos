<?php 
ini_set('display_errors', 'On');
include "../simple_html_dom.php";
include "../apis/include/BackgroundModel.php";
include "../apis/include/DaysModel.php";

class ManageDays{
	private $db;
	function __construct(){
		$this->db = new DaysModel();
	}
	public function genDays(){
		$data = $this->db->genDays();
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		foreach ($data as $key => $value) {
			$this->insertDays($value);
		}
	}

	public function insertDays($data){
		$this->db->insertDays($data);
	}

	public function listDays(){
		$data = $this->db->listDays();
		return $data;
	}
	public function holidays(){
		return $this->db->listHoliday();
	}

	public function setIsHoliday(){
		$days = $this->listDays();
		$holidays = $this->holidays();
		foreach ($days as $key => $value) {
			$is_holiday = 0;
			$name = $value['name'];
			foreach ($holidays as $k => $v) {
				if($value['day']==$v['holiday']){
					if($v['description_en']=='Saturday' || $v['description_en']=='Sunday'){
						$is_holiday = 2;
					}else{
						$is_holiday = 1;
					}
					$name = $v['description_en'];
				}
			}
			echo $value['sid'].' '.$is_holiday;
			echo "<br/>";
			$this->updateDate($value['sid'], $name, $is_holiday);
		}
	}

	public function updateDate($day_sid, $name, $is_holiday){
		$this->db->updateDate($day_sid, $name, $is_holiday);
	}

	public function genStandby7x24(){
		exit();
		$days = $this->listDays();
		$buddy = $this->db->standbyBuddy();

		$start_day = "2017-05-03";
		$stop_day = "2018-01-01";

		$data = array();
		$start_run = false;
		echo "<pre>";
		print_r($buddy);
		echo "</pre>";

		$index0 = 2;//วัน จ ศ
		$index1 = 1;//เสาทิต
		$index2 = 0;//หยุดสามวัน
		$index3 = 3;//หยุดสงกรานต์ ปีใหม่

		$buddy_array[0] = $buddy[$index0];
		$buddy_array[1] = $buddy[$index1];
		$buddy_array[2] = $buddy[$index2];
		$buddy_array[3] = $buddy[$index3];
		
		$current_type_holiday = "";

		foreach ($days as $key => $value) {
			if($value['day']==$start_day){
				$start_run = true;
			}else if($value['day']==$stop_day){
				$start_run = false;
			}
			if($start_run){

				if($current_type_holiday==""){

					$current_type_holiday = $value['is_holiday'];
					// if($value['is_holiday']==0 || $value['is_holiday']==1){
					// 	$index0--;
					// 	$index0 %=6;
					// }else if($value['is_holiday']==2){
					// 	$index1--;
					// 	$index1 %=6;
					// }else if($value['is_holiday']==3){
					// 	$index2--;
					// 	$index2 %=6;
					// }else if($value['is_holiday']==4){
					// 	$index3--;
					// 	$index3 %=6;
					// }
				}

				if($current_type_holiday!=$value['is_holiday']){
					if($value['is_holiday']==0 || $value['is_holiday']==1){
						$index0++;
						$index0 %=6;
						$buddy_array[0] = $buddy[$index0];
					}else if($value['is_holiday']==2){
						$index1++;
						$index1 %=6;
						$buddy_array[1] = $buddy[$index1];
					}else if($value['is_holiday']==3){
						$index2++;
						$index2 %=6;
						$buddy_array[2] = $buddy[$index2];
					}else if($value['is_holiday']==4){
						$index3++;
						$index3 %=6;
						$buddy_array[3] = $buddy[$index3];
					}
				}else{
					if($value['is_holiday']==0 || $value['is_holiday']==1){
						$index0++;
						$index0 %=6;
						$buddy_array[0] = $buddy[$index0];
					}else if($value['is_holiday']==2){
						
						$buddy_array[1] = $buddy[$index1];
					}else if($value['is_holiday']==3){
						
						$buddy_array[2] = $buddy[$index2];
					}else if($value['is_holiday']==4){
						
						$buddy_array[3] = $buddy[$index3];
					}
				}


				if($value['is_holiday']==0 || $value['is_holiday']==1){
					$staff_is = $buddy_array[0];
				}else if($value['is_holiday']==2){
					$staff_is = $buddy_array[1];
				}else if($value['is_holiday']==3){
					$staff_is = $buddy_array[2];
				}else if($value['is_holiday']==4){
					$staff_is = $buddy_array[3];
				}

			
				// echo $value['day']." ".$value['is_holiday'];
				// echo "<br/>";
				$time_start = "08:00";
				$how_long_hours = "24";
				if($value['is_holiday']==0){
					$time_start = "17:00";
					$how_long_hours = "15";
				}
				$dataArray = array(
					'days_sid'=>$value['sid'], 'day_name'=>$value['name'],
					'day'=>$value['day'],'is_holiday'=>$value['is_holiday'],
					'appointment'=>$time_start,
					'expect_duration'=>$how_long_hours,
					'staff'=>$staff_is,
					'subject'=>'Standby 7x24'
					);

				array_push($data, $dataArray);
				$this->db->insertAppointment($dataArray);
				$current_type_holiday = $value['is_holiday'];
			}
		}
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}

	function dev(){
		$obj = new BackgroundModel();
		$project = $obj->calProgressAndUpdateProjectProgress();
		// $appointment = $obj->updateCheckPointStandby();

		echo "<pre>";
		print_r($project);
		echo "</pre>";

		// echo "<pre>";
		// print_r($appointment);
		// echo "</pre>";
	}
}
$c = new ManageDays();
$c->dev();
// $c->listHoliday();

?>