<?php 
ini_set('display_errors', 'On');
include "../simple_html_dom.php";
include "../apis/include/BackgroundModel.php";

Class BackgroundProcess {

	private $datetime;
	public function __construct(){
		// $prefix = array("","mvg_");
		// $prefix = array("");
		// foreach ($prefix as $key => $value) {
		// 	$this->main($value);			
		// }
		// $this->mainMvg();

		$this->runInsertTasklog();
	}

	public function runInsertTasklog(){
		$backgroundModel = new BackgroundModel();
		$backgroundModel->firstDayDecember();
	}

	public function main($prefix){
		$backgroundModel = new BackgroundModel();
		$res = $backgroundModel->newTicket($prefix);
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		$hd = "";
		foreach ($res as $key => $value) {
			if($hd!=""){
				$hd .= ",";
			}
			$hd .= $value['refer_remedy_hd'];

			$file = fopen("http://ws.flgupload.com/soap/testWebServiceRemedy/RemedySLA.php?id=".$value['refer_remedy_hd'], "r");
			$stringSLA = "";
			while (!feof( $file)) {
				$stringSLA .= fgets( $file );	
			}
			echo $stringSLA;
			$backgroundModel->updateSLARemedy($value['refer_remedy_hd'], $stringSLA, $value['owner'], $value['sid']);

			$notification_sid = 0;
			if($value['status']=="1"){
				$notification_sid = "1";
			}else{
				$notification_sid = "3";
			}

			$this->sendNotify(
				$value['owner'], 
				"Notify: ".$value['no_ticket']." (".$value['refer_remedy_hd'].") is ".$value['status_name']." ",
				$notification_sid
			);

			$this->sendNotify("autsakorn.t@firstlogic.co.th", "Notify: ".$value['no_ticket']." (".$value['refer_remedy_hd'].") is ".$value['status_name']." ");
		}
		if($hd!=""){
			$this->sendNotify("autsakorn.t@firstlogic.co.th",$prefix." Has Update ".$hd);
		}
		// close();
	}

	private function sendNotify($to,$message, $notification_sid=0){
		$this->datetime = date("d m Y H:i:s");
		$dataPost = array (
	        'to' => $to,
	        'message'=>$message,
	        'notification_sid'=>$notification_sid
		);
		$dataPost = http_build_query($dataPost);
		$context_options = array (
			'http' => array (
		    'method' => 'POST',
		    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
		    	. "Content-Length: " . strlen($dataPost) . "\r\n",
		        'content' => $dataPost
		    )
		);
		$context = stream_context_create($context_options);
		$url = "http://vspace.in.th/apis/v1/incident/sendNotification";
		if($message!=""){
			$html = file_get_html($url, false, $context)->plaintext;
		}
	}

	public function mainMvg(){
		$backgroundModel = new BackgroundModel();
		$res = $backgroundModel->newTicket("mvg_");
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		$hd = "";
		foreach ($res as $key => $value) {
			if($hd!=""){
				$hd .= ",";
			}
			$hd .= $value['refer_remedy_hd'];

			$file = fopen("http://ws.flgupload.com/soap/testWebServiceRemedy/RemedySLA.php?id=".$value['refer_remedy_hd'], "r");
			$stringSLA = "";
			while (!feof( $file)) {
				$stringSLA .= fgets( $file );	
			}
			echo $value['refer_remedy_hd'];
			echo $stringSLA;
			$backgroundModel->updateSLARemedy($value['refer_remedy_hd'], $stringSLA, $value['owner'], $value['sid']);

		}
		// $this->datetime = date("d m Y h:i");
		$dataPost = array (
	        'to' => 'autsakorn.t@firstlogic.co.th',
	        'message'=>'Case New MVG, '.$hd,
		);
		$dataPost = http_build_query($dataPost);
		$context_options = array (
			'http' => array (
		    'method' => 'POST',
		    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
		    	. "Content-Length: " . strlen($dataPost) . "\r\n",
		        'content' => $dataPost
		    )
		);
		$context = stream_context_create($context_options);
		$url = "http://vspace.in.th/apis/v1/incident/sendNotification";
		if($hd!=""){
			$html = file_get_html($url, false, $context)->plaintext;
		}
		// close();
	}
}
$backgroundProcess = new BackgroundProcess();
?>