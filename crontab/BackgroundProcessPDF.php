<?php
ini_set('display_errors', 'On');

include __dir__."/../simple_html_dom.php";
include __dir__."/../apis/include/BackgroundModel.php";
include __dir__."/../apis/include/ContactModel.php";
include __dir__."/../apis/include/ProjectModel.php";
include __dir__."/../apis/include/OvertimeModel.php";
require_once __dir__."/../apis/core/constant.php";
require_once __dir__."/../apis/include/SendMail.php";

Class BackgroundProcessPDF {

	private $datetime;
	private $backgroundModel;

	public function __construct(){
		$this->backgroundModel = new BackgroundModel();
		$minutes = date("i");

		$this->genPDF();
		// if($minutes%1==0){
			$this->sendMailPDF();
		// }
	}
	public function genPDF(){
		$data = $this->backgroundModel->genPDF();
		foreach ($data as $key => $value) {
			echo "<pre>";
			print_r($value);
			echo "</pre>";
			if(file_exists(__dir__."/../uploads/signature/".$value['file_name'])){
				echo "Have File Signature".$value['task_sid'];
                fopen("".HOST_NAME."/nodes/pdf/index.php?task=".$value['task_sid']."&email=","r");
				$this->backgroundModel->updateWhenGenPDF($value['sid']);
				
			}else{
				echo "Have not file Signature ".$value['task_sid']." path: ".__dir__."/../uploads/signature/".$value['file_name'];
			}
			// $url = fopen("http://vspace01.vspace.in.th/nodes/pdf/?task=".$value['task_sid']."&email=","r");
			// $url = fopen("http://vspace02.vspace.in.th/nodes/pdf/?task=".$value['task_sid']."&email=","r");
			// $url = fopen("http://vspace03.vspace.in.th/nodes/pdf/?task=".$value['task_sid']."&email=","r");		
			// $this->backgroundModel->updateWhenGenPDF($value['sid']);

			// file_exists(filename)
		}
	}
	public function sendMailPDF(){
		$data = $this->backgroundModel->listNewGenPDF();
		$sendMail = new SendMail();
		foreach ($data as $key => $value) {
			echo "<pre>";
			print_r($value);
			echo "</pre>";
			//fopen("".HOST_NAME."/api/sendmail/sendEmailAfterSigned/".$value['task_sid']."/", "r");
            $sendMail->sendEmailAfterSigned($value['task_sid']);
			$this->backgroundModel->updateWhenSendMailPDF($value['sid']);
		}
	}
}
$backgroundProcess = new BackgroundProcessPDF();
?>