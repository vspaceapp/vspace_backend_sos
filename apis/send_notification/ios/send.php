<?php

function sendNotificationIOS($deviceToken,$message,$url,$production=0){
// Put your device token here (without spaces):
// $deviceToken = '4a625486b57eaad93bf2fd3cf1ea4d9d7dfa750d4f7fbb6fe79cd03b68e16683';

// Put your private key's passphrase here:
$passphrase = 'password';

// $message = $argv[1];
// $url = $argv[2];
// $message = "Hi, TANANCHAI";
// $url = "case.flgupload.com";
// if (!$message || !$url)
//     exit('Example Usage: $php newspush.php \'Breaking News!\' \'https://raywenderlich.com\'' . "\n");

////////////////////////////////////////////////////////////////////////////////
// sleep(10);
$ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', __dir__."/../../../apis/send_notification/ios/CerProd.pem");
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// sandbox
// Open a connection to the APNS server
$fp = stream_socket_client(
  'ssl://gateway.push.apple.com:2195', $err,
  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

if(!$production){
  echo 'Connected to APNS' . PHP_EOL;
}
// Create the payload body
$body['aps'] = array(
  'alert' => $message,
  'sound' => 'default',
  'link_url' => $url,
  'category'=>'NEWS_CATEGORY',
  // 'content-available'=>1
  'badge'=>0
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result){
  if(!$production){
    echo 'Message not delivered' . PHP_EOL;
  }
} else{
  if(!$production){
    echo 'Message successfully delivered' . PHP_EOL;
  }
}

// Close the connection to the server
fclose($fp);

}
