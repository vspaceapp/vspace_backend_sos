<?php class TaxiViewApp{

	public static function view(){
		$html = '<link href="http://vspace.in.th/production/square-cash/css/style.css" rel="stylesheet prefetch"></link>
				<style>
					#FormTaxiView, #FormTaxiWhenBackView {display:none;height:90%;overflow-x: hidden;}
					#FormTaxiView .table-view-cell {
						padding-top:0px;
					}
					#FormTaxiView .wrapper {
						background: initial;
					}
					#FormTaxiView #app {
						overflow-x: hidden;
						overflow-y: auto;
						background: initial;
						border-color: transparent;
					}
					#FormTaxiView #app #keypad li .number{
						bottom: initial;
					}
					#FormTaxiView button.btn-primary{
						background: #3c8dbc;
						color:#ffffff;
					}
					#FormTaxiView button.btn-warning{
						background: #f39c12;
						color:#ffffff;
					}
				</style>';
		$html .= '<div id="FormTaxiView" class="">
					<section id="app">
	                  	<div id="app-container" class="wrapper">
	                  			<div style="text-align:center;margin-bottom:20px;clear: both;">Taxi Fare?</div>
					          <section id="display" class="semi">
					            <div class="semi mten">
					              <span id="numerals"><span class="numeral displayed">0</span></span>
					            </div>
					          </section>
					          <ul id="keypad" class="semi">
					            <li id="one" class="key" role="button"><span class="number">1</span></li>
					            <li id="two" class="key" role="button"><span class="number">2</span></li>
					            <li id="three" class="key" role="button"><span class="number">3</span></li>
					            <li id="four" class="key" role="button"><span class="number">4</span></li>
					            <li id="five" class="key" role="button"><span class="number">5</span></li>
					            <li id="six" class="key" role="button"><span class="number">6</span></li>
					            <li id="seven" class="key" role="button"><span class="number">7</span></li>
					            <li id="eight" class="key" role="button"><span class="number">8</span></li>
					            <li id="nine" class="key" role="button"><span class="number">9</span></li>
					            <li id="dot" class="key" role="button"><span class="number">.</span></li>
					            <li id="zero" class="key" role="button"><span class="number">0</span></li>
					            <li id="back" role="button"><span class="number">X</span></li>
					          </ul>
					        <section id="req-pay" class="full active">
					          <button role="button" class="btn btn-warning fifty" id="BackToMain">Back</button>
					          <button role="button" class="btn btn-primary fifty" id="DoneTaxi">Done</button>
					        </section>
					    </div>
				    </section>
              </div>
              
              ';
        $html .= '<script src="http://vspace.in.th/production/square-cash/js/index.js"></script>';
        return $html;
	}
}