<?php class MoreSystemViewKaew{
	private $email;

	public function __construct($email=""){
		$this->$email = $email;
	}

	public function templateCaseSettingFilter($data,$email,$returnback){
		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	                <span class=" pull-left">
	                  <a class="icon icon-left-nav pull-left" href="#'.$returnback.'/left/reload"></a>
	                </span>
	                <span class=" pull-right" style="visibility: hidden;">
	                  <a class="icon icon-left-nav pull-right" href="#"></a>
	                </span>
	                <span class="mhn-ui-page-title"><span class="fa fa-fw fa-wrench" style="font-size:1em;"></span> Settings </span>
	            </div>
            	<div class="mhn-ui-row mhn-ui-apps">
            		<div class="content">
			          <ul class="table-view">
			              <li class="table-view-divider">Owner</li>
			              <li class="table-view-cell cell-block">
			                <div class="row">
			                  <div class="col-xs-2"></div>
			                  <div class="col-xs-8">
			                    <div class="segmented-control">
			                      <span class="control-item  active " data="myself">My Self</span>
			                      <span class="control-item " data="group">Group</span>
			                    </div>
			                  </div>
			                  <div class="col-xs-2"></div>
			                </div>
			              </li>
			              <li class="table-view-divider">Type</li>
			              <li class="table-view-cell">Incident
			                    <div id="filter_incident" data-name="incident" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>
			              <li class="table-view-cell">Request
			                  <div id="filter_request" data-name="request" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>
			              <li class="table-view-cell">Question
			                  <div id="filter_question" data-name="question" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>
			              <li class="table-view-cell">Implement
			                  <div id="filter_implement" data-name="implement" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>                        
			              <li class="table-view-cell">Install
			                  <div id="filter_install" data-name="install" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>
			              <li class="table-view-cell">Preventive Maintenance
			                  <div id="filter_pm" data-name="pm" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>
			              <li class="table-view-cell">Learning
			                  <div id="filter_learning" data-name="learning" class="toggle">
			                      <div class="toggle-handle"></div>
			                    </div>
			              </li>  
			          </ul>
			      </div>
			    </div>
	            <!--<div class="mhn-ui-bottom-link-bar">
	                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
	            </div>-->';
      return $html;
	}

	private function test(){

	}
}
?>