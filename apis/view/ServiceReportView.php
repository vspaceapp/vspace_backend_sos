<?php class ServiceReportView {

	private $ticket_sid;
	private $email;
	private $tasks_sid;

	function __construct($email, $ticket_sid, $tasks_sid){
		$this->email = $email;
		$this->ticket_sid = $ticket_sid;
		$this->tasks_sid = $tasks_sid;
	}
	public function genHtmlAppServiceReport($data,$backTo){
        $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" href="#'.$backTo.'"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">'.$data['subject_service_report'].'</span>
	          	</div>
          	<div class="mhn-ui-row mhn-ui-apps">
          	<div class="content">
              <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider cell-block">
                    <div>Info</div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Subject <span class="pull-right">'.$data['subject_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service No. <span class="pull-right">'.$data['no_task'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service Type <span class="pull-right">'.$data['service_type_name'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service Status <span class="pull-right">'.$data['status_name'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service Report (PDF) <span class="pull-right">'.(($data['pdf_report']!="-")?'<a href="'.$data['pdf_report'].'">PDF</a>':'-').'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Taxi total <span class="pull-right">'.$data['taxi_fare_total'].'</span></p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider">Engineer</li>
                    <li class="table-view-cell ">'.
                        (($data['last_status']<'500')?'<a class="navigate-right" href="#EditSrEngineer/'.$data['ticket_sid'].'/'.$data['sid'].'/right/1">':'').
                        '<img class="media-object pull-left" src="'.((isset($data['engineer_info']['employee_pic']))?($data['engineer_info']['employee_pic']):'').'">
                            <div class="media-body"><p>'.((isset($data['engineer_info']['thainame']))?$data['engineer_info']['thainame']:'').'<br/><span class="">'.(isset($data['engineer_info']['email'])?$data['engineer_info']['email']:'').'</span><br/>'.(isset($data['engineer_info']['mobile'])?$data['engineer_info']['mobile']:'').'</p></div>'
                        .($data['last_status']<'500'?'</a>':'').
                    '</li>
                </ul>
            </div>

            <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider">Associate Engineer</li>';
                    if(count($data['associate_engineer_info'])>0){
                        foreach ($data['associate_engineer_info'] as $key => $value) {
                        $html .= ('<li class="table-view-cell ">'.
                            ($data['last_status']<'500'?'<a data-pic="'.(isset($value['employee_pic'])?$value['employee_pic']:'').'" data-name="'.(isset($value['thainame'])?$value['thainame']:'').'" data-email="'.$value['email'].'" data-mobile="'.(isset($value['mobile'])?$value['mobile']:'').'"  class="navigate-right" href="#EditAssociateEngineer/'.$data['ticket_sid'].'/'.$data['sid'].'/right/1">':'').
                            '<img class="media-object pull-left" src="'.(isset($value['employee_pic'])?$value['employee_pic']:'').'">
                                <div class="media-body"><p>'.(isset($value['thainame'])?$value['thainame']:'').'<br/><span class="">'.$value['email'].'</span><br/>'.(isset($value['mobile'])?$value['mobile']:'').'</p></div>'.
                            ($data['last_status']<'500'?'</a>':'').
                            '</li>');
                        }
                    }else{
                        $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>-</p></div></li>';
                    }
                    if($data['last_status']<'500'){
                        $html .= '<li class="table-view-cell cell-block"><div style=""><div style="float:right;text-align: center;width: 100px;"><a href="#AddAssociateEngineer/'.$data['ticket_sid'].'/'.$data['sid'].'/right/1"><p><span class="btn btn-positive btn-outlined btn-xs circle icon icon-plus"></span> <br/><small>Add Engineer</small></p></a></div></div></li>';
                    }
            $html .= '</ul>
            </div>

            <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider">Appointment</li>
                    <li class="table-view-cell media cell-block ">
                        '.(($data['can_change_appointment'] && 1==2)?('
                            <a href="#EditTimeAppointment/{{data.ticket_sid}}/{{data.tasks_sid}}/{{data.appointment}}/{{view}}" class="navigate-right">
                              <div class="media-body">
                                <p>Start <span class="pull-right">'.$data['appointment'].'</span></p>
                              </div>
                            </a>
                        '):('
                          <div class="media-body">
                            <p>Start <span class="pull-right">'.$data['appointment'].'</span></p>
                          </div>
                        ')).'
                    </li>
                    <li class="table-view-cell media cell-block">
                      <div class="media-body">
                        <p>Expect Finish
                            <span class="pull-right">
                            '.$data['expect_finish'].'
                            </span>
                        </p>
                      </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                        <p>Expect Overtime
                            <span class="pull-right">
                            '.$data['overtime_expect'].'
                            </span>
                        </p>
                      </div>
                    </li>
                </ul>
              </div>
              <div class="card">
                  <ul class="table-view">
                    <li class="table-view-cell table-view-divider">
                      '.(($data['can_change_end_user'] && 1==2)?('
                        <a href="#SrEditEndUser/{{data.ticket_sid}}/{{data.tasks_sid}}/{{view}}" class="navigate-right">
                          <p>End User Service Report </p>
                        </a>
                      '):('
                          <p>End User Service Report </p>
                      ')).'
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Name <span class="pull-right">'.$data['end_user_contact_name_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Email <span class="pull-right">'.$data['end_user_email_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                            <p>Phone/Mobile<span class="pull-right">'.$data['end_user_phone_service_report'].'/ '.$data['end_user_mobile_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                            <p>Company <span class="pull-right">'.$data['end_user_company_name_service_report'].'</span></p>
                        </div>
                    </li>
                  </ul>
              </div>
          </div></div>';
        return $html;
 	}
	function genHtmlAppEditSrEngineer($data, $ticket_sid, $tasks_sid){
	    $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" href="#service_report/'.$this->ticket_sid.'/'.$this->tasks_sid.'/left/other"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="width: 210px">Change Engineer</span>
	          </div>';
	    $html .= '<div class="mhn-ui-row mhn-ui-apps"><div class="content list-engineer"><ul class="table-view">';
	        if(count($data)>0){
	            foreach ($data as $key => $value) {
	                $html .= '<li class="table-view-cell cell-block_"><a class="navigate-right" data-name="'.$value['thainame'].'" data-email="'.$value['emailaddr'].'" data-mobile="'.$value['mobile'].'" data-pic="'.$value['pic_employee'].'" href="#EditSrEngineer/'.$ticket_sid.'/'.$tasks_sid.'/right/2"><img class="media-object pull-left" src="'.$value['pic_employee'].'" /><div class="media-body"><p>'.$value['thainame'].'<br/><span class="">'.$value['emailaddr'].'</span><br/>'.$value['mobile'].'</p></div></a></li>';
	            }
	        }else{
	            $html .= '<li class="table-view-cell cell-block"><div><p>Not found employee that you can add task</p></div></li>';
	        }
	    $html .= '</ul></div></div>';
	    return $html;
	}

	function genHtmlAppEditSrEngineerConfirm($data, $ticket_sid, $tasks_sid, $case_change_engineer){
	    // $html = '<header class="bar bar-nav">
	    //           <a class="icon icon-left-nav pull-left" href="#EditSrEngineer/'.$ticket_sid.'/'.$tasks_sid.'/left/1"></a>
	    //         <div class="" style="width: 210px"><h1 class="title">Confirm Change Engineer</h1></div>
	    //       </header>';

	     $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" href="#EditSrEngineer/'.$ticket_sid.'/'.$tasks_sid.'/left/1"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="width: 210px">Confirm Change Engineer</span>
	          </div>';

	    $html .= '<div class="mhn-ui-row mhn-ui-apps"><div class="content">';
	    $html .= '<ul class="table-view">';
	    $html .= '<li class="table-view-divider cell-block"><div>From Engineer</div></li>';
	    $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$data['engineer_info']['employee_pic'].'" /><div class="media-body"><p>'.$data['engineer_info']['thainame'].'<br/>'.$data['engineer_info']['emailaddr'].'<br/>'.$data['engineer_info']['mobile'].'</p></div></li>';
	    $html .= '<li class="table-view-divider cell-block"><div>To Engineer</div></li>';
	    if(isset($case_change_engineer['name'])){
	        // $html .= '<div>'.$case_change_engineer['name'].'</div>';
	        $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$case_change_engineer['pic'].'" /><div class="media-body"><p>'.$case_change_engineer['name'].'<br/><span class="change_to">'.$case_change_engineer['email'].'</span><br/>'.$case_change_engineer['mobile'].'</p></div></li>';
	        $html .= '<li class="table-view-cell cell-block"><div><button id="btnConfirmChangeEngineer" data-change-to="'.$case_change_engineer['email'].'" class="btn btn-block btn-positive">Done</button></div></li>';
	    }
	    $html .= '</ul>';
	    $html .= '</div></div>';
	    return $html;
	}

	function genHtmlAddAssociateEngineer($data, $ticket_sid, $tasks_sid){
	    // $html = '<header class="bar bar-nav">
	    //           <a class="icon icon-left-nav pull-left" href="#service_report/'.$ticket_sid.'/'.$tasks_sid.'/left/other"></a>
	    //           <a class="icon icon-right-nav pull-right" href="#AddAssociateEngineer/'.$ticket_sid.'/'.$tasks_sid.'/right/2"></a>
	    //         <div class="" style="width: 210px"><h1 class="title">Add Associate Engineer</h1></div>
	    //       </header>';

	     $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" href="#service_report/'.$ticket_sid.'/'.$tasks_sid.'/left/other"></a>
              		<a class="icon icon-right-nav pull-right" href="#AddAssociateEngineer/'.$ticket_sid.'/'.$tasks_sid.'/right/2"></a>
	            	<span class="mhn-ui-page-title" style="">Add Associate Engineer</span>
	          </div>';

	    $html .= '<div class="mhn-ui-row mhn-ui-apps"><div class="content list-engineer"><ul class="table-view">';
	        if(count($data)>0){
	            foreach ($data as $key => $value) {
	                $html .= '<li class="table-view-cell cell-block_"><img class="media-object pull-left" src="'.$value['pic_employee'].'" /><div class="media-body"><p>'.$value['thainame'].'<br/><span class="">'.$value['emailaddr'].'</span><br/>'.$value['mobile'].'</p></div><div data-pic="'.$value['pic_employee'].'" data-email="'.$value['email'].'" data-name="'.$value['thainame'].'" data-mobile="'.$value['mobile'].'" class="toggle"><div class="toggle-handle"></div></div></li>';
	            }
	        }else{
	            $html .= '<li class="table-view-cell cell-block"><div><p>Not found employee that you can add task</p></div></li>';
	        }
	    $html .= '</ul></div></div>';
	    return $html;
	}
	function genHtmlAddAssociateEngineerComfirm($data, $ticket_sid, $tasks_sid, $case_change_engineer){
	    // $html = '<header class="bar bar-nav">
	    //           <a class="icon icon-left-nav pull-left" href="#AddAssociateEngineer/'.$ticket_sid.'/'.$tasks_sid.'/left/3"></a>
	    //         <div class="" style="width: 210px"><h1 class="title">Confirm Add Associate Engineer</h1></div>
	    //       </header>';
	     $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" href="#AddAssociateEngineer/'.$ticket_sid.'/'.$tasks_sid.'/left/1"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Confirm Add Associate Engineer</span>
	          </div>';

	    $html .= '<div class="mhn-ui-row mhn-ui-apps"><div class="content">';
	    $html .= '<ul class="table-view">';
	    // $html .= '<li class="table-view-divider cell-block"><div>From Engineer</div></li>';
	    // $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$data['engineer_info']['employee_pic'].'" /><div class="media-body"><p>'.$data['engineer_info']['thainame'].'<br/>'.$data['engineer_info']['emailaddr'].'<br/>'.$data['engineer_info']['mobile'].'</p></div></li>';
	    $html .= '<li class="table-view-divider cell-block"><div>New Associate Engineer</div></li>';
	    if(count($case_change_engineer)>0){
	        foreach ($case_change_engineer as $key => $value) {
	            $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$value['pic'].'" /><div class="media-body"><p>'.$value['name'].'<br/><span class="change_to">'.$value['email'].'</span><br/>'.$value['mobile'].'</p></div></li>';
	        }
	        $html .= '<li class="table-view-cell cell-block"><div><button id="btnConfirmAddAssociateEngineer" class="btn btn-block btn-positive">Done</button></div></li>';
	    }else{
	        $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>Please choose Engineer</p></div></li>';
	    }
	    
	    $html .= '</ul>';
	    $html .= '</div></div>';
	    return $html;
	}

	function genHtmlEditAssociateEngineer($data, $case_change_engineer){
	    // $html = '<header class="bar bar-nav">
	    //           <a class="icon icon-left-nav pull-left" href="#service_report/'.$data['ticket_sid'].'/'.$data['sid'].'/left/other"></a>
	    //         <div class="" style="width: 210px"><h1 class="title">Remove Engineer</h1></div>
	    //       </header>';

	    $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" href="#service_report/'.$this->ticket_sid.'/'.$this->tasks_sid.'/left/other"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Remove Engineer</span>
	          </div>';
	            
	    $html .= "<div class='mhn-ui-row mhn-ui-apps'><div class='content'><div class='card'><ul class='table-view'>";
	    if(isset($case_change_engineer['email']) && $case_change_engineer['email']!=""){
	        $html .= "<li class='table-view-divider cell-block'><div><p>Click Done for remove Engineer</p></div></li><li class='table-view-cell'><img class='media-object pull-left' src='".$case_change_engineer['pic']."' /><div class='media-body'><p>".$case_change_engineer['name']."<br/>".$case_change_engineer['email']."<br/>".$case_change_engineer['mobile']."</p></div></li>";
	        $html .= "<li class='table-view-cell cell-block'><div><button id='btnRemoveEngineerConfirm' data-email='".$case_change_engineer['email']."' class='btn btn-block btn-positive'>Done</button></div></li>";
	    }else{
	        $html .= "<li class='table-view-cell cell-block'>-</li>";
	    }
	    $html .= "</ul></div></div></div>";
	    return $html;
	}

	function templateServiceReportUnsigned($data){
	    $html = '<div class="content-wrapper"><section class="content">';
	    $html .= '<ul class="table-view"><li class="table-view-cell cell-block"><div><p>Service Report Unsigned<span class="pull-right">
	        Sort By: 
	        <button class="btn btn-primary sort-by" data-sort-by="appointment">Appointment</button>
	        <button class="btn btn-primary sort-by" data-sort-by="end_user_contact_name_service_report">End User</button>
	        <button class="btn btn-primary sort-by" data-sort-by="engineer">Engineer</button>
	        </span></p></div></li></ul>';
	    $html .= '<ul class="table-view">';
	    $html .= '<li class="table-view-cell cell-block"><div><p><span class="pull-right">'.count($data).' Rows.</span></p></div></li>';
	    foreach ($data as $key => $value) {
	        $html .= '<li class="table-view-cell cell-block">
	                    <img class="media-object pull-left" src="'.$value['pic'].'">
	                    <div class="media-body">
	                        <div class="row">
	                            <div class="col-md-6">
	                                <p>'.$value['subject_service_report'].'</p>
	                                <p>'.$value['end_user_company_name'].'</p>
	                                <p>'.$value['no_task'].'<span data="no_ticket" class="pull-right">'.$value['no_ticket'].'</span></p>
	                                <p>Appointment <span data="appointment" class="pull-right">'.$value['appointment'].'</span></p>
	                                <p>'.$value['engineer_thainame'].'</p>
	                            </div>
	                            <div class="col-md-6">
	                                <p><span class="pull-right-">'.$value['end_user_contact_name_service_report'].'</span></p>
	                                <p>'.$value['end_user_email_service_report'].'</p>
	                                <p>'.$value['end_user_mobile_service_report'].'<span data="" class="pull-right">'.$value['end_user_phone_service_report'].'</span></p>
	                                <p><span data="" class="pull-right-">'.$value['end_user_company_name_service_report'].'</span></p>
	                                <p>
	                                    <span class="pull-right"><button class="resend_signed btn btn-warning btn- btn-outlined" data-tasks-sid="'.$value['sid'].'" >Resend Email To Customer Sign</button>
	                                    </span>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
	                </li>';
	    }
	    $html .= '</ul></section></div>';
	    return $html;
	}
}