<?php
include "ProjectManagementSummaryView.php";
include "ProjectManagementCasesView.php";
class ProjectManagementView{
	private $email;
  private $data;
  private $tabName;
  private $canAssignTo;
  private $windowHeight;
  private $permission_role;
  private $listUserCanAddProject;

	public function __construct( $email = ""){
		$this->email  = $email;
	}

	public function templateProjectManagement($email,$data, $order_by, $search="",$permission_role, $canAssignTo, $windowHeight){
    $this->data = $data;
    $this->canAssignTo = $canAssignTo;
    $this->windowHeight = $windowHeight;
    $this->permission_role = $permission_role;

		$html = '<div class="content">
				    <div class="row" style="margin: 0px 0px 10px 0px;">
                  <div class="col-xs-6" style="padding-left: 5px;">
                    <table><thead><tr><th><span class="title">Project Management</span><span class="subTitle"></span></th></tr></thead></table>
                  </div>
                </div>
                  <div class="filter" style="width: 100%;margin-bottom: 0px; padding-left:0px;">
                      <!--<span class="m m-search" style="left: 50px;"></span>-->
                      <input type="text" id="search" class="form-control" placeholder="FILTER" value="'.$search.'" style="background-color: #ffffff; border: 1px solid #ccc;">
                    </div>

                <div class="row" style="margin-top:5px;">
                  <div class="col-xs-6" style="margin: 0px 0px;">
                        <div class="box-tools pull-" style="text-align: left;">'.
                            (($permission_role['project_create_able'])?'<button style="padding:4px;" type="button" id="CreateProject" data-toggle="control-sidebar" class="btn btn-primary- btn-sm  ">
                              <i class="fa fa-edit"></i> Create Project
                           </button>':'').
                           '<!--<button type="button" id="toggleImage" class="btn btn-primary- btn-sm daterange ">
                               <i class="fa fa-fw fa-image"></i>
                           </button>
                           <div class="btn-group">
                               <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               <i class="fa fa-fw fa-gear"></i></button>
                                <ul class="dropdown-menu pull-right" role="menu_">
                                   <li><a href="javascript:void(0);"><input type="checkbox" /></a></li>
                                   <li class="divider"></li>
                                   <li><a href="javascript:void(0);">Standby Buddy 1</a></li>
                               </ul>
                            </div>-->
                       </div>
                  </div>
                  
                  <!--<div class="col-xs-1" style="vertical-align: bottom;padding-right: 0px;">
                    <span class="pull-right"> </span>
                  </div>-->
                  <div class="col-xs-4" style="padding: 0px 0px;">
                    <select id="sort" class="form-control input-sm">
                      <option '.(($order_by['column']=='P.create_datetime')?'selected':'').' value="P.create_datetime">ORDER BY CREATE</option>
                      <option '.(($order_by['column']=='P.contract')?'selected':'').' value="P.contract">ORDER BY CONTRACT</option>
                      <option '.(($order_by['column']=='P.project_type')?'selected':'').' value="P.project_type">ORDER BY TYPE</option>
                      <option '.(($order_by['column']=='PO.owner')?'selected':'').' value="PO.owner">ORDER BY OWNER</option>
                    </select>
                  </div>
                  <div class="col-xs-2" style="padding: 0px 15px 0px 0px;">
                    <select id="sort_type" class="form-control input-sm">
                      <option '.(($order_by['type']=='desc')?'selected':'').' value="desc">DESC</option>
                      <option '.(($order_by['type']=='asc')?'selected':'').' value="asc">ASC</option>
                    </select>
                  </div>
                  <div class="col-xs-12"><span class="pull-right">'.count($this->data).' ROWS.</span></div>
                  '.$this->tableView().'
                </div>
          </div>
		      <div class="properties" style="overflow: hidden;">
                  <div class="slider" style="cursor:initial;"></div>
                  <div class="propertyEditorWrapper">
                    <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#projectinfo" data-toggle="tab" aria-expanded="true">Project Info</a></li>
                        <li class=""><a href="#Cases" data-toggle="tab" aria-expanded="false">Cases</a></li>
                        <!--<li class=""><a href="#detail" data-toggle="tab" aria-expanded="false">Detail</a></li>-->
                      </ul>
                    </div>
                    <div class="propertyEditor" style="width:98%; height: 90%; position: relative;">
                    
                    </div>
                  </div>
            </div>';
    $html .= (($permission_role['project_create_able'])?$this->createProjectView():'');
		return $html;
	}

  public function createProjectView(){
      $html = '<style>
      #btnCreateDummyContract {
        background-color: #2776C5 !important;
        color: #ffffff !important;
      }
      </style>';
      $html .= '<aside class="control-sidebar control-sidebar-dark " >
                  <div class="box-tools pull-right" style="margin-right: 10px;">
                   
                    <button type="button" class="btn btn-box-tool" id="closeCrateProject" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                  <div style="margin: 30px 15px;" id="main-form">'.$this->selectProjectStatus().' '.$this->createDummyContract().' '.$this->projectTypeForm().' '.$this->contractForm().' '.$this->ownerForm().' '.$this->periodForm().' '.$this->summaryForm().'</div>
          </aside>';
      return $html;
  }

  public function setPermissionRole($permission_role){
      $this->permission_role = $permission_role;
  }

  private function selectProjectStatus(){
    $html = '<div id="selectProjectStatus" class="form-project">';
    $html .= '<div>Select Phase</div>';
    $html .= '<div class="row"><div class="col-xs-12"><ul class="control-sidebar-menu">';
     
    $html .= '<li>
            <a href="javascript:void(0)" class="optionalListProjectStatus" data-type="Pre Sale">
              <div class="menu-info" style="margin-left:10px;">
                <h4 class="control-sidebar-subheading">Pre Sale</h4>
                <p>Project ที่ยังไม่มี Contract</p>
              </div>
            </a>
          </li>';
    $html .= '<li>
            <a href="javascript:void(0)" class="optionalListProjectStatus" data-type="Post Sale">
              <div class="menu-info" style="margin-left:10px;">
                <h4 class="control-sidebar-subheading">Post Sale</h4>
                <p>Project ที่มี Contract แล้ว</p>
              </div>
            </a>
          </li>'; 
      
    $html .= '</ul></div></div>';
    $html .= '</div>';
    return $html;
  }

  private function createDummyContract(){
    $html = '<div id="createDummyContract" class="form-project">';
    $html .= '<div>Create Dummy Contract</div>';
    $html .= '<div class="row"><div class="col-xs-12"><ul class="control-sidebar-menu" style="margin: 0 15px 0px 0px;">';
     
    $html .= '<li>
            <div class="menu-info" style="margin-left:20px;"><div>Project Name</div><input class="form-control" id="create_dummy_contract_project_name" type="text" /></div>
          </li>';
    $html .= '<li>
            <div class="menu-info" style="margin-left:20px;"><div>Customer Company</div><input class="form-control" id="create_dummy_contract_customer_company" type="text" /></div>
          </li>';
    $html .= '<li style="margin-top:20px;">
            <div class="menu-info" style="margin-left:20px;"><button type="button" style="width:100%;" class="btn btn-xs btn-primary" id="btnCreateDummyContract">Next</button></div>
          </li>';
    $html .= '</ul></div></div>';
    $html .= '</div>';
    return $html;
  }

  private function projectTypeForm(){
      // $dataProjectType = array(
      //   array('icon'=>'../img/icon/implement.png','name'=>'Implement','detail'=>'Project Imeplement'),
      //   array('icon'=>'../img/icon/install.png','name'=>'Install','detail'=>'Project Install'),
      //   array('icon'=>'../img/icon/project-management.png','name'=>'Learning','detail'=>'Project Learning, none contract')
      // );
      $dataProjectType = json_decode($this->permission_role['project_create_able'], true);
      if(is_array($dataProjectType))
        return $this->optionalList($dataProjectType);
      return "";
  }
  private function optionalList($dataProjectType=array()){
    $html = '<div id="selectProjectType" class="form-project">';
    $html .= '<div>Select The Project Type</div>';
    $html .= '<div class="row"><div class="col-xs-12"><ul class="control-sidebar-menu">';

      foreach ($dataProjectType as $key => $value) {
        $html .= '<li>
            <a href="javascript:void(0)" class="optionalList" data-type="'.$value['name'].'">
              <img class="media-object pull-left" src="'.$value['icon'].'">
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">'.$value['name'].'</h4>
                <p>'.$value['detail'].'</p>
              </div>
            </a>
          </li>'; 
      }
    $html .= '</ul></div></div>';
    $html .= '</div>';
    return $html;
  }

  private function contractForm(){
      $html = '<div id="contractForm" class="form-project hide">';
      $html .= '<div><a href="javascript:void(0);" class="btn btn-box-tool" id="backToProjectStatus"><i class="fa fa-fw fa-chevron-left"></i></a></div><div></div>';
      $html .= '<div class="row"><div class="col-xs-12">';
      $html .= '<div><br/>Find Contract/ Dummy Contract</div>';
      $html .= '<div><input class="form-control" id="contract_no" /></div>';
      $html .= '<div id="listContract" style="overflow-y: auto; padding:10px; width:100%; height: '.($this->windowHeight-200).'px;"></div>';
      $html .= '</div></div>';
      $html .= '</div>';
      $html .= '';
      return $html;
  }

  private function ownerForm(){
      $html = '<div id="ownerForm" class="form-project hide">';
        $html .= '<div>
          <a href="javascript:void(0);" class="btn btn-box-tool" style="visibility: hidden;" id="backToContractForm"><i class="fa fa-fw fa-chevron-left"></i></a>
          <a href="javascript:void(0);" class="btn btn-box-tool pull-right" id="nextToPeriodForm"><i class="fa fa-fw fa-chevron-right"></i></a>
        </div>';
        $html .= '<div class="row" style="position: relative;height: 90%; margin-top:10px;">';
          $html .= '<div class="col-xs-12" style="position:_; height:90%;">';
            $html .= '<div id="" class="row">';
                $html .= '<div class="col-xs-6"  id="listStaff">';
                $html .= '<div>List Staff<span class="numberOfStaff pull-right" >'.count($this->canAssignTo).' Row(s)</span></div>';
                $html .= '<ul class="control-sidebar-menu" style="height:'.(($this->windowHeight)?($this->windowHeight-200).'px;':'auto;').' overflow-y:auto; overflow-x:hidden;">';
                    foreach ($this->canAssignTo as $key => $value) {
                      $html .= '<li data-type="'.$value['email'].'" class="optionalStaff">
                                  <a href="javascript:void(0)" class="" data-type="'.$value['email'].'">
                                    <img class="media-object pull-left" src="'.$value['pic_employee'].'">
                                    <div class="menu-info">
                                      <h4 class="control-sidebar-subheading">'.$value['thainame'].'</h4>
                                      <p>'.$value['engname'].'<br/>'.$value['email'].'<br/>'.$value['mobile'].'</p>
                                    </div>
                                  </a>
                                </li>';
                    }
                $html .= '</ul></div>';
                $html .= '<div class="col-xs-6" id="selectedStaff"><div>Selected Staff <span class="numberOfStaff pull-right">0 Row(s)</span></div><ul class="control-sidebar-menu" style="height:'.($this->windowHeight-200).'px; overflow-y:auto; overflow-x:hidden;" ></ul></div>';
            $html .= '</div>';
          $html .= '</div>';
          $html .= '<div class="col-xs-12" style="position:_; height:10%;"></div>';
        $html .= '</div>';
      $html .= '</div>';
      $html .= '';
      return $html;
  }

  private function periodForm(){
      $html = '<div id="periodForm" class="form-project hide">';
      $html .= '<div>
        <a href="javascript:void(0);" class="btn btn-box-tool" id="backToOwnerForm"><i class="fa fa-fw fa-chevron-left"></i></a>
        <a href="javascript:void(0);" class="btn btn-box-tool pull-right" id="nextToSummaryForm"><i class="fa fa-fw fa-chevron-right"></i></a>
      </div>';
      $html .= '<div class="row"><div class="col-xs-12">';
      $html .= '<div><br/>Period Project</div>';
      $html .= '<div id="">
                    <div>
                      <ul class="table-view">
                          <li class="table-view-cell cell-block">
                          <div id="calendar-details">
                            <div class="check-in">
                              <h5>Start Date</h5>
                              <h6 id="check-in-date">Choose a date</h6>
                            </div>
                            <div class="arrow"></div>
                            <div class="check-out">
                              <h5>End Date</h5>
                              <h6 id="check-out-date">Choose a date</h6>
                            </div>
                          </div>
                          <div id="calendar" style="width:100%;overflow:auto;height:'.($this->windowHeight-300).'px;"></div>
                          </li>
                      </ul>
                    </div>
                </div>';
      $html .= '</div></div>';
      $html .= '</div>';
      $html .= '';
      return $html;
  }
  public function setWindowHeight($windowHeight){
    $this->windowHeight = $windowHeight;
  }
  private function mandayForm(){
      $html = '<div id="mandayForm" class="form-project hide">';
      $html .= '<div>
        <a href="javascript:void(0);" class="btn btn-box-tool" id="backToPeriodForm"><i class="fa fa-fw fa-chevron-left"></i></a>
        <a href="javascript:void(0);" class="btn btn-box-tool pull-right" id="nextToSummaryForm"><i class="fa fa-fw fa-chevron-right"></i></a>
      </div>';
      $html .= '<div class="row"><div class="col-xs-12">';
      $html .= '<div><br/>Mandays Project Per Engineer</div>';
      $html .= '<div id="">
                    <div><br/>Man Days (Unit Day, 1 Man Day = 8 Hours)</div>
                    <div><input id="manday" type="number" class="form-control" /></div>
                    <div><br/>Man Hours (Unit Hours, 8 Hours = 1 Man Day)</div>
                    <div><input id="manhours" type="number" class="form-control" /></div>
                </div>';
      $html .= '</div></div>';
      $html .= '</div>';
      $html .= '';
      return $html;
  }
  private function summaryForm(){
      $html = '<div id="summaryForm" class="form-project hide">';
      $html .= '<div>
        <a href="javascript:void(0);" class="btn btn-box-tool" id="backToPeriodForm"><i class="fa fa-fw fa-chevron-left"></i></a>
        <!--<a href="javascript:void(0);" class="btn btn-box-tool pull-right" id="nextToSummaryForm"><i class="fa fa-fw fa-chevron-right"></i></a>-->
      </div>';
      $html .= '<div class="row"><div class="col-xs-12">';
      $html .= '<div><br/>Confirm Project</div>';
      $html .= '<div class="summaryForm">
                </div>';
      $html .= '<div style="margin-top:20px;"><button id="CreateProjectClick" class="btn btn-xs btn-primary" style="width:100%;display:block;">Done</button></div>';
      $html .= '</div></div>';
      $html .= '</div>';
      $html .= '';
      return $html;
  }

  private function tableView(){
      $html = '';
      $html .= '<div class="container-user-administration-page project-list"><div class="list-group"><ul>';
                foreach ($this->data as $key => $value) {
                    $html .= '
                    <li class="box_data" data_project_sid="'.$value['sid'].'" data_contract="'.$value['contract'].'">
                      <!--<div class="picture"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/peterlandt/48.jpg">
                        <div class="badge green"><i class="fa fa-check"></i></div>
                      </div>-->
                      <div class="content">
                          <div class="description">
                              <!--<div class="location"><i class="fa fa-map-marker"></i><span>Paris, France</span></div>-->
                              <div class="tags">
                                <div class=" ">'.$value['contract'].'</div>
                                <!--.tag.blue Clerk-->
                                <!--<div class="tag pull-right green">Online</div>-->
                                <!--.tag.red Inactive-->
                                <br/>
                                <div class=" ">'.$value['end_user'].'</div>
                              </div>
                          </div>
                      </div>
                      <div class="footer">
                        <div class="info left"><i class="fa fa-clock-o"></i><span>Man Hours '.$value['allManHours'].' Hr.</span></div><br/>
                        <div class="info left"><i class="fa fa-clock-o"></i><span>Used '.$value['use_hours'].'.'.$value['use_minutes_mod'].' Hr.</span></div>
                        <div class="info right"><i class="fa fa-calendar"></i><span>Added '.$value['create_datetime_df'].'</span></div>
                      </div>
                      <!--<div class="functions">
                        <div class="func"><i class="fa fa-envelope"></i></div>
                        <div class="func"><i class="fa fa-trash"></i></div>
                      </div>-->
                    </li>';
              }
        $html .= '</ul></div></div>';
    return $html;
  }

  private $engineer;
  private $project_detail;
  private $man_hours_total;
  public function templateProjectDetail($email,$token,$data,$engineerR,$project_detail, $tabName, $listUserCanAddProject) {
        $this->engineer = $engineerR;
        $this->tabName = $tabName;
        $this->data = $data;
        $this->project_detail = $project_detail;
        $this->listUserCanAddProject = $listUserCanAddProject;

        $this->man_hours_total = $this->project_detail['man_hours_total'];
        $html = '';
        if($this->tabName=="#projectinfo"){
            $html .= $this->infoProject();
            // $html .= $this->summaryProject();
        }else if($this->tabName=="#Cases"){
            $html .= $this->caseInContract();
        }
        return $html;
  } 

  private function infoProject(){
    return ProjectManagementSummaryView::infoProject($this->project_detail,$this->engineer, $this->man_hours_total, $this->data, $this->email, $this->listUserCanAddProject);
  }
  private function summaryProject(){
    return ProjectManagementSummaryView::view($this->engineer, $this->man_hours_total, $this->data);
  }

  private function projectOwner(){

  }

  private function caseInContract(){
    return ProjectManagementCasesView::view($this->data);
  }

  public function setCanAssignTo($canAssignTo){
      $this->canAssignTo = $canAssignTo;
  }

  public function templateProjectManagementApp($email, $data, $order_by, $search, $permission_role,$canAssignTo,$windowHeight){
      $this->email = $email;
      $this->data = $data;
      $this->search = $search;
      $this->permission_role = $permission_role;
      $this->canAssignTo = $canAssignTo;
      $this->windowHeight = $windowHeight;

      require_once '../../view/ProjectManagementAppView.php';

      return ProjectManagementAppView::listProject($email, $data, $order_by, $search, $permission_role,$canAssignTo,$windowHeight);
  }
}

?>


