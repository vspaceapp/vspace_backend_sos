<?php
class TodolistViewApp
{
    private $email;
    private $data;
    private $tabName;
    private $canAssignTo;
    private $windowHeight;
    private $permission_role;
    public function __construct($email = "")
    {
        $this->email = $email;
    }

    public function templateTodolistViewApp($email, $data, $order_by, $search = "", $permission_role, $canAssignTo, $windowHeight)
    {
        require_once '../../view/FormEndUserView.php';
        require_once '../../view/FormAppointmentSrView.php';
        require_once '../../view/SignatureServiceReportView.php';
        require_once '../../view/TaxiViewApp.php';
        require_once '../../view/ActionServiceReportViewApp.php';
        require_once '../../view/FollowUpAndClosedViewApp.php';
        require_once '../../view/FollowUpAppView.php';
        $this->data            = $data;
        $this->canAssignTo     = $canAssignTo;
        $this->windowHeight    = $windowHeight;
        $this->permission_role = $permission_role;

        $html = '<link href="http://vspace.in.th/develop/pos/tinder-animation-gsap/css/style.css" rel="stylesheet"></link>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/CSSPlugin.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/easing/EasePack.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenLite.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.0/utils/Draggable.min.js"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.min.css" rel="stylesheet prefetch" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.concat.min.js"></script>
            <script src="http://vspace.in.th/pages/lib/moment.min.js"></script>
            <style>
              .TodolistViewApp .app {
                background-color: initial;
                height: 109%;
              }
              .TodolistViewApp .app div.photo {
                left:0;right:0;
                width:90%; background-color:white;
                height:initial;
              }
              .TodolistViewApp .content-object {
                left:0;
                right:0;
                position: absolute;
                width:100%;overflow-y: auto; overflow-x: hidden;
                height: calc(100% - 65px);
              }
              .TodolistViewApp .app span.meta {
                margin-top: auto;
                bottom: 0px;
                position: relative;
                width: 100%;
              }
              .TodolistViewApp .app {
                padding-top: 0px;
              }
              .TodolistViewApp .app .footer {
                  position: absolute;
                  margin: auto;
                  bottom: 0px;
                  left: 0;
                  padding: initial;
                  width: 90%;
                  right: 0;
              }
              .TodolistViewApp .content-data {
                color: #222d32;
                padding:10px;
              }
              .TodolistViewApp .content-data .clear{
                clear:both;
              }
              .TodolistViewApp .content-data .media-object {
                float:left;
              }
              .TodolistViewApp .info, .TodolistViewApp .rate {background-color:#ffffff;}
              .TodolistViewApp .meta {
                color: #aaaaaa;
                text-align: right;
                font-size: 0.8em;
              }
              .TodolistViewApp .moments {
                width: initial;
              }
              .TodolistViewApp div.photo {
                box-shadow: none;
                border-radius: 0px;
                border: none;
              }
              .TodolistViewApp .app::before {
                position: absolute;
                color: #aaaaaa;
                left: initial;
                top: 150px;
                content: "Scroll here to view below";
                -webkit-transform: rotate(30deg);
                -moz-transform: rotate(30deg);
                -o-transform: rotate(30deg);
                -ms-transform: rotate(30deg);
                transform: rotate(270deg);
                left: -70px;
              }
              .TodolistViewApp .rate:nth-child(1),.TodolistViewApp .rate:nth-child(2) {
                  margin-left: 4px;
                  background: none;
                  background-position: center center;
                  background-repeat: no-repeat;
                  background-color: #ffffff;
              }
              .TodolistViewApp .rate:nth-child(3) {
                  margin-left: -4px;
                  background: none;
                  background-size: 25px;
                  background-position: center;
                  background-repeat: no-repeat;
                  background-color: #ffffff;
              }
              .TodolistViewApp .rate#btnLeft::after {
                  content: "";
                  bottom: -20px;
                  left: 15px;
                  font-size: 0.85em;
                  position: absolute;
                  text-align: center;
              }
              .TodolistViewApp .rate#btnRight::after{
                position: absolute; content: "";
                width: 100px;
                bottom: -20px;
                left: 90px;
                font-size: 0.85em;
              }
              .TodolistViewApp .rate#btnLeft.step100{
                content: "";
                visibility:hidden;
              }
              .TodolistViewApp .rate#btnRight.step100::after{
                content: "Start The Journey";
              }

              .TodolistViewApp .rate#btnLeft.step200{
                content: "";
                visibility:hidden;
              }
              .TodolistViewApp .rate#btnRight.step200::after{
                content: "Start The Task";
              }

              .TodolistViewApp .rate#btnLeft.step300{
                content: "";visibility:hidden;
              }
              .TodolistViewApp .rate#btnRight.step300::after{
                content: "Close The Job";
              }

              .TodolistViewApp .rate#btnLeft.step300::after{
                content: "End User Signature via Email";
              }
              .TodolistViewApp .rate#btnRight.step300::after{
                content: "End User Signature Now";
              }
              .TodolistViewApp .rate {
                border-radius: initial;width: 100%;
              }
              .TodolistViewApp .footer {
                width:90%;
              }
              .TodolistViewApp .footer .btnLeft button {
                color:#ffffff;
              }
              .TodolistViewApp .footer .btnLeft {
                float:left;width:49%; margin-right:1%;display:none;
              }
              .TodolistViewApp .footer .btnRight {
                float:right;width:49%; margin-left:1%;display:none;
              }
              .TodolistViewApp .footer .btnLeft.step100 {
                display:none;
              }
              .TodolistViewApp .footer .btnRight.step100 {
                display:block;
              }
              .TodolistViewApp .footer .btnLeft.step200 {
                display:none;
              }
              .TodolistViewApp .footer .btnRight.step200 {
                display:block;
              }
              .TodolistViewApp .footer .btnLeft.step300 {
                display:block;
              }
              .TodolistViewApp .footer .btnRight.step300 {
                display:block;
              }
              .TodolistViewApp .footer .btnLeft.step400 {
                display:block;
              }
              .TodolistViewApp .footer .btnRight.step400 {
                display:block;
              }
              .TodolistViewApp .footer .btnLeft.step500 {
                display:none;
              }
              .TodolistViewApp .footer .btnRight.step500 {
                display:block;
              }

              .TodolistViewApp .btn-block {
                font-size:14px;
              }
              .TodolistViewApp #req-pay {
                text-align: center;
              }
              .TodolistViewApp #BackToMain {
                display:none;
              }
              .page.kb-active {
                height:60%;
              }
              .TodolistViewApp .messages .messages-content {
                height: calc(101% - 50px);
              }
              .TodolistViewApp .datepicker .block {box-shadow: none;}
            </style>
            <div class="TodolistViewApp">
                <div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="backToMoreSystem pull-left" href="#MoreSystem" style="color:#fff;"><span class="icon icon-left-nav"></span></a>
                  <a class="pull-left" id="BackToMain" href="javascript:void(0);" style="color:#fff;"><span class="icon icon-left-nav"></span></a>
                  <i class="ion-clipboard"></i> To Do List
                </div>
                <div class="mhn-ui-row mhn-ui-apps animated bounceInRight">
                    <div class="app" style="width:100%;">
                      <div class="content-object">
                      </div>
                      <div class="footer animated bounceInRight">
                            <!--<div class="discription-step" style="width:100%;"></div>-->
                            <div style="width:100%;">
                              <div class="btnLeft"><button class="btn btn-block btn-warning">L</button></div>
                              <div class="btnRight"><button class="btn btn-block btn-primary">R</button></div>
                              <div style="clear:both;"></div>
                            </div>
                      </div>
                    </div>' . FormEndUserView::view() . '' . SignatureServiceReportView::view() . FormAppointmentSrView::view() . TaxiViewApp::view() . ActionServiceReportViewApp::view($this->windowHeight) . FollowUpAndClosedViewApp::view() . FollowUpAppView::view() . '
                </div>
            </div>
            <script>
              var sequenceData = 0;
              var allDataForPage;
              var currentTaskStatus;
              var currentTaskSid;
              var serviceNo;

               $(".TodolistViewApp div#swipe_like").on("click", function() {
                    // swipeLike();
                  location.reload();
                });
                $(".TodolistViewApp div#swipe_dislike").on("click", function() {
                    swipeDislike();
                });
                addNewProfile();
                function swipe() {
                    Draggable.create("#photo", {
                        throwProps: true,
                        onDragEnd: function(endX) {
                            if (Math.round(this.endX) > 0) {
                                swipeLike();
                            } else {
                                swipeDislike();
                            }
                            console.log(Math.round(this.endX));
                        }
                    });
                }

                function swipeLike() {
                    var $photo = $("div.content-object").find("#photo");
                    var swipe = new TimelineMax({
                        repeat: 0,
                        yoyo: false,
                        repeatDelay: 0,
                        onComplete: remove,
                        onCompleteParams: [$photo]
                    });
                    swipe.staggerTo($photo, 0.8, {
                        bezier: [{
                            left: "+=400",
                            top: "+=300",
                            rotation: "60"
                        }],
                        ease: Power1.easeInOut
                    });
                    sequenceData--;
                    if(sequenceData<0){
                      sequenceData = allDataForPage.length-1;
                    }
                    addNewProfile();
                }

                function swipeDislike() {
                    var $photo = $("div.content-object").find("#photo");
                    var swipe = new TimelineMax({
                        repeat: 0,
                        yoyo: false,
                        repeatDelay: 0,
                        onComplete: remove,
                        onCompleteParams: [$photo]
                    });
                    swipe.staggerTo($photo, 0.8, {
                        bezier: [{
                            left: "+=-350",
                            top: "+=300",
                            rotation: "-60"
                        }],
                        ease: Power1.easeInOut
                    });
                    sequenceData++;
                    addNewProfile();
                }
                function remove(photo) {
                    $(photo).remove();
                }

                $(".TodolistViewApp").on("click",".btnRight.step100 button",function(){
                    executeStartTheJourney();
                });

                $(".TodolistViewApp").on("click",".btnLeft.step100 button",function(){
                  // alert(currentTaskSid);
                });

                $(".TodolistViewApp").on("click",".btnRight.step200 button",function(){
                    $("#FormTaxiView").show();
                    $(".TodolistViewApp #BackToMain").css("display","initial");
                    $(".TodolistViewApp #FormTaxiView").attr("class","animated bounceInRight");
                    $(".TodolistViewApp .backToMoreSystem").hide();
                    $(".TodolistViewApp .app").hide();
                });

                $(".TodolistViewApp").on("click",".btnRight.step300 button",function(){
                    getDataTaskDetail(currentTaskSid).done(function(res){
                        $("#FollowUpAndClosedViewApp #result-action").html(res.task_detail.input_action.solution);
                        $(".TodolistViewApp #FollowUpAndClosedViewApp").show();
                        $(".TodolistViewApp #FollowUpAndClosedViewApp .chat").attr("class","chat animated bounceInRight");
                        $(".TodolistViewApp #BackToMain").css("display","initial");
                        $(".TodolistViewApp .backToMoreSystem").hide();
                        $(".TodolistViewApp .app").hide();
                    });
                });

                $(".TodolistViewApp").on("click",".btnLeft.step300 button",function(){
                  getDataTaskDetail(currentTaskSid).done(function(res){
                    console.log(res);
                    $(".TodolistViewApp #ActionServiceReportViewApp .messages-content").html(res.task_detail.input_action.solution);
                    $(".TodolistViewApp #ActionServiceReportViewApp").show();
                    $(".TodolistViewApp #ActionServiceReportViewApp .chat").attr("class","chat animated bounceInRight");
                    $(".TodolistViewApp #BackToMain").css("display","initial");
                    $(".TodolistViewApp .backToMoreSystem").hide();
                    $(".TodolistViewApp .app").hide();
                  });
                });
                var currentRowAction = false;
                $(".TodolistViewApp #ActionServiceReportViewApp").on("click",".messages-content div", function(){
                    console.log($(this).data("row"));
                    currentRowAction = $(this).data("row");
                    $(".TodolistViewApp #ActionServiceReportViewApp .message-input").val($(this).text());
                });

                function updateScrollbar() {
                    $("#ActionServiceReportViewApp .messages-content").mCustomScrollbar({
                        axis:"y"
                    });
                    $("#ActionServiceReportViewApp .messages-content").mCustomScrollbar("scrollTo","top",{
                        scrollInertia:1000
                    });
                }
                if(devicePlatform=="iOS"){
                  // $(".TodolistViewApp #ActionServiceReportViewApp").on("click",".message-input", function(){
                  //     $(".page").addClass("kb-active");
                  //     $("body").scrollTop(0);
                  // });
                }
                $(".TodolistViewApp #ActionServiceReportViewApp").on("keydown", function(e) {
                    console.log(e.which);
                    if (e.which == 13) {
                        insertMessage();
                        return false;
                    }
                })
                $(".TodolistViewApp #ActionServiceReportViewApp").on("click",".message-submit", function(){
                   insertMessage();
                   $(".page").removeClass("kb-active");
                });

                function insertMessage(){
                    var action = $(".TodolistViewApp .message-input").val();
                    if(currentRowAction){
                      $(".TodolistViewApp #ActionServiceReportViewApp .messages-content div[data-row=\""+currentRowAction+"\"]").html(action);
                      $(".TodolistViewApp .message-input").val("");
                      currentRowAction = false;
                    }else{
                      console.log(action);
                      $(".TodolistViewApp .message-input").val("");
                      var numberRowAction = $(".TodolistViewApp #ActionServiceReportViewApp .messages-content").find("div").length;
                      $(".TodolistViewApp #ActionServiceReportViewApp .messages-content").append("<div data-row=\""+(numberRowAction+1)+"\">"+action+"</div>");
                    }
                    $(".TodolistViewApp .message-input").focus();
                    $("#ActionServiceReportViewApp .messages-content").scrollTop($(".messages-content").get(0).scrollHeight);

                    $.ajax({
                      type: "POST", url: END_POINT_2+"v1/insert/input_action", data: {task_sid:currentTaskSid,solution:$("#ActionServiceReportViewApp .messages-content").html(),problem:"",recommend:"",email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token")}
                    });
                }
                $(".TodolistViewApp").on("click",".btnRight.step400 button",function(){
                    getDataTaskDetail(currentTaskSid).done(function(res){
                        console.log(res.task_detail);
                        serviceNo = res.task_detail.no_task;
                        var $actionCurrent =  $("<div>");
                        $actionCurrent.html($(res.task_detail.input_action.solution));
                        $(".TodolistViewApp #signature-pad #action_input").html($actionCurrent.html());
                        $(".TodolistViewApp .backToMoreSystem").hide();
                        $(".TodolistViewApp #signature-pad").show();                      
                        $(".TodolistViewApp .app").hide();                 
                        $("head").append("<script src=\"http://vspace.in.th/pages/signaturepad/customerApp/js/signature_pad.js\"><\/script><script src=\"http://vspace.in.th/pages/signaturepad/customerApp/js/appSign.js\"><\/script>");
                    });
                });
                $(".TodolistViewApp").on("click",".btnLeft.step400 button",function(){
                    resendSigned(currentTaskSid).done(function(res){
                      alert("Sent Email");
                      sendTimestampSr("-","-").done(function(){
                          location.reload();
                      });
                    });
                });
                var resendSigned = function(tasks_sid){
                    return $.ajax({
                        url: END_POINT_1+"sendmail/sendEmailAfterClosedToCustomer/"+tasks_sid,
                        type: "POST",
                          data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token")}
                    });
                }

                function addNewProfile() {
                    // var names = ["Lieke", "Christina", "Sanne", "Soraya", "Chanella", "Larissa", "Michelle"][Math.floor(Math.random() * 7)];
                    // var ages = ["19", "22", "18", "27", "21", "18", "24"][Math.floor(Math.random() * 7)]
                    // var photos = ["1", "2", "3", "4", "5", "6", "7"][Math.floor(Math.random() * 7)]
                    var data = ' . json_encode($this->data) . ';
                    allDataForPage = data;
                    console.log(data);
                    sequenceData %=data.length;
                    if(data.length>0){

                        currentTaskSid = data[sequenceData]["tasks_sid"];
                        console.log(currentTaskSid);
                        var genBtn = false;

                        $.each(data[sequenceData]["service_type_list"], function(k,v){
                          console.log(v.task_status);
                          console.log(data[sequenceData]["tasks_log_status"]);
                          if(parseInt(v.task_status)>=parseInt(data[sequenceData]["tasks_log_status"]) && !genBtn){
                            genBtn = true;
                            console.log(v.task_status);
                            $(".TodolistViewApp .subject_type").html(v.subject);

                            $(".TodolistViewApp .footer .btnLeft").attr("class","btnLeft step"+v.task_status);
                            $(".TodolistViewApp .footer .btnRight").attr("class","btnRight step"+v.task_status);

                            if(v.task_status=="300"){
                              $(".TodolistViewApp .footer .btnLeft button").html("Action");
                              $(".TodolistViewApp .footer .btnRight button").html("Follow Up | Closed");
                            }else{
                              $(".TodolistViewApp .footer .btnLeft button").html(v.btn_left);
                              
                              $(".TodolistViewApp .footer .btnRight button").html(v.btn_right);
                            }
                            // $(".TodolistViewApp .discription-step").html(v.descript_status);
                            currentTaskStatus = v.task_status;
                          }
                        });

                        $("div.content-object").prepend("<div class=\"photo\" id=\"photo\" ><span class=\"meta\"><p>Swipe Right or Left to skip</p>" + "<span class=\"moments\">"+(sequenceData+1)+"/"+data.length+"</span></span>" + "<div class=\"content-data\"><div style=\"margin-bottom:10px;\">Step <span class=\"subject_type pull-right\">"+data[sequenceData]["subject_type"]+"</span></div><div><img src="+data[sequenceData]["pic"]+" class=\"media-object\" /></div><div style=\"clear:both;\">Service No. <span class=\"pull-right text-right\">" +data[sequenceData]["no_task"]+ "</span></div><div class=\"clear\">Subject <span class=\"pull-right\">" +data[sequenceData]["name"]+ "</span></div><hr/><div class=\"appointment_change\" data-tasks-sid=\""+data[sequenceData]["tasks_sid"]+"\"><span class=\"pull-right\"><i style=\"font-size: 0.8em;\" class=\"fa fa-fw fa-chevron-right\"></i></span><div class=\"clear\">Appointment<span class=\"pull-right\">"+data[sequenceData]["appointment"]+"</span></div><div class=\"clear\">Expect Finish <span class=\"pull-right\">"+data[sequenceData]["expect_finish"]+"</span></div></div><hr/><div class=\"end_user_signed\" data-tasks-sid=\""+data[sequenceData]["tasks_sid"]+"\"><div>End User <span class=\"pull-right\"><i style=\"font-size: 0.8em;\" class=\"fa fa-fw fa-chevron-right\"></i></span></div><div>Name <span class=\"pull-right end_user_contact_name_service_report\">" +data[sequenceData]["end_user_contact_name_service_report"]+ "</span></div><div>Email <span class=\"pull-right end_user_email_service_report\">" +data[sequenceData]["end_user_email_service_report"]+ "</span></div><div>Mobile <span class=\"pull-right end_user_mobile_service_report\">" +data[sequenceData]["end_user_mobile_service_report"]+ "</span></div><div>Phone <span class=\"pull-right end_user_phone_service_report\">" +data[sequenceData]["end_user_phone_service_report"]+ "</span></div><div>Company <span class=\"pull-right end_user_company_name_service_report\">" +data[sequenceData]["end_user_company_name_service_report"]+ "</span></div></div><hr/><div>Case No. <span class=\"pull-right\">"+data[sequenceData]["no_ticket"]+"</span></div><div class=\"clear\">Subject <span class=\"pull-right\">"+data[sequenceData]["ticket_subject"]+"</span></div><div class=\"clear\">End User <span class=\"pull-right\">"+data[sequenceData]["end_user_company_name"]+"</span></div><div class=\"clear\">Address <span class=\"pull-right\">"+data[sequenceData]["end_user_site"]+"</span></div><hr/></div></div>");
                        swipe();
                    }else{
                        $("div.content-object").prepend("<div class=\"photo\" id=\"photo\" ><div class=\"content-data\"><div style=\"text-align:center;\">No Data To Do List</div></div></div>");
                        $(".UnsignedServiceViewApp .footer").css("display","none");
                    }
                }

                var sendTimestampFollowUpSr = function(taxi_fare_,taxi_fare_stang_){
                  return $.ajax({
                    type: "POST",url:END_POINT_2+"v1/timestampSr", data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),tasks_sid:currentTaskSid,task_status:currentTaskStatus,taxi_fare:taxi_fare_,taxi_fare_stang:taxi_fare_stang_,lat:lat,lng:lng,follow_up:1},
                    success:function(res){
                      console.log(res);
                    }
                  });
                }

                var sendTimestampSr = function(taxi_fare_,taxi_fare_stang_){
                  return $.ajax({
                    type: "POST",url:END_POINT_2+"v1/timestampSr", data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),tasks_sid:currentTaskSid,task_status:currentTaskStatus,taxi_fare:taxi_fare_,taxi_fare_stang:taxi_fare_stang_,lat:lat,lng:lng},
                    success:function(res){
                      console.log(res);
                    }
                  });
                }
                $(".TodolistViewApp").on("click",".end_user_signed",function(){
                    var tasks_sid = $(this).data("tasks-sid");
                    getDataTaskDetail(tasks_sid).done(function(res){
                        $("#FormEndUserView #company").val(res.task_detail.end_user_company_name_service_report);
                        $("#FormEndUserView #name").val(res.task_detail.end_user_contact_name_service_report);
                        $("#FormEndUserView #email").val(res.task_detail.end_user_email_service_report);
                        $("#FormEndUserView #mobile").val(res.task_detail.end_user_mobile_service_report);
                        $("#FormEndUserView #phone").val(res.task_detail.end_user_phone_service_report);
                    });
                    $("#FormEndUserView").show();
                    $(".TodolistViewApp #BackToMain").css("display","block");
                    $(".TodolistViewApp .backToMoreSystem").hide();                  
                    $(".app").attr("class","app bounceOutLeft animated");
                    $(".app").hide();
                    $("#FormEndUserView").addClass("bounceInRight animated");
                });

                $(".TodolistViewApp").on("click","#BackToMain2",function(){
                  backToMainUnsigned();
                });
                $(".TodolistViewApp").on("click","#BackToMain",function(){
                  backToMainUnsigned();
                });
                var backToMainUnsigned = function(){
                    $("#FormAppointmentSrView").hide();
                    $("#FormTaxiView").hide();
                    $(".TodolistViewApp #BackToMain").hide();
                    $(".TodolistViewApp #FollowUpAppView").hide();
                    $("#FormEndUserView").hide();
                    $("#FollowUpAndClosedViewApp").hide();
                    $("#ActionServiceReportViewApp").hide();
                    $(".TodolistViewApp .app").show();
                    $(".TodolistViewApp #before_signature").hide();
                    $(".TodolistViewApp #signature-pad").hide();
                    $(".TodolistViewApp .backToMoreSystem").show();
                    $(".TodolistViewApp .app").attr("class","app");
                    $(".TodolistViewApp .app").show();
                    $(".TodolistViewApp .app").addClass("bounceInLeft animated");
                    $(".TodolistViewApp .footer").attr("class","footer animated bounceInLeft");
                }
                $(".TodolistViewApp").on("click","#btnChangeEnduser",function(){
                    var enduser = {name:"",email:"",phone:"",mobile:"",company:""};
                    enduser.company = $("#FormEndUserView #company").val();
                    enduser.name = $("#FormEndUserView #name").val();
                    enduser.email = $("#FormEndUserView #email").val();
                    enduser.mobile = $("#FormEndUserView #mobile").val();
                    enduser.phone = $("#FormEndUserView #phone").val();

                    $(".end_user_signed .end_user_company_name_service_report").html(enduser.company);
                    $(".end_user_contact_name_service_report").html(enduser.name);
                    $(".end_user_email_service_report").html(enduser.email);
                    $(".end_user_mobile_service_report").html(enduser.mobile);
                    $(".end_user_phone_service_report").html(enduser.phone);
                    changeEndUser(enduser).done(function(res){
                        console.log(res);
                        backToMainUnsigned();
                    });
                });

                var $calendar = $(".TodolistViewApp #FormAppointmentSrView");
                $(".TodolistViewApp").on("click","#FollowUpServiceReport",function(){
                    mylib.calendar.currentDate = new Date();
                    mylib.calendar.expect = {
                        hours: 2,
                        minutes: 0
                    };
                    $calendar = $("#FollowUpAppView");
                    $(".TodolistViewApp #FollowUpAndClosedViewApp").hide();
                    mylib.calendar.setDateLabels();
                    $("#FollowUpAppView").show();
                    $(".TodolistViewApp #BackToMain").css("display","block");
                    $(".TodolistViewApp .backToMoreSystem").hide();
                    $(".app").attr("class","app bounceOutLeft animated");
                    $(".app").hide();
                    $("#FollowUpAppView").addClass("bounceInRight animated");
                });
                $(".TodolistViewApp").on("click","#btnChangeAppointment", function(){
                    var day = $(".TodolistViewApp #FormAppointmentSrView #day").text();
                    var month = $(".TodolistViewApp #FormAppointmentSrView #month").text();
                    var year = $(".TodolistViewApp #FormAppointmentSrView #year").text();
                    var hour = $(".TodolistViewApp #FormAppointmentSrView #hour").text();
                    var minutes = $(".TodolistViewApp #FormAppointmentSrView #minutes").text();
                    var hourExpect = $(".TodolistViewApp #FormAppointmentSrView #hourExpect").text();
                    var minutesExpect = $(".TodolistViewApp #FormAppointmentSrView #minutesExpect").text();
                    var appointment_change = year+"-"+month+"-"+day+" "+hour+":"+minutes;
                    var expect_duration = hourExpect+":"+minutesExpect;
                    changeAppointment(appointment_change,expect_duration).done(function(res){
                      console.log(res);
                      alert("Done");
                      window.location = "#todolistview/right/"+(parseInt(currentTaskSid)+100000+parseInt(res.rand));
                    });
                });

                $(".TodolistViewApp").on("click","#btnFollowUpDone", function(){
                    var day = $(".TodolistViewApp #FollowUpAppView #day").text();
                    var month = $(".TodolistViewApp #FollowUpAppView #month").text();
                    var year = $(".TodolistViewApp #FollowUpAppView #year").text();
                    var hour = $(".TodolistViewApp #FollowUpAppView #hour").text();
                    var minutes = $(".TodolistViewApp #FollowUpAppView #minutes").text();
                    var hourExpect = $(".TodolistViewApp #FollowUpAppView #hourExpect").text();
                    var minutesExpect = $(".TodolistViewApp #FollowUpAppView #minutesExpect").text();
                    var appointment_change = year+"-"+month+"-"+day+" "+hour+":"+minutes;
                    var expect_duration = hourExpect+":"+minutesExpect;
                    sendTimestampFollowUpSr("-","-").done(function(res){
                      console.log(res);
                      alert("Done");
                      window.location = "#todolistview/right/"+(parseInt(currentTaskSid)+100000+parseInt(res.rand));
                    });
                });
                var changeAppointment = function(appointment_change,expect_duration){
                  return $.ajax({
                    type: "POST", url: END_POINT_2+"v1/servicereport/EditTimeAppointmentMobile1",
                    data: {datetime:appointment_change,task_sid:currentTaskSid,email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),expect_duration:expect_duration}
                  });
                }
                var changeEndUser = function(enduser){
                  return $.ajax({
                      type: "POST", url: END_POINT_2+"v1/enduser/ChangeEndUser", data: {tasks_sid:currentTaskSid,email: localStorage.getItem("case_email"),token:localStorage.getItem("case_token"), data:enduser}
                  });
                }

                var getDataTaskDetail = function(tasks_sid){
                  return $.ajax({
                      type: "POST", url: END_POINT_2+"v1/task_detail/"+tasks_sid, data: {email: localStorage.getItem("case_email"),token:localStorage.getItem("case_token")}
                  });
                }
                 $(".TodolistViewApp").on("click",".appointment_change", function(){
                    $calendar = $("#FormAppointmentSrView");
                    var tasks_sid = $(this).data("tasks-sid");
                    getDataTaskDetail(tasks_sid).done(function(res){
                      console.log(res);
                      $(".TodolistViewApp #FormAppointmentSrView .card .appointment").html(res.task_detail.appointment);
                      expect_duration = res.task_detail.expect_duration.split(":");
                      mylib.calendar.expect = {hours:parseInt(expect_duration[0]), minutes:parseInt(expect_duration[1])};
                      mylib.calendar.setDateLabels();
                    });
                    $("#FormAppointmentSrView").show();
                    $(".TodolistViewApp #BackToMain").css("display","block");
                    $(".TodolistViewApp .backToMoreSystem").hide();
                    $(".app").attr("class","app bounceOutLeft animated");
                    $(".app").hide();
                    $("#FormAppointmentSrView").addClass("bounceInRight animated");
                });
                var mylib = {};
                mylib.calendar = {};
                mylib.calendar.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                mylib.calendar.weekdays = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"];
                mylib.calendar.currentDate = new Date();
                mylib.calendar.expect = {
                    hours: 2,
                    minutes: 0
                };
                mylib.calendar.setDateLabels = function() {
                  console.log($calendar);
                  var roundedTime = this.getRoundedTime();
                  this.currentDate.setHours(roundedTime.hours);
                  this.currentDate.setMinutes(roundedTime.minutes);
                  this.currentDate.setSeconds(0);
                  var d = this.currentDate;
                  if (d.getDate() < 10) {
                      $calendar.find("#day").html("0" + d.getDate());
                  } else {
                      $calendar.find("#day").html(d.getDate());
                  }
                  if ((d.getMonth() + 1) < 10) {
                      $calendar.find("#month").html("0" + (d.getMonth() + 1));
                  } else {
                      $calendar.find("#month").html((d.getMonth() + 1));
                  }
                  $calendar.find("#year").html(d.getFullYear());
                  $calendar.find("#hour").html(roundedTime.hours);
                  $calendar.find("#minutes").html(roundedTime.minutes);
                  $calendar.find("#ampm").html(roundedTime.ampm);
                  $calendar.find("#title-time").html(roundedTime.hours + ":" + roundedTime.minutes);
                  $calendar.find("#title").html(this.weekdays[d.getDay()] + ", " + this.months[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear());
                  $calendar.find("#hourExpect").html(mylib.calendar.expect.hours);
                  $calendar.find("#minutesExpect").html(mylib.calendar.expect.minutes);
              }
              mylib.calendar.getRoundedTime = function() {
                  var retObj = {};
                  retObj.hours = this.currentDate.getHours();
                  var minutes = this.currentDate.getMinutes();
                  if (minutes < 15) {
                      retObj.minutes = 0;
                  } else if (minutes == 15) {
                      retObj.minutes = 15;
                  } else if (minutes <= 30) {
                      retObj.minutes = 30;
                  } else if (minutes <= 45) {
                      retObj.minutes = 45;
                  } else {
                      retObj.minutes = 0;
                      retObj.hours = retObj.hours + 1;
                  }
                  retObj.ampm = retObj.hours >= 12 ? "PM" : "AM";
                  if (retObj.hours > 24) retObj.hours = retObj.hours - 24;
                  if (retObj.hours < 10) retObj.hours = "0" + retObj.hours;
                  if (retObj.minutes < 10) retObj.minutes = "0" + retObj.minutes;
                  return retObj;
              }
              mylib.calendar.setDateLabels();
              $(".TodolistViewApp").on("click", ".addDay", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setDate(oldDate.getDate() + 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".addMonth", function() {
                  console.log("addMonth");
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setMonth(oldDate.getMonth() + 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".addYear", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setFullYear(oldDate.getFullYear() + 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subDay", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setDate(oldDate.getDate() - 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subMonth", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setMonth(oldDate.getMonth() - 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subYear", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setFullYear(oldDate.getFullYear() - 1);
                  mylib.calendar.setDateLabels();
              });
              // $(".TodolistViewApp").on("click", ".toggleAMPM", toggleAMPM);
              $(".TodolistViewApp").on("click", ".addHour", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setHours(oldDate.getHours() + 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".addMinutes", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setMinutes(oldDate.getMinutes() + 15);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subHour", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setHours(oldDate.getHours() - 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subMinutes", function() {
                  var oldDate = mylib.calendar.currentDate;
                  mylib.calendar.currentDate.setMinutes(oldDate.getMinutes() - 15);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".addHourExpect", function() {
                  var oldDate = mylib.calendar.expect.hours;
                  mylib.calendar.expect.hours = (oldDate + 1);
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".addMinutesExpect", function() {
                  var oldDate = mylib.calendar.expect.minutes;
                  mylib.calendar.expect.minutes = (oldDate + 15) % 60;
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subHourExpect", function() {
                  var oldDate = mylib.calendar.expect.hours;
                  mylib.calendar.expect.hours = (oldDate - 1);
                  if (mylib.calendar.expect.hours < 1) {
                      mylib.calendar.expect.hours = 1;
                  }
                  mylib.calendar.setDateLabels();
              });
              $(".TodolistViewApp").on("click", ".subMinutesExpect", function() {
                  var oldDate = mylib.calendar.expect.minutes;
                  oldDate = (oldDate - 15);
                  if (oldDate < 0) {
                      oldDate = 0;
                  }
                  mylib.calendar.expect.minutes = oldDate;
                  mylib.calendar.setDateLabels();
              });
              // ARRIVED 
              $(".TodolistViewApp").on("click",".btnRight.step500 button",function(){
                  $("#FormTaxiView").show();
                  $(".TodolistViewApp #BackToMain").css("display","initial");
                  $(".TodolistViewApp #FormTaxiView").attr("class","animated bounceInRight");
                  $(".TodolistViewApp .backToMoreSystem").hide();
                  $(".TodolistViewApp .app").hide();
              });
              // END ARRIVED

              // START THE JOURNEY
              var executeStartTheJourney = function(){
                  taxi_fare = 0;
                  taxi_fare_stang = 0;
                    $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog .mhn-ui-dialog-title").html("Confirm Start The Journey?");                  
                  $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog .mhn-ui-dialog-title").css("margin-bottom","20px");
                  $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog p").html(" ");
                  $(".mhn-ui-dialog-wrap.confirmStartTask").show();       
              }
              // END THE JOURNEY

              // START TASK
              var taxi_fare = 0;
              var taxi_fare_stang = 0;
              $(".TodolistViewApp").on("click","#DoneTaxi",function(){
                  $(".TodolistViewApp .numeral.displayed.hide").remove();
                  $(".TodolistViewApp .decimal.smallhide").remove();
                  taxi_fare = ($(".TodolistViewApp #FormTaxiView #numerals>.displayed.baht").text());
                  taxi_fare_stang = ($(".TodolistViewApp #FormTaxiView #numerals>.displayed.stang").text());
                  if(currentTaskStatus>400){
                    $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog .mhn-ui-dialog-title").html("Confirm Arrived? (Taxi Fare(฿) "+(taxi_fare+"."+taxi_fare_stang)+")");
                  }else{
                    $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog .mhn-ui-dialog-title").html("Confirm Start Service Report? (Taxi Fare(฿) "+(taxi_fare+"."+taxi_fare_stang)+")");
                  }
                  $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog .mhn-ui-dialog-title").css("margin-bottom","20px");
                  $(".mhn-ui-dialog-wrap.confirmStartTask .mhn-ui-dialog p").html(" ");
                  $(".mhn-ui-dialog-wrap.confirmStartTask").show();                 
              });
              $(".mhn-ui-dialog-wrap.confirmStartTask a.mhn-ui-dialog-btn").click(function(){                  
                  if($(this).data("action")=="confirm"){
                    sendTimestampSr(taxi_fare+"."+taxi_fare_stang,taxi_fare_stang).done(function(){
                      location.reload();
                    });
                  }
              });
              //END START TASK

              // CLOSED SERVICE
              $(".TodolistViewApp #FollowUpAndClosedViewApp").on("click","#ClosedServiceReport", function(){
                  $(".mhn-ui-dialog-wrap.confirmClosedService .mhn-ui-dialog .mhn-ui-dialog-title").html("Confirm Close Service?");
                  $(".mhn-ui-dialog-wrap.confirmClosedService .mhn-ui-dialog .mhn-ui-dialog-title").css("margin-bottom","20px");
                  $(".mhn-ui-dialog-wrap.confirmClosedService .mhn-ui-dialog p").html(" ");
                  $(".mhn-ui-dialog-wrap.confirmClosedService").show();
              });
              $(".mhn-ui-dialog-wrap.confirmClosedService a.mhn-ui-dialog-btn").click(function(){
                  console.log($(this));
                  if($(this).data("action")=="confirm"){
                    sendTimestampSr("-","-").done(function(){
                      location.reload();
                    });
                  }
              });
              // END CLOSED SERVICE
            </script>';
        // $html .= MoreSystemView::bottomBar();
        return $html;
    }
}
