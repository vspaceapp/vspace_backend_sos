<?php class OrganizationAppView{
	public static function view($email, $orga, $permission_role){
		$html = '<link rel="stylesheet" href="http://vspace.in.th/production/css3-tree-family/css/style.css" />';
		$html .= '<style>
					.sidebar-open .content-wrapper-properties, .sidebar-collapse .content-wrapper-properties { overflow: auto;}
					.tree {
					    /* width: 1000px; */
					    width: initial;
					    margin: 0 auto;
					    /* overflow: auto; */
					    text-align: center;
					}
					.tree ul ul ul {margin-left:20px;}
					.tree li a {
    					max-width: 135px;
					}
					.tree li a:hover, .tree li a:hover+ul li a {
						background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
					}
					.tree li a:hover+ul li a {
						background: none; color: initial; border: 1px solid #cccccc;
					}
					#permission-user {
						position:absolute;
						width: 100%;
						right:0;
						bottom: 0;
						height:50%;
						background: #222;
						color: #ccc; padding:5px;
						-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.8);
    					box-shadow: 0 0 5px rgba(0, 0, 0, 0.8);
    					overflow-y: auto;
    					overflow-x: hidden;
					}
					.play-user {float:left; width:30%;}
					.play-permission {float:left; width: 70%;}
					.play-permission::before {content:"Permission"; position:absolute; top:0px;}
					.users-list-name {color: #ffffff;}
					.users-list>li {width:20%;}
				</style>
				<div class="object content">
                        <div class="box-header with-border"><h3 class="box-title"><i class="fa fa-fw fa-calendar"></i> Organization</h3></div>
                        <div class="row" id="object-body" style="">
						<div class="tree">';
				
							$html .= '<ul><li>
								<a href="javascript:void(0);" data-role-sid="'.$orga[0]['role_sid'].'">'.$orga[0]['name'].'</a>';
								$is_a_supervisor_role_sid_array = $orga[0]['is_a_supervisor_role_sid_array'];
								$html .= '<ul>';
								foreach ($is_a_supervisor_role_sid_array as $sK => $sV) {
									foreach ($orga as $key => $value) {
										if($sV==$value['sid']){
											// foreach ($value['staff'] as $staffK => $staffV) {
												$html .= '<li><a data-role-sid="'.$value['role_sid'].'" href="javascript:void(0);">'.$value['name'].'</a>';
												if(count($value['is_a_supervisor_role_sid_array'])>0 && $value['is_a_supervisor_role_sid_array'][0] != "0"){
													$html .= '<ul>';
													foreach ($value['is_a_supervisor_role_sid_array'] as $sKK => $sVV) {
														
														foreach ($orga as $kkk => $vvv) {
															if($sVV==$vvv['sid']){
																$html .= '<li><a data-role-sid="'.$vvv['role_sid'].'" href="javascript:void(0);">'.$vvv['name'].'</a>';
																// $html .= '<ul>';
																// foreach ($vvv['staff'] as $staffKK => $staffVV) {
																// 	$html .= '<li><a href="javascript:void(0);">'.$staffVV['name'].'</a></li>';
																// }
																// $html .= '</ul>';
																$html .= '</li>';
															}
														}

													}
													$html .= '</ul>';
												}
												$html .= '</li>';
											// }
										}
									}
								}
								$html .= '</ul>';
								
				$html .= '</ul></div><div style="clear:both;"></div>';
				$html .= '</div></div>';
		// $hmtl .= $email;
				$html .= self::script();
		return $html;
	}

	public static function script(){
		$script = '<script>
			$(".tree").on("click","a",function(){
				servicePermissionOfRoleSid($(this).data("role-sid")).done(function(res){
					console.log(res);
					$(this).addClass("active");
					playPermissionAndUser(res);
				});
			});
			var playPermissionAndUser = function(res){
				$("#permission-user").remove();
				$elm = $("<div>");
				$elm.append("<div id=\"permission-user\"><div class=\"box-tools pull-right\" style=\"margin-right: 10px;\"><button type=\"button\" class=\"btn btn-box-tool\" id=\"closePlayPermissionUser\" data-widget=\"remove\"><i class=\"fa fa-times\"></i></button></div></div>");
				$elm.find("#permission-user").append("<div>"+res.permission.name+"</div>");

				$elm.find("#permission-user").append("<div class=\"play-user\"><ul class=\"users-list clearfix\"></ul></div>");
				$.each(res.user, function(k,v){
					if(v.active==1 && v.picture){
						$elm.find(".play-user .users-list").append("<li><img src=\""+v.picture+"\" ><a class=\"users-list-name\" href=\"javascript:void(0)\">"+v.name+"</a><span class=\"users-list-date\"></span></li>");
					}
				});

				var create_project = "<i class=\"fa fa-fw fa-close\"></i>";
				if(res.permission.project_create_able && res.permission.project_create_able !="0"){
					create_project = "<i class=\"fa fa-fw fa-check\"></i>";
				}
				$elm.find("#permission-user").append("<div class=\"play-permission\"><div>Create Project "+create_project+"</div></div>");	

				var case_create_able = "<i class=\"fa fa-fw fa-close\"></i>";
				if(res.permission.case_create_able && res.permission.case_create_able !="0"){
					case_create_able = "<i class=\"fa fa-fw fa-check\"></i>";
				}
				$elm.find("#permission-user .play-permission").append("<div>Create Case "+case_create_able+"</div>");

				var can_create_service = "";
				if(res.permission.can_create_service=="0"){
					can_create_service = "Not";
				}else if(res.permission.can_create_service=="1"){
					can_create_service = "When is owner case";
				}else if(res.permission.can_create_service=="2"){
					can_create_service = "All Case";
				}
				$elm.find("#permission-user .play-permission").append("<div>Create Service: "+can_create_service+"</div>");		
				$elm.find("#permission-user").append("<div style=\"clear:both;\"></div>");

				// can assign project
				$elmPermission = $("<div>");
				$elmPermission.append("<ul class=\"users-list clearfix\">");
				if(res.permission.permission_project_detail.length>0){
					$.each(res.permission.permission_project_detail, function(k,v){
						$.each(v.staff, function(kk,vv){
							if(vv.picture){$elmPermission.find("ul").append("<li style=\"width:initial;\"><img style=\"width:48px;\" src=\""+vv.picture+"\" /></li>");}
						});
					});
					$elmPermission.append("</ul>");
				}else{
					$elmPermission.html("-");
				}
				$elm.find("#permission-user .play-permission").append("<div style=\"width:50%\"><div>Can Assign Project</div>"+$elmPermission.html()+"</div>");

				$elmPermission = $("<div>");
				$elmPermission.append("<ul class=\"users-list clearfix\">");
				if(res.permission.can_assign_detail.length>0){
					$.each(res.permission.can_assign_detail, function(k,v){
						$.each(v.staff, function(kk,vv){
							if(vv.picture){$elmPermission.find("ul").append("<li style=\"width:initial;\"><img style=\"width:48px;\" src=\""+vv.picture+"\" /></li>");}
						});
					});
					$elmPermission.append("</ul>");
				}else{
					$elmPermission.html("-");
				}
				$elm.find("#permission-user .play-permission").append("<div style=\"width:50%\"><div>Can Assign Case</div>"+$elmPermission.html()+"</div>");

				$elm.find("#permission-user").append("<div style=\"clear:both;\"></div>");

				$(".object.content").append($elm.html());
			}
			$("body").on("click","#closePlayPermissionUser", function(){
				$("#permission-user").remove();
			});
			var servicePermissionOfRoleSid = function(role_sid){
				return $.ajax({
					type: "POST",
					url: END_POINT_2+"v1/role/getPermissionAndUserOfRoleSid",
					data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),role_sid:role_sid},
				});
			}
		</script>';
		return $script;
	}
}