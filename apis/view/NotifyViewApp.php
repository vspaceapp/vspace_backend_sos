<?php
class NotifyViewApp{
	private $email;
  private $data;
  private $tabName;
  private $canAssignTo;
  private $windowHeight;
  private $permission_role;
	public function __construct( $email = ""){
		$this->email  = $email;
	}

	public function templateNotifyViewApp($email,$data, $order_by, $search="",$permission_role, $canAssignTo, $windowHeight){
    $this->data = $data;
    $this->canAssignTo = $canAssignTo;
    $this->windowHeight = $windowHeight;
    $this->permission_role = $permission_role;

		$html = '
            <style>
            	.NotifyViewApp .app {
	            	background-color: initial;
	            }
	            .NotifyViewApp .app div.photo {
	                left:0;right:0;
	                width:90%; background-color:white;
	                height: initial;
	            }
	            .NotifyViewApp .content-object {
	                left:0;
	                right:0;
	                position: absolute;
	                width:100%;overflow-y: auto; overflow-x: hidden;
	                height:'.($windowHeight-190).'px;               
	            }
	            .NotifyViewApp .app span.meta {
	                margin-top: auto;
	                bottom: 0px;
	                position: relative;
	                width: 100%;
	            }
	            .NotifyViewApp .app {
	                padding-top: 0px;
	            }
	            .NotifyViewApp .app .footer {
	                position: absolute;
	                margin: auto;
	                bottom: 30px;
	                left: 0;
	                padding: initial;
	                width: 180px;
	                right: 0;
	            }
	            .NotifyViewApp .content-data {
	                color: #222d32;
	                padding:10px;
	            }
	            .NotifyViewApp .content-data .clear{
	                clear:both;
	            }
	            .NotifyViewApp .content-data .media-object {
	                float:left;
	            }
	            .NotifyViewApp .info, .NotifyViewApp .rate {background-color:#ffffff;}
	            #resendSigned, #signnow {
	                position: relative;
	            }
	            #resendSigned::after{
	                position: absolute; content: "Resend Email";
	                width: 100px;
	                bottom: -25px;
	                left: -20px;
	                font-size: 0.85em;
	            }
	            #signnow::after{
	                position: absolute; content: "Sign Now";
	                width: 100px;
	                bottom: -25px;
	                left: -20px;
	                font-size: 0.85em;
	            }
	            .NotifyViewApp .rate:nth-child(1),.NotifyViewApp .rate:nth-child(2) {
	                  margin-left: 4px;
	                  background: none;
	                  background-position: center center;
	                  background-repeat: no-repeat;
	                  background-color: #ffffff;
	            }
	            .NotifyViewApp .rate:nth-child(3) {
	                  margin-left: -4px;
	                  background: none;
	                  background-size: 25px;
	                  background-position: center;
	                  background-repeat: no-repeat;
	                  background-color: #ffffff;
	            }
	            .NotifyViewApp .meta {
	                color: #aaaaaa;
	                text-align: right;
	                font-size: 0.8em;
	            }
	            .NotifyViewApp .moments {
	                width: initial;
	            }
	            .NotifyViewApp div.photo {
	                box-shadow: none;
	                border-radius: 0px;
	                border: none;
	            }
	            span.meta {
	                display: block;
	                height: 48px;
	                background-color: #FFF;
	                margin-top: 267px;
	                border-radius: 8px 8px 8px 8px;
	                font-size: 18px;
	                box-sizing: border-box;
	                padding: 12px;
	            }
	            .NotifyViewApp .app::before {
	                position: absolute;
	                color: #aaaaaa;
	                left: initial;
	                top: 150px;
	                content: "";
	                -webkit-transform: rotate(30deg);
	                -moz-transform: rotate(30deg);
	                -o-transform: rotate(30deg);
	                -ms-transform: rotate(30deg);
	                transform: rotate(270deg);
	                left: -70px;
	            }
              	.NotifyViewApp .app {
                	background-color: initial;
              	}
              	.NotifyViewApp .app div.photo {
                	left:0;right:0;
              	}
              	.NotifyViewApp .app .footer {
                	position: absolute;
                	margin: auto;
                	bottom: 60px;
                	left: 0;
                  	padding: initial;
                  	width: 180px;
                  	right: 0;
              	}
              	.NotifyViewApp .info, .NotifyViewApp .rate {background-color:#ffffff;}
            </style>
            <div class="NotifyViewApp">
                <div class="mhn-ui-app-title-head animated bounceInRight" style="text-align:center;">
                	<a class="icon icon-left-nav pull-left" href="#MoreSystem/'.rand(0,10000).'"></a>
                	<span class="fa fa-bell-o"></span> Notify
                </div>
                <div class="mhn-ui-row mhn-ui-apps animated bounceInRight">
                    <div class="app" style="width:100%; height:'.($windowHeight-200).'px;">
                      <div class="content">
                      	<ul class="table-view">';

                      	foreach ($this->data as $key => $value) {
                      		$html .= $value['name'];
                      	}
    $html .= '          </ul>
                      </div>
                  </div>
                </div>
            </div>
            <script>
               	setInterval(function() {
                    var seen_notification_sid = $(".table-view-cell.notification").attr("data-notification-sid");
                    if (seen_notification_sid) {
                        console.log(seen_notification_sid);
                        localStorage.setItem("seen_notification_sid", seen_notification_sid);
                    }
                }, 1000);
            </script>';
    $html .= MoreSystemView::bottomBar();
		return $html;
	}
}
?>