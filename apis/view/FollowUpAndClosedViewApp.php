<?php class FollowUpAndClosedViewApp {

	public static function view(){
		$html = '
				<link href="http://vspace.in.th/production/messaging/css/style.css" rel="stylesheet" />
				<style>
					#FollowUpAndClosedViewApp {display:none;}
					.message-box {
						position: absolute;
    					bottom: 0;
    					z-index: 1000;
					}
					.TodolistViewApp #FollowUpAndClosedViewApp .messages .messages-content {
						height: calc(101% - 160px);
					}
					#result-action {
						background: #FFF;color: #222; padding:5px; height: 100%;
					}
				</style>';
		$html .= '<div id="FollowUpAndClosedViewApp">
						<div class="chat" style="">
						  <div class="messages">
						    <div class="messages-content" style="background-color: #FFF;">
						    	<div style="color: #000000;">Action: </div>
						    	<div id="result-action" style=""></div>
						    </div>
						    <div class="" style="bottom: 10px;position: absolute;width: 100%;">
						    	<ul class="table-view">
						    		<li class="table-view-divider"><div><small>ตรวจสอบ Action ก่อน Follow Up / Close Job</small></div></li>
						    		<li class="table-view-cell" style="background-color:#f39c12;">
						    			<a class="navigate-right" href="javascript:void(0);" id="FollowUpServiceReport">
						    				<div>
						    					<p>Follow Up</p>
						    					<br/><div>มีนัดเข้า Service ในเรื่องเดียวกันนี้ ครั้งต่อไป</div>
						    				</div>
						    			</a>
						    		</li>
						    		<li class="table-view-cell" style="background-color:green;">
						    			<a class="navigate-right" id="ClosedServiceReport" href="javascript:void(0);">
						    				<div>
						    					<p>Close Job</p>
						    					<br/><div>แก้ปัญหาในเรื่องนี้สำเร็จแล้ว</div>
						    				</div>
						    			</a>
						    		</li>
						    	</ul>
						    </div>
						  </div>
						</div>
						<!--<div class="message-box">
						    <input type="text" class="message-input" placeholder="Type action..." />
						    <button type="submit" class="message-submit">Send</button>
						</div>-->
				</div>';
		return $html;
	}
}