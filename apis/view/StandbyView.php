<?php
class StandbyView {

	public static function toolbar($permission_role = array()){
		$html = '';
		$html .= '<div class="box-tools pull-right">
					<!--<button type="button" id="openCalendar" class="btn btn-primary- btn-sm daterange ">
                  		<i class="fa fa-fw fa-calendar-times-o"></i>
                  	</button>
                    <button type="button" id="toggleImage" class="btn btn-primary- btn-sm daterange ">
                        <i class="fa fa-fw fa-eye"></i>
                    </button>-->
                    <div class="btn-group">
			        		<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			            		<i class="fa fa-fw fa-eye"></i>
			            	</button>
			                <ul class="dropdown-menu pull-right" role="menu">
			                    <li><a href="javascript:void(0);" id="Toggle7x24">Show/Hide 7x24</a></li>
			                    <!--<li class="divider"></li>
			                    <li><a href="javascript:void(0);">Standby Buddy 1</a></li>-->
			                </ul>
			        </div>';

		if($permission_role['generate_7x24_able']){
        	$html .= '	<div class="btn-group">
			        		<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			            	<i class="fa fa-fw fa-gear"></i></button>
			                <ul class="dropdown-menu pull-right" role="menu">
			                    <li><a href="javascript:void(0);" id="Generate7x24">Generate 7x24</a></li>
			                    <!--<li class="divider"></li>
			                    <li><a href="javascript:void(0);">Standby Buddy 1</a></li>-->
			                </ul>
			        </div>';
		}
		$html .= '</div>';
		return $html;
	}
}