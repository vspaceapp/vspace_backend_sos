<?php
class CalendarViewApp{
    private $email;
  private $data;
  private $tabName;
  private $canAssignTo;
  private $windowHeight;
  private $permission_role;
  private $month;
    public function __construct( $email = ""){
        $this->email  = $email;
    }

    public function templateCalendarViewApp($email,$data, $month, $search="",$permission_role, $canAssignTo, $windowHeight, $addMonth){
    $this->data = $data;
    $this->month = $month;
    $this->canAssignTo = $canAssignTo;
    $this->windowHeight = $windowHeight;
    $this->permission_role = $permission_role;
  
        $html = '<link rel="stylesheet" href="http://vspace.in.th/production/calendar-flip-animation/css/style.css" /> 
            <style>
              .CalendarViewApp {
                background: rgb(43, 68, 80);
                position: absolute;
                left: 0px; right:0; top:0; bottom: 0px;
              }
              .CalendarViewApp .app {
                background-color: initial;
              }
              .CalendarViewApp .app div.photo {
                left:0;right:0;
                width:90%;
                //height:'.($windowHeight-220).'px;
              }
              .CalendarViewApp .app span.meta {
                margin-top:'.($windowHeight-285).'px;
              }
              .CalendarViewApp .app .footer {
                  position: absolute;
                  margin: auto;
                  bottom: 60px;
                  left: 0;
                  padding: initial;
                  width: 180px;
                  right: 0;
              }
              .CalendarViewApp .info, .CalendarViewApp .rate {background-color:#ffffff;}
              .CalendarViewApp .weeks span {
                  padding: 2%;
              }
              .CalendarViewApp .calendar {
                  background: #2b4450;
                  border-radius: 4px;
                  box-shadow: none;
                  height: '.$windowHeight.'px;
                  perspective: 1000;
                  transition: .9s;
                  transform-style: preserve-3d;
                  width: 100%;
              }
              .CalendarViewApp .week-days {
                  color: #dfebed;
                  display: flex;
                  justify-content: space-between;
                  font-weight: 600;
                  padding: 30px 2%;
              }
              .CalendarViewApp .weeks {
                  color: #fff;
                  display: flex;
                  flex-direction: column;
                  padding: 0 2%;
              }
              .v-hidden {
                visibility: hidden;
              }
              span .bussy {
                  content: "•";
                  color: blue;
                  font-size: 1.4em;
                  position: absolute;
                  right: 0px;
                  top: -4px;
                  width: 10px;
                  height: 10px;
                  border-radius: 50px;
                  border: 1px solid blue;
                  background-color: blue;
              }

              span .consult {
                  content: "•";
                  color: orange;
                  font-size: 1.4em;
                  position: absolute;
                  right:30px;
                  top: -4px;
                  width: 10px;
                  height: 10px;
                  border-radius: 50px;
                  border: 1px solid orange;
                  background-color: orange;
              }
              span .standby {
                  content: "•";
                  color: green;
                  font-size: 1.4em;
                  position: absolute;
                  right: 10px;
                  top: -4px;
                  width: 10px;
                  height: 10px;
                  border-radius: 50px;
                  border: 1px solid green;
                  background-color: green;
              }
              span .task {
                  content: "•";
                  color: #f78536;
                  font-size: 1.4em;
                  position: absolute;
                  right: 20px;
                  top: -4px;
                  width: 10px;
                  height: 10px;
                  border-radius: 50px;
                  border: 1px solid #f78536;
                  background-color: #f78536;
              }
              .weeks div span {
                position: relative;
              }
              .CalendarViewApp .actions {
                  bottom: 0;
                  border-top: 1px solid rgba(73, 114, 133, .6);
                  /* display: flex; */
                  /* justify-content: space-between; */
                  position: absolute;
                  width: 100%;
                  height: 20%;
                  bottom: 10%;
                  left: 0;
                  right: 0;
              }
              .CalendarViewApp .info {
                overflow-y: scroll;
                height: 70%;
                overflow-x: hidden;
              }
              .info div:not(.observations) {
        			    margin-bottom: 10px;
        			}
            </style>
            <div class="CalendarViewApp animated bounceInRight">
                
                <div class=" animated bounceInRight">
                    <div class="" style="width:100%;">
                      <div class="content-object">
                           <div class="">
                              <div class="calendar">
                                <div class="front animated bounceInLeft">
                                  <div class="current-date" data-value="'.$this->month[0].'">
                                    <a class="icon icon-left-nav pull-left" id="prevMonth" href="javascript:void(0);"></a>
                                    <h1>'.$this->month[1].'</h1> 
                                    <a class="icon icon-right-nav pull-right" id="nextMonth" href="javascript:void(0);"></a>
                                  </div>

                                  <div class="current-month">
                                    <ul class="week-days">
                                      <li>MON</li>
                                      <li>TUE</li>
                                      <li>WED</li>
                                      <li>THU</li>
                                      <li>FRI</li>
                                      <li>SAT</li>
                                      <li>SUN</li>
                                    </ul>

                                    <div class="weeks">
                                      <div>';
                  foreach ($this->data as $key => $value) {

                      $event = ((count($value['task'])>0)?'<div class="task"></div>':'');
                      $bussy = ((count($value['leaveAndBussyList']['leave'])>0)?'<div class="bussy"></div>':'');
                      $standby = ((count($value['standby7x24'])>0)?'<div class="standby"></div>':'');

                      $bussy .= ((count($value['dataConsultAce'])>0)?'<div class="consult"></div>':'');


                      if($key==0){
                        if($value['date_w']=="Mon"){
                          $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span>';
                        } else if($value['date_w']=="Tue"){
                          $html .= '<span class="v-hidden">00</span><span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span>';
                        } else if($value['date_w']=="Wed"){
                          $html .= '<span class="v-hidden">00</span><span class="v-hidden">00</span><span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span>';
                        } else if($value['date_w']=="Thu"){
                          $html .= '<span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span>';
                        } else if($value['date_w']=="Fri"){
                          $html .= '<span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span>';
                        } else if($value['date_w']=="Sat"){
                          $html .= '<span class="v-hidden"></span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span>';
                        } else if($value['date_w']=="Sun"){
                          $html .= '<span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span></div>';
                        }
                      }else if(count($this->data)==($key+1)){
                          if($value['date_w']=="Mon"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span></div>';
                          }else if($value['date_w']=="Tue"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span></div>';
                          }else if($value['date_w']=="Wed"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span></div>';
                          }else if($value['date_w']=="Thu"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span><span class="v-hidden">00</span><span class="v-hidden">00</span><span class="v-hidden">00</span></div>';
                          }else if($value['date_w']=="Fri"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span><span class="v-hidden">00</span><span class="v-hidden">00</span></div>';
                          }else if($value['date_w']=="Sat"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span><span class="v-hidden">00</span></div>';
                          }else if($value['date_w']=="Sun"){
                              $html .= '<span data-date="'.$value['date_Ymd'].'" class="">'.$standby.$event.$bussy.$value['d'].'</span></div>';
                          }
                      }else{
                        if($value['date_w']=="Mon"){
                            $html .= '<div>';
                        }
                        $html .= '<span class="" data-date="'.$value['date_Ymd'].'">'.$standby.$event.$bussy.$value['d'].'</span>';

                        if($value['date_w']=="Sun"){
                            $html .= '</div>';
                        }
                      }
                      
                  } 
                  
                  $html .=    '     </div>
                                  </div>
                                </div>

                                <div class="back animated bounceInRight" style="display:none;">
                                  <!--<input placeholder="Whats the event?">-->
                                  <div class="info">
                                    <div class="date">
                                      <p class="info-date">
                                      Date: <span></span>
                                      </p>
                                      <p class="info-time">
                                        Time: <span>6:35 PM</span>
                                      </p>
                                    </div>
                                    <div class="address">
                                      <p>
                                        Address: <span></span>
                                      </p>
                                    </div>
                                    <div class="observations">
                                      <p>
                                        Observations: <span></span>
                                      </p>
                                    </div>
                                  </div>

                                  <div class="actions">
                                    <button class="save" style="visibility:hidden;">
                                      Save <i class="ion-checkmark"></i>
                                    </button>
                                    <button class="dismiss">
                                      Closed <i class="ion-android-close"></i>
                                    </button>
                                  </div>
                                </div>

                              </div>
                            </div>
                      </div>
                  </div>
                </div>
            </div>
            <script>
              var dataForMonth = ('.((json_encode($this->data))).');
              var app = {
                settings: {
                  container: $(".CalendarViewApp .calendar"),
                  calendar: $(".CalendarViewApp .front"),
                  days: $(".CalendarViewApp .weeks span"),
                  form: $(".CalendarViewApp .back"),
                  input: $(".CalendarViewApp .back input"),
                  buttons: $(".CalendarViewApp .back button")
                },

                init: function() {
                  instance = this;
                  settings = this.settings;
                  this.bindUIActions();
                },

                swap: function(currentSide, desiredSide) {
                  //settings.container.toggleClass("flip");
                  //settings.calendar.hide();
                  //settings.form.show();
                  currentSide.fadeOut();
                  currentSide.hide();

                  //settings.container.toggleClass("flip");
                  desiredSide.show();
                  
                },
             
                bindUIActions: function() {
                  settings.days.on("click", function(){
                    console.log($(this).data("date"));
                    var needToViewDate = $(this).data("date");
                    $.each(dataForMonth,function(k,v){

                      if(v.date_Ymd==needToViewDate){

                        console.log(v);
                        $actionInfo = $("<div>");
                        $.each(v.task,function(kk,vv){
                          $actionInfo.append("<div class=\"\"><p class=\"info-time\">"+vv.service_type_name+" "+vv.appointment_datetime_df+"</p><p class=\"info-date\">"+vv.end_user+" "+vv.subject+"</p><div class=\"address\"><p>"+vv.no_task+" "+vv.no_ticket+"</p><p>"+vv.end_user_site+"</p></div>");
                        });
                        $.each(v.standby7x24,function(kk,vv){
                            $actionInfo.append("<div class=\"\"><p class=\"info-date\">Standby <span>"+vv.time_start+" - "+vv.time_end+"</span></p></div>");
                        });
                        
                        if(v.dataConsultAce != ""){
                           $actionInfo.append("<div class=\"\"><p class=\"info-date\">Consult ACE <span>08:00 - 17:00</span></p></div>");
                        }
                        $.each(v.leaveAndBussyList.leave,function(kk,vv){
                            $actionInfo.append("<div class=\"\"><p class=\"info-date\">"+vv.description+" <span>"+vv.busy_start_time+" - "+vv.busy_end_time+"</span></p></div>");
                        });
                        $(".CalendarViewApp .back .info").html($actionInfo.html());
                      }
                    });
                    instance.swap(settings.calendar, settings.form);
                    //settings.input.focus();
                  });

                  settings.buttons.on("click", function(){
                    instance.swap(settings.form, settings.calendar);
                  });
                }
              }
            var addMonth = parseFloat('.$addMonth.');

            app.init();
            $(".CalendarViewApp").on("click","#prevMonth",function(){
                var month = $(".current-date").data("value");
                addMonth--;
                $(".CalendarViewApp").addClass("animated bounceOutLeft");
                loadTemplateCalendarViewApp("prev");
            });
            $(".CalendarViewApp").on("click","#nextMonth",function(){
                var month = $(".current-date").data("value");
                addMonth++; 
                $(".CalendarViewApp").addClass("animated bounceOutLeft");
                loadTemplateCalendarViewApp("next");
            });
            var loadTemplateCalendarViewApp = function(slideViewFrom){
              loadTemplateCalendarServer().done(function(res){
                console.log(res);
                $(".mhn-ui-wrap .page-home").html(res.template);
                if(slideViewFrom=="prev"){
                  $(".CalendarViewApp").addClass(" animated bounceInRight");
                }else{
                  $(".CalendarViewApp").addClass(" animated bounceInRight");
                }
              });
            }

            var loadTemplateCalendarServer = function() {
                return $.ajax({
                    type: "POST",
                    url: END_POINT_2 + "v1/moresystem/loadTemplateCalendarViewApp",
                    data: {
                        email: localStorage.getItem("case_email"),
                        token: localStorage.getItem("case_token"),
                        search: "",
                        order_by: "",
                        addMonth:addMonth,
                        windowHeight: windowHeight
                    },
                    success: function(res) {
                        console.log(res);
                    }
                });
            }
            </script>';
    $html .= MoreSystemView::bottomBar();
        return $html;
    }
}
?>