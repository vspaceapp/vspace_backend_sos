<?php class PlanningServiceView{
	
	private $email;	
	private $dataDate;
    private $viewDate;
	private $month = array('January','February','March','April','May','June','July','August','September','October','November','December');
    private $windowHeight;
	public function __construct($email=""){
		$this->email = $email;
	}

	public function templatePlanningService($email, $viewDate="",$data= array(),$windowHeight){
        $this->windowHeight = $windowHeight;
		$html = '';
        $date_explode = explode("-", $viewDate);
        $day_selected = $date_explode[0];
        $month_selected = $date_explode[1];
        $year_selected = $date_explode[2];
		$html .= '<div class="object content">
                        <div class="box-header with-border">
                          <h3 class="box-title"><i class="fa fa-fw fa-calendar"></i> Planning</h3>
                          <div style="clear:both;">
                              <ul class="year months">
                                <li><a href="javascript:void(0);" title="Jan" data-value="2016" class="'.(($year_selected=="2016")?'selected':'').'">2016</a></li>
                                <li><a href="javascript:void(0);" title="Feb" data-value="2017" class="'.(($year_selected=="2017")?'selected':'').'">2017</a></li>
                              </ul>
                          </div>
                          <div style="clear:both;">
                              <ul class="month months">
                                <li><a href="javascript:void(0);" title="Jan" data-value="01" class="'.(($month_selected=="01")?'selected':'').'">Jan</a></li>
                                <li><a href="javascript:void(0);" title="Feb" data-value="02" class="'.(($month_selected=="02")?'selected':'').'">Feb</a></li>
                                <li><a href="javascript:void(0);" title="Mar" data-value="03" class="'.(($month_selected=="03")?'selected':'').'">Mar</a></li>
                                <li><a href="javascript:void(0);" title="Apr" data-value="04" class="'.(($month_selected=="04")?'selected':'').'">Apr</a></li>
                                <li><a href="javascript:void(0);" title="May" data-value="05" class="'.(($month_selected=="05")?'selected':'').'">May</a></li>
                                <li><a href="javascript:void(0);" title="Jun" data-value="06" class="'.(($month_selected=="06")?'selected':'').'">Jun</a></li>
                                <li><a href="javascript:void(0);" title="Jul" data-value="07" class="'.(($month_selected=="07")?'selected':'').'">Jul</a></li>
                                <li><a href="javascript:void(0);" title="Aug" data-value="08" class="'.(($month_selected=="08")?'selected':'').'">Aug</a></li>
                                <li><a href="javascript:void(0);" title="Sep" data-value="09" class="'.(($month_selected=="09")?'selected':'').'">Sep</a></li>
                                <li><a href="javascript:void(0);" title="Oct" data-value="10" class="'.(($month_selected=="10")?'selected':'').'">Oct</a></li>
                                <li><a href="javascript:void(0);" title="Nov" data-value="11" class="'.(($month_selected=="11")?'selected':'').'">Nov</a></li>
                                <li><a href="javascript:void(0);" title="Dec" data-value="12" class="'.(($month_selected=="12")?'selected':'').'">Dec</a></li>
                              </ul>
                          </div>
                          <div style="clear:both;">
                              <ul class="day months">';
                              for( $i = 1; $i <= 31 ; $i++){
                                if($i<10){
                                    $j = "0".$i;
                                }else{
                                    $j = $i;
                                }
                               $html.= '<li><a href="javascript:void(0);" title="'.$i.'" data-value="'.$j.'" class="'.(($day_selected==$i)?'selected':'').'">'.$i.'</a></li>';
                              }
                              $html .= '</ul>
                          </div>
                          '.$this->toolsbar().'
                        </div>
                    <div class="row" id="object-body" style="">
	                    <div class="col-xs-12" >
	                    	
	                    </div>
                        <div class="col-xs-12">
                            '.$this->planningDayView($data,$viewDate).'
                        </div>
                    </div>
                  </div>';
        $html .= $this->formMeeting();
        $html .= $this->formReport();
		return $html;
	}

    private function planningDayView($data,$viewDate){
        $this->viewDate = $viewDate;
        $html = '<div class="planning-day-view animated bounceInRight"><ul>';
        $html .= '<li class="">
                      <article tabindex="0">
                        <div class="outline"></div>
                        <div class="dismiss"></div>
                        <div class="binding"></div>
                        <h1>Planning '.$viewDate.'</h1>
                        <table style="width:100%;">
                          <thead>
                            <tr><th>#</th><th>0:00-01:59</th><th>02:00-3:59</th><th>04:00-05.59</th><th>06:00-07:59</th><th>08:00-09:59</th><th>10:00-11:59</th><th>12:00-13.59</th><th>14.00-15.59</th><th>16:00-17:59</th><th>18:00-19:59</th><th>20:00-21:59</th><th>22:00-23:59</th></tr>
                          </thead>
                          <tbody></tbody>
                        </table>
                      </article>
                    </li>';
        $html .= '<li class="">
                      <article tabindex="0" style="overflow:auto; height:'.($this->windowHeight-200).'px;">
                        
                        <table style="width:100%;">
                          
                          <tbody>';
        $html .= $this->planningDayRowView($data);
        $html .=    '</tbody></table>
                      </article>
                    </li>';
        $html .= "</ul></div>
        ".$this->planningDayRowBoxActivityView($data)."";
        
        return $html;
    }

    private function controlSidebar(){
        $html = '';
        $html .= '';
        return $html;
    }

    private function formReport(){
        $html = '';
        $html .= '<aside class="control-sidebar control-sidebar-dark" id="report">
                    <div class="box-tools pull-right" style="margin-right: 10px;">
                        <button type="button" class="btn btn-box-tool" id="closeReport" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div style="margin: 30px 15px;" >
                        <h5>Report <span class="name"></span> </h5>
                        <hr/>
                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-4"><span class="pull-right">เหตุผล</span></div>
                            <div class="col-sm-8">
                                <select id="type_report" class="form-control">
                                    <option value="ไม่ว่างแต่ไม่ลงตาราง Planning">ไม่ว่างแต่ไม่ลงตาราง Planning</option>
                                    <option value="ไม่รับสาย">ไม่รับสาย</option>
                                    <option value="รอลูกค้านัดเวลาคอนเฟิร์ม">รอลูกค้านัดเวลาคอนเฟิร์ม</option>
                                    <option value="99">อื่นๆ</option>
                                </select>
                            </div>
                        </div>
                        <div class="row " style="margin-top:10px;" >
                            <div class="col-sm-4"><span class="pull-right">วิธีแก้ปัญหา</div>
                            <div class="col-sm-8">
                            <textarea class="form-control" rows="3" id="solution" placeholder="Enter ..."></textarea>
                            </div>
                        </div>
                        <div class="row " style="margin-top:10px;" >
                            <div class="col-sm-4"><span class="pull-right other">อื่นๆ</span></div>
                            <div class="col-sm-8">
                            <textarea class="form-control" rows="3" id="other" placeholder="Enter ..."></textarea>
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-12"">
                                <input type="button" class="btn btn-block btn-warning btn-flat" style="background-color: #e08e0b;" value="Report" id="saveReport"/>
                            </div>
                        </div>
                    </div>
                </aside>';

        return $html;
    }


     private function formMeeting(){
        $html = '';
        $html .= '<aside class="control-sidebar control-sidebar-dark" id="meeting">
                    <div class="box-tools pull-right" style="margin-right: 10px;">
                        <button type="button" class="btn btn-box-tool" id="closeCreateMeeting" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div style="margin: 30px 15px;" >
                        <h5>Create Meeting or Trainning</h5>
                        <hr/>
                        <form>
                            <div class="row" style="margin-top: 10px;" >
                            <div for="type-meeting" class="col-sm-6">ชนิดการประชุม</div>

                            <div class="col-sm-6">
                                <select class="form-control" id="type-meeting">
                                    <option value="1" >SSS Department Meeting</option>
                                    <option value="2" >Meeting K.Niwat Team (09:00 - 10:30 AM)</option>
                                    <option value="3" >Meeting K.Anusorn Team (10:30 - 12:00 AM)</option>
                                    <option value="4" >Meeting K.Niwat & K.Anusorn Team</option>
                                    <option value="5" >SSS Training</option>
                                </select>
                            </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                            <div for="subject-meeting" class="col-sm-6">หัวข้อการประชุม</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control"  name="subject-meeting" />
                            </div>
                            </div>
                           
                           <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-6">

                                    <div > วันที่ </div>

                                    <div class="input-group date " >
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" id="datepicker">
                                    </div>
                                   
                                </div>
                                <div class="col-sm-6">
                                    <div >ห้อง:</div>

                                    <div>
                                      <select class="form-control" id="type-meeting">
                                        <option value="1" >Floor 10 room 1001</option>
                                        <option value="2" >Floor 10 room 1002</option>
                                        <option value="3" >Floor 10 room 1003</option>
                                        <option value="4" >Floor 8 room 801</option>
                                        <option value="5" >Floor 8 room 802</option>
                                        <option value="6" >Floor 9 room 901</option>
                                        <option value="7" >Floor 9 room 901</option>
                                        <option value="8" >Floor 18 room 1801</option>
                                        <option value="9" >Floor 19 room 1901</option>
                                    </select>
                                    </div>
                                        
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">    
                                <div class="col-sm-12">
                                    <div for="attendance-meeting" >ผู้เข้าร่วม</div>
                                
                                    <div class="row">
                                        <div class="col-sm-3"><input type="checkbox" id="all"/>All</div>
                                        <div class="col-sm-3"><input type="checkbox" id="GSP"/>GSP</div>
                                        <div class="col-sm-3"><input type="checkbox" id="SAA"/>SAA</div>
                                        <div class="col-sm-3"><input type="checkbox" id="ACE"/>ACE</div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-3"><input type="checkbox" id="OtherProduct"/>Other Product</div>
                                       <div class="col-sm-3"><input type="checkbox" id="Implement"/>Implement</div>
                                       <div class="col-sm-3"><input type="checkbox" id="Install"/>Install</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <input type="submit" value="Click me" >
                            </div>
                        </form>
                    </div>
                </aside>';
        return $html;   
    }

    private function planningDayRowView($data){
        $html = '';
        $role_name_view = "";
        foreach ($data as $key => $value) {
            if($value['role_name_view']!=$role_name_view){
                $role_name_view = $value['role_name_view'];
                $html .= '<tr><td>'.$role_name_view.'</td><td colspan="12"></td></tr>';
            }
            $html .= '<tr class="e-'.$value['empno'].'">
                <td class="is-holiday" title="'.$value['thainame'].', '.$value['mobile'].', Number Task this month: '.$value['numberTaskThisMonth'].'">
                    <img class="pic-standby- direct-chat-img" src="'.$value['pic_engineer'].'" alt="" />
                    <div class="nickname">'.$value['nickname'].' </div><div class="fullname" style="clear:both;" data-email="'.$value['email'].'" data-name="'.$value['thainame'].'">';
                    if($this->email=="autsakorn.t@firstlogic.co.th" || $this->email=="sarinaphat.t@firstlogic.co.th" || $this->email=="teerawut.p@firstlogic.co.th"){
                        $html .= '<span class="addReport">'.$value['thainame'].'</span>';
                    }else{
                        $html .= '<span class="addReport">'.$value['thainame'].'</span>';
                    }
                        $html .= '</div><div style="clear:both;" class="mobile">'.$value['mobile'].'</div><div class="TaskThisMonth" style="clear:both;">'.(isset($value['numberTaskThisMonth'])?'Service Month: '.$value['numberTaskThisMonth']:'').'</div>
                
                    </div>
                </td>';
                for ($i=0; $i < 24; $i+=2) { 
                    $html .= '<td class="t-'.$i.'"></td>';
                }
            $html .= '</tr>';
        }
        return $html;
    }

    private function planningDayRowBoxActivityView($data){
        $html = "<script>$(function(){";
        foreach ($data as $key => $value) {
            foreach ($value['tasks'] as $k => $v) {
                if($this->viewDate==$v['appointment_date_df']){
                    if($v['appointment_h']%2==1){
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['appointment_h']-1)."').append('".$this->activityBox($v,'odd')."');";   
                    }else{
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['appointment_h'])."').append('".$this->activityBox($v)."');";   
                    }
                }else{
                    $html .= "$('.e-".$value['empno']."').find('.t-0').append('".$this->activityBox($v)."');";   
                }
            }
            if(isset($value['ace']['start_h'])){
                if(($value['ace']['start_h'])%2==1){
                    $html .= "$('.e-".$value['empno']."').find('.t-".intval($value['ace']['start_h']-1)."').append('".$this->activityBox($value['ace'],'odd')."');";
                }else{
                    $html .= "$('.e-".$value['empno']."').find('.t-".intval($value['ace']['start_h'])."').append('".$this->activityBox($value['ace'])."');";
                }
            }
            if(isset($value['consultAce']['start_h'])){
                if(($value['consultAce']['start_h'])%2==1){
                    $html .= "$('.e-".$value['empno']."').find('.t-".intval($value['consultAce']['start_h']-1)."').append('".$this->activityBox($value['consultAce'],'odd','consult')."');";
                }else{
                    $html .= "$('.e-".$value['empno']."').find('.t-".intval($value['consultAce']['start_h'])."').append('".$this->activityBox($value['consultAce'],'','consult')."');";
                }
            }
            if(count($value['standby7x24'])>0){
                foreach ($value['standby7x24'] as $k => $v) {
                    if(($v['start_h'])%2==1){
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['start_h']-1)."').append('".$this->activityBox($v,'odd')."');";
                    }else{
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['start_h'])."').append('".$this->activityBox($v)."');";
                    }
                }
            }
            if(count($value['otherProducts'])>0){
                foreach ($value['otherProducts'] as $k => $v) {
                    if(($v['start_h'])%2==1){
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['start_h']-1)."').append('".$this->activityBox($v,'odd')."');";
                    }else{
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['start_h'])."').append('".$this->activityBox($v)."');";
                    }
                }
            }
            if(count($value['listBusyEmployeeByDay'])>0) {
                foreach ($value['listBusyEmployeeByDay'] as $k => $v) {
                    if(intval($v['start_h'])%2==1){
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['start_h']-1)."').append('".$this->activityBox($v,'odd')."');";   
                    }else{
                        $html .= "$('.e-".$value['empno']."').find('.t-".intval($v['start_h'])."').append('".$this->activityBox($v,'')."');";   
                    }
                }

            }
        }
        $html .= "});</script>";
        return $html;
    }
    private function activityBox($data,$startPosition="",$type=""){
        $html = '';
        if(explode(":", $data['expect_duration'])){
            $explode = explode(":", $data['expect_duration']);
            $expectDuration = $explode[0];
        }else{
            $expectDuration = $data['expect_duration'];            
        }
        if(isset($data['appointment_date_df'])){
            if($this->viewDate!=$data['appointment_date_df']){
                $expectDuration = $expectDuration-(24-intval($data['appointment_h']));
            }
        }
        $expectDuration /= 2;
        $expectDuration *= 100;
        $left = 0;
        if($startPosition=="odd"){
            $left = 50;
        }
        if(isset($data['service_type'])){
            if($data['case_type']=="Preventive Maintenance"){
                $expectDuration = 100;
            }
            $subjectActivity = '';
            $subjectActivity = str_replace("'", "", trim($data['ticket_subject']));
            $subjectActivity = str_replace(".", "", $subjectActivity);

            $service_data_title = ''.$data['no_task'].'  '.$data['ticket_end_user_company_name'].' '.$subjectActivity.' '.$data['no_ticket'].'';

            $service_data = ''.$data['icon'].' '.$data['ticket_end_user_company_name'].'<br/>'.$data['no_task'].' '.$subjectActivity.'<br/>'.$data['no_ticket'].'';

            $html = '<div style="left:'.$left.'%;width:'.$expectDuration.'%;" class="activity-planning service-type-'.$data['service_type'].'"><a title="" href="'.URL.'pages/casesmanagement/#service_report_from_planning/'.$data['ticket_sid'].'/'.$data['tasks_sid'].'/right/'.rand(0,10000).'" target="_blank">'.$service_data.'</a></div>';
        }else if(isset($data['ace_1_email'])){
            $name = "ACE";
            if($type=="consult"){
                $name = "Consultant ACE";
            }
            $html = '<div style="left:'.$left.'%;width:'.$expectDuration.'%;" class="activity-planning ace">'.$name.'</div>';
        }else if(isset($data['type_time_leave'])){
            $html = '<div style="left:'.$left.'%;width:'.$expectDuration.'%;" class="activity-planning leave">'.$data['name'].' '.((isset($data['detail']))?$data['detail']:'').'</div>';
        }else if(isset($data['email']) && isset($data['buddy_no'])){
            $name = "7x24";
            $html = '<div style="left:'.$left.'%;width:'.$expectDuration.'%;" class="activity-planning standby-7x24"> '.$name.'</div>';
        }else if(isset($data['email'])){
            $name = "Standby Other Product";
            $html = '<div style="left:'.$left.'%;width:'.$expectDuration.'%;" class="activity-planning standby-7x24"> '.$name.'</div>';
        }

        return $html;
    }

	private function toolsbar(){
		$html = '';
		$html .= '<div class="box-tools pull-right">
                    
					<button type="button" id="openCalendar" class="btn btn-primary- btn-sm daterange " >
                  		<i class="fa fa-fw fa-calendar-check-o"></i>
                  	</button>

                    <div class="btn-group">
                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-fw fa-eye"></i> View </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a id="toggleImage" title="Toggle Image" >
                                    <i class="fa fa-fw fa-image"></i> Show/Hide Image Staff</a>
                                </li>
                                <li><a id="toggleNickName" title="Toggle Nick Name" >
                                        <i class="fa fa-fw fa-user"></i> Show/Hide Nick name Staff
                                    </a>
                                </li>
                                <li><a id="toggleMobile" title="Toggle Mobile" >
                                        <i class="fa fa-fw fa-mobile-phone"></i> Show/Hide Mobile
                                    </a>
                                </li>
                                <li><a id="toggleTaskThisMonth" title="Toggle Number Task" >
                                        <i class="fa fa-fw fa-tasks"></i> Show/Hide Number Task
                                    </a>
                                </li>
                            </ul>
                    </div>';
		if($this->email=="autsakorn.t@firstlogic.co.th" || $this->email=="sarinaphat.t@firstlogic.co.th" || $this->email=="teerawut.p@firstlogic.co.th"){
			 $html .= '
                    <button type="button" id="viewReportEngineer" class="btn btn-primary- btn-sm daterange " >
                        <i class="fa fa-fw fa-area-chart"></i>
                    </button>
                    <button type="button" id="addMeeting" class="btn btn-primary- btn-sm daterange " >
                        <i class="fa fa-fw fa-users"></i>
                    </button>
                    <div class="btn-group">
			        		<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			            	<i class="fa fa-fw fa-gear"></i></button>
			                <ul class="dropdown-menu pull-right" role="menu">
			                    <li><a href="javascript:void(0);">Standby Buddy</a></li>
			                    <li class="divider"></li>
			                    <li><a href="javascript:void(0);">Standby Buddy 1</a></li>
			                </ul>
			        </div>';
		}
		$html .= '</div>';
		return $html;
	}


	public function templateCalendarYear($dataDateYear,$windowHeight){
		$this->dataDate = $dataDateYear;
		$html = '';
        $html .= '<div class="object content animated bounceInRight">
                        <div class="box-header with-border">
                            <button type="button" id="btnBack" class="btn btn-primary- btn-sm daterange " >
                                <i class="fa fa-fw fa-chevron-left"></i>
                            </button>
                            <h3 class="box-title pull-right"><i class="fa fa-fw fa-calendar-check-o"></i> Choose Date</h3>
                        </div>
                    <div class="row" style="overflow:auto;height:'.($windowHeight-250).'px;">
                    <div class="col-xs-12">
                    <div class="standby"><ul>';
		        for ($i=0; $i <12 ; $i++) { 
		            $html .= $this->templateCalendarView($i);
		        }
		        $html .= '</ul></div><!--End .standby-->
        				</div>
        			</div>
        </div><!--End .object.content-->';
        return $html;
	}

	
	private function templateCalendarView($index){
        $dataDate = $this->dataDate[$index];
        $html = '<li class="">
                      <article tabindex="0">
                        <div class="outline"></div>
                        <div class="dismiss"></div>
                        <div class="binding"></div>
                        <h1>'.$this->month[$index].'</h1>
                        <table>
                          <thead>
                            <tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>
                          </thead>
                          <tbody>';
        $htmlRow = '';
        $n = 0;
        for ($i=0; $i < 31; $i++) { 
            if($n%7==0){
                if($htmlRow!=""){
                    $htmlRow .= '</tr>';
                }
                $htmlRow .= "<tr>";
            }
            if($i==0){
                if($dataDate[$i]['date_w']=="Sun"){
                    $htmlRow .= '<td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'>
                                <div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                }else if($dataDate[$i]['date_w']=="Mon"){
                    $htmlRow .= '<td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=1;
                }else if($dataDate[$i]['date_w']=="Tue"){
                    $htmlRow .= '<td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=2;
                }else if($dataDate[$i]['date_w']=="Wed"){
                    $htmlRow .= '<td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=3;
                }else if($dataDate[$i]['date_w']=="Thu"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=4;
                }else if($dataDate[$i]['date_w']=="Fri"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=5;
                }else if($dataDate[$i]['date_w']=="Sat"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=6;
                }
            }else{
                $htmlRow .= '<td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                        <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                        </td>';
            }
            $n++;
        }
        $html .= ''.$htmlRow;
        $html .= '</tbody></table>
                      </article>
                    </li>';
        return $html;
    }
}
?>