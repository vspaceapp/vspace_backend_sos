<?php class RequestTaxiView{

	public function view($email, $data, $filter, $windowHeight){
		$html = '<div class="content">';
		$html .= '<div class="box" style="border: none;box-shadow: none;">';

		$html .= '<div class="row">
					<div class="col-xs-8">
						<div style="margin:0px 5px;">
							<div style="margin:0px -10px;">
								<div class="box-header">
					              	<h3 class="box-title"><i class="fa fa-fw fa-taxi"></i> Taxi Approval</h3>
					              	<div class="box-tools">
						                <!--<div class="input-group input-group-sm" style="width: 150px;">
						                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
						                  <div class="input-group-btn">
						                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
						                  </div>
						                </div>-->
					              	</div>
					            </div>
							</div>';
		$html .= '<div style="clear:both;"><ul class="year months">
						<li><a href="javascript:void(0);" title="Jan" data-value="2016" class="'.(($filter['year']=="2016")?"selected":"").'">2016</a></li>
						<li><a href="javascript:void(0);" title="Feb" data-value="2017" class="'.(($filter['year']=="2017")?"selected":"").'">2017</a></li>
					</ul></div>';
		$html .= '<div style="clear:both;"><ul class="month months">
						<li><a href="javascript:void(0);" title="Jan" data-value="01" class="'.(($filter['month']=="01")?"selected":"").'">Jan</a></li>
						<li><a href="javascript:void(0);" title="Feb" data-value="02" class="'.(($filter['month']=="02")?"selected":"").'">Feb</a></li>
						<li><a href="javascript:void(0);" title="Mar" data-value="03" class="'.(($filter['month']=="03")?"selected":"").'">Mar</a></li>
						<li><a href="javascript:void(0);" title="Apr" data-value="04" class="'.(($filter['month']=="04")?"selected":"").'">Apr</a></li>
						<li><a href="javascript:void(0);" title="May" data-value="05" class="'.(($filter['month']=="05")?"selected":"").'">May</a></li>
						<li><a href="javascript:void(0);" title="Jun" data-value="06" class="'.(($filter['month']=="06")?"selected":"").'">Jun</a></li>
						<li><a href="javascript:void(0);" title="Jul" data-value="07" class="'.(($filter['month']=="07")?"selected":"").'">Jul</a></li>
						<li><a href="javascript:void(0);" title="Aug" data-value="08" class="'.(($filter['month']=="08")?"selected":"").'">Aug</a></li>
						<li><a href="javascript:void(0);" title="Sep" data-value="09" class="'.(($filter['month']=="09")?"selected":"").'">Sep</a></li>
						<li><a href="javascript:void(0);" title="Oct" data-value="10" class="'.(($filter['month']=="10")?"selected":"").'">Oct</a></li>
						<li><a href="javascript:void(0);" title="Nov" data-value="11" class="'.(($filter['month']=="11")?"selected":"").'">Nov</a></li>
						<li><a href="javascript:void(0);" title="Dec" data-value="12" class="'.(($filter['month']=="12")?"selected":"").'">Dec</a></li>
					</ul></div>';
		$html .= '<div style="text-align:right;">'.count($data).' Rows.</div>';
		$html .= '<div id="present-data" style="overflow:scroll;position:absolute;height:'.($windowHeight-200).'px;left:10px;right:0;"><table class="table table-hover table-bordered" style="font-size:11px;">
                <tbody><tr>
                  <th>SR No.</th>
                  <th>Subject</th>
                  <th>End Usr / Address</th>
                  <th style="widht:50px;">Taxi(Baht)</th>
                  <!--<th>By</th>-->
                  <th>Status</th>
                </tr>';
        if(count($data)>0){
			foreach ($data as $key => $value) {
				$html .= '<tr class="list-approval" data-sid="'.$value['sid'].'">
			                  <td style="font-size:18px;">'.$value['no_task'].'</td>
			                  <td width="300">'.$value['subject_service_report'].'</td>
			                  <td width="300"><span style="font-size:14px;">'.$value['end_user'].'</span><br style="margin: 0px 0px 5px 0px;">'.$value['end_user_site'].'</td>
			                  <td style="font-size:22px; text-align:right;">'.$value['taxi_fare_total'].'</td>
			                  <!--<td>'.$value['thainame'].'</td>-->
			                  <td>
			                  	'.(($value['taxi_fare_total_approved'])?'<span class="label label-success">Approved</span>':'<span class="label label-warning">Pending</span>').'
			                  </td>
			                </tr>';
			}
		}else{
			$html .= '<tr><td colspan="5" style="text-align:center;">No Data</td></tr>';
		}
		$html .= '</tbody></table></div></div>';
		$html .= '</div>';
		$html .= '
		<div class="col-xs-4" >
			<div style="margin-top:80px;">Activity Log <span class="pull-right "><a class="toggle-detail" href="javascript:void(0);">Show Detail</a></span></div>
			<div style="margin:0px -5px;border:1px solid #f1f1f1; padding:5px; overflow:scroll;height:'.($windowHeight-300).'px;left:10px;right:0; margin-top:10px;background-color: #222d32;
    color: #fff;" class="properties_detail">Select Row.</div>
			<div style="text-align:right;margin: 10px -5px -8px -5px;padding: 5px;">
				<span class="">
						<button type="button" class="btn btn-default btn-primary bg-blue" id="approveTaxi">Approve</button>
						<button type="button" class="btn btn-default btn-warning label-warning">Pending</button>
				</span>
			</div>
		</div>';

		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '';
		return $html;
	}

	public function viewDetail($email, $data){
		$html = '<div><div> </div></div>';
		foreach ($data as $key => $value) {
			$taxi_fare = '';
			$taxi_fare_class = 'data-detail';
			if(isset($value['taxi_fare']) && $value['taxi_fare'] && $value['taxi_fare']!="-"){
				$taxi_fare = '<i class="fa fa-fw fa-taxi text-red"></i> '.$value['taxi_fare'].' Baht.';
				$taxi_fare_class = 'data-taxi';
			}
			if($value['status']=="-2" && $email=="autsakorn.t@firstlogic.co.th"){
				$html .= '<div class="activity-row '.$taxi_fare_class.'"><i class="fa fa-fw fa-clock-o"></i> '.$value['create_datetime'].'<span class="pull-right">'.$taxi_fare.'</span><br/>'.$value['create_by_thainame'].' ลบ '.$value['engineer_thainame'].' ออกจาก Service</div>';
			} 
			else if($value['status']==0){
				$html .= '<div class="activity-row '.$taxi_fare_class.'"><i class="fa fa-fw fa-clock-o"></i> '.$value['create_datetime'].'<span class="pull-right">'.$taxi_fare.'</span><br/>'.$value['create_by_thainame'].' '.$value['service_status_name'].' '.$value['engineer_thainame'].'</div>';
			}else if($value['status']=="600"){
				$html .= '<div class="activity-row '.$taxi_fare_class.'"><i class="fa fa-fw fa-clock-o"></i> '.$value['create_datetime'].'<span class="pull-right">'.$taxi_fare.'</span><br/>'.$value['create_by_thainame'].' กลับถึงปลายทาง</div>';
			}else{
				$html .= '<div class="activity-row '.$taxi_fare_class.'"><i class="fa fa-fw fa-clock-o"></i> '.$value['create_datetime'].'<span class="pull-right">'.$taxi_fare.'</span><br/>'.$value['create_by_thainame'].' '.$value['service_status_name'].'</div>';
			}
		}
		$html .= '';
		return $html;
	}
}
?>