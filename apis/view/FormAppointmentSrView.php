<?php class FormAppointmentSrView{

	public static function view(){
		$html = '<style>
					#FormAppointmentSrView {display:none;height:98%;}
					#FormAppointmentSrView .table-view-cell {
						padding-top:0px;
					}
					#FormAppointmentSrView .datepicker .block {box-shadow: none;}
				</style>';
		$html .= '<div id="FormAppointmentSrView" class="content">
	                <div class="card">
	                  <ul class="table-view">
	                      <li class="table-view-divider">Appointment</li>
	                      <li class="table-view-cell"><div class="appointment">06.01.2017 11:00</div></li>
	                  </ul>
	                </div>
	                <div class="card">
	                  <ul class="table-view" x-apple-data-detectors="false">
	                      <li class="table-view-divider">New Appointment Date</li>
	                      <li class="table-view-cell cell-block" x-apple-data-detectors="false">
	                        <div id="appointment_datetime" x-apple-data-detectors="false">
	                           <div class="datepicker" x-apple-data-detectors="false">
	                              <div id="title" class="labels">Fri, Jan 6, 2017</div>
	                              <div class="block action addDay">+</div>
	                              <div class="block action addMonth">+</div>
	                              <div class="block action addYear">+</div>
	                              <div></div>
	                              <div class="block middle" id="day" x-apple-data-detectors="false">06</div>
	                              <div class="block middle" id="month" x-apple-data-detectors="false">01</div>
	                              <div class="block middle" id="year" x-apple-data-detectors="false">2017</div>
	                              <div></div>
	                              <div class="block action subDay">-</div>
	                              <div class="block action subMonth">-</div>
	                              <div class="block action subYear">-</div>
	                            </div>
	                        </div>
	                      </li>
	                      <li class="table-view-divider">New Appointment Time</li>
	                      <li class="table-view-cell cell-block">
	                        <div id="appointment_datetime_">
	                           <div class="datepicker">
	                              <!-- <div id="title-time" class="labels"></div> -->
	                              <div class="block action addHour">+</div>
	                              <div class="block action addMinutes">+</div>
	                              <!-- <div class="block noborder" >&nbsp;</div> -->
	                              <div></div>
	                              <div class="block middle" id="hour">11</div>
	                              <div class="block middle" id="minutes">00</div>
	                              <!-- <div class="block middle action toggleAMPM" id="ampm" >AM</div> -->
	                              <div></div>
	                              <div class="block action subHour">-</div>
	                              <div class="block action subMinutes">-</div>
	                              <!-- <div class="block noborder" >&nbsp;</div> -->
	                              <!-- <div class="action-row">
	                                <div class="action" onclick="">Cancel</div>
	                                <div class="action" onclick="">Reset</div>
	                                <div class="action" onclick="">OK</div>
	                              </div> -->
	                            </div>
	                        </div>
	                      </li>
	                      <li class="table-view-divider">Expect Duration</li>
	                      <li class="table-view-cell cell-block">
	                            <div class="datepicker">
	                            
	                            <div class="block action addHourExpect">+</div>
	                            <div class="block action addMinutesExpect">+</div>
	                            
	                            <div></div>
	                            <div class="block middle" id="hourExpect">2</div>
	                            <div class="block middle" id="minutesExpect">0</div>
	                            <div></div>
	                            <div class="block action subHourExpect">-</div>
	                            <div class="block action subMinutesExpect">-</div>
	                            
	                            <!-- <div class="action-row">
	                              <div class="action" onclick="">Cancel</div>
	                              <div class="action" onclick="">Reset</div>
	                              <div class="action" onclick="">OK</div>
	                            </div> -->
	                            </div>
	                      </li>
	                      <li class="table-view-cell cell-block" style="margin-top:10px;">
	                      	<div style="float:left; width:30%;">
	                            <a href="javascript:void(0);" style="color:#ffffff;" class="btn btn-block btn-warning" id="BackToMain">Back</a>
	                        </div>
	                        <div style="float:left; width:68%; margin-left:2%;">
	                            <button type="button" class="btn btn-positive btn-block" id="btnChangeAppointment">Done</button>
	                        </div>
	                      </li>
	                  </ul>
	                </div>
	               
	            </div>';
	    return $html;
	}
}