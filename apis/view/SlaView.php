<?php class SlaView{
    private $email;
    private $data;
    private $ticket_sid;
    private $worklog;
    private $step;
    private $type_request;
    private $btnBack;
    public function __construct($email){
        $this->email = $email;
    }

    public function templateStandbyView($email){
        
    }
    public function templateRuleSlaView($email, $ruleSla){
        $html = '';
        $html .= '<div class="row" style="margin-bottom:10px;">';
        $html .= '<div class="col-xs-6"><div style="border-bottom:1px solid #f1f1f1">Contract: <span class="pull-right">'.$ruleSla['contract_no'].'</span></div></div>';
        $html .= '<div class="col-xs-6"><div style="border-bottom:1px solid #f1f1f1">Project: <span class="pull-right">'.$ruleSla['project'].'</span></div></div>';
        $html .= '</div>';
        $html .= '<div class="row" style="margin-bottom:10px;">';
        $html .= '<div class="col-xs-6"><div style="border-bottom:1px solid #f1f1f1">Customer: <span class="pull-right">'.$ruleSla['customer'].'</span></div></div>';
        $html .= '<div class="col-xs-6"><div style="border-bottom:1px solid #f1f1f1">Address: <span class="pull-right">'.$ruleSla['full_address'].'</span></div></div>';
        $html .= '</div>';
        $html .= '<div class="row">';
        $html .= '<div class="col-xs-12">';
        $html .= '<table class="table table-bordered"><tbody><tr><th>SLA Name</th><th>Critical</th><th>High</th><th>Normal</th><th>Medium</th><th>Low</th><th>Unit</th></tr>';
        foreach ($ruleSla['sla_detail'] as $key => $value) {
            $html .= '<tr><td><div>'.$value['sla_name'].'</div></td><td>'.$value['critical'].'</td><td>'.$value['high'].'</td><td>'.$value['normal'].'</td><td>'.$value['medium'].'</td><td>'.$value['low'].'</td><td>'.$value['unit_time'].'</td></tr>';
        }
        $html .= '</tbody></table>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
    public function templateSLA($ticket_sid,$sla_page,$worklog, $step,$type_request){
        $this->ticket_sid = $ticket_sid;
        $this->worklog = $worklog;
        $this->step = $step;
        $this->type_request = $type_request;

        if($this->type_request=="web"){
          $this->btnBack = "javascript:void(0);";
        }else{
          $this->btnBack = "#CaseDetail/CaseWip/left/".$this->ticket_sid."/";
        }

        if($sla_page=="2"){
        	 return $this->templateResponse();
        }else if($sla_page=="3"){
            return $this->templateWorkAround();
        }else if($sla_page=="4"){
            return $this->templatePending();
        }else if($sla_page=="5"){
            if($this->step=="1"){
              return $this->templateResolveWL();
            }else if($this->step=="2"){
              return $this->templateResolveSolution();
            }else if($this->step=="3"){
              return $this->templateResolveSolutionDetail();
            }
        }else if($sla_page=="6"){
            return $this->templateEmpty();
        }else if($sla_page=="7"){
            return $this->templateOnsite();
        }else if($sla_page=="8"){
            return $this->templateWorklog();
        }else if($sla_page=="9"){
            return $this->templateResponse();
        }else if($sla_page=="10"){
            return $this->templateNoWorkAround();
        }else if($sla_page=="11"){
          return $this->templateNoOnsite();
        }else{
          return $this->templateEmpty();
        }
    }
    private function templateEmpty(){
        $html = '';
        return $html;
    }
    private function templateResponse(){

    	$html = '';
    	$html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Response</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog">WIP</textarea>
                            <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaResponse">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateWorkAround(){

      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">WorkAround</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                          
                          <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaWorkAround">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templatePending(){
      
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Pending</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                            <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        <li class="table-view-divider">Expect Pending Date</li>
                        <li class="table-view-cell cell-block" x-apple-data-detectors="false">
                              <!-- <div class="input-datetime" id="appointment_datetime"></div> -->
                              <div class="datepicker" x-apple-data-detectors="false">
                                <div id="title" class="labels">Wed, Jan 4, 2017</div>
                                <div class="block action addDay">+</div>
                                <div class="block action addMonth">+</div>
                                <div class="block action addYear">+</div>
                                <div></div>
                                <div class="block middle" id="day" x-apple-data-detectors="false">04</div>
                                <div class="block middle" id="month" x-apple-data-detectors="false">01</div>
                                <div class="block middle" id="year" x-apple-data-detectors="false">2017</div>
                                <div></div>
                                <div class="block action subDay">-</div>
                                <div class="block action subMonth">-</div>
                                <div class="block action subYear">-</div>
                                <!-- <div class="action-row"> -->
                                  <!-- <div class="action" onclick="">Cancel</div> -->
                                  <!-- <div class="action" onclick="">Reset</div> -->
                                  <!-- <div class="action" onclick="">OK</div> -->
                                <!-- </div> -->
                              </div>
                        </li>
                        <li class="table-view-divider">Expect Pending Time</li>
                        <li class="table-view-cell cell-block">
                            <div class="datepicker">
                              <!-- <div id="title-time" class="labels"></div> -->
                              <div class="block action addHour">+</div>
                              <div class="block action addMinutes">+</div>
                              <!-- <div class="block noborder" >&nbsp;</div> -->
                              <div></div>
                              <div class="block middle" id="hour">22</div>
                              <div class="block middle" id="minutes">00</div>
                              <!-- <div class="block middle action toggleAMPM" id="ampm" >AM</div> -->
                              <div></div>
                              <div class="block action subHour">-</div>
                              <div class="block action subMinutes">-</div>
                              <!-- <div class="block noborder" >&nbsp;</div> -->
                              <!-- <div class="action-row">
                                <div class="action" onclick="">Cancel</div>
                                <div class="action" onclick="">Reset</div>
                                <div class="action" onclick="">OK</div>
                              </div> -->
                            </div>
                        </li>
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaPending">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateResolveWL(){
      
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Resolve</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                            <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>                       
                         <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnNextSlaResolveToSolution">Next</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateResolveSolution(){
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Resolve</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Solution</div>
                            <textarea rows="5" id="solution"></textarea>
                            <p style="color:red;">Solution กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                       
                         <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnNextSlaResolveToSolutionDetail">Next</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateResolveSolutionDetail(){
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Resolve</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                       
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Solution Detail</div>
                            <textarea rows="5" id="solution_detail"></textarea>
                            <p style="color:red;">Solution Detail กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                         <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaResolve">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateOnsite(){
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Onsite</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                          
                          <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaOnsite">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateWorklog(){
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">Worklog</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                          
                          <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaWorklog">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }   

    private function templateNoWorkAround(){
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">No WorkAround</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                          
                          <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaNoWorkaround">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }

    private function templateNoOnsite(){
      $html = '';
      $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                  <a class="back-from-end-user-choose pull-left backToActivity" href="'.$this->btnBack.'" style=""><span class="icon icon-left-nav"></span></a>
                  <a class="icon pull-right icon-person manual-person" style="visibility: hidden;" href="javascript:void(0);"></a>
                  <span class="mhn-ui-page-title">No Onsite</span>
                </div>
                <div class="mhn-ui-row mhn-ui-apps">
                  <div class="content">
                    <ul class="table-view">
                        <li class="table-view-cell" style="padding-right:15px;">
                            <div>Work Log</div>
                            <textarea rows="5" id="worklog"></textarea>
                          
                          <p style="color:red;">Work Log กรอกเป็นภาษาอังกฤษเท่านั้น</p>
                        </li>
                        
                        <li class="table-view-cell" style="padding-right:15px;">
                          <div><button class="btn btn-positive btn-block" id="btnDoneSlaNoOnsite">Done</button></div>
                        </li>
                    </ul>          
                  </div>
                </div>';
        return $html;
    }
}
?>