<?php class MeetingServiceView{
	
	private $email;	
	private $dataDate;
    private $viewDate;
	// private $month = array('January','February','March','April','May','June','July','August','September','October','November','December');
    private $windowHeight;
	public function __construct($email=""){
		$this->email = $email;
	}

	public function templateMeetingService($email,$windowHeight){
        $this->windowHeight = $windowHeight;
		$html = '';
        // $date_explode = explode("-", $viewDate);
        // $day_selected = $date_explode[0];
        // $month_selected = $date_explode[1];
        // $year_selected = $date_explode[2];
		$html .= '<div class="object content">
                        <div class="box-header with-border">
                          <h3 class="box-title"><i class="fa fa-fw fa-users"></i> Meeting</h3>
                         
                          '.$this->toolsbar().'
                        </div>
                    
                  </div>';
        $html .= $this->formMeeting();
        $html .= $this->TypeMeeting();
        // $html .= $this->formReport();
		return $html;
	}

    private function controlSidebar(){
        $html = '';
        $html .= '';
        return $html;
    }


    private function TypeMeeting(){
        $html = '';
        $html .= '<aside class="control-sidebar control-sidebar-dark" id="typeMeeting">
                    <div class="box-tools pull-right" style="margin-right: 10px;">
                        <button type="button" class="btn btn-box-tool" id="closeTypeMeeting" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div style="margin: 30px 15px;" >
                        <h5>ตั้งค่าชนิดการประชุมและตั้งค่าเริ่มต้นผู้เข้าร่วมประชุม</h5>
                        <hr/>
                        <form id="empform">
                            <div class="row" style="margin-top: 10px;" >
                                <div for="type-meeting" class="col-sm-4">กำหนดชนิดการประชุม*</div>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="type_meeting" />
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div for="subject-meeting" class="col-sm-4">เวลาเริ่ม - สิ้นสุดการประชุม*</div>
                                <div class="col-sm-4 ">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timepicker1" type="text" class="form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                                <div class="col-sm-4 ">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timepicker2" type="text" class="form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row" style="margin-top:10px;">
                                <div for="people-meeting" class="col-sm-12">กำหนดผู้เข้าร่วมประชุม </div>
                            </div>

                            <div class="row" style="margin-top:10px;">
                                <div class="col-sm-12 emp">
                                    <div class="col-sm-4"><input type="checkbox" id="checkAll" /> All</div> 
                                </div>
                            </div>

                            <div class="row" style="margin-top:10px;">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-block btn-info" id="saveConfig">Save Config</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </aside>';
        return $html;  
    }

     private function formMeeting(){
        $html = '';
        $html .= '<aside class="control-sidebar control-sidebar-dark" id="meeting">
                    <div class="box-tools pull-right" style="margin-right: 10px;">
                        <button type="button" class="btn btn-box-tool" id="closeCreateMeeting" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div style="margin: 30px 15px;" >
                        <h5>Create Meeting or Trainning</h5>
                        <hr/>
                        <form>
                            <div class="row" style="margin-top: 10px;" >
                            <div for="type-meeting" class="col-sm-6">ชนิดการประชุม</div>

                            <div class="col-sm-4">
                                <select class="form-control" id="type-meeting">
                                    <option value="1" >SSS Department Meeting</option>
                                    <option value="2" >Meeting K.Niwat Team</option>
                                    <option value="3" >Meeting K.Anusorn Team</option>
                                    <option value="4" >Meeting K.Niwat & K.Anusorn Team</option>
                                    <option value="5" >SSS Training</option>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <button type="button" id="addTypeMeeting" class="btn btn-primary daterange " >
                                    <i class="fa fa-fw fa-plus-square"></i>
                                </button>   
                            </div>

                            </div>

                            <div class="row" style="margin-top: 10px;">
                            <div for="subject-meeting" class="col-sm-6">หัวข้อการประชุม</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control"  name="subject-meeting" />
                            </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div for="subject-meeting" class="col-sm-6">วันเริ่ม - สิ้นสุดการประชุม</div>
                                <div class="col-sm-6 ">
                                   <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" id="datepicker">
                                   </div>
                                </div>
                            </div>

                           <div class="row" style="margin-top: 10px;">
                                
                                <div class="col-sm-6">
                                    <div >ห้อง:</div>
                                </div>

                                <div class="col-sm-6">
                                    <div>
                                      <select class="form-control" id="type-meeting">
                                        <option value="1" >Floor 10 room 1001</option>
                                        <option value="2" >Floor 10 room 1002</option>
                                        <option value="3" >Floor 10 room 1003</option>
                                        <option value="4" >Floor 8 room 801</option>
                                        <option value="5" >Floor 8 room 802</option>
                                        <option value="6" >Floor 9 room 901</option>
                                        <option value="7" >Floor 9 room 901</option>
                                        <option value="8" >Floor 18 room 1801</option>
                                        <option value="9" >Floor 19 room 1901</option>
                                    </select>
                                    </div>
                                        
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">    
                                <div class="col-sm-12">
                                    <div for="attendance-meeting" >ผู้เข้าร่วม</div>
                                
                                    <div class="row">
                                        <div class="col-sm-3"><input type="checkbox" id="all"/>All</div>
                                        <div class="col-sm-3"><input type="checkbox" id="GSP"/>GSP</div>
                                        <div class="col-sm-3"><input type="checkbox" id="SAA"/>SAA</div>
                                        <div class="col-sm-3"><input type="checkbox" id="ACE"/>ACE</div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-3"><input type="checkbox" id="OtherProduct"/>Other Product</div>
                                       <div class="col-sm-3"><input type="checkbox" id="Implement"/>Implement</div>
                                       <div class="col-sm-3"><input type="checkbox" id="Install"/>Install</div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </aside>';
        return $html;   
    }

	private function toolsbar(){
		$html = '';
		$html .= '<div class="box-tools pull-right">
                  
                    <button type="button" id="addMeeting" class="btn btn-primary- btn-sm daterange " >
                        <i class="fa fa-fw fa-plus-square"></i>
                    </button>';
		
		$html .= '</div>';
		return $html;
	}


	public function templateCalendarYear($dataDateYear,$windowHeight){
		$this->dataDate = $dataDateYear;
		$html = '';
        $html .= '<div class="object content animated bounceInRight">
                        <div class="box-header with-border">
                            <button type="button" id="btnBack" class="btn btn-primary- btn-sm daterange " >
                                <i class="fa fa-fw fa-chevron-left"></i>
                            </button>
                            <h3 class="box-title pull-right"><i class="fa fa-fw fa-calendar-check-o"></i> Choose Date</h3>
                        </div>
                    <div class="row" style="overflow:auto;height:'.($windowHeight-250).'px;">
                    <div class="col-xs-12">
                    <div class="standby"><ul>';
		        for ($i=0; $i <12 ; $i++) { 
		            $html .= $this->templateCalendarView($i);
		        }
		        $html .= '</ul></div><!--End .standby-->
        				</div>
        			</div>
        </div><!--End .object.content-->';
        return $html;
	}

	
	private function templateCalendarView($index){
        $dataDate = $this->dataDate[$index];
        $html = '<li class="">
                      <article tabindex="0">
                        <div class="outline"></div>
                        <div class="dismiss"></div>
                        <div class="binding"></div>
                        <h1>'.$this->month[$index].'</h1>
                        <table>
                          <thead>
                            <tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>
                          </thead>
                          <tbody>';
        $htmlRow = '';
        $n = 0;
        for ($i=0; $i < 31; $i++) { 
            if($n%7==0){
                if($htmlRow!=""){
                    $htmlRow .= '</tr>';
                }
                $htmlRow .= "<tr>";
            }
            if($i==0){
                if($dataDate[$i]['date_w']=="Sun"){
                    $htmlRow .= '<td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'>
                                <div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                }else if($dataDate[$i]['date_w']=="Mon"){
                    $htmlRow .= '<td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=1;
                }else if($dataDate[$i]['date_w']=="Tue"){
                    $htmlRow .= '<td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=2;
                }else if($dataDate[$i]['date_w']=="Wed"){
                    $htmlRow .= '<td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=3;
                }else if($dataDate[$i]['date_w']=="Thu"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=4;
                }else if($dataDate[$i]['date_w']=="Fri"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=5;
                }else if($dataDate[$i]['date_w']=="Sat"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td></td><td></td><td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                </td>';
                    $n+=6;
                }
            }else{
                $htmlRow .= '<td data-date="'.$dataDate[$i]['date_Ymd'].'" '.(($dataDate[$i]['holiday'])?'class="is-holiday"':'').'><div class="day">'.$dataDate[$i]['date_day'].'</div>
                        <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                        </td>';
            }
            $n++;
        }
        $html .= ''.$htmlRow;
        $html .= '</tbody></table>
                      </article>
                    </li>';
        return $html;
    }
}
?>