<?php
class UnsignedServiceViewApp{
	private $email;
  private $data;
  private $tabName;
  private $canAssignTo;
  private $windowHeight;
  private $permission_role;
	public function __construct( $email = ""){
		$this->email  = $email;
	}

	public function templateUnsignedServiceViewApp($email,$data, $order_by, $search="",$permission_role, $canAssignTo, $windowHeight){
    require_once '../../view/FormEndUserView.php';
    require_once '../../view/SignatureServiceReportView.php';
    $this->data = $data;
    $this->canAssignTo = $canAssignTo;
    $this->windowHeight = $windowHeight;
    $this->permission_role = $permission_role;
    $sourcePath = 'http://vspace.in.th/pages/signaturepad/customerApp/';
    $html = '<link href="http://vspace.in.th/develop/pos/tinder-animation-gsap/css/style.css" rel="stylesheet"></link>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/CSSPlugin.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/easing/EasePack.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenLite.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.0/utils/Draggable.min.js"></script>
            <script src="http://vspace.in.th/pages/lib/moment.min.js"></script>
            <style>
              .UnsignedServiceViewApp .app {
                background-color: initial;height: initial;
              }
              .UnsignedServiceViewApp .app div.photo {
                left:0;right:0;
                width:90%; background-color:white;
                height: 100%;
              }
              .UnsignedServiceViewApp .content-object {
                left:0;
                right:0;
                position: absolute;
                width:100%;overflow-y: auto; overflow-x: hidden;
                height: calc(100% - 60px);               
              }
              .UnsignedServiceViewApp .app span.meta {
                margin-top: auto;
                bottom: 0px;
                position: relative;
                width: 100%;
              }
              .UnsignedServiceViewApp .app {
                padding-top: 0px;
              }
              .UnsignedServiceViewApp .app .footer {
                  position: absolute;
                  margin: auto;
                  bottom: 0px;
                  left: 0;
                  padding: initial;
                  width: 180px;
                  right: 0;
              }
              .UnsignedServiceViewApp .content-data {
                color: #222d32;
                padding:10px;
              }
              .UnsignedServiceViewApp .content-data .clear{
                clear:both;
              }
              .UnsignedServiceViewApp .content-data .media-object {
                float:left;
              }
              .UnsignedServiceViewApp .info, .UnsignedServiceViewApp .rate {background-color:#ffffff;}
              #resendSigned, #signnow {
                position: relative;
              }
              #resendSigned::after{
                position: absolute; //content: "Resend Email";
                width: 100px;
                bottom: -25px;
                left: -20px;
                font-size: 0.85em;
              }
              #signnow::after{
                position: absolute; // content: "Sign Now";
                width: 100px;
                bottom: -25px;
                left: -20px;
                font-size: 0.85em;
              }
              .UnsignedServiceViewApp .rate:nth-child(1),.UnsignedServiceViewApp .rate:nth-child(2) {
                  margin-left: 4px;
                  background: none;
                  background-position: center center;
                  background-repeat: no-repeat;
                  background-color: #ffffff;
              }
              .UnsignedServiceViewApp .rate:nth-child(3) {
                  margin-left: -4px;
                  background: none;
                  background-size: 25px;
                  background-position: center;
                  background-repeat: no-repeat;
                  background-color: #ffffff;
              }
              .UnsignedServiceViewApp .meta {
                color: #aaaaaa;
                text-align: right;
                font-size: 0.8em;
              }
              .UnsignedServiceViewApp .moments {
                width: initial;
              }
              .UnsignedServiceViewApp div.photo {
                box-shadow: none;
                border-radius: 0px;
                border: none;
              }
              span.meta {
                display: block;
                height: 48px;
                background-color: #FFF;
                margin-top: 267px;
                border-radius: 8px 8px 8px 8px;
                font-size: 18px;
                box-sizing: border-box;
                padding: 12px;
              }
              .app::before {
                position: absolute;
                color: #aaaaaa;
                left: initial;
                top: 150px;
                content: "Scroll here to view below";
                -webkit-transform: rotate(30deg);
                -moz-transform: rotate(30deg);
                -o-transform: rotate(30deg);
                -ms-transform: rotate(30deg);
                transform: rotate(270deg);
                left: -70px;
              }
              .UnsignedServiceViewApp .footer .btnLeft {
                float:left;width:49%; margin-right:1%;
                // display:none;
              }
              .UnsignedServiceViewApp .footer .btnLeft button {
                color:#ffffff;
              }
              .UnsignedServiceViewApp .footer .btnRight {
                float:right;width:49%; margin-left:1%;
                // display:none;
              }
              .UnsignedServiceViewApp .app .footer {
                  position: absolute;
                  margin: auto;
                  bottom: 0px;
                  left: 0;
                  padding: initial;
                  width: 90%;
                  right: 0;
              }
              .UnsignedServiceViewApp .btn-block {
                  padding: 10px 0px;
                  font-size: 14px;
              }
            </style>
            <div class="UnsignedServiceViewApp">
                <div class="mhn-ui-app-title-head" style="text-align:center;">
                    <a class="backToMoreSystem pull-left" href="#MoreSystem" style="color:#fff;"><span class="icon icon-left-nav"></span></a>
                    <i class="ion-compose"></i> Unsigned Service Report
                </div>
                <div class="mhn-ui-row mhn-ui-apps animated bounceInRight">
                      <div class="app" style="width:100%;">
                        <div class="content-object">
                        </div>
                        <div class="footer">
                            <div style="width:100%;text-align:center;">
                              <div style="width:100%;">
                                <div class="btnLeft"><button id="resendSigned" class="btn btn-block btn-warning"><i class="fa fa-fw fa-send-o" style="font-size: 1em;"></i>Resend Email</button></div>
                                <div class="btnRight"><button id="signnow" class="btn btn-block btn-primary"><i class="fa fa-fw fa-pencil-square-o" style="font-size: 1em;"></i>Signature Now</button></div>
                                <div style="clear:both;"></div>
                              </div>
                              <!--<div class="rate" id="resendSigned">
                                <i class="fa fa-fw fa-send-o" style="font-size: 2em; color:#d9534f;margin-top: 15px;"></i>
                              </div>
                              <div class="info" style="visibility: hidden;"></div>
                              <div class="rate" id="signnow">
                                <i class="fa fa-fw fa-pencil-square-o" style="font-size: 2em; color:#5cb85c; margin-top: 15px;"></i>
                              </div>-->
                            </div>
                        </div>
                      </div>
                      '.FormEndUserView::view().''.SignatureServiceReportView::view().'
                </div>
            </div>
            <script>
              var serviceNo;
              var currentTaskSid;
              var allDataForPage;
              
              $(".UnsignedServiceViewApp").on("click","#resendSigned",function(){
                  resendSigned(currentTaskSid).done(function(res){
                    alert("Sent Email");
                  });
              })
              var resendSigned = function(tasks_sid){
                  return $.ajax({
                      url: END_POINT_1+"sendmail/sendEmailAfterClosedToCustomer/"+tasks_sid,
                      type: "POST",
                      data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token")}
                  });
              }
              var sequenceData = 0;
               $(".TodolistViewApp div#swipe_like").on("click", function() {
                    // swipeLike();
                  location.reload();
                });
                $(".TodolistViewApp div#swipe_dislike").on("click", function() {
                    swipeDislike();
                });
                addNewProfile();

                function swipe() {
                    Draggable.create("#photo", {
                        throwProps: true,
                        onDragEnd: function(endX) {
                            if (Math.round(this.endX) > 0) {
                                swipeLike();
                            } else {
                                swipeDislike();
                            }
                            //console.log(Math.round(this.endX));
                        }
                    });
                }

                function swipeLike() {
                    var $photo = $("div.content-object").find("#photo");
                    var swipe = new TimelineMax({
                        repeat: 0,
                        yoyo: false,
                        repeatDelay: 0,
                        onComplete: remove,
                        onCompleteParams: [$photo]
                    });
                    swipe.staggerTo($photo, 0.8, {
                        bezier: [{
                            left: "+=400",
                            top: "+=300",
                            rotation: "60"
                        }],
                        ease: Power1.easeInOut
                    });
                    sequenceData--;
                    if(sequenceData<0){
                      sequenceData = allDataForPage.length-1;
                    }
                    addNewProfile();
                }

                function swipeDislike() {
                    var $photo = $("div.content-object").find("#photo");
                    var swipe = new TimelineMax({
                        repeat: 0,
                        yoyo: false,
                        repeatDelay: 0,
                        onComplete: remove,
                        onCompleteParams: [$photo]
                    });
                    swipe.staggerTo($photo, 0.8, {
                        bezier: [{
                            left: "+=-350",
                            top: "+=300",
                            rotation: "-60"
                        }],
                        ease: Power1.easeInOut
                    });
                    sequenceData++;
                    addNewProfile();
                }
                function remove(photo) {
                    $(photo).remove();
                }
                
                function addNewProfile() {
                    // var names = ["Lieke", "Christina", "Sanne", "Soraya", "Chanella", "Larissa", "Michelle"][Math.floor(Math.random() * 7)];
                    // var ages = ["19", "22", "18", "27", "21", "18", "24"][Math.floor(Math.random() * 7)]
                    // var photos = ["1", "2", "3", "4", "5", "6", "7"][Math.floor(Math.random() * 7)]
                    var data = '.json_encode($this->data).';
                    allDataForPage = data;
                    console.log(sequenceData);
                    sequenceData %=data.length;
                    if(data.length>0){
                      currentTaskSid = data[sequenceData]["tasks_sid"];
                      $("div.content-object").prepend("<div class=\"photo\" id=\"photo\" ><span class=\"meta\"><p>Swipe Right or Left to skip</p>" + "<span class=\"moments\">"+(sequenceData+1)+"/"+data.length+"</span></span>" + "<div class=\"content-data\"><div style=\"clear:both;\">Service No. <span class=\"pull-right text-right\">" +data[sequenceData]["no_task"]+ "</span></div><div class=\"clear\">Subject <span class=\"pull-right\">" +data[sequenceData]["name"]+ "</span></div><div class=\"clear\">Appointment <span class=\"pull-right\">"+data[sequenceData]["appointment"]+"</span></div><hr/><div>Engineer</div><div></div><div class=\"clear\"><span class=\"pull-right\"><img class=\"pull-right media-object\" src=\""+data[sequenceData]["pic"]+"\" /><div class=\"clear\">"+data[sequenceData]["engineer_thainame"]+"</div></span></div><div class=\"clear\"></div><hr/><div class=\"end_user_signed\" data-tasks-sid=\""+data[sequenceData]["tasks_sid"]+"\"><div>End User <span class=\"pull-right\"><i style=\"font-size: 0.8em;\" class=\"fa fa-fw fa-chevron-right\"></i></span></div><div>Name <span class=\"pull-right end_user_contact_name_service_report\">" +data[sequenceData]["end_user_contact_name_service_report"]+ "</span></div><div>Email <span class=\"pull-right end_user_email_service_report\">" +data[sequenceData]["end_user_email_service_report"]+ "</span></div><div>Mobile <span class=\"pull-right end_user_mobile_service_report\">" +data[sequenceData]["end_user_mobile_service_report"]+ "</span></div><div>Phone <span class=\"pull-right end_user_phone_service_report\">" +data[sequenceData]["end_user_phone_service_report"]+ "</span></div><div>Company <span class=\"pull-right end_user_company_name_service_report\">" +data[sequenceData]["end_user_company_name_service_report"]+ "</span></div></div><hr/><div>Case No. <span class=\"pull-right\">"+data[sequenceData]["no_ticket"]+"</span></div><div class=\"clear\">Subject <span class=\"pull-right\">"+data[sequenceData]["ticket_subject"]+"</span></div><hr/></div></div>");
                      swipe();
                    }else{
                      $("div.content-object").prepend("<div class=\"photo\" id=\"photo\" ><div class=\"content-data\"><div style=\"text-align:center;\">No Data Unsigned Service Report</div></div></div>");
                      $(".UnsignedServiceViewApp .footer").css("display","none");
                    }
                }
                $(".UnsignedServiceViewApp").on("click",".end_user_signed",function(){
                    var tasks_sid = $(this).data("tasks-sid");
                    getDataTaskDetail(tasks_sid).done(function(res){
                        $("#FormEndUserView #company").val(res.task_detail.end_user_company_name_service_report);
                        $("#FormEndUserView #name").val(res.task_detail.end_user_contact_name_service_report);
                        $("#FormEndUserView #email").val(res.task_detail.end_user_email_service_report);
                        $("#FormEndUserView #mobile").val(res.task_detail.end_user_mobile_service_report);
                        $("#FormEndUserView #phone").val(res.task_detail.end_user_phone_service_report);
                    });
                    $("#FormEndUserView").show();
                    $(".UnsignedServiceViewApp .backToMoreSystem").hide();
                    $(".app").addClass("bounceOutLeft animated");
                    $(".app").hide();
                    $("#FormEndUserView").addClass("bounceInRight animated");
                });

                $(".UnsignedServiceViewApp").on("click","#signnow",function(){
                    getDataTaskDetail(currentTaskSid).done(function(res){
                      console.log(res.task_detail);
                      serviceNo = res.task_detail.no_task;
                      var $actionCurrent =  $("<div>");
                      $actionCurrent.html(res.task_detail.input_action.solution);
                      $(".UnsignedServiceViewApp #signature-pad #action_input").html($actionCurrent);
                      $(".UnsignedServiceViewApp .backToMoreSystem").hide();
                      $(".UnsignedServiceViewApp #signature-pad").show();                      
                      $(".UnsignedServiceViewApp .app").hide();                 
                      $("head").append("<script src=\"http://vspace.in.th/pages/signaturepad/customerApp/js/signature_pad.js\"><\/script><script src=\"http://vspace.in.th/pages/signaturepad/customerApp/js/appSign.js\"><\/script>");
                    });
                });

                var getDataTaskDetail = function(tasks_sid){
                  return $.ajax({
                      type: "POST", url: END_POINT_2+"v1/task_detail/"+tasks_sid, data: {email: localStorage.getItem("case_email"),token:localStorage.getItem("case_token")}
                  });
                }
                $(".UnsignedServiceViewApp").on("click","#BackToMain2",function(){
                  backToMainUnsigned();
                });
                $(".UnsignedServiceViewApp").on("click","#BackToMain",function(){
                  backToMainUnsigned();
                });
                $(".UnsignedServiceViewApp").on("click","#SignpadForm",function(){
                  openSignpadForm();
                });
                var openSignpadForm = function(){
                    $("#before_signature").hide();
                    $("#signature-pad").show();
                    $("#signature-pad").attr("class","animated bounceInRight");
                }
                var backToMainUnsigned = function(){
                    $("#FormEndUserView").hide();
                    
                    $(".UnsignedServiceViewApp .app").show();
                    $(".UnsignedServiceViewApp #before_signature").hide();
                    $(".UnsignedServiceViewApp #signature-pad").hide();
                    $(".UnsignedServiceViewApp .backToMoreSystem").show();
                    
                    $(".UnsignedServiceViewApp .footer").attr("class","footer animated bounceInRight");

                    $(".UnsignedServiceViewApp .app").attr("class","app");
                    $(".UnsignedServiceViewApp .app").show();
                    // $(".UnsignedServiceViewApp .app").addClass("bounceInLeft animated");
                    $(".UnsignedServiceViewApp .footer").attr("class","footer animated bounceInLeft");
                }
                $(".UnsignedServiceViewApp").on("click","#btnChangeEnduser",function(){
                    var enduser = {name:"",email:"",phone:"",mobile:"",company:""};
                    enduser.company = $("#FormEndUserView #company").val();
                    enduser.name = $("#FormEndUserView #name").val();
                    enduser.email = $("#FormEndUserView #email").val();
                    enduser.mobile = $("#FormEndUserView #mobile").val();
                    enduser.phone = $("#FormEndUserView #phone").val();

                    $(".end_user_signed .end_user_company_name_service_report").html(enduser.company);
                    $(".end_user_contact_name_service_report").html(enduser.name);
                    $(".end_user_email_service_report").html(enduser.email);
                    $(".end_user_mobile_service_report").html(enduser.mobile);
                    $(".end_user_phone_service_report").html(enduser.phone);
                    changeEndUser(enduser).done(function(res){
                        console.log(res);
                        backToMainUnsigned();
                    });
                });
                var changeEndUser = function(enduser){
                  return $.ajax({
                      type: "POST", url: END_POINT_2+"v1/enduser/ChangeEndUser", data: {tasks_sid:currentTaskSid,email: localStorage.getItem("case_email"),token:localStorage.getItem("case_token"), data:enduser}
                  });
                  
                }
            </script>';
    // $html .= MoreSystemView::bottomBar();
    return $html;
	}
}
?>