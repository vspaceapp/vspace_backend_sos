<?php class CaseManagementCreateView{

	public static $caseType;
	public static function controlSider($caseType){
    	$html = '<style>
    			#case-control {}
		    	.control-sidebar {
				    padding-top: 0px;
				    width: 100%;
				    height: 100%;
				    position: absolute;
				    top: 0;
				    bottom: 0;
				    left: 0;
				    right: 0;
				}
				.control-sidebar.open {
					z-index: 100;
				    position: absolute;
				    //background-color: white;
				    right: 0;width: 100%;height: 100%;
				    border-radius: 10px 10px 10px 10px;
				    display:block;
				    overflow:hidden;
				}
				.create-case-form {
				    position: absolute;
				    overflow-y: scroll;
				    overflow-x: hidden;
				    left: 20px;
				    right: 10px;
				    bottom: 0;
				    top: 30px;
				}
				.control-sidebar-menu .menu-info{
                  margin-left: 0px;
                }
				//#projectCaseForm .control-sidebar-menu .menu-info { margin-left:0px;}
		    	</style>';
    	$html .= '<div id="case-control"><div class="control-sidebar control-sidebar-dark hide" >';
    	$html .= CaseManagementCreateView::view($caseType);
    	$html .= '</div></div>';
    	$html .= '<script>
    			$(function(){
    				var CreateControl = function(){
    					var data = {
    						contract_no:"",project_owner_sid:"",
    						subject:"",detail:"",case_type:"",enduser_case:"",enduser_address:"",urgency:"",
    						requester: {name:"",email:"",mobile:"",phone:"",company:""},
    						enduser: {name:"",email:"",mobile:"",phone:"",company:""},
    						owner:{thainame:"",email:"",mobile:"",pic:""}
    					}
    					this.getData = function(){
    						return data;
    					}
    					this.setProjectOwnerSid = function(project_owner_sid){
    						data.project_owner_sid = project_owner_sid;
    					}
    					this.setUrgency = function(urgency){
    						data.urgency = urgency;
    					}
    					this.setRequester = function(requester){
    						data.requester = requester;
    					}
    					this.setContactUser = function(contact_user){
    						data.enduser = contact_user;
    					}
    					this.setEndUser = function(end_user){
    						data.enduser_case = end_user;
    					}
    					this.setEndUserSite = function(end_user_site){
    						data.enduser_address = end_user_site;
    					}
    					this.setType = function(type){
    						data.case_type = type;
    					}
    					this.setSubject = function(subject){
    						data.subject = subject;
    					}
    					this.setDetail = function(detail){
    						data.detail = detail;
    					}
    					this.setOwner = function(owner){
    						data.owner = owner;
    					}
    					this.setContractNo = function(contract_no){
    						data.contract_no = contract_no;
    					}
	    				this.loadCaseType = function(){
	    					return $.ajax({
	    						type: "POST",url: END_POINT_2+"v1/case/listCaseType",
	    						data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token")},
	    					});
	    				}
	    				this.loadProject = function(project_type){
	    					return $.ajax({
	    						type: "POST",url:END_POINT_2+"v1/implement/project",
	    						data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),project_type:project_type},
	    					});
	    				}
	    				this.loadRequesterEndUser = function(contract_no){
		    				return $.ajax({
		    					type: "POST",url:END_POINT_2+"v1/enduser/information",
		    					data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),contract_no:contract_no}
		    				});
		    			}
		    			this.loadUserCanAssignTask = function(contract_no){
		    				return $.ajax({
		    					type:"POST",url:END_POINT_2+"v1/user/listUserCanAddTask",
		    					data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),contract_no:contract_no,department:"sss"}
		    				});
		    			}
		    			this.findContractInfo = function(contract_no){
		    				return $.ajax({
		    					type:"POST",url:END_POINT_2+"v1/contract/find_contract_info",
		    					data:{email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),contract:contract_no}
		    				});
		    			}
	    			}
    				var createControl = new  CreateControl();
	    			$("#CreateCase").click(function(){
	    				$("#case-control").find(".control-sidebar").attr("class","control-sidebar control-sidebar-dark open animated bounceInRight");
	    				$("#case-control").find(".create-case-form").addClass("hide");
	    				$("#case-control").find("#caseTypeCaseForm").attr("class","create-case-form animated bounceInRight");
	    				$("#case-control #contractCaseForm ul").html("");
	    				$("#case-control #search_contract").val("");
	    			});
	    			$("#case-control").on("click","#closeCreateCase",function(){
	    				$("#case-control").find(".control-sidebar").attr("class","control-sidebar control-sidebar-dark hide");
	    			});
	    			$("#case-control").on("click","#caseTypeCaseForm .optionalList",function(){
	    				console.log(this);
	    				var caseType = $(this).attr("data-type");
	    				var caseTypeIsProject = $(this).attr("data-project");
	    				createControl.setType(caseType);
	    				if(caseTypeIsProject=="project"){
							createControl.loadProject(caseType).done(function(res){
								$elm = $("<div>");
								// if(caseType=="Implement"){
								// 	var imgIcon = "implement.png";
								// }else if(caseType=="Install"){
								// 	var imgIcon = "install.png";
								// }else if(caseType=="POC"){
								// 	var imgIcon = "7.png";
								// }else{
								// 	var imgIcon = "project-management.png";
								// }

								$.each(res.data, function(k,v){
									$elm.append("<li><a class=\"optionalList\" data-project-owner-sid=\""+v.project_owner_sid+"\" data-enduser-name=\""+v.end_user+"\" data-enduser-address=\""+v.end_user_address+"\" data-contract=\""+v.contract+"\" href=\"javascript:void(0);\"><div class=\"menu-info\">"+v.contract+"<br/>"+v.name+"<div><span>Man Days / Man Hours:</span> <span class=\"pull-right\">"+v.man_days+" Days / "+v.man_hours+" Hrs.</span></div><div><span>Project Period:</span> <span class=\"pull-right\">"+v.project_start+" To "+v.project_end+"</span></div></div><div class=\"menu-info\"><span>End User:</span> <span class=\"pull-right\">"+v.end_user+"</span></div><div class=\"menu-info\"><span>End User Address:</span> <span class=\"pull-right\">"+v.end_user_address+"</span></div></a></li>");
								});
								$("#case-control #ProjectList").html($elm.html());
								$("#projectCaseForm").attr("class","create-case-form animated bounceInRight");
								$("#caseTypeCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
							});
	    				}else{
							$("#contractCaseForm").attr("class","create-case-form animated bounceInRight");
	    					$("#caseTypeCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
							// subjectCaseForm
	    				}
	    			});
	    			$("#case-control").on("click","#projectCaseForm .optionalList",function(){
	    				contract_no = $(this).data("contract");
	    				createControl.setContractNo(contract_no);
	    				createControl.setEndUser($(this).data("enduser-name"));
	    				createControl.setEndUserSite($(this).data("enduser-address"));
	    				createControl.setProjectOwnerSid($(this).data("project-owner-sid"));
	    				console.log(createControl.getData());
	    				openSubjectForm();
	    			});
	    			function openSubjectForm(){
	    				$(".create-case-form").addClass("hide");
	    				$("#case-control #subjectCaseForm").attr("class","create-case-form animated bounceInRight");
	    			}
	    			$("#case-control").on("click",".nextToEndRequester",function(){
	    				var subject = $("#subject_case").val();
	    				console.log(subject.length);
	    				if(subject.length>3){
		    				console.log($("#subject_case").val());
		    				console.log($("#detail_case").val());
		    				createControl.setUrgency($(this).data("urgency"));
		    				createControl.setSubject($("#subject_case").val());
		    				createControl.setDetail($("#detail_case").val());
		    				$("#case-control #subjectCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
		    				$("#case-control #requesterCaseForm").attr("class","create-case-form animated bounceInRight");
		    				var dataC = createControl.getData();
		    				console.log(dataC);
		    				createControl.loadRequesterEndUser(dataC.contract_no).done(function(res){
		    					$("#requesterList ul").html("");
		    					$.each(res.data,function(k,v){
		    						$("#requesterList ul").append("<li class=\"table-view-cell\"><a href=\"javascript:void(0);\" data-name=\""+v.name+"\" data-email=\""+v.email+"\" data-mobile=\""+v.mobile+"\" data-phone=\""+v.phone+"\" data-company=\""+v.company+"\" class=\"navigate-right request-click \"><div><p>Name <span class=\"pull-right\">"+v.name+"</span></p><p>Email <span class=\"pull-right\">"+v.email+"</span></p><p>Mobile <span class=\"pull-right\">"+v.mobile+"</span></p><p>Phone <span class=\"pull-right\">"+v.phone+"</span></p><p>Company <span class=\"pull-right\">"+v.company+"</span></p></div></a></li>");
		    					});
		    					var txtAddNew = "Add New Requester";
	    						$("#requesterList ul").append("<li class=\"table-view-cell\"><a class=\"navigate-right\" id=\"AddRequester\" href=\"javascript:void(0);\"><div><p>"+txtAddNew+"</p></div></a></li>");
		    				});
		    			}else{
		    				$("#subject_case").focus();
		    			}
	    			});
	    			$("#case-control").on("click",".request-click",function(){
	    				var tmp = {name:"",email:"",mobile:"",phone:"",company:""};
	    				tmp.name = $(this).data("name");
	    				tmp.email = $(this).data("email");
	    				tmp.mobile = $(this).data("mobile");
	    				tmp.phone = $(this).data("phone");
	    				tmp.company = $(this).data("company");
	    				createControl.setRequester(tmp);
	    				openFormEndUser();
	    			});
	    			function openFormEndUser(){
	    				var dataC = createControl.getData();
	    				console.log(dataC);
	    				$("#case-control #requesterCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
	    				$("#case-control #addRequesterForm").attr("class","create-case-form animated bounceOutLeft hide");
	    				$("#case-control #endUserCaseForm").attr("class","create-case-form animated bounceInRight");

	    				createControl.loadRequesterEndUser(dataC.contract_no).done(function(res){
	    					$("#contactUserList ul").html("");
	    					$.each(res.data,function(k,v){
	    						$("#contactUserList ul").append("<li class=\"table-view-cell\"><a href=\"javascript:void(0);\" data-name=\""+v.name+"\" data-email=\""+v.email+"\" data-mobile=\""+v.mobile+"\" data-phone=\""+v.phone+"\" data-company=\""+v.company+"\" class=\"navigate-right contact-user-click \"><div><p>Name <span class=\"pull-right\">"+v.name+"</span></p><p>Email <span class=\"pull-right\">"+v.email+"</span></p><p>Mobile <span class=\"pull-right\">"+v.mobile+"</span></p><p>Phone <span class=\"pull-right\">"+v.phone+"</span></p><p>Company <span class=\"pull-right\">"+v.company+"</span></p></div></a></li>");
	    						if((k+1)==res.data.length){

	    						}
	    					});
	    					var txtAddNew = "Add New Contact User";
	    					$("#contactUserList ul").append("<li class=\"table-view-cell\"><a class=\"navigate-right\" id=\"AddEndUser\" href=\"javascript:void(0);\"><div><p>"+txtAddNew+"</p></div></a></li>");
	    				});
	    			}
	    			$("#case-control").on("click",".contact-user-click",function(){
	    				var tmp = {name:"",email:"",mobile:"",phone:"",company:""};
	    				tmp.name = $(this).data("name");
	    				tmp.email = $(this).data("email");
	    				tmp.mobile = $(this).data("mobile");
	    				tmp.phone = $(this).data("phone");
	    				tmp.company = $(this).data("company");
	    				createControl.setContactUser(tmp);
	    				openFormOwner();
	    			});
	    			function openFormOwner(){
	    				var dataC = createControl.getData();
	    				console.log(dataC);
	    				$("#case-control #endUserCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
	    				$("#case-control #addEndUserForm").attr("class","create-case-form animated bounceOutLeft hide");
	    				$("#case-control #ownerCaseForm").attr("class","create-case-form animated bounceInRight");
	    				createControl.loadUserCanAssignTask(dataC.contract_no).done(function(res){
	    					$("#ownerCaseForm ul").html("");
	    					$.each(res.data,function(k,v){
	    						$("#ownerCaseForm ul").append("<li class=\"table-view-cell\"><a data-email=\""+v.emailaddr+"\" data-thainame=\""+v.thainame+"\"  data-mobile=\""+v.mobile+"\" data-pic=\""+v.pic_employee+"\" href=\"javascript:void(0);\" class=\"navigate-right owner-click\"><img class=\"media-object pull-left\" src=\""+v.pic_employee+"\"><div class=\"media-body\"><p>"+v.thainame+"</p><p>"+v.emailaddr+"</p><p>"+v.mobile+"</p></div></a></li>");
	    					});
	    				});
	    			}
	    			$("#case-control").on("click",".owner-click", function(){
	    				var tmp = {thainame:"",email:"",mobile:"",pic:""};
	    				tmp.thainame = $(this).data("thainame");
	    				tmp.email = $(this).data("email");
	    				tmp.mobile = $(this).data("mobile");
	    				tmp.pic = $(this).data("pic");
	    				createControl.setOwner(tmp);
	    				$("#case-control #ownerCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
						verifyEndUserAndAddress();
	    			});
	    			function verifyEndUserAndAddress(){
	    				var dataC = createControl.getData();
						if(dataC.enduser_case && dataC.enduser_address){
							summaryCreateCaseOpen();
						}else{
							$("#case-control #caseInfoOtherForm").attr("class","create-case-form animated bounceInRight");
							$("#caseInfoOtherForm #end_user_company_name").val(dataC.enduser_case);
							$("#caseInfoOtherForm #end_user_address").val(dataC.enduser_address);
						}
	    			}
	    			$("#case-control").on("click","#doneCaseInfoOtherForm",function(){
	    				var end_user_company_name = $("#caseInfoOtherForm #end_user_company_name").val();
	    				var end_user_address = $("#caseInfoOtherForm #end_user_address").val();

	    				if(end_user_company_name.length>2 && end_user_address.length>2){
		    				createControl.setEndUser(end_user_company_name);
	    					createControl.setEndUserSite(end_user_address);
	    					verifyEndUserAndAddress();
	    					$("#case-control #caseInfoOtherForm").attr("class","create-case-form animated bounceOutLeft");
	    				}else{
	    					if(end_user_company_name.length<=2){
		    					$("#caseInfoOtherForm #end_user_company_name").focus();
		    				}
		    				if(end_user_address.length<=2){
	    						$("#caseInfoOtherForm #end_user_address").focus();		    					
		    				}
	    				}
	    			});

	    			function summaryCreateCaseOpen(){
	    				var dataC = createControl.getData();
	    				console.log(dataC);
	    				var v = dataC;
	    				$("#case-control #summaryCaseForm ul").html("");
	    				$("#case-control #summaryCaseForm").attr("class","create-case-form animated bounceInRight");
	    				$("#case-control #summaryCaseForm ul").append("<div class=\"card\" style=\"background-color:initial;\"><li class=\"table-view-cell cell-block\"><div><p>Type <span class=\"pull-right\">"+v.case_type+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Urgency <span class=\"pull-right\">"+v.urgency+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Contract <span class=\"pull-right\">"+v.contract_no+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>End User<span class=\"pull-right\">"+v.enduser_case+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Address <span class=\"pull-right\">"+v.enduser_address+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Subject <span class=\"pull-right\">"+v.subject+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Detail <span class=\"pull-right\">"+v.detail+"</span></p></div><li></div>");

	    				$("#case-control #summaryCaseForm ul").append("<div class=\"card\" style=\"background-color:initial;\"><li class=\"table-view-divider cell-block\"><div><p>Requester</p></div><li><li class=\"table-view-cell cell-block\"><div><p>Name <span class=\"pull-right\">"+v.requester.name+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Email <span class=\"pull-right\">"+v.requester.email+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Mobile <span class=\"pull-right\">"+v.requester.mobile+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Phone <span class=\"pull-right\">"+v.requester.phone+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Company <span class=\"pull-right\">"+v.requester.company+"</span></p></div><li></div>");

	    				$("#case-control #summaryCaseForm ul").append("<div class=\"card\" style=\"background-color:initial;\"><li class=\"table-view-divider cell-block\"><div><p>Contact User</p></div><li><li class=\"table-view-cell cell-block\"><div><p>Name <span class=\"pull-right\">"+v.enduser.name+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Email <span class=\"pull-right\">"+v.enduser.email+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Mobile <span class=\"pull-right\">"+v.enduser.mobile+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Phone <span class=\"pull-right\">"+v.enduser.phone+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Company <span class=\"pull-right\">"+v.enduser.company+"</span></p></div><li></div>");

	    				$("#case-control #summaryCaseForm ul").append("<div class=\"card\" style=\"background-color:initial;\"><li class=\"table-view-divider cell-block\"><div><p>Owner</p></div><li><li class=\"table-view-cell cell-block\"><img class=\"media-object\" src=\""+v.owner.pic+"\" /><div><p> <span class=\"pull-right\">"+v.owner.thainame+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Email <span class=\"pull-right\">"+v.owner.email+"</span></p></div><li><li class=\"table-view-cell cell-block\"><div><p>Mobile <span class=\"pull-right\">"+v.owner.mobile+"</span></p></div><li></div>");

	    				$("#case-control #summaryCaseForm ul").append("<div class=\"\" style=\"background-color:initial;\"><li class=\"table-view-divider cell-block\"><div><p><button type=\"button\" class=\"btn btn-block btn-primary\" id=\"btnDoneCreateCase\">Done</button></p></div><li></div>");
	    			}
	    			$("#case-control").on("click","#btnDoneCreateCase", function(){
	    				var dataC = createControl.getData();
	    				console.log(dataC);
	    				sendCreateCaseToServer(dataC).done(function(res){
	    					console.log(res);
	    					if(!res.error){
	    						var rd = 0;
	    						rd += Math.floor((Math.random() * 1000) + 1);
	    						location.reload();
	    					}
	    				});
	    			});
	    			function sendCreateCaseToServer(dataC){
	    				return $.ajax({
	    					type: "POST",url: END_POINT_2+"v1/casemanagement/createCases",
	    					data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),storage:dataC},
	    					success:function(res){
	    						
	    					}
	    				});
	    			}
	    			$("#case-control").on("click","#AddRequester",function(){
						$("#case-control #requesterCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
	    				$("#case-control #addRequesterForm").attr("class","create-case-form animated bounceInRight");
	    			});
	    			$("#case-control").on("click","#AddEndUser",function(){
						$("#case-control #endUserCaseForm").attr("class","create-case-form animated bounceOutLeft hide");
	    				$("#case-control #addEndUserForm").attr("class","create-case-form animated bounceInRight");
	    			});

	    			$("#case-control").on("click","#BackToListRequester", function(){
	    				$("#case-control #addRequesterForm").attr("class","create-case-form animated bounceOutRight hide");
	    				$("#case-control #requesterCaseForm").attr("class","create-case-form animated bounceInLeft");
	    			});
	    			$("#case-control").on("click","#BackToListContactUser", function(){
	    				$("#case-control #endUserCaseForm").attr("class","create-case-form animated bounceInLeft");
	    				$("#case-control #addEndUserForm").attr("class","create-case-form animated bounceOutRight hide");
	    			});
	    			$("#case-control").on("click","#done_requester", function(){
	    				var tmp = {name:"",email:"",mobile:"",phone:"",company:""};
	    				var $elem = $(this).closest("ul");
	    				tmp.name = $elem.find(".name").val();
	    				tmp.email = $elem.find(".email").val();
	    				tmp.mobile = $elem.find(".mobile").val();
	    				tmp.phone = $elem.find(".phone").val();
	    				tmp.company = $elem.find(".company").val();
	    				createControl.setRequester(tmp);
	    				openFormEndUser();
	    			});
	    			$("#case-control").on("click","#done_end_user", function(){
	    				var tmp = {name:"",email:"",mobile:"",phone:"",company:""};
	    				var $elem = $(this).closest("ul");
	    				tmp.name = $elem.find(".name").val();
	    				tmp.email = $elem.find(".email").val();
	    				tmp.mobile = $elem.find(".mobile").val();
	    				tmp.phone = $elem.find(".phone").val();
	    				tmp.company = $elem.find(".company").val();
	    				createControl.setContactUser(tmp);
	    				openFormOwner();
	    			});
	    			$("#case-control").on("change","#search_contract",function(){
		    			$("#contractCaseForm ul").html("<li class=\"table-view-cell\"><div><p>Finding Contract...</p></div></li>");
	    				$("#contractCaseForm ul.control-sidebar-menu").attr("class","control-sidebar-menu animated bounceInRight");
	    				createControl.findContractInfo($(this).val()).done(function(res){
	    					$("#contractCaseForm ul.control-sidebar-menu").attr("class","control-sidebar-menu animated bounceOutLeft");
	    					$("#contractCaseForm ul").html("");
	    					$elem = $("<div>");
	    					if(res.data.length>0){
		    					$.each(res.data,function(k,v){
		    						console.log(res.data);
									$elem.append("<li><a class=\"optionalContract\" data-end_user_name=\""+v.ENDUSER_NAME+"\" data-end_user_address=\""+v.ENDUSER_ADDRESS+"\" data-project_name=\""+v.PROJECT_NAME+"\" data-contract_no=\""+v.CONTRACT_NO+"\" href=\"javascript:void(0);\"><div class=\"menu-info\" style=\"margin-left:0px;\"><h4 class=\"control-sidebar-subheading\">"+v.CONTRACT_NO+"</h4><p>Project Name <span class=\"pull-right\">"+v.PROJECT_NAME+"</span></p><p>End User Name <span class=\"pull-right\">"+v.ENDUSER_NAME+"</span></p><p>End User Address <span class=\"pull-right\">"+v.ENDUSER_ADDRESS+"</span></p></div></a></li>");
									if(res.data.length==(k+1)){
										console.log($elem);
										$("#contractCaseForm ul").html($elem.html());
										$("#contractCaseForm ul.control-sidebar-menu").attr("class","animated control-sidebar-menu slideInRight");
									}
		    					});
		    				}else{
		    					$elem.append("<li class=\"table-view-cell\"><div><p>Not Found</p></div></li>");
		    					$("#contractCaseForm ul").html($elem.html());
		    					$("#contractCaseForm ul.control-sidebar-menu").attr("class","animated control-sidebar-menu slideInRight");
		    				}
	    					console.log(res);	    					
	    				});
	    			});
	    			$("#case-control #contractCaseForm").on("click",".optionalContract",function(){
	    				var end_user_name = $(this).data("end_user_name");
	    				var end_user_address = $(this).data("end_user_address");
	    				var project_name = $(this).data("project_name");
	    				var contract_no = $(this).data("contract_no");
	    				createControl.setEndUser(end_user_name);
						createControl.setEndUserSite(end_user_address);
						createControl.setContractNo(contract_no);
						openSubjectForm();
	    			});
	    		});
    			</script>';
    	return $html;
    }

	public static function view($caseType){
		self::$caseType = $caseType;
		$html = '';
		$html .= '<div class="box-tools pull-right" style="margin-right: 10px;">
                    <button type="button" class="btn btn-box-tool" id="closeCreateCase" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><div style="margin: 30px 15px;">'.self::caseTypeForm().''.self::contractForm().''.self::projectForm().''.self::subjectForm().''.self::requesterCaseForm().''.self::endUserCaseForm().''.self::ownerCaseForm().''.self::summaryCaseForm().''.self::addRequesterForm().''.self::addEndUserForm().' '.self::caseInfoOtherForm().'</div>';
		return $html;
	}

	public static function caseTypeForm(){
		$html = '<div id="caseTypeCaseForm" class="create-case-form ">
			<div><i class="fa fa-fw fa-list-ul" style="font-size:1em;"></i> Select Type</div>
			<ul class="control-sidebar-menu">';
			// <img class="media-object pull-left" src="'.$value['img'].'">
		foreach (self::$caseType as $key => $value) {
			$html .= '<li>
			            <a href="javascript:void(0)" class="optionalList" data-type="'.$value['name'].'" data-project="'.$value['project'].'">
			              <div class="menu-info">
			                <h4 class="control-sidebar-subheading">'.$value['name'].'</h4>
			                <p>'.$value['detail'].'</p>
			              </div>
			            </a>
			          </li>';
		}
		$html .= '</ul></div>';
		return $html;
	}

	public static function contractForm(){
		$html = '<div id="contractCaseForm" class="create-case-form hide">
			<div><i class="fa fa-fw fa-check-square-o" style="font-size:1em;"></i> Find Contract</div>
			<div><input type="search" id="search_contract" placeholder="Input Contract" class="form-control" /></div>
			<ul class="control-sidebar-menu">';
		
		$html .= '</ul></div>';
		return $html;
	}

	public static function projectForm(){
		$html = '<div id="projectCaseForm" class="create-case-form hide">
			<div style=""><i class="fa fa-fw fa-map-signs" style="font-size:1em;"></i> Select Project</div>
			<ul class="control-sidebar-menu" id="ProjectList">';
		
		$html .= '</ul></div>';
		return $html;
	}

	public static function subjectForm(){
		$html = '<div id="subjectCaseForm" class="create-case-form hide">
			<div><i class="fa fa-fw fa-info-circle"></i> Case Information</div>
			<div class="card" style="background-color:initial;"><ul class="table-view">
			<li class="table-view-cell" style="background:none;"><div><i class="fa fa-fw fa-pencil-square" style="font-size:1em;"></i> Subject</div></li>
			<li class="table-view-cell cell-block" style="background:none;"><div><textarea type="text" id="subject_case" ></textarea></div></li>
			<li class="table-view-cell" style="background:none;"><div><i class="fa fa-fw fa-align-justify" style="font-size:1em;"></i> Detail</div></li>
			<li class="table-view-cell cell-block" style="background:none;"><div><textarea type="text" id="detail_case" ></textarea></div></li>
			</ul></div>
			<div class="card" style="background-color:initial;"><ul class="table-view">
					<li class="table-view-divider"><div><p><i class="fa fa-fw fa-clock-o" style="font-size:1em;"></i> Urgency</p></div></li>
					<li class="table-view-cell"><a class="nextToEndRequester navigate-right" data-urgency="Low" href="javascript:void(0);"><div><i class="pull-left fa fa-fw fa-hourglass-2 text-green" style="font-size:1em;"></i><p>Low</p></div></a></li>
					<li class="table-view-cell"><a class="nextToEndRequester navigate-right" data-urgency="Medium" href="javascript:void(0);"><div><i class="pull-left fa fa-fw fa-hourglass-2 text-yellow" style="font-size:1em;"></i><p>Medium</p></div></a></li>
					<li class="table-view-cell"><a class="nextToEndRequester navigate-right" data-urgency="High" href="javascript:void(0);"><div><i class="pull-left fa fa-fw fa-hourglass-2 text-orange" style="font-size:1em;"></i><p>High</p></div></a></li>
					<li class="table-view-cell"><a class="nextToEndRequester navigate-right" data-urgency="Critical" href="javascript:void(0);"><div><i class="pull-left fa fa-fw fa-hourglass-2 text-red" style="font-size:1em;"></i><p>Critical</p></div></a></li>
				</ul>
			</div>';
		$html .= '</div>';
		return $html;
	}

	public static function caseInfoOtherForm(){
		$html = '<div id="caseInfoOtherForm" class="create-case-form hide">
			<div><i class="fa fa-fw fa-info-circle"></i> Case Information</div>
			<div class="card" style="background-color:initial;"><ul class="table-view">
			<li class="table-view-cell" style="background:none;"><div><i class="fa fa-fw fa-pencil-square" style="font-size:1em;"></i> End User Company Name</div></li>
			<li class="table-view-cell cell-block" style="background:none;"><div><textarea type="text" id="end_user_company_name" ></textarea></div></li>
			<li class="table-view-cell" style="background:none;"><div><i class="fa fa-fw fa-align-justify" style="font-size:1em;"></i> End User Address</div></li>
			<li class="table-view-cell cell-block" style="background:none;"><div><textarea type="text" id="end_user_address" ></textarea></div></li>
			</ul></div>
			<div><button type="button" class="btn btn-block btn-primary" id="doneCaseInfoOtherForm">Next</button></div>
			';
		$html .= '</div>';
		return $html;
	}

	public static function requesterCaseForm(){
		$html = '<div id="requesterCaseForm" class="create-case-form hide">
			<div><i class="fa fa-fw fa-street-view" style="font-size:1em;"></i> Select Requester </a></div>
			<div id="requesterList" class=""><ul class="table-view"></ul></div>';
		$html .= '</div>';
		return $html;
	}

	public static function endUserCaseForm(){
		$html = '<div id="endUserCaseForm" class="create-case-form hide">
			<div><i class="fa fa-fw fa-user" style="font-size:1em;"></i> Select Contact User</div>
			<div id="contactUserList"><ul class="table-view"></ul></div>';
		$html .= '</div>';
		return $html;
	}

	public static function ownerCaseForm(){
		$html = '<div id="ownerCaseForm" class="create-case-form hide">
			<div>Select Owner</div>
			<div id="ownerList"><ul class="table-view"></ul></div>';
		$html .= '</div>';
		return $html;
	}
	public static function summaryCaseForm(){
		$html = '<div id="summaryCaseForm" class="create-case-form hide">
			<div style="margin:0px 5px;"><i style="font-size:1em;" class="fa fw fa-edit"></i> Confirm Create Case</div>
			<div class="summaryCaseForm"><ul class="table-view table-view-summary"></ul></div>';
		$html .= '</div>';
		return $html;
	}

	public static function addRequesterForm(){
		$html = '<div id="addRequesterForm" class="create-case-form hide">';
		$html .= '<div><a href="javascript:void(0);" id="BackToListRequester" ><span class="icon icon-left-nav pull-left"></span></a></div><div><i class="fa fa-fw fa-street-view" style="font-size:1em;"></i> Add New Requester</div>';
		$html .= '<ul class="table-view requester">
                    <li class="table-view-divider">Name</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="name form-control"></div></li>
                    <li class="table-view-divider">Email</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="email form-control"></div></li>
                    <li class="table-view-divider">Phone</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="phone form-control"></div></li>
                    <li class="table-view-divider">Mobile</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="mobile form-control"></div></li>
                    <li class="table-view-divider">Company</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="company form-control"></div></li>
                    <li class="table-view-cell" style="padding-right:15px">
                      <div><a href="javascript:void(0);" id="done_requester" class="btn btn-block btn-primary">Next</a></div>
                    </li>
                  </ul></div>';
        return $html;
	}

	public static function addEndUserForm(){
		$html = '<div id="addEndUserForm" class="create-case-form hide">';
		$html .= '<div><a href="javascript:void(0);" id="BackToListContactUser"><span class="icon icon-left-nav pull-left"></span></a></div><div><i class="fa fa-fw fa-user" style="font-size:1em;"></i> Add New Contact User</div>';
		$html .= '<ul class="table-view contact_user" >
                    <li class="table-view-divider">Name</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text"  class="name form-control"></div></li>
                    <li class="table-view-divider">Email</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="email form-control"></div></li>
                    <li class="table-view-divider">Phone</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="phone form-control"></div></li>
                    <li class="table-view-divider">Mobile</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="mobile form-control"></div></li>
                    <li class="table-view-divider">Company</li>
                    <li class="table-view-cell" style="padding-right:15px"><div><input type="text" class="company form-control"></div></li>
                    <li class="table-view-cell" style="padding-right:15px">
                      <div><a href="javascript:void(0);" id="done_end_user" class="btn btn-block btn-primary">Next</a></div>
                    </li>
                  </ul></div>';
        return $html;
	}
	
}
?>