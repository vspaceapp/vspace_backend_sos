<?php class MoreSystemView{
    private $email;
    private $data;

    private $month = array('January','February','March','April','May','June','July','August','September','October','November','December');
    private $dataDate;
    public function __construct($email=""){
        $this->email = $email;
    }
    function loadTemplateNewVSpace(){
      $html = '<script src="http://vspace.in.th/react-vspace/static/js/main.375d1327.js"></script>
      <script>
        if($(".page").length>1){
          $(".page:eq(0)").remove();
        }
      </script>';
      $html .= '<div id="root"></div><style>body{position:static;background-color:#FFFFFF;color:#222222;} .page.center {-webkit-transform: initial; transform: initial;}</style>';
      return $html;
    }
    function templateStandbyView($email, $dataDateYear, $buddy, $permission_role = ""){
        require_once '../../view/StandbyView.php';

        $this->dataDate = $dataDateYear;
        $html = '';
        $html .= '<div class="object content">
                        <div class="box-header with-border">
                          <h3 class="box-title"><i class="fa fa-fw fa-calendar"></i> Calendar Standby</h3>
                          '.StandbyView::toolbar($permission_role).'
                        </div>
                    <div class="row" style="overflow:auto;">
                    <div class="col-xs-12"> 
                    <div class="standby"><div style="padding: 0 15px;"><span class="pull-right"><i class="fa fa-search"></i> Click Name Month to Zoom</span></div><ul>';
        for ($i=0; $i <12 ; $i++) { 
            $html .= $this->templateCalendarView($i);
        }
        $html .= '</ul></div><!--End .standby-->
        </div>
            <div class="col-xs-6">
                <div class="tools" style="overflow:auto; font-size:11px;">
                '.$this->showBuddy($buddy).'
                </div>
            </div>
        </div>

        </div>';
        return $html;
    }

    private function showBuddy($buddy){
        $html = '';
        $html .= '<table class="table table-bordered">
              <tbody>
                <tr>
                  <th>No.</th>
                  <th style="">One call Number One</th>
                  <th style="">One call Number Two</th>
                </tr>';
        
        foreach ($buddy as $key => $value) {
            $email_one = explode(".", $value['email_buddy_one']);
            $email_two = explode(".", $value['email_buddy_two']);

            $html .= '<tr>
                    <td>'.($key+1).'</td>
                    <td>
                      <div class="direct-chat-msg">
                        <img class="direct-chat-img" src="'.$value['pic_buddy_one'].'" alt="message user image">
                        <div class="direct-chat-text_" style="padding-left: 50px;font-size: 0.9em;">
                          <span class="direct-chat-name pull-left_ ">
                            <a href="#ProfileView/'.$value['email_buddy_one'].'">'.$value['thainame_buddy_one'].'</a>
                          </span>
                          <span class="direct-chat-timestamp pull-right" style="width:60px;"><div class="buddy '.$email_one[0].'"></div>'.
                            (($value['next_call_is_one'])?'<span><i class="fa fa-fw fa-star"></i></span>':'')
                          .'</span>
                          <br>
                          <span class="direct-chat-timestamp pull-right_">'.$value['mobile_buddy_one'].'<br>
                            '.$value['email_buddy_one'].'</span>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="direct-chat-msg">
                        <img class="direct-chat-img" src="'.$value['pic_buddy_two'].'" alt="message user image">
                        <div class="direct-chat-text_" style="padding-left: 50px;font-size: 0.9em;">
                        <span class="direct-chat-name pull-left_ ">
                          <a href="#ProfileView/'.$value['email_buddy_two'].'">'.$value['thainame_buddy_two'].'</a>
                        </span>
                        <span class="direct-chat-timestamp pull-right" style="width:60px;"><div class="buddy '.$email_two[0].'"></div>'.
                            (($value['next_call_is_two'])?'<span><i class="fa fa-fw fa-star"></i></span>':'')
                        .'</span>
                        <br>
                        <span class="direct-chat-timestamp pull-right_">'.$value['mobile_buddy_two'].'<br>
                        '.$value['email_buddy_two'].'</span>
                        </div>
                      </div>
                    </td>
                  </tr>';
        }
                  
        $html .= '           
                </tbody>
              <tfoot>
              <tr>
                <td colspan="3">
                  <div class="row">
                  <div class="col-md-12">
                  <span><i class="fa fa-fw fa-star"></i></span> Next Call</div></div>
                </td>
              </tr>
              </tfoot>
          </table>';
        return $html;
    }
    private function templateCalendarView($index){
        $dataDate = $this->dataDate[$index];
        $html = '<li class="">
                      <article tabindex="0">
                        <div class="outline"></div>
                        <div class="dismiss"></div>
                        <div class="binding"></div>
                        <h1>'.$this->month[$index].'</h1>
                        <table>
                          <thead>
                            <tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>
                          </thead>
                          <tbody>';
        $htmlRow = '';
        $n = 0;
        for ($i=0; $i < 31; $i++) { 
            if($n%7==0){
                if($htmlRow!=""){
                    $htmlRow .= '</tr>';
                }
                $htmlRow .= "<tr>";
            }
            if($i==0){
                if($dataDate[$i]['date_w']=="Sun"){
                    $htmlRow .= '<td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'" >
                                <div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                }else if($dataDate[$i]['date_w']=="Mon"){
                    $htmlRow .= '<td></td><td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                    $n+=1;
                }else if($dataDate[$i]['date_w']=="Tue"){
                    $htmlRow .= '<td></td><td></td><td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                    $n+=2;
                }else if($dataDate[$i]['date_w']=="Wed"){
                    $htmlRow .= '<td></td><td></td><td></td><td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                    $n+=3;
                }else if($dataDate[$i]['date_w']=="Thu"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                    $n+=4;
                }else if($dataDate[$i]['date_w']=="Fri"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td></td><td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                    $n+=5;
                }else if($dataDate[$i]['date_w']=="Sat"){
                    $htmlRow .= '<td></td><td></td><td></td><td></td><td></td><td></td><td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                                <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                                '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                                </td>';
                    $n+=6;
                }
            }else{
                $htmlRow .= '<td class="standby-day '.(($dataDate[$i]['holiday'])?'is-holiday':'is-work-day').' '.$dataDate[$i]['date_Ymd'].'"><div class="day" data-day="'.$dataDate[$i]['date_Ymd'].'">'.$dataDate[$i]['date_day'].'</div>
                        <div class="holiday">'.(($dataDate[$i]['holiday'])?$dataDate[$i]['holiday_desc']:'').'</div>
                        '.$this->showStandby7x24($dataDate[$i]['standby7x24']).'
                        </td>';
            }
            $n++;
        }
        $html .= ''.$htmlRow;
        $html .= '</tbody></table>
                      </article>
                    </li>';
        return $html;
    }

    private function showStandby7x24($standby7x24){
        $html = '';
        $html .= '';
        $dataArray = array();
        foreach ($standby7x24 as $key => $value) {
            $email = $value['email'];

            $email = explode(".", $email);
            $time_start = explode(":", $value['time_start']);
            $time_start_ = "start".$time_start[0];
            if(!in_array($email[0], $dataArray)){
                $html .= '<div class="buddy '.$email[0].' '.$time_start_.'"><img class="pic-standby direct-chat-img" src="'.$value['pic'].'" alt=""></div>';
            }
            array_push($dataArray, $email[0]);
        }
        return $html;
    }

    function templateCreateNewSlaRuleView($email){
        $html = '';
        $html .= '<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Different Height</h3>
            </div>
            <div class="box-body">
              <input class="form-control input-lg" type="text" placeholder=".input-lg">
              <br>
              <input class="form-control" type="text" placeholder="Default input">
              <br>
              <input class="form-control input-sm" type="text" placeholder=".input-sm">
            </div>
            <!-- /.box-body -->
          </div>';
        return $html;
    }
    function templateSlaSystemView($email, $ruleSla, $search){
        $html = '';
        $html .= '<div class="object content">
                    <div class="box_ box-" style="position: relative;height: 100%;">
                        <div class="box-header with-border">
                          <h3 class="box-title"><span class="fa fa-bell-o"></span> SLA System</h3>
                        </div>
                        <div class="box-body" style="position: relative;height: 90%;">
                            
                            <div style="width: 100%;"><input type="text" value="'.$search.'" class="form-control input-sm" placeholder="Search" id="search" /></div>
                            <div style="text-align:right;">'.count($ruleSla).' Rows.</div>
                            <div style="overflow: auto;position: absolute;top: 60px;bottom: 0;left: 10px;right: 0;">'.$this->templateShowRuleSla($ruleSla).'</div>
                        </div>
                    </div>
                </div>
                <script>
                    $("#createNew").click(function(){
                        new ObjectListView().loadTemplateCreateNewSlaRule();
                    });
                </script>';
        $html .= '<div class="properties"><div class="slider"></div><div class="content"><div class="properties-editor">'.$this->templateSlaSystemViewTab().'</div></div></div>';

        // print_r($ruleSla);
        return $html;
    }
    private function templateShowRuleSla($ruleSla){
        $html = '<table class="table table-bordered">
                <tbody><tr><th style="width:5%;">#</th><th style="width: 20%;"><a href="#/contract_no/'.rand(0,10000).'">Contract</a></th>
                           <th style=""><a href="#/project/'.rand(0,10000).'">Project</a></th>
                           <th style=""><a href="#/customer/'.rand(0,10000).'">Customer</a></th>
                           <th style=""><a href="#/end_user/'.rand(0,10000).'">End User</a></th>
                           </tr>';
        foreach ($ruleSla as $key => $value) {
            // overflow: hidden;text-overflow: ellipsis;white-space: nowrap;
            $html .= '<tr>
                  <td style="width: 5%;">
                    <input type="radio" name="select-rule-sla" data-sid="'.$value['sid'].'" class="select-rule-sla" />
                    </td>
                  <td style=""><div style="">'.$value['contract_no'].'</div></td>
                  <td style="">
                    <div class="" style="">
                      '.$value['project'].'
                    </div>
                  </td>
                  <td style=""><div style="style="">'.$value['customer'].'</div></td>
                  <td style=""><div style="style="">'.$value['end_user'].'</div></td>
                </tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }
    private function templateSlaSystemViewTab(){
        $html = '';
        $html .= '<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-fw fa-pencil-square-o"></i> New SLA</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-fw fa-reorder"></i> View SLA</a></li>
              <!--<li><a href="#tab_3" data-toggle="tab">Tab 3</a></li>-->
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">'.$this->templateFormNewSla().'</div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                The European languages are members of the same family. Their separate existence is a myth.
                For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                in their grammar, their pronunciation and their most common words. Everyone realizes why a
                new common language would be desirable: one could refuse to pay expensive translators. To
                achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                words. If several languages coalesce, the grammar of the resulting language is more simple
                and regular than that of the individual languages.
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>';
        return $html;
    }
    private function templateFormNewSla(){
        $html = '
                <div class="row">
                    <div class="col-md-4"><b>Contract No: </b>
                        <p><input class="form-control input-sm" type="text" placeholder="Contract No, Ex: F16-6101"></p>
                    </div>
                    <div class="col-md-4"><b>Project: </b>
                        <p><input class="form-control input-sm" type="text" placeholder="Project"></p>
                    </div>
                    <div class="col-md-4"><b>Customer: </b>
                        <p><input class="form-control input-sm" type="text" placeholder="Customer"></p>
                    </div>
                </div>
                <b>Urgency: </b>
                <p><div class="row">
                
                <div class="col-xs-3" style="padding:0px 0px 0px 15px;">SLA Name</div>
                <div class="col-xs-2" style="padding:0px;">Critical</div>
                <div class="col-xs-2" style="padding:0px;">High</div>
                <div class="col-xs-2" style="padding:0px;">Medium</div>
                <div class="col-xs-2" style="padding:0px;">Low</div>
                <div class="col-xs-1" style="padding:0px;"><a herf="javascript:void(0);" class="fa fa-fw fa-plus-circle add-row"></a></div>
              </div></p>
                <div class="row-sla">'.$this->templateFormNewSlaRowSlaDefaultRow().'</div>';
        $html .= $this->templateBtnDoneSla();

        return $html;
    }
    private function templateBtnDoneSla(){
        $html = '';
        $html .= '<br/><div class="row"><div class="col-md-8"></div><div class="col-md-3" style="padding:0px;"><button class="btn btn-block btn-positive">Done</button></div><div class="col-md-1"></div></div>';
        return $html;
    }
    private function templateFormNewSlaRowSlaDefaultRow(){
        $html = '';
        for ($i=0; $i < 3; $i++) { 
            $html .= $this->templateFormNewSlaRowSla();
        }
        return $html;
    }
    private function templateFormNewSlaRowSla(){
        $html = '';
        $html .= '<div class="row">';
        $html .= '<div class="col-xs-3" style="padding:0px 0px 0px 15px;">';
        $html .= '<input type="text" class="form-control" placeholder="">';
        $html .= '</div>';
        $html .= '<div class="col-xs-2" style="padding:0px;">';
        $html .= '<input type="text" class="form-control" placeholder="">';
        $html .= '</div>';
        $html .= '<div class="col-xs-2" style="padding:0px;">';
        $html .= '<input type="text" class="form-control" placeholder="">';
        $html .= '</div>';
        $html .= '<div class="col-xs-2" style="padding:0px;">';
        $html .= '<input type="text" class="form-control" placeholder="">';
        $html .= '</div>';
        $html .= '<div class="col-xs-2" style="padding:0px;">';
        $html .= '<input type="text" class="form-control" placeholder="">';
        $html .= '</div>';
        $html .= '<div class="col-xs-1" style="padding:0px;"><a herf="javascript:void(0);" class="del-row"><i class="fa fa-fw fa-minus-circle "></i></a></div>';
        $html .= '</div>';
        return $html;
    }
    function templateCaseWip($data, $email, $case_status="", $caseType = array()){

        if($case_status=="resolved"){
            $title = "Resolved";
            $beforePage = "CaseResolved";
        }else if($case_status=="missed"){
        	$title = "Case Missed SLA";
        	$beforePage = "CaseMissed";
        }else{
            $title = "Case In Process";
            $beforePage = "CaseWip";
        }
        $html = '
            <style>
                .control-sidebar-menu .menu-info{
                  margin-left: 0px;
                }
            </style>
            <div class="mhn-ui-app-title-head" style="text-align:center;">
                <span class=" pull-left" style="">
                	'.
                  (($email=="autsakorn.t@firstlogic.co.th" || 1)?'<a class="icon icon-compose pull-left" id="CreateCase" href="javascript:void(0);" ></a>':'<a class="icon icon-compose pull-left" href="../mycase/index.html#CreateCase/1/Next" ></a>')
                .'</span>
                <span class=" pull-right">
                  <a class="icon icon-more-vertical pull-right" href="#CaseSettingFilter/'.$beforePage.'"></a>
                </span>
                <span class="mhn-ui-page-title">'.$title.'</span>
            </div>
            <div class="mhn-ui-row mhn-ui-apps animated bounceInRight">
                <div class="content menu-view-bottom">
                    <ul class="table-view" style="">';
        $html .= '<li class="table-view-divider cell-block"><input type="search" placeholder="Search" id="search" style="background-color:#f1f1f1;" /></li>';
        $html .= '<li class="table-view-cell cell-block"><div><p style="text-align:center;"><span class="">'.count($data).' Rows.</span></p></div></li>';
        foreach ($data as $key => $value) {
            $html .= '<li class="table-view-cell ">
                <a class="navigate-right" href="#CaseDetail/'.$beforePage.'/right/'.$value['sid'].'/">
                    <img class="media-object pull-left" src="http://vspace.in.th/api/assets/images/employee/'.$value['maxportraitfile'].'">
                    <div class="media-body">'.$value['subject'].'
                    <p>'.$value['no_ticket'].' '.(($value['refer_remedy_hd'])?'('.$value['refer_remedy_hd'].')':'').'</p>
                      <p>'.$value['end_user'].'</p>
                      <p>Type <span class="pull-right">'.$value['case_type'].'</span></p>
                      <p>Urgency <span class="pull-right">'.$value['urgency'].'</span></p>
                      <p>Status <span class="pull-right">'.$value['status_name'].'</span></p>'
                      .(($value['create_datetime_df'])?'
                        <p>Created <span class="pull-right">'.$value['create_datetime_df'].'</span></p>
                      ':'').( (isset($value['missed'])&&$value['missed'])?$this->showMissedSLA($value['target_sla']):'').'
                    </div>
                </a></li>';
        }
        $html .= '</ul>
                </div>
            </div>
            <div class="mhn-ui-bottom-link-bar">
                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            </div>';

        if(count($caseType)==0){
          $caseType = array(
          	array('name'=>'Request','href'=>'CaseInputSerialContract','img'=>'../img/icon/6.png',"detail"=>"Request"),
          	array('name'=>'Preventive Maintenance','href'=>'CaseInputSerialContract','img'=>'../img/icon/pm.png',"detail"=>"Preventive Maintenance"),
          	array('name'=>'Install','href'=>'CaseProject','img'=>'../img/icon/install.png','detail'=>"Install"),
            array('name'=>'Implement', 'href'=>'CaseProject', 'img'=>'../img/icon/implement.png','detail'=>'Implement'),
            array('name'=>'POC', 'href'=>'CaseProject', 'img'=>'../img/icon/7.png','detail'=>"Prove Of Concept"),
          	array('name'=>'Learning','href'=>'CaseProject','img'=>'../img/icon/project-management.png',"detail"=>"Learning")
          );
        }
        require_once '../../view/CaseManagementCreateView.php';
        $html .= CaseManagementCreateView::controlSider($caseType);
        return $html;
    }
    
    private function showMissedSLA($target_sla){
    	$html = '<p><div class="row"><div class="col-xs-3"><p>Missed</p></div><div class="col-xs-9">';
    	foreach ($target_sla as $key => $value) {
    		if(strpos($value['status'],'issed')>0){
	    		$html .= '<p><span class="pull-right">'.$value['name'].'</span></p>';
	    	}
    	}
    	$html .= "</div></div></p>";
    	return $html;
    }
    function templateCaseDetail($data, $email, $returnBack,$view_page){
        $this->data = $data;
        $this->email = $email;

        $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
                <span class=" pull-left" style="">
                  <a class="icon icon-left-nav pull-left" href="#'.$returnBack.'/left" ></a>
                </span>';
        if($view_page=="3" && $this->data['can_create_new_sr']){
            $html .= '<span class=" pull-right" style=""><a class="icon icon-compose pull-right" href="#CreateServiceReport/CaseWip|CaseDetail/right/'.$this->data['sid'].'/1"></a></span>';
        }else{
            $html .= '<span class=" pull-right" style="visibility:hidden"><a class="icon icon-more-vertical pull-right" href="javascript:void(0);"></a></span>';
        }
        $html .= '<span class="mhn-ui-page-title">
                    <div class="segmented-control" style="width: 210px;margin:auto;">
                        <a style="width: 70px;" class="control-item '.(($view_page=="1")?'active':'').'" data-view="1" data="case_info" href="javascript:void(0);">Case Info</a>
                        <a style="width: 70px;" class="control-item '.(($view_page=="2")?'active':'').'" data-view="2" data="sla" href="javascript:void(0);">Activity</a>
                        <a style="width: 70px;" class="control-item '.(($view_page=="3")?'active':'').'" data-view="3" data="service_report" href="javascript:void(0);">Service</a>
                    </div>
                </span>
            </div>
            <div class="mhn-ui-row mhn-ui-apps">
                '.(($this->templateCaseDetailView($view_page))).'
            </div>
            ';
        $html .= '<div class="mhn-ui-bottom-link-bar">
                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            </div>';
        return $html;
    }

    function templateCaseDetailWeb($data, $email, $returnBack,$view_page, $permission_role=array()){
        $this->data = $data;
        $this->email = $email;
        $html = '';
        if($view_page=="3" && $this->data['can_create_new_sr']){
            $html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
                <span class=" pull-left" style="">
                  <!--<a class="icon icon-left-nav pull-left" href="#'.$returnBack.'/left" ></a>-->
                </span>';
            $html .= '<span class=" pull-right" style=""><a style="font-size: 1.5em; margin: 10px 10px 0px 0px;" class="fa fa-fw fa-pencil-square-o pull-right" href="#CreateServiceReport/'.rand(0,10000).'/right/'.$this->data['sid'].'/1"></a></span>';

            $html .= '<span class="mhn-ui-page-title"></span></div>';
        }else{
            // $html .= '<span class=" pull-right" style="visibility:hidden"><a class="icon icon-more-vertical pull-right" href="javascript:void(0);"></a></span>';
        }
        $html .= '
            <div class="mhn-ui-row mhn-ui-apps">
                '.(($this->templateCaseDetailView($view_page))).'
            </div>';
        if(isset($permission_role['prefix_table']) && $permission_role['prefix_table']==""){
          $html .= '<script>if($(".tab-spare-part").length<1){$(".propertyEditorWrapper ul.nav-tabs").append("<li class=\"tab-spare-part\"><a href=\"#sparepartOracle\" data-toggle=\"tab\" aria-expanded=\"true\">Spare Part</a></li>");}</script>';
        }
          // else{
        //   $html .= '<script>if($(".tab-spare-part").length<1){$(".propertyEditorWrapper ul.nav-tabs").append("<li class=\"tab-spare-part\"><a href=\"#sparepart\" data-toggle=\"tab\" aria-expanded=\"true\">Spare Part</a></li>");}</script>';
        // }

        
        return $html;
    }
    public function templateCreateCaseControl($email, $dataProjectParse= array(), $caseType = array()){
        $html = '';

       
        require_once '../../view/CaseManagementCreateView.php';
        $html = CaseManagementCreateView::controlSider($caseType);
        
        return $html;
    }
    private function templateCaseDetailView($viewPage){
        if($viewPage=="1"){
            return $this->templateCaseDetailCaseInfo();
        }else if($viewPage=="2"){
            return $this->templateCaseDetailActivity();
        }else if($viewPage=="3"){
            return $this->templateCaseDetailService();
        }else if($viewPage=="4"){
            return "";
        } else if($viewPage=="5"){
            return $this->templateCaseDetailSparePart();
        }
    }

    private function templateCaseDetailSparePart(){
        require_once '../../view/ActivityCase.php';

        $obj = new CaseModel();
        $data = $obj->listOracleSr($this->email, $this->data['sid'], "");

        // $activityCase = new ActivityCase($this->email);
        $html = '<div class="content menu-view-bottom" style="width:100%;">';
        $html .= '<ul class="table-view"><li class="table-view-divider"><div>Spare<a style="font-size: 1.5em;" class="pull-right" href="javascript:void(0);" id="CreateOracleSr"><i class="fa fa-fw fa-edit"></i></a></div></li>';
        if(count($data)>0){
          foreach ($data as $key => $value) {
            $html .= '<li class="table-view-cell">
                          <a href="javascript:void(0);" class="navigate-right view-spare" data-spare-sid="'.$value['sid'].'" sr_no="'.$value['oracle_sr'].'" model="'.$value['model'].'" type="'.$value['oracle_sr_type'].'" oracle_create_date="'.$value['oracle_create_date'].'">
                            <div>
                            <p>SR <span class="pull-right">'.$value['oracle_sr'].'</span></p>
                            <p>Model <span class="pull-right">'.$value['model'].'</span></p>
                            <p>Type <span class="pull-right">'.$value['oracle_sr_type'].'</span></p>
                            <p>Oracle Create Date <span class="pull-right">'.$value['oracle_create_date'].'</span></p>
                            </div>
                          </a>
                      </li>';
          }
        }else{
            $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>No Spare Part</p></div></li>';
        }
        $html .= '</ul>';
        $html .= "</div>";
        return $html;
    }
    private function templateCaseDetailService(){
        require_once '../../view/ActivityCase.php';
        $activityCase = new ActivityCase($this->email);
        $html = '<div class="content menu-view-bottom" style="width:100%;">';
        if(count($this->data['service_report'])>0){
            $html .= $activityCase->htmlServiceReport($this->data['sid'],$this->data['service_report'],$this->email);
        }else{
            $html .= '<div class=""><ul class="table-view"><li class="table-view-cell cell-block"><div style="text-align:center;"><p>No Service Report</p></div></li></ul></div>';
        }
        $html .= "</div>";
        return $html;
    }
    private function templateCaseDetailActivity(){
        require_once '../../view/ActivityCase.php';

        $html = '<div class="content menu-view-bottom">';
        $activityCase = new ActivityCase($this->email);
        $html .= $activityCase->htmlActivityCase(
            (isset($this->data['sla'])?$this->data['sla']:array()), 
            (isset($this->data['expect_pending_finish'])?$this->data['expect_pending_finish']:""),
            $this->data['work_log_remedy'],
            $this->data['sid'],
            $this->data['service_report'],
            $this->email
        );
        $html .= "</div>";
        return $html;
    }
    private function templateCaseDetailCaseInfo(){
        $html = '<div class="content menu-view-bottom"><div class="card" style="background-color:initial;">
              <ul class="table-view">
                <li class="table-view-divider">Case Info</li>
                <li class="table-view-cell cell-block">
                  <p>
                    Subject <span class="pull-right">'.$this->data['subject'].'</span>
                  </p>
                </li>
                <li class="table-view-cell cell-block">
                  <p>
                    Case No. <span class="pull-right">'.$this->data['no_ticket'].''.(($this->data['refer_remedy_hd'])?"(".($this->data['refer_remedy_hd']).")":'').'
                    </span>
                  </p>
                </li>
                <li class="table-view-cell cell-block">
                    <p>Contract No. <span class="pull-right">'.$this->data['contract_no'].'</span></p>
                </li>
                <li class="table-view-cell cell-block">
                    <p>Type <span class="pull-right">'.$this->data['case_type'].'</span></p>
                </li>
                <li class="table-view-cell cell-block">
                    <p>Urgency <span class="pull-right">'.$this->data['urgency'].'</span></p>
                </li>
                '.(($this->data['end_user'])?
                    '<li class="table-view-cell media cell-block">
                        <p>End User <span class="pull-right">'.$this->data['end_user'].'</span></p>
                    </li>':'').
                (($this->data['end_user_site'])?
                    '<li class="table-view-cell media cell-block">
                        <p>Address <span class="pull-right">'.$this->data['end_user_site'].'</span></p>
                    </li>':'').
                '<li class="table-view-cell cell-block"><div><p>Created <span class="pull-right">'.$this->data['create_datetime_df'].'</span></p></div></li>
                <li class="table-view-cell cell-block">
                    <p>Status <span class="pull-right">'.$this->data['status_name'].'</span></p>
                </li>
                </ul></div>';

        if(isset($this->data['serial_no']) && count($this->data['serial_no'])>0 && $this->data['serial_no']!=""){
            $html .= '<div class="card" style="background-color:initial;">
                <ul class="table-view">
                <li class="table-view-divider">Serial</li>';
            foreach ($this->data['serial_no'] as $key => $value) {
                $html .= '<li class="table-view-cell cell-block"><p>'.$value['serial'].'</p></li>';
            }
            $html .= '</ul></div>';
        }
        
            $html .= '<div class="card" style="background-color:initial;"><ul class="table-view">';
            if($this->data['end_user_contact_name']){
                $html .= '<li class="table-view-divider">Contact User Info</li>
                    <li class="table-view-cell cell-block"><p>Name 
                    <span class="pull-right">'.$this->data['end_user_contact_name'].'</span></p></li>';
            }
            $html .= '
                    <li class="table-view-cell cell-block">
                      <p>Email <span class="pull-right">'.$this->data['end_user_email'].'</span></p>
                    </li>
                    <li class="table-view-cell cell-block">
                      <p>Phone <span class="pull-right">'.$this->data['end_user_phone'].'</span></p>
                    </li>
                    <li class="table-view-cell cell-block">
                      <p>Mobile <span class="pull-right">'.$this->data['end_user_mobile'].'</span></p>
                    </li>
                    <li class="table-view-cell cell-block">
                      <p>Company <span class="pull-right">'.$this->data['end_user_company_name'].'</span></p>
                    </li>
                </ul>
              </div>
              <div class="card" style="background-color:initial;">
                  <ul class="table-view">
                      <li class="table-view-divider">Owner</li>
                      <li class="table-view-cell">
                        <img class="media-object pull-left" src="http://vspace.in.th/api/assets/images/employee/'.$this->data['owner_picture'].'">
                        <div class="media-body">
                          <p>'.$this->data['thainame'].'</p>
                          <p>'.$this->data['owner'].'</p>
                          <p></p><p>'.$this->data['owner_mobile'].'</p>
                        </div>
                      </li>
                  </ul>
              </div></div>';

            return $html;
    }

    function templateBugReport($data, $email){

        $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            <!--<div class="mhn-ui-app-time">&nbsp;</div>-->
            <a class=" pull-left" href="#MoreSystem" style="color:#fff;"><span class="icon icon-left-nav"></span></a>
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title"><span class="ion-information-circled" style="font-size:1em;"></span> Bug Reports</span>
            </div>
            <div class="mhn-ui-row mhn-ui-apps">
                <div class="content">
                    <ul class="table-view">
                        <li class="table-view-divider cell-block"><div style="text-align:left;">Subject Bug</div></li>
                        <li class="table-view-cell cell-block"><div><p><textarea id="subject_bug_report" rows="4"></textarea></p></div></li>
                        <li class="table-view-cell cell-block"><div><p>แจ้งปัญหาการใช้งานระบบ vSpace</p></div></li>
                        <li class="table-view-cell cell-block"><div><p><button class="btn btn-primary btn-block" id="btnDoneBugReport">Done</button></p></div></li>
                    </ul>
                </div>
            </div>
            <div class="mhn-ui-bottom-link-bar">
                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            </div>';
        return $html;
    }

    function templatePortfolio($data){
        $html = '<div class="mhn-ui-app-title-head">
            <!--<div class="mhn-ui-app-time">&nbsp;</div>-->
            <span class="mhn-ui-page-title"><span class="ion-information-circled" style="font-size:1em;"></span> Portfolio</span>
            </div>
            <div class="mhn-ui-row mhn-ui-apps">
                <div class="content menu-view-bottom">
                    <ul class="table-view">
                        ';
        foreach ($data as $key => $value) {
            $html .= '<li class="table-view-divider cell-block"><div style="text-align:left;">
                <div class="row"><div class="col-xs-3"><p>'.$value['date_dmY'].'</p></div>
                <div class="col-xs-9">';
                foreach ($value['timeline_real'] as $k => $v) {
                    $html .= '<p class="pull-right">'.$v['no_task'].', '.$v['no_ticket'].' '.$v['contract_no'].' '.$v['subject'].' </p>';                    
                }
            $html .= '</div>
                </div></div></li>';
        }
        $html .= '</ul>
                </div>
            </div>
            <div class="mhn-ui-bottom-link-bar">
                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            </div>';
    
        return $html;
    }
    
    function templateSyncRemedy($data){
        $html = '<div class="mhn-ui-app-title-head">
            <!--<div class="mhn-ui-app-time">&nbsp;</div>-->
            <span class="mhn-ui-page-title"><span class="fa fa-fw fa-share-alt" style="font-size:1em;"></span> Sync Remedy</span>
            </div>
            <div class="mhn-ui-row mhn-ui-apps">
                <div class="content">
                    <ul class="table-view">
                        <li class="table-view-divider cell-block"><div style="text-align:left;">Remedy User</div></li>
                        <li class="table-view-cell cell-block"><div style="text-align:center;"><input id="remedy_user" value="'.$data['remedy_user'].'" type="text" /></div></li>
                        <li class="table-view-divider cell-block"><div style="text-align:left;">Remedy Password</div></li>
                        <li class="table-view-cell cell-block"><div style="text-align:center;"><input id="remedy_password" value="'.$data['remedy_password'].'"type="password" /></div></li>
                        <li class="table-view-cell cell-block"><div><p>กรอก User และ Password (ที่เข้าใช้งานระบบ Remedy) เพื่อใช้ระบุตัวตนเมื่อส่งข้อมูลไปยังระบบ Remedy</p></div></li>
                        <li class="table-view-cell cell-block"><div style="text-align:center;"><button id="btnDoneRemedyUserPassword" class="btn btn-primary btn-block">Done</button></div></li>
                    </ul>
                </div>
            </div>
            <div class="mhn-ui-bottom-link-bar">
                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            </div>';
    
        return $html;
    }
    function templateProfile($data){
        $html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            <!--<div class="mhn-ui-app-time">&nbsp;</div>-->
            <a class=" pull-left" href="#MoreSystem" style="color:#fff;"><span class="icon icon-left-nav"></span></a>
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title" ><span class="ion-person"></span> Profile</span>
            </div>
            <div class="mhn-ui-row mhn-ui-apps">
                <div class="content menu-view-bottom">
                    <ul class="table-view">
                        <li class="table-view-cell cell-block"><div style="text-align:center;"><img class="media-object" style="margin:auto; width:20%; height:initial;" src="'.$data['pic_employee'].'" /></div></li>
                        <li class="table-view-cell cell-block"><div style="text-align:center;"><p>'.$data['thainame'].'<br/>'.$data['emailaddr'].'<br/>'.$data['mobile'].'</p></div></li>
                        <li class="table-view-cell cell-block"><div style="text-align:center;"><p>'.$data['compename'].'<br/>'.$data['unitename'].'</p></div></li>
                        <li class="table-view-cell cell-block">
                            <div style="text-align:center;">
                            '.(($data['path_signature_original'])?'
                                <img style="background-color:#FFFFFF; height: 50px;" src="http://vspace.in.th'.$data['path_signature'].'"/>
                            ':'-').'
                            </div>';
            $html .= '<div style="text-align:center;"><p><a style="color:gold;" href="#Signature/right" class="ion-compose"> Signature</a></p></div>';
      
            $html .=  '</li>
                    </ul>
                </div>
            </div>
            <div class="mhn-ui-bottom-link-bar">
                <a href="#MoreSystem"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            </div>';
    
        return $html;
    }
    public function templateStaffSignature($data){
      $sourcePath = 'http://vspace.in.th/pages/signaturepad/employee/';
      $html = '';
      $html .= '<link rel="stylesheet" href="'.$sourcePath.'css/signature-pad.css">
                <style type="text/css">
                  .description {
                    text-align:center;color:#222222;
                  }
                  body {background:#2c3e50;}
                  .m-signature-pad--header {
                    left: 20px;
                    right: 20px;
                    bottom: 4px;
                    height: 28px;
                  }
                  .m-signature-pad--body{
                    top: 50px;
                  }
                  @media screen and (max-height: 320px){
                    .m-signature-pad--header .description {
                        font-size: 1em;
                        /*margin-top: 1em;*/
                    }
                    .m-signature-pad--header .description {
                        /*color: #C3C3C3;*/
                        color: #222222;
                        text-align: center;
                        font-size: 1.2em;
                        /*margin-top: 1.8em;*/
                    }
                  }
                    @media screen and (max-width: 1024px) {
                      .m-signature-pad {
                          margin: 0% !important;
                      }
                  }
                  .m-signature-pad--header .description {
                    padding: 20px;
                    font-size: 14px;
                  }
                  .m-signature-pad--footer .button {
                    color: #222222;
                  }
                </style>
              </head>
              <body >
               
                <div id="signature-pad" class="m-signature-pad">
                  <div class="m-signature-pad--header">
                    <div class="description">
                      Sign New
                    </div>
                    
                  </div>
                  <div class="m-signature-pad--body">
                    <canvas></canvas>
                  </div>
                  <div class="m-signature-pad--footer">
                    <div class="description">Sign above</div>
                    <button type="button" class="button clear" data-action="clear" style="">Clear</button>
                    <!--<a  class="button back" href="#profile"><button type="button">Back</button></a>-->
                    <button type="button" class="button save" data-action="save">Save</button>
                  </div>
                 
                    <input class="output" name="output" value=""  type="hidden" />
                    
                </div>
                <div class="mhn-ui-bottom-link-bar">
                  <a href="#MoreSystem/'.(rand(0,1000)).'"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
                  <!--<span class="mhn-ui-bottom-btn ion-ios-locked" onclick="mhnUI.page.show(\'page-lock\')"></span>-->
                </div>
              <script src="'.$sourcePath.'js/signature_pad.js"></script><script src="'.$sourcePath.'js/appSign.js"></script>';
      return $html;
    }

    function templateAllApplication($email,$dataProfile,$dataRemedy,$dataTodolist,$dataUnsigned,$permission_role=array()){
        $badgeProfile = "";
        $badgeRemedy = "";
        $badgeTodolist = "";
        $badgeSrUnsigned = "";
        if(!$dataProfile['path_signature_original']){
            $badgeProfile = "1";
        }
        if(!$dataRemedy['remedy_password']){
            $badgeRemedy = "1";
        }
        if(count($dataTodolist)>0){
            $badgeTodolist = count($dataTodolist);
        }
        if(count($dataUnsigned)>0){
            $badgeSrUnsigned = count($dataUnsigned);
        }
        
        $appStyle = "";
        if($email=="autsakorn.t@firstlogic.co.th" && 1==3){
        	$appStyle = 'style="margin-top:260px; overflow-y:auto;overflow-x:hidden;bottom:40px;"';
        }
        $appWidget = "";
        if($email=="autsakorn.t@firstlogic.co.th" && 1==3){
        	$styleWidget = "border-radius: 20px;position: relative;overflow: hidden;-webkit-animation: appear 500ms ease-out forwards;animation: appear 500ms ease-out forwards;";
        	$appWidget .= '
        	<div style="margin-top:60px;height:40px;width:100%;position: fixed;right: 0;left: 0;bottom: 0;top: 0;" class="animated bounceInRight">
        		<div style="'.$styleWidget.'height: 200px;border-radius: 5px;>
        			<div class="">
	        			<ul class="table-view">
	        				<li class="table-view-divider cell-block"><div><p><i class="ion-clipboard"></i> SUGGESTIONS</p></div></li>
	        			</ul>
        			</div>
        		</div>
        	</div>
        	<div style="margin-top:100px;height: 150px;width:100%;position: fixed;right: 0;left: 0;bottom: 0;top: 0;overflow-y: auto;overflow-x: hidden;" class="animated bounceInRight">
        		<div style="'.$styleWidget.'height: 200px;border-radius: 5px; >
        			<div class="">
	        			<ul class="table-view">
	        				<li class="table-view-cell"><a class="navigate-right" href="javascript:void(0);">TEST</a></li>
	        				<li class="table-view-cell"><a class="navigate-right" href="javascript:void(0);">TEST</a></li>
	        				<li class="table-view-cell"><a class="navigate-right" href="javascript:void(0);">TEST</a></li>
	        				<li class="table-view-cell"><a class="navigate-right" href="javascript:void(0);">TEST</a></li>
	        				<li class="table-view-cell"><a class="navigate-right" href="javascript:void(0);">TEST</a></li>
	        				<li class="table-view-cell"><a class="navigate-right" href="javascript:void(0);">TEST</a></li>
	        			</ul>
        			</div>
        		</div>
        	</div>';
        }
        
        $html = '<div class="mhn-ui-app-title-head">
            <!--<div class="mhn-ui-app-time">&nbsp;</div>-->
            <span class="mhn-ui-page-title"><span class="fa fa-fw fa-th" style="font-size:1em;"></span> vSpace Application</span>
            <span class="pull-right"><i class="mhn-ui-icon-logout fa fa-fw fa-power-off" style="font-size:1em;"></i></span>
            <!--<div class="mhn-ui-filter">
                <span class="mhn-ui-btn ion-funnel"></span>
                <div class="mhn-ui-filter-list">
                    <div data-filter="all" class="active">All Application</div>
                    <div data-filter="general">General Application</div>
                    <div data-filter="social">Social Application</div>
                    <div data-filter="credits">Credits Application</div>
                </div>
            </div>-->
        </div>'.$appWidget.'
        <div class="mhn-ui-row mhn-ui-apps" '.$appStyle.'>
            '.(($email=="autsakorn.t@firstlogic.co.th" || $email=="sarinaphat.t@firstlogic.co.th" || 1)?
            '<div class="mhn-ui-col">
                <div class="mhn-ui-icon " data-open="">
                    <a href="#newVSpace">
                      <span class="fa fa-fw fa-hand-peace-o" data-color="#2980b9" style="background-color:rgb(0, 188, 212);position:relative;">
                        <small class="badge badge-moresystem"></small>
                      </span>
                    </a>
                    <div class="mhn-ui-icon-title">New vSpace</div>
                </div>
            </div>':'').'
            <div class="mhn-ui-col" data-filter="general">
                <div class="mhn-ui-icon page-author" data-open="page-author">
                    <span class="ion-person" data-color="#2980b9" style="background-color:#2980b9;position:relative;">
                        <small class="badge badge-moresystem">'.$badgeProfile.'</small>
                    </span>
                    <div class="mhn-ui-icon-title">Profile</div>
                </div>
            </div>
            '.(($permission_role['comp_refer_ebiz']=="first")?'<div class="mhn-ui-col" data-filter="general">
                <div class="mhn-ui-icon">
                    <a href="../home/index.html#view/1"><span class="ion-clipboard" data-color="gold" style="background-color:gold;position:relative;">
                        <small class="badge badge-moresystem" id="badge-todolist">'.$badgeTodolist.'</small>
                    </span></a>
                    <div class="mhn-ui-icon-title">To-do List</div>
                </div>
            </div>':'<div class="mhn-ui-col" data-filter="general">
                            <div class="mhn-ui-icon">
                                <a href="#todolistview/right/'.rand(0,10000).'"><span class="ion-clipboard" data-color="gold" style="background-color:gold;position:relative;">
                                    <small class="badge badge-moresystem" id="badge-todolist">'.$badgeTodolist.'</small>
                                </span></a>
                                <div class="mhn-ui-icon-title">To-do List</div>
                            </div>
                        </div>').'
            '.(($permission_role['comp_refer_ebiz']=="cdgm" || $email=="teerawut.p@firstlogic.co.th")?'':'').'
            <div class="mhn-ui-col" data-filter="general">
                <div class="mhn-ui-icon" >
                    '.(($email=="autsakorn.t@firstlogic.co.th" || $email=="visa.r@firstlogic.co.th" || 1)?'<a href="#unsignedServiceView/right/'.rand(0,10000).'">':'<a href="../home/index.html#view/2">').
                    '<span class="ion-compose" data-color="chocolate" style="background-color:chocolate;position:relative;">
                        <small class="badge badge-moresystem" id="badge-unsigned">'.$badgeSrUnsigned.'</small>
                    </span></a>
                    <div class="mhn-ui-icon-title">Unsigned</div>
                </div>
            </div>
            <div class="mhn-ui-col" data-filter="general">
                <div class="mhn-ui-icon" data-open="">
                    '.(($email=="autsakorn.t@firstlogic.co.th" || 1)?'<a href="#notifyServiceView/right/'.rand(0,10000).'">':'<a href="../home/index.html#view/4">').
                        '<span class="fa fa-bell-o" data-color="#00c0ef" style="background-color:#00c0ef;position:relative;">
                            <small class="badge badge-moresystem" id="badge-notify"></small>
                        </span>
                    </a>
                    <div class="mhn-ui-icon-title">Notify</div>
                </div>
            </div>
            <!--<div class="mhn-ui-col" data-filter="general">
                <div class="mhn-ui-icon" data-href="http://codecanyon.net/user/khadkamhn/portfolio">
                    <a href="#Portfolio"><span class="ion-ios-briefcase" data-color="#f39c12" style="background-color:#4bae4f;"></span></a>
                    <div class="mhn-ui-icon-title">Portfolio</div>
                </div>
            </div>-->
            <div class="mhn-ui-col" data-filter="credits">
                <div class="mhn-ui-icon" data-href="">
                    '.(($email=="autsakorn.t@firstlogic.co.th" || $email=="visa.r@firstlogic.co.th" || 1)?'<a href="#calendarView/right/'.rand(0,10000).'">':'<a href="../calendar/index.html">').
                    '<span class="fa fa-fw fa-calendar" data-color="##16a085" style="background-color:#16a085;"></span></a>
                    <div class="mhn-ui-icon-title">Calendar</div>
                </div>
            </div>
            '.(($email=="autsakorn.t@firstlogic.co.th" || 1)?
                '<div class="mhn-ui-col" data-filter="credits">
                    <div class="mhn-ui-icon" data-href="">
                        <a href="#ProjectManagement/right/'.rand(0,10000).'"><span class="fa fa-pie-chart" data-color="#f39c12" style="background-color:#74d600;"></span></a>
                        <div class="mhn-ui-icon-title">Project</div>
                    </div>
                </div>'
                :'').'
            '.(($email=="autsakorn.t@firstlogic.co.th" || 1)?
                '<div class="mhn-ui-col" data-filter="credits">
                    <div class="mhn-ui-icon" data-href="">
                        <a href="#CaseWip/right"><span class="fa fa-hourglass-2" data-color="#f39c12" style="background-color:#f39c12;"></span></a>
                        <div class="mhn-ui-icon-title">Cases</div>
                    </div>
                </div>'
                :
            '<div class="mhn-ui-col" data-filter="credits">
                <div class="mhn-ui-icon" data-href="">
                    <a href="../mycase/index.html"><span class="fa fa-hourglass-2" data-color="#f39c12" style="background-color:#f39c12;"></span></a>
                    <div class="mhn-ui-icon-title">Cases</div>
                </div>
            </div>
            ').'
            '.(($email=="autsakorn.t@firstlogic.co.th" || 1)?
                '<div class="mhn-ui-col" data-filter="credits">
                    <div class="mhn-ui-icon" data-href="">
                        <a href="#CaseResolved/right"><span class="fa fa-hourglass" data-color="#f39c12" style="background-color:green;"></span></a>
                        <div class="mhn-ui-icon-title">Resolved</div>
                    </div>
                </div>'
                :
            '').''.(($email=="autsakorn.t@firstlogic.co.th" || 1)?
                '<div class="mhn-ui-col" data-filter="credits">
                    <div class="mhn-ui-icon" data-href="">
                        <a href="#CaseMissed/right"><span class="fa fa-bell-slash-o" data-color="#f39c12" style="background-color:red;"></span></a>
                        <div class="mhn-ui-icon-title">Missed</div>
                    </div>
                </div>'
                :
            '').'
            <div class="mhn-ui-col" data-filter="credits">
                <div class="mhn-ui-icon" data-href="">
                    <a href="#SyncRemedy"><span class="fa fa-fw fa-share-alt" data-color="#000" style="background-color:#000;position:relative;">
                        <small class="badge badge-moresystem">'.$badgeRemedy.'</small>
                        </span></a>
                    <div class="mhn-ui-icon-title">Remedy</div>
                </div>
            </div>
            <div class="mhn-ui-col" data-filter="credits">
                <div class="mhn-ui-icon" data-href="">
                    <a href="#BugReport"><span class="ion-information-circled" data-color="#785447" style="background-color:#785447;"></span></a>
                    <div class="mhn-ui-icon-title">Bug Reports</div>
                </div>
            </div>'.(($email=="autsakorn.t@firstlogic.co.th" && 1==2)?
              '<div class="mhn-ui-col" data-filter="credits">
                <div class="mhn-ui-icon" data-href="">
                    <a href="#ColorGameView/right/'.rand(0,10000).'"><span class="ion-information-circled" data-color="#785447" style="background-color:#785447;"></span></a>
                    <div class="mhn-ui-icon-title">Color Game</div>
                </div>
            </div>':'').'
            <!--<div class="mhn-ui-col" data-filter="credits">
                <div class="mhn-ui-icon mhn-ui-icon-logout">
                    <span class="ion-android-unlock" data-color="#4bae4f"></span>
                    <div class="mhn-ui-icon-title">Sign Out</div>
                </div>
            </div>-->
        </div>
        <div class="mhn-ui-bottom-link-bar">
            <a href="#MoreSystem/'.(rand(0,1000)).'"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            <!--<span class="mhn-ui-bottom-btn ion-ios-locked" onclick="mhnUI.page.show(\'page-lock\')"></span>-->
        </div>';
        return $html;
    }
    function template(){
        $html = '<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.0/utils/Draggable.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
       
        <div class="mhn-ui-wrap" id="">
            <div class="mhn-ui-page page-lock">
                <div class="mhn-ui-date-time" >
                    <div class="mhn-ui-time">6:02 PM</div>
                    <div class="mhn-ui-day">Friday</div>
                    <div class="mhn-ui-date">September 05, 2015</div>
                </div>
                <div class="mhn-lock-wrap">
                    
                    <div class="mhn-lock"></div>
                </div>
                <div class="mhn-lock-title" data-title="Draw a pattern to unlock"></div>
            </div>

            <div class="mhn-ui-page page-home">
                
            </div>

            <div class="mhn-ui-page page-author">
                <div class="mhn-ui-app-time"></div>
                <div class="mhn-ui-app-title-head"><span class="ion-person"></span> Profile</div>
                <div class="mhn-ui-row mhn-ui-apps text-center">
                    
                </div>
                
                <div class="mhn-ui-bottom-link-bar">
                    <span class="mhn-ui-bottom-btn ion-ios-home" onclick="mhnUI.page.show(\'page-home\')"></span>
                </div>
            </div>

            <div class="mhn-ui-page page-contact">
                <div class="mhn-ui-app-time">&nbsp;</div>
                <div class="mhn-ui-app-title-head"><span class="ion-chatbox"></span> Contact</div>
                
            </div>

            <div class="mhn-ui-page page-credits">
                <div class="mhn-ui-app-time">&nbsp;</div>
                <div class="mhn-ui-app-title-head"><span class="ion-information-circled"></span> Credits</div>
                <div class="mhn-ui-credit-list">
                    
                </div>
                <div class="mhn-ui-bottom-link-bar">
                    <span class="mhn-ui-bottom-btn ion-ios-home" onclick="mhnUI.page.show(\'page-home\')"></span>
                    <span class="mhn-ui-bottom-btn ion-ios-locked" onclick="mhnUI.page.show(\'page-lock\')"></span>
                </div>
            </div>
            <div class="mhn-ui-dialog-wrap confirmClosedService">
                <div class="mhn-ui-dialog">
                    <div class="mhn-ui-dialog-title">Are you sure?</div>
                    <p>This application wants to open an external link. To confirm, please click on yes button.</p>
                    <a data-action="confirm" class="mhn-ui-dialog-btn" target="_blank">Yes</a>
                    <a data-action="cancel" class="mhn-ui-dialog-btn">No</a>
                </div>
            </div>
            <div class="mhn-ui-dialog-wrap confirmStartTask">
                <div class="mhn-ui-dialog">
                    <div class="mhn-ui-dialog-title">Are you sure?</div>
                    <p>This application wants to open an external link. To confirm, please click on yes button.</p>
                    <a data-action="confirm" class="mhn-ui-dialog-btn" target="_blank">Yes</a>
                    <a data-action="cancel" class="mhn-ui-dialog-btn">No</a>
                </div>
            </div>
            <div class="mhn-ui-dialog-wrap dialog-logout">
                <div class="mhn-ui-dialog">
                    <div class="mhn-ui-dialog-title">Are you sure Sign out?</div>
                    <p></p>
                    <a data-action="confirm" class="mhn-ui-dialog-btn" target="_blank">Yes</a>
                    <a data-action="cancel" class="mhn-ui-dialog-btn">No</a>
                </div>
            </div>
        </div>';
        return $html;
    }

    public static function bottomBar(){
        $html = '<div class="mhn-ui-bottom-link-bar">
            <a href="#MoreSystem/'.(rand(0,1000)).'"><span class="mhn-ui-bottom-btn ion-ios-home"></span></a>
            <!--<span class="mhn-ui-bottom-btn ion-ios-locked" onclick="mhnUI.page.show(\'page-lock\')"></span>-->
        </div>';
        return $html;
    }
}