<?php class ServiceReportCreateView{

	private $ticket_sid;
	public function __construct(){

	}

	private function genBtnReturnBack($returnBack,$classBtnReturnBack){
		$html = "";
		if(strpos($returnBack,"|")>0){
			$dataReturnBack = explode("|", $returnBack);
			$html = '<a class="'.$classBtnReturnBack.' pull-left" href="#'.$dataReturnBack['1'].'/'.$dataReturnBack[0].'/left/'.$this->ticket_sid.'/3/'.rand(0,10000).'" style=""><span class="icon icon-left-nav"></span></a>';
		}else{
			$html = '<a class="backToService pull-left" href="javascript:void(0);" style=""><span class="icon icon-left-nav"></span></a>';
		}
		return $html;
	}

	private function genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack){
		$dataReturnBack = explode("|", $returnBack);
		$html = '<a class="'.$classBtnReturnBack.' pull-left" href="#CreateServiceReport/'.$returnBack.'/left/'.$this->ticket_sid.'/'.rand(0,10000).'" style=""><span class="icon icon-left-nav"></span></a>';
		return $html;
	}

	public function serviceReportEndUserChoose($data,$email,$returnBack,$dataCase, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-end-user-choose";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBack($returnBack,$classBtnReturnBack).' 
            <a class="icon pull-right icon-person manual-person" href="javascript:void(0);"></a>
            <span class="mhn-ui-page-title">Choose End User / add new End User</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps">
		<div class="content">
          <div class="list-data"><div>
        <ul class="table-view">';
            
            foreach ($data as $key => $value) {
            $html .= '<li class="table-view-cell">
              <a href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'" class="navigate-right end-user-choose" data-name="'.$value['name'].'" data-email="'.$value['email'].'" data-mobile="'.$value['mobile'].'" data-phone="'.$value['phone'].'" data-company="'.$value['company'].'">
                <p data="name">'.$value['name'].'</p>
                <p data="email">'.$value['email'].'</p>
                <p data="mobile">'.$value['mobile'].'</p>
                <p data="phone">'.$value['phone'].'</p>
                <p data="company">'.$value['company'].'</p>
              </a>
            </li>';
            }           
       $html .= '</ul>
    </div></div>
    </div></div>
    ';
		return $html;
	}

	public function serviceReportEndUserCreateNew($data,$email,$returnBack,$dataCase, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-end-user-choose";
		if($type_request=="web"){
			$html = '<div style="position:absolute; top:0px;width:100%; padding:10px;">';
		}else{
			$html = '<div style="position:absolute; top:0px;width:100%; padding:0px;">';
		}
		$html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">Create End User</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps">
			<div class="content">
          <ul class="table-view">
              <li class="table-view-divider">Name</li>
              <li class="table-view-cell" style="padding-right:15px;"><input id="name" type="text"></li>
              <li class="table-view-divider">Email</li>
              <li class="table-view-cell" style="padding-right:15px;"><input id="email" type="text"></li>
              <li class="table-view-divider">Phone</li>
              <li class="table-view-cell" style="padding-right:15px;"><input id="phone" type="text"></li>
              <li class="table-view-divider">Mobile</li>
              <li class="table-view-cell" style="padding-right:15px;"><input id="mobile" type="text"></li>
              <li class="table-view-divider">Company</li>
              <li class="table-view-cell" style="padding-right:15px;"><input id="company" type="text"></li>
              <li class="table-view-cell" style="padding-right:15px;">
                <div>
                    <a href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'" class="btn btn-block btn-positive" id="btnCreateEnduser">Done</a>
                  </div>
              </li>
          </ul>
      </div></div></div>';
		
		return $html;
	}

	public function serviceReportSubject($data,$email,$returnBack,$dataCase, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-subject";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">What is Subject?</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps"><div class="content">
            <ul class="table-view">
                <li class="table-view-cell" style="padding-right:15px;">
                    <textarea rows="5" id="subject"></textarea>
                </li>
                <li class="table-view-cell" style="padding-right:15px;">
                    <div>
                        <a id="btn-create-subject" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'" class="btn btn-block btn-positive">Next</a>
                    </div>
                </li>
            </ul>
        </div></div>';
		
		return $html;
	}

	public function serviceReportAddress($data,$email,$returnBack,$dataCase, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-address";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">Address of Service?</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps"><div class="content">
            <ul class="table-view">
                <li class="table-view-cell" style="padding-right:15px;">
                    <textarea rows="5" id="address">'.$dataCase['end_user_site'].'</textarea>
                </li>
                <li class="table-view-cell" style="padding-right:15px;">
                    <div>
                        <a id="btn-create-address" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'" class="btn btn-block btn-positive">Next</a>
                    </div>
                </li>
            </ul>
        </div></div>';
		return $html;
	}

	public function serviceReportType($data,$email,$returnBack,$dataCase, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-type";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">Choose Type Service</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps">
				<div class="content">
		        <ul class="table-view">
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/1.png" data-type-sid="1" data-type-name="Onsite" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/1.png">
		                  <div class="media-body">
		                    <p data="type">Onsite</p>
		                    <p style="display:none;" data="type_sid">1</p>
		                    <p>สำหรับงาน Service ที่ Site ลูกค้า</p>
		                  </div>
		              </a>
		            </li>
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/2.png" data-type-sid="2" data-type-name="Remote" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/2.png">
		                  <div class="media-body">
		                    <p data="type">Remote</p>
		                    <p style="display:none;" data="type_sid">2</p>
		                    <p>สำหรับงาน Remote จาก Office หรือบ้านไป Service ให้ลูกค้า</p>
		                  </div>
		              </a>
		            </li>
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/3.png" data-type-sid="3" data-type-name="Document" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/3.png">
		                  <div class="media-body">
		                    <p data="type">Document</p>
		                    <p style="display:none;" data="type_sid">3</p>
		                    <p>สำหรับงานทำ Document ที่ Office หรือบ้าน</p>
		                  </div>
		              </a>
		            </li>
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/4.png" data-type-sid="4" data-type-name="Meeting-Intranal" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/4.png">
		                  <div class="media-body">
		                    <p data="type">Meeting-Intranal</p>
		                    <p style="display:none;" data="type_sid">4</p>
		                    <p>สำหรับประชุมภายใน ที่ไม่ต้องมีเดินทาง </p>
		                  </div>
		              </a>
		            </li>
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/5.png" data-type-sid="5" data-type-name="Meeting-Extranal" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/5.png">
		                  <div class="media-body">
		                    <p data="type">Meeting-Extranal</p>
		                    <p style="display:none;" data="type_sid">5</p>
		                    <p>สำหรับประชุมที่ Site ลูกค้าหรือมีการเดินทาง</p>
		                  </div>
		              </a>
		            </li>
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/6.png" data-type-sid="6" data-type-name="Pre-Install" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/6.png">
		                  <div class="media-body">
		                    <p data="type">Pre-Install</p>
		                    <p style="display:none;" data="type_sid">6</p>
		                    <p>สำหรับทดสอบการ Install ภายใน Office</p>
		                  </div>
		              </a>
		            </li>
		          
		            <li class="table-view-cell">
		              <a class="navigate-right choose-type-service" data-type="../img/icon/7.png" data-type-sid="7" data-type-name="Testing" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
		                  <img class="media-object pull-left" src="../img/icon/7.png">
		                  <div class="media-body">
		                    <p data="type">Testing</p>
		                    <p style="display:none;" data="type_sid">7</p>
		                    <p>สำหรับทดสอบหรือเทส ภายใน Office</p>
		                  </div>
		              </a>
		            </li>
		          
		        </ul>
		    </div>
		</div>';
		
		return $html;
	}

	
	public function serviceReportAppointment($data,$email,$returnBack,$dataCase, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-appointment";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">Choose Appointment Datetime</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps">
		<div class="content">
            <ul class="table-view">
                <li class="table-view-divider">Appointment Date</li>
                <li class="table-view-cell cell-block" x-apple-data-detectors="false">
                    <!-- <div class="input-datetime" id="appointment_datetime"></div> -->
                    <div class="datepicker" x-apple-data-detectors="false">
                      <div id="title" class="labels">Thr, Dec 22, 2016</div>
                      <div class="block action addDay">+</div>
                      <div class="block action addMonth">+</div>
                      <div class="block action addYear">+</div>
                      <div></div>
                      <div class="block middle" id="day" x-apple-data-detectors="false">22</div>
                      <div class="block middle" id="month" x-apple-data-detectors="false">12</div>
                      <div class="block middle" id="year" x-apple-data-detectors="false">2016</div>
                      <div></div>
                      <div class="block action subDay">-</div>
                      <div class="block action subMonth">-</div>
                      <div class="block action subYear">-</div>
                      <!-- <div class="action-row"> -->
                        <!-- <div class="action" onclick="">Cancel</div> -->
                        <!-- <div class="action" onclick="">Reset</div> -->
                        <!-- <div class="action" onclick="">OK</div> -->
                      <!-- </div> -->
                    </div>
                </li>
                <!-- <li class="table-view-divider">Expect Finish Datetime</li>
                <li class="table-view-cell" style="padding-right:15px;">
                    <div class="input-datetime" id="expect_finish_datetime"></div>
                </li>
                <li class="table-view-cell"><div id="expect_duration"></div></li> -->
                <li class="table-view-divider">Appointment Time</li>
                <li class="table-view-cell cell-block">
                  <div class="datepicker">
                    <!-- <div id="title-time" class="labels"></div> -->
                    <div class="block action addHour">+</div>
                    <div class="block action addMinutes">+</div>
                    <!-- <div class="block noborder" >&nbsp;</div> -->
                    <div></div>
                    <div class="block middle" id="hour">13</div>
                    <div class="block middle" id="minutes">45</div>
                    <!-- <div class="block middle action toggleAMPM" id="ampm" >AM</div> -->
                    <div></div>
                    <div class="block action subHour">-</div>
                    <div class="block action subMinutes">-</div>
                    <!-- <div class="block noborder" >&nbsp;</div> -->
                    <!-- <div class="action-row">
                      <div class="action" onclick="">Cancel</div>
                      <div class="action" onclick="">Reset</div>
                      <div class="action" onclick="">OK</div>
                    </div> -->
                  </div>
                </li>
                <li class="table-view-divider">Expect Duration</li>
                <li class="table-view-cell cell-block">
                    <div class="datepicker">
                    
                    <div class="block action addHourExpect">+</div>
                    <div class="block action addMinutesExpect">+</div>
                    
                    <div></div>
                    <div class="block middle" id="hourExpect">2</div>
                    <div class="block middle" id="minutesExpect">0</div>
                    <div></div>
                    <div class="block action subHourExpect">-</div>
                    <div class="block action subMinutesExpect">-</div>
                    
                    <!-- <div class="action-row">
                      <div class="action" onclick="">Cancel</div>
                      <div class="action" onclick="">Reset</div>
                      <div class="action" onclick="">OK</div>
                    </div> -->
                    </div>
                </li>
                <li class="table-view-cell cell-block">
                  <div><a class="btn btn-positive btn-block" id="btnAppointment" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">Next</a></div>
                </li>
            </ul>
            
        </div></div>';
		
		return $html;
	}

	public function serviceReportEngineer($data,$email,$returnBack,$dataCase, $yourCreateInfo, $type_request){
		$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-engineer";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right btnNext" style="" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">Choose Engineer</span>
            </div>';
			$html .= '<div class="mhn-ui-row mhn-ui-apps">
			<div class="content menu-view-bottom_"><div>
        
          <div class="card">
            <ul class="table-view">';
              
              $html .= '<li class="table-view-cell">
                              <a href="javascript:void(0);">
                                <img class="media-object pull-left" src="'.$yourCreateInfo['employee_pic'].'">
                                <div class="media-body">
                                    <p>'.$yourCreateInfo['thainame'].'</p>
                                    <p>'.$yourCreateInfo['emailaddr'].'</p>
                                    <p>'.$yourCreateInfo['mobile'].'</p>
                                </div>
                              </a>
                              <div class="toggle" data-email="'.$yourCreateInfo['emailaddr'].'" data-thainame="'.$yourCreateInfo['thainame'].'" data-pic_employee="'.$yourCreateInfo['employee_pic'].'" data-mobile="'.$yourCreateInfo['mobile'].'">
                                <div class="toggle-handle"></div>
                              </div>
                        </li>';
      
                  
      $html .= '</ul>
          </div>
          <div class="card">
            <ul class="table-view">';

            foreach ($data as $key => $value) {
             $html .= '<li class="table-view-cell">
                    <a href="javascript:void(0);">
                      <img class="media-object pull-left" src="'.$value['pic_employee'].'">
                      <div class="media-body">
                          <p>'.$value['thainame'].'</p>
                          <p>'.$value['email'].'</p>
                          <p>'.$value['mobile'].'</p>
                      </div>
                    </a>
                    <div class="toggle" data-email="'.$value['email'].'" data-thainame="'.$value['thainame'].'" data-pic_employee="'.$value['pic_employee'].'" data-mobile="'.$value['mobile'].'" >
                      <div class="toggle-handle"></div>
                    </div>
              </li>';
              }
            $html .= '<li class="table-view-cell cell-block">
                  <div><a class="btn btn-positive btn-block btnNext" id="" href="#CreateServiceReport/'.$returnBack.'/right/'.$ticket_sid.'/'.rand(0,10000).'">Next</a></div>
                </li>';
            $html .= '</ul>
          </div>
    </div></div></div>';

    // print_r($data);
		return $html;
	}

public function ConfirmserviceReport($data,$email,$returnBack,$dataCase,$case_sr_of_ticket, $type_request){
	$ticket_sid = $dataCase['sid'];
		$this->ticket_sid = $ticket_sid;

		$classBtnReturnBack = "back-from-confirm";

		$html = '<div class="mhn-ui-app-title-head" style="text-align:center;">
            '.$this->genBtnReturnBackBetweenProcess($returnBack,$classBtnReturnBack).' 
            <a class="pull-right" style="visibility: hidden;" href="javascript:void(0);">
                <span class="icon icon-right-nav"></span>
            </a>
            <span class="mhn-ui-page-title">Confirm Create Service Report</span>
            </div>';
		$html .= '<div class="mhn-ui-row mhn-ui-apps">
		<div class="content">
        <div class="card">
          <ul class="table-view">
            <li class="table-view-divider">End User</li>
            <li class="table-view-cell" style="padding-right:15px;">
                <p>Name <span class="pull-right">'.$case_sr_of_ticket['end_user']['name'].'</span></p>
            </li>
            <li class="table-view-cell" style="padding-right:15px;">
                <p>Email <span class="pull-right">'.$case_sr_of_ticket['end_user']['email'].'</span></p>
            </li>
            <li class="table-view-cell" style="padding-right:15px;">
                <p>Phone <span class="pull-right">'.$case_sr_of_ticket['end_user']['phone'].'</span></p>
            </li>
            <li class="table-view-cell" style="padding-right:15px;">
                <p>Mobile <span class="pull-right">'.$case_sr_of_ticket['end_user']['mobile'].'</span></p>
            </li>
            <li class="table-view-cell" style="padding-right:15px;">
                <p>Company <span class="pull-right">'.$case_sr_of_ticket['end_user']['company'].'</span></p>
            </li>
          </ul>
        </div>
        <div class="card">
          <ul class="table-view">
          	<li class="table-view-divider" style="padding-right:15px;">Service Info</li>
            <li class="table-view-cell cell-block"><div><p>Subject <span class="pull-right">'.$case_sr_of_ticket['subject'].'</span></p></li>
            <li class="table-view-cell cell-block"><div><p>Address <span class="pull-right">'.$case_sr_of_ticket['address'].'</span></p></div></li>
            <li class="table-view-divider" style="padding-right:15px;">Type Service</li>
            <li class="table-view-cell" style="padding-right:15px;">
                <img class="media-object pull-left" src="'.$case_sr_of_ticket['type'].'">
                <p>'.$case_sr_of_ticket['type_name'].'</p>
            </li>
          </ul>
        </div>
        <div class="card">
          <ul class="table-view">
            <li class="table-view-divider">Engineer</li>';
              foreach ($case_sr_of_ticket['engineer_info'] as $key => $value) {
                  $html .= '<li class="table-view-cell" style="padding-right:15px;">
                            <img class="media-object pull-left" src="'.$value['employee_pic'].'">
                            <div class="media-body">
                                <p>'.$value['thainame'].'</p>
                                <p>'.$value['emailaddr'].'</p>
                            </div>
                          </li>';
              }
          
          if($type_request=="web"){
          		$href = 'javascript:void(0);';
          }else{
	          $dataReturnBack = explode("|", $returnBack);
	          $href = '#'.$dataReturnBack['1'].'/'.$dataReturnBack[0].'/right/'.$this->ticket_sid.'/3';
	      }
          $html .= '
         	<li class="table-view-divider" style="padding-right:15px;">Appointment</li>
            <li class="table-view-cell cell-block"><div><p>Appointment Datetime <span class="pull-right">'.$case_sr_of_ticket['appointment_datetime'].'</span></p></div></li>  
            <li class="table-view-cell cell-block"><div>
                <p>Expect Duration <span class="pull-right">'.$case_sr_of_ticket['expect_duration']['hours'].' Hours '.$case_sr_of_ticket['expect_duration']['minutes'].' Minutes</span></p>
            </li>
          </ul>
        </div>
        <ul class="table-view">
          <li class="table-view-cell" style="padding-right:15px;">
              <div>
                  <a id="btnCreateServiceReport" href="'.$href.'" class="btn btn-block btn-positive">Done</a>
              </div>
          </li>
        </ul>
    </div></div>';
		// print_r($case_sr_of_ticket);
		return $html;
	}
}
?>