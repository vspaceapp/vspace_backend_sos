<?php class FlowManagementAppView{
	public static function view($email, $orga, $permission_role){
		$html = '<link rel="stylesheet" href="http://vspace.in.th/production/css3-tree-family/css/style.css" />';
		$html .= '<style>
					.sidebar-open .content-wrapper-properties, .sidebar-collapse .content-wrapper-properties { overflow: auto;}
					.tree {
					    /* width: 1000px; */
					    width: initial;
					    margin: 0 auto;
					    /* overflow: auto; */
					    text-align: center;
					}
					.tree ul ul ul {margin-left:20px;}
					.tree li a {
    					max-width: 135px;
					}
					.tree li a:hover, .tree li a:hover+ul li a {
						background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
					}
					.tree li a:hover+ul li a {
						background: none; color: initial; border: 1px solid #cccccc;
					}
					#permission-user {
						position:absolute;
						width: 100%;
						right:0;
						bottom: 0;
						height:50%;
						background: #222;
						color: #ccc; padding:5px;
						-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.8);
    					box-shadow: 0 0 5px rgba(0, 0, 0, 0.8);
    					overflow-y: auto;
    					overflow-x: hidden;
					}
					.play-user {float:left; width:30%;}
					.play-permission {float:left; width: 70%;}
					.play-permission::before {content:"Permission"; position:absolute; top:0px;}
					.users-list-name {color: #ffffff;}
					.tree li a {
						font-size: 24px;
						
					}
				</style>
				<div class="object content">
                        <div class="box-header with-border"><h3 class="box-title"><i class="fa fa-fw fa-calendar"></i> Flow Management</h3></div>
                        <div class="row" id="object-body" style="">
						<div class="tree"><ul>';
				$html .= '<li><a href="javascript:void(0);">Project</a>
							<ul>
								<li>
									<a href="javascript:void(0);">Pre Sale</a>
									<ul>
										<li><a href="javascript:void(0);">POC</a></li>
										<li><a href="javascript:void(0);">Implement</a>
										</li>										
									</ul>
								</li>
								<li><a href="javascript:void(0);">Post Sale</a>
									<ul>
										<li><a title="Implement" href="javascript:void(0);">Implement</a>
										</li>
										<li><a title="Preventive Maintenance" href="javascript:void(0);">PM</a>
										</li>
										<li><a title="Install" href="javascript:void(0);">Install</a>
										</li>
										<li><a title="Incident" href="javascript:void(0);">Incident</a>
										</li>
										<li><a title="Request" href="javascript:void(0);">Request</a>
										</li>
										<li><a title="Question" href="javascript:void(0);">Question</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>';
				$html .= '</ul>
							<!--<ul>
								<li><a href="javascript:void(0);">Onsite</a></li>
								<li><a href="javascript:void(0);">Remote</a></li>
								<li><a href="javascript:void(0);">Document</a></li>
								<li><a href="javascript:void(0);">Meeting-Intranal</a></li>
								<li><a href="javascript:void(0);">Meeting-Extranal</a></li>
								<li><a href="javascript:void(0);">Pre-Install</a></li>
								<li><a href="javascript:void(0);">Testing</a></li>
							</ul>-->
				</div><div style="clear:both;"></div>';
				$html .= '</div></div>';
		// $hmtl .= $email;
				// $html .= self::script();
		return $html;
	}

	public static function script(){
		$script = '<script>
			$(".tree").on("click","a",function(){
				servicePermissionOfRoleSid($(this).data("role-sid")).done(function(res){
					console.log(res);
					playPermissionAndUser(res);
				});
			});
			var playPermissionAndUser = function(res){
				$("#permission-user").remove();
				$elm = $("<div>");
				$elm.append("<div id=\"permission-user\"><div class=\"box-tools pull-right\" style=\"margin-right: 10px;\"><button type=\"button\" class=\"btn btn-box-tool\" id=\"closePlayPermissionUser\" data-widget=\"remove\"><i class=\"fa fa-times\"></i></button></div></div>");
				$elm.find("#permission-user").append("<div>"+res.permission.name+"</div>");

				$elm.find("#permission-user").append("<div class=\"play-user\"><ul class=\"users-list clearfix\"></ul></div>");
				$.each(res.user, function(k,v){
					if(v.active==1 && v.picture){
						$elm.find(".play-user .users-list").append("<li><img src=\""+v.picture+"\" ><a class=\"users-list-name\" href=\"javascript:void(0)\">"+v.name+"</a><span class=\"users-list-date\"></span></li>");
					}
				});

				var create_project = "<i class=\"fa fa-fw fa-close\"></i>";
				if(res.permission.project_create_able && res.permission.project_create_able !="0"){
					create_project = "<i class=\"fa fa-fw fa-check\"></i>";
				}
				$elm.find("#permission-user").append("<div class=\"play-permission\"><div>Create Project "+create_project+"</div></div>");	

				var case_create_able = "<i class=\"fa fa-fw fa-close\"></i>";
				if(res.permission.case_create_able && res.permission.case_create_able !="0"){
					case_create_able = "<i class=\"fa fa-fw fa-check\"></i>";
				}
				$elm.find("#permission-user .play-permission").append("<div>Create Case "+case_create_able+"</div>");

				var can_create_service = "";
				if(res.permission.can_create_service=="0"){
					can_create_service = "Not";
				}else if(res.permission.can_create_service=="1"){
					can_create_service = "When is owner case";
				}else if(res.permission.can_create_service=="2"){
					can_create_service = "All Case";
				}
				$elm.find("#permission-user .play-permission").append("<div>Create Service: "+can_create_service+"</div>");		
				

				$elm.find("#permission-user").append("<div style=\"clear:both;\"></div>");

				$(".object.content").append($elm.html());
			}
			$("body").on("click","#closePlayPermissionUser", function(){
				$("#permission-user").remove();
			});
			var servicePermissionOfRoleSid = function(role_sid){
				return $.ajax({
					type: "POST",
					url: END_POINT_2+"v1/role/getPermissionAndUserOfRoleSid",
					data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),role_sid:role_sid},
				});
			}
		</script>';
		return $script;
	}
}