<?php class ProjectManagementAppSettingView {

	public static function view(){
		$html = '';
		$html .= '<div id="" class="form-project " style="margin: 30px 15px; background-color:#FFFFFF; border-radius: 10px 10px 10px 10px;">
						<div class="row"     style="background-color: initial;"">
							<div class="col-xs-1"></div>
							<div class="col-xs-11">
								<ul class="table-view" style="border: 1px solid #f1f1f1;border-radius: 0px 10px 0px 0px;">
						            <li class="table-view-cell">
						              	<i class="media-object pull-left fa fa-fw fa-chevron-down style="font-size:18px;"></i>
						                  <div class="media-body">
						                    <p data="type" style="line-height:35px; font:"#FFFFF";><B>Order By</B></p>
						                  </div>
						            </li>
						        </ul>
							</div>
							<div class="col-xs-1">
								<div style="margin: 10px 10px;"><i class="fa fa-fw fa-arrow-circle-right" style="color:#4F4F4F; "></i></div>
							</div>
							<div class="col-xs-11">
								<ul class="table-view project-orber-by" style="border: 1px solid #f1f1f1;border-radius: 0px 0px 10px 0px;">
						            <li class="table-view-cell project-order-by-cell">
						              	<i class="media-object pull-left fa fa-fw fa fa-calendar" style="font-size:18px;"></i>
						                  <div class="media-body">
						                    <p data="type" style="line-height:30px;">Created Datetime</p>
						                  </div>
						            </li>
						            <li class="table-view-cell project-order-by-cell">
						              	<i class="media-object pull-left fa fa-fw fa-edit"></i>
						                  <div class="media-body">
						                    <p data="type" style="line-height:30px;">Contract</p>
						                  </div>
						            </li>
						            <li class="table-view-cell project-order-by-cell">
						              	<i class="media-object pull-left fa fa-fw fa-list-ul"></i>
						                  <div class="media-body">
						                    <p data="type" style="line-height:30px;">Type</p>
						                  </div>
						            </li>
						            <li class="table-view-cell project-order-by-cell" style="border-bottom:none;">
						              	<i class="media-object pull-left fa fa-fw fa-user"></i>
						                  <div class="media-body">
						                    <p data="type" style="line-height:30px;">Owner</p>
						                  </div>
						            </li>
						        </ul>
							</div>
						</div>
						
					</div>';
		return $html;

	}

	// <!--<select id="sort" class="form-control input-sm">
	//                       <option selected="" value="P.create_datetime">ORDER BY CREATE</option>
	//                       <option value="P.contract">ORDER BY CONTRACT</option>
	//                       <option value="P.project_type">ORDER BY TYPE</option>
	//                       <option value="PO.owner">ORDER BY OWNER</option>
	//                     </select>-->
}