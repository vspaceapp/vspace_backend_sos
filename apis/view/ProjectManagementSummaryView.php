<?php class ProjectManagementSummaryView{
	private $engineer;
  public static function calOvertimeExpect($data){
      $overtime = 0;
      foreach ($data as $key => $value) {
        foreach ($value['task'] as $k => $v) {
            $overtime += intval($v['overtime_expect']);
        }
      }
      return $overtime;
  }
	public static function view($engineer,$man_hours_total, $dataCase){
		$engineer;

    $total_minute = $man_hours_total*60;
		$html = '<div class="row">';
        $html .= '<div class="col-md-5">
                    <table class="table table-bordered table-case " style="width: initial;">
                     <tbody><tr>
                      <th style="width:200px;text-align: left;" title="">Engineer</th>
                      <th style="width:50px;text-align: right;">Times</th>
                      <th style="width:100px;text-align: right;">All Service</th>
                      <th style="width: 50px;text-align: right;">Taxi(baht)</th>
                      </tr>';
        $timesAll = 0;
        $useMinutesAll = 0;
        $taxiAll = 0;

        if(count($engineer)>0){
            foreach ($engineer as $key => $value) {
                  $timesAll += $value['times'];
                  $useMinutesAll += $value['use_minutes'];
                  $taxiAll += $value['taxi'];

                  $hours = floor($value['use_minutes']/60);
                  $minutes_hours = $value['use_minutes']%60;

                  $html .= '<tr>
                      <td style="text-align: left;">
                        <img class="media-object pull-left" src="'.$value['pic'].'"> <span><div style="clear:both;"><small style="font-size:11px; overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">'.$value['thainame'].'</small></div><div style="clear:both;"><small style="font-size:11px; overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">'.$value['engineer'].'</small></div></span>
                      </td>
                      <th style="width:50px;text-align: right;">'.$value['times'].'</th>
                      <td style="text-align: right;">'.$hours.'.'.$minutes_hours.' Hr.</td>
                      <td style="text-align: right;">'.$value['taxi'].'</td>
                    </tr>';
            }
            
            $hoursAll = floor($useMinutesAll/60);
            $minutes_hours_all = $useMinutesAll%60;
            $html .= '</tbody>
                          <tfoot>
                           <tr>
                            <td></td>
                            <td style="text-align: right;">'.$timesAll.'</td>
                            <td style="text-align: right;">'.$hoursAll.'.'.$minutes_hours_all.' Hr.</td>
                            <td style="text-align: right;">'.$taxiAll.'</td>
                            </tr>
                          </tfoot>';
        }else{
            $html .= '<tr><td colspan="4" style="text-align:center;">No Data</td></tr>';
        }
        $html .= '</table></div>';
            // END col-md-5
        if($total_minute){
          $endPercent = ($useMinutesAll/$total_minute);
        }else{
          $endPercent = 0;
        }       
        $html .= '<div class="col-md-3">
                      <div class="report-statistic-box">
                        <div class="box-header">Completed</div>
                          <div class="box-content delivery-rate" style="width: 130px;height: 130px;">
                            <div class="percentage">'.number_format($endPercent*100,1).'%</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                          <div class="report-statistic-box">
                            <div class="box-header">Taxi</div>
                            <div class="box-content"><div class="sentTotal">'.number_format($taxiAll).'</div></div>
                            <div class="box-foot">
                              <span class="arrow arrow-up"></span>
                              <span class="arrow arrow-down"></span>
                              <div class="box-foot-right" style="margin-top: -40px;">Baht.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                      <div class="report-statistic-box">
                        <div class="box-header">Overtime</div>
                        <div class="box-content"><div class="sentTotal open-rate">'.self::calOvertimeExpect($dataCase).'</div></div>
                        <div class="box-foot">
                          <span class="arrow arrow-up"></span>
                          <span class="arrow arrow-down"></span>
                          <div class="box-foot-right" style="margin-top: -40px;">Hr.</div>
                        </div>
                      </div>
                    </div>
        </div><script>$(".delivery-rate").percentCircle({width: 130,trackColor: "#ececec",barColor: "#f5ab34",barWeight: 5,endPercent: '.$endPercent.',fps: 60});</script>';
        return $html;
	 }
    public static function infoProject($projectDetail,$engineer,$man_hours_total, $dataCase, $email="", $listUserCanAddProject = array()){
        $html = '<style>
          #createDummyContract, #selectProjectStatus {
            display: none;
          }
          .dropdown-menu {
            max-height: 300px;
            overflow: auto;
          }
        </style><div class="container-user-administration-page" style=""><div class="content menu-view-bottom ">';

        $html .= '<div class="profile">
                      <div class="picture">
                        <!--<div class="badge green"><i class="fa fa-check"></i></div>-->
                      </div>
                      <div class="details" >
                          <div class="tags">
                            <div class="tag orange">'.$projectDetail['project_detail']['name'].'</div>
                            <div class="tag green">'.$projectDetail['project_detail']['contract_no'].'</div>
                            <div class="tag yellow">'.$projectDetail['project_detail']['phase'].'</div>
                          </div>
                          <!--<div class="title"> - </div>
                          <div class="titulus"> - </div>-->
                          <div class="description">
                            
                            <div class="info-row"><i class="fa fa-clock-o"></i><span class="caption">Man Hours:</span><span class="">'.$projectDetail['man_hours_total'].'</span></div>
                            <div class="info-row"><i class="fa fa-calendar"></i><span class="caption">Start - End:</span><span class="">'.$projectDetail['project_detail']['project_start'].' - '.$projectDetail['project_detail']['project_end'].'</span></div>
                            '.((isset($projectDetail['project_detail']['end_user']) && $projectDetail['project_detail']['end_user']!="")?'
                                                          <div class="info-row">
                                                            <div class="info-block"><i class="fa fa-home"></i><span class="">'.$projectDetail['project_detail']['end_user'].'</span></div>
                                                            <div class="info-block"><i class="fa fa-map-marker"></i><span class="">'.$projectDetail['project_detail']['end_user_address'].'</span></div>
                                                          </div>':'').'
                          </div>
                      </div>
                      <div class="functions">
                        <!--<div class="func"><i class="fa fa-envelope"></i></div>
                        <div class="func"><i class="fa fa-comments"></i></div>-->
                        <!--<div class="func"><i class="fa fa-edit"></i></div>-->
                        <!--<div class="func"><i class="fa fa-flag"></i></div>
                        <div class="func"><i class="fa fa-share-alt"></i></div>-->
                        <!--<div class="func"><i class="fa fa-trash"></i></div>-->
                      </div>';
        
        $deleteProject = '';
        foreach ($projectDetail['owner'] as $key => $value) {
            
            $canChangeOwnerProject = '';
            $changeManday = '';
            

            if($email=="autsakorn.t@firstlogic.co.th" || 1){
              if($projectDetail['project_detail']['create_project_by']==$email){


               $changeManday = '<div>Change Man Days<span class="pull-right"><button type="button" class="btn" onclick="changeManday(\''.$value['project_owner_sid'].'\',\''.$value['man_days'].'\',1)">+</button><button type="button" class="btn" onclick="changeManday(\''.$value['project_owner_sid'].'\',\''.$value['man_days'].'\',-1)">-</button></span></div><div style="clear:both;"></div>';

               $canChangeOwnerProject = '
               <div class="dropdown modify">
               <button class="btnModify btn" type="button"  data-toggle="dropdown" id="dropdownMenu1">Change Staff <span class="caret"></span></button>
               <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
                  foreach ($listUserCanAddProject as $kk => $vv) {
                    $canChangeOwnerProject .= '<li><a onclick="changeStaff(\''.$value['project_sid'].'\',\''.$value['project_owner_sid'].'\',\''.$vv['emailaddr'].'\');" href="javascript:void(0);">'.$vv['thainame'].'</a></li>';
                  }
               $canChangeOwnerProject .= '</ul>
               </div>';

               if(count($dataCase)==0){
                $deleteProject = '<div style="clear:both;"></div><div><span class="pull-right"><button class="btn" onclick="deleteProject(\''.$value['project_sid'].'\')">Delete Project</button></span></div>';
                }
                if($projectDetail['project_detail']['data_status']<0){
                  $deleteProject = '<div style="clear:both;"></div><div><span class="pull-right">Project Deleted</span></div>';
                  $changeManday = '';
                  $canChangeOwnerProject = '';
                }

              }
            }

             $html .= '<div class="profile"  style="clear:both;">
                        <div class="picture"><img src="'.$value['pic_full'].'">
                          <!--<div class="badge green"><i class="fa fa-check"></i></div>-->
                        </div>
                        <div class="details" style="float: left;">
                            <div class="description">
                              <!--<div class="title"> - </div>
                              <div class="titulus"> - </div>-->
                              <div class="info-row"><div class="info-block"><span class=""></span><span class="">'.$value['thainame'].'</span></div></div>
                              <div class="info-row">
                                <div class="info-block"><i class="fa fa-envelope"></i><span class="">'.$value['email'].'</span></div>
                              </div>
                              <div class="info-row"><i class="fa fa-clock-o"></i><span class="caption">Man Days:</span><span class="">'.$value['man_days'].'</span>'.$changeManday.'</div>
                              <!--<div class="info-row"><i class="fa fa-clock-o"></i><span class="caption">Man Hours:</span><span class="">'.$value['man_hours'].'</span></div>-->
                              '.(($value['project_owner_status']=="1")?$canChangeOwnerProject:"Removed").'
                            </div>
                        </div>
                    </div>';
            $html .= ProjectManagementSummaryView::scriptModify();
        }

        $html .= '<div style="clear:both;"></div><div class="pull-right">Create By: '.$projectDetail['craeate_project_thainame'].'<br/>'.$projectDetail['create_datetime_df'].'</div>';
        $html .= $deleteProject;

        $html .= '<hr style="clear:both; padding-top:20px;" />';
        $html .=  '<div class="logs" style="clear:both;">
                  '.ProjectManagementSummaryView::view($engineer,$man_hours_total, $dataCase).'
                  </div>
                </div>';
        $html .= '</div></div>';
        return $html;
    }

    public static function scriptModify(){
        $html = '<script>';
        $html .= '
        function changeStaff(project_sid,project_owner_sid, emailaddr){
          serviceChangeStaff(project_sid, project_owner_sid, emailaddr).done(function(res){
            $(".box_data.active").click();
          });
        }
        function serviceChangeStaff(project_sid, project_owner_sid, emailaddr){
          return $.ajax({
            type: "POST", url: END_POINT_2+"v1/projectmodify/changestaff",
            data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),project_sid:project_sid,project_owner_sid:project_owner_sid,new_staff_email:emailaddr},
            error:function(){
              alert("Unsuccess!");
            }
          });
        }
        function changeManday(project_owner_sid,man_days,changeDay){
          var man_days_new = parseInt(man_days)+parseInt(changeDay);
          
          serviceChangeManDays(project_owner_sid,man_days_new).done(function(res){
            $(".box_data.active").click();
          });
        }
        function serviceChangeManDays(project_owner_sid,man_days_new){
          return $.ajax({
            type: "POST", url: END_POINT_2+"v1/projectmodify/changemanday",
            async:false,
            data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),project_owner_sid:project_owner_sid,man_days_new:man_days_new},
            error:function(){
              alert("Unsuccess!");
            }
          });
        }

        function deleteProject(project_sid){
          if(confirm("Confrim Delete Project")){
            serviceDeleteProject(project_sid).done(function(res){
              $(".box_data.active").click();
            });
          }
        }
        function serviceDeleteProject(project_sid){
           return $.ajax({
            type: "POST", url: END_POINT_2+"v1/projectmodify/deleteproject",
            async:false,
            data: {email:localStorage.getItem("case_email"),token:localStorage.getItem("case_token"),project_sid:project_sid},
            error:function(){
              alert("Unsuccess!");
            }
          });
        }
        ';
        // $html .= '$(".modify").on("click",".btnModify", function(){
        //     $(".form-project").hide();
        //     // alert("123");
        //     $(".control-sidebar").attr("class","control-sidebar control-sidebar-dark control-sidebar-open open animated bounceInRight");
        // })';
        $html .= '</script>';
        return $html;
    }
}
?>