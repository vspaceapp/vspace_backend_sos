<?php class FormEndUserView {
	public static function view(){
		$html = '<style>
					#FormEndUserView {display:none;height:90%;}
					#FormEndUserView .table-view-cell {
						padding-top:0px;
					}
				</style>';
		$html .= '<div id="FormEndUserView" class="content">
                  <ul class="table-view">
                    <li class="table-view-divider">Name</li>
                    <li class="table-view-cell" style="padding-right:15px;">
                      <input id="name" type="text" value="">
                    </li>
                    <li class="table-view-divider">Email</li>
                    <li class="table-view-cell" style="padding-right:15px;">
                      <input id="email" type="text" value="">
                    </li>
                    <li class="table-view-divider">Phone</li>
                    <li class="table-view-cell" style="padding-right:15px;">
                      <input id="phone" type="text" value="">
                    </li>
                    <li class="table-view-divider">Mobile</li>
                    <li class="table-view-cell" style="padding-right:15px;">
                      <input id="mobile" type="text" value="">
                    </li>
                    <li class="table-view-divider">Company</li>
                    <li class="table-view-cell" style="padding-right:15px;">
                      <input id="company" type="text" value="">
                    </li>
                    <li class="table-view-cell" style="padding-right:15px;padding-top: 10px;">
                        <div style="float:left; width:30%;">
                            <a href="javascript:void(0);" style="color:#ffffff;" class="btn btn-block btn-warning" id="BackToMain">Back</a>
                        </div>
                        <div style="float:left; width:68%; margin-left:2%;">
                            <a href="javascript:void(0);" class="btn btn-block btn-positive" id="btnChangeEnduser">Done</a>
                        </div>
                    </li>
                  </ul>
              </div>';
        return $html;
	}
}