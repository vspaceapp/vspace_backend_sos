<?php class ProjectManagementCasesView{

    public static function view($data){
        $html = '';
        if(count($data)>0){
            $html .= '<div class="box box-warning box-solid">
                        <div class="box-header with-border">
                          <h3 class="box-title">List Case In Project</h3>
                        </div>
                        <div class="box-body">';
                        foreach ($data as $key => $value) {
                            $html .= '<table class="table table-case table-condensed table-bordered" style="width: 100%; margin-bottom:10px; font-size:12px;">
                            <tr>
                              <td style="width:85px;"><label>'.$value['no_ticket'].'</label></td>
                              <td style="width:85px;"><label>'.$value['case_type'].'</label></td>
                              <td colspan="6"><label title="'.$value['subject'].'">'.$value['subject'].'</label></td>
                              <td><small title="'.$value['thainame'].'">Created by '.$value['thainame'].'</small></td>
                            </tr>';
                                foreach ($value['task'] as $k => $v) {
                                    
                                 $html .= '<tr>
                                      <td></td>
                                      <td><label title="'.$v['no_task'].'">'.$v['no_task'].'</label></td>
                                      <td colspan="6" title="'.$v['subject_service_report'].'">'.$v['subject_service_report'].'<span class="pull-right">'.$v['pdf_report'].'</span></td>
                                      <td>'.$v['service_type_name'].'</td>
                                    </tr>';
                                    if(count($v['timestamp'])>0){
                                        $html .= '<tr>
                                              <td></td>
                                              <td></td>
                                              <td style="width: 100px;">Engineer</td>
                                              <td style="width:100px;">Started</td>
                                              <td style="width:100px;">Closed</td>
                                              <td style="width:100px;">Taxi On</td>
                                              <td style="width:100px;">Taxi Out</td>
                                              <td style="width:100px;">Hours Used</td>
                                              <td style="width:100px;">Cal Hours Used</td>
                                        </tr>';
                                        foreach ($v['timestamp'] as $kk => $vv) {
                                            $hours = floor($vv['use_minutes']/60);
                                            $minutes_hours = $vv['use_minutes']%60;
                                            $html .= '<tr>
                                              <td></td>
                                              <td></td>
                                              <td style="width: 100px;" title="'.$vv['thainame'].'">'.$vv['thainame'].'</td>
                                              <td style="width:100px;">'.$vv['start_job'].'</td>
                                              <td style="width:100px;">'.$vv['close_job'].'</td>
                                              <td style="width:100px;">'.$vv['taxi_fare_1'].'</td>
                                              <td style="width:100px;">'.$vv['taxi_fare_2'].'</td>
                                              <td>'.$hours.' Hr. '.$minutes_hours.' Min.</td>
                                              <td>'.$hours.' Hr. '.$minutes_hours.' Min.</td>
                                            </tr>';
                                        }
                                    }else{
                                         $html .= '<tr>
                                              <td></td>
                                              <td></td><td colspan="7" style="text-align:center;">No Data Timestamp Service</td>
                                        </tr>';
                                    }

                                }
                            $html .= '</table>';
                        }
                       
            $html .= '</div>
                      </div>';
                    
        }else{
            $html .= '<div class="box box-warning box-solid">
                        <div class="box-body">
                          <div style="text-align: center;">Not Found Case In This Project</div>
                        </div>
                    </div>';
        }
                // </div>';
        return $html;
    }
	public static function viewKaea($data){
		$html = '';
		$task = 0;
		$timestamp = 0;
		$html .='<table class="table-case table table-bordered table-condensed" style="width: 100%;">';
		foreach ($data as $key => $value) {
				$html .= '
 						<tbody style="width: 100px;"><tr>
        					<th>'.$value['no_ticket'].'</th>
       				    	<th>'.$value['case_type'].'</th>
        					<th>'.$value['subject'].'</th>
        					<th></th>
        					<th></th>
        					<th></th>
        					<th></th>
        					<th></th>
        					<th>created by<br>'.$value['thainame'].'</th>
      					 </tr>
    				 ';
    		foreach ($value['task'] as $k => $v) {

    		 	$html .=  '<tr><td></td><td>'.$v['no_task'].'<td><td></td><td></td><td></td><td></td><td></td><td>'.$v['service_type_name'].'</td></tr>';	
    		 	$html .= '<tr><td></td><td></td><td>Engineer</td><td>Started</td><td>Closed</td><td>Taxi On</td><td>Taxi Out</td><td>Hours Used</td><td>Cal Hours Used</td></tr>';
    		
    			foreach ($v['timestamp'] as $ke => $t) {
    				$html .= '<tr><td></td><td></td><td>'.$t['thainame'].'</td><td>'.$t['start_job'].'</td><td>'.$t['close_job'].'</td><td>'.$t['taxi_fare_1'].'</td><td>'.$t['taxi_fare_2'].'</td><td>'.$t['use_minutes'].'</td></td><td>'.$t['use_minutes'].'</td></tr>';
						
    				
    			}
    		}	 		
    	
		}
		$html .= '</tbody></table>';
		return $html;
	}
}
?>