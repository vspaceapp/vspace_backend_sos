<?php class SignatureServiceReportView{

  public static function view($data = array()){
      $sourcePath = 'http://vspace.in.th/pages/signaturepad/customerApp/';
      $html = '';
      $html .= '<link rel="stylesheet" href="'.$sourcePath.'css/signature-pad.css">
                <style type="text/css">
                  #signature-pad .m-signature-pad--header .description::before {
                    content: "Action";
                    position: absolute;
                    top: 0px;
                    left: 20px;
                    font-weight: bold;
                  }
                  #signature-pad .description {
                    text-align:center;color:#222222;
                  }
                  #signature-pad .m-signature-pad--header .description {
                    text-align: left;
                  }
                  body {background:#2c3e50;}
                  .m-signature-pad--header {
                    left: 20px;
                    right: 20px;
                    bottom: 4px;
                    height: 48%;
                  }
                  .m-signature-pad--body{
                    top: 50%;
                  }
                  @media screen and (max-height: 320px){
                    .m-signature-pad--header .description {
                        font-size: 1em;
                        /*margin-top: 1em;*/
                    }
                    .m-signature-pad--header .description {
                        /*color: #C3C3C3;*/
                        color: #222222;
                        text-align: center;
                        font-size: 1.2em;
                        /*margin-top: 1.8em;*/
                    }
                  }
                    @media screen and (max-width: 1024px) {
                      .m-signature-pad {
                          margin: 0% !important;
                          top:-10px;
                          height: 102%;
                      }
                  }
                  .m-signature-pad--header .description {
                      padding: 20px;
                      font-size: 14px;
                      height: 95%;
                      /* padding-top: 30px; */
                      border: 1px solid #f1f1f1;
                      margin: 20px;
                      padding-bottom: 10px;
                  }
                  .m-signature-pad--footer .button {
                    color: #222222;
                  }
                  #signature-pad { display:none;}
                  //#before_signature {display:none;}
                </style>
              </head>
              <body >
                <!--<div id="before_signature" class="content">
                    <div id="action_input"></div>
                    <div class="mhn-ui-bottom-link-bar">
                        <a href="javascript:void(0);" id="BackToMain2" style="margin-right:50px;"><span class="mhn-ui-bottom-btn icon icon-left-nav"></span></a>
                        <a href="javascript:void(0);" id="SignpadForm"><span class="mhn-ui-bottom-btn icon icon-right-nav"></span></a>
                    </div>
                </div>-->
                <div id="signature-pad" class="m-signature-pad">
                  <div class="m-signature-pad--header">
                    <div class="description" id="action_input">
                      Sign New
                    </div>
                    
                  </div>
                  <div class="m-signature-pad--body">
                    <canvas></canvas>
                  </div>
                  <div class="m-signature-pad--footer">
                    <div class="description">Sign above</div>
                    <button type="button" class="button clear" id="BackToMain2" data-action="clear" style="">Back</button>
                    <!--<a  class="button back" href="#profile"><button type="button">Back</button></a>-->
                    <button type="button" id="saveSignatureCustomer" class="button save" data-action="save">Save</button>
                  </div>
                 
                    <input class="output" name="output" value=""  type="hidden" />
                </div>
                ';
      return $html;
    }
}
?>