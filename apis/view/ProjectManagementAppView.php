<?php class ProjectManagementAppView{

	public static $email;
	private static $data;
	private static $search;
	private static $permission_role;
	private static $canAssignTo;
	private static $windowHeight;
	public static function controlSidebar(){
		require_once 'ProjectManagementAppSettingView.php';

		$html = '';
		$html .= '<div class="control-sidebar-setting ">
					<div class="box-tools pull-right" style="margin-right: 10px;">
						<button type="button" class="btn btn-box-tool" id="closeSettingProject" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
					'.((self::$email=="autsakorn.t@firstlogic.co.th" && 1==2)?ProjectManagementAppSettingView::view():'').'
				  </div>';
		return $html;

	}
	public static function listProject($email, $data, $order_by, $search, $permission_role,$canAssignTo,$windowHeight){
		self::$email = $email;
		self::$data = $data;
		self::$search = $search;
		self::$permission_role = $permission_role;
		self::$canAssignTo = $canAssignTo;
		self::$windowHeight = $windowHeight;

		$html = '
				<link href="http://vspace.in.th/pages/libs/jquery-ui-date-range-picker/css/style.css" rel="stylesheet"/>
				<link href="http://vspace.in.th/pages/lib/email-report-overview/css/style.css?v=5" rel="stylesheet" type="text/css"/>
				<link href="http://vspace.in.th/pages/projectmanagementv1/user-administration-page/css/style.css" rel="stylesheet" type="text/css"/>
				<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
				<style>.info-left, .info-left .fa,.info-left .fa.fa-calendar{ font-size:11px;}.fa-calendar {font-size:11px;}
				.report-statistic-box, .report-statistic-box .box-header {background-color: initial;}
				.report-statistic-box {border: 1px solid #999999;}
				.container-user-administration-page {position: absolute; top:0; bottom:0; left:0; right:0;}.report-statistic-box {float:left;width:31%;}
				.container-user-administration-page > .content .profile .picture img {
				    width: 4rem;
				    height: 4rem;
				    -moz-border-radius: 4rem;
				    -webkit-border-radius: 4rem;
				    border-radius: 4rem;
				}
				.report-statistic-box .box-content .percentage {position:initial;} .report-statistic-box .box-content .sentTotal {font-size:24px;}
				.control-sidebar-setting,.control-sidebar {
					display:none;
				}
				.control-sidebar-setting.open{
					z-index: 100;
				    color: #222;
				    position: absolute;
				    background-color: white;
				    right: 0;width: 100%;height: 100%;
				    border-radius: 10px 10px 10px 10px;
				    display:block;
				}
				.control-sidebar.open {
					z-index: 100;
				    //color: #222;
				    position: absolute;
				    //background-color: white;
				    right: 0;width: 100%;height: 100%;
				    border-radius: 10px 10px 10px 10px;
				    display:block;
				    overflow:hidden;
				}
				.mhn-ui-page .control-sidebar-setting .table-view p {
    				color: #222222;
				}
				.control-sidebar-setting .table-view .table-view-cell {
				    background-image: none;
				    background-repeat: no-repeat;
				    background-position: 15px 100%;
				    border-bottom: inherit;
				}
				.project-orber-by li:nth-child(1){
					background-color: #f1f1f1;
				}
				.control-sidebar {
					padding-top: 0px;
					width:100%;
					height:100%;
					position: absolute;
					top:0;bottom:0;left:0;right:0;
				}
				.ProjectManagement .form-project {
				    position: absolute;
				    width: 94%;
				    height: 90%;
				}
				.ProjectManagement .ui-datepicker td span, .ProjectManagement .ui-datepicker td a {
				    display: block;
				    padding: .2em;
				    text-align: right;
				    text-decoration: none;
				}
				.ProjectManagement .ui-datepicker-multi .ui-datepicker-group {
				    float: left;
				}
				.ProjectManagement .ui-datepicker-multi .ui-datepicker-group table {
				    width: 95%;margin: 0 auto .4em;
				}
				.ProjectManagement .ui-datepicker table {
				    width: 100%;font-size: .9em;border-collapse: collapse;margin: 0 0 .4em;
				}
				.ProjectManagement .ui-datepicker .ui-datepicker-header {
				    position: relative;padding: .2em 0;
				}
				.ProjectManagement .ui-datepicker.ui-datepicker-multi.ui-datepicker-multi-2 .ui-datepicker-group.ui-datepicker-group-first {
				    padding-right: 5%;clear: both;
				}
				.ProjectManagement .ui-datepicker .ui-datepicker-prev, .ProjectManagement .ui-datepicker .ui-datepicker-next {
				    position: absolute;top: 2px;width: 1.8em;height: 1.8em;
				}
				.ProjectManagement .ui-datepicker .ui-datepicker-title {
				    margin: 0 2.3em;line-height: 1.8em;text-align: center;
				}
				.ProjectManagement .ui-state-disabled {
				    cursor: default!important;pointer-events: none;
				}
				.ProjectManagement .ui-state-disabled {
				    cursor: default!important;pointer-events: none;color: #909090;
				}
				.ProjectManagement .ui-state-default{
					// background-color:#fff;
				}
				.ProjectManagement .ui-datepicker td span,.ProjectManagement .ui-datepicker td a {
				    display: block;padding: .2em;text-align: right;text-decoration: none;
				}
				.ProjectManagement #summaryForm {
					overflow-y: auto;overflow-x: hidden;
				}
				#createDummyContract, #selectProjectStatus {
					display: none;
				}
				</style>
		      <div class="">
		            <div class="mhn-ui-app-title-head animated bounceInRight" style="text-align:center;">'
		                .((self::$permission_role['project_create_able'])?'
		                <span class=" pull-left" style="">
		                  <a class="icon icon-compose pull-left" id="btnCreateProject" href="javascript:void(0);"></a>
		                </span>'
		                :'<span class="pull-left" style="">
		                  <a class="icon icon-compose pull-left" style="visibility:hidden;" href="javascript:void(0);"></a>
		                </span>').
		                '<span class="hide pull-left" style="">
		                  <a class="icon icon-left-nav pull-left" id="btnBackToProjectList" href="javascript:void(0);"></a>
		                </span><span class="pull-right">
		                  '.(($email=="autsakorn.t@firstlogic.co.th" || $email=="sarinaphat.t@firstlogic.co.th")?'
		                  <a class="icon icon-more-vertical pull-right" id="ProjectSettingFilter" href="javascript:void(0);"></a>
		                  ':'').'
		                  <a class="icon icon-more-vertical pull-right hide" id="ProjectEdition" href="javascript:void(0);"></a>
		                </span>
		                <span class="mhn-ui-page-title">Project Management</span>
		            </div>
		            <div class="mhn-ui-row mhn-ui-apps ">

		              <div class="content menu-view-bottom animated bounceInRight"><ul class="table-view">
		              		<li class="table-view-divider cell-block"><input type="search" value="'.$search.'" placeholder="Search" id="searchProject" style="background-color:#f1f1f1;"></li>
		              		<li class="table-view-divider cell-block"><div style="text-align:center;"><p>'.count(self::$data).' Rows.</p></div></li>
		              ';
		            foreach (self::$data as $key => $value) {
		                $html .= '<li class="table-view-cell">
		                	<a class="navigate-right select-project" onclick="selectProject(this);" href="javascript:void(0);" data-project-sid="'.$value['sid'].'" data-contract="'.$value['contract'].'" >
		                		<div>'.$value['contract'].'<p>'.$value['end_user'].'</p></div>
		                		<div class="info-left"><p><i class="fa fa-clock-o"></i> Man Hours <span class="pull-right">'.$value['allManHours'].' Hr.</span></p></div>
                        		<div class="info-left"><p><i class="fa fa-clock-o"></i> Used <span class="pull-right">'.$value['use_hours'].'.'.$value['use_minutes_mod'].' Hr.</span></p></div>
                        		<div class="info-left"><p><i class="fa fa-calendar"></i> Added <span class="pull-right">'.$value['create_datetime_df'].'</span></p></div>
		                	</a></li>';
		            }
		            $html .= '</ul></div>
		            </div>
		      </div>';
		$html .= self::controlSidebar();
		$html .= MoreSystemView::bottomBar();

		$projectManagementView = new ProjectManagementView();
		$projectManagementView->setPermissionRole($permission_role);
		$projectManagementView->setCanAssignTo($canAssignTo);
		$projectManagementView->setWindowHeight($windowHeight+100);
		if($permission_role['project_create_able']){
			$html .= $projectManagementView->createProjectView();
		}
		$html .= '
		<script src="http://vspace.in.th/pages/lib/email-report-overview/js/index.js?v=5"></script>
		<script>
		var numberOpenSetting = 0;
		var confirmCreateProject = new Object();
		confirmCreateProject.type = "Implement";
		$("#btnCreateProject").click(function(){
			$(".ProjectManagement").find(".control-sidebar").attr("class","control-sidebar control-sidebar-dark open animated bounceInRight");
		    $(".ProjectManagement .form-project").attr("class", "form-project hide");
		    $(".ProjectManagement #selectProjectStatus").removeClass("hide");
		    $(".ProjectManagement #selectProjectStatus").show();
		    $(".ProjectManagement #selectProjectStatus").attr("class", "form-project animated bounceInRight");
		});
		$(".ProjectManagement #closeCrateProject").click(function(){
			$(".ProjectManagement .control-sidebar").attr("class","control-sidebar animated bounceOutRight");
		});
		$(".ProjectManagement .optionalList").click(function(){
			confirmCreateProject.type = $(this).attr("data-type");
        	$(".ProjectManagement #selectProjectType").addClass("animated bounceOutLeft");
        	console.log(confirmCreateProject);
        	formSelectStatusProject();
			//contractForm();
		});
		var formSelectStatusProject = function() {
	        $(".control-sidebar").addClass("control-sidebar-open");
	        $(".form-project").attr("class", "form-project hide");
	        $("#selectProjectStatus").removeClass("hide");
	        $("#selectProjectStatus").show();
	        $("#selectProjectStatus").attr("class", "form-project animated bounceInRight");
	    }
	    $(".ProjectManagement .optionalListProjectStatus").click(function(){
	    	project_status = $(this).data("type");
	        confirmCreateProject.project_status = project_status;
	        if(project_status=="Post Project" || project_status=="Post Sale"){
	            $("#selectProjectStatus").attr("class", "form-project animated bounceOutLeft");
	            contractForm();
	        }else{
	            $(".form-project").attr("class", "form-project hide");
	            $("#createDummyContract").removeClass("hide");
	            $("#createDummyContract").show();
	            $("#createDummyContract").attr("class", "form-project animated bounceInRight");
	        }
	    });

		var contractForm = function() {
        	$(".ProjectManagement #contractForm").removeClass("hide");
        	$(".ProjectManagement #contractForm").show();
        	$(".ProjectManagement #contractForm").attr("class", "form-project animated bounceInRight");
    	}
    	var templateListContract = Handlebars.compile("<ul class=\'control-sidebar-menu animated bounceInRight\'>\{\{#each this\}\}<li><a class=\'optionalContract\' end_user_name=\'\{\{ENDUSER_NAME\}\}\' end_user_address=\'\{\{ENDUSER_ADDRESS\}\}\' project_name=\'\{\{PROJECT_NAME\}\}\' contract_no=\'\{\{CONTRACT_NO\}\}\' href=\'javascript:void(0);\'><div class=\'menu-info\' style=\'margin-left:0px;\'><h4 class=\'control-sidebar-subheading\'>\{\{\{CONTRACT_NO\}\}\}</h4>\{\{#if PROJECT_NAME\}\}<p>Project Name <span class=\'pull-right\'>\{\{\{PROJECT_NAME\}\}\}</span></p><p>End User Name <span class=\'pull-right\'>\{\{\{ENDUSER_NAME\}\}\}</span></p><p>End User Address <span class=\'pull-right\'>\{\{\{ENDUSER_ADDRESS\}\}\}</span></p>\{\{/if\}\}</div></a></li>\{\{\/each\}\}</ul>");
    	
    	$(".ProjectManagement #contract_no").change(function(){
    		var contract_no_input = $(this).val();
    		
	        var load = [{
	            CONTRACT_NO: "Find the contract..."
	        }];
	        console.log(load);
	        $(".ProjectManagement #listContract").html(templateListContract(load));
	        findTheContract(contract_no_input).done(function(res){
	        	if(res.data.length>0){
	        		$("#listContract").html(templateListContract(res.data));
	        		$(".ProjectManagement .optionalContract").click(function(){
						var contract_info = new Object();
					    contract_info.project_name = $(this).attr("project_name");
					    contract_info.end_user_name = $(this).attr("end_user_name");
					    contract_info.end_user_address = $(this).attr("end_user_address");
					    confirmCreateProject.contract_no = $(this).attr("contract_no");
					    contract_info.description = "";
					    confirmCreateProject.subject = $(this).attr("project_name");
					    confirmCreateProject.contract_info = contract_info;
					    console.log(confirmCreateProject);
					    $("#contractForm").attr("class", "form-project animated bounceOutLeft");
					    ownerForm();
					});
	        	}
	        	else
	        		$("#listContract").html("<div style=\"margin-top:10px; text-align:center;\">Not Found Data</div>");
	        });
    	});
    	var findTheContract = function(contract_no_input) {
	        return $.ajax({
	            type: "POST",
	            url: END_POINT_2 + "v1/contract/find_contract_info",
	            data: {
	                email: localStorage.getItem("case_email"),
	                token: localStorage.getItem("case_token"),
	                contract: contract_no_input
	            }
	        });
	    }
    	
		$(".ProjectManagement").on("click","#backToProjectType",function() {
        	$(".ProjectManagement #contractForm").addClass("animated bounceOutRight");
        	$(".ProjectManagement #selectProjectType").attr("class", "form-project animated bounceInLeft");
    	});
    	$(".ProjectManagement").on("click","#backToProjectStatus",function() {
        	$(".ProjectManagement #contractForm").addClass("animated bounceOutRight");
        	$(".ProjectManagement #selectProjectStatus").attr("class", "form-project animated bounceInLeft");
    	});
	    ownerForm = function() {
	        
	    	$("#ownerForm").attr("class", "form-project animated bounceInRight");
	    }
	    $(".ProjectManagement #backToContractForm").click(function() {
        	$("#contractForm").attr("class", "form-project animated bounceInLeft");
        	$("#ownerForm").attr("class", "form-project animated bounceOutRight");
    	});
	    $(".ProjectManagement").on("click","#listStaff>ul>li.optionalStaff>a",function(){
	     	$(this).parent().append(\'<div class="div-manday row"><div class="col-xs-6"><span class="pull-right">Man Days</span></div><div class="col-xs-5"><input type="number" min="0" placeholder="Man Days" class="man_day form-control input-sm" /></div></div>\');
        	$(this).parent().appendTo($("#selectedStaff>ul"));
        	countStaffSelected();
        	countStaffList();
	    });
	    $(".ProjectManagement").on("click","#selectedStaff>ul>li.optionalStaff>a",function(){
	    	$(this).parent().find(\'.div-manday\').remove();
	    	$(this).parent().appendTo($("#listStaff>ul"));
	    	countStaffSelected();
        	countStaffList();
	    });
	    var countStaffSelected = function() {
	        var numberOfStaff = $("#selectedStaff>ul li").length;
	        $("#selectedStaff .numberOfStaff").html("" + numberOfStaff + " Row(s).");
	    }
	    var countStaffList = function() {
	        var numberOfStaff = $("#listStaff>ul li").length;
	        $("#listStaff .numberOfStaff").html("" + numberOfStaff + " Row(s).");
	    }
	    $(".ProjectManagement").on("click","#backToOwnerForm",function(){
	    	$("#ownerForm").attr("class", "form-project animated bounceInLeft");
        	$("#periodForm").attr("class", "form-project animated bounceOutRight");
	    });
	    $(".ProjectManagement").on("click","#nextToPeriodForm",function() {
	    	var checkIn, checkOut, numberOfMonths = [12, 1],
            $calendar = $("#calendar").datepicker({
	            	numberOfMonths: numberOfMonths,
	                prevText: "",
	                nextText: "",
	                beforeShowDay: function(date) {
	                date = moment(date);
	                var now = moment().subtract(5, "day"),
	                show = date.isAfter(now),
	                css = "";
	                if (checkIn && checkOut && date.isSameOrAfter(checkIn) && date.isSameOrBefore(checkOut)) {
	                        css = "ui-datepicker-reserved";
	                    	if (date.isSame(checkIn)) css += " ui-datepicker-checkin";
	                    	if (date.isSame(checkOut)) css += " ui-datepicker-checkout";
	                }
	                    // console.log(show);
	                	return [show, css];
	                },
	                onSelect: function(value) {
	                var date = moment($calendar.datepicker("getDate"));
	                if (checkIn && !checkOut && date.isSameOrAfter(checkIn)) checkOut = date;
	                else {
	                    checkIn = date;
	                	checkOut = null;
	                }
	                $("#check-in-date").text(checkIn ? checkIn.format("YYYY-MM-DD") : "Choose a date");
	            	$("#check-out-date").text(checkOut ? checkOut.format("YYYY-MM-DD") : "Choose a date");
	            },
	            onChangeMonthYear: function() {
	            	$calendar.addClass("fade-in");
	        	}
	        }).on("animationend webkitAnimationEnd", function() {
	        	$calendar.removeClass("fade-in");
	        });

	        $("#ownerForm").attr(\'class\', \'form-project animated bounceOutLeft\');
	        $("#periodForm").attr(\'class\', \'form-project animated bounceInRight\');
	        var numberSelectedStaff = $("#selectedStaff").find(\'li\').length;
	        var staff = new Array();
	        var staffHtml = new Array();
	        for (var i = 0; i < numberSelectedStaff; i++) {
	            staff.push({
	                email: $("#selectedStaff li:eq(" + i + ")").attr(\'data-type\').trim(),
	                man_day: $("#selectedStaff li:eq(" + i + ") input.man_day").val(),
	            });
	            staffHtml.push($("#selectedStaff li:eq(" + i + ")").html().trim());
	            if ((i + 1) == numberSelectedStaff) {
	                confirmCreateProject.engineer = staff;
	                confirmCreateProject.staffHtml = staffHtml;
	                console.log(confirmCreateProject);
	            }
	        }
	    });
        $(".ProjectManagement").on("click","#nextToSummaryForm",function() {
	        confirmCreateProject.start = $("#check-in-date", this.$el).text();
        	confirmCreateProject.end = $("#check-out-date", this.$el).text();
        	$("#periodForm", this.$el).attr("class", "form-project animated bounceOutLeft");
        	$("#summaryForm", this.$el).attr("class", "form-project animated bounceInRight");
        	confirmCreateProject.man_day = $("#manday").val();
        	confirmCreateProject.man_hour = $("#manhours").val();
        	console.log(confirmCreateProject);
        	$("#summaryForm .summaryForm").html("<div>Phase: <span class=\"pull-right\">" + confirmCreateProject.project_status + "</span></div>");
        	if(confirmCreateProject.contract_no){
		        $("#summaryForm .summaryForm").append("<div>Contract: <span class=\"pull-right\">" + confirmCreateProject.contract_no + "</span></div>");
		    }
	        $("#summaryForm .summaryForm").append("<div>Project Name: <span class=\"pull-right\">" + confirmCreateProject.contract_info.project_name + "</span></div>");
			$("#summaryForm .summaryForm").append("<div>End User: <span class=\"pull-right\">" + confirmCreateProject.contract_info.end_user_name + "</span></div>");
			if(confirmCreateProject.contract_info.end_user_address){
		        $("#summaryForm .summaryForm").append("<div>End User Address: <span class=\"pull-right\">" + confirmCreateProject.contract_info.end_user_address + "</span></div>");
		    }
	        var $staff = $("<ul class=\"control-sidebar-menu\">");
	        $.each(confirmCreateProject.staffHtml, function(k, v) {
	            $staff.append("<li>" + v + "</li></div>");
	            $staff.find("li:last").find("a>div").append("<p>Man Days <span class=\"pull-right\">" + confirmCreateProject.engineer[k]["man_day"] + "</span></p>");
	        });
	        $staff.find(".div-manday").remove();
	        $("#summaryForm .summaryForm").append($staff);
	        $("#summaryForm .summaryForm").append("<div>Period: <span class=\"pull-right\">" + confirmCreateProject.start + " To " + confirmCreateProject.end + "</span></div>");
	    });
	    $(".ProjectManagement").on("click","#backToPeriodForm", function() {
        	$("#summaryForm", this.$el).attr("class", "form-project animated bounceOutRight");
        	$("#periodForm", this.$el).attr("class", "form-project animated bounceInLeft");
    	});
    	$(".ProjectManagement").on("click","#CreateProjectClick",function(){
    		CreateProjectToServer(confirmCreateProject).done(function(res){
    			rd = Math.floor((Math.random() * 1000) + 1);
				window.location = "#ProjectManagement/right/"+rd;
    		});
    	});
    	var CreateProjectToServer = function(data) {
	        console.log(confirmCreateProject);
	        return $.ajax({
	            type: "POST",
	            url: END_POINT_2 + "v1/project/CreateProject1",
	            data: {
	                data: data,
	                email: localStorage.getItem("case_email"),
	                token: localStorage.getItem("case_token"),
	            }
	        });
	    }
	    $(".ProjectManagement").on("click","#btnCreateDummyContract", function(){
	    	var contract_info = new Object();
	        var _this = $(this).closest("#createDummyContract");
	        contract_info.project_name = $(_this).find("#create_dummy_contract_project_name").val();
	        contract_info.end_user_name = $(_this).find("#create_dummy_contract_customer_company").val();
	        contract_info.end_user_address = "";
	        contract_info.description = "";
	        confirmCreateProject.contract_no = "";
	        confirmCreateProject.subject = contract_info.project_name;
	        confirmCreateProject.contract_info = contract_info;
	        $("#createDummyContract").attr("class", "form-project animated bounceOutLeft");
	        ownerForm();
	    });

		function dismiss(){
			$(".control-sidebar-setting").removeClass("open");
		}
		function settingOpen(){
			numberOpenSetting+=Math.floor((Math.random() * 1000) + 1);
			$(".control-sidebar-setting").attr("class","control-sidebar-setting open animated bounceIn");
		}
		$("#ProjectSettingFilter").on({
			"click": settingOpen,
		});
		$(".project-order-by-cell").click(function(){
			$(this).prependTo($(this).parent());
		});
		$("#closeSettingProject").click(function(){
			$(".control-sidebar-setting").attr("class","control-sidebar-setting open animated bounceOut");
			setTimeout(function() {
				$(".control-sidebar-setting").attr("class","control-sidebar-setting animated bounceOut");
				window.location = "#ProjectManagement/right/"+numberOpenSetting;
			}, 500);
		});

		function selectProject(e){ 
			var project_sid = $(e).attr("data-project-sid"), contrct_no = $(e).attr("data-contract");
			if(project_sid && contrct_no){
				$(".mhn-ui-apps").find(">.content.menu-view-bottom:eq(0)").attr("class","content menu-view-bottom animated bounceOutLeft");
				$(".mhn-ui-page-title").attr("class","mhn-ui-page-title animated bounceOutLeft");
				$("#ProjectSettingFilter").addClass("hide");

				projectDetailApp = new ProjectDetailApp();
				projectDetailApp.loadTemplateProjectDetail(project_sid, contrct_no, "#projectinfo").done(function(res){
					$(".container-user-administration-page").remove();

					$("#btnCreateProject").parent().addClass("hide");
					$("#btnBackToProjectList").parent().removeClass("hide");
					$("#btnBackToProjectList").parent().show();

					console.log(res.project_detail.project_detail);
					$(".mhn-ui-page-title").html(res.project_detail.project_detail.name);
					$(".mhn-ui-page-title").attr("class","mhn-ui-page-title animated bounceInRight");

					$(".mhn-ui-apps").append(res.template);

					$(".mhn-ui-apps").find(".container-user-administration-page").attr("class","container-user-administration-page menu-view-bottom animated bounceInRight");
					
					$("#btnBackToProjectList").click(function(){
						$(".container-user-administration-page").attr("class","container-user-administration-page menu-view-bottom animated bounceOutRight");
						$(".mhn-ui-apps").find(".content:eq(0)").attr("class","content menu-view-bottom animated bounceInLeft");
						$(".mhn-ui-page-title").attr("class","mhn-ui-page-title animated bounceInLeft");
						
						$("#btnCreateProject").parent().removeClass("hide");
						$("#btnCreateProject").parent().show();
						$("#btnBackToProjectList").parent().addClass("hide");
						$("#btnBackToProjectList").parent().show();

						$(".mhn-ui-page-title").html("Project Management");
						$("#ProjectSettingFilter").removeClass("hide");
						$("#ProjectSettingFilter").show();
					});
				});
			}
		 }
		 var ProjectDetailApp = function(){
		 	this.loadTemplateProjectDetail = function(project_sid, contract, tabName) {
		        return $.ajax({
		            type: "POST",
		            url: END_POINT_2 + "v1/moresystem/templateProjectDetail",
		            data: {
		                email: localStorage.getItem("case_email"),
		                token: localStorage.getItem("case_token"),
		                project_sid: project_sid,
		                contract: contract,
		                tabName: tabName
		            },
		        });
		    }
		 }
		 </script>';
		return $html;		  
	}
}