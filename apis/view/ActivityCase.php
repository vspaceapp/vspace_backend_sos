<?php
Class ActivityCase {

	private $email;

	function __construct($email){
		$this->email = $email;
	}
	public function htmlActivityCase($sla,$expect_pending_finish, $worklog, $ticket_sid, $service_report){
		$html = "<div class='card'>";
		$html .= "<ul class='table-view'>";
		$html .= "<li class='table-view-divider cell-block'><div>SLA Indicators</div></li>";

		if(count($sla)>0){
			foreach ($sla as $key => $value) {
				$html .= "<li class='table-view-cell cell-block'><div><p>".$value['name']."<span class='pull-right'><span style='font-style: italic;font-size: 12px;'>".$value['due_datetime']."</span><br/>Status: ".$value['status']."</span></p></div></li>";			
			}
		}else{
			$html .= "<li class='table-view-cell cell-block' style='text-align:center;'><div><p>SLA Not Measured</p></div></li>";			
		}
		$html .= "</ul></div>";
		
		if($expect_pending_finish!=""){
			$html .= "<div class='card'><ul class='table-view'>";
			$html .= '<li class="table-view-cell cell-block">
                        <div class="pull-left"><p>Expect Pending Finish:</p></div>
                        <div class="pull-right"><p>
                            <span style="font-style: italic;font-size: 12px;">'.$expect_pending_finish.'</span></p>
                        </div>
                    </li>';
			$html .= "</ul>";
			$html .= "</div>";
		}

		if($this->email=="autsakorn.t@firstlogic.co.th" || 1){
			$html .= '<div class="card">
                      <ul class="table-view">                      	
                          <li class="table-view-divider">Change Status</li>
                          <li class="table-view-cell"><a class="navigate-right" href="#sla/'.$ticket_sid.'/2/Response/'.rand(0,10000).'"><p>Response</p></a></li>';
        }else{
        	$html .= '<div class="card">
                      <ul class="table-view">                      	
                          <li class="table-view-divider">Change Status</li>
                          <li class="table-view-cell"><a class="navigate-right" href="#sla/'.$ticket_sid.'/2/Response"><p>Response</p></a></li>';
        }
		if(count($sla)>0){
			// CASE THIS IS FROM REMEDY
			     if($this->email=="autsakorn.t@firstlogic.co.th" || 1){
				        $html .= '<li class="table-view-cell">
                            <a class="navigate-right" href="#sla/'.$ticket_sid.'/7/Onsite/'.rand(0,10000).'"><p>Onsite</p></a>
                      </li>
                      <li class="table-view-cell">
                           <a class="navigate-right" href="#sla/'.$ticket_sid.'/11/NoOnsite/'.rand(0,10000).'"><p>No Onsite</p></a>
                      </li>
                      <li class="table-view-cell">
                          <a class="navigate-right" href="#sla/'.$ticket_sid.'/3/Workaround/'.rand(0,10000).'"><p>Work Around</p></a>
                      </li>
                      <li class="table-view-cell">
                          <a class="navigate-right" href="#sla/'.$ticket_sid.'/10/No_Workaround/'.rand(0,10000).'"><p>No Work Around</p></a>  
                      </li>';
            }else{
            	   $html .= '<li class="table-view-cell">
                            <a class="navigate-right" href="#sla/'.$ticket_sid.'/7/Onsite"><p>Onsite</p></a>
                      </li>
                      <li class="table-view-cell">
                           <a class="navigate-right" href="#sla/'.$ticket_sid.'/11/NoOnsite"><p>No Onsite</p></a>
                      </li>
                      <li class="table-view-cell">
                          <a class="navigate-right" href="#sla/'.$ticket_sid.'/3/Workaround"><p>Work Around</p></a>
                      </li>
                      <li class="table-view-cell">
                          <a class="navigate-right" href="#sla/'.$ticket_sid.'/10/No_Workaround"><p>No Work Around</p></a>  
                      </li>
                      <li class="table-view-cell">
                          <a class="navigate-right" href="#pending/'.$ticket_sid.'/4/Pending"><p>Pending</p></a>          
                      </li>';
            }
		}
		$html .= '<li class="table-view-cell">
                    <a class="navigate-right" href="#sla/'.$ticket_sid.'/4/Pending/'.rand(0,10000).'"><p>Pending</p></a>          
              </li>';
		// INTERNAL CASE
		$numberCreatedServiceReport = count($service_report);
		$signatedService = 0;
		foreach ($service_report as $key => $value) {
			if($value['customer_signated']){
				$signatedService++;
			}
		}
		if($numberCreatedServiceReport>$signatedService){
			$html .= '<li class="table-view-cell cell-block" style="text-align:center;"><div><p>This case can not Resolve, because Service Report Uncompleted</p></div></li>';
		}else if($numberCreatedServiceReport==$signatedService){
			// if(count($sql)>0){
				if($this->email=="autsakorn.t@firstlogic.co.th" || 1){
					$html .= '<li class="table-view-cell">
                            <a class="navigate-right" href="#sla/'.$ticket_sid.'/5/Resolve/'.rand(0,10000).'"><p>Resolve</p></a>
                          </li>';
                }else{
                	$html .= '<li class="table-view-cell">
                            <a class="navigate-right" href="#sla/'.$ticket_sid.'/5/Resolve"><p>Resolve</p></a>
                          </li>';
                }
			// }
		}
		
		$html .= '</ul></div>';

		$html .= "<div class='card'><ul class='table-view'>";
		$html .= "<li class='table-view-divider cell-block'><div>Worklog</div></li>";
		$html .= '<li class="table-view-cell">
                    <a class="navigate-right" href="#sla/'.$ticket_sid.'/8/Worklog/'.rand(0,10000).'"><p><span style="font-size: 1.3em;" class="icon icon-compose"></span> Worklog</p></a></li>';
		if(count($worklog)>0){
			foreach ($worklog as $key => $value) {
				$html .= '<li class="table-view-cell cell-block">
                              <img class="media-object pull-left" src="'.$value['create_by_pic'].'">
                              <div class="media-body">
                                <div><p>'.$value['worklog'].'</p></div>
                                <div><p><small>Created <span class="">'.$value['create_by_thainame'].'</span></small></p></div>
                                <!-- <div><p><small>&nbsp;<span class="pull-right">'.$value['create_by'].'</span></small></p></div> -->
                                <div><p><small><span class="">'.$value['create_datetime'].'</span></small></p></div>
                              </div>
                            </li>';		
			}
		}else{
			$html .= "<li class='table-view-cell cell-block' style='text-align:center;'><div><p>-</p></div></li>";			
		}
		$html .= "</ul>";
		$html .= "</div>";
		return $html;
	}

	public function htmlServiceReport($ticket_sid, $service_report, $email){
		$html = '';
		if(count($service_report)>0){
			$html .= '<div class="card"><ul class="table-view"><li class="table-view-divider"><div>Service Report</div></li>';
            foreach ($service_report as $key => $value) {
            	$html .= '<li class="table-view-cell media"><a class="navigate-right" href="#service_report/'.$ticket_sid.'/'.$value['sid'].'/right/'.rand(0,100000).'">
                                <img class="media-object pull-left" src="../img/icon/'.$value['service_type'].'.png" />
                                <div class="media-body">
                                	<p>'.$value['no_task'].'</p>
                                    <p>Subject <span class="pull-right">'.$value['subject_service_report'].'</span></p>
                                    <p>Appointment <span class="pull-right">'.$value['appointment'].'</span></p>
                                    <p></p>
                                    <div class="row">
                                       	<div class="col-xs-2"><p>Engineer</p></div>
                                       	<div class="col-xs-10">
                                        <div class="pull-right"><p>'.$value['engineer'].'</p></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       	<div class="col-xs-2"><p>Status</p></div>
                                       	<div class="col-xs-10">
                                        <div class="pull-right"><p>'.$this->statusSrName($value['last_status']).'</p></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       	<div class="col-xs-2"><p>PDF</p></div>
                                       	<div class="col-xs-10">
                                        <div class="pull-right"><p>'.(($value['pdf_report']!="-")?"Signed":"Unsigned").'</p></div>
                                        </div>
                                    </div>
                                    <p></p>
                                </div>
                        </a></li>';
            }                                      
            $html .= '</ul></div>';
		}

		return $html;
	}

	private function statusSrName($lastStatus){
		if($lastStatus==0){
			return "New";
		}else if($lastStatus==100){
			return "Accepted";
		}else if($lastStatus==200){
			return "On the way";
		}else if($lastStatus==300){
			return "Working";
		}else if($lastStatus==400){
			return "Closed";
		}else if($lastStatus==500){
			return "Send Customer signatured";
		}else if($lastStatus==600){
			return "Completed";
		}
	}
}