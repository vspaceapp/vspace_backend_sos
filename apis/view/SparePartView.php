<?php class SparePartView{
    private $email;
    private $data;
	public function __construct($email=""){
        $this->email = $email;
    }

    public function templateSpareTask($data, $spare_sid, $ticket_sid){
    	$html = '<div class="content menu-view-bottom" style="width:100%;">';
    	$html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a id="backToSparePart" class="icon icon-left-nav pull-left" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Oracle Task</span>
	    </div>';
        $html .= '<ul class="table-view"><li class="table-view-divider"><div>Oracle Task<a style="font-size: 1.5em;" class="pull-right" href="javascript:void(0);" spare-sid="'.$spare_sid.'" id="CreateOracleTask"><i class="fa fa-fw fa-edit"></i></a></div></li>';
        if(count($data)>0){
          foreach ($data as $key => $value) {
            $html .= '<li class="table-view-cell">
                          <a href="javascript:void(0);" class="navigate-right view-task" data-spare-sid="'.$value['ticket_spare_sid'].'" data-spare-task-sid="'.$value['sid'].'">
                            <div>
                            <p>Task <span class="pull-right">'.$value['oracle_task'].'</span></p>
                            </div>
                          </a>
                      </li>';
          }
        }else{
            $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>No Task</p></div></li>';
        }
        $html .= '</ul>';
        $html .= "</div>";
        return $html;
    }

    public function templateSendMailReturnPart($email, $data_oracle_sr, $data_oracle_task, $data_oracle_part, $contact){
    	$html = '<div class="content menu-view-bottom" style="width:100%;">';
    	$html .= '<div class="mhn-ui-app-title-head" style="text-align:center;border-bottom:1px solid #f1f1f1;">
	              	<a id="backToSparePart" class="icon icon-left-nav pull-left" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Send Mail To DHL</span>
	    </div>';
	    $date_return = "";
	    if(isset($data_oracle_part[0]['return_spare_part_date'])){
	    	$date_return = $data_oracle_part[0]['return_spare_part_date'];
	    }
	    $caseID = $data_oracle_sr['oracle_sr'];
        $taskID = $data_oracle_task['oracle_task'];

    	$subject = 'PARTS RETURN NEEDED on '.$date_return.' / (Thailand) CASEID/TASKID = "'.$caseID.'"/"'.$taskID.'" ';
        $firstline = '';
        $caseID = $data_oracle_sr['oracle_sr'];
        $taskID = $data_oracle_task['oracle_task'];
        $threeA = 'threeA is Checked Default';
        $threeB = '';
        $txtThreeB = '';

        $contactPerson = '<input type="text" id="contactPerson" value="'.$contact['engname'].'" />';
        $phone = '<input type="text" id="phone" value="'.$contact['mobile'].'" /> ';
        $collectionAddress = '<textarea style="width:100%;" id="address">202, 10th Fl. CDG House Nanglinchi Rd.Chongnonsee, Yannawa, Bangkok, Thailand 10120</textarea>';
        
        foreach ($data_oracle_part as $key => $value) {
        	
        }
        $status = 'Good';
        // $partNumber = array(array('pn' => '12345678', 'status' => $status));       
        $partNumber = $data_oracle_part;
        $quantity = 1;
        
        $spacial = "Please call to contact person before onsite to pick up part.";
        $message = '<span style="font-weight: bolder;"><div style="text-decoration: underline;">FSC to arrange DHL collection</div>';
        $message .= '<div><span style="color:#3333FF">(1) Case ID</span> = <span style="color:red">' . $caseID . '</span></div>';
        $message .= '<div><span style="color:#3333FF">(2) Task ID</span> = <span style="color:red">' . $taskID . '</span></div>';
        $message .= '<div><span style="color:#3333FF">(3) Requested Part Collection date & time (local country time) (please put X accordingly)</span></div>';
        $message .= '<div style="cursor:pointer;" class="threeOption">&nbsp;&nbsp;&nbsp;&nbsp;(a) [ <span  class="threeOptionShow">/</span> ] NEXT CALENDAR DAY between 0900hrs - 1700hrs</div>';
        $message .= '<div style="cursor:pointer;" class="threeOption">&nbsp;&nbsp;&nbsp;&nbsp;(b) [ <span  class="threeOptionShow">&nbsp;&nbsp;</span> ] Specific date & time <br/>&nbsp;&nbsp;&nbsp;&nbsp;[Example = 04-JAN-2017 (15:00hrs)] <br/>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:red"><input type="text" id="txtThreeB" /></span></div>';
        $message .= '<div><span style="color:#3333FF">(4) Site Contact Person</span>        = <span style="color:red">' . $contactPerson . '.</span></div>';
        $message .= '<div><span style="color:#3333FF">(5) Site Contact Number(s)</span> = <span style="color:red">' . $phone . '</span></div>';
        $message .= '<div><span style="color:#3333FF">(6) Collection Address</span>          = <br/><span style="color:red">' . $collectionAddress . '</span></div>';
        $message .= '<div><span style="color:#3333FF">(7) Parts Information</span> (Note = please use separate line for each part number)</div>';
        $numberPart = 0;
        foreach ($partNumber as $key => $value) {
        	if($value['spare_part_return_type']!=""){
	            if ($numberPart == 0) {
	                $order = "(a)";
	            } else if ($numberPart == 1) {
	                $order = "(b)";
	            } else if ($numberPart == 2) {
	                $order = "(c)";
	            } else if ($numberPart == 3) {
	                $order = "(d)";
	            } else if ($numberPart == 4) {
	                $order = "(e)";
	            } else if ($numberPart == 5) {
	                $order = "(f)";
	            } else if ($numberPart == 6) {
	                $order = "(g)";
	            } else if ($numberPart == 7) {
	                $order = "(h)";
	            } else if ($numberPart == 8) {
	                $order = "(i)";
	            } else if ($numberPart == 9) {
	                $order = "(j)";
	            }
	            $numberPart++;
	            $message .= '<div class="part_number" part_sid="'.$value['sid'].'">&nbsp;&nbsp;&nbsp;&nbsp;' . $order . ' Part Number = <span style="color:red;">' . $value['pn'] . '</span>, Quantity = <span style="color:red;"> 1 </span>, Part Status =  <span style="color:red;"> '.$value['spare_part_return_type'].' </span></div>';
	        }
        }
        $message .= '<div><span style="color:#3333FF">(8) Any Special Instructions</span> = <span style="color:red">'.$spacial.'</span></div></span>';

        if(count($data_oracle_part)>0){
	        $html .= '<div style="font-weight: bolder;" class="email-subject">'.$subject."</div><br/>";
	        $html .= '<div class="email-message">'.$message.'</div>';
	        $html .= '<div class=""><hr/>Add CC To: <input type="text" id="cc_to" class="form-control" /></div>';
     	   	$html .= '<div style="margin-top:10px;"><button class="btn btn-block btn-primary" id="btnSendEmail">Send Email</button></div>';

     	   	$html .= '<div class="alert alert-info" style="margin-top:10px;">When click button \'Send Email\' system will send Email to th.spl@dhl.com,th.Orale@dhl.com and CC to sirirat.c@firstlogic.co.th</div>';
    	}else{
    		$html .= '<div>Please update Return Type and Return spare part date before send mail to DHL</div>';
    	}
    	$html .= '</div><br/><br/>';
    	return $html;
    }

    public function templateSparePart($data, $spare_sid, $ticket_sid, $spare_task_sid){
    	$html = '<div class="content menu-view-bottom" style="width:100%;">';
    	$html .= '<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a id="backToSparePart" class="icon icon-left-nav pull-left" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Part Information</span>
	    </div>';
        $html .= '<ul class="table-view"><li class="table-view-divider"><div>Part 
        <a style="font-size: 1.5em;" class="pull-right" href="javascript:void(0);" spare_task_sid="'.$spare_task_sid.'" spare-sid="'.$spare_sid.'" id="CreateOraclePart"><i class="fa fa-fw fa-edit"></i></a> 
        <a spare_task_sid="'.$spare_task_sid.'" spare-sid="'.$spare_sid.'" id="SendMailPart" style="font-size: 1.5em; margin-top:-2px;" class="pull-right" href="javascript:void(0);"><i class="fa fa-fw fa-envelope-o"></i></a> </div></li>';
        if(count($data)>0){
          foreach ($data as $key => $value) {

          	$return_type['option'] = array('','Part return','Good return','Logistics DOA','MFG/New install DOA','CPAS Request Label','FCO Return (Field Change Order)','Return for Repair ESD static sensitive device');

          	$spare_part_return_type = "";
          	$spare_part_return_type .= '<select data-part-sid="'.$value['sid'].'" class="spare_part_return_type">';
          	foreach ($return_type['option'] as $k => $v) {
          		$spare_part_return_type .= '<option value="'.$v.'" '.(($v==$value['spare_part_return_type'])?"selected":"").'>'.$v.'</option>';
          	}
          	$spare_part_return_type .= "</select>";
            $html .= '<li class="table-view-cell cell-block">
                          <!--<a href="javascript:void(0);" class="navigate-right view-task" data-spare-sid="'.$spare_sid.'" data-spare-task-sid="'.$value['ticket_spare_task_sid'].'">-->
                            <div>
                            <p>Part Number <span class="pull-right">'.$value['pn'].'</span></p>
                            <p>Part description <span class="pull-right">'.$value['part_description'].'</span></p>
                            <p>Request spare part date<span class="pull-right">'.$value['request_spare_part_date'].'</span></p>
                            <p>Receive spare part date <span class="pull-right">'.$value['receive_spare_part_date'].'</span></p>
                            <p>Delivery due date<span class="pull-right">'.$value['delivery_due_date'].'</span></p>
                            <p>Return spare part date<span class="pull-right"><input style="height:20px;" data-part-sid="'.$value['sid'].'" class="return_spare_part_date" type="date" value="'.$value['return_spare_part_date'].'" /></span></p>
                            <p>Return type<span class="pull-right">'.$spare_part_return_type.'</span></p>
                            </div>
                          <!--</a>-->
                      </li>';
          }
        }else{
            $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>No Task</p></div></li>';
        }
        $html .= '</ul>';
        $html .= "</div>";
        return $html;
    }

    public function templateCreateOracleSrStep1($data){
    	$html = '';
    	$html .= '<div class="content menu-view-bottom">
    	<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a id="backToSparePart" class="icon icon-left-nav pull-left" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Step 1, Choose Type</span>
	    </div>
    	<ul class="table-view">
    	<li class="table-view-divider cell-block"><div>Type</div></li>
    	<li class="table-view-cell"><a href="javascript:void(0);" data-type="Part Only" class="navigate-right type-sr"><div><p><span class="type-data">Part Only</span></p></div></a></li>
    	<li class="table-view-cell"><a href="javascript:void(0);" data-type="Escalate"  class="navigate-right type-sr"><div><p><span class="type-data">Escalate</span><span>(Collabaration)</span></p></div></a>
    	</li>
    	</ul></div>';

    	return $html;
    }

    public function templateCreateOracleSrStep2($data){
    	$html = '';
    	$html .= '<div class="content menu-view-bottom">
	    	<div class="mhn-ui-app-title-head" style="text-align:center;">
		              	<a id="backToType" class="icon icon-left-nav pull-left" href="javascript:void(0);"></a>
		              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
		            	<span class="mhn-ui-page-title" style="">Step 2. Oracle SR Information</span>
		    </div>
	    	<ul class="table-view">
		    	<li class="table-view-divider cell-block"><div>Oracle SR</div></li>
		    	<li class="table-view-cell cell-block"><input type="text" class="sr" value="'.$data['sr']['no_sr'].'"></li>
		    	<li class="table-view-divider cell-block"><div>Model</div></li>
		    	<li class="table-view-cell cell-block"><input type="text" class="model" value="'.$data['sr']['model'].'"></li>
		    	<li class="table-view-divider cell-block"><div>Oracle Create Date</div></li>
		    	<li class="table-view-cell cell-block"><input type="date" class="oracle_create_date" value="'.$data['sr']['created_date'].'"></li>
		    	<li class="table-view-cell cell-block"><div><br/><p><button id="btnNextToTask" class="btn-block btn btn-primary">Next To Task</button></p></div></li>
		    	<li class="table-view-cell cell-block"><div><p><button id="btnNextToConfirmSr" class="btn btn-success btn-block">Finish</button></p></div></li>
	    	</ul>
    	</div>';

    	return $html;
    }

    public function templateCreateOracleSrStep3($data){
    	$html = '';
    	$html .= '<div class="content menu-view-bottom">
    		<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" id="backToSr" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Step 3. Oracle Task</span>
	    	</div>
	    	<ul class="table-view">
	    		<li class="table-view-divider cell-block"><div>Task No.</div></li><li class="table-view-cell cell-block"><input type="text" class="task" value=""></li>
	    		<li class="table-view-cell cell-block"><div><br/><p><button id="btnNextToPart" class="btn btn-primary btn-block">Next To Part</button></p></div></li>
	    		<li class="table-view-cell cell-block"><div><p><button id="btnNextToConfirmTask" class="btn btn-success btn-block">Finish</button></p></div></li>
	    	</ul>
    	</div>';

    	return $html;
    }
    public function templateCreateOracleSrStep4($data){
    	$html = '';
    	$html .= '<div class="content menu-view-bottom">
    		<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a class="icon icon-left-nav pull-left" id="backToTask" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Step 4. Part Information</span>
	    	</div>
	    	<ul class="table-view">
	    		<li class="table-view-divider cell-block"><div>Part Number</div></li>
	    		<li class="table-view-cell cell-block"><input type="text" class="part_number" value=""></li>
	    		<li class="table-view-divider cell-block"><div>Part Description</div></li>
	    		<li class="table-view-cell cell-block"><input type="text" class="part_description" value=""></li>
	    		<li class="table-view-divider cell-block"><div>Request Spare Part Date</div></li>
	    		<li class="table-view-cell cell-block"><input type="date" class="request_spare_part_date" value=""></li>
	    		<li class="table-view-divider cell-block"><div>Delivery Due Date</div></li>
	    		<li class="table-view-cell cell-block"><input type="date" class="delivery_due_date" value=""></li>
	    		<li class="table-view-divider cell-block"><div>Receive Spare Part Date</div></li>
	    		<li class="table-view-cell cell-block"><input type="date" class="receive_spare_part_date" value=""></li>
	    		<li class="table-view-divider cell-block"><div>Part Quantity</div></li>
	    		<li class="table-view-cell cell-block"><input type="number" min="1" class="part_quantity" value="1"></li>
	    		<li class="table-view-cell cell-block"><div><br/><p><button id="btnNextToConfirm" class="btn btn-success btn-block">Next To Confirm</button></p></div></li>
	    	</ul>
    	</div>';

    	return $html;
    }

     public function templateCreateOracleSrStep5($data){
    	$html = '';
    	$html .= '<div class="content menu-view-bottom">
    		<div class="mhn-ui-app-title-head" style="text-align:center;">
	              	<a id="backToPart" class="icon icon-left-nav pull-left" id="backToTask" href="javascript:void(0);"></a>
	              	<a class="icon icon-left-nav pull-right" href="javascript:void(0);" style="visibility:hidden;"></a>
	            	<span class="mhn-ui-page-title" style="">Step 5. Confirm</span>
	    	</div>
	    	<ul class="table-view">
	    		<li class="table-view-divider cell-block"><div>SR Information</div></li>
	    		<li class="table-view-cell cell-block"><div><p>Type <span class="pull-right">'.$data['sr']['type'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><p>Sr No. <span class="pull-right">'.$data['sr']['no_sr'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><p>Model <span class="pull-right">'.$data['sr']['model'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><p>Oracle Create Date <span class="pull-right">'.$data['sr']['created_date'].'</span></p></li>

	    		<li class="table-view-divider cell-block"><div>Task Information</div></li>
	    		<li class="table-view-cell cell-block"><div><p>Task No. <span class="pull-right">'.$data['task']['no_task'].'</span></p></li>
	    		
	    		<li class="table-view-divider cell-block"><div>Part Information</div></li>
	    		<li class="table-view-cell cell-block"><div><p>Part Number <span class="pull-right">'.$data['part']['part_number'].'</span></p></div></li>
	    		<li class="table-view-cell cell-block"><div><p>Part Description <span class="pull-right">'.$data['part']['part_description'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><p>Request Spare Part Date <span class="pull-right">'.$data['part']['request_spare_part_date'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><p>Delivery Due Date <span class="pull-right">'.$data['part']['delivery_due_date'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><p>Receive Spare Part Date <span class="pull-right">'.$data['part']['receive_spare_part_date'].'</span></p></li>
	    		<li class="table-view-cell cell-block"><div><br/><p>Part Quantity <span class="pull-right">'.$data['part']['part_quantity'].'</p></li>
	    		<li class="table-view-cell cell-block"><div><p><button id="btnDoneToCreatePart" class="btn btn-success btn-block">Done</button></p></div></li>
	    	</ul>
    	</div>';

    	return $html;
    }
}
?>