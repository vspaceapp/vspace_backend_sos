<?php
class Outlook{

    private $db;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }
	public function sendEvent($data, $sr_sid="") {
        ob_start();
        $message = 'BEGIN:VCALENDAR
PRODID:-//Microsoft Corporation//Outlook 15.0 MIMEDIR//EN
VERSION:2.0
';
        if ($data['method'] != 'CANCEL' || empty($data['method'])) {
            $message .= 'METHOD:REQUEST'
                    . '';
        } else {
            $message .= 'METHOD:CANCEL'
                    . '';
        }
        $message .= '
X-MS-OLK-FORCEINSPECTOROPEN:TRUE
BEGIN:VTIMEZONE
TZID:SE Asia Standard Time
BEGIN:STANDARD
DTSTART:16010101T000000
TZOFFSETFROM:+0700
TZOFFSETTO:+0700
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
';
        if (!empty($data['attendee']) && FALSE) {
            $message .= 'ATTENDEE;CN=FLG;RSVP=TRUE:mailto:' . $data['attendee'] . ''
                    . '';
        }
        $message .= 'CLASS:PUBLIC
DESCRIPTION:\n
DTEND;';
//exsample
//$data['end'] = 20140918T120000
//$data['start'] = 20140918T090000
        $message .= 'TZID="SE Asia Standard Time":' . $data['end'] . '
DTSTART;TZID="SE Asia Standard Time":' . $data['start'] . '
LAST-MODIFIED:20141223T033038Z
';
        if (!empty($data['location'])) {
            $message .= 'LOCATION:' . $data['location'] . '
';
        }
        if (!empty($data['organizer'])) {
            $message .= 'ORGANIZER;CN="FLG Service":mailto:' . $data['organizer'] . '
';
        }
        $message .= 'PRIORITY:5
SEQUENCE:0
';
        if ($data['method'] != 'CANCEL' || empty($data['method'])) {
            
        } else {
            $message .= 'STATUS:CANCELLED';
        }
        if (!empty($data['summary'])) {
            $message .= 'SUMMARY; LANGUAGE=en-th:FLG';
        }
        $message .= '
TRANSP:OPAQUE
';
        if (!empty($data['UID'])) {
            $message .= 'UID:' . $data['UID'];
        }
        $message .= '
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">\n<HTML>\n<HEAD>\n<META NAME="Generator" CONTENT="MS Exchange Server version rmj.rmm.rup.rpr">\n<TITLE>' . $data['title'] . '</TITLE>\n</HEAD>\n<BODY>\n<!-- Converted 
	from text/rtf format -->\n\n<P DIR=LTR><SPAN LANG="en-us"><FONT FACE="Calibri"></FONT></SPAN><SPAN LANG="en-us"></SPAN><SPAN LANG="en-us"></SPAN><SPAN LANG="en-us"></SPAN><SPAN LANG="th"> <FONT SIZE=4 FACE="Cordia New (Thai)">' . $data['summary'] . '</FONT></SPAN><SPAN LANG="en-us"> 
	<FONT FACE="Calibri"></FONT></SPAN><SPAN LANG="en-us"></SPAN></P>\n\n</BODY>\n</HTML>
X-MICROSOFT-CDO-BUSYSTATUS:BUSY
X-MICROSOFT-CDO-IMPORTANCE:1
X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY
X-MICROSOFT-DISALLOW-COUNTER:FALSE
X-MS-OLK-AUTOSTARTCHECK:FALSE
X-MS-OLK-CONFTYPE:0
BEGIN:VALARM
TRIGGER:-PT120M
ACTION:DISPLAY
DESCRIPTION:Reminder
END:VALARM
END:VEVENT
END:VCALENDAR';
        /* Setting the header part, this is important */
        $headers = "From: <" . $data['from'] . ">\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/calendar; method=REQUEST;\n";
        $headers .= 'charset="UTF-8;\n"';
        $headers .= 'Content-Disposition: inline; filename=calendar.ics';
        $headers .= "\n";
        $headers .= "Content-Transfer-Encoding: 8bit";
        /* mail content , attaching the ics detail in the mail as content */
        $subject = $data['title'] . "";
        $subject = html_entity_decode($subject, ENT_QUOTES, 'UTF-8');
        // $to = "autsakorn.t@firstlogic.co.th";
        $to = "" . $data['to'] . "";
        /* mail send */
        if (mail($to, $subject, $message, $headers)) {
             if($sr_sid!=""){
                $this->saveRecordSentOutlook($sr_sid, $data['to']);
            }
            echo "sent";
        } else {
            echo "error";
        }
    }



    public function sendEventTis620($data) {
        ob_start();
        $message = 'BEGIN:VCALENDAR
PRODID:-//Microsoft Corporation//Outlook 15.0 MIMEDIR//EN
VERSION:2.0
';
        if ($data['method'] != 'CANCEL' || empty($data['method'])) {
            $message .= 'METHOD:REQUEST'
                    . '';
        } else {
            $message .= 'METHOD:CANCEL'
                    . '';
        }
        $message .= '
X-MS-OLK-FORCEINSPECTOROPEN:TRUE
BEGIN:VTIMEZONE
TZID:SE Asia Standard Time
BEGIN:STANDARD
DTSTART:16010101T000000
TZOFFSETFROM:+0700
TZOFFSETTO:+0700
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
';
        if (!empty($data['attendee']) && FALSE) {
            $message .= 'ATTENDEE;CN=FLG;RSVP=TRUE:mailto:' . $data['attendee'] . ''
                    . '';
        }
        $message .= 'CLASS:PUBLIC
DESCRIPTION:\n
DTEND;';
//exsample
//$data['end'] = 20140918T120000
//$data['start'] = 20140918T090000
        $message .= 'TZID="SE Asia Standard Time":' . $data['end'] . '
DTSTART;TZID="SE Asia Standard Time":' . $data['start'] . '
LAST-MODIFIED:20141223T033038Z
';
        if (!empty($data['location'])) {
            $message .= 'LOCATION:' . $data['location'] . '
';
        }
        if (!empty($data['organizer'])) {
            $message .= 'ORGANIZER;CN="FLG Service":mailto:' . $data['organizer'] . '
';
        }
        $message .= 'PRIORITY:5
SEQUENCE:0
';
        if ($data['method'] != 'CANCEL' || empty($data['method'])) {
            
        } else {
            $message .= 'STATUS:CANCELLED';
        }
        if (!empty($data['summary'])) {
            $message .= 'SUMMARY; LANGUAGE=en-th:FLG';
        }
        $message .= '
TRANSP:OPAQUE
';
        if (!empty($data['UID'])) {
            $message .= 'UID:' . $data['UID'];
        }
        $message .= '
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">\n<HTML>\n<HEAD>\n<META NAME="Generator" CONTENT="MS Exchange Server version rmj.rmm.rup.rpr">\n<TITLE>' . $data['title'] . '</TITLE>\n</HEAD>\n<BODY>\n<!-- Converted 
    from text/rtf format -->\n\n<P DIR=LTR><SPAN LANG="en-us"><FONT FACE="Calibri"></FONT></SPAN><SPAN LANG="en-us"></SPAN><SPAN LANG="en-us"></SPAN><SPAN LANG="en-us"></SPAN><SPAN LANG="th"> <FONT SIZE=4 FACE="Cordia New (Thai)">' . $data['summary'] . '</FONT></SPAN><SPAN LANG="en-us"> 
    <FONT FACE="Calibri"></FONT></SPAN><SPAN LANG="en-us"></SPAN></P>\n\n</BODY>\n</HTML>
X-MICROSOFT-CDO-BUSYSTATUS:BUSY
X-MICROSOFT-CDO-IMPORTANCE:1
X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY
X-MICROSOFT-DISALLOW-COUNTER:FALSE
X-MS-OLK-AUTOSTARTCHECK:FALSE
X-MS-OLK-CONFTYPE:0
BEGIN:VALARM
TRIGGER:-PT120M
ACTION:DISPLAY
DESCRIPTION:Reminder
END:VALARM
END:VEVENT
END:VCALENDAR';
        /* Setting the header part, this is important */
        $headers = "From: <" . $data['from'] . ">\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/calendar; method=REQUEST;\n";
        $headers .= 'charset="TIS-620;\n"';
        $headers .= 'Content-Disposition: inline; filename=calendar.ics';
        $headers .= "\n";
        $headers .= "Content-Transfer-Encoding: 8bit";
        /* mail content , attaching the ics detail in the mail as content */
        $subject = $data['title'] . "";
        // $subject = html_entity_decode($subject, ENT_QUOTES, 'TIS-620');
        // $to = "teerawut.p@firstlogic.co.th";
        $to = "" . $data['to'] . "";
        /* mail send */
        if (mail($to, $subject, $message, $headers)) {
            if($sr_sid!=""){
                $this->saveRecordSentOutlook($sr_sid);
            }
            echo "sent";
        } else {
            echo "error";
        }
    }

    public function saveRecordSentOutlook($sr_sid, $sent_to){
        $sql = "INSERT INTO sent_outlook (tasks_sid, sent_to, create_datetime) VALUES (:tasks_sid,:sent_to,NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$sr_sid,':sent_to'=>$sent_to));

    }

    public function isSentOutlook($sr_sid, $email){
        $sql = "SELECT * FROM sent_outlook WHERE tasks_sid = :tasks_sid AND sent_to = :sent_to";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$sr_sid,':sent_to'=>$email));
        $r = $q->fetch();
        if($r['sid']>0){
            return true;
        }
        return false;
    }
}
?>