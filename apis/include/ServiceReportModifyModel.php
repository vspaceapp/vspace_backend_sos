<?php
class ServiceReportModifyModel{

    private $db;
    private $objCons;
    private $email;
    public $permission_role;
    function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->email = $email;
        if($this->email!=""){
            $this->permission_role = $this->objCons->permissionRoleByEmail($this->email); 
        }
    }

    public function cancelSR($tasks_sid, $email){
        $sql = "UPDATE tasks SET last_status = '-2', updated_by = :email, updated_datetime = NOW() WHERE sid = :tasks_sid";   
        
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':tasks_sid' => $tasks_sid,
            ':email'=>$email
            ));
    }


    public function changeSubject($subject_service_report, $tasks_sid, $email){

        $sql = "UPDATE tasks SET subject_service_report = :subject_service_report, updated_by = :email, updated_datetime = NOW() 
        WHERE sid = :tasks_sid";

        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':subject_service_report' => $subject_service_report,
            ':email'=>$email,
            ':tasks_sid' => $tasks_sid
        ));
    }

    public function changeServiceType($new_service_type,$tasks_sid,$email){
        $sql = "UPDATE tasks SET service_type = :new_service_type, updated_by = :email, updated_datetime = NOW() WHERE sid = :tasks_sid";

        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':new_service_type'=> $new_service_type,
            ':tasks_sid'=> $tasks_sid,
            ':email'=> $email
            ));
    }

    public function getStatusTasks($tasks_sid,$email){
        $sql = "SELECT last_status FROM tasks WHERE sid = :tasks_sid";

        //$r = $q->fetch();
        //return $r['last_status'];

        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':tasks_sid' => $tasks_sid
        ));
        $r = $q->fetch(); 
        return $r['last_status'];
    }

    public function inputAction($email,$token, $input_action, $input_action_sid, $tasks_sid){
        $sql = "UPDATE task_input_action SET solution = :input_action, update_by = :email, update_datetime = NOW() WHERE sid = :input_action_sid ";

        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':input_action'=> $input_action,
            ':email'=>$email,
            ':input_action_sid'=>$input_action_sid
            ));
    }

}
?>