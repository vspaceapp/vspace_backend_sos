<?php
error_reporting(0);
ini_set('display_errors', 0);
ini_set('max_execution_time', 300);

class SlaModel{

	private $db;
  private $m;
  private $role_sid, $email, $token, $permission_sid;
	private $ticket_sid;

	function __construct($email="") {
//        require_once dirname(__FILE__) . '/db_connect.php';
//        require_once ROOT_PATH.'apis/include/CaseModel.php';

        require_once  __DIR__."/db_connect.php";
        require_once  __DIR__."/CaseModel.php";
        // opening db connection
        $this->email = $email;
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function getRuleSlaBySid($rule_sid){
        $sql = "SELECT * FROM rule_sla_contract WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$rule_sid));
        $r = $q->fetch();

    	$sql = "SELECT RSD.*,RSC.contract_no,RSC.project, RSC.customer FROM rule_sla_contract RSC LEFT JOIN rule_sla_detail RSD ON RSC.sid = RSD.rule_sla_contract_sid WHERE RSC.sid = :sid";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':sid'=>$rule_sid));
    	$r['sla_detail'] = $q->fetchAll();
    	return $r;
    }

    public function getRuleSla($search, $sort, $sort_type){
    	$sql = "SELECT * FROM rule_sla_contract WHERE 1 ";
    	if($search){
    		$sql .= "AND (contract_no LIKE '%".$search."%' OR project LIKE '%".$search."%' OR customer LIKE '%".$search."%' OR end_user LIKE '%".$search."%') ";
    	}
    	$sql .= " ORDER BY ".$sort." ".$sort_type;
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();

    	return $r;
    }

    public function teamOfEmployee(){
        // $sql = "SELECT DISTINCT(CONCAT(trim(unitlev1),'/',trim(unitlev3))) team FROM employee WHERE updated_datetime is not null AND (termin_type = '' OR termin_type IS NULL) ORDER BY unitlev1";


        $sql = "SELECT DISTINCT(group_name) team FROM gable_incharge_team ORDER BY group_name ASC";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        $data = array();
        foreach ($r as $key => $value) {
            array_push($data, $value['team']);
        }
        return $data;
    }

    public function enduserOfTicket() {
        $sql = "SELECT DISTINCT(end_user) end_user FROM ticket WHERE end_user <> ''
        AND (LENGTH(refer_remedy_hd) = '8' OR LENGTH(refer_remedy_hd) = '15') ORDER BY end_user ASC ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        $data = array();
        foreach ($r as $key => $value) {
            array_push($data, $value['end_user']);
        }
        return $data;
    }

    public function generateSlaRule($data, $create_by){
        try{
            $value_of_key = "";
            if($data['ruleFor']=="Contract"){
                $value_of_key = trim($data['contract_no']);
            }else if($data['ruleFor']=="Company"){
                $value_of_key = trim($data['enduser']);
            }else if($data['ruleFor']=="Team"){
                $value_of_key = trim($data['teamSupport'][0]['name']);
            }else if($data['ruleFor']=="Serial"){
                $value_of_key = trim($data['serial_no']);
            }

            $sql = "INSERT INTO sla_rule (key_by, value_of_key, team_unit, support_start, support_end, period_start, period_end, response_critical, response_high, response_medium, response_low, onsite_critical, onsite_high, onsite_medium, onsite_low, workaround_critical, workaround_high, workaround_medium, workaround_low, resolution_critical, resolution_high, resolution_medium, resolution_low,
                response_critical_unit, response_high_unit, response_medium_unit, response_low_unit,
                onsite_critical_unit, onsite_high_unit, onsite_medium_unit, onsite_low_unit,
                workaround_critical_unit, workaround_high_unit, workaround_medium_unit, workaround_low_unit,
                resolution_critical_unit, resolution_high_unit, resolution_medium_unit, resolution_low_unit,
                created_datetime, updated_datetime, created_by, data_status, updated_by,project_name,end_user, support_day, support_hour, break_time, set_break, minute_break, contract, province, serial_no )
            VALUES (:key_by, :value_of_key, :team_unit, :support_start, :support_end, :period_start, :period_end, :response_critical, :response_high, :response_medium, :response_low, :onsite_critical, :onsite_high, :onsite_medium, :onsite_low, :workaround_critical, :workaround_high, :workaround_medium, :workaround_low, :resolution_critical, :resolution_high, :resolution_medium, :resolution_low,
                :response_critical_unit, :response_high_unit, :response_medium_unit, :response_low_unit,
                :onsite_critical_unit, :onsite_high_unit, :onsite_medium_unit, :onsite_low_unit,
                :workaround_critical_unit, :workaround_high_unit, :workaround_medium_unit, :workaround_low_unit,
                :resolution_critical_unit, :resolution_high_unit, :resolution_medium_unit, :resolution_low_unit,
                NOW(), NOW() , :created_by, '1', :updated_by,:project_name,:end_user, :support_day, :support_hour,
                :break_time, :set_break, :minute_break, :contract, :province, :serial_no )";
            $q = $this->db->prepare($sql);

            $province = "";
            if(isset($data['province']) && count($data['province'])>0){
                foreach ($data['province'] as $key => $value) {
                    if(isset($value['value'])){
                        $province .= $value['value'];
                        if($key+1!=count($data['province'])){
                            $province .= ",";
                        }
                    }
                }
            }
            if(isset($data['teamSupport'])){
                foreach ($data['teamSupport'] as $key => $value) {
                    if(isset($value['start_time'])){
                        $start_time = explode(" ",$value['start_time']);
                    }else{
                        $start_time[4] = "00:00:00";
                    }
                    if(isset($value['end_time'])){
                        $end_time = explode(" ",$value['end_time']);
                    }else{
                        $end_time[4] = "00:00:00";
                    }
                    if(isset($value['break_time'])){
                        $break_time = explode(" ",$value['break_time']);
                    }else{
                        $break_time[4] = "00:00:00";
                    }
                    $q->execute(array(
                        ':key_by'=>$data['ruleFor'],
                        ':value_of_key'=>$value_of_key,
                        ':team_unit'=>trim($value['name']),
                        ':support_start'=>($value['support_hour']=="24")?"00:00":$start_time[4],
                        ':support_end'=>($value['support_hour']=="24")?"00:00":$end_time[4],
                        ':period_start'=>$data['period_start'],
                        ':period_end'=>$data['period_end'],
                        ':response_critical'=>$data['response_critical'],
                        ':response_high'=>$data['response_high'],
                        ':response_medium'=>$data['response_medium'],
                        ':response_low'=>$data['response_low'],
                        ':onsite_critical'=>$data['onsite_critical'],
                        ':onsite_high'=>$data['onsite_high'],
                        ':onsite_medium'=>$data['onsite_medium'],
                        ':onsite_low'=>$data['onsite_low'],
                        ':workaround_critical'=>$data['workaround_critical'],
                        ':workaround_high'=>$data['workaround_high'],
                        ':workaround_medium'=>$data['workaround_medium'],
                        ':workaround_low'=>$data['workaround_low'],
                        ':resolution_critical'=>$data['resolution_critical'],
                        ':resolution_high'=>$data['resolution_high'],
                        ':resolution_medium'=>$data['resolution_medium'],
                        ':resolution_low'=>$data['resolution_low'],
                        ':response_critical_unit'=>$data['response_critical_unit'],
                        ':response_high_unit'=>$data['response_high_unit'],
                        ':response_medium_unit'=>$data['response_medium_unit'],
                        ':response_low_unit'=>$data['response_low_unit'],
                        ':onsite_critical_unit'=>$data['onsite_critical_unit'],
                        ':onsite_high_unit'=>$data['onsite_high_unit'],
                        ':onsite_medium_unit'=>$data['onsite_medium_unit'],
                        ':onsite_low_unit'=>$data['onsite_low_unit'],
                        ':workaround_critical_unit'=>$data['workaround_critical_unit'],
                        ':workaround_high_unit'=>$data['workaround_high_unit'],
                        ':workaround_medium_unit'=>$data['workaround_medium_unit'],
                        ':workaround_low_unit'=>$data['workaround_low_unit'],
                        ':resolution_critical_unit'=>$data['resolution_critical_unit'],
                        ':resolution_high_unit'=>$data['resolution_high_unit'],
                        ':resolution_medium_unit'=>$data['resolution_medium_unit'],
                        ':resolution_low_unit'=>$data['resolution_low_unit'],
                        ':created_by'=>$create_by,
                        ':updated_by'=>$create_by,
                        ':project_name'=>$data['project'],
                        ':end_user'=>$data['enduser'],
                        ':support_day'=>trim($value['support_day']),
                        ':support_hour'=>trim($value['support_hour']),
                        ':break_time'=>$break_time[4],
                        ':set_break'=>$value['set_break'],
                        ':minute_break'=>$value['minute_break'],
                        ':contract'=>$data['contract_no'],
                        ':province'=>$province,
                        ':serial_no'=>$data['serial_no']
                    ));
                }
            }else{
                // $start_time = explode(" ", $value['start_time']);
                // $end_time = explode(" ", $value['end_time']);

                $q->execute(array(
                    ':key_by'=>$data['ruleFor'],
                    ':value_of_key'=>$value_of_key,
                    ':team_unit'=>"",
                    ':support'=>"",
                    ':support_start'=>"",
                    ':support_end'=>"",
                    ':period_start'=>$data['period_start'],
                    ':period_end'=>$data['period_end'],
                    ':response_critical'=>$data['response_critical'],
                    ':response_high'=>$data['response_high'],
                    ':response_medium'=>$data['response_medium'],
                    ':response_low'=>$data['response_low'],
                    ':onsite_critical'=>$data['onsite_critical'],
                    ':onsite_high'=>$data['onsite_high'],
                    ':onsite_medium'=>$data['onsite_medium'],
                    ':onsite_low'=>$data['onsite_low'],
                    ':workaround_critical'=>$data['workaround_critical'],
                    ':workaround_high'=>$data['workaround_high'],
                    ':workaround_medium'=>$data['workaround_medium'],
                    ':workaround_low'=>$data['workaround_low'],
                    ':resolution_critical'=>$data['resolution_critical'],
                    ':resolution_high'=>$data['resolution_high'],
                    ':resolution_medium'=>$data['resolution_medium'],
                    ':resolution_low'=>$data['resolution_low'],
                    ':response_critical_unit'=>$data['response_critical_unit'],
                    ':response_high_unit'=>$data['response_high_unit'],
                    ':response_medium_unit'=>$data['response_medium_unit'],
                    ':response_low_unit'=>$data['response_low_unit'],
                    ':onsite_critical_unit'=>$data['onsite_critical_unit'],
                    ':onsite_high_unit'=>$data['onsite_high_unit'],
                    ':onsite_medium_unit'=>$data['onsite_medium_unit'],
                    ':onsite_low_unit'=>$data['onsite_low_unit'],
                    ':workaround_critical_unit'=>$data['workaround_critical_unit'],
                    ':workaround_high_unit'=>$data['workaround_high_unit'],
                    ':workaround_medium_unit'=>$data['workaround_medium_unit'],
                    ':workaround_low_unit'=>$data['workaround_low_unit'],
                    ':resolution_critical_unit'=>$data['resolution_critical_unit'],
                    ':resolution_high_unit'=>$data['resolution_high_unit'],
                    ':resolution_medium_unit'=>$data['resolution_medium_unit'],
                    ':resolution_low_unit'=>$data['resolution_low_unit'],
                    ':created_by'=>$create_by,
                    ':updated_by'=>$create_by,
                    ':project_name'=>$data['project'],
                    ':end_user'=>$data['enduser'],
                    ':support_day'=>"",
                    ':support_hour'=>"",
                    ':break_time'=>"",
                    ':set_break'=>"",
                    ':minute_break'=>"",
                    ':contract'=>$data['contract_no'],
                    ':province'=>$province,
                    ':serial_no'=>$data['serial_no']
                ));
            }
        }catch(PDOException $e) {
            $error = $e->getMessage();
        }
    }

    public function editSlaRule($data, $create_by){
        // print_r($data);
        try{
            $province = "";
            if(isset($data['province']) && count($data['province'])>0){
                foreach ($data['province'] as $key => $value) {
                    if(isset($value['value'])){
                        $province .= $value['value'];
                        if($key+1!=count($data['province'])){
                            $province .= ",";
                        }
                    }
                }
            }

            $sql = "UPDATE sla_rule SET updated_datetime = NOW(),
            period_start = :period_start,
            period_end = :period_end,

            response_critical = :response_critical,
            response_high = :response_high,
            response_medium = :response_medium,
            response_low = :response_low,

            response_critical_unit = :response_critical_unit,
            response_high_unit = :response_high_unit,
            response_medium_unit = :response_medium_unit,
            response_low_unit = :response_low_unit,

            onsite_critical = :onsite_critical,
            onsite_high = :onsite_high,
            onsite_medium = :onsite_medium,
            onsite_low = :onsite_low,

            onsite_critical_unit = :onsite_critical_unit,
            onsite_high_unit = :onsite_high_unit,
            onsite_medium_unit = :onsite_medium_unit,
            onsite_low_unit = :onsite_low_unit,

            workaround_critical = :workaround_critical,
            workaround_high = :workaround_high,
            workaround_medium = :workaround_medium,
            workaround_low = :workaround_low,

            workaround_critical_unit = :workaround_critical_unit,
            workaround_high_unit = :workaround_high_unit,
            workaround_medium_unit = :workaround_medium_unit,
            workaround_low_unit = :workaround_low_unit,

            resolution_critical = :resolution_critical,
            resolution_high = :resolution_high,
            resolution_medium = :resolution_medium,
            resolution_low = :resolution_low,

            resolution_critical_unit = :resolution_critical_unit,
            resolution_high_unit = :resolution_high_unit,
            resolution_medium_unit = :resolution_medium_unit,
            resolution_low_unit = :resolution_low_unit,
            updated_by = :email, province = :province
             WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':period_start'=>$data['period_start'],
                ':period_end'=>$data['period_end'],

                ':response_critical'=>$data['response_critical'],
                ':response_high'=>$data['response_high'],
                ':response_medium'=>$data['response_medium'],
                ':response_low'=>$data['response_low'],

                ':response_critical_unit'=>$data['response_critical_unit'],
                ':response_high_unit'=>$data['response_high_unit'],
                ':response_medium_unit'=>$data['response_medium_unit'],
                ':response_low_unit'=>$data['response_low_unit'],

                ':onsite_critical'=>$data['onsite_critical'],
                ':onsite_high'=>$data['onsite_high'],
                ':onsite_medium'=>$data['onsite_medium'],
                ':onsite_low'=>$data['onsite_low'],

                ':onsite_critical_unit'=>$data['onsite_critical_unit'],
                ':onsite_high_unit'=>$data['onsite_high_unit'],
                ':onsite_medium_unit'=>$data['onsite_medium_unit'],
                ':onsite_low_unit'=>$data['onsite_low_unit'],

                ':workaround_critical'=>$data['workaround_critical'],
                ':workaround_high'=>$data['workaround_high'],
                ':workaround_medium'=>$data['workaround_medium'],
                ':workaround_low'=>$data['workaround_low'],

                ':workaround_critical_unit'=>$data['workaround_critical_unit'],
                ':workaround_high_unit'=>$data['workaround_high_unit'],
                ':workaround_medium_unit'=>$data['workaround_medium_unit'],
                ':workaround_low_unit'=>$data['workaround_low_unit'],

                ':resolution_critical'=>$data['resolution_critical'],
                ':resolution_high'=>$data['resolution_high'],
                ':resolution_medium'=>$data['resolution_medium'],
                ':resolution_low'=>$data['resolution_low'],

                ':resolution_critical_unit'=>$data['resolution_critical_unit'],
                ':resolution_high_unit'=>$data['resolution_high_unit'],
                ':resolution_medium_unit'=>$data['resolution_medium_unit'],
                ':resolution_low_unit'=>$data['resolution_low_unit'],
                ':email'=>$create_by,
                ':province'=>$province,
                ':sid'=>$data['sid']
                )
            );
        }catch(PDOException $e) {
            $error = $e->getMessage();
        }
    }

    public function deleteSlaRule($data, $create_by){
        try{
            $sql = "UPDATE sla_rule SET
            updated_datetime = NOW(),
            data_status = '0',
            updated_by = :email
            WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':email'=>$create_by,
                ':sid'=>$data['sid']
                ));

        }catch(PDOException $e) {
            $error = $e->getMessage();
        }
    }
    public function slaRule(){
        try{
            $sql = "SELECT SR.*,U.name created_by_name, CONCAT(SR.support_day,'x',SR.support_hour) support FROM sla_rule SR LEFT JOIN user U ON SR.created_by = U.email WHERE SR.data_status = '1' ORDER BY created_datetime DESC";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e) {
            $error = $e->getMessage();
        }
    }

    public function slaRuleDetail($sid){
        try {
            $sql = "SELECT * FROM sla_rule WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$sid));
            $r = $q->fetch();
            if(isset($r['province']) && $r['province']!=""){
                $province = explode(",", $r['province']);
                $provinceValues = array();
                foreach ($province as $key => $value) {
                    array_push($provinceValues, array('value'=>$value,'label'=>$value));
                }
                $r['province'] = $provinceValues;
            }else{
                $r['province'] = array();
            }

            return $r;
        }catch(PDOException $e){
            $error = $e->getMessage();
            return $error;
        }
    }

    public function slaProcessNewTicket(){
        //flow
        // 1. list new ticket
        // 2. cancel old sla (change cases_status_sid = 0)
        // 3. insert new sla for incident ticket
        // 4.
        try {
            $objCaseModel = new CaseModel();
            $tickets = $this->listTicketStatusZero(); // return ticket
            foreach ($tickets as $key => $ticket) {
                //เปลี่ยน case_status_sid ของเก่าให้เป็น 0 เพราะว่าจะคำนวนของใหม่
                $sql = "UPDATE ticket_sla SET case_status_sid = 0 WHERE ticket_sid = :ticket_sid ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':ticket_sid'=>$ticket['sid']));
                if($ticket['case_type'] == 'Incident'){
                    $dataSla = $this->findSlaRule($ticket);
                }else if($ticket['case_type'] == 'request'){
                    $dataSla = $this->findSlaRuleForServiceRequest($ticket);
                }
                if(isset($dataSla['sid']) && $dataSla['sid']>0){
                    $this->insertUsageSla($ticket, $dataSla);
                }

                $sql = "UPDATE ticket SET gen_sla = 1 WHERE sid = :ticket_sid ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':ticket_sid'=>$ticket['sid']));

                //Need data format array key
                // 'end_user'
                // 'team_support'
                // 'ticket_sid'
                // $ticket['end_user_group'];
                // list all log status sla (ไม่เอา worklog)
                $dataTicketStatus = $this->getTicketStatus($ticket['sid']);

                foreach ($dataTicketStatus as $k => $v) {
                    $objCaseModel->updateChangeAssignedTicket($ticket['sid'], $v['worklog'], $v['case_status_sid'], $v['create_by'], $v['expect_pending'], $v['solution'], $v['solution_detail']);
                }
                $dataTicketAlert = $this->getTicketAlert($ticket['sid']);
                if(count($dataTicketAlert)<1){
                    // เอาออกชั่วคราว เพราะมีปัญหา
                    //$this->findAlertRule($ticket);
                }

                // เอาออก เพราะเปลี่ยนการ sql listTicketStatusZero ไม่ต้องเปลี่ยนกลับ
                //$this->updateStatusToAssigned($ticket['sid']);
            }
            return $tickets;
        }catch(PDOException $e){

        }
    }

    private function getTicketAlert($ticket_sid){
        $sql = "SELECT * FROM ticket_alert WHERE ticket_sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    private function getTicketStatus($ticket_sid){
        $sql = "SELECT * FROM ticket_status WHERE ticket_sid = :ticket_sid AND case_status_sid != '8' ORDER BY create_datetime ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }
    private function calDuedate($created_datetime, $value, $unit, $slaData){

        $unitC = "";
        if($unit=="Min"){
            $unitC = "minute";
        }
        if($unit=="Hour"){
            $unitC = "hour";
        }
        if($unit=="Day"){
            $unitC = "day";
        }
        if($unit=="NBD"){
            $unitC = "day";
        }
        if($slaData['set_break']=="true"){
            return $this->calForNot7x24($created_datetime, $value, $unit, $slaData);
        }else{
	        $sql = "SELECT DATE_ADD(:created_datetime, INTERVAL :value ".$unitC.") AS due_datetime ";
	        $q = $this->db->prepare($sql);
	        $q->execute(array(':created_datetime'=>$created_datetime,':value'=>$value));
	        $r = $q->fetch();
	        return $r['due_datetime'];
        }
    }
		private function calForNot7x24($created_datetime, $value, $unit, $slaData){
			$due_datetime = $created_datetime;
			$this->db->beginTransaction();
			if($unit=="Min" || $unit=="Hour"){
				if($unit=="Hour"){
					$value *= 60;
				}
				$totalMinute = 0;
				$i = 0;

				$sql = "SELECT DATE_FORMAT(DATE_ADD(:break_time, INTERVAL :value minute),'%H:%i:%s') AS end_break_time ";
				$q = $this->db->prepare($sql);
				$q->execute(array(':break_time'=>'2017-09-09 '.$slaData['break_time'],':value'=>$slaData['minute_break']));
				$r = $q->fetch();
				$end_break_time = $r['end_break_time'];


				while ($totalMinute!=$value) {

					$sql = "SELECT DATE_ADD(:created_datetime, INTERVAL :value minute) AS due_datetime,
						DATE_FORMAT(DATE_ADD(:created_datetime, INTERVAL :value minute),'%H:%i:%s') AS due_time,
						DATE_FORMAT(DATE_ADD(:created_datetime, INTERVAL :value minute),'%Y-%m-%d') AS due_date ";
	        $q = $this->db->prepare($sql);
	        $q->execute(array(':created_datetime'=>$created_datetime,':value'=>$i));
	        $r = $q->fetch();
					$due_datetime = $r['due_datetime'];

					if(!$this->isHoliday($r['due_date'])){
						if(($r['due_time']>$slaData['support_start'] && $r['due_time']<$slaData['break_time']) ||
						($r['due_time']>$end_break_time && $r['due_time']<$slaData['support_end']) ){
							$totalMinute++;
						}
					}
					$i++;
				}
			}else{
				$totalDay = 0;
				$i = 0;
				while ($totalDay!=$value) {
					$sql = "SELECT DATE_ADD(:created_datetime, INTERVAL :value day) AS due_datetime,
					DATE_FORMAT(DATE_ADD(:created_datetime, INTERVAL :value day),'%Y-%m-%d') AS due_date ";
	        $q = $this->db->prepare($sql);
	        $q->execute(array(':created_datetime'=>$created_datetime,':value'=>$i));
	        $r = $q->fetch();
					$due_datetime = $r['due_datetime'];
					if(!$this->isHoliday($r['due_date'])){
						$totalDay++;
					}
					$i++;
				}
			}
			$this->db->commit();
			return $due_datetime;
		}
		private function isHoliday($day){
			$sql = "SELECT * FROM tbl_holiday WHERE holiday = :holiday ORDER BY holiday DESC";
			$q = $this->db->prepare($sql);
			$q->execute(array(':holiday'=>$day));
			$r = $q->fetch();
			if(isset($r['id']) && $r['id']>0){
				return true;
			}else{
				return false;
			}
		}
    private function insertUsageSla($ticket, $slaData){
        $ticket_sid = $ticket['sid'];
        $hd = $ticket['refer_remedy_hd'];
        $contract_no = $ticket['contract'];
        $status = "In Process";
        $objCaseModel = new CaseModel();
        $objCaseModel->setTicketSet($ticket['sid']);
        $createDatetimeSlaForTicket = $objCaseModel->getCreateDatetimeSlaForTicket();
        if($createDatetimeSlaForTicket){
            $useCreatedDatetime = $createDatetimeSlaForTicket;
        }else{
            $useCreatedDatetime = $ticket['create_datetime'];
        }

        // INSERT RESPONSE
        if($slaData['response']>0){
            $due_datetime = $this->calDuedate($useCreatedDatetime, $slaData['response'], $slaData['response_unit'], $slaData);
            $name = 'Response';
            $uniq = $name." ".$ticket_sid." ".$slaData['response']." ".$slaData['response_unit'];
            $name .= " (".$slaData['response']."".$slaData['response_unit'].")";
            $case_status_sid = "2";
            $this->insertTicketSla($name, $ticket_sid, $hd, $status, $due_datetime,
                $uniq, $contract_no, $case_status_sid, $slaData, $slaData['response'], $slaData['response_unit']);
        }

        // INSERT resolution
        if($slaData['resolution']>0){
            $due_datetime = "";
            $due_datetime = $this->calDuedate($useCreatedDatetime, $slaData['resolution'], $slaData['resolution_unit'], $slaData);
            $name = 'Resolution';
            $uniq = $name." ".$ticket_sid." ".$slaData['resolution']." ".$slaData['resolution_unit'];
            $name .= " (".$slaData['resolution']."".$slaData['resolution_unit'].")";
            $case_status_sid = "5";
            $this->insertTicketSla($name, $ticket_sid, $hd, $status, $due_datetime,
                $uniq, $contract_no, $case_status_sid, $slaData, $slaData['resolution'], $slaData['resolution_unit']);
        }



    }

    private function insertTicketSla($name, $ticket_sid, $hd, $status, $due_datetime, $uniq, $contract_no,
                                     $case_status_sid, $slaData, $slaValue, $slaUnit){
        $sql = "INSERT INTO ticket_sla (name, ticket_sid, hd, status, due_datetime, created_datetime, updated_datetime, uniq, contract_no, case_status_sid, sla_value, sla_unit, support_start, support_end, break_time, set_break, minute_break)
            VALUES  (:name, :ticket_sid, :hd, :status, :due_datetime, NOW(), NOW(), :uniq, :contract_no, :case_status_sid, :sla_value, :sla_unit, :support_start, :support_end, :break_time, :set_break, :minute_break)";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(
                ':name'=>$name,
                ':ticket_sid'=>$ticket_sid,
                ':hd'=>$hd,
                ':status'=>$status,
                ':due_datetime'=>$due_datetime,
                ':uniq'=>$uniq,
                ':contract_no'=>$contract_no,
                ':case_status_sid'=>$case_status_sid,
                ':sla_value'=>$slaValue,
                ':sla_unit'=>$slaUnit,
                ':support_start'=>$slaData['support_start'],
                ':support_end'=>$slaData['support_end'],
                ':break_time'=>$slaData['break_time'],
                ':set_break'=>$slaData['set_break'],
                ':minute_break'=>$slaData['minute_break']
            ));
    }
    private function updateStatusToAssigned($sid){
	    $sql = "SELECT case_status_sid FROM ticket_status WHERE ticket_sid = :sid AND case_status_sid <> 8 ORDER BY create_datetime DESC LIMIT 0,1";
	    $q = $this->db->prepare($sql);
	    $q->execute(array(':sid'=>$sid));
	    $r = $q->fetch();
	    $status = ($r['case_status_sid'])?$r['case_status_sid']:'1';

        $sql = "UPDATE ticket SET status = :status WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':status'=>$status,':sid'=>$sid));
    }

    public function listTicketStatusZero(){
        try{
//            $sql = "SELECT sid,sid ticket_sid, refer_remedy_hd,serial_no,case_type,
//            contract_no contract,end_user,end_user_site end_user_address,
//            team team_support,urgency,impact,create_datetime, status FROM ticket
//            WHERE status = 0 ORDER BY sid ASC LIMIT 0,100";
//            $q = $this->db->prepare($sql);
//            $q->execute();
//            $r = $q->fetchAll();
//            return $r;

              $sql = "SELECT sid,sid ticket_sid, refer_remedy_hd,serial_no,case_type,
              contract_no contract,end_user,end_user_site end_user_address,
              team team_support,urgency,impact,create_datetime, status, request_type, number_of_device
              FROM ticket t
              WHERE gen_sla = 0
              ORDER BY create_datetime ASC LIMIT 0,100";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
        }
    }

    public function genPriority($urgency, $impact){
	    $sql = "SELECT severity FROM severity WHERE urgency = :urgency and impact = :impact";
	    $q = $this->db->prepare($sql);
	    $q->execute(array(":urgency"=>$urgency, ":impact"=>$impact));
	    $r = $q->fetch();
	    return isset($r['severity'])?$r['severity']:"none";
    }

    public function getPriorityForServiceRequest($number_of_device){
	    if($number_of_device > 20){
	        return "low";
        }else if ($number_of_device > 10){
            return "medium";
        }else if ($number_of_device > 3){
            return "high";
        }else{
            return "critical";
        }
    }

    public function findSlaRule($data){
        $priority = $this->genPriority(trim($data['urgency']), trim($data['impact']));
        $serial_no = trim($data['serial_no']);
        $contract = trim($data['contract']);
        $end_user = trim($data['end_user']);
        $team_support = trim($data['team_support']);
        $end_user_group = "";
        if(isset($data['end_user_group'])){
            $end_user_group = trim($data['end_user_group']);
        }
        $end_user_address = trim($data['end_user_address']);
        //$period_of_service = trim($data['period_of_service']);
        $dataFindby = "";
        // [nanth]
        $dataFocus = $this->findByTeam($team_support);

        if(count($dataFocus)>0 && $priority!='none'){
            $dataFindby = "team";
            $dataSlaRule = array();
            $ruleAll = $dataFocus;
            $dataProcessProvince = array();
            $dataProcessPeriodOfService = array();

//            if($end_user_address != ""){
//               $dataProcessProvince = $this->processProviceAndPeriodOfService($dataFocus, $end_user_address);
//               $dataFocus = $dataProcessProvince[0];
//            }
//            if($period_of_service!=""){
//                $dataProcessPeriodOfService = $this->processPeriodOfService($dataFocus, $period_of_service, $data['contract'], $data['team_support']);
//                $dataFocus = $dataProcessPeriodOfService[0];
//            }
            $dataSlaRule['sid'] = $dataFocus[0]['sid'];
            $dataSlaRule['support_start'] = $dataFocus[0]['support_start'];
            $dataSlaRule['support_end'] = $dataFocus[0]['support_end'];
            $dataSlaRule['break_time'] = $dataFocus[0]['break_time'];
            $dataSlaRule['set_break'] = $dataFocus[0]['set_break'];
            $dataSlaRule['minute_break'] = $dataFocus[0]['minute_break'];
            $dataSlaRule['ruleAll'] = $ruleAll;
            $dataSlaRule['priority'] = $priority;
            $dataSlaRule['dataFindby'] = $dataFindby;
            $dataSlaRule['dataProcessProvince'] = $dataProcessProvince;
            $dataSlaRule['dataProcessPeriodOfService'] = $dataProcessPeriodOfService;

            if($priority=="Severity-1"){
                $dataSlaRule['response'] = $dataFocus[0]['response_critical'];
                $dataSlaRule['response_unit'] = $dataFocus[0]['response_critical_unit'];
                $dataSlaRule['resolution'] = $dataFocus[0]['resolution_critical'];
                $dataSlaRule['resolution_unit'] = $dataFocus[0]['resolution_critical_unit'];

            }else if($priority=="Severity-2"){
                $dataSlaRule['response'] = $dataFocus[0]['response_high'];
                $dataSlaRule['response_unit'] = $dataFocus[0]['response_high_unit'];
                $dataSlaRule['resolution'] = $dataFocus[0]['resolution_high'];
                $dataSlaRule['resolution_unit'] = $dataFocus[0]['resolution_high_unit'];
            }else if($priority=="Severity-3"){
                $dataSlaRule['response'] = $dataFocus[0]['response_medium'];
                $dataSlaRule['response_unit'] = $dataFocus[0]['response_medium_unit'];
                $dataSlaRule['resolution'] = $dataFocus[0]['resolution_medium'];
                $dataSlaRule['resolution_unit'] = $dataFocus[0]['resolution_medium_unit'];

            }else if($priority=="Severity-3"){
                $dataSlaRule['response'] = $dataFocus[0]['response_low'];
                $dataSlaRule['response_unit'] = $dataFocus[0]['response_low_unit'];
                $dataSlaRule['resolution'] = $dataFocus[0]['resolution_low'];
                $dataSlaRule['resolution_unit'] = $dataFocus[0]['resolution_low_unit'];
            }
            return $dataSlaRule;
        }else{
            return array('ruleAll'=>array(),'dataFindby'=>$dataFindby,'priority'=>$priority);
        }
    }



    public function findSlaRuleForServiceRequest($dataTicket){

        $dataFocus = $this->findSlaByServiceRequestType($dataTicket['request_type']);

        $dataSlaRule['sid'] = $dataFocus['sid'];
        $dataSlaRule['support_start'] = $dataFocus['support_start'];
        $dataSlaRule['support_end'] = $dataFocus['support_end'];
        $dataSlaRule['break_time'] = $dataFocus['break_time'];
        $dataSlaRule['set_break'] = $dataFocus['set_break'];
        $dataSlaRule['minute_break'] = $dataFocus['minute_break'];
        $dataSlaRule['ruleAll'] = $dataFocus;
        $dataSlaRule['dataFindby'] = 'type service request';

        $priority = $this->getPriorityForServiceRequest($dataTicket['number_of_device']);
        if($priority == "critical"){
            $dataSlaRule['response'] = $dataFocus['response_critical'];
            $dataSlaRule['response_unit'] = $dataFocus['response_critical_unit'];
            $dataSlaRule['resolution'] = $dataFocus['resolution_critical'];
            $dataSlaRule['resolution_unit'] = $dataFocus['resolution_critical_unit'];
        }else if($priority == "high"){
            $dataSlaRule['response'] = $dataFocus['response_high'];
            $dataSlaRule['response_unit'] = $dataFocus['response_high_unit'];
            $dataSlaRule['resolution'] = $dataFocus['resolution_high'];
            $dataSlaRule['resolution_unit'] = $dataFocus['resolution_high_unit'];
        }else if($priority == "medium"){
            $dataSlaRule['response'] = $dataFocus['response_medium'];
            $dataSlaRule['response_unit'] = $dataFocus['response_medium_unit'];
            $dataSlaRule['resolution'] = $dataFocus['resolution_medium'];
            $dataSlaRule['resolution_unit'] = $dataFocus['resolution_medium_unit'];
        }else if($priority == "low"){
            $dataSlaRule['response'] = $dataFocus['response_low'];
            $dataSlaRule['response_unit'] = $dataFocus['response_low_unit'];
            $dataSlaRule['resolution'] = $dataFocus['resolution_low'];
            $dataSlaRule['resolution_unit'] = $dataFocus['resolution_low_unit'];
        }
        return $dataSlaRule;
    }

    public function findSlaByServiceRequestType($type){
        $sql = "SELECT * FROM sla_rule WHERE data_status = '1' 
        and key_by = 'type service request' and value_of_key = :type
        LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':type'=> $type));
        $r = $q->fetch();
        return $r;
    }

    private function processProviceAndPeriodOfService($data, $province){
        // print_r($data);
        $dataProviceExist = array();
        $dataProviceNull = array();
        foreach ($data as $key => $value) {
            if($value['province']!=""){
                $province_array = explode(",",$value['province']);
                foreach ($province_array as $k => $v) {
                    if(strpos(" ".$province." ", $v)>0){
                        array_push($dataProviceExist, $value);
                    }
                }
            }else{
                array_push($dataProviceNull, $value);
            }
        }
        $dataReturn = array();
        if(count($dataProviceExist)>0){
            $dataReturn = $dataProviceExist;
        }else if(count($dataProviceNull)>0){
            $dataReturn = $dataProviceNull;
        }else{
            $dataReturn = $data;
        }
        return array($dataReturn, $dataProviceExist, $dataProviceNull, $data);
    }
    private function processPeriodOfService($data, $period_of_service, $contract_no, $team_support){
        $dataMatchPeriodOfService = array();
        $dataUnmatchPeriodOfService = array();

        if( (substr($contract_no,0,2)=="T1") || (substr($contract_no,0,2)=="T2") ){
            if($team_support=="GSEC/ICS" || $team_support=="DCT/NET" || $team_support=="ECS//EPS" || $team_support=="MVG/SDS2"){
                $period_of_service = "7x24";
            }
        }


        $period_of_service = str_replace(" ", "", $period_of_service);
        $period_of_service = str_replace("X", "x", $period_of_service);
        $period_of_service = str_replace("*", "x", $period_of_service);
        $period_of_service = str_replace("X", "-", $period_of_service);
        foreach ($data as $key => $value) {
            if(($value['support_day']."x".$value['support_hour'])==$period_of_service || ($value['support_hour']."x".$value['support_day'])==$period_of_service ){
                array_push($dataMatchPeriodOfService, $value);
            }else{
                array_push($dataUnmatchPeriodOfService, $value);
            }
        }

        // print_r($dataMatchPeriodOfService);
        // print_r($dataUnmatchPeriodOfService);

        if(count($dataMatchPeriodOfService)>0){
            $dataReturn = $dataMatchPeriodOfService;
        }else if(count($dataUnmatchPeriodOfService)>0){
            $dataReturn = $dataUnmatchPeriodOfService;
        }else{
            $dataReturn = $data;
        }
        return array($dataReturn, $dataMatchPeriodOfService, $dataUnmatchPeriodOfService, $data);
    }
    private function findBySerial($serial_no, $team){
        try{
            $sql = "SELECT * FROM sla_rule WHERE data_status = '1' AND serial_no = :serial_no AND team_unit = :team ORDER BY created_datetime DESC";
            $q = $this->db->prepare($sql);
            $q->execute(array(':serial_no'=>$serial_no,':team'=>$team));
            $r = $q->fetchAll();

            // if(count($r)<1){
            //     $sql = "SELECT * FROM sla_rule WHERE data_status = '1' AND serial_no = :serial_no ORDER BY created_datetime, sid DESC";
            //     $q = $this->db->prepare($sql);
            //     $q->execute(array(':serial_no'=>$serial_no));
            //     $r = $q->fetchAll();

            //     return $r;
            // }else{
                return $r;
            // }
        }catch(PDOException $e){

        }
    }

    private function findByContract($contract, $team){
        $sql = "SELECT * FROM sla_rule WHERE data_status = '1'
        AND (serial_no = '' OR serial_no is null) AND contract = :contract AND team_unit = :team ORDER BY created_datetime, sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract'=>$contract,':team'=>$team));
        $r = $q->fetchAll();

        // if(count($r)<1){
        //     $sql = "SELECT * FROM sla_rule WHERE data_status = '1'
        //     AND (serial_no = '' OR serial_no is null)
        //     AND contract = :contract
        //     ORDER BY created_datetime, sid DESC";
        //     $q = $this->db->prepare($sql);
        //     $q->execute(array(':contract'=>$contract));
        //     $r = $q->fetchAll();
        //     return $r;
        // }else{
            return $r;
        // }
    }

    private function findByEnduser($end_user, $team, $end_user_group){
        $sql = "SELECT * FROM sla_rule WHERE data_status = '1'
        AND (serial_no = '' OR serial_no is null)
        AND (contract = '' OR contract is null)
        AND (end_user = :end_user OR end_user = :end_user_group)
        AND team_unit = :team ORDER BY created_datetime, sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':end_user'=>$end_user,':team'=>$team, ':end_user_group'=>$end_user_group));
        $r = $q->fetchAll();

        // if(count($r)<1){
        //     $sql = "SELECT * FROM sla_rule WHERE data_status = '1'
        //     AND (serial_no = '' OR serial_no is null)
        //     AND (contract = '' OR contract is null)
        //     AND end_user = :end_user ORDER BY created_datetime, sid DESC";
        //     $q = $this->db->prepare($sql);
        //     $q->execute(array(':end_user'=>$end_user));
        //     $r = $q->fetchAll();
        //     return $r;
        // }else{
            return $r;
        // }
    }

    private function findByTeam($team){
        $sql = "SELECT * FROM sla_rule WHERE data_status = '1' AND team_unit = :team
        AND (serial_no = '' OR serial_no is null)
        AND (contract = '' OR contract is null)
        AND (end_user = '' OR end_user is null)
        ORDER BY created_datetime, sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':team'=> $team));
        $r = $q->fetchAll();
        return $r;
    }

    public function loadNotification($data){
        $sql = "SELECT * FROM gable_alert ORDER BY sid ASC ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    private function mapTeamSupport($team_support){
        $sql = "SELECT * FROM gable_map_group WHERE map_value = :team_support AND for_task = 'alert' ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':team_support'=>$team_support));
        $r = $q->fetch();
        if(isset($r['map_to']) && $r['map_to']!=""){
            return $r['map_to'];
        }else{
            return $team_support;
        }
    }

    private function debugAlert($email, $team_support, $end_user, $ticket_sid){
        $sql = "INSERT INTO gable_alert_debug (email, team_support, end_user, ticket_sid)
        VALUES (:email, :team_support, :end_user, :ticket_sid) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':email'=>$email,
            ':team_support'=>$team_support,
            ':end_user'=>$end_user, ':ticket_sid'=>$ticket_sid
            )
        );
    }
    public function findAlertRule($data){
        $end_user = isset($data['end_user_group'])?$data['end_user_group']:$data['end_user'];
        $ticket_sid = isset($data['ticket_sid'])?$data['ticket_sid']:0;

        // not use in kbank
        //$team_support = $this->mapTeamSupport($data['team_support']);
        $team_support = $data['team_subport'];

        // FIND GROUP CODE AND ASSIGNED_GROUP
        $sql = "SELECT GA.* FROM gable_alert GA WHERE GA.group_code = :group_code
        AND GA.assigned_group = :assigned_group
        ORDER BY FIELD(status,'Open','Response','Onsite','Workaround','Resolve','Resolution','Pending') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':group_code'=>$end_user,':assigned_group'=>$team_support));
        $r = $q->fetchAll();

        if(count($r)<1){
            $sql = "SELECT GA.* FROM gable_alert GA WHERE GA.assigned_group = :assigned_group AND GA.group_code = ''
            ORDER BY FIELD(status,'Open','Response','Onsite','Workaround','Resolve','Resolution','Pending') ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':assigned_group'=>$team_support));
            $r = $q->fetchAll();
        }

        // not use in kbank project
        //$this->debugAlert("",$team_support, $end_user, $ticket_sid);

        $data = array(
            $r,
            $this->admEndUserGroup($end_user),
            $this->serviceGroup($data['team_support']),
            $end_user,
            $data['team_support'],
            $ticket_sid
        );

        // print_r($r);

        $this->processQueue($data);

        return $data;
    }

    private function processQueue($data){
        $alertRule = $data[0];
        $adm = $data[1];
        $serviceMgr = $data[2];
        $end_user_group = $data[3];
        $team_support = $data[4];
        $ticket_sid = isset($data[5])?$data[5]:0;

        // print_r($alertRule);
        foreach ($alertRule as $key => $value) {
            $insert = array();
            $insert['ticket_status'] = $value['status'];
            $insert['group_code'] =  $end_user_group;
            $insert['team_support'] = $team_support;
            $insert['data'] = array();
            $insert['ticket_sid'] = $ticket_sid;

            if(isset($value['adm']) && $value['adm']){
                $tmp = $this->prepareAlertValue($value['adm'], "adm");
                array_push($insert['data'], $tmp);
            }
            if(isset($value['adm_mgr']) && $value['adm_mgr']){
                $tmp = $this->prepareAlertValue($value['adm_mgr'], "adm_mgr");
                array_push($insert['data'], $tmp);
            }
            if(isset($value['incharge']) && $value['incharge']){
                $tmp = $this->prepareAlertValue($value['incharge'], "incharge");
                array_push($insert['data'], $tmp);
            }
            if(isset($value['service_mgr']) && $value['service_mgr']){
                $tmp = $this->prepareAlertValue($value['service_mgr'], "service_mgr");
                array_push($insert['data'], $tmp);
            }
            if(isset($value['mgr']) && $value['mgr']){
                $tmp = $this->prepareAlertValue($value['mgr'], "mgr");
                array_push($insert['data'], $tmp);
            }
            // if(isset($value['other']) && $value['mgr']){
            //     $tmp = $this->prepareAlertValue($value['mgr'], "mgr");
            //     array_push($insert['data'], $tmp);
            // }

            // print_r($insert);

            foreach ($insert['data'] as $keyData => $valueData) {
                foreach ($valueData as $row => $dataRow) {
                    $insert['role'] = $dataRow['role'];
                    $insert['type'] = $dataRow['type'];
                    $insert['type_alert'] = $dataRow['type_alert'];
                    $insert['unit'] = $dataRow['unit'];
                    $insert['value'] = $dataRow['value'];
                    $insert['value_minute'] = $dataRow['value_minute'];
                    $this->insertTicketAlert($insert);
                }
            }

        }
    }
    private function insertTicketAlert($data){

        $case_status_sid = "";

        if(isset($data['ticket_status'])){

            $in_queue = 1;
            $ticket_status = strtolower($data['ticket_status']);
            if($ticket_status=="response"){
                $case_status_sid = "2";
            }else if($ticket_status=="workaround"){
                $case_status_sid = "3";
            }else if($ticket_status=="onsite"){
                $case_status_sid = "7";
            }else if($ticket_status=="resolution"){
                $case_status_sid = "5";
            }else if($ticket_status=="resolve"){
                $case_status_sid = "5";
            }else if($ticket_status=="open" || $ticket_status=="assigned"){
                $case_status_sid = "1";
                $in_queue = 2;
            }
        }
        if(isset($data['ticket_sid']) && $data['ticket_sid']>0){
            $sql = "INSERT INTO ticket_alert (ticket_sid,role, case_status_sid, ticket_status, type, type_alert, value, unit, team_support, group_code, created_datetime, value_minute, updated_datetime, in_queue)
            VALUES (:ticket_sid, :role, :case_status_sid, :ticket_status, :type, :type_alert, :value, :unit, :team_support, :group_code, NOW(), :value_minute, NOW(), :in_queue) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':ticket_sid'=>isset($data['ticket_sid'])?$data['ticket_sid']:0,
                ':role'=>isset($data['role'])?$data['role']:"",
                ':case_status_sid'=>$case_status_sid,
                ':ticket_status'=>isset($data['ticket_status'])?$data['ticket_status']:"",
                ':type'=>isset($data['type'])?$data['type']:"",
                ':type_alert'=>isset($data['type_alert'])?$data['type_alert']:"",
                ':value'=>isset($data['value'])?$data['value']:"",
                ':unit'=>isset($data['unit'])?$data['unit']:"",
                ':team_support'=>isset($data['team_support'])?$data['team_support']:"",
                ':group_code'=>isset($data['group_code'])?$data['group_code']:"",
                ':value_minute'=>isset($data['value_minute'])?$data['value_minute']:0,
                ':in_queue'=>$in_queue
            ));
        }
    }
    private function prepareAlertValue($stringAlert, $role){
        $alert = explode(",", $stringAlert);
        $result = array();
        foreach ($alert as $key => $value) {

            $extractAlertValue = $this->extractAlertValue($value, $role);
            array_push($result, $extractAlertValue);
        }
        // print_r($result);
        return $result;
    }

    private function extractAlertValue($string, $role){
        $string = trim($string);
        $string = strtolower($string);

        $result = array();
        $result['role'] = $role;

        if(strpos(" ".$string." ", "mail")){
            $result['type'] = "mail";
        }else if(strpos(" ".$string." ", "sms")){
            $result['type'] = "notification";
        }else if(strpos(" ".$string." ", "phone")){
            $result['type'] = "phone";
        }

        $result['type_alert'] = "";
        if(strpos(" ".$string." ", "before")){
            $result['type_alert'] = "before";
        }else if(strpos(" ".$string." ", "more")){
            $result['type_alert'] = "more";
        }else if(strpos(" ".$string." ", "after")){
            $result['type_alert'] = "after";
        }

        $result['unit'] = "min";
        if(strpos(" ".$string." ", "min")){
            $result['unit'] = "min";
        }else if(strpos(" ".$string." ", "hr")){
            $result['unit'] = "hour";
        }else if(strpos(" ".$string." ", "hour")){
            $result['unit'] = "hour";
        }else if(strpos(" ".$string." ", "day")){
            $result['unit'] = "day";
        }


        $result['value'] = "";
        preg_match('/\d+/', $string, $matches);

        $result['value'] = isset($matches[0])?$matches[0]:"0";

        if($result['unit']=="hour"){
            $result['value_minute'] = $result['value']*60;
        }else if($result['unit']=="day"){
            $result['value_minute'] = $result['value']*60*24;
        }else{
            $result['value_minute'] = $result['value'];
        }

        return $result;
    }

    private function admEndUserGroup($end_user_group){
        $sql = "SELECT * FROM gable_adm WHERE group_code = :end_user_group ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':end_user_group'=>$end_user_group));
        $r = $q->fetchAll();
        return $r;
    }
    private function serviceGroup($group_name){
        $sql = "SELECT * FROM gable_tip_group_manager WHERE group_name = :group_name ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':group_name'=>$group_name));
        $r = $q->fetchAll();
        return $r;
    }

    public function alertRunQueue(){
        require_once __dir__."/SendMail.php";
        require_once __dir__."/../../apis/send_notification/ios/send.php";
        require_once __dir__."/../../apis/send_notification/andriod/send.php";
        $data = $this->listAlertInQueue();
        if(isset($data['before'])){
            foreach ($data['before'] as $k => $v) {
                 $this->startSendAlert($v);
            }
        }

        if(isset($data['after'])){
            foreach ($data['after'] as $k => $v) {
                $this->startSendAlert($v);
            }
        }
        if(isset($data['toSend'])){
            foreach ($data['toSend'] as $k => $v) {
                $this->startSendAlert($v);
            }
        }
    }

    private function startSendAlert($data){
        if(isset($data['type'])){
            if($data['type']=="notification" || $data['type']=="mail"){
                $this->sendAlertViaNotification($data, $data['type']);
            }else{
                $this->sendAlertViaNotification($data, "mail");
            }
        };
    }

    private function messageMailTicket($data, $status){

        $message = "";
        // $data['subject'];
        // $data['end_user'];
        // $data['owner_name'];

        // $data['subject'];
        // $data['end_user'];
        // $data['end_user_site'];
        // $data['refer_remedy_hd'];
        // $data['owner_name'];
        // $data['owner_mobile'];
        // $data['requester_full_name'];
        // $data['requester_phone'];
        // $data['requester_mobile'];
        // $data['requester_email']
        // $data['end_user_contact_name'];
        // $data['end_user_phone'];
        // $data['end_user_mobile'];
        // $data['end_user_email'];
        // $data['urgency'];
        // $data['impact'];
        // $data['create_datetime'];

        $message .= "Subject: ".$data['subject']."<br/><br/>";
        $message .= "Customer company: ".$data['end_user']."<br/>";
        $message .= "Requester name: ".$data['requester_full_name']."<br/>";
        $message .= "Requester phone: ".$data['requester_phone']."<br/>";
        $message .= "Requester mobile: ".$data['requester_mobile']."<br/>";
        $message .= "Requester email: ".$data['requester_email']."<br/><br/>";

        $message .= "Customer contact: ".$data['end_user_contact_name']."<br/>";
        $message .= "Customer phone: ".$data['end_user_phone']."<br/>";
        $message .= "Customer mobile: ".$data['end_user_mobile']."<br/>";
        $message .= "Customer email: ".$data['end_user_email']."<br/><br/>";

        $message .= "Owner: ".$data['owner_name']."<br/>";
        $message .= "Owner mobile: ".$data['owner_mobile']."<br/><br/>";

        // $message .= "Urgency: ".$data['urgency']."<br/>";
        $message .= "Priority: ".ucfirst($this->genPriority($data['urgency'], $data['impact']))."<br/>";
				$message .= "Type: ".$data['case_type']."<br/><br/>";

        $message .= "Status: ".$status."<br/>";
        $message .= "Last worklog: ".$data['last_worklog']."<br/>";
        $message .= "Created date&time: ".$data['create_datetime']."<br/>";

        return $message;

    }
    private function sendAlertViaNotification($data, $typeOfSend){

        $infoTicket = $this->getTicketDetail($data['ticket_sid']);

        $unit['min'] = 'นาที';
        $unit['hour'] = 'ชั่วโมง';
        $unit['day'] = 'วัน';
        $unit['nbd'] = 'วัน';
        // $data['type_alert']

        $ticket_number = isset($infoTicket['refer_remedy_hd'])?$infoTicket['refer_remedy_hd']:$infoTicket['no_ticket'];
        $ticket_subject = $infoTicket['subject'];
        $end_user = $infoTicket['end_user'];
        $owner_name = $infoTicket['owner_name'];
				$end_user_code = $infoTicket['end_user_code'];

        $sla_due_datetime  = isset($data['sla_due_datetime'])?$data['sla_due_datetime']:"";
        $slaInfo = '';
        $slaInfoMail = '';
        if($sla_due_datetime){
            $slaInfo = ", ".$data['ticket_status']." Target ".$sla_due_datetime;
            $slaInfoMail = "<br/>".$data['ticket_status']." Target ".$sla_due_datetime;
        }

        $statusInfo = "";
        $statusInfoMail = "";
        if(isset($data['ticket_status']) && $data['type_alert']==""){
            $statusInfo = ", สถานะ ".$data['ticket_status'];
            $statusInfoMail = "<br/>"."สถานะ ".$data['ticket_status'];
        }

        $message = '';
        $subjectMail = '';
        $sendTo = array();



        $type_alert = '';
        $type_alertMail = '';
        if($data['type_alert']=='before'){
            $type_alert = ', อีก '.$data['value'].' '.$unit[$data['unit']]. ' จะตก SLA '.$data['ticket_status'];
            $type_alertMail = '<br/>'.'อีก '.$data['value'].' '.$unit[$data['unit']]. ' จะตก SLA '.$data['ticket_status'];
        }else if($data['type_alert']=='after' || $data['type_alert']=='more'){
            $type_alert = ', เกินกำหนด SLA '.$data['ticket_status'].' แล้ว ';
            if($data['value']>0){
                $type_alert .= $data['value'].' '.$unit[$data['unit']];
            }
            $type_alertMail = '<br/>'.'เกินกำหนด SLA '.$data['ticket_status'].' แล้ว '.$data['value'].''.$unit[$data['unit']];
        }

        $message = $ticket_number."".$statusInfo."".$type_alert.$slaInfo.", Owner: ".$owner_name.", ".$ticket_subject.", ".$end_user;


        $subjectMail = $end_user_code." : ".$ticket_number." : ".$data['ticket_status'].$type_alert." : ".$ticket_subject;

        $messageMail = $this->messageMailTicket($infoTicket, $data['ticket_status']);

        if($data['role']=="incharge"){
            array_push($sendTo, $infoTicket['owner']);
        }else if($data['role']=='service_mgr'){
            $serviceMgr = $this->getServiceMgr($infoTicket['product_type'], $infoTicket['product_item'], $infoTicket['product_product'], $infoTicket['team']);
            if(isset($serviceMgr['service_mgr']) && $serviceMgr['service_mgr']){
                array_push($sendTo, $serviceMgr['service_mgr']);
            }
        }else if($data['role']=='mgr'){
            $serviceMgr = $this->getServiceMgr($infoTicket['product_type'], $infoTicket['product_item'], $infoTicket['product_product'], $infoTicket['team']);
            if(isset($serviceMgr['mgr']) && $serviceMgr['mgr']){
                array_push($sendTo, $serviceMgr['mgr']);
            }
        }else if($data['role']=='adm'){
            $adm = $this->getAdm($infoTicket['end_user_group'], "ADM");
            foreach ($adm as $key => $value) {
                array_push($sendTo, $value['email']);
            }
        }else if($data['role']=='adm_mgr'){
            $adm = $this->getAdm($infoTicket['end_user_group'], "ADM Mgr.");
            foreach ($adm as $key => $value) {
                array_push($sendTo, $value['email']);
            }
        }else{

        }
        if(count($sendTo)>0){

            $objSendMail = new SendMail();
            $forRole = "";
            foreach ($sendTo as $key => $value) {
                // $this->m->sendNotificationAction($value, $message, 0, $data['ticket_sid']);
                if($typeOfSend=="notification"){
                    $forRole .= "(".$value." ".$data['role'].") ";
                    // if($infoTicket['owner']=='autsakorn.t@firstlogic.co.th'){
                    //     $value = 'autsakorn.t@firstlogic.co.th';
                    // }
                    $this->m->sendNotificationAction($value, $message, 0, $data['ticket_sid']);
                }else if($typeOfSend=="mail"){
                    if(isset($data['type']) && $data['type']=="phone"){
                        $value = "contactcenter@g-able.com";
                        $subjectMail = "โทรแจ้ง ".$owner_name." ".$subjectMail;
                    }
                    // if($infoTicket['owner']=='autsakorn.t@firstlogic.co.th'){
                    //     $value = 'autsakorn.t@firstlogic.co.th';
                    // }
                    $objSendMail->noticeMailTicket($value, $messageMail, $subjectMail, $data['ticket_sid']);
                }
            }

            //DEBUG SENT TO admin
            $messageDebug = $message." [Debug: สำหรับส่งให้ ".$forRole."]";
            $to = 'autsakorn.t@firstlogic.co.th';
            if($typeOfSend=="notification"){
                $this->m->sendNotificationAction($to, $messageDebug, 0, $data['ticket_sid']);
            }
            // $message .= "[Debug To พี่หน่อย: Alert นี้ สำหรับส่งให้ ".$value." Role:".$data['role']."]";
            $to = 'natnicha.d@g-able.com';
            if($typeOfSend=="notification"){
                $this->m->sendNotificationAction($to, $messageDebug, 0, $data['ticket_sid']);
            }
            // else if($typeOfSend=="mail"){
            //     $objSendMail->noticeMailTicket($value, $message, $message, $data['ticket_sid']);
            // }
        }
        $this->updateTicketAlertToSendTime($data['sid']);
    }



    private function updateTicketAlertToSendTime($ticket_alert_sid){
        $sql = "UPDATE ticket_alert set send_datetime = NOW(), in_queue = 0 WHERE sid = :ticket_alert_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_alert_sid'=>$ticket_alert_sid));
    }

    // private function getInfoBeforeSend($data){
    //     $dataTicket = $this->getTicketDetail($data['ticket_sid']);
    //     return array('dataTicket'=>$dataTicket);
    // }

    private function getServiceMgr($type, $item, $product, $team){
        $sql = "SELECT service_mgr, mgr FROM gable_tip_group_manager WHERE type = :type AND item = :item AND product = :product AND group_name = :team ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':type'=>$type,
            ':item'=>$item,
            ':product'=>$product,
						':team'=>$team
            ));
        return $r = $q->fetch();
    }
    private function getAdm($end_user_group, $role){
        $sql = "SELECT role, email FROM gable_adm WHERE group_code = :end_user_group AND role = :role ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':end_user_group'=>$end_user_group,':role'=>$role));
        return $q->fetchAll();
    }
    private function getTicketDetail($ticket_sid){
        $sql = "SELECT T.subject, T.end_user, T.end_user_site, T.no_ticket, T.refer_remedy_hd,
				T.owner, T.create_datetime, T.end_user_group, T.end_user_code, T.case_type,
        CASE WHEN U.email IS NOT NULL THEN U.formal_name ELSE E.thainame END owner_name,
        (SELECT product_categorization_tier_1 FROM test_create_case TCC WHERE TCC.case_id = T.refer_remedy_hd ORDER BY sid DESC LIMIT 0,1 ) product_type,
        (SELECT product_categorization_tier_2 FROM test_create_case TCC WHERE TCC.case_id = T.refer_remedy_hd ORDER BY sid DESC LIMIT 0,1) product_item,
        (SELECT product_categorization_tier_3 FROM test_create_case TCC WHERE TCC.case_id = T.refer_remedy_hd ORDER BY sid DESC LIMIT 0,1) product_product,
				E.mobile owner_mobile,
        T.requester_full_name, T.requester_phone,
        T.requester_mobile, T.requester_email,
        T.end_user_contact_name, T.end_user_phone,
        T.end_user_mobile, T.end_user_email, T.urgency, T.impact,
        T.create_datetime, T.team, T.end_user_code,
        (SELECT worklog FROM ticket_status TS WHERE TS.ticket_sid = T.sid ORDER BY sid DESC LIMIT 0,1 ) last_worklog
        FROM ticket T
        LEFT JOIN user U ON T.owner = U.email OR T.owner = U.email_2
        LEFT JOIN employee E ON E.emailaddr = T.owner
        WHERE T.sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_sid));

        $r = $q->fetch();
        return $r;
    }
    private function listAlertInQueue(){
        $sql = "SELECT TA.*,
        DATE_ADD(NOW(), INTERVAL TA.value_minute minute) duetime, NOW() datenow,
        TS.name sla_name, TS.hd, TS.due_datetime sla_due_datetime
        FROM ticket_alert TA
        INNER JOIN ticket_sla TS ON TA.ticket_sid = TS.ticket_sid
        INNER JOIN ticket T ON T.sid = TS.ticket_sid
        AND TA.case_status_sid = TS.case_status_sid AND TS.status = 'In Process'
        WHERE (TA.send_datetime IS NULL OR TA.send_datetime = '0000-00-00 00:00:00' )
        AND DATE_ADD(NOW(), INTERVAL TA.value_minute minute) > TS.due_datetime
        AND TA.type_alert = 'before' AND TA.in_queue = 1
        AND TA.ticket_sid NOT IN ('13776')
        AND T.status != '4' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $before = $q->fetchAll();


        $sql = "SELECT TA.*,
        DATE_ADD(NOW(), INTERVAL TA.value_minute minute) duetime, NOW() datenow,
        TS.name sla_name, TS.hd, TS.due_datetime sla_due_datetime
        FROM ticket_alert TA
        INNER JOIN ticket_sla TS ON TA.ticket_sid = TS.ticket_sid
        INNER JOIN ticket T ON T.sid = TS.ticket_sid
        AND TA.case_status_sid = TS.case_status_sid AND TS.status = 'In Process'
        WHERE (TA.send_datetime IS NULL OR TA.send_datetime = '0000-00-00 00:00:00' )
        AND DATE_ADD(TS.due_datetime, INTERVAL TA.value_minute minute) < NOW()
        AND (TA.type_alert = 'after' OR TA.type_alert = 'more') AND TA.in_queue = 1
        AND TA.ticket_sid NOT IN ('13776')
        AND T.status != '4' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $after = $q->fetchAll();


        $sql = "SELECT * FROM ticket_alert TA WHERE TA.type_alert = '' AND TA.in_queue = 2 AND TA.ticket_sid NOT IN ('13776') ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $toSend = $q->fetchAll();

        return array('before'=>$before, 'after'=>$after, 'toSend'=>$toSend);
    }
}
?>
