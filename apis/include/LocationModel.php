<?php

class LocationModel{

  private $db;

  private $email,$latitude="",$longitude="",$altitude="",$accuracy="",$altitudeAccuracy="",$heading="",$speed="",$code_version="";
  private $device_platform="",$device_version="";
  private $registration_id = "";

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

   // public function editendser( $pk,$name,$value){
      
   //    $sql = "UPDATE ticket_log SET ".$name." = :value WHERE ticket_sid = :pk ";
   //    $q = $this->conn->prepare($sql);
   //    return $q->execute(array(':value'=>$value,':pk'=>$pk));
   // }

   public function sendLocation( $email,$latitude,$longitude,$altitude,$accuracy,$altitudeAccuracy,$heading,$speed,$code_version,$device_platform,$device_version,$registrationId=""){
      $this->email = $email;
      $this->latitude = $latitude;
      $this->longitude = $longitude;
      $this->altitude = $altitude;
      $this->accuracy = $accuracy;
      $this->altitudeAccuracy = $altitudeAccuracy;
      $this->heading = $heading;
      $this->speed = $speed;
      $this->code_version = $code_version;
      $this->device_version = $device_version;
      $this->device_platform = $device_platform;
      $this->registration_id = $registrationId;

      $this->insertData();
   }

   private function isRepeatDataUserlocation(){
      $sql = "SELECT sid FROM user_location WHERE email = :email ";
      $q = $this->db->prepare($sql);
      $q->execute(array(':email'=>$this->email));

      $r = $q->fetch();
      if(isset($r['sid']) && $r['sid']>0 ){
          return true;
      }
      return false;
   }

   public function selectAllUserLocation($email){
        $sql = "SELECT UR.role_sid,R.can_assign FROM user_role UR LEFT JOIN role R ON UR.role_sid = R.sid WHERE UR.email = :email AND UR.data_status > 0";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        $role_sid = $r['role_sid'];
        $can_assign = $r['can_assign'];

        // $sql = "SELECT E.*,UR.email email,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_employee  
        // FROM user_role UR LEFT JOIN employee E ON UR.email = E.emailaddr AND E.emailaddr <> '' 
        // LEFT JOIN role R ON R.sid = UR.role_sid 
        // WHERE (UR.email = :email OR UR.role_sid IN (".$can_assign.")) AND UR.data_status > 0 ";
        // if($search!=""){
        //     $sql .= " AND (UR.email LIKE '%".$search."%' OR E.thainame LIKE '%".$search."%' ) ";
        // }

        // if(count($notEmail)>0){
        //     foreach ($notEmail as $key => $value) {
        //         $sql .= " AND UR.email <> '".$value."' ";
        //     }
        // }

        // $sql .= " ORDER BY R.sid DESC,E.thainame ";
        // // echo $sql;
        // $q = $this->db->prepare($sql);
        // $q->execute(array(':email'=>$email));
        // $r = $q->fetchAll();

        $sql = "SELECT UL.*,DATE_FORMAT(update_datetime,'%b %D, %H:%i') as datetime,U.sid user_sid, CONCAT('class_u_',U.sid) class_element, U.name user_name 
        FROM user_location UL 
        LEFT JOIN user U ON U.email = UL.email 
        LEFT JOIN role R ON U.role_sid = R.sid 
        WHERE UL.email <> '' AND U.active = '1' AND U.role_sid IN (".$can_assign.") ORDER BY UL.update_datetime ASC ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {
          $this->email = $value['email'];
          $r[$key]['case_user_info'] = $this->caseUserInfo();
        }

        return $r;
   }

   public function caseUserInfo(){

      $sql = "SELECT maxportraitfile pic,thainame, nickname, engname from employee WHERE emailaddr = :emailaddr ";
      $q = $this->db->prepare($sql);
      $q->execute(array(':emailaddr'=>$this->email));
      $r = $q->fetch();

      return $r;
   }
   private function insertData(){
      
      if($this->isRepeatDataUserlocation()){
            $sql = "UPDATE user_location SET 
            latitude = :latitude,
            longitude = :longitude,
            altitude = :altitude,
            accuracy = :accuracy,
            altitude_accuracy = :altitude_accuracy,
            heading = :heading,
            speed = :speed,
            update_datetime = NOW(),
            code_version = :code_version,
            device_platform = :device_platform,
            device_version = :device_version,
            registration_id = :registration_id 
            WHERE email = :email ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
              ':latitude'=> $this->latitude,
              ':longitude'=> $this->longitude,
              ':altitude'=> $this->altitude,
              ':accuracy'=> $this->accuracy,
              ':altitude_accuracy'=> $this->altitudeAccuracy,
              ':heading'=> $this->heading,
              ':speed'=>$this->speed,
              ':code_version'=>$this->code_version,
              ':device_platform'=>$this->device_platform,
              ':device_version'=>$this->device_version,
              ':email'=> $this->email,
              ':registration_id'=>$this->registration_id
              )
            );

            $sql = "UPDATE user SET 
            latitude = :latitude,
            longitude = :longitude,
            location_updated = NOW(),
            updated_datetime = NOW() 
            WHERE email = :email ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':latitude'=>$this->latitude,
                ':longitude'=>$this->longitude,
                ':email'=>$this->email
              ));

      }else{
          $sql = "INSERT INTO user_location (email,latitude,longitude,altitude,accuracy,altitude_accuracy,heading,speed,create_datetime,code_version,device_platform,device_version,update_datetime) VALUES (:email,:latitude,:longitude,:altitude,:accuracy,:altitude_accuracy,:heading,:speed,NOW(),:code_version,:device_platform,:device_version,NOW()) ";

          $q = $this->db->prepare($sql);
          $q->execute(array(
            ':email'=> $this->email,
            ':latitude'=> $this->latitude,
            ':longitude'=> $this->longitude,
            ':altitude'=> $this->altitude,
            ':accuracy'=> $this->accuracy,
            ':altitude_accuracy'=> $this->altitudeAccuracy,
            ':heading'=> $this->heading,
            ':speed'=>$this->speed,
            ':code_version'=>$this->code_version,
            ':device_platform'=>$this->device_platform,
            ':device_version'=>$this->device_version
            )
          );
      }
      $sql = "INSERT INTO user_location_log (email,latitude,longitude,altitude,accuracy,altitude_accuracy,heading,speed,create_datetime,code_version,device_platform,device_version) VALUES (:email,:latitude,:longitude,:altitude,:accuracy,:altitude_accuracy,:heading,:speed,NOW(),:code_version,:device_platform,:device_version) ";

      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':email'=> $this->email,
        ':latitude'=> $this->latitude,
        ':longitude'=> $this->longitude,
        ':altitude'=> $this->altitude,
        ':accuracy'=> $this->accuracy,
        ':altitude_accuracy'=> $this->altitudeAccuracy,
        ':heading'=> $this->heading,
        ':speed'=>$this->speed,
        ':code_version'=>$this->code_version,
        ':device_platform'=>$this->device_platform,
        ':device_version'=>$this->device_version
        )
      );
   }
}
?>