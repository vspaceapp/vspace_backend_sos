<?php
error_reporting(-1);
ini_set('display_errors', 'On');
class SettingTipGroupModel{
	private $db;
  private $m;
  private $email, $token;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
  }
  public function selectData(){
    try{
        $sql = "SELECT * FROM gable_tip_group_manager ORDER BY sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return array('status'=>1, 'message'=>'Data Ok', 'data'=>$r);
    }catch(PDOException $e){
        return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function addData($data){
    try{
      $type = isset($data['type'])?$data['type']:'';
      $item = isset($data['item'])?$data['item']:'';
      $product = isset($data['product'])?$data['product']:'';
      $group_name = isset($data['group_name'])?$data['group_name']:'';
      $service_mgr = isset($data['service_mgr'])?$data['service_mgr']:'';
      $mgr = isset($data['mgr'])?$data['mgr']:'';
      $email = isset($data['email'])?$data['email']:'';

      if($type && $item && $product && $group_name && $service_mgr && $mgr && $email){
        $sql = "INSERT INTO gable_tip_group_manager (type, item, product, group_name, service_mgr, mgr, created_by, created_datetime)
        VALUES (:type, :item, :product, :group_name, :service_mgr, :mgr, :created_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':type'=>$type,
          ':item'=>$item,
          ':product'=>$product,
          ':group_name'=>$group_name,
          ':service_mgr'=>$service_mgr,
          ':mgr'=>$mgr,
          ':created_by'=>$email
        ));
        return array('status'=>1, 'message'=>'Added', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'type, item, product, group_name, service_mgr, mgr ต้องไม่เป็นค่าว่าง', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function editData($data){
    try{
      if(isset($data['sid'])){
        $type = isset($data['type'])?$data['type']:'';
        $item = isset($data['item'])?$data['item']:'';
        $product = isset($data['product'])?$data['product']:'';
        $group_name = isset($data['group_name'])?$data['group_name']:'';
        $service_mgr = isset($data['service_mgr'])?$data['service_mgr']:'';
        $mgr = isset($data['mgr'])?$data['mgr']:'';
        $email = isset($data['email'])?$data['email']:'';

        $sql = "UPDATE gable_tip_group_manager
        SET type = :type, item = :item,
        product = :product, group_name = :group_name, service_mgr = :service_mgr,
        mgr = :mgr, updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(
          array(
            ':type'=>$type,
            ':item'=>$item,
            ':product'=>$product,
            ':group_name'=>$group_name,
            ':service_mgr'=>$service_mgr,
            ':mgr'=>$mgr,
            ':email'=>$email,
            ':sid'=>$data['sid']
          )
        );
        return array('status'=>1, 'message'=>'Updated', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function deleteData($data){
    try{
      if(isset($data['sid'])){
        $email = isset($data['email'])?$data['email']:'';

        $sql = "UPDATE gable_tip_group_manager SET data_status = '-1', updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email,':sid'=>$data['sid']));
        return array('status'=>1, 'message'=>'Deleted', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }

}
