<?php
class EnduserModel{

	private $contract_no;
	private $db;

    private $name = "";
    private $end_user_email = "";
    private $phone = "";
    private $mobile = "";
    private $company = "";
    private $email;
    private $tasks_sid;
    private $m;
    private $ticket_sid;
    private $end_user;

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
    }
    public function listEndUser20161215($contract_no,$email="", $ticket_sid=""){
        $this->contract_no = $contract_no;
       
        $data = $this->getEndUser20161215();

        return $data;
    }
    public function listEndUser($contract_no,$email="", $ticket_sid=""){
    	$res = array();
    	$this->contract_no = $contract_no;
        if($email!=""){
           $this->m->setTable($email);
        }
        $this->ticket_sid= $ticket_sid;

        $this->end_user = $this->getEndUserFromTicketSid();

        $data = $this->listEndUsersTask();
        foreach ($data as $key => $value) {
            if(isset($value['end_user_contact_name_service_report']) && $value['end_user_contact_name_service_report']!="" && isset($value['end_user_email_service_report']) && $value['end_user_email_service_report']!="" ){

                $email = explode("@", $value['end_user_email_service_report']);
                if(count($email)>1){
                    array_push($res, array(
                        'name'=>$value['end_user_contact_name_service_report'],
                        'company'=>$value['end_user_company_name_service_report'],
                        'email'=>$value['end_user_email_service_report'],
                        'mobile'=>$value['end_user_mobile_service_report'],
                        'phone'=>$value['end_user_phone_service_report']
                        )
                    );
                }
            }
        }
        
    	$data = $this->listEndUsersTicket();
    	foreach ($data as $key => $value) {
    		if(isset($value['end_user_contact_name']) && $value['end_user_contact_name']!="" && isset($value['end_user_email']) && $value['end_user_email']!=""){
                $email = explode("@", $value['end_user_email']);
                if(count($email)>1){
    	    		array_push($res, array(
    	    			'name'=>$value['end_user_contact_name'],
    	    			'company'=>$value['end_user_company_name'],
    	    			'email'=>$value['end_user_email'],
    	    			'mobile'=>$value['end_user_mobile'],
    	    			'phone'=>$value['end_user_phone']
    	    			)
    	    		);
                }
	    	}
    	}

    	
    	return $res;
    }
    private function getEndUser20161215(){
        $sql = "SELECT contact_name name, company, email, mobile, phone FROM contact WHERE contact_no = :contract_no GROUP BY email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract_no'=>$this->contract_no));
        $r = $q->fetchAll();

        return $r;
    }

    private function getEndUserFromTicketSid(){
        $sql = "SELECT end_user FROM ".$this->m->table_ticket." WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$this->ticket_sid));
        $r = $q->fetch();
        return $r['end_user'];
    }
    private function listEndUsersTicket(){
    	$sql = "SELECT TI.* FROM ".$this->m->table_ticket." TI LEFT JOIN ".$this->m->table_tasks." TA ON TI.sid = TA.ticket_sid WHERE 
        (TI.contract_no = :contract_no OR TI.end_user = :end_user) AND TI.end_user_contact_name <> '' GROUP BY TI.end_user_email ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':contract_no'=>$this->contract_no,':end_user'=>$this->end_user));
    	$r = $q->fetchAll();

    	return $r;
    }

    private function listEndUsersTask(){

    	$sql = "SELECT TA.* FROM ".$this->m->table_ticket." TI LEFT JOIN ".$this->m->table_tasks." TA ON TI.sid = TA.ticket_sid WHERE (TI.contract_no = :contract_no OR TI.end_user = :end_user) AND TA.end_user_contact_name_service_report <> '' GROUP BY TA.end_user_email_service_report ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':contract_no'=>$this->contract_no,':end_user'=>$this->end_user));
    	$r = $q->fetchAll();

    	return $r;
    }

    public function changeEndUser($email, $tasks_sid, $data){
        if($email!=""){
           $this->m->setTable($email);
        }

        $this->email = $email;
        $this->tasks_sid = $tasks_sid;
        $this->name = $data['name'];
        $this->end_user_email = $data['email'];
        $this->phone = $data['phone'];
        $this->mobile = $data['mobile'];
        $this->company = $data['company'];

        return $this->updateEndUser();
    }

    private function updateEndUser(){
        $sql = "UPDATE ".$this->m->table_tasks." SET 
        end_user_contact_name_service_report = :name, 
        end_user_phone_service_report = :phone,
        end_user_mobile_service_report = :mobile,
        end_user_email_service_report = :email,
        end_user_company_name_service_report = :company,
        updated_by = :update_by,
        updated_datetime = NOW()   
        WHERE sid = :tasks_sid  ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':name'=>$this->name,
            ':phone'=>$this->phone,
            ':mobile'=>$this->mobile,
            ':email'=>$this->end_user_email,
            ':company'=>$this->company,
            ':update_by'=>$this->email,
            ':tasks_sid'=>$this->tasks_sid
            ));
    }
}
?>