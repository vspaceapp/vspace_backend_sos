<?php

class PreventiveMaintenanceModel{

	private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

   
   public function selectSerialTicket( $ticket_sid ){
   		$sql = "SELECT *,TS.sid ticket_serial_sid,(SELECT status FROM task_do_serial_pm TSP WHERE TSP.ticket_serial_sid = TS.sid ORDER BY TSP.sid DESC LIMIT 0,1 ) status  
   			FROM ticket_serial TS 
   			INNER JOIN task_do_serial_pm TDSPM ON TDSPM.ticket_serial_sid = TS.sid 
   			WHERE TS.ticket_sid = :ticket_sid AND (TDSPM.status < 0 || TDSPM.status is null) 
   			AND ((SELECT status FROM task_do_serial_pm TSP WHERE TSP.ticket_serial_sid = TS.sid ORDER BY TSP.sid DESC LIMIT 0,1 ) < 0) GROUP BY TS.sid 
   			ORDER BY TS.sid ASC";
   		$q = $this->conn->prepare($sql);
		$q->execute(array(':ticket_sid'=>$ticket_sid));
		$result = $q->fetchAll();
		
		if( !($result) ){
			return "";
		}
		return $result;
   }

   	public function listCasePM(){
   		$sql = "SELECT T.no_ticket,T.receive_mail_datetime,
			T.*, 
			T.status status,
			(SELECT (select status FROM tasks_log TT WHERE TT.tasks_sid = TASK.sid AND TT.engineer = T.engineer ORDER BY create_datetime DESC LIMIT 0,1) status 
			 				FROM tasks_log T WHERE T.tasks_sid = TASK.sid GROUP BY T.engineer ORDER BY status DESC LIMIT 0,1) last_status,
			TASK.sid tasks_sid, TASK.no_task no_task, 
			(SELECT DATE_FORMAT(appointment,'%d/%m/%Y %H:%i') FROM tasks_log WHERE tasks_sid = TASK.sid ORDER BY sid DESC LIMIT 0,1 ) due_datetime    
			FROM  ticket T 
				LEFT JOIN tasks TASK ON TASK.ticket_sid = T.sid 
			WHERE 1  AND T.case_type = 'Preventive Maintenance'  ";
		$q = $this->conn->prepare($sql);
		$q->execute();
		$r = $q->fetchAll();
		foreach ($r as $key => $value) {
			$r[$key]['last_status_task'] = $this->getStatusTask($value['tasks_sid']);
			$r[$key]['tasks'] = $this->engineerOfTaskDetailOfTicket($value['tasks_sid']);//(1)
			$r[$key]['pdf_report']  = $this->getPDF($value['tasks_sid'], $value['no_task']);
			$r[$key]['customer_signed'] = $this->getCustomerSigned($value['tasks_sid']);
		}
		return $r;
   }

   private function getStatusTask($tasks_sid){
		$sql = "SELECT DISTINCT(T.engineer) engineer, 
		(select status FROM tasks_log TT WHERE TT.tasks_sid = :tasks_sid AND TT.engineer = T.engineer ORDER BY create_datetime DESC LIMIT 0,1) status 
		FROM tasks_log T WHERE T.tasks_sid = :tasks_sid ORDER BY status DESC LIMIT 0,1 ";
		$q = $this->conn->prepare($sql);
		$q->execute(array(':tasks_sid'=>$tasks_sid,':tasks_sid'=>$tasks_sid));
		$r = $q->fetch();
		if($r['status']=='0'){
			return 'Assign';
		}else if($r['status']=='-1'){
			return 'Engineer Rejected';
		}else if($r['status']=='-2'){
			return 'Admin Removed';
		}else if($r['status']=='100'){
			return 'Accepted';
		}else if($r['status']=='200'){
			return 'Start the journey';
		}else if($r['status']=='300'){
			return 'Onsite';
		}else if($r['status']=='400'){
			return 'Closed';
		}else if($r['status']=='500'){
			return 'Send customer sign';
		}else if($r['status']=='600'){
			return 'Finished';
		}
	}

	private function engineerOfTaskDetailOfTicket($tasks_sid, $create_datetime =""){

		$sql = "SELECT TL.engineer,TL.tasks_sid,E.thaifirstname,E.thailastname,E.thainame,E.engfirstname,E.englastname,E.maxportraitfile picture,
				E.mobile,E.emailaddr email,(SELECT status FROM tasks_log WHERE TL.engineer = engineer AND TL.tasks_sid = tasks_sid ORDER BY sid DESC LIMIT 0,1) last_status_task      
				FROM tasks_log TL 
				LEFT JOIN employee E ON E.emailaddr = TL.engineer 
				WHERE TL.tasks_sid = :tasks_sid GROUP BY engineer ORDER BY TL.create_datetime ASC ";
		$q = $this->conn->prepare($sql);
		$q->execute(array(':tasks_sid'=>$tasks_sid));
		$r = $q->fetchAll();
		
		return $r;
	}

	private function getCustomerSigned($task_sid){
		$sql = "SELECT *,DATE_FORMAT(create_datetime,'%d/%m/%Y %H:%i') create_datetime 
		FROM  customer_signature_task WHERE task_sid = :task_sid AND status >= 0";
		$q = $this->conn->prepare($sql);
		$q->execute(array(':task_sid'=>$task_sid));
		$r = $q->fetch();
		if($r['sid']>0){
			return $r;
		}
		return "";
	}

	private function getPDF($task_sid, $no_task){
		$sql = "SELECT file_name FROM customer_signature_task CST WHERE CST.task_sid = :task_sid ORDER BY sid DESC LIMIT 0,1 ";
		$q = $this->conn->prepare($sql);
		$q->execute(array(':task_sid'=>$task_sid));
		$r = $q->fetch();
		if(isset($r['file_name']) && $r['file_name']!=""){
			if(file_exists('/home/flguploa/domains/flgupload.com/public_html/case/pdf/'.$no_task.'.pdf')){
				return "http://case.flgupload.com/pdf/".$no_task.".pdf";
			}
			return "";
		}
		return "";
	}
   public function selectToDoPM( $ticket_sid ){

   		$sql = "SELECT * FROM ticket_to_do_pm WHERE ticket_sid = :ticket_sid ORDER BY sid DESC LIMIT 0,1";
   		$q = $this->conn->prepare($sql);
		$q->execute(array(':ticket_sid'=>$ticket_sid));
		$result = $q->fetch();
		
		if( !($result) ){

			echo "No Result";
		}
		return $result;
   }

	
	public function insertDoSerialPM( $data = array() ){
		$seleclSQL = "SELECT * FROM task_do_serial_pm WHERE task_sid = :task_sid AND ticket_serial_sid = :ticket_serial_sid ";
		$selectQ = $this->conn->prepare($seleclSQL);
		$selectQ->execute(array(':task_sid'=>$data['task_sid'],':ticket_serial_sid'=>$data['ticket_serial_sid']));
		$r = $selectQ->fetchAll();

		if(count($r)>0){
			$sql = "UPDATE task_do_serial_pm SET status = :status, update_datetime = now(), update_by = :email WHERE ticket_serial_sid = :ticket_serial_sid AND task_sid = :task_sid ";
			$q = $this->conn->prepare($sql);
			return $q->execute(array(
					':status'=>$data['status'],
					':email'=>$data['create_by'],
					':ticket_serial_sid'=>$data['ticket_serial_sid'],
					':task_sid'=>$data['task_sid']
				));
		}else{
			$sql = "INSERT INTO task_do_serial_pm 
						(ticket_serial_sid, task_sid, status, create_by, create_datetime) 
						VALUES 
						(:ticket_serial_sid,:task_sid,:status,:create_by, NOW())";
						
			$q = $this->conn->prepare($sql);
			return $q->execute(array(
				':ticket_serial_sid'=>isset($data['ticket_serial_sid'])?$data['ticket_serial_sid']:'',
				':task_sid'=>isset($data['task_sid'])?$data['task_sid']:'',
				':status'=>isset($data['status'])?$data['status']:'',
				':create_by'=>isset($data['create_by'])?$data['create_by']:''
				)
			);
		}
	}	

	public function insertInputAction ( $data = array() ){
		$sql = "INSERT INTO task_input_action
					( task_sid, problem ,  solution,  recommend, create_datetime, create_by, pm_service) 
					VALUES 
					(:task_sid, :problem , :solution, :recommend, NOW() ,:create_by, :pm_service )";
					
		$q = $this->conn->prepare($sql);
		return $q->execute(array(
			':task_sid'=>isset($data['task_sid'])?nl2br($data['task_sid']):'',
			':problem'=>isset($data['problem'])?nl2br($data['problem']):'',
			':solution'=>isset($data['solution'])?nl2br($data['solution']):'',
			':recommend'=>isset($data['recommend'])?nl2br($data['recommend']):'',
			':create_by'=>isset($data['create_by'])?$data['create_by']:'',
			':pm_service'=>isset($data['pm_service'])?nl2br($data['pm_service']):''
			)
		);
	}

	public function selectInformationTask($sr_sid){
		$sql = "SELECT 
		TASK.sid tasks_sid, TASK.no_task no_task, TICKET.no_ticket no_ticket, 
		T.engineer,TICKET.*, 
		DATE_FORMAT((SELECT tl.appointment FROM tasks_log tl WHERE T.engineer = tl.engineer AND T.tasks_sid = tl.tasks_sid ORDER BY tl.sid DESC LIMIT 0,1),'%Y%m%dT%H%i%s') appointment,
		DATE_FORMAT((SELECT tl.appointment FROM tasks_log tl WHERE T.engineer = tl.engineer AND T.tasks_sid = tl.tasks_sid ORDER BY tl.sid DESC LIMIT 0,1),'%Y%m%dT%H%i%s') endtime  
		FROM tasks_log T 
		LEFT JOIN tasks TASK ON TASK.sid = T.tasks_sid 
		LEFT JOIN ticket TICKET ON TICKET.sid = TASK.ticket_sid   
		WHERE T.tasks_sid = :sr_sid 
		AND (SELECT tl.status FROM tasks_log tl WHERE T.engineer = tl.engineer AND T.tasks_sid = tl.tasks_sid ORDER BY tl.sid DESC LIMIT 0,1) >= 0  
		GROUP BY T.engineer ";

		$q = $this->conn->prepare($sql);
		$q->execute(array(':sr_sid'=>$sr_sid));
		$r = $q->fetchAll();
		return $r;
	}
}
?>