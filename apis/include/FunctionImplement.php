<?php
function implementList($email, $project_type=""){
	global $myProject;
	$obj = new ProjectModel();
    $myprojects = $obj->listmyprojects($email, $project_type);
    setInformationMyProject($myprojects);
}

function myProjectInstall($email,$project_type=""){
    global $myProject;
    $obj = new ProjectModel();
    $myprojects = $obj->listmyprojects($email, $project_type);
    setInformationMyProject($myprojects);
}

function setInformationMyProject($myprojects){
    global $myProject;
    $myProject = array();
    
    foreach ($myprojects as $key => $value) {
        $tmp = array();
        $tmp["detail"] = "<div class='row' style=''>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><span class='glyphicon glyphicon-edit'></span> Contract: </strong>".$value['contract']."</div>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><span class='glyphicon glyphicon-edit'></span> Type: </strong>".$value['project_type']."</div>";
        $tmp["detail"] .= "</div>";

        $tmp["detail"] .= "<div class='row' style=''>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><i class='fa fa-fw fa-institution' style='margin-left:-2px;'></i> Customer: </strong>".$value['customer']."</div>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><span class='glyphicon glyphicon-tasks'></span> Project: </strong>".$value['project']."</div>";
        $tmp["detail"] .= "</div>";
        
        $tmp["detail"] .= "<div class='row' style=''>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><span class='glyphicon glyphicon-play'></span> Start: </strong>".$value['project_start']."</div>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><i class='fa fa-fw fa-pause' style='margin-left:-2px;'></i> End: </strong>".$value['project_end']."</div>";
        $tmp["detail"] .= "</div>";
        $tmp["detail"] .= "<div class='row' style=''>";
        $tmp["detail"] .= "<div class='col-md-6'><strong><span class='glyphicon glyphicon-time'></span> Man Hours: </strong> ".(($value['man_days'])?$value['man_days']:"-")." Hours</div>";
        $tmp["detail"] .= "<div class='col-md-6 note-myproject'>คลิกเพื่อ Create Case</div>";
        $tmp["detail"] .= "</div>";

        $tmp["sid"] = $value["sid"];
        $tmp["primary"] = "Contract: ".$value["contract"];
        $tmp["customer"] = $value['customer'];
        $tmp["contract"] = $value["contract"];
        $tmp["man_days"] = $value["man_days"];
        $tmp["man_hours"] = $value["man_hours"];
        $tmp["end_user"] = $value["end_user"];
        $tmp["end_user_address"] = $value["end_user_address"];
        $tmp["project_start"] = $value["project_start"];
        $tmp["project_end"] = $value["project_end"];
        $tmp["project_owner_sid"] = $value['project_owner_sid'];
        $tmp["project_name"] = $value["project"];
        $tmp["subject_type"] = "New Project (Please set type project)";
        $tmp["option"] = '<div class="row"><div class="col-md-6"><div class="dropdownlist_type"></div></div><div class="col-md-6"></div></div>';
        $tmp["option"] .= '<div class="row" style="margin-top:10px;"><div class="col-md-6"><button style="width:100%; webkit-user-select: none;" class="btnSubmit">Click Me For Submit</button></div></div>';
        // $tmp["option"] .= '<div class="dropdownlist_engineer"></div>';
        // $tmp["goto"] = "gotoProjectManagement('".$value['sid']."')";
        if($value['project_type']=="Implement"){
            $tmp["goto"] = "../../pages/implementcreate?project=".$tmp['sid'];
        }else if($value['project_type']=="Install"){
             $tmp["goto"] = "../../pages/installcreate?project=".$tmp['sid'];
        }
        // $tmp["javascriptFunctionOption"] = 'genDropdownProjectType($li);';
        array_push($myProject, $tmp);
    }
}
?>