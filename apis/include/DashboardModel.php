<?php
// require_once 'inc'
class DashboardModel{

	private $db;

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

		private function listTeam(){
			$sql = "SELECT DISTINCT(team) FROM ticket WHERE team != '?' AND team != '' AND team != 'Other' ORDER BY team ASC";
			$q = $this->db->prepare($sql);
			$q->execute();
			$r = $q->fetchAll();
			return $r;
		}
		private function prepareDataCasePerTeam($caseByTeam, $team, $data){
			foreach ($team as $key => $value) {
				// $this->caseDeshboard($data, $value);
				$numberCase = $this->caseDeshboard($data, $value['team']);
				array_push($caseByTeam,
					array('name'=>$value['team'],
						'Incident'=>$numberCase['incident'],
						'Implement'=>$numberCase['implement'],
						'Request'=>$numberCase['request'],
						'Preventive maintenance'=>$numberCase['preventive_maintenance'],
						'Question'=>$numberCase['question']
					)
				);
			}
			return $caseByTeam;
		}
		private function prepareDataMissedSlaPerTeam($caseMissedSla, $team, $data){
			foreach ($team as $key => $value) {
				$numberCase = $this->reportCaseMissedSla($data, $value['team']);
				array_push($caseMissedSla,
					array(
						'name'=>$value['team'],
						'Missed SLA'=>$numberCase['case_missed_sla'],
						'Total Case'=>$numberCase['total_case_incident']
					)
				);
			}
			return $caseMissedSla;
		}

		public function dashboardWeb($data){
				try{
					$team = $this->listTeam();
					$caseByTeam = $this->prepareDataCasePerTeam(array(), $team, $data);
					$caseMissedSla = $this->prepareDataMissedSlaPerTeam(array(), $team, $data);
					return array(
						'status'=>1,
						'data'=>array(
							'team'=>$this->listTeam(),
							'caseByTeam'=>$caseByTeam,
							'caseMissedSla'=>$caseMissedSla
							// 'case'=>array('type'=>$this->caseDeshboard($data))
						),
					);
				}catch(PDOException $e){
					return array('status'=>0,'data'=>array(),'message'=>'Error');
				}
		}
		private function reportCaseMissedSla($data, $team){
			$return = array();

			$return['case_missed_sla'] = $this->caseMissedSla($data, $team);
			$return['total_case_incident'] = $this->selectCasePerMonth(" case_type = 'Incident' ", $data, $team);
			return $return;
		}
		private function caseMissedSla($data, $team){
			$sql = "SELECT COUNT(sid) number_case_missed_sla FROM ticket
			WHERE case_type = 'Incident' AND sla_remedy LIKE '%Mis%' AND DATE_FORMAT(create_datetime,'%Y-%m') = :month ";
			if($team!=""){
				$sql .= " AND team = '".$team."'";
			}
			$q = $this->db->prepare($sql);
			$q->execute(array(':month'=>$data['month']));
			$r = $q->fetch();
			return $r['number_case_missed_sla'];
		}
		private function selectCasePerMonth($type, $data, $team){
			$sql = "SELECT COUNT(sid) numberOfCase FROM ticket
			WHERE ".$type." AND DATE_FORMAT(create_datetime,'%Y-%m') = :month ";
			if($team!=""){
				$sql .= " AND team = '".$team."'";
			}
			$q = $this->db->prepare($sql);
			$q->execute(array(':month'=>$data['month']));
			$r = $q->fetch();
			return $r['numberOfCase'];
		}
		private function caseDeshboard($data, $team=""){
			$return = array();

			$return['incident'] = $this->selectCasePerMonth("case_type = 'Incident' ", $data, $team);
			$return['implement'] = $this->selectCasePerMonth(" (case_type = 'Implement' OR case_type = 'Install') ", $data, $team);
			$return['request'] = $this->selectCasePerMonth("case_type = 'Request' ", $data, $team);
			$return['preventive_maintenance'] = $this->selectCasePerMonth("case_type = 'Preventive Maintenance' ", $data, $team);
			$return['question'] = $this->selectCasePerMonth(" (case_type = 'Inquiries' OR case_type = 'Question') ", $data, $team);
			return $return;
		}
    public function dashboard($role_sid){
    	return $this->meunurole($role_sid);
    }

    public function dashboard2($email, $token){
        $sql = "SELECT DR.*,D.name, D.tag_class FROM dashboard_role DR LEFT JOIN dashboard D ON D.sid = DR.dashboard_sid AND DR.row <> 0
        LEFT JOIN user_role UR ON DR.role_sid = UR.role_sid
        WHERE UR.email = :email
        ORDER BY DR.row ASC, DR.sequence ASC;";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));

        return $q->fetchAll();
    }

    private function meunurole($role_sid){
    	$sql = "SELECT M.* FROM modules MODULES
    			LEFT JOIN menus M ON MODULES.sid = M.modules_sid
    			LEFT JOIN menus_role MR ON MR.menus_sid = M.sid
    			WHERE MR.role_sid = :role_sid
    			";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':role_sid'=>$role_sid));
    	return $q->fetchAll();
    }

    public function module(){
    }
    public function listRole($email, $token){
        $sql = "SELECT * FROM role ORDER BY sid ASC";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $q->fetchAll();
    }

		public function getQuantityTicket(){
			$sql = "SELECT
						SUM(CASE WHEN (hour(create_datetime) = 6 AND severity = 'Severity-1') then 1 else 0 end) as serv1_6,
						SUM(CASE WHEN (hour(create_datetime) = 7 AND severity = 'Severity-1') then 1 else 0 end) as serv1_7,
						SUM(CASE WHEN (hour(create_datetime) = 8 AND severity = 'Severity-1') then 1 else 0 end) as serv1_8,
						SUM(CASE WHEN (hour(create_datetime) = 9 AND severity = 'Severity-1') then 1 else 0 end) as serv1_9,
						SUM(CASE WHEN (hour(create_datetime) = 10 AND severity = 'Severity-1') then 1 else 0 end) as serv1_10,
						SUM(CASE WHEN (hour(create_datetime) = 11 AND severity = 'Severity-1') then 1 else 0 end) as serv1_11,
						SUM(CASE WHEN (hour(create_datetime) = 12 AND severity = 'Severity-1') then 1 else 0 end) as serv1_12,
						SUM(CASE WHEN (hour(create_datetime) = 13 AND severity = 'Severity-1') then 1 else 0 end) as serv1_13,
						SUM(CASE WHEN (hour(create_datetime) = 14 AND severity = 'Severity-1') then 1 else 0 end) as serv1_14,
						SUM(CASE WHEN (hour(create_datetime) = 15 AND severity = 'Severity-1') then 1 else 0 end) as serv1_15,
						SUM(CASE WHEN (hour(create_datetime) = 16 AND severity = 'Severity-1') then 1 else 0 end) as serv1_16,
						SUM(CASE WHEN (hour(create_datetime) = 17 AND severity = 'Severity-1') then 1 else 0 end) as serv1_17,
						SUM(CASE WHEN (hour(create_datetime) = 18 AND severity = 'Severity-1') then 1 else 0 end) as serv1_18,
						SUM(CASE WHEN (hour(create_datetime) = 19 AND severity = 'Severity-1') then 1 else 0 end) as serv1_19,
						SUM(CASE WHEN (hour(create_datetime) = 20 AND severity = 'Severity-1') then 1 else 0 end) as serv1_20,
						SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-1') then 1 else 0 end) as serv1_21,
						SUM(CASE WHEN (hour(create_datetime) = 22 AND severity = 'Severity-1') then 1 else 0 end) as serv1_22,
						SUM(CASE WHEN (hour(create_datetime) = 23 AND severity = 'Severity-1') then 1 else 0 end) as serv1_23,
						SUM(CASE WHEN (hour(create_datetime) = 6 AND severity = 'Severity-2') then 1 else 0 end) as serv2_6,
						SUM(CASE WHEN (hour(create_datetime) = 7 AND severity = 'Severity-2') then 1 else 0 end) as serv2_7,
						SUM(CASE WHEN (hour(create_datetime) = 8 AND severity = 'Severity-2') then 1 else 0 end) as serv2_8,
						SUM(CASE WHEN (hour(create_datetime) = 9 AND severity = 'Severity-2') then 1 else 0 end) as serv2_9,
						SUM(CASE WHEN (hour(create_datetime) = 10 AND severity = 'Severity-2') then 1 else 0 end) as serv2_10,
						SUM(CASE WHEN (hour(create_datetime) = 11 AND severity = 'Severity-2') then 1 else 0 end) as serv2_11,
						SUM(CASE WHEN (hour(create_datetime) = 12 AND severity = 'Severity-2') then 1 else 0 end) as serv2_12,
						SUM(CASE WHEN (hour(create_datetime) = 13 AND severity = 'Severity-2') then 1 else 0 end) as serv2_13,
						SUM(CASE WHEN (hour(create_datetime) = 14 AND severity = 'Severity-2') then 1 else 0 end) as serv2_14,
						SUM(CASE WHEN (hour(create_datetime) = 15 AND severity = 'Severity-2') then 1 else 0 end) as serv2_15,
						SUM(CASE WHEN (hour(create_datetime) = 16 AND severity = 'Severity-2') then 1 else 0 end) as serv2_16,
						SUM(CASE WHEN (hour(create_datetime) = 17 AND severity = 'Severity-2') then 1 else 0 end) as serv2_17,
						SUM(CASE WHEN (hour(create_datetime) = 18 AND severity = 'Severity-2') then 1 else 0 end) as serv2_18,
						SUM(CASE WHEN (hour(create_datetime) = 19 AND severity = 'Severity-2') then 1 else 0 end) as serv2_19,
						SUM(CASE WHEN (hour(create_datetime) = 20 AND severity = 'Severity-2') then 1 else 0 end) as serv2_20,
						SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-2') then 1 else 0 end) as serv2_21,
						SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-2') then 1 else 0 end) as serv2_22,
						SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-2') then 1 else 0 end) as serv2_23,
						SUM(CASE WHEN (hour(create_datetime) = 6 AND severity = 'Severity-3') then 1 else 0 end) as serv3_6,
						SUM(CASE WHEN (hour(create_datetime) = 7 AND severity = 'Severity-3') then 1 else 0 end) as serv3_7,
						SUM(CASE WHEN (hour(create_datetime) = 8 AND severity = 'Severity-3') then 1 else 0 end) as serv3_8,
						SUM(CASE WHEN (hour(create_datetime) = 9 AND severity = 'Severity-3') then 1 else 0 end) as serv3_9,
						SUM(CASE WHEN (hour(create_datetime) = 10 AND severity = 'Severity-3') then 1 else 0 end) as serv3_10,
						SUM(CASE WHEN (hour(create_datetime) = 11 AND severity = 'Severity-3') then 1 else 0 end) as serv3_11,
						SUM(CASE WHEN (hour(create_datetime) = 12 AND severity = 'Severity-3') then 1 else 0 end) as serv3_12,
						SUM(CASE WHEN (hour(create_datetime) = 13 AND severity = 'Severity-3') then 1 else 0 end) as serv3_13,
						SUM(CASE WHEN (hour(create_datetime) = 14 AND severity = 'Severity-3') then 1 else 0 end) as serv3_14,
						SUM(CASE WHEN (hour(create_datetime) = 15 AND severity = 'Severity-3') then 1 else 0 end) as serv3_15,
						SUM(CASE WHEN (hour(create_datetime) = 16 AND severity = 'Severity-3') then 1 else 0 end) as serv3_16,
						SUM(CASE WHEN (hour(create_datetime) = 17 AND severity = 'Severity-3') then 1 else 0 end) as serv3_17,
						SUM(CASE WHEN (hour(create_datetime) = 18 AND severity = 'Severity-3') then 1 else 0 end) as serv3_18,
						SUM(CASE WHEN (hour(create_datetime) = 19 AND severity = 'Severity-3') then 1 else 0 end) as serv3_19,
						SUM(CASE WHEN (hour(create_datetime) = 20 AND severity = 'Severity-3') then 1 else 0 end) as serv3_20,
						SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-3') then 1 else 0 end) as serv3_21,
						SUM(CASE WHEN (hour(create_datetime) = 22 AND severity = 'Severity-3') then 1 else 0 end) as serv3_22,
						SUM(CASE WHEN (hour(create_datetime) = 23 AND severity = 'Severity-3') then 1 else 0 end) as serv3_23,
						SUM(CASE WHEN (hour(create_datetime) = 6  ) then 1 else 0 end) as sum_tk6,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 7 ) then 1 else 0 end) as sum_tk7,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 8 ) then 1 else 0 end) as sum_tk8,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 9 ) then 1 else 0 end) as sum_tk9,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 10 ) then 1 else 0 end) as sum_tk10,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 11 ) then 1 else 0 end) as sum_tk11,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 12 ) then 1 else 0 end) as sum_tk12,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 13 ) then 1 else 0 end) as sum_tk13,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 14 ) then 1 else 0 end) as sum_tk14,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 15 ) then 1 else 0 end) as sum_tk15,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 16 ) then 1 else 0 end) as sum_tk16,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 17 ) then 1 else 0 end) as sum_tk17,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 18 ) then 1 else 0 end) as sum_tk18,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 19 ) then 1 else 0 end) as sum_tk19,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 20 ) then 1 else 0 end) as sum_tk20,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 21 ) then 1 else 0 end) as sum_tk21,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 22 ) then 1 else 0 end) as sum_tk22,
						SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 23 ) then 1 else 0 end) as sum_tk23
						FROM ticket	WHERE date(create_datetime) = CURDATE() AND refer_remedy_hd >= 'INC000001029301'
						AND case_type = 'Incident'
						GROUP BY cast(create_datetime as date)";
						 // = CURDATE()
			$q = $this->db->prepare($sql);
			$q->execute();
			return $q->fetchAll();
		}

			public function getSumStatusTicket(){
						$sql = "SELECT
						SUM(CASE WHEN (`status` = 1 and `owner` = 'Service Center' AND team  = 'EndPoint Service Center') then 1 else 0 end) as Assign,
						SUM(CASE WHEN (`status` = 1 and `owner` <> 'Service Center') then 1 else 0 end) as Assigned,
					  SUM(CASE WHEN (`status` = 2 ) then 1 else 0 end) as In_progress,
						SUM(CASE WHEN (`status` = 4 and `owner` <> 'nanthawat.s.m+sos.vspace.01@gmail.com') then 1 else 0 end) as Pending,
						SUM(CASE WHEN (`status` = 4 and `owner` = 'nanthawat.s.m+sos.vspace.01@gmail.com') then 1 else 0 end) as Request_Info
					 FROM ticket WHERE refer_remedy_hd >= 'INC000001029301' AND case_type = 'Incident'";
					$q = $this->db->prepare($sql);
					$q->execute();
					return $q->fetchAll();
		 }

		 public function getTicketTeam(){
			  $sql = "SELECT team,
				SUM(CASE WHEN (`status` = 1 and `owner` <> 'Service Center') then 1 else 0 end) as Assigned,
				SUM(CASE WHEN (`status` = 2 ) then 1 else 0 end) as In_progress,
				SUM(CASE WHEN (`status` = 4 and `owner` <> 'nanthawat.s.m+sos.vspace.01@gmail.com') then 1 else 0 end) as Pending
				FROM ticket WHERE refer_remedy_hd >= 'INC000001029301' AND team <> 'Incident Coordinator' AND case_type = 'Incident'
				 GROUP BY team";
				$q = $this->db->prepare($sql);
				$q->execute();
				return $q->fetchAll();
		 }

		 public function getTicketTeamSC(){
			  $sql = "SELECT `user`.`name`,
				SUM(CASE WHEN (`status` = 1 and `owner` <> 'Service Center') then 1 else 0 end) as Assigned,
				SUM(CASE WHEN (`status` = 2 ) then 1 else 0 end) as In_progress,
				SUM(CASE WHEN (`status` = 4 and `owner` <> 'nanthawat.s.m+sos.vspace.01@gmail.com') then 1 else 0 end) as Pending,
				SUM(CASE WHEN (`status` = 1 and `owner` <> 'Service Center') then 1 else 0 end)+SUM(CASE WHEN (`status` = 2 ) then 1 else 0 end) +
				SUM(CASE WHEN (`status` = 4 and `owner` <> 'nanthawat.s.m+sos.vspace.01@gmail.com') then 1 else 0 end) AS Total
				FROM ticket INNER JOIN `user` ON ticket.`owner` = `user`.email
				WHERE refer_remedy_hd >= 'INC000001029301' AND team <> 'Incident Coordinator'
				AND team = 'EndPoint Service Center' GROUP BY owner ORDER BY Total DESC";
				$q = $this->db->prepare($sql);
				$q->execute();
				return $q->fetchAll();
		 }

		 public function getAlertSLA(){
			  $sql = "SELECT ticket.refer_remedy_hd,CASE WHEN (ticket_sla.case_status_sid = 5) THEN 'Resolution' ELSE 0 END AS `status`,
								ticket.team,ticket.`owner`,ticket.severity,ticket_sla.due_datetime,
								CASE WHEN(NOW() > ticket_sla.due_datetime) then 1 ELSE 0 END AS overdue
								FROM ticket_sla INNER JOIN ticket ON ticket_sla.ticket_sid = ticket.sid
								WHERE ticket_sla.`status` = 'In Process' AND ticket_sla.case_status_sid <> 0 AND ticket_sla.case_status_sid <> 2 AND
								(( NOW( ) > DATE_SUB( ticket_sla.due_datetime, INTERVAL 5 HOUR ) AND ticket.severity = 'Severity-3' )
								OR ( NOW( ) > DATE_SUB( ticket_sla.due_datetime, INTERVAL 1.5 HOUR ) AND ticket.severity = 'Severity-2' )
								OR ( NOW( ) > DATE_SUB( ticket_sla.due_datetime, INTERVAL 1 HOUR ) AND ticket.severity = 'Severity-1' ))
								AND ticket.refer_remedy_hd >= 'INC000001029301' AND
								ticket.`status` <> 4 AND ticket.`status` <> 5 GROUP BY ticket.refer_remedy_hd ORDER BY ticket_sla.due_datetime ASC";
				$q = $this->db->prepare($sql);
				$q->execute();
				return $q->fetchAll();
		 }

		 public function getSumAlertSLATeam () {
			 $sql = "SELECT	ticket.team,SUM(CASE WHEN ( NOW( ) > ticket_sla.due_datetime ) THEN	1 ELSE 0 END) AS ticket
			 FROM	ticket_sla INNER JOIN ticket ON ticket_sla.ticket_sid = ticket.sid
			 INNER JOIN case_status ON ticket_sla.case_status_sid = case_status.sid WHERE	ticket_sla.`status` = 'In Process'
			 AND ticket_sla.case_status_sid <> 0 AND(( NOW( ) > DATE_SUB( ticket_sla.due_datetime, INTERVAL 5 HOUR )
			 AND ticket.severity = 'Severity-3' ) OR ( NOW( ) > DATE_SUB( ticket_sla.due_datetime, INTERVAL 1.5 HOUR )
			 AND ticket.severity = 'Severity-2' ) OR ( NOW( ) > DATE_SUB( ticket_sla.due_datetime, INTERVAL 1 HOUR )
			 AND ticket.severity = 'Severity-1' )) AND ticket.refer_remedy_hd >= 'INC000001029301' AND ticket.`status` <> 4
			 AND ticket.`status` <> 5 GROUP BY	ticket.team ORDER BY ticket DESC LIMIT 8";
			 $q = $this->db->prepare($sql);
			 $q->execute();
			 return $q->fetchAll();
		 }

		 public function getSeverityTicket(){
			 $sql = "SELECT ticket.refer_remedy_hd,case_status.`name` AS status,ticket.team,ticket.`owner`,
							 ticket.severity,ticket_sla.due_datetime,CASE WHEN(NOW() > ticket_sla.due_datetime) then 1 ELSE 0 END AS overdue
							 FROM ticket_sla INNER JOIN ticket ON ticket_sla.ticket_sid = ticket.sid
							 INNER JOIN case_status ON ticket_sla.case_status_sid = case_status.sid
							 WHERE ticket_sla.`status` = 'In Process' AND ticket_sla.case_status_sid <> 0 AND
							 NOW() > DATE_SUB(ticket_sla.due_datetime, INTERVAL 1 HOUR) AND ticket.refer_remedy_hd >= 'INC000001029301' AND
							 ticket.`status` <> 4 AND ticket.`status` <> 5 AND ticket.severity = 'Severity-1'
							 GROUP BY ticket.refer_remedy_hd ORDER BY ticket_sla.due_datetime ASC";
			 $q = $this->db->prepare($sql);
			 $q->execute();
			 return $q->fetchAll();
		}
}

?>
