<?php

class PreventiveMaintenanceModel{

	private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

   
   public function selectSerialTicket( $ticket_sid ){
   		$sql = "SELECT *,TS.sid ticket_serial_sid 
   			FROM ticket_serial TS 
   			LEFT JOIN task_do_serial_pm TDSPM ON TDSPM.ticket_serial_sid = TS.sid 
   			WHERE TS.ticket_sid = :ticket_sid AND (TDSPM.status < 0 || TDSPM.status is null) 
   			ORDER BY TS.serial_detail ASC";
   		$q = $this->conn->prepare($sql);
		$q->execute(array(':ticket_sid'=>$ticket_sid));
		$result = $q->fetchAll();
		
		if( !($result) ){
			echo "No Result";
		}
		return $result;
   }


   public function selectToDoPM( $ticket_sid ){

   		$sql = "SELECT * FROM ticket_to_do_pm WHERE ticket_sid = :ticket_sid ORDER BY sid DESC LIMIT 0,1";
   		$q = $this->conn->prepare($sql);
		$q->execute(array(':ticket_sid'=>$ticket_sid));
		$result = $q->fetch();
		
		if( !($result) ){

			echo "No Result";
		}
		return $result;
   }

	
	public function insertDoSerialPM( $data = array() ){

		$sql = "INSERT INTO task_do_serial_pm 
					(ticket_serial_sid, task_sid, status, create_by, create_datetime) 
					VALUES 
					(:ticket_serial_sid,:task_sid,:status,:create_by, NOW())";
					
		$q = $this->conn->prepare($sql);
		return $q->execute(array(
			':ticket_serial_sid'=>isset($data['ticket_serial_sid'])?$data['ticket_serial_sid']:'',
			':task_sid'=>isset($data['task_sid'])?$data['task_sid']:'',
			':status'=>isset($data['status'])?$data['status']:'',
			':create_by'=>isset($data['create_by'])?$data['create_by']:''
			)
		);

	}	

	public function insertInputAction ( $data = array() ){
		$sql = "INSERT INTO task_input_action
					( task_sid, problem ,  solution,  recommend, create_datetime, create_by) 
					VALUES 
					(:task_sid, :problem , :solution, :recommend, NOW() ,:create_by )";
					
		$q = $this->conn->prepare($sql);
		return $q->execute(array(
			':task_sid'=>isset($data['task_sid'])?$data['task_sid']:'',
			':problem'=>isset($data['problem'])?$data['problem']:'',
			':solution'=>isset($data['solution'])?$data['solution']:'',
			':recommend'=>isset($data['recommend'])?$data['recommend']:'',
			':create_by'=>isset($data['create_by'])?$data['create_by']:'' 
			)
		);
	}
}
?>