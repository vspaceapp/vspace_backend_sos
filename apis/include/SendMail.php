<?php
require_once __dir__."/db_handler.php";
require_once __dir__."/../../apis/core/constant.php";
require "PHPMailer/PHPMailerAutoload.php";
class SendMail{

private $mode_dev = false;

public function sendMailToDHL($email, $data, $cc_to="",$person, $address, $contactNumber, $partCollectionDate) {

        $subject = 'PARTS RETURN NEEDED on '.$data['return_spare_part_date_format_subject'].' / (Thailand) CASEID/TASKID = "'.$data['oracle_sr'].'"/"'.$data['oracle_task'].'"';
        // $subject = "TEST";
        if($email!="autsakorn.t@firstlogic.co.th"){
            $to = "th.spl@dhl.com,th.Orale@dhl.com";
            // $to = "autsakorn.t@firstlogic.co.th";
            // $cc = $cc_to;
            $cc = "sirirat.c@firstlogic.co.th, nontanan.k@firstlogic.co.th";
            //.$cc_to;
            if($cc_to!=""){
                $cc .= ",".$cc_to;
            }else{
                $cc = $cc_to;
            }
        }else{
            $cc = '';
            $to = "autsakorn.t@firstlogic.co.th";
            if($cc_to!=""){
                $cc .= ",".$cc_to;
            }else{
                $cc = $cc_to;
            }
        }
        $from = $email;
        // 'teerawut.p@firstlogic.co.th'; //"autsakorn.t@firstlogic.co.th";
        // $from = $email;

        $message = '<html>
                <head>
                </head>
                <body style="max-width:500px">
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div><b><u>FSC to arrange DHL collection</u></b></div>
                        <div>
                            <span style="color:#3333FF">(1) Case ID</span> = <span style="color:red">'.$data['oracle_sr'].'</span>
                        </div>
                        <div>
                            <span style="color:#3333FF">(2) Task ID</span> = <span style="color:red">'.$data['oracle_task'].'</span>
                        </div>
                        <div>
                            <span style="color:#3333FF">(3) Requested Part Collection date &amp; time (local country time) (please put X accordingly)</span>
                        </div>
                        <div>
                            (a) [ <span> '.(($partCollectionDate=='')?'/':'').'</span> ] NEXT CALENDAR DAY between 0900hrs - 1700hrs
                         </div>
                        <div>
                            (b) [ <span> '.(($partCollectionDate!='')?'/':'').'</span> ] Specific date & time <br> [Example = 04-JAN-2017 (15:00hrs)] <br>

                        <span style="color:red">'.$partCollectionDate.'</span>
                        </div>
                        <div>
                            <span styl!e="color:#3333FF">(4) Site Contact Person</span>   =  <span style="color:red">
                            '.$person.'.</span>
                        </div>

                        <div>
                            <span style="color:#3333FF">(5) Site Contact Number(s)</span> = <span style="color:red">
                            '.$contactNumber.'</span>
                        </div>

                        <div>
                            <span style="color:#3333FF">(6) Collection Address</span>  = <br>
                            <span style="color:red">
                                '.$address.'
                            </span>
                        </div>
                        <div>
                            <span style="color:#3333FF">(7) Parts Information</span> (Note = please use separate line for each part number)
                        </div>
                        <div class="part_number" part_sid="524"> (a) Part Number = <span style="color:red;">'.$data['pn'].'</span> , Quantity = <span style="color:red;"> 1 </span>
                                , Part Status =  <span style="color:red;"> '.$data['spare_part_return_type'].'</span>
                        </div>
                        <div>
                                <span style="color:#3333FF">(8) Any Special Instructions</span> = <span style="color:red">Please call to contact person before onsite to pick up part.</span>
                        </div>
                    </div>
                </body>
                </html>';


        $headers = "From: $from\n";
        $headers .= 'MIME-Version: 1.0' . "\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
        $headers .= 'Cc: ' . $cc . "\r\n";
        $headers .= 'Bcc: autsakorn.t@firstlogic.co.th,sarinaphat.t@firstlogic.co.th' . "\r\n";
        return $sucess = @mail($to, $subject, $message, $headers);
        // echo 'to : '. $to."<br/>".'from : '.$from."<br/>".'subject : '.$subject."<br/>".'headers : '. $headers."<br/> message : ".$message;
}
public function noticeEngineerCaseAssigned($ticket_sid) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials: true');
            $db_handler = new DbHandler();
            $dataSR = $db_handler->getDataTaskForOpenCase($ticket_sid);
            $to = $dataSR['emailaddr'];
            $subject = ''.$dataSR['no_ticket'].' has been assigned to you.'   ;
            $engineer_name = $dataSR['thainame'];
            $message = '
                <html>
                <head>
                </head>
                <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div>เรียน '.$engineer_name.' </div>
                        <div>'.$dataSR['no_ticket'].' has been assigned to you. </div>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">
                        <div></div>
                        <div>รายละเอียด</div>
                        <div>Task : '.$dataSR['no_ticket'].'</div>
                        <div>Serial No. : '. (($dataSR['serial_no']!="")?$dataSR['serial_no']:"-").'</div>
                        <div>Contract No. : '. $dataSR['contract_no'].'</div>
                        <div>Subject : <b>'.$dataSR['subject'].'</b></div>
                        <div>Description : '. $dataSR['description'].'</div>
                        <div>Type : '. $dataSR['case_type'].'</div>
                        <div>'.(($dataSR['urgency']!="")?('Urgency : '.$dataSR['urgency']):"").'</div>
                        <div>'.(($dataSR['end_user']!="")?('End User Company : '.$dataSR['end_user']):"").'</div>
                        <div>'.(($dataSR['end_user_site']!="")?('Address : '.$dataSR['end_user_site']):"").'</div>
                        <div></div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>
                </html>';
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: vlogic@vspace.in.th <vlogic@vspace.in.th>";
            $headers .= "\n" . 'Cc: ' . $dataSR['create_by'] . "";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,teerawut.p@firstlogic.co.th' . "\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $sucess = @mail($to, $subject, $message, $headers);
}
public function sendToCustomerCaseOpened($ticket_sid) {

            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials: true');

            $db_handler = new DbHandler();
            $dataSR = $db_handler->getDataTaskForOpenCase($ticket_sid);
            $subject = 'Case '.$dataSR['no_ticket'].' has been openned support to you.' ;
            $end_user_name = $dataSR['end_user_contact_name'];
            if ( iconv_substr($end_user_name, 0,3, "UTF-8") == "คุณ"){
                $title_name = $end_user_name;
            }else{
                $title_name =  "คุณ".$end_user_name;
            }
            $engineer_name = $dataSR['thainame'];
            $message = '
                <html>
                <head>
                </head>
                <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div>เรียน '.$title_name.' </div>
                        <div>ขณะนี้ Case No : '.$dataSR['no_ticket'].' อยู่ในระหว่างดำเนินการโดยมีรายละเอียดดังนี้ </div>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">
                        <div>Subject : '.$dataSR['subject'].'</div>
                        <div>Case Type : '. $dataSR['case_type'].'</div>
                        <div>Urgency : '.$dataSR['urgency'].' </div>
                        <div>Company : '. $dataSR['end_user'].'</div>
                        <div>Address : '. $dataSR['end_user_site'].'</div>
                        <div>Responsibility by : '. $engineer_name.' ( '.$dataSR['owner'].' ) </div>
                        <div>Case : '.$dataSR['no_ticket'].'</div>
                        <div>Contract No. : '. $dataSR['contract_no'].'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>
                </html>';
            // echo $message;
            if($this->mode_dev){
                $to = 'autsakorn.t@firstlogic.co.th';
            }else{
                $to = $dataSR['end_user_email'];
            }
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: vlogic@vspace.in.th <vlogic@vspace.in.th>";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,teerawut.p@firstlogic.co.th' . "\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $sucess = @mail($to, $subject, $message, $headers);
}
public function sendToEngineerProjectAssigned($project_sid, $dataSR=array()) {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credentials: true');
        if(isset($dataSR['list_owner'])){
            foreach ( $dataSR['list_owner'] as $key => $value) {
                $owner_mail =  $value['owner'];
                $mandays = $value['man_days'];
                $manHours = $value['man_hours'];
                $thainame = $value['thainame'];
                if ( iconv_substr($thainame, 0,3, "UTF-8") == "คุณ"){
                    $title_name = $thainame;
                }else{
                    $title_name =  "คุณ".$thainame;
                }
                $mail = new PHPMailer;
                $mail->setFrom('vSpace', 'vlogic@vspace.in.th');
                $mail->addAddress($owner_mail);     // Add a recipient
                $mail->addReplyTo($dataSR['create_by'], $dataSR['create_by']);
                $mail->addCC( $dataSR['create_by']);
                $mail->addBCC('autsakorn.t@firstlogic.co.th');
                $mail->addBCC('teerawut.p@firstlogic.co.th');
                $mail->isHTML(true);                                  // Set email format to HTML
                $message = '
                <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div>เรียน '.$title_name.' </div>
                        <div>คุณได้รับมอบหมายโครงการ </div>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">
                        <div>Project name : '.$dataSR['name'].'</div>
                        <div>Contract No. : '. $dataSR['contract'].'</div>
                        <div>Project type : '. $dataSR['project_type'].'</div>
                        <div>Company : '. $dataSR['end_user'].'</div>
                        <div>Address : '. $dataSR['end_user_address'].'</div>
                        <div>Start Project: '.$value['project_start'].'</div>
                        <div>End Project: '.$value['project_end'].'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>';
                $mail->Subject = 'Project '.$dataSR['contract'].' has been openned to you.';
                $mail->Body    = $message;
                // echo $message;
                // exit();
               if(!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    echo 'Message has been sent';
                }
            }
        }
    }
    //[new]
public function sendEmailAfterClosedToCustomer($tasksid, $email) {
        $db_handler = new DbHandler();
        $dataSR = $db_handler->getDataTaskForPDF($tasksid, $email);
        $string_associate_name = '';
        $string_associate_email = '';
        foreach ($dataSR['associate'] as $key => $value) {
            if($value['thainame']!="" && $value['engineer']!=""){
                if($string_associate_name == ''){
                    $string_associate_name = ', '.$value['thainame'];
                }else{
                    $string_associate_name .= ', '. $value['thainame'];
                }
                if($string_associate_email == ''){
                    $string_associate_email = $value['engineer'];
                }else{
                    $string_associate_email .= ','. $value['engineer'];
                }
            }
        }
        if($dataSR['end_user_contact_name_service_report']!="" && $dataSR['end_user_email_service_report']){
            $customer_firstname = $dataSR['end_user_contact_name_service_report'];
            $customer_email = $dataSR['end_user_email_service_report'];
        }else{
            $customer_firstname = $dataSR['ticket_end_user_contact_name'];
            $customer_email = $dataSR['ticket_end_user_email'];
        }

        if($dataSR['subject_service_report']!=""){
            $symtom = $dataSR['subject_service_report'];
        }else{
            $symtom = $dataSR['subject'];
        }

        $to = $customer_email.','.$dataSR['engineer_email'];
        if($string_associate_email!=""){
            $to .= ",".$string_associate_email;
        }
        $subject = 'Please sign e-Service Report ' .  $dataSR['no_task'];

        if ( iconv_substr( $customer_firstname, 0,3, "UTF-8") == "คุณ"){
            $title_name =  $customer_firstname;
        }else{
            $title_name =  "คุณ". $customer_firstname;
        }

        $requestNumber = $dataSR['no_ticket'];

        $engineer_name = $dataSR['thainame'];
        $message = '
                <html>
                <head>
                </head>

                <body>
                    <div style="margin:14pt 0;">
                        <font face="Times New Roman,serif" size="3">
                            <span lang="th" style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px;">
                                    เรียน '.$title_name.' <br/>
                            </span>
                        </font>

                        <font face="Times New Roman,serif" size="3">
                            <span lang="th" style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px;">
                                    <br />
                                    '.$dataSR['comptname'].' ขอขอบคุณ ที่ท่านไว้วางใจ ใช้บริการของเรา <br/>
                                    กรุณาตรวจรายละเอียด ในการทำงานของ เอกสาร Service Report <br/>
                                    หมายเลข : '. $dataSR['no_task'] .' <br/>
                                    ที่ได้ดำเนินการ: '.$symtom.' <br/>
                                    โดย '.$engineer_name .$string_associate_name. ' <br/>

                                    <a title="Click to check and sign" href="'.HOST_NAME.'/pages/signaturepad/customer/?task='.$tasksid.'&engineer='.$email.'" target="_blank" style="text-decoration: initial;">
                                        <br />
                                        <span style="font-family: browallia new; font-size: 18px; font-weight: bold; font-style: italic; color: #ffa900;">กรุณาลงลายมือชื่อที่นี่<br />(Please Check and Sign Here) Click!!!</span>
                                        <br />

                                        <font face="Times New Roman,serif" size="3">
                                            <span lang="th" style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px; color: crimson;">
                                                    Compatible with Google Chrome only
                                            </span>
                                        </font>
                                    </a><br/><br />

                                    Service Report ใบนี้เป็นเอกสารประกอบการให้บริการ ในเคสหมายเลข '. $requestNumber .' <br/><br/>

                                    หากท่านมีข้อสงสัยประการใด กรุณาติดต่อ Engineer ที่ดำเนินการ ได้ทันที <br/><br/>

                                    ขอแสดงความนับถือ<br/>
                                    '.$dataSR['comptname'].'<br/><br/>
                            </span>
                        </font>

                        <font face="Times New Roman,serif" size="3">
                            <span style="font-size:12px;">
                                    ลายมือชื่อ ที่ได้เขียนลงในเอกสารนี้ ไม่ได้ถูกเก็บบันทึกลงในระบบ
                                    และใช้ประกอบลงในเอกสาร Service Report ฉบับนี้ เท่านั้น
                            </span>
                        </font>

                    </div>
                </body>
                </html>
            ';
        $headers = "From:Service Report System <eservicereport@vspace.in.th>\nReply-To: ".$engineer_name." <".$dataSR['engineer_email'].">";
        if($dataSR['prefix_table']=="mvg_" && 1==2){
            $to = "mas.autsakorn@gmail.com";
        }else{
            // echo $sucess = @mail($to, $subject, $message, $headers);
            $headers .= "\n" . 'Cc: ' . $email . "";
        }
        // if($dataSR['prefix_table']=="mvg_" || $dataSR['prefix_table']=="tcs_"){
        $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th' . "\n";
        // }
        $headers .= 'MIME-Version: 1.0' . "\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
        return @mail($to, $subject, $message, $headers);
}
public function sendToUserRegisterWithOtpOld($data) {
                header('Access-Control-Allow-Origin: *');
                header('Access-Control-Allow-Credentials: true');
                if ( iconv_substr($data['name'], 0,3, "UTF-8") == "คุณ"){
                    $title_name = $data['name'];
                }else{
                    $title_name =  "".$data['name'];
                }
                $mail = new PHPMailer;
                $mail->setFrom("eservicereport@flgupload.com", "eservicereport@flgupload.com");
                $mail->addAddress($data['email']);     // Add a recipient
                $mail->addReplyTo("eservicereport@flgupload.com", "vspace.in.th");
                $mail->addBCC('autsakorn.t@firstlogic.co.th');
                $mail->addBCC('teerawut.p@firstlogic.co.th');
                $mail->addBCC($data['email']);
                $mail->isHTML(true);                                  // Set email format to HTML
                $message = '
                <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div>เรียน '.$title_name.' </div>
                        <div>OTP Registration </div>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">

                        <div>OTP Registration = '.$data['otp'].'</div>
                        <div>Detail</div>
                        <div>Name : '.$data['name'].'</div>
                        <div>Email. : '. $data['email'].'</div>
                        <div>mobile. : '. $data['mobile'].'</div>
                        <div>OTP. : '. $data['otp'].'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>';
                $mail->Subject = 'No Reply: OTP Registration';
                $mail->Body    = $message;
                // echo $message;
                // exit();
                if(!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    echo 'Message has been sent';
                }
}
public function sendToUserRegisterWithOtp($data) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials: true');
           	if ( iconv_substr($data['name'], 0,3, "UTF-8") == "คุณ"){
                $title_name = $data['name'];
            }else{
                $title_name =  "".$data['name'];
            }
            $subject = 'No Reply: OTP Registration';
            $message = '
               <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div>เรียน '.$title_name.' </div>
                        <div>OTP Registration </div>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">

                        <div>OTP Registration = '.$data['otp'].'</div>
                        <div>Detail</div>
                        <div>Name : '.$data['name'].'</div>
                        <div>Email. : '. $data['email'].'</div>
                        <div>mobile. : '. $data['mobile'].'</div>
                        <div>OTP. : '. $data['otp'].'</div>

                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>';
            //print_r($message);
            // exit();
            if($this->mode_dev){
                $to = 'autsakorn.t@firstlogic.co.th';
            }else{
                $to = $data['email'];
            }
            //From:Service Report System <eservicereport@flgupload.com>\nReply-To: Teerawut Phonghan <teerawut.p@firstlogic.co.th>
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: vlogic@vspace.in.th <vlogic@vspace.in.th>";

            // $headers .= "\n" . 'Cc: ' . $dataSR['engineer_email'] . "\n";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,teerawut.p@firstlogic.co.th' . "\n";
            // $headers .= "\n" . 'Bcc: ' . 'teerawut.p@firstlogic.co.th' . "\n";

            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";

            $sucess = @mail($to, $subject, $message, $headers);

}
public function sendToUserForgotPasswordWithOtp($data) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials: true');
            if ( iconv_substr($data['name'], 0,3, "UTF-8") == "คุณ"){
                $title_name = $data['name'];
            }else{
                $title_name =  "".$data['name'];
            }
            $subject = 'No Reply: OTP Forgot Password';
            $message = '
               <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left;" >
                        <div>เรียน '.$title_name.' </div>
                        <div>OTP Registration </div>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">

                        <div>OTP Registration = '.$data['otp'].'</div>
                        <div>Detail</div>
                        <div>Name : '.$data['name'].'</div>
                        <div>Email. : '. $data['email'].'</div>
                        <div>mobile. : '. $data['mobile'].'</div>
                        <div>OTP. : '. $data['otp'].'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>';
            // echo $message;
            // exit();
            if($this->mode_dev){
                $to = 'autsakorn.t@firstlogic.co.th';
            }else{
                $to = $data['email'];
            }
            //From:Service Report System <eservicereport@flgupload.com>\nReply-To: Teerawut Phonghan <teerawut.p@firstlogic.co.th>
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: vlogic@vspace.in.th <vlogic@vspace.in.th>";
            // $headers .= "\n" . 'Cc: ' . $dataSR['engineer_email'] . "\n";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,teerawut.p@firstlogic.co.th' . "\n";
            // $headers .= "\n" . 'Bcc: ' . 'teerawut.p@firstlogic.co.th' . "\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $sucess = @mail($to, $subject, $message, $headers);
}
public function inviteMember($email, $invite_email, $message){
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials: true');
            $name = explode('.', $email);
            $invite_message = $message;
            $subject = '[vSpace] '. $email.' invited you to join vspace';
            $message = '
                <head>
                </head>
                <body>
                    <div style="background-color:rgb(0,188,212); text-align:center; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:center;" >
                        <div> '.$email.' invites you to join vSpace </div>
                        <div>Greeting: '.$invite_message.'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:center; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>';
            // echo $message;
            // exit();
            if($this->mode_dev){
                $to = 'autsakorn.t@firstlogic.co.th';
            }else{
                $to = $invite_email;
            }
            //From:Service Report System <eservicereport@flgupload.com>\nReply-To: Teerawut Phonghan <teerawut.p@firstlogic.co.th>
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: ".$email." <".$email.">";
            // $headers .= "\n" . 'Cc: ' . $dataSR['engineer_email'] . "\n";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,teerawut.p@firstlogic.co.th' . "\n";
            // $headers .= "\n" . 'Bcc: ' . 'teerawut.p@firstlogic.co.th' . "\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $sucess = @mail($to, $subject, $message, $headers);
}
public function notificationMember($email, $invite_email, $message){
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials: true');
            $name = explode('.', $email);
            $invite_message = $message;
            $subject = '[vSpace] มีการแจ้งเตือนใหม่เกี่ยวกับคุณบน vSpace';
            $message = '
                <head>
                </head>
                <body>
                    <div style="background-color:rgb(0,188,212); text-align:center; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:center;" >
                        <div> '.$email.' assign you in vSpace </div>
                        <div>Notification: '.$invite_message.'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:center; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>';
            // echo $message;
            // exit();
            if($this->mode_dev){
                $to = 'autsakorn.t@firstlogic.co.th';
            }else{
                $to = $invite_email;
            }
            //From:Service Report System <eservicereport@flgupload.com>\nReply-To: Teerawut Phonghan <teerawut.p@firstlogic.co.th>
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: ".$email." <".$email.">";
            // $headers .= "\n" . 'Cc: ' . $dataSR['engineer_email'] . "\n";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,teerawut.p@firstlogic.co.th' . "\n";
            // $headers .= "\n" . 'Bcc: ' . 'teerawut.p@firstlogic.co.th' . "\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $sucess = @mail($to, $subject, $message, $headers);
}
public function noticeMailTicket($to, $message, $subject, $ticket_sid) {
            $message = '
                <html>
                <head>
                </head>
                <body style="max-width:500px">
                    <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                        <h1 style="color:#FFFFFF">
                         vSpace
                        </h1>
                    </div>
                    <div style="color:#222222; background-color:#f1f1f1; padding:20px 10px; text-align:left; ">
                        <div></div>
                        <div></div>
                        <div>'.$message.'</div>
                    </div>
                    <div style="color:#222222; padding:20px 10px; text-align:left; background-color: #FAFBFC;" >
                        <div>
                            Best Regards<br/>
                            '.HOST_NAME.'
                        </div>
                    </div>
                </body>
                </html>';
            // echo $message;
            // exit();
            //From:Service Report System <eservicereport@flgupload.com>\nReply-To: Teerawut Phonghan <teerawut.p@firstlogic.co.th>
            $headers = "From:vSpace<vlogic@vspace.in.th>\nReply-To: vlogic@vspace.in.th <vlogic@vspace.in.th>";
            // $headers .= "\n" . 'Cc: ' . $dataSR['create_by'] . "";
            $headers .= "\n" . 'Bcc: ' . 'autsakorn.t@firstlogic.co.th,natnicha.d@g-able.com' . "\n";
            // $headers .= "\n" . 'Bcc: ' . 'teerawut.p@firstlogic.co.th' . "\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $sucess = @mail($to, $subject, $message, $headers);
}
public function sendEmailAfterSigned($tasksid, $email="") {
                $db_handler = new DbHandler();
                $dataSR = $db_handler->getDataTaskForPDF($tasksid,$email);
                $string_associate_name = '';
                $string_associate_email = '';
                foreach ($dataSR['associate'] as $key => $value) {
                    if($value['thainame']!="" && $value['engineer']!=""){
                        if($string_associate_name == ''){
                            $string_associate_name = ', '.$value['thainame'];
                        }else{
                            $string_associate_name .= ', '. $value['thainame'];
                        }
                        if($string_associate_email == ''){
                            $string_associate_email = $value['engineer'];
                        }else{
                            $string_associate_email .= ','. $value['engineer'];
                        }
                    }
                }
                $sr = $dataSR['no_task'];
                $engineerEmail = $from = $dataSR['engineer_email'];
                if($dataSR['end_user_contact_name_service_report']!="" && $dataSR['end_user_email_service_report']){
                    $contact_person_first_name = $dataSR['end_user_contact_name_service_report'];
                    $customer_email = $dataSR['end_user_email_service_report'];
                }else{
                    $contact_person_first_name = $dataSR['ticket_end_user_contact_name'];
                    $customer_email = $dataSR['ticket_end_user_email'];
                }
                $customer_firstname = $contact_person_first_name;
                if ( iconv_substr($customer_firstname, 0,3, "UTF-8") == "คุณ"){
                    $title_name = $customer_firstname;
                }else{
                    $title_name =  "คุณ".$customer_firstname;
                }
                $to = $customer_email.','.$engineerEmail;
                if($string_associate_email!=""){
                    $to .= ",".$string_associate_email;
                }
                $subject = 'e-Service Report ' . $sr;
                // $customer_lastname = $this->contact_person_last_name;
                //     if ($customer_lastname == ''){
                //         $fullname = $title_name;
                //     }else{
                //         $fullname = $title_name.' '.$customer_lastname;
                //     }
                $fullname = $title_name;
                $requestNumber = $dataSR['no_ticket'];
                $date = $dataSR['appointment'];//;$this->appointment_time;
                if($date == ''){
                    $string_date = '';
                }else{
                    $string_date = ''.$date;
                }
                $engineer_name = $dataSR['thainame'];
                // $associate_name = "";
                // if($associate_name == ''){
                //     $string_associate_name = '';
                // }else{
                //     $string_associate_name = ', '.$associate_name;
                // }
                if($dataSR['subject_service_report']!=""){
                    $symtom = $dataSR['subject_service_report'];
                }else{
                    $symtom = $dataSR['subject'];
                }
                // preparing attachments
                $explodeFileName = "";
                if(isset($dataSR['file_name_signated']) && $dataSR['file_name_signated']!=""){
                    $explodeFileName = explode(".",$dataSR['file_name_signated']);
                }
                $filename = $explodeFileName[0].".pdf";
                $filePath = __dir__."/../../pdf/".$filename;
                if(!file_exists($filePath)){
                  echo "Not has PDF file";
                  exit();
                }
                $genMD5 = md5_file($filePath);
                $apologizeMessage = '
                            <font face="Times New Roman,serif" size="3">
                                <span lang="th" style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px; color: crimson;">
                                    ต้องขออภัยในความผิดพลาด ระบบได้ทำการส่งข้อมูลที่ถูกต้องมาให้ใหม่ที่เมลนี้ครับ <br/><br/>
                                </span>
                            </font>
                ';
                // <font face="Times New Roman,serif" size="3">
                //                 <span lang="th" style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px;">
                //                          เรียน '.$fullname.' <br/><br/>
                //                 </span>
                //             </font>

                $message = '
                    <html>
                    <head>
                    </head>
                    <body>
                        <div style="margin:14pt 0;">
                            <font face="Times New Roman,serif" size="3">
                                <span lang="th" style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px;">
                                         '.$dataSR['comptname'].' <br/>
                                         ขอส่งรายละเอียดการทำงานตามเอกสารใบ Service Report หมายเลข '.$sr.'<br/><br/>
                                         ในการให้บริการเมื่อวันที่ '. $string_date .'<br/>
                                         เพื่อดำเนินการ: '.$symtom.' <br/>
                                         โดย '.$engineer_name .''.$string_associate_name. ' <br/>
                                         <br/>ขอขอบคุณ ที่ไว้วางใจ ใช้บริการของเรา<br/><br/>
                                         เอกสาร Service Report หมายเลข '. $sr .' <br/>
                                         มี MD5 Signature คือ
                                </span>
                            </font>
                            <font face="Times New Roman,serif" size="3">
                                <span style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px; color:crimson;">
                                    '.$genMD5.'<br/>
                                </span>
                            </font>
                            <font face="Times New Roman,serif" size="3">
                                <span style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:13px;">
                                    สามารถตรวจสอบ MD5 Signature ได้โดยใช้ Tool MD5 ทั่วไป หรือทาง online ได้ที่
                                    <a href="http://onlinemd5.com">http://onlinemd5.com </a>
                                    <br/><br/>
                                    ขอแสดงความนับถือ<br/>
                                    '.$dataSR['comptname'].'<br/><br/>
                                     </span>
                                </span>
                            </font>
                            <font face="Times New Roman,serif" size="3">
                                <span style="font-family:Tahoma,Thonburi,Arial,Helvetica; font-size:12px;">
                                    หมายเหตุ:<br/>
                                    MD5 มีไว้ตรวจสอบความถูกต้องของไฟล์ เมื่อมีไฟล์จำนวน สองไฟล์
                                    ถ้าเนื้อหาในไฟล์เหมือนกันทุกประการ ก็จะได้ค่า MD5 เหมือนกัน แต่หากว่า ค่า MD5 ไม่ตรงกัน นั้นแสดงว่าต้องมีไฟล์ใดๆไฟล์หนึ่งที่ไม่สมบูรณ์
                                </span>
                            </font>

                        </div>
                    </body>
                    </html>
                ';
                // หากท่านมีข้อสงสัยประการใด สามารถติดต่อได้ที่ Call Center<br/>
                //                     หมายเลขโทรศัพท์ 02-685-9155 <br/>
                //                     อ้างอิง หมายเลขเคส '.$requestNumber.'<br/>
                $headers = "From:Service Report System <eservicereport@vspace.in.th>\nReply-To: ".$engineer_name." <"
                            .$engineerEmail.">\n";
        //        $headers = "From: $from\nReply-To: $form";
                // if (trim($this->additive_mail) != '') {
                if($dataSR['prefix_table']=="mvg_" && 1==2){
                    $to = "mas.autsakorn@gmail.com";
                }else{
                    $headers .= 'Cc: '.$engineerEmail.''."\r\n";
                }
                // if($dataSR['prefix_table']=="mvg_" || $dataSR['prefix_table']=="tcs_"){
                    $headers .= 'Bcc:autsakorn.t@firstlogic.co.th'."\r\n";
                // }
                // } else {
                    // $headers .= 'Cc: ' .str_replace (';', ',', $this->engineerEmail()) . ', ' . $this->getAssociateEngineerEmails() . ', ' . implode(', ', $this->getSupervisorMails());
                // }
                // $headers .= "\n";
                $semi_rand = md5(time());
                $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
                // headers for attachment
                $headers .= "MIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
                // multipart boundary
                $message = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"utf-8\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
                $message .= "--{$mime_boundary}\n";
                $file = fopen($filePath, "rb");
                $data = fread($file, filesize($filePath));
                fclose($file);
                $data = chunk_split(base64_encode($data));
                $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$filename\"\n" .
                "Content-Disposition: attachment;\n" . " filename=\"$filename\"\n" .
                "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
                $message .= "--{$mime_boundary}\n";
                // if (!YII_TEST) {
                    $sucess = @mail($to, $subject, $message, $headers);
                // } else {
                //     $sucess = true;
                // }
  }

}
