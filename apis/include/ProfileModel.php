<?php

class ProfileModel{

	private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

   
   public function changePassword( $email, $old_password, $new_password ){

   		$md5oldPassword = md5($old_password);
   		$md5newPassword = md5($new_password);

   		$sql = "SELECT password FROM user WHERE email = :email ";
   		$query = $this->conn->prepare($sql);
   		$query->execute(array(':email' => $email ));
   		$result = $query->fetch();
   		

   		if($md5oldPassword == $result['password']){
   			
   			$sqlUpdatePassword = " UPDATE user 
   								   SET password = :new_password , updated_datetime = NOW()
								   WHERE email = :email ";
			$q = $this->conn->prepare($sqlUpdatePassword);
			return $q->execute(array(':new_password'=> $md5newPassword, ':email' => $email ));


   		}else{
   			return false;
   		}
	
   }


}
?>