<?php
class FCM {

	private $token = "";

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function addToken($token){
    	$this->setToken($token);
    	$this->insertToken();
    }

    private function insertToken(){
    	$sql = "INSERT INTO fcm (token) VALUES (:token)";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':token'=>$this->getToken()));

    }

    private function setToken($token){
    	$this->token = $token;
    }

    public function getToken(){
    	return $this->token;
    }
}