<?php class OrganizationModel {
	private $email;
	private $db;
	public $permission_role;
	private $rootSupervisor;
	private $objCons;
	private $oragnize;
	private $res;
	public function __construct($email){
		require_once dirname(__FILE__) . '/db_connect.php';
		$this->email = $email;
		$this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
		
		if($this->email!=""){
            $this->permission_role = $this->objCons->permissionRoleByEmail($this->email); 
        }
	}

	public function myOrganization(){
		$this->oragnize = array();
		$this->res = array();

		$this->rootSupervisor = $this->permission_role['root_supervisor_role_sid'];
		$data = $this->getUnderMyStructure();
		
		array_push($this->oragnize, $data);
		$this->res = $this->oragnize;

		$this->genArrayOrganization($data);
		
		return array('array'=>$this->oragnize,'data'=>$this->getUnderMyStructure(),'root'=>$this->rootSupervisor,'res'=>$this->res);
	}

	public function genArrayOrganization($data){
		$is_a_supervisor_role_sid = $data['is_a_supervisor_role_sid'];
		if($is_a_supervisor_role_sid){
			
			foreach ($data['is_a_supervisor_role_sid_array'] as $key => $value) {
				$this->rootSupervisor = $value;
				$data = $this->getUnderMyStructure();
				$tmp = array();
				array_push($this->res, $data);

				// $this->oragnize[count($this->oragnize)-1]['under_'.count($this->res)] = array();
				// for($j=0;$j<count($this->res);$j++){
					// array_push($this->oragnize[count($this->oragnize)-1]['under_'.count($this->res)], $data);					
				// }

				$this->genArrayOrganization($data);
			}
		}else{
			return;
		}
		// return $array;
	}

	public function getRootSupervisorRoleSid(){

	}

	public function getOverMyStructure(){
		
	}

	public function getUnderMyStructure(){
		$sql = "SELECT *,sid role_sid FROM role R WHERE R.sid = :sid ";
		$q = $this->db->prepare($sql);
		$q->execute(array(':sid'=>$this->rootSupervisor));
		$r = $q->fetch();
		$r['is_a_supervisor_role_sid_array'] = explode(",", $r['is_a_supervisor_role_sid']);

		$sql = "SELECT * FROM user U WHERE U.role_sid = :role_sid";
		$q = $this->db->prepare($sql);
		$q->execute(array(':role_sid'=>$r['sid']));
		$r['staff'] = $q->fetchAll();

		return $r;
	}	
}