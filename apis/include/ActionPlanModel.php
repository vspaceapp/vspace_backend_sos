<?php

class ActionPlanModel{

	private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

   
   public function selectActionPlanSubject( $action_plan_sid = ""){
   		

   		$sql = "SELECT * FROM action_plan_subject WHERE 1 "; 
   		if($action_plan_sid!=""){
	   		$sql .= "AND sid = '".$action_plan_sid."' ";
	   	}
   		$sql .= "ORDER BY sid DESC ";
   		$q = $this->conn->prepare($sql);
		$q->execute();
		$result = $q->fetchAll();
		
		if( !($result) ){
			return "";
		}
		return $result;


   }

   public function saveActionPlanSubject( $description = "" , $cause = ""  , $solution= "", $expect= "" , $email= ""){

   		$sql = "INSERT INTO action_plan_subject
   				( subject, cause, solution, expect, create_by, create_datetime) 
   				VALUES 
   				(:description,:cause,:solution, :expect, :email, NOW())";
   		$q = $this->conn->prepare($sql);
   			$q->execute(array(
			':description'=>($description),
			':cause'=>($cause),
			':solution'=>($solution),
			':expect'=>($expect),
			':email'=>$email
			));
		return $this->conn->lastInsertId();
   }

    public function updateActionPlanSubject( $description = "" , $cause = ""  , $solution= "", $expect= "" , $email= "", $action_plan_sid){

   		$sql = "UPDATE action_plan_subject SET 
   			subject = :description, 
   			cause = :cause, 
   			solution = :solution, 
   			expect = :expect 
   			WHERE sid = :action_plan_sid";
   		$q = $this->conn->prepare($sql);
   			$q->execute(array(
			':description'=>($description),
			':cause'=>($cause),
			':solution'=>($solution),
			':expect'=>($expect),
			':action_plan_sid'=>$action_plan_sid
			));
		return $action_plan_sid;
   }


 public function saveActionPlanTask($taskName,$action_plan_subject_sid,$task_sid, $email){
 	if($task_sid==""){
	 	return $this->insertTask($action_plan_subject_sid, $taskName, $email);
 	}else{
 		return $this->updateTask($task_sid, $taskName, $email);
 	}
 }

 private function insertTask($action_plan_subject_sid, $name, $email){
 	$sql = "INSERT INTO action_plan_task (action_plan_subject_sid, name, create_datetime, create_by) 
 			VALUES (:action_plan_subject_sid, :name, NOW(), :email) ";
 	$q = $this->conn->prepare($sql);
 	$q->execute(array(
 		':action_plan_subject_sid'=>$action_plan_subject_sid,
 		':name'=>$name,
 		':email'=>$email
 		));
 	return $this->conn->lastInsertId();
 }

 private function updateTask($task_sid, $name, $email){
 	$sql = "UPDATE action_plan_task SET name = :name WHERE sid = :sid ";
 	$q = $this->conn->prepare($sql);
 	$q->execute(array(
 		':name'=>$name,
 		':sid'=>$task_sid
 		));
 	return $task_sid;
 }

 public function saveActionPlanDetail( 
 		$action_plan_task_sid = "" , 
 		$duration= "", 
 		$action = ""  , 
 		$command= "", 
 		$objective= "" , 
 		$expect_result= "", 
 		$responsibility= "", 
 		$remark= "", 
 		$status= "", 
 		$rows){

 	if($this->hadDetail($action_plan_task_sid, $rows)){
 		$this->updateDetail($action_plan_task_sid, $action, $command, $objective, $expect_result, $duration, $responsibility, $remark, $status, $rows);
 	}else{
 		$this->insertDetail($action_plan_task_sid, $action, $command, $objective, $expect_result, $duration, $responsibility, $remark, $status, $rows);
 	}

 }

 private function hadDetail($action_plan_task_sid, $rows){
 	$sql = "SELECT sid FROM action_plan_detail WHERE action_plan_task_sid = :action_plan_task_sid AND rows = :rows ";
 	$q = $this->conn->prepare($sql);
 	$q->execute(array(':action_plan_task_sid'=>$action_plan_task_sid,':rows'=>$rows));
 	$r = $q->fetch();
 	if($r['sid']>0){
 		return true;
 	}
 	return false;
 }
   private function updateDetail($action_plan_task_sid = "" , $action = ""  , $command= "", $objective= "" 
 										, $expect_result= "", $duration= "", $responsibility= "", $remark= "", $status= "", $rows){
   		$sql = "UPDATE action_plan_detail SET 
 				action = :action, 
 				command = :command,
 				objective = :objective,
 				expect_result = :expect_result,
 				duration = :duration,
 				responsibility = :responsibility,
 				remark = :remark,
 				status = :status 
 				WHERE action_plan_task_sid = :action_plan_task_sid AND rows = :rows ";
 		$q = $this->conn->prepare($sql);
 		$r = $q->execute(array(
					':action'=>($action),
					':command'=>($command),
					':objective'=>($objective),
					':expect_result'=>($expect_result),
					':duration'=>($duration),
					':responsibility'=>($responsibility),
					':remark'=>($remark),
					':status'=>($status),
					':action_plan_task_sid'=>($action_plan_task_sid),
					':rows' => $rows
		));
   }

   private function insertDetail($action_plan_task_sid = "" , $action = ""  , $command= "", $objective= "" 
 										, $expect_result= "", $duration= "", $responsibility= "", $remark= "", $status= "", $rows){
   		$sql = "INSERT INTO action_plan_detail 
					( action_plan_task_sid, action, command, objective, expect_result, duration, responsibility, remark, status, rows ) 
					VALUES 
					(:action_plan_task_sid, :action, :command, :objective, :expect_result, :duration, :responsibility, :remark, :status, :rows)";
			$q = $this->conn->prepare($sql);
		return 	$q->execute(array(
						':action_plan_task_sid'=>$action_plan_task_sid,
						':action'=>($action),
						':command'=>($command),
						':objective'=>($objective),
						':expect_result'=>($expect_result),
						':duration'=>($duration),
						':responsibility'=>($responsibility),
						':remark'=>($remark),
						':status'=>($status),
						':rows' => $rows
					));
   }


   public function selectActionPlanBySubject( $subject_sid = "" ){
   		$sql = "SELECT * FROM action_plan_subject 
   				WHERE sid = :subject_sid ";
   		$q = $this->conn->prepare($sql);
		$q->execute(array(':subject_sid'=>$subject_sid));
		$result = $q->fetch();
		$result['task'] = $this->getTask($result['sid']);
		return $result;
   }

   private function getTask($action_plan_subject_sid){
   		$sql = "SELECT * FROM action_plan_task WHERE action_plan_subject_sid = :action_plan_subject_sid ";
   		$q = $this->conn->prepare($sql);
   		$q->execute(array(':action_plan_subject_sid'=>$action_plan_subject_sid));
   		$r = $q->fetchAll();
   		foreach ($r as $key => $value) {
   			$r[$key]['detail'] = $this->getDetail($value['sid']);
   		}
   		return $r;
   }

   private function getDetail($action_plan_task_sid){
   		$sql = "SELECT duration,action,command,objective,expect_result,responsibility,remark,status FROM action_plan_detail WHERE action_plan_task_sid = :action_plan_task_sid ORDER BY rows ASC";
   		$q = $this->conn->prepare($sql);
		$q->execute(array(':action_plan_task_sid'=>$action_plan_task_sid));
		$result = $q->fetchAll();

		$data = array();
		foreach ($result as $key => $value) {
			$data[$key][0] = $value['duration'];
			$data[$key][1] = $value['action'];
			$data[$key][2] = $value['command'];
			$data[$key][3] = $value['objective'];
			$data[$key][4] = $value['expect_result'];
			$data[$key][5] = $value['responsibility'];
			$data[$key][6] = $value['remark'];
			$data[$key][7] = $value['status'];
		}
		return $data;
   }

   public function deleteActionplan($subject_sid){
   	 $sql = "DELETE FROM action_plan_subject WHERE sid = :subject_sid ";
   	 $q = $this->conn->prepare($sql);
   	 return $q->execute(array(':subject_sid'=>$subject_sid));
   }

 //   public function selectToDoPM( $ticket_sid ){

 //   		$sql = "SELECT * FROM ticket_to_do_pm WHERE ticket_sid = :ticket_sid ORDER BY sid DESC LIMIT 0,1";
 //   		$q = $this->conn->prepare($sql);
	// 	$q->execute(array(':ticket_sid'=>$ticket_sid));
	// 	$result = $q->fetch();
		
	// 	if( !($result) ){

	// 		echo "No Result";
	// 	}
	// 	return $result;
 //   }

	
	// public function insertDoSerialPM( $data = array() ){

	// 	$sql = "INSERT INTO task_do_serial_pm 
	// 				(ticket_serial_sid, task_sid, status, create_by, create_datetime) 
	// 				VALUES 
	// 				(:ticket_serial_sid,:task_sid,:status,:create_by, NOW())";
					
	// 	$q = $this->conn->prepare($sql);
	// 	return $q->execute(array(
	// 		':ticket_serial_sid'=>isset($data['ticket_serial_sid'])?$data['ticket_serial_sid']:'',
	// 		':task_sid'=>isset($data['task_sid'])?$data['task_sid']:'',
	// 		':status'=>isset($data['status'])?$data['status']:'',
	// 		':create_by'=>isset($data['create_by'])?$data['create_by']:''
	// 		)
	// 	);

	// }	

	// public function insertInputAction ( $data = array() ){
	// 	$sql = "INSERT INTO task_input_action
	// 				( task_sid, problem ,  solution,  recommend, create_datetime, create_by) 
	// 				VALUES 
	// 				(:task_sid, :problem , :solution, :recommend, NOW() ,:create_by )";
					
	// 	$q = $this->conn->prepare($sql);
	// 	return $q->execute(array(
	// 		':task_sid'=>isset($data['task_sid'])?$data['task_sid']:'',
	// 		':problem'=>isset($data['problem'])?$data['problem']:'',
	// 		':solution'=>isset($data['solution'])?$data['solution']:'',
	// 		':recommend'=>isset($data['recommend'])?$data['recommend']:'',
	// 		':create_by'=>isset($data['create_by'])?$data['create_by']:'' 
	// 		)
	// 	);
	// }
}
?>