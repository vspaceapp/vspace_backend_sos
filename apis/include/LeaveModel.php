<?php
class LeaveModel{

	private $db;
    
    private $email;
    private $startDate;
    private $endDate;

    private $tbl_busy_employee_doc = "tbl_busy_employee_doc";
    private $tbl_busy_employee = "tbl_busy_employee";
    private $tbl_busy_mlookup = "tbl_busy_mlookup";
    private $type_busy_id = "";
    private $detail = "";
    private $busy_date = "";
    private $date_busy = "";
    private $busy_start_time = "";
    private $busy_end_time = "";
    private $create_by = "";
    private $approve = "";
    private $approved_by = "";
    private $type_reason_id = "";
    private $data_status = "";
    private $type_time_leave = "";
    private $report = "";
    private $contract_number = "";
    private $busy_employee_doc_id = "";
    private $modified_by = "";
    
    private $urgency = "";

 	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function loadLeave($email, $token){
        $sql = "SELECT BE.*,BM.*, E.thainame  FROM ".$this->tbl_busy_employee. " BE 
        LEFT JOIN employee E ON E.emailaddr = BE.email 
        LEFT JOIN ".$this->tbl_busy_mlookup." BM ON BE.type_busy_id = BM.id 
        WHERE BE.email = :email ORDER BY BE.date_busy DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetchAll();
        return $r;
    }

    public function typeLeave($email, $token, $type){

    	$sql = "SELECT * FROM tbl_busy_mlookup  BM WHERE BM.busy_mlookup_id = :type AND BM.id NOT IN (13,11,22,24,25,26,27,48,49,28,29) ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':type'=>$type));
    	$r = $q->fetchAll();
    	return $r;
    }

    private function genDate($date_, $hours){
    	$sql = "SELECT DATE_FORMAT(date_add('".$date_."', INTERVAL ".$hours." hour),'%Y-%m-%d') dates, DATE_FORMAT(date_add('".$date_."', INTERVAL ".$hours." hour),'%W') dates_of_week ";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetch();
    	return $r;
    }

    public function processBeforeLeave($email, $token, $data){
        $this->startDate = $data['startDate'];
        $this->endDate = $data['endDate'];
        if($this->endDate=='Choose a date'){
            $this->endDate = $this->startDate;
        }
        $p['numberLeave'] = $this->calNumberLeave();

        $startDate = explode("-", $this->startDate);
        $hour = 0; //ตั้งค่าชั่วโมงเป็น 0
        $holiday = 0; //สำหรับไว้หักลบวันหยุดออกไป
        $d = array();
        $index = 0; //ไว้ทำ index ให้ array
        for ($i = 0; $i < $p['numberLeave']; $i++) {
        	$genDate = $this->genDate($this->startDate,$hour);
            $dayLeave = $genDate['dates'];

            //mktime(0 + $hour, 0, 0, $startDate[1], $startDate[2], $startDate[0]);
            $dOfWeek = $genDate['dates_of_week'];//date("l", mktime(0 + $hour, 0, 0, $startDate[1], $startDate[2], $startDate[0]));

            // $dayLeave = date('Y-m-d', $dayLeave);
            $isHoliday = $this->isHoliday($dayLeave);
            $d[$index]['dOfWeek'] = $this->convertDayToThaiFormat($dOfWeek);
            $d[$index]['dayLeaveTh'] = $this->convertDateToThaiFormat($dayLeave);
            $d[$index]['dayLeave'] = $dayLeave;
            if ($isHoliday=="") {
                $d[$index]['can_leave'] = true;
                // $d[$index]['urgency'] = $this->checkUrgency($dayLeave, $_POST['typeLeave']);
                // $d[$index]['haveService'] = $this->checkHaveService($dayLeave);
                // if (!empty($_POST['typeTimeLeave'])) {
                //     $d[$index]['busyStartTime'] = $_POST['busyStartTime'];
                //     $d[$index]['busyEndTime'] = $_POST['busyEndTime'];
                //     $d[$index]['typeTimeLeave'] = $_POST['typeTimeLeave'];
                //     if ($d[$index]['typeTimeLeave'] != "am-pm") {
                //         $quotaOfUser -=0.5;
                //     } else {
                //         $quotaOfUser--;
                //     }
                // } else {
                //     $quotaOfUser--;
                // }
                // $d[$index]['quota'] = $quotaOfUser;
                
            } else {
                $d[$index]['can_leave'] = false;
                $holiday++;
            }
            $d[$index]['dayDescription'] = $isHoliday;
            $d[$index]['time'] = 'am-pm';
            $index++;
            $hour+=24;
        }
        $p['numberLeave'] = $p['numberLeave'] - $holiday;
        $p['dateCanLeave'] = $d;
        return $p;
    }

    public function processLeave($email, $token, $data){
        $this->create_by = $email;
        $this->urgency = "N";
        $this->detail = $data['detail'];
        $res = $this->saveToTblBusyEmployeeDoc();
        if($res){
            foreach ($data['list_can_leave'] as $key => $value) {
                if($value['can_leave']=="true"){
                    $this->date_busy = $value['dayLeave'];
                    $this->email = $email;

                    $this->type_busy_id = $data['type_leave'];
                    if($value['time']=="am-pm"){
                        $this->busy_start_time = "00:00";
                        $this->busy_end_time = "23:59";
                    }else if($value['time']=="am"){
                        $this->busy_start_time = "00:00";
                        $this->busy_end_time = "11:59";
                    }else if($value['time']=="pm"){
                        $this->busy_start_time = "12:00";
                        $this->busy_end_time = "23:59";
                    }
                    $this->data_status = "Y";
                    $this->type_time_leave = $value['time'];
                    
                    $this->saveToTblBusyEmployee();
                }
            }
        }
    }

    private function saveToTblBusyEmployeeDoc() {

        $sql = "INSERT INTO " . $this->tbl_busy_employee_doc . " "
                . "(id,approve,create_by,data_status,create_datetime) "
                . "VALUES ('','" . $this->urgency . "','" . $this->create_by . "','Y',NOW())";
        $q = $this->db->prepare($sql);
        $q->execute();

        $this->busy_employee_doc_id = $this->db->lastInsertId();
        return $this->busy_employee_doc_id;
    }

    private function saveToTblBusyEmployee() {
        $employeeID = "";

        if (empty($this->urgency))
            $this->urgency = 'W';
        else
            $this->urgency = '';
        // $date_leave = explode("/", $dayLeave);
        // $date_busy = $date_leave[2] . "-" . $date_leave[1] . "-" . $date_leave[0];
        
        $sql = "INSERT INTO " . $this->tbl_busy_employee . " "
                . "(id,"
                . "employee_id,"
                . "type_busy_id,"
                . "detail,"
                . "busy_date,"
                . "busy_start_time,"
                . "busy_end_time,"
                . "create_datetime,"
                . "create_by,"
                . "approve,"
                . "type_reason_id,"
                . "data_status,"
                . "type_time_leave,"
                . "report,"
                . "contract_number,"
                . "busy_employee_doc_id,"
                . "date_busy,email) "
                . " VALUES "
                . "('',"
                . "'" . $employeeID . "',"
                . "'" . $this->type_busy_id . "',"
                . "'" . $this->detail . "',"
                . "'',"
                . "'" . $this->busy_start_time . "',"
                . "'" . $this->busy_end_time . "',"
                . "NOW(),"
                . "'" . $this->email . "',"
                . "'" . $this->urgency . "',"
                . "'',"
                . "'Y',"
                . "'" . $this->type_time_leave . "',"
                . "'',"
                . "'',"
                . "'" . $this->busy_employee_doc_id . "',"
                . "'" . $this->date_busy . "','".$this->email."')";

        $q = $this->db->prepare($sql);
        $r = $q->execute();

        return array('r' => $r);
    }


    private function convertDayToThaiFormat($day) {
        if ($day == 'Sunday')
            $r = "อาทิตย์";
        else if ($day == 'Monday')
            $r = "จันทร์";
        else if ($day == 'Tuesday')
            $r = "อังคาร";
        else if ($day == 'Wednesday')
            $r = "พุธ";
        else if ($day == 'Thursday')
            $r = "พฤหัสบดี";
        else if ($day == 'Friday')
            $r = "ศุกร์";
        else if ($day == 'Saturday')
            $r = "เสาร์";
        return $r;
    }

    private function convertDateToThaiFormat($date) {
        $date = explode('-', $date);
        $thaiMonthArr = array(
            "01" => "มกราคม",
            "02" => "กุมภาพันธ์",
            "03" => "มีนาคม",
            "04" => "เมษายน",
            "05" => "พฤษภาคม",
            "06" => "มิถุนายน",
            "07" => "กรกฎาคม",
            "08" => "สิงหาคม",
            "09" => "กันยายน",
            "10" => "ตุลาคม",
            "11" => "พฤศจิกายน",
            "12" => "ธันวาคม"
        );
        return intval($date[2]) . " " . $thaiMonthArr[$date[1]] . " " . $date[0];
    }

    private function isHoliday($date){
        $sql = "SELECT * FROM tbl_holiday WHERE holiday = :date_ ORDER BY holiday ASC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':date_'=>$date));
        $r = $q->fetch();
        if(isset($r['id']) && $r['id']>0){
            return $r['description_th'];
        }
        return "";
    }

    private function calNumberLeave(){
        // $startDate = explode("-", $this->startDate);
        // $endDate = explode("-", $this->endDate);
        // $startDate = mktime(0, 0, 0, $startDate[1], $startDate[2], $startDate[0]);
        // $endDate = mktime(0, 0, 0, $endDate[1], $endDate[2], $endDate[0]);
        // $numberHour = (($endDate - $startDate) / 60) / 60;
        // $numberDay = round($numberHour / 24);
        // return $numberDay + 1;

        $sql = "SELECT DATEDIFF('".$this->endDate."','".$this->startDate."') AS DiffDate";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return intval($r['DiffDate'])+1;

    }

}