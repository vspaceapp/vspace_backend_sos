<?php
class ServiceType {

	private $db;
	private $serviceType;

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function checkServiceStatusRequired($checkStatus, $listServiceStatus){
    	foreach ($listServiceStatus as $key => $value) {
    		if($value['sid']==$checkStatus){
    			return array(
    				'result'=>true,
    				'subject'=>$value['subject'],'descript_status'=>$value['descript_status'],'btn_right'=>$value['btn_right'],'btn_left'=>$value['btn_left'],
    				'task_status'=>$value['task_status'],
    				'service_type'=>$value['service_type']
    				);
    		}
    	}
    	return array('result'=>false,'subject'=>'','descript_status'=>'','btn_right'=>'','btn_left'=>'','task_status'=>'','service_type'=>'');
    }

    public function listServiceStatus($serviceType){
    	$this->setServiceType($serviceType);
    	return $this->getServiceStatus();
    }

	private function setServiceType($serviceType){
		$this->serviceType = $serviceType;
	}

	private function getServiceStatus(){
		$sql = "SELECT SS.*,ST.type service_type FROM service_type_status STS 
			LEFT JOIN service_status SS ON STS.service_status_sid = SS.sid 
			LEFT JOIN service_type ST ON STS.service_type_sid = ST.sid 
			WHERE STS.service_type_sid = :service_type";

		$q = $this->db->prepare($sql);
		$q->execute(array(':service_type'=>$this->serviceType));
		$r = $q->fetchAll();
		return $r;
	}

}

?>