<?php
class Location_Model{
  private $db;

  private $objCons;
  function __construct($email="") {
      require_once dirname(__FILE__) . '/db_connect.php';
      // opening db connection
      $this->objCons = new DbConnect();
      $this->db = $this->objCons->connect();
  }

  public function index($email){
      $sql = "SELECT sid,CASE WHEN formal_name <> '' THEN formal_name ELSE name END name, email ";
      $sql .= ",latitude, longitude, DATE_FORMAT(updated_datetime, '%Y-%m-%d %H:%i') updated_datetime ";
      $sql .= ",picture_profile ";
      $sql .= " FROM user WHERE (role_sid IN ('permission_ticket') OR email = :email) AND active = 1 AND latitude IS NOT NULL AND latitude <> '' AND latitude <> 0 ORDER BY role_sid, formal_name ASC ";

      $q=$this->db->prepare($sql);
      $q->execute(array(
        ':email'=>$email
      ));
      $r = $q->fetchAll();
      return $r;

  }
}
?>
