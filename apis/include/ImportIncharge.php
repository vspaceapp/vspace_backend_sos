<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
// define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '');
date_default_timezone_set('Europe/London');

class ImportIncharge{
	private $db;
    private $role_sid, $email, $token, $permission_sid;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $db = new DbConnect();
        $this->db = $db->connect();
    }

	function readExcelFile(){
		/** Include PHPExcel_IOFactory */
		require_once '../../../PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

		$strPath = "../../../uploads/";
		$OpenFile = "Incharge.xls";


		if (!file_exists($strPath.$OpenFile)) {
			exit($OpenFile . EOL);
		}
		// echo date('H:i:s') , " Load from Excel2007 file" , EOL;
		$callStartTime = microtime(true);
		$objPHPExcel = PHPExcel_IOFactory::load($strPath.$OpenFile);

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		
		$highestRow = $objWorksheet->getHighestRow();
		// echo "<br/>";
		$highestColumn = $objWorksheet->getHighestColumn();
		// echo "<br/>";

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
		    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
		        ++$r;
		        foreach($headingsArray as $columnKey => $columnHeading) {
		            $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
		        }
		    }
		}

		foreach ($namedDataArray as $key => $value) {
			if($this->checkRepeat($value['Email Address'], $value['Full Name'], $value['Support Group Name*'], $value['Login ID'])){
				$this->insertData($value['Email Address'], $value['Full Name'], $value['Support Group Name*'], $value['Login ID']);
			}
		}
		
		// $xlApp = new COM("Excel.Application");
		// $xlBook = $xlApp->Workbooks->Open($strPath."/".$OpenFile);
		//$xlBook = $xlApp->Workbooks->Open(realpath($OpenFile));

		// $xlSheet1 = $xlBook->Worksheets(1);
	}
	private function checkRepeat($email, $name, $group_name, $login_id){
		$sql = "SELECT * FROM gable_incharge_team WHERE email = :email AND name = :name AND group_name = :group_name AND login_id = :login_id  ";
		$q = $this->db->prepare($sql);
		$q->execute(array(
			':email'=>trim($email), ':name'=>trim($name), ':group_name'=>trim($group_name), ':login_id'=>trim($login_id)));
		$r = $q->fetch();
		if(isset($r['sid']) && $r['sid']>0){
			return false;
		}else{
			return true;
		}
	}

	private function insertData($email, $name, $group_name, $login_id){
		$sql = "INSERT INTO gable_incharge_team (email, name, group_name, login_id) VALUES (:email, :name, :group_name, :login_id) ";
		$q = $this->db->prepare($sql);
		return $q->execute(array(':email'=>trim($email),':name'=>trim($name),':group_name'=>trim($group_name),':login_id'=>trim($login_id)));

	}

}
?>