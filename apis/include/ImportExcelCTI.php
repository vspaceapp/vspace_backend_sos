<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '');
date_default_timezone_set('Europe/London');

class ImportExcelTIPGroup{
	private $db;
    private $role_sid, $email, $token, $permission_sid;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $db = new DbConnect();
        $this->db = $db->connect();
    }

	function readExcelFile(){
		/** Include PHPExcel_IOFactory */
		require_once '../../../PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

		$strPath = "../../../uploads/";
		$OpenFile = "CTI(TIP).xls";


		if (!file_exists($strPath.$OpenFile)) {
			exit($OpenFile . EOL);
		}
		// echo date('H:i:s') , " Load from Excel2007 file" , EOL;
		$callStartTime = microtime(true);
		$objPHPExcel = PHPExcel_IOFactory::load($strPath.$OpenFile);

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		
		$highestRow = $objWorksheet->getHighestRow();
		// echo "<br/>";
		$highestColumn = $objWorksheet->getHighestColumn();
		// echo "<br/>";

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
		    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
		        ++$r;
		        foreach($headingsArray as $columnKey => $columnHeading) {
		            $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
		        }
		    }
		}

		foreach ($namedDataArray as $key => $value) {
			if($this->checkRepeat($value['Type'], $value['Item'], $value['Product'], $value['Support Group Name'])){
				$this->insertData($value['Type'], $value['Item'], $value['Product'], $value['Support Group Name']);
			}
		}
		
		// $xlApp = new COM("Excel.Application");
		// $xlBook = $xlApp->Workbooks->Open($strPath."/".$OpenFile);
		//$xlBook = $xlApp->Workbooks->Open(realpath($OpenFile));

		// $xlSheet1 = $xlBook->Worksheets(1);
	}
	private function checkRepeat($type, $item, $product, $group){
		$sql = "SELECT * FROM tip_group WHERE type = :type AND item = :item AND product = :product AND group_name = :group  ";
		$q = $this->db->prepare($sql);
		$q->execute(array(
			':type'=>$type, ':item'=>$item, ':product'=>$product, ':group'=>trim($group)));
		$r = $q->fetch();
		if(isset($r['sid']) && $r['sid']>0){
			return false;
		}else{
			return true;
		}
	}

	private function insertData($type, $item, $product, $group){
		$sql = "INSERT INTO tip_group (type, item, product, group_name) VALUES (:type, :item, :product, :group) ";
		$q = $this->db->prepare($sql);
		return $q->execute(array(':type'=>$type,':item'=>$item,':product'=>$product,':group'=>$group));

	}

}
?>