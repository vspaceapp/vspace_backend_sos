<?php
class ERequestModel{

	
	private $db;
	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }


    public function listeRequest(){

    	$sql = "SELECT R.*,E.thainame,E.maxportraitfile pic_sale,EE.emailaddr engineer_email, EE.thainame engineer_thainame FROM flguploa_firstlogicis.tbl_esrs_service_request_form R 
    	LEFT JOIN employee E ON R.sales_id = E.empno 
    	LEFT JOIN employee EE ON EE.empno = R.assign_engineer 
        LEFT JOIN flguploa_cases.project P ON R.dummy_contract = P.contract 
    	WHERE R.stage = 'D' AND R.data_status = 'Y' AND R.approval = 'Y' AND R.doc_date > '2016-01-01' AND P.sid is NULL ORDER BY R.date_time_modify DESC ";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();

    	return $r;
    }
}
?>