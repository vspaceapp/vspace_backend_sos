<?php

require_once  __dir__."/../../core/constant.php";
class VSpaceToKBank{
    
    private $db;
    private $userName, $password;
    function __construct() {
        require_once __dir__.'/../db_connect.php';
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->userName = REMEDY_USER;
        $this->password = REMEDY_PASS;

    }
    public function prepareDataUpdateTicket($ticket, $defaultByTeam = null){
        $data = array();
        $defaultInfo = isset($defaultByTeam)?$this->getDefaultByTeam($defaultByTeam):null;
        if(isset($defaultInfo)){
            $data['assigned_support_company'] = $defaultInfo['assigned_support_company'];
            $data['assigned_support_organization'] = $defaultInfo['assigned_support_organization'];
            $data['assigned_group']  = $defaultInfo['assigned_group'];
            $data['assigned_group_id']  = $defaultInfo['assigned_group_id'];
            $data['assignee']  = $defaultInfo['assignee'];
            $data['assignee_login_id']  = $defaultInfo['assignee_login_id'];
        }else{
            $data['assigned_support_company'] = $this->checkParam($ticket['assigned_support_company']);
            $data['assigned_support_organization'] = $this->checkParam($ticket['assigned_support_organization']);
            $data['assigned_group']  = $this->checkParam($ticket['assigned_group']);
            $data['assigned_group_id']  = $this->checkParam($ticket['assigned_group_id']);
            $data['assignee']  = $this->checkParam($ticket['assignee']);
            $data['assignee_login_id']  = $this->checkParam($ticket['assignee_login_id']);
        }
        $data['incident_number'] = $ticket['incident_number'];
        $data['status'] = $ticket['status'];
        $data['status_reason'] = $this->checkParam($ticket['status_reason']);
        $data['z1d_status_reason'] = $data['status_reason'];
        $data['z1d_note'] = $this->checkParam($ticket['z1d_note']);
        $data['z1d_action'] = $this->checkParam($ticket['z1d_action'], "UPDATE");
        $data['z1d_char01'] = $this->checkParam($ticket['z1d_char01']);
        $data['resolution'] = $this->checkParam($ticket['resolution']);
        $data['resolution_category'] = $this->checkParam($ticket['resolution_category'], "Client");
        $data['resolution_category_tier_2'] = $this->checkParam($ticket['resolution_category_tier_2'], "Other");
        $data['resolution_category_tier_3'] = $this->checkParam($ticket['resolution_category_tier_3']);
        return $data;
    }

    private function checkParam($value, $default = null){
        return isset($value)?$value:$default;
    }

    private function createSoapClient($wsdlUrl, $nameSpace){
        try{
            // SOAP 1.2 client
            $params = array(
                'encoding' => 'UTF-8',
                'trace' => 1,
                'exceptions' => 1,
                //'stream_context' => stream_context_create($opts),
                'cache_wsdl' => WSDL_CACHE_NONE
            );

            $client = new SoapClient($wsdlUrl, $params);
            $dataHeader = array(
                "userName" => $this->userName,
                "password" => $this->password,
                "authentication"=>"?",
                "locale"=>"",
                "timeZone"=>"?"
            );
            $header = new SOAPHeader($nameSpace, "AuthenticationInfo", $dataHeader,false);
            $client->__setSoapHeaders($header);
            return $client;
        }catch(Exception $e){
            $this->insertLogReturnFromRemedy($e->getMessage() , "", "", 1);
            return null;
        }
    }

    // recommend use prepareData function to prepare data first or you want to manual set params.
    public function updateIncidentCase($data, $submitter){
        if(USE_LOCAL_XML){
            $dest = dirname(__FILE__) . DIRECTORY_SEPARATOR.REMEDY_FILENAME_XML_UPDATE_TICKET;
        }else{
            $dest = REMEDY_WEBSERVICE_URL_UPDATE_TICKET;
        }
        $soapClient = $this->createSoapClient($dest, REMEDY_NAMESPACE_UPDATE_TICKET); // created by file.xml local
        if(isset($soapClient)){
            try{
                $result = $soapClient->New_Set_Operation_UpdateIncident( array(
                    "Incident_Number"=>$data['incident_number'],
                    "Status"=>$data['status'],
                    "Status_Reason"=>$data['status_reason'],
                    "z1D_Status_Reason"=> $data['z1d_status_reason'],

                    "Assigned_Support_Company"=>$data['assigned_support_company'],
                    "Assigned_Support_Organization"=>$data['assigned_support_organization'],
                    "Assigned_Group"=>$data['assigned_group'],
                    "Assignee"=>$data['assignee'],
                    "Assigned_Group_ID"=>$data['assigned_group_id'],
                    "Assignee_Login_ID"=>$data['assignee_login_id'],

                    "z1D_Note"=>$data['z1d_note'],
                    "Resolution_Category"=>$data['resolution_category'],
                    "Resolution_Category_Tier_2"=>$data['resolution_category_tier_2'],
                    "Resolution_Category_Tier_3"=>$data['resolution_category_tier_3'],
                    "Resolution"=>$data['resolution'],
                    "z1D_Action"=>$data['z1d_action'],
                    "z1D_Char01"=>$data['z1d_char01']
                ));
                //$res = $json = json_encode($result);
                $res['response'] = $result;
//                $res['last_request'] = $client->__getLastRequest();
//                $res['last_response'] = $client->__getLastResponse();
//                $res['params'] = func_get_args();
                $res = json_encode($res);
                $this->insertLogReturnFromRemedy($res, $data['incident_number'], $submitter, 0, "", $data['status']);
                return true;
            }catch(Exception $e){
                $res['last_request'] = $soapClient->__getLastRequest();
                $res['last_response'] = $soapClient->__getLastResponse();
                $res['error'] =$e->getMessage();
                $res['params'] = func_get_args();
                $res = json_encode($res);
                $this->insertLogReturnFromRemedy($res , $data['incident_number'], $submitter, 1, "", $data['status']);
                return false;
            }
        }
        return false;

    }

    private function insertLogReturnFromRemedy($res, $incidentNumber, $submitter, $error, $worklog = "", $status = ""){
        $sql = "INSERT INTO response_remedy (response_data, create_datetime, other, create_by, worklog, error, stamp_sla)
                        VALUE (:response_data, NOW(), :other, :create_by, :worklog, :error, :stamp_sla)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(
            array(
                ":response_data"=>$res ,
                ":other"=>$incidentNumber ,
                ":create_by"=>$submitter ,
                ":worklog"=>$worklog,
                ":error"=>$error,
                ":stamp_sla"=>$status
            )
        );
    }

    private function getDefaultByTeam($team){

        $info = array();
        $info['assignee'] = REMEDY_ASSIGNEE_NAME;
        $info['assignee_login_id'] = REMEDY_ASSIGNEE_LOGIN_ID;
        if($team == "EndPoint Service Center"){
            $remedyInfo = unserialize(REMEDY_ENDPOINT_INFO);
            $info['assigned_support_company'] = $remedyInfo['support_company'];
            $info['assigned_support_organization'] = $remedyInfo['support_organization'];
            $info['assigned_group'] = $remedyInfo['support_group_name'];
            $info['assigned_group_id'] = $remedyInfo['support_group_id'];
        }else if($team == "Incident Coordinator"){
            $remedyInfo = unserialize(REMEDY_INCIDENT_CO_INFO);
            $info['assigned_support_company'] = $remedyInfo['support_company'];
            $info['assigned_support_organization'] = $remedyInfo['support_organization'];
            $info['assigned_group'] = $remedyInfo['support_group_name'];
            $info['assigned_group_id'] = $remedyInfo['support_group_id'];
        }else{
            return null;
        }
        return $info;
    }

    public function createUpdateWorklog(
        $submitter,
        $Incident_Number,
        $Summary,
        $Note,
        $z2AF_Work_Log01_attachmentName = "",
        $z2AF_Work_Log01_attachmentData = "",
        $z2AF_Work_Log01_attachmentOrigSize = "",
        $z2AF_Work_Log02_attachmentName = "",
        $z2AF_Work_Log02_attachmentData = "",
        $z2AF_Work_Log02_attachmentOrigSize = "",
        $z2AF_Work_Log03_attachmentName = "",
        $z2AF_Work_Log03_attachmentData = "",
        $z2AF_Work_Log03_attachmentOrigSize = ""
        ){

        if(USE_LOCAL_XML){
            $dest = dirname(__FILE__) . DIRECTORY_SEPARATOR.REMEDY_FILENAME_XML_UPDATE_WORKLOG;
        }else{
            $dest = REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG;
        }
        $soapClient = $this->createSoapClient($dest, REMEDY_NAMESPACE_UPDATE_WORKLOG);
        if(isset($soapClient)){
            try{
                $View_Access = "Internal";
                $Secure_Work_Log = "Yes";
                //$Work_Info_Type = "General Information"; พี่หวานบอกว่า ให้ใช้เป็น Status Update เพื่อไม่ให้ remedy ส่งมาซ่ำ
                $Work_Info_Type = "Status Update";
                $result  = $soapClient->New_Create_Operation_CreateIncidentWorkLog( array(
                    "Incident_Number"=>$Incident_Number,
                    "Summary"=>$Summary,
                    "Note"=>$Note,
                    //"Work_Info_Type"=>$Work_Info_Type,
                    "Work_Log_Type"=>$Work_Info_Type,
                    "z2AF_Work_Log01_attachmentName"=>$z2AF_Work_Log01_attachmentName,
                    "z2AF_Work_Log01_attachmentData"=>$z2AF_Work_Log01_attachmentData,
                    "z2AF_Work_Log01_attachmentOrigSize"=>$z2AF_Work_Log01_attachmentOrigSize,
                    "z2AF_Work_Log02_attachmentName"=>$z2AF_Work_Log02_attachmentName,
                    "z2AF_Work_Log02_attachmentData"=>$z2AF_Work_Log02_attachmentData,
                    "z2AF_Work_Log02_attachmentOrigSize"=>$z2AF_Work_Log02_attachmentOrigSize,
                    "z2AF_Work_Log03_attachmentName"=>$z2AF_Work_Log03_attachmentName,
                    "z2AF_Work_Log03_attachmentData"=>$z2AF_Work_Log03_attachmentData,
                    "z2AF_Work_Log03_attachmentOrigSize"=>$z2AF_Work_Log03_attachmentOrigSize,
                    "Secure_Work_Log"=>$Secure_Work_Log,
                    "View_Access"=>$View_Access
                ));
                $res['response'] = $result;
                $res = json_encode($res);
                $this->insertLogReturnFromRemedy($res, $Incident_Number, $submitter, 0, $Note);
                return true;
            }catch(Exception $e){
                $res['last_request'] = $soapClient->__getLastRequest();
                $res['last_response'] = $soapClient->__getLastResponse();
                $res['error'] =$e->getMessage();
                $res['params'] = func_get_args();
                $res = json_encode($res);
                $this->insertLogReturnFromRemedy($res, $Incident_Number, $submitter,1, $Note);
                return false;
            }
        }
        return false;
    }
}
?>