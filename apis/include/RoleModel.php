<?php
class RoleModel{

	private $db;
    private $role_sid, $token, $permission_sid;

    public $email;
    private $ticketPermission = array();

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();

        $this->ticketPermission[0]['create_sr'] = true;
        $this->ticketPermission[0]['change_owner'] = true;
        $this->ticketPermission[0]['change_status'] = true;
        $this->ticketPermission[0]['add_worklog'] = true;
        $this->ticketPermission[0]['edit_address'] = true;
        $this->ticketPermission[0]['edit_contact_user'] = true;
        $this->ticketPermission[0]['edit_note'] = true;
        $this->ticketPermission[0]['download_attach_file'] = true;

        $this->ticketPermission[1]['create_sr'] = false;
        $this->ticketPermission[1]['change_owner'] = false;
        $this->ticketPermission[1]['change_status'] = false;
        $this->ticketPermission[1]['add_worklog'] = true;
        $this->ticketPermission[1]['edit_address'] = false;
        $this->ticketPermission[1]['edit_contact_user'] = false;
        $this->ticketPermission[1]['edit_note'] = false;
        $this->ticketPermission[1]['download_attach_file'] = false;
    }
    private function getEmail2($email){
        $sql = "SELECT * FROM user U WHERE U.email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['email_2'];
    }
    public function permissionTicketWithRole($data, $request){
        $email_2 = $this->getEmail2($request['email']);
        $permission = $this->checkPermission($request['email'], $data['owner']);
        $index = 0;

        if($data['create_by']==$request['email'] || $data['create_by'] == $email_2){
            // $data['permission'] = array(true,"Created");
            $index = 0;
        }else if($permission){
            // $data['permission'] = array($permission,"Created");
            $index = 0;
        }else if($data['owner']==$request['email'] || $data['owner'] == $email_2){
            // $data['permission'] = array(true,"Owner");
            $index = 0;
        }else{
            // $data['permission'] = array(false,"View Only");
            $index = 1;
        }
        return $this->ticketPermission[$index];
    }


    public function dashboard($role_sid){
    	return $this->meunurole($role_sid);
    }

    public function updateRoleDetail($sid, $column, $value, $email){
        $sql = "UPDATE role SET ".$column." = :value, updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':value'=>$value,':email'=>$email,':sid'=>$sid));
    }

    public function updateRole($email, $token, $data){
        if(isset($data['sid'])){
            $sql = "UPDATE role SET can_assign = :can_assign, prefix_table = :prefix_table, escaration_to = :escaration_to, supervisor_role_sid = :supervisor_role_sid, name = :name, role_name_view = :role_name_view, updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            return $q->execute(array(
                ':can_assign'=>$data['can_assign'],
                ':prefix_table'=>$data['prefix_table'],
                ':escaration_to'=>$data['escaration_to'],
                ':supervisor_role_sid'=>$data['supervisor_role_sid'],
                ':name'=>$data['name'],
                ':role_name_view'=>$data['name'],
                ':email'=>$email,
                ':sid'=>$data['sid']
                )
            );
        }else{

            $data['rolename'] = $data['name'];
            $data['fullRolename'] = $data['name'];
            $data['description'] = $data['name'];

            return $this->saveRole($email, $token, $data);
        }
    }

    private function meunurole($role_sid){
    	$sql = "SELECT M.* FROM modules MODULES
    			LEFT JOIN menus M ON MODULES.sid = M.modules_sid
    			LEFT JOIN menus_role MR ON MR.menus_sid = M.sid
    			WHERE MR.role_sid = :role_sid
    			";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':role_sid'=>$role_sid));
    	return $q->fetchAll();
    }

    public function module(){

    }

    public function dataRole(){
        $sql = "SELECT * FROM role WHERE sid = :role_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':role_sid'=>$this->role_sid));
        $r = $q->fetch();
        return $r;
    }

    public function listRole($email, $token){
        $sql = "SELECT *, (SELECT GROUP_CONCAT(U.email SEPARATOR ', ') FROM user U WHERE U.role_sid = R.sid ) member FROM role R ORDER BY R.name ASC";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $q->fetchAll();
    }

    public function saveRole($email, $token, $data){
        $sql = "INSERT INTO role( role_name_view, can_assign, name, description, create_datetime, create_by)
                VALUES (:role_name_view, :can_assign, :full_role_name_view, :description, NOW(), :email)";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
                    ':role_name_view' => $data['rolename'] ,
                    ':can_assign' => $data['can_assign'] ,
                    ':full_role_name_view' => $data['fullRolename'] ,
                    ':description' =>  $data['description'] ,
                    ':email' => $email
                    ));
    }

    public function viewSettingRole($role_sid, $email, $token){
        $this->role_sid = $role_sid;
        $this->email = $email;
        $this->token = $token;

        $menuSetting = $this->menuSetting();
        $menuMobileSetting = $this->menuMobileSetting();
        $segmentSetting = $this->segmentSetting();
        $dashboardSetting = $this->dashboardSetting();
        $dataRole = $this->dataRole();
        $userInRole = $this->userInRole();
        $userRoleInRole = $this->userRoleInRole();

        return array('dataRole'=>$dataRole,'menu'=>$menuSetting,'menuMobile'=>$menuMobileSetting,'segment'=>$segmentSetting,'dashboard'=>$dashboardSetting,'userInRole'=>$userInRole,'userRoleInRole'=>$userRoleInRole);
    }

    private function userInRole(){
        $sql = "SELECT *,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) picture, U.updated_datetime, U.role_sid FROM user U LEFT JOIN employee E ON U.email = E.emailaddr WHERE role_sid = :role_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':role_sid'=>$this->role_sid));
        $r = $q->fetchAll();
        return $r;
    }

        private function userRoleInRole(){
        $sql = "SELECT *,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) picture FROM user_role UR LEFT JOIN employee E ON UR.email = E.emailaddr WHERE role_sid = :role_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':role_sid'=>$this->role_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function menuSetting(){
        $sql = 'SELECT M.*,
        (SELECT role_sid FROM menus_role WHERE menus_sid = M.sid AND role_sid = :role_sid ORDER BY sid DESC LIMIT 0,1) value_set
         FROM menus M
                WHERE data_status != -1 ORDER BY  M.modules_sid, M.sequence ';
        $q = $this->db->prepare($sql);
        $q->execute( array(':role_sid'=>$this->role_sid) );
        $r = $q->fetchAll();
        return $r;

    }
    private function menuMobileSetting(){
      $sql = 'SELECT MB.*,
            (SELECT role_sid FROM menus_mobile_role WHERE menus_mobile_sid = MB.sid AND role_sid = :role_sid ORDER BY sid DESC LIMIT 0,1) value_set
                FROM menus_mobile MB
                WHERE 1 ORDER BY MB.is_main DESC,MB.name ASC';
      $q = $this->db->prepare($sql);
        $q->execute( array(':role_sid'=>$this->role_sid) );
        $r = $q->fetchAll();
        return $r;

    }
    private function segmentSetting(){
       $sql = 'SELECT *,
       (SELECT role_sid FROM segmented_mobile_role WHERE segmented_mobile_sid = SM.sid AND role_sid = :role_sid ORDER BY sid DESC LIMIT 0,1) value_set
       FROM segmented_mobile SM
                WHERE 1';
       $q = $this->db->prepare($sql);
        $q->execute( array(':role_sid'=>$this->role_sid) );
        $r = $q->fetchAll();
        return $r;
    }
    private function dashboardSetting(){
        $sql = 'SELECT *,
        (SELECT role_sid FROM dashboard_role WHERE dashboard_sid = D.sid AND role_sid = :role_sid ORDER BY sid DESC LIMIT 0,1) value_set
         FROM dashboard D
                WHERE 1 AND D.data_status >= 0';
        $q = $this->db->prepare($sql);
        $q->execute( array(':role_sid'=>$this->role_sid) );
        $r = $q->fetchAll();
        return $r;
    }

    public function managePermission( $type, $value, $role_sid, $permission_sid, $email, $token,$column=""){
        $this->role_sid = $role_sid;
        $this->permission_sid = $permission_sid;
        $this->email = $email;
        if( $type == "menu"){
            if( $value=="true"){
                $this->insertMenu();
            }else{
                $this->deleteMenu();
            }
        }else if( $type == "menu_mobile" ){
            if( $value=="true"){
                $this->insertMenuMobile();
            }else{
                $this->deleteMenuMobile();
            }
        }else if( $type == "segment" ){
            if( $value=="true"){
                $this->insertSegment();
            }else{
                $this->deleteSegment();
            }
        }else if( $type == "dashboard" ){
            if( $value=="true"){
                $this->insertDashboard();
            }else{
                $this->deleteDashboard();
            }
        }else if($type=="role"){
            $this->updateRoleOneColumn($value, $column);
        }
    }
    private function updateRoleOneColumn($value, $column){
        $sql = "UPDATE role SET ".$column." = :value,updated_datetime = NOW(), updated_by = :email  WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        return $q->execute(array(':value'=>$value,':email'=>$this->email,':sid'=>$this->role_sid));

    }
    private function insertMenu() {
        $sql = 'INSERT INTO menus_role ( menus_sid, role_sid) VALUES ( :menus_sid ,:role_sid )';
        $q = $this->db->prepare($sql);
        return $q->execute( array( ':menus_sid' => $this->permission_sid,':role_sid' => $this->role_sid ));
    }

    private function insertMenuMobile(){
        $sql = 'INSERT INTO menus_mobile_role ( menus_mobile_sid, role_sid)
        VALUES ( :menus_mobile_sid ,:role_sid )';
        $q = $this->db->prepare($sql);
        return $q->execute( array( ':menus_mobile_sid' => $this->permission_sid,':role_sid' => $this->role_sid ));
    }

    private function insertSegment(){
        $sql = 'INSERT INTO segmented_mobile_role ( segmented_mobile_sid, role_sid, detail)
        VALUES ( :segmented_mobile_sid ,:role_sid, :detail )';
        $q = $this->db->prepare($sql);

        if($this->permission_sid=="4"){
            $detail = "Notifications";
        }else if($this->permission_sid=="2"){
            $detail = "Unsigned";
        }else if($this->permission_sid=="1"){
            $detail = "TaskSR";
        }else if($this->permission_sid=="3"){
            $detail = "Feeds";
        }
        return $q->execute( array( ':segmented_mobile_sid' => $this->permission_sid,':role_sid' => $this->role_sid,':detail'=>$detail ));
    }

    private function insertDashboard(){
        $sql = 'INSERT INTO dashboard_role ( role_sid, dashboard_sid, row, sequence)
        VALUES ( :role_sid, :dashboard_sid, (SELECT row FROM dashboard WHERE sid = :dashboard_sid LIMIT 0,1), (SELECT sequence FROM dashboard WHERE sid = :dashboard_sid LIMIT 0,1) )';
        $q = $this->db->prepare($sql);
        return $q->execute( array( ':role_sid' => $this->role_sid ,':dashboard_sid' => $this->permission_sid));
    }


    private function deleteMenu(){
        $sql = 'DELETE FROM menus_role WHERE menus_sid = :menus_sid AND role_sid = :role_sid';
        $q = $this->db->prepare($sql);
        return $q->execute( array(':menus_sid' => $this->permission_sid, ':role_sid' => $this->role_sid));

    }

    private function deleteMenuMobile(){
        $sql = 'DELETE FROM  menus_mobile_role WHERE menus_mobile_sid = :menus_mobile_sid AND role_sid = :role_sid';
        $q = $this->db->prepare($sql);
        return $q->execute( array(':menus_mobile_sid' => $this->permission_sid, ':role_sid' => $this->role_sid));

    }

    private function deleteSegment(){
        $sql = 'DELETE FROM  segmented_mobile_role WHERE segmented_mobile_sid = :segmented_mobile_sid AND role_sid = :role_sid';
        $q = $this->db->prepare($sql);
        return $q->execute( array(':segmented_mobile_sid' => $this->permission_sid, ':role_sid' => $this->role_sid));

    }

    private function deleteDashboard(){
        $sql = 'DELETE FROM  dashboard_role WHERE role_sid = :role_sid AND dashboard_sid = :dashboard_sid';
        $q = $this->db->prepare($sql);
        return $q->execute( array(':role_sid' => $this->role_sid, ':dashboard_sid' => $this->permission_sid));

    }

    public function getPermissionOfRoleSid($role_sid){
        $data = $this->getRoleByRoleSid($role_sid);
        $data['can_assign_detail'] = $this->listRoleAndUser($data['can_assign']);
        $data['permission_project_detail'] = $this->listRoleAndUser($data['permission_project']);
        return $data;
    }

    public function getRoleByRoleSid($role_sid){
        $sql = "SELECT * FROM role WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$role_sid));
        return $q->fetch();
    }

    public function listRoleAndUser($string){
        $array = explode(",", $string);
        $data = array();
        foreach ($array as $key => $value) {
            array_push($data, array('role_sid'=>$value,'staff'=>$this->getUserOfRoleSid($value)));
        }
        return $data;
    }
    public function getUserOfRoleSid($role_sid){
        $sql = "SELECT *,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) picture FROM user U LEFT JOIN employee E ON U.email = E.emailaddr WHERE role_sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$role_sid));
        return $q->fetchAll();
    }

    public function checkPermission($userOnline, $userOwner){
	      // for test only 12/1/2017
            return true;
          //

          // original code
//        $role_user_online_sid = $this->getRoleByEmail($userOnline);
//        $role_owner_sid = $this->getRoleByEmail($userOwner);
//
//        if($role_owner_sid){
//            $sql = "SELECT * FROM role R WHERE R.sid = :sid AND R.admin_role_sid LIKE :role_sid ";
//            $q = $this->db->prepare($sql);
//            $q->execute(array(':sid'=>$role_user_online_sid,':role_sid'=>'%'.$role_owner_sid.',%'));
//            $r = $q->fetch();
//            if(isset($r['sid']) && $r['sid']>0){
//                return true;
//            }else{
//                return false;
//            }
//        }else{
//            return false;
//        }
        // end of original code
    }

    public function checkServicesPermission($permissionType){
	    $dataRole = $this->getRoleByEmail();
	    return $dataRole[$permissionType] == 1;
    }

    public function getRoleByEmail(){
	    $sql = "SELECT R.* 
          FROM role R
          INNER JOIN user U on U.role_sid = R.sid
          WHERE U.email = :email
          LIMIT 0,1";
	    $q = $this->db->prepare($sql);
	    $q->execute(array(':email'=>$this->email));
	    return $q->fetch();
    }

}

?>
