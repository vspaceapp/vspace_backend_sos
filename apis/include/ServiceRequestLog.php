<?php
/**
 * Created by PhpStorm.
 * User: nanthawat
 * Date: 6/2/2018
 * Time: 5:22 PM
 */
include_once __dir__."/db_connect.php";
class ServiceRequestLog{
    private $work_order_id, $service_request_id, $company, $customer_name, $customer_phone, $customer_email, $customer_mobile,
        $location, $address, $summary, $note, $number_of_device, $assigned_group_id, $status, $service_request_type,
        $product_category_tier_1, $product_category_tier_2, $product_category_tier_3;
    private $team, $owner, $missingField = [];
    private $caseModel;
    private $db;
    public $no_ticket = null;

    function __construct(){
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->caseModel = new CaseModel();
    }
    public function initData($data){
        $checkData = $this->checkRequireField($data);
        if(!$checkData){
            return false;
        }
        $this->work_order_id = $data['work_order_id'];
        $this->service_request_id = $data['service_request_id'];
        $this->company = $data['company'];
        $this->customer_name = $data['customer_name'];
        $this->customer_phone = $data['customer_phone'];
        $this->customer_email = $data['customer_email'];
        $this->customer_mobile = $data['customer_mobile'];
        $this->location = $data['location'];
        $this->address = $data['address'];
        $this->summary = $data['summary'];
        $this->note = $data['note'];
        $this->number_of_device = $data['number_of_device'];
        $this->assigned_group_id = $data['assigned_group_id'];
        $this->status = empty($data['status'])?"Assigned":$data['status'];
        $this->service_request_type = $data['service_request_type'];
        $this->product_category_tier_1 = isset($data['product_category_tier_1'])?$data['product_category_tier_1']:'';
        $this->product_category_tier_2 = isset($data['product_category_tier_2'])?$data['product_category_tier_2']:'';
        $this->product_category_tier_3 = isset($data['product_category_tier_3'])?$data['product_category_tier_3']:'';
        return true;
    }

    private function checkRequireField($data){
        $this->missingField = [];
        empty($data['work_order_id']) && array_push($this->missingField, 'work_order_id');
        empty($data['service_request_id']) && array_push($this->missingField, 'service_request_id');
        empty($data['customer_name']) && array_push($this->missingField, 'customer_name');
        empty($data['customer_email']) && array_push($this->missingField, 'customer_email');
        empty($data['location']) && array_push($this->missingField, 'location');
        empty($data['summary']) && array_push($this->missingField, 'summary');
        empty($data['note']) && array_push($this->missingField, 'note');
        empty($data['number_of_device']) && array_push($this->missingField, 'number_of_device');
        empty($data['assigned_group_id']) && array_push($this->missingField, 'assigned_group_id');
        empty($data['service_request_type']) && array_push($this->missingField, 'service_request_type');
        if(count($this->missingField)>0){
            return false;
        }
        return true;
    }

    public function getMissingField(){
        return $comma_separated = implode(",", $this->missingField);
    }

    public function insertLog(){
        try{
            $sql = "INSERT INTO ticket_request_log
            (
                no_ticket,
                work_order_id,
                service_request_id,
                company,
                customer_name,
                customer_phone,
                customer_email,
                customer_mobile,
                location,
                address,
                summary,
                note,
                number_of_device,
                assigned_group_id,
                status,
                service_request_type,
                product_category_tier_1,
                product_category_tier_2,
                product_category_tier_3,
                create_datetime
            ) VALUES
            (
                :no_ticket,
                :work_order_id,
                :service_request_id,
                :company,
                :customer_name,
                :customer_phone,
                :customer_email,
                :customer_mobile,
                :location,
                :address,
                :summary,
                :note,
                :number_of_device,
                :assigned_group_id,
                :status,
                :service_request_type,
                :product_category_tier_1,
                :product_category_tier_2,
                :product_category_tier_3,
                NOW()
            )";
            $q = $this->db->prepare($sql);
            return $q->execute(
                array(
                    ":no_ticket"=>$this->no_ticket,
                    ":work_order_id"=>$this->work_order_id,
                    ":service_request_id"=>$this->service_request_id,
                    ":company"=>$this->company,
                    ":customer_name"=>$this->customer_name,
                    ":customer_phone"=>$this->customer_phone,
                    ":customer_email"=>$this->customer_email,
                    ":customer_mobile"=>$this->customer_mobile,
                    ":location"=>$this->location,
                    ":address"=>$this->address,
                    ":summary"=>$this->summary,
                    ":note"=>$this->note,
                    ":number_of_device"=>$this->number_of_device,
                    ":assigned_group_id"=>$this->assigned_group_id,
                    ":status"=>$this->status,
                    ":service_request_type"=>$this->service_request_type,
                    ":product_category_tier_1"=>$this->product_category_tier_1,
                    ":product_category_tier_2"=>$this->product_category_tier_2,
                    ":product_category_tier_3"=>$this->product_category_tier_3
                )
            );
        }catch(Exception $e){
            return false;
        }

    }

    public function setTeamAndOwner(){
        $defaultAssigned  = $this->caseModel->getDefaultOwnerByRoleSid($this->assigned_group_id);
        if(!$defaultAssigned){
            return false;
        }
        $this->team = $defaultAssigned['team'];
        $this->owner = $defaultAssigned['owner'];
        return true;
    }

    public function isDuplicate(){
        $sql = "SELECT sid FROM ticket_request_log 
                WHERE work_order_id = :work_order_id AND service_request_id = :service_request_id";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(
                ":work_order_id"=>$this->work_order_id,
                ":service_request_id"=>$this->service_request_id
            )
        );
        $r = $q->fetch();
        if(isset($r['sid']) && $r['sid']>0){
            return true;
        }
        return false;
    }


    public function mapDataToTicketObject(){
        $this->caseModel->refer_remedy_hd = $this->work_order_id;
        $this->caseModel->team = $this->team;
        $this->caseModel->owner = $this->owner;
        $this->caseModel->end_user = $this->company;
        $this->caseModel->end_user_company_name = $this->company;
        $this->caseModel->case_company = $this->company;
        $this->caseModel->end_user_contact_name = $this->customer_name;
        $this->caseModel->end_user_phone = $this->customer_phone;
        $this->caseModel->end_user_mobile = $this->customer_mobile;
        $this->caseModel->end_user_email = $this->customer_email;
        $this->caseModel->incident_location = $this->location;
        $this->caseModel->end_user_site = $this->address;
        $this->caseModel->incident_address = $this->address;
        $this->caseModel->subject = $this->summary;
        $this->caseModel->description = $this->note;
        $this->caseModel->note = $this->note;
        $this->caseModel->number_of_device = $this->number_of_device;
        $this->caseModel->team_id = $this->assigned_group_id;
        $this->caseModel->case_type = 'request';
        $this->caseModel->product_categorization_tier_1 = $this->product_category_tier_1;
        $this->caseModel->product_categorization_tier_2 = $this->product_category_tier_2;
        $this->caseModel->product_categorization_tier_3 = $this->product_category_tier_3;
        $this->caseModel->resolution_product_category_tier_1 = $this->product_category_tier_1;
        $this->caseModel->resolution_product_category_tier_2 = $this->product_category_tier_2;
        $this->caseModel->resolution_product_category_tier_3 = $this->product_category_tier_3;
        $this->caseModel->request_type = $this->service_request_type;
        $this->caseModel->contract_no = '';
        $this->caseModel->via = '';
        $this->caseModel->due_datetime = '';
        $this->caseModel->serial_no = '';
        return $this->caseModel;
    }
}
?>