<?php

class TicketModel{

	private $conn;

  private $email;
  private $task_request;
  private $expect_time;
  private $tasks_sid;
  private $task_request_sid;
  private $m;
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->conn = $db->connect();
    }

   // public function editendser( $pk,$name,$value){
      
   //    $sql = "UPDATE ticket_log SET ".$name." = :value WHERE ticket_sid = :pk ";
   //    $q = $this->conn->prepare($sql);
   //    return $q->execute(array(':value'=>$value,':pk'=>$pk));
   // }

   public function editendser( $pk,$name,$value){
      if($email!=""){
        $this->m->setTable($email);
      }
   		$sql = "UPDATE ".$this->m->table_ticket." SET ".$name." = :value WHERE sid = :pk ";
      $q = $this->conn->prepare($sql);
      $q->execute(array(':value'=>$value,':pk'=>$pk));

      $sql = "UPDATE ".$this->m->table_ticket_log." SET ".$name." = :value WHERE ticket_sid = :pk ";
      $q = $this->conn->prepare($sql);
      return $q->execute(array(':value'=>$value,':pk'=>$pk));
   }

   public function deleteSignatureCustomer($signature_customer_sid){
      if($email!=""){
        $this->m->setTable($email);
      }
      $sql = "UPDATE ".$this->m->table_customer_signature_task." SET task_sid = 0, status = -1 WHERE sid = :sid ";
      $q = $this->conn->prepare($sql);
      return $q->execute(array(':sid'=>$signature_customer_sid));
   }

   public function inputTaskRequest($email,$task_request,$expect_time,$tasks_sid,$task_request_sid){
      if($email!=""){
        $this->m->setTable($email);
      }
      $this->task_request_sid = $task_request_sid;
      $this->email = $email;
      $this->task_request = trim(nl2br($task_request));
      $this->expect_time = $expect_time;
      $this->tasks_sid = $tasks_sid;

      if($this->task_request_sid=="" || $this->task_request_sid<1){
        return $this->insertTaskRequest();
      }else{
        return $this->updateTaskRequest();
      }
   }

   private function insertTaskRequest(){
      $sql = "INSERT INTO ".$this->m->table_task_input_request." (tasks_sid, task_request, expect_time, create_datetime, create_by) VALUES (:tasks_sid, :task_request, :expect_time, NOW(), :create_by)";
      $q = $this->conn->prepare($sql);
      $q->execute(array(
        ':tasks_sid'=>$this->tasks_sid, 
        ':task_request'=>$this->task_request, 
        ':expect_time'=>$this->expect_time,
        ':create_by'=>$this->email
        ));
      return $this->conn->lastInsertId();
   }

   private function updateTaskRequest(){
    $sql = "UPDATE ".$this->m->table_task_input_request." SET task_request = :task_request, expect_time = :expect_time,update_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
    $q = $this->conn->prepare($sql);
    $q->execute(array(
      ':task_request'=>$this->task_request,
      ':expect_time'=>$this->expect_time,
      ':email'=>$this->email,
      ':sid'=>$this->task_request_sid
      ));
    return $this->task_request_sid;
   }
   public function getTaskRequest($task_request_sid){
      if($email!=""){
        $this->m->setTable($email);
      }
      $this->task_request_sid = $task_request_sid;
      $sql = "SELECT *,DATE_FORMAT(expect_time,'%H:%i') expect_time,DATE_FORMAT(expect_time,'%H') expect_time_h,DATE_FORMAT(expect_time,'%i') expect_time_m FROM ".$this->m->table_task_input_request." WHERE sid = :sid ";
      $q = $this->conn->prepare($sql);
      $q->execute(array(
        ':sid'=>$this->task_request_sid
        ));
      return $q->fetch();
   }
   public function deleteTaskRequest($task_request_sid,$email){
      if($email!=""){
        $this->m->setTable($email);
      }
      $this->task_request_sid = $task_request_sid;
      $this->email = $email;

      $sql = "DELETE FROM ".$this->m->table_task_input_request." WHERE sid = :sid ";
      $q =  $this->conn->prepare($sql);
      return $q->execute(array(':sid'=>$task_request_sid));
   }
}
?>