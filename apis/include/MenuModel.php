<?php Class MenuModel {
	
	private $db;

	private $type;
	private $email;

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function menuService($type, $email){
    	$this->type = $type;
    	$this->email = $email;
    	$res = array();
    	if($this->type=="mobile"){
            $type2 = "menu";
    		$res = $this->menuMobile($type2);
    	}else{

    	}

    	return $res;
    }
    public function moreService($type, $email){
        $this->type = $type;
        $this->email = $email;

        $res = array();
        if($this->type=="mobile"){
            $type2 = "more";
            $res = $this->menuMobile($type2);
        }else{

        }
        return $res;
    }
    private function menuMobile($type){
    	$sql = "SELECT MM.* FROM user_role U 
    	LEFT JOIN menus_mobile_role MMR ON U.role_sid = MMR.role_sid 
    	LEFT JOIN menus_mobile MM ON MMR.menus_mobile_sid = MM.sid 
    	WHERE U.email = :email AND type = :type ORDER BY MM.sequence ASC 
    	";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':email'=>$this->email,':type'=>$type));
    	$r = $q->fetchAll();

        if(count($r)>0){
        	return $r;
        }else{
            $sql = "SELECT MM.* FROM user U 
                LEFT JOIN menus_mobile_role MMR ON U.role_sid = MMR.role_sid 
                LEFT JOIN menus_mobile MM ON MMR.menus_mobile_sid = MM.sid 
                WHERE U.email = :email AND type = :type ORDER BY MM.sequence ASC 
                ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':email'=>$this->email,':type'=>$type));
            return $r = $q->fetchAll();
        }
    }

    private function menuWeb(){

    }
}

?>