<?php
error_reporting(-1);
ini_set('display_errors', 'On');
class SettingAdmModel{
	private $db;
  private $m;
  private $email, $token;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
  }
  public function selectData(){
    try{
        $sql = "SELECT * FROM gable_adm ORDER BY sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return array('status'=>1, 'message'=>'Data Ok', 'data'=>$r);
    }catch(PDOException $e){
        return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function addData($data){
    try{
      $email_adm = isset($data['email_adm'])?$data['email_adm']:'';
      $role = isset($data['role'])?$data['role']:'';
      $group_code = isset($data['group_code'])?$data['group_code']:'';

      $email = isset($data['email'])?$data['email']:'';

      if($email_adm && $role && $email && $group_code){
        $sql = "INSERT INTO gable_adm (email, role, group_code, created_by, created_datetime)
        VALUES (:email_adm, :role, :group_code, :created_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':email_adm'=>$email_adm,
          ':role'=>$role,
          ':group_code'=>$group_code,
          ':created_by'=>$email
        ));
        return array('status'=>1, 'message'=>'Added', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'email_adm, role, group_code, email ต้องไม่เป็นค่าว่าง', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function editData($data){
    try{
      if(isset($data['sid'])){
        $email_adm = isset($data['email_adm'])?$data['email_adm']:'';
        $role = isset($data['role'])?$data['role']:'';
        $group_code = isset($data['group_code'])?$data['group_code']:'';

        $email = isset($data['email'])?$data['email']:'';
        $token = isset($data['token'])?$data['token']:'';

        if($email_adm && $role && $group_code && $email && $token){

          $sql = "UPDATE gable_adm
          SET email = :email_adm, role = :role,
          group_code = :group_code, updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
          $q = $this->db->prepare($sql);
          $q->execute(
            array(
              ':email_adm'=>$email_adm,
              ':role'=>$role,
              ':group_code'=>$group_code,
              ':email'=>$email,
              ':sid'=>$data['sid']
            )
          );
          return array('status'=>1, 'message'=>'Updated', 'data'=>array());
        }else{
          return array('status'=>0, 'message'=>'ข้อมูล email_adm, role, group_code, email, token ต้องไม่เป็นค่าว่าง', 'data'=>array());
        }
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function deleteData($data){
    try{
      if(isset($data['sid'])){
        $email = isset($data['email'])?$data['email']:'';

        $sql = "UPDATE gable_adm SET data_status = '-1', updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email,':sid'=>$data['sid']));
        return array('status'=>1, 'message'=>'Deleted', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
}
