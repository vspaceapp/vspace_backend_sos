<?php
class PictureControl{

	public $path;
	
	/*
	FUNCTION NAME : contructor
	PURPOSE : For set default variable
	PARAMETER : 1. picture folder path (PATHNAME with postfix /)
	RETURN : NaN
	*/
	public function __construct($path){
		$this->path = $path;
	}
	
	/*
	FUNCTION NAME : uploadPicture
	PURPOSE : For upload pictures to server 
	PARAMETER : 1. real file name 
				2. tempolary file name 
	RETURN : Success = true
			 Fail - false
	*/
	public function uploadPicture($file_name,$file_name_temp){
		$sourcePath = $file_name_temp;       // Storing source path of the file in a variable
		$targetPath = $this->path.$file_name; // Target path where file is to be stored
		return move_uploaded_file($sourcePath,$targetPath);    // Moving Uploaded file
	}
	
	/*
	FUNCTION NAME : uploadPicture
	PURPOSE : For get file extention from full information e.g. image/png, image/jpg, image/jpeg
	PARAMETER : 1. full type name
	RETURN : Success = file extention
			 Fail - error
	*/
	public function getPictureExtention($type){
		return substr($type,strpos($type,'/')+1);
	}
	
	/*
	FUNCTION NAME : renamePicture
	PURPOSE : For rename picture file name
	PARAMETER : 1. Existing file name 
				2. New file name 
	RETURN : Success = true
			 Fail - false
	*/
	public function renamePicture($file_name_source,$file_name_new){
		return rename($this->path.$file_name_source, $this->path.$file_name_new);
	}
	
	/*
	FUNCTION NAME : resizePicture
	PURPOSE : resize dimention W H not size files
	PARAMETER : 1. Temporary file name
				2. New file name 
				3. set picture width 
	RETURN : Success = true
			 Fail - false
	*/
	public function resizePicture($file_name_new,$width){

		//$size=GetimageSize($file_name_temp);
		$size=GetimageSize($this->path.$file_name_new);
		$height=round($width*$size[1]/$size[0]);
		//$images_orig = ImageCreateFromJPEG($file_name_temp);
		$images_orig = ImageCreateFromJPEG($this->path.$file_name_new);
		$photoX = ImagesX($images_orig);
		$photoY = ImagesY($images_orig);
		$images_fin = ImageCreateTrueColor($width, $height);
		ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		ImageJPEG($images_fin,$this->path.$file_name_new);
		ImageDestroy($images_orig);
		ImageDestroy($images_fin);
	}
	
	/*
	FUNCTION NAME : compressPicture
	PURPOSE : reduce quality , reduce size
	PARAMETER : 1. Temporary file name
				2. New file name 
				3. set picture width 
	RETURN : Success = true
			 Fail - false
	*/
	public function compressPicture($file_name_new,$qty){
		$info = getimagesize($this->path.$file_name_new); 
		
		if ($info['mime'] == 'image/jpeg') 
			$image = imagecreatefromjpeg($this->path.$file_name_new); 
		elseif ($info['mime'] == 'image/gif') 
			$image = imagecreatefromgif($this->path.$file_name_new); 
		elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($this->path.$file_name_new); 
			
		imagejpeg($image, $this->path.$file_name_new, $qty);
		
	}

}
?>