<?php
 error_reporting(-1);
ini_set('display_errors', 'On');
/**
 * Handling database connection
 *
 */
define('END_POINT_APIS','apis');
define('END_POINT_API','api');
define('END_POINT_PATH_EMPLOYEE','http://vspace.in.th/api/assets/images/employee/');
include_once __dir__.'/../core/config.php';
//include '../../core/config.php';

class DbConnect {

    private $conn;
    private $email;

    public $table_ticket = "ticket";
    public $table_ticket_log = "ticket_log";
    public $table_ticket_status = "ticket_status";
    public $table_ticket_owner = "ticket_owner";
    public $table_ticket_serial = "ticket_serial";
    public $table_ticket_to_do_pm = "ticket_to_do_pm";

    public $table_ticket_spare = "ticket_spare";
    public $table_ticket_spare_task = "ticket_spare_task";
    public $table_ticket_spare_part = "ticket_spare_part";
    public $table_ticket_sla = "table_ticket_sla";


    public $table_tasks = "tasks";
    public $table_tasks_log = "tasks_log";
    public $table_task_do_serial_pm = "task_do_serial_pm";
    public $table_task_followup_log = "task_followup_log";
    public $table_task_input_action = "task_input_action";
    public $table_task_input_request = "task_input_request";
    public $table_task_spare = "task_spare";

    public $table_project_owner = "project_owner";
    public $table_project = "project";

    public $table_customer_signature_task = "customer_signature_task";

    public $table_activity_log = "activity_log";
    function __construct() {
    }
    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        // // Connecting to mysql database
        // $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        // // Check for database connection error
        // if (mysqli_connect_errno()) {
        //     echo "Failed to connect to MySQL: " . mysqli_connect_error();
        // }
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        // generate a database connection, using the PDO connector
        // @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
        $this->conn = new PDO(DB_TYPE . ':host=' . DB_HOST.";port=" .DB_PORT. ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USERNAME, DB_PASSWORD, $options);
        // returing connection resource
        return $this->conn;
    }

    public function setTable($email){
        $this->table_ticket = "ticket";
        $this->table_ticket_log = "ticket_log";
        $this->table_ticket_status = "ticket_status";
        $this->table_ticket_owner = "ticket_owner";
        $this->table_ticket_serial = "ticket_serial";
        $this->table_ticket_to_do_pm = "ticket_to_do_pm";

        $this->table_ticket_spare = "ticket_spare";
        $this->table_ticket_spare_task = "ticket_spare_task";
        $this->table_ticket_spare_part = "ticket_spare_part";
        $this->table_ticket_sla = "ticket_sla";

        $this->table_tasks = "tasks";
        $this->table_tasks_log = "tasks_log";
        $this->table_task_do_serial_pm = "task_do_serial_pm";
        $this->table_task_followup_log = "task_followup_log";
        $this->table_task_input_action = "task_input_action";
        $this->table_task_input_request = "task_input_request";
        $this->table_task_spare = "task_spare";

        $this->table_project_owner = "project_owner";
        $this->table_project = "project";

        $this->table_customer_signature_task = "customer_signature_task";

        $this->table_activity_log = "activity_log";

        $role_sid = $this->getRoleByEmail($email);
        if($role_sid){
            $this->table_ticket = $this->getTablePrefix($role_sid).$this->table_ticket;
            $this->table_ticket_log = $this->getTablePrefix($role_sid).$this->table_ticket_log;
            $this->table_ticket_status = $this->getTablePrefix($role_sid).$this->table_ticket_status;
            $this->table_ticket_owner = $this->getTablePrefix($role_sid).$this->table_ticket_owner;
            $this->table_ticket_serial = $this->getTablePrefix($role_sid).$this->table_ticket_serial;
            $this->table_ticket_to_do_pm = $this->getTablePrefix($role_sid).$this->table_ticket_to_do_pm;

            $this->table_ticket_spare =  $this->getTablePrefix($role_sid).$this->table_ticket_spare;
            $this->table_ticket_spare_task =  $this->getTablePrefix($role_sid).$this->table_ticket_spare_task;
            $this->table_ticket_spare_part =  $this->getTablePrefix($role_sid).$this->table_ticket_spare_part;
            $this->table_ticket_sla = $this->getTablePrefix($role_sid).$this->table_ticket_sla;


            $this->table_tasks = $this->getTablePrefix($role_sid).$this->table_tasks;
            $this->table_tasks_log = $this->getTablePrefix($role_sid).$this->table_tasks_log;
            $this->table_task_do_serial_pm = $this->getTablePrefix($role_sid).$this->table_task_do_serial_pm;
            $this->table_task_followup_log = $this->getTablePrefix($role_sid).$this->table_task_followup_log;
            $this->table_task_input_action = $this->getTablePrefix($role_sid).$this->table_task_input_action;
            $this->table_task_input_request = $this->getTablePrefix($role_sid).$this->table_task_input_request;
            $this->table_task_spare = $this->getTablePrefix($role_sid).$this->table_task_spare;

            $this->table_project = $this->getTablePrefix($role_sid).$this->table_project;
            $this->table_project_owner = $this->getTablePrefix($role_sid).$this->table_project_owner;

            $this->table_customer_signature_task = $this->getTablePrefix($role_sid).$this->table_customer_signature_task;
            $this->table_activity_log = $this->getTablePrefix($role_sid).$this->table_activity_log;
        }else{
            $this->table_ticket = "gen_".$this->table_ticket;
            $this->table_ticket_log = "gen_".$this->table_ticket_log;
            $this->table_ticket_status = "gen_".$this->table_ticket_status;
            $this->table_ticket_owner = "gen_".$this->table_ticket_owner;
            $this->table_ticket_serial = "gen_".$this->table_ticket_serial;
            $this->table_ticket_to_do_pm = "gen_".$this->table_ticket_to_do_pm;

            $this->table_ticket_spare =  "gen_".$this->table_ticket_spare;
            $this->table_ticket_spare_task =  "gen_".$this->table_ticket_spare_task;
            $this->table_ticket_spare_part =  "gen_".$this->table_ticket_spare_part;
            $this->table_ticket_sla = "gen_".$this->table_ticket_sla;

            $this->table_tasks = "gen_".$this->table_tasks;
            $this->table_tasks_log = "gen_".$this->table_tasks_log;
            $this->table_task_do_serial_pm = "gen_".$this->table_task_do_serial_pm;
            $this->table_task_followup_log = "gen_".$this->table_task_followup_log;
            $this->table_task_input_action = "gen_".$this->table_task_input_action;
            $this->table_task_input_request = "gen_".$this->table_task_input_request;
            $this->table_task_spare = "gen_".$this->table_task_spare;

            $this->table_project = "gen_".$this->table_project;
            $this->table_project_owner = "gen_".$this->table_project_owner;

            $this->table_customer_signature_task = "gen_".$this->table_customer_signature_task;
            $this->table_activity_log = "gen_".$this->table_activity_log;
        }
    }

    public function isAccount($email){
        $this->email = $email;
        // if($this->email=="autsakorn.t@firstlogic.co.th"){
        //     return false;
        // }
        $address = explode("@", $this->email);
        if(in_array($address[1], $this->listAccountInternal())){
            return true;
        }
        return false;
    }

    private function listAccountInternal(){

        $accountInternal = array('firstlogic.co.th', 'g-able.com');
        return $accountInternal;
    }

    public function sendNotificationAction($email, $message, $notification_sid=0, $ticket_sid=0,$tasks_sid=0,$project_sid=0){
        $sql = "SELECT UL.* FROM user U LEFT JOIN user_location UL ON U.email = UL.email WHERE U.email = :email ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();

        // คลุ้ม try-catch เพราะมีปัยหา registration_id รอแก้ไข
        try{
            if(isset($r['device_platform'])){
                if($r['device_platform']=="Android"){
                    sendAndroid($r['registration_id'],$message);
                }else{
                    sendNotificationIOS($r['registration_id'],$message,'case.flgupload.com',1);
                }
            }

            $sql = "INSERT INTO notification_send (send_to, message, create_datetime, notification_sid,ticket_sid, tasks_sid, project_sid) VALUES (:send_to, :message, NOW(),:notification_sid, :ticket_sid, :tasks_sid, :project_sid) ";
            $q = $this->conn->prepare($sql);
            $q->execute(array(':send_to'=>$email,':message'=>$message,':notification_sid'=>$notification_sid,':ticket_sid'=>$ticket_sid, ':tasks_sid'=>$tasks_sid, ':project_sid'=>$project_sid));
        }catch(Exception $e){

        }

    }

    public function getTablePrefix($role_sid){
        $sql = "SELECT prefix_table FROM role WHERE sid = :role_sid";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':role_sid'=>$role_sid));
        $r = $q->fetch();
        return $r['prefix_table'];
    }

    public function getCompReferEBiz($role_sid){
        $sql = "SELECT comp_refer_ebiz FROM role WHERE sid = :role_sid";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':role_sid'=>$role_sid));
        $r = $q->fetch();
        return $r['comp_refer_ebiz'];
    }

    public function getTeamByEmail($email){
        $sql = "SELECT team FROM user_role WHERE email = :email ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['team'];
    }

    public function getRoleByEmail($email){
        $res = "";
        $sql = "SELECT role_sid FROM user_role WHERE email = :email ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        if(!isset($r['role_sid']) || $r['role_sid']==""){
            $sql = "SELECT role_sid FROM user WHERE email = :email";
            $q = $this->conn->prepare($sql);
            $q->execute(array(':email'=>$email));
            $r = $q->fetch();
            $res = $r['role_sid'];
        }else{
            $res = $r['role_sid'];
        }
        return $res;
    }
    public function permissionRoleByEmail($email){
        $role_sid = $this->getRoleByEmail($email);

        $sql = "SELECT * FROM role WHERE sid = :sid";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':sid'=>$role_sid));
        $r = $q->fetch();
        return $r;
    }
    public function segmentMobileRole($role_sid){
        $data = array();
        for ($i=1; $i <= 4; $i++) {
            $data[$i-1] = $this->segmentMobileSid($role_sid, $i);
        }
        return $data;
    }

    public function segmentMobileSid($role_sid, $segmented_mobile_sid){
        $sql = "SELECT SMR.segmented_mobile_sid sid, SMR.detail,SM.name FROM segmented_mobile_role SMR LEFT JOIN segmented_mobile SM ON SMR.segmented_mobile_sid = SM.sid WHERE SMR.role_sid = :role_sid AND SMR.segmented_mobile_sid = :segmented_mobile_sid";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':role_sid'=>$role_sid,':segmented_mobile_sid'=>$segmented_mobile_sid));
        $r = $q->fetch();
        return $r;
    }

    public function getInfoEmployee($email){
        $sql = "SELECT thainame, engname, emailaddr,  CONCAT('".END_POINT_PATH_EMPLOYEE."',maxportraitfile) employee_pic, mobile FROM employee WHERE emailaddr = :email ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r;
    }

    public function getInfoUserByEmail($email){
        $sql = "SELECT email, CASE WHEN formal_name <> '' THEN formal_name ELSE name END name, picture_profile, picture_profile_base64,
        company_logo, path_signature
         FROM user WHERE email = :email";
        $q = $this->conn->prepare($sql);
        $q->execute(array(":email"=>$email));
        $r = $q->fetch();
        return $r;
    }

    public function getCanAssign($email){
        $sql = "SELECT * FROM role R LEFT JOIN user_role UR ON R.sid = UR.role_sid WHERE UR.email = :email";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['can_assign'];
    }

    public function getPermissionProject($email){
        $sql = "SELECT * FROM role R LEFT JOIN user_role UR ON R.sid = UR.role_sid WHERE UR.email = :email";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['permission_project'];
    }

    public function convertDateSlashForDb($date){

        if($date){
            $date = explode("/", $date);
            return $date[2]."-".$date[1]."-".$date[0];
        }
        return "";
    }

    public function listUserCanAddTask($email,$search="",$notEmail = array()){
        $sql = "SELECT UR.role_sid,R.can_assign FROM user_role UR LEFT JOIN role R ON UR.role_sid = R.sid WHERE UR.email = :email AND UR.data_status > 0";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        $role_sid = $r['role_sid'];
        $can_assign = $r['can_assign'];
        if($can_assign==""){
            // $can_assign = $role_sid;
            $sqlCondition = " (UR.email = :email AND UR.data_status > 0) ";
        }else{
            $sqlCondition = " (UR.email = :email OR UR.role_sid IN (".$can_assign.")) ";
        }
        $sql = "SELECT E.*,UR.email email,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_employee,R.role_name_view
        FROM user_role UR LEFT JOIN employee E ON UR.email = E.emailaddr AND E.emailaddr <> ''
        LEFT JOIN role R ON R.sid = UR.role_sid
        LEFT JOIN user U ON UR.email = U.email
        WHERE 1 AND UR.data_status > 0 AND ".$sqlCondition." OR (UR.role_sid = '321' and U.user_type = 1)";
        if($search!=""){
            $sql .= " AND (UR.email LIKE '%".$search."%' OR E.thainame LIKE '%".$search."%' ) ";
        }

        if(count($notEmail)>0){
            foreach ($notEmail as $key => $value) {
                $sql .= " AND UR.email <> '".$value."' ";
            }
        }

        $sql .= " ORDER BY E.thainame ";
        // echo $sql;
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetchAll();
        $data = array();
        $data = $r;
        // array_push($data, $this->me($email));

        return $data;
    }

    public function me($email){
        $sql = "SELECT user.picture_profile pic_employee,
        CASE WHEN formal_name <> '' THEN formal_name ELSE name END thainame,
        CASE WHEN formal_name <> '' THEN formal_name ELSE name END engname,
        CASE WHEN formal_name <> '' THEN formal_name ELSE name END name,
        email email, email emailaddr
         FROM user WHERE email = :email ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        return $q->fetch();
    }
    public function listUserCanAddProject($email,$search="",$notEmail = array()){
        $sql = "SELECT UR.role_sid,R.permission_project FROM user_role UR LEFT JOIN role R ON UR.role_sid = R.sid WHERE UR.email = :email AND UR.data_status > 0";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        $role_sid = $r['role_sid'];
        $can_assign = $r['permission_project'];
        if($can_assign==""){
            // $can_assign = $role_sid;
            $sqlCondition = " (UR.email = :email AND UR.data_status > 0) ";
        }else{
            $sqlCondition = " (UR.email = :email OR UR.role_sid IN (".$can_assign.")) ";
        }
        $sql = "SELECT E.*,UR.email email,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_employee,R.role_name_view
        FROM user_role UR LEFT JOIN employee E ON UR.email = E.emailaddr AND E.emailaddr <> ''
        LEFT JOIN role R ON R.sid = UR.role_sid
        WHERE 1 AND UR.data_status > 0 AND ".$sqlCondition;
        if($search!=""){
            $sql .= " AND (UR.email LIKE '%".$search."%' OR E.thainame LIKE '%".$search."%' ) ";
        }

        if(count($notEmail)>0){
            foreach ($notEmail as $key => $value) {
                $sql .= " AND UR.email <> '".$value."' ";
            }
        }

        $sql .= " ORDER BY R.sid DESC,E.thainame ";
        // echo $sql;
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetchAll();
        return $r;
    }


}
?>
