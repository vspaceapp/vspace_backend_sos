<?php
require_once __dir__."/../../apis/core/constant.php";
class BackgroundModel{
	private $db;
	private $m;
	private $ticket_sid;

    private $email;
    private $work_log;
    private $expect_pending;
    private $status;
    private $refer_remedy_hd;

	function __construct() {
        require_once __dir__.'/db_connect.php';
        // opening db connection
        $this->m = new DbConnect();
        $this->db = $this->m->connect();
 	}
    public function sendMailOpenTicket(){
        require_once 'SendMail.php';
        $objsendmail = new SendMail();
        $data = $this->newOpenTicket();
        print_r($data);
        foreach ($data as $key => $value) {
            $objsendmail->noticeEngineerCaseAssigned($value['sid']);
            // $objsendmail->sendToCustomerCaseOpened($value['sid']);
            $this->updateSentMailOpenTicket($value['sid']);
        }
    }
    public function updateSentMailOpenTicket($sid){
        $sql = "UPDATE ticket SET send_mail_open_ticket = 1, send_email_open_ticket_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$sid));

    }
    public function newOpenTicket(){
        $sql = "SELECT sid FROM ticket WHERE send_mail_open_ticket = 0 AND refer_remedy_hd IS NULL AND TIMESTAMPDIFF(MINUTE,create_datetime,NOW()) > 5 ";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $r = $q->fetchAll();
    }
    public function genPDF(){
        $data = $this->listNewCustomerSign();
        // print_r($data);
        return $data;
    }
    public function updateWhenGenPDF($customer_signature_task_sid){
        $sql = "UPDATE customer_signature_task SET gen_pdf = 1, updated_gen_pdf_datetime = NOW() WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$customer_signature_task_sid));

    }
    public function updateWhenSendMailPDF($customer_signature_task_sid){
        $sql = "UPDATE customer_signature_task SET send_email_pdf = 1, send_email_datetime = NOW() WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$customer_signature_task_sid));
    }
    public function listNewCustomerSign(){
        $sql = "SELECT CST.sid,CST.task_sid, CST.gen_pdf, TIMESTAMPDIFF(MINUTE,CST.create_datetime,NOW()) create_datetime_diff,
        CST.create_datetime,CST.file_name
        FROM customer_signature_task CST WHERE CST.gen_pdf = 0 AND TIMESTAMPDIFF(MINUTE,CST.create_datetime,NOW()) > 5 LIMIT 0,10";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    public function listNewGenPDF(){
        $sql = "SELECT CST.sid,CST.task_sid, CST.send_email_pdf,
        TIMESTAMPDIFF(MINUTE,CST.updated_gen_pdf_datetime,NOW()) updated_gen_pdf_datetime_diff, CST.updated_gen_pdf_datetime
        FROM customer_signature_task CST WHERE CST.send_email_pdf = 0 AND CST.gen_pdf = 1 AND TIMESTAMPDIFF(MINUTE,CST.updated_gen_pdf_datetime,NOW()) > 5 LIMIT 0,10";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function processUpdateTaxifare(){
        $data = $this->listUnSetRequestTaxi();
        foreach ($data as $key => $value) {
            $sql = "SELECT request_taxi FROM tasks_log WHERE tasks_sid = :tasks_sid AND engineer = :engineer ORDER BY sid DESC LIMIT 0,1 ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':tasks_sid'=>$value['tasks_sid'],':engineer'=>$value['engineer']));
            $r = $q->fetch();

            $sql = "UPDATE tasks_engineer SET request_taxi = :request_taxi, updated_request_taxi = NOW() WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':request_taxi'=>$r['request_taxi'],':sid'=>$value['sid']));
        }
    }

    public function listUnSetRequestTaxi(){
        $sql = "SELECT * FROM tasks_engineer WHERE updated_request_taxi = '0000-00-00 00:00:00' AND engineer <> '' ORDER BY sid ASC LIMIT 0,10";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    /// UPDATE START AND CLOSE tasks
    public function updateCloseSr(){
        $data = $this->listSrInProcess();

        foreach ($data as $key => $value) {
            // echo "<pre>";
            // print_r($value);
            // echo "</pre>";
            $startDatetime = $this->selectTasksLogStartByTasksSid($value['sid'], $value['engineer']);
            if($startDatetime){
                $this->updateStartSrQuery($startDatetime, $value['sid']);
            }
            $closedDatetime = $this->selectTasksLogClosedByTasksSid($value['sid'], $value['engineer']);
            // echo $closedDatetime;
            if($closedDatetime){
                $this->updateCloseSrQuery($closedDatetime, $value['sid']);
            }
        }
    }
    private function updateStartSrQuery($closedDatetime, $sid){
        $sql = "UPDATE tasks SET started_datetime = :started_datetime WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':started_datetime'=>$closedDatetime, ':sid'=>$sid));
    }
    private function updateCloseSrQuery($closedDatetime, $sid){
        $sql = "UPDATE tasks SET closed_datetime = :closed_datetime WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':closed_datetime'=>$closedDatetime, ':sid'=>$sid));
    }

    public function listSrInProcess(){
        $sql = "SELECT T.sid, T.engineer FROM tasks T LEFT JOIN ticket TT ON T.ticket_sid = TT.sid WHERE T.closed_datetime = '0000-00-00 00:00:00' AND T.last_status >=400 AND (T.appointment_type = 0 OR T.appointment_type = 1) ORDER BY T.sid LIMIT 0,10";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    public function selectTasksLogClosedByTasksSid($tasks_sid, $engineer){
        $sql = "SELECT sid, CASE WHEN datetime_to_report <> '0000-00-00 00:00:00' THEN datetime_to_report ELSE create_datetime END datetime_to_report FROM tasks_log WHERE tasks_sid = :tasks_sid AND engineer = :engineer AND status = '400' ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid,':engineer'=>$engineer));
        $r = $q->fetch();
        if(isset($r['sid']) && $r['sid']>0){
            return $r['datetime_to_report'];
        }else{
            return false;
        }
    }
    public function selectTasksLogStartByTasksSid($tasks_sid, $engineer){
        $sql = "SELECT sid, CASE WHEN datetime_to_report <> '0000-00-00 00:00:00' THEN datetime_to_report ELSE create_datetime END datetime_to_report FROM tasks_log WHERE tasks_sid = :tasks_sid AND engineer = :engineer AND status = '300' ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid,':engineer'=>$engineer));
        $r = $q->fetch();
        if(isset($r['sid']) && $r['sid']>0){
            return $r['datetime_to_report'];
        }else{
            return false;
        }
    }
    // END update START AND CLOSE tasks

    // จะถูกเรียกกว่า Background Process เพื่อคำนวนความคืบหน้า Project
    public function calProgressAndUpdateProjectProgress(){
        $sql = "SELECT sid, name, contract,create_by FROM project WHERE data_status <> '400' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $finished_hours = 0;
            $expect_hours = 0;
            $dataTicket = $this->ticketByProjectSid($value['sid']);
            $progress = 0;

            foreach ($dataTicket as $k => $v) {
                if($v['status']=='5'){
                    $finished_hours += $v['man_days'];
                }
                $expect_hours += $v['man_days'];
            }
            $r[$key]['finished_hours'] = $finished_hours;
            $r[$key]['expect_hours'] = $expect_hours;
            if($expect_hours>0){
                $progress = $r[$key]['progress'] = $finished_hours/$expect_hours*100;
            }else{
                $progress = $r[$key]['progress'] = 0;
            }
            $this->updateProjectProgress($value['sid'], $finished_hours, $expect_hours, $progress);
        }
        return $r;
    }
    private function updateProjectProgress($project_sid, $finished_hours, $expect_hours, $progress){
        $sql = "UPDATE project SET
        planning_expect_hours = :planning_expect_hours,
        updated_progress = NOW(),
        planning_finished_hours = :planning_finished_hours,
        progress = :progress
        WHERE sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(
                ':planning_expect_hours'=>$expect_hours,
                ':planning_finished_hours'=>$finished_hours,
                ':progress'=>$progress,
                ':project_sid'=>$project_sid
            )
        );
    }
    public function ticketByProjectSid($project_sid){
        $sql = "SELECT * FROM ticket WHERE project_sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        return $q->fetchAll();
    }
    public function updateCheckPointStandby(){
        $sql = "SELECT T.* FROM tasks T
        WHERE DATE_ADD(T.appointment, INTERVAL T.expect_duration hour) < NOW()
        AND T.appointment_type IN (2,3,4)
        AND (SELECT TL.status FROM tasks_log TL WHERE TL.tasks_sid = T.sid ORDER BY status DESC LIMIT 0,1) < 600 ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $this->insertIntoTaskslog($value['engineer'], $value, '600');
        }
        return $r;
    }

    public function insert_user_create_dummy_contract_company($data){
        if(count($data['can_create_dummy_company'])>0){
            foreach ($data['can_create_dummy_company'] as $key => $v) {
                     $sql = "INSERT INTO user_create_dummy_contract_company
                     (user_sid, COMPANY_SEQ, COMPANY_ID, COMPANY_NAME)
                     VALUES (:user_sid, :company_seq, :company_id, :company_name) ";
                     $q = $this->db->prepare($sql);
                     $q->execute(array(
                        ':user_sid'=>$data['user_sid'],
                        ':company_seq'=>$v['COMPANY_SEQ'],
                        ':company_id'=>$v['COMPANY_ID'],
                        ':company_name'=>$v['COMPANY_NAME']
                        ));

            }
        }else{
             $sql = "INSERT INTO user_create_dummy_contract_company
                     (user_sid, COMPANY_SEQ, COMPANY_ID, COMPANY_NAME)
                     VALUES (:user_sid, :company_seq, :company_id, :company_name) ";
                     $q = $this->db->prepare($sql);
                     $q->execute(array(
                        ':user_sid'=>$data['user_sid'],
                        ':company_seq'=>"",
                        ':company_id'=>"",
                        ':company_name'=>""
                        ));
        }
    }

    public function selectUserWithEmpno(){
        $sql = "SELECT U.sid user_sid, U.email, E.empno, UCDCC.sid ucdcc_sid FROM user U
        INNER JOIN employee E ON U.email = E.emailaddr
        LEFT JOIN user_create_dummy_contract_company UCDCC ON UCDCC.user_sid = U.sid
        WHERE UCDCC.sid IS NULL LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function tableProjectNoDataContract(){
        $sql = "SELECT P.*,R.comp_refer_ebiz,E.empno empno,P.project_for_company,
        (SELECT DATE_FORMAT(PO.project_start,'%d/%m/%Y') FROM project_owner PO WHERE PO.project_sid = P.sid LIMIT 0,1) project_start,
        (SELECT DATE_FORMAT(PO.project_end,'%d/%m/%Y') FROM project_owner PO WHERE PO.project_sid = P.sid LIMIT 0,1) project_end
        FROM project P LEFT JOIN user_role UR ON P.create_by = UR.email LEFT JOIN role R ON UR.role_sid = R.sid
        LEFT JOIN employee E ON P.create_by = E.emailaddr
        WHERE 1 AND (P.contract = '' OR P.contract = 'Bad Request' OR P.contract = 'Wait Dummy Contract' OR P.contract LIKE '%Warning%' ) AND P.phase <> 'None Contract' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function updateDummyContract($project_sid, $dummyContract){
        $sql = "UPDATE project SET contract = :contract, updated_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract'=>$dummyContract,':sid'=>$project_sid));
    }

    public function getConfig(){
        $sql = "SELECT * FROM config ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r;
    }

    public function runExecuteRuleSla($prefix_table){
        $data = $this->getDataTableSla($prefix_table);
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        foreach ($data as $key => $value) {
            // echo "<pre>";
            // print_r($value);
            // echo "</pre>";
            foreach ($value['data'] as $k => $v) {
                $this->addRuleSla($prefix_table,$v);
            }
        }
    }
    private function addRuleSla($prefix_table, $data){
        $this->addRuleSlaContract($prefix_table, $data);
    }
    private function addRuleSlaContract($prefix_table, $data){
        $sql = "SELECT * FROM rule_sla_contract WHERE contract_no = :contract_no AND project = :project AND customer = :customer
        AND full_address = :full_address ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract_no'=>$data['contract_no'],':project'=>$data['project'],':customer'=>$data['customer'],':full_address'=>$data['end_user_address']));
        $r = $q->fetch();

        if(isset($r['sid']) && $r['sid']>0){
            $lastInsertId = $r['sid'];
            $this->addRuleSlaDetail($prefix_table, $data, $lastInsertId);
        }else{
             $sql = "INSERT INTO rule_sla_contract (contract_no, project, customer, for_prefix, full_address, create_datetime, end_user) VALUES (:contract_no, :project, :customer, :for_prefix, :full_address, NOW(),:end_user)";
            $q = $this->db->prepare($sql);
            $q->execute(array(':contract_no'=>$data['contract_no'],':project'=>$data['project'],':customer'=>$data['customer'],':for_prefix'=>$prefix_table,':full_address'=>$data['end_user_address'],':end_user'=>$data['end_user']));
            $lastInsertId = $this->db->lastInsertId();
            $this->addRuleSlaDetail($prefix_table, $data, $lastInsertId);
        }
    }
    private function addRuleSlaDetail($prefix_table, $data, $rule_sla_contract_sid){
        $sql = "SELECT * FROM rule_sla_detail WHERE rule_sla_contract_sid = :rule_sla_contract_sid AND sla_name = :sla_name";
        $q = $this->db->prepare($sql);
        $q->execute(array(':rule_sla_contract_sid'=>$rule_sla_contract_sid,':sla_name'=>$data['sla_name']));
        $r = $q->fetch();

        if(isset($r['sid']) && $r['sid']>0){
            if($data['urgency']!=""){
                $sql = "UPDATE rule_sla_detail SET rule_sla_contract_sid = :rule_sla_contract_sid, sla_name = :sla_name, ".strtolower($data['urgency'])." = :time ,unit_time = :unit, updated_datetime = NOW(), sla_name_remedy = :sla_name_remedy WHERE sid = :sid ";
                $q = $this->db->prepare($sql);
                $r = $q->execute(array(':rule_sla_contract_sid'=>$rule_sla_contract_sid,':sla_name'=>$data['sla_name'],':time'=>$data['time'],':unit'=>$data['unit'],':sla_name_remedy'=>$data['name'],':sid'=>$r['sid']));
                if(!$r){
                    echo "<pre>";
                    print_r($data);
                    echo "</pre>";
                    echo $rule_sla_contract_sid;
                    echo $sql;
                }
            }
        }else{
            if($data['urgency']!=""){
                $sql = "INSERT INTO rule_sla_detail (rule_sla_contract_sid, sla_name, ".strtolower($data['urgency']).", unit_time, created_datetime,sla_name_remedy) VALUES
                (:rule_sla_contract_sid,:sla_name,:time_urgency,:unit, NOW(),:sla_name_remedy) ";
                $q = $this->db->prepare($sql);
                $r = $q->execute(array(
                    ':rule_sla_contract_sid'=>$rule_sla_contract_sid,
                    ':sla_name'=>$data['sla_name'],
                    ':time_urgency'=>$data['time'],
                    ':unit'=>$data['unit'],
                    ':sla_name_remedy'=>$data['name']
                    )
                );
                if(!$r){
                    echo "<pre>";
                    print_r($data);
                    echo "</pre>";
                    echo $rule_sla_contract_sid;
                    echo $sql;
                }
            }
        }
    }
    private function getDataTableSla($prefix_table){
        $sql = "SELECT DISTINCT(contract_no) FROM ".$prefix_table."ticket_sla WHERE contract_no NOT LIKE '%No Information%' AND contract_no NOT LIKE '%Renewing%'
        AND contract_no NOT LIKE '%Expired%' AND contract_no NOT LIKE '%Per Call%' AND contract_no <> '' ORDER BY contract_no ASC LIMIT 0,1000000 ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {
            $r[$key]['data'] = $this->getDataTableSlaDetail($prefix_table, $value['contract_no']);
        }
        return $r;
    }
    private function getDataTableSlaDetail($prefix_table, $contract_no){
        $sql = "SELECT * FROM ".$prefix_table."ticket_sla WHERE contract_no = :contract_no";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract_no'=>$contract_no));
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {

            $dataTime = str_replace("Normal_Low_", "", $value['name']);
            $dataTime = str_replace("NSO_Response_Normal_", "Response_", $dataTime);
            $dataTime = str_replace("NSO_Onsite_Normal_", "Onsite_", $dataTime);
            $dataTime = str_replace("NSO_WA_Normal_", "WA_", $dataTime);
            $dataTime = str_replace("NSO_Resolution_Normal_", "Resolution_", $dataTime);
            $dataTime = str_replace("_2010", "", $dataTime);
            $dataTime = str_replace("_2013", "", $dataTime);
            $dataTime = str_replace("_2011", "", $dataTime);
            $dataTime = str_replace("_2012", "", $dataTime);
            $dataTime = str_replace("_2014", "", $dataTime);
            $dataTime = str_replace("_2015", "", $dataTime);
            $dataTime = str_replace("_2016", "", $dataTime);
            $dataTime = str_replace("_7x24", "", $dataTime);
            $dataTime = str_replace("_5x8", "", $dataTime);
            $dataTime = str_replace("_SLA8", "", $dataTime);
            $dataTime = str_replace("MVG_", "", $dataTime);
            $dataTime = str_replace("TCS_Normal_", "", $dataTime);
            $dataTime = str_replace("FIRSTSoftware_", "", $dataTime);
            $dataTime = str_replace("FIRST5x9_", "", $dataTime);
            $dataTime = str_replace("NSO_", "", $dataTime);
            $dataTime = str_replace("NamedAccount_", "", $dataTime);
            $dataTime = str_replace("Normal_", "", $dataTime);
            $dataTime = str_replace("_B", "", $dataTime);
            $dataTime = str_replace("_C", "", $dataTime);
            $dataTime = str_replace("_High", "", $dataTime);
            $dataTime = explode("_", $dataTime);
            $sla_name = $dataTime[count($dataTime)-2];
            $dataTime = $dataTime[count($dataTime)-1];

            $unit = "";
            if(strpos($dataTime, "min")>0){
                $dataTime = str_replace("min", "", $dataTime);
                $unit = "min";
            }else if(strpos($dataTime, "hr")>0){
                $dataTime = str_replace("hr", "", $dataTime);
                $unit = "hr";
            }else if(strpos($dataTime, "day")>0){
                $dataTime = str_replace("day", "", $dataTime);
                $unit = "day";
            }else if(strpos($dataTime, "NBD")>0){
                $dataTime = str_replace("NBD", "", $dataTime);
                $unit = "NBD";
            }
            $r[$key]['time'] = $dataTime;
            $r[$key]['unit'] = $unit;
            $r[$key]['sla_name'] = $sla_name;
        }
        return $r;
    }

    public function updateTableSlaUrgency($prefix_table){
        $data = $this->getDataUrgencyEmpty($prefix_table);
        foreach ($data as $key => $value) {
            $dataUrgency = $this->getDataUrgencyCase($prefix_table, $value);
            if($dataUrgency){
                $this->updateUrgencyTableSla($prefix_table, $value, $dataUrgency);
            }
        }
    }
    private function updateUrgencyTableSla($prefix_table, $data, $dataUrgency){
        $sql = "UPDATE ".$prefix_table."ticket_sla SET urgency = :urgency WHERE ticket_sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':urgency'=>$dataUrgency,':ticket_sid'=>$data['ticket_sid']));
    }
    private function getDataUrgencyCase($prefix_table, $data){
        $sql = "SELECT urgency FROM ".$prefix_table."ticket WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$data['ticket_sid']));
        $r = $q->fetch();
        return $r['urgency'];
    }

    private function getDataUrgencyEmpty($prefix_table){
        $sql = "SELECT * FROM ".$prefix_table."ticket_sla WHERE urgency = '' ORDER BY sid LIMIT 0,10";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    public function updateInfoTableSLA($prefix_table){
        $data = $this->getDataSlaProjectEmpty($prefix_table);

        foreach ($data as $key => $value) {
            $dataContract = $this->getDataContract($prefix_table,$value);
            if($dataContract){
                $this->updateProjectTableSla($prefix_table, $value, $dataContract);
            }
        }
    }
    private function updateProjectTableSla($prefix_table, $data, $dataContract){
        $sql = "UPDATE ".$prefix_table."ticket_sla SET project = :project, end_user = :end_user, customer = :customer, end_user_address = :end_user_address WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $project = $dataContract->PROJECT_NAME;
        $end_user = $dataContract->ENDUSER_NAME;
        $customer = $dataContract->CUSTOMER_NAME;
        $end_user_address = $dataContract->ENDUSER_ADDRESS;

        $q->execute(array(':project'=>$project,':end_user'=>$end_user,':customer'=>$customer,':end_user_address'=>$end_user_address, ':sid'=>$data['sid']));
    }

    private function getDataContract($prefix_table, $data){

        $url = "http://flgupload.com/contractInfoEbiz.php?contract=".$data['contract_no'];

        $dataContract = file_get_html($url)->plaintext;
        $res['data'] = json_decode($dataContract);
        if(isset($res['data'][0])){
            return $res['data'][0];
        }else{
            return false;
        }

    }
    public function updateContractTableSLA($prefix_table){

        $data = $this->getDataSlaContractEmpty($prefix_table);
        foreach ($data as $key => $value) {
            $contract_no = $this->getContractByTicketSid($prefix_table, $value);
            $this->updateContractToTableSla($prefix_table,$value['sid'],$contract_no);
        }
    }

    private function updateContractToTableSla($prefix_table, $ticket_sla_sid, $contract_no){
        $sql = "UPDATE ".$prefix_table."ticket_sla SET contract_no = :contract WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract'=>$contract_no,':sid'=>$ticket_sla_sid));

    }

    private function getContractByTicketSid($prefix_table, $data){
        $sql = "SELECT * FROM ".$prefix_table."ticket WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$data['ticket_sid']));
        $r = $q->fetch();
        return $r['contract_no'];
    }

    private function getDataSlaContractEmpty($prefix_table){
        $sql = "SELECT * FROM ".$prefix_table."ticket_sla WHERE contract_no = '' ORDER BY sid LIMIT 0,30 ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    private function getDataSlaProjectEmpty($prefix_table){

        $firstChar = "F";
        if($prefix_table==""){
            $firstChar = "F";
        }else if($prefix_table=="mvg_"){
            $firstChar = "M";
        }else if($prefix_table=="tcs_"){
            $firstChar = "T";
        }

        $sql = "SELECT * FROM ".$prefix_table."ticket_sla WHERE project = '' AND contract_no <> '' AND contract_no LIKE '".$firstChar."%' ORDER BY sid LIMIT 0,30 ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function checkStandby($prefix_table){
        $data = $this->listCaseBeforeCheckStandby($prefix_table);
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        if(count($data)>0){
            foreach ($data as $key => $value) {
                $this->executeCheckStandby($prefix_table, $value);
            }
        }else{
            return 0;
        }
    }

    private function executeCheckStandby($prefix_table, $data){
        // $data['create_datetime'];
        $sql = "SELECT * FROM tasks T
        WHERE T.appointment < :create_case
        AND DATE_ADD(T.appointment, INTERVAL T.expect_duration hour) > :create_case AND appointment_type = 2 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':create_case'=>$data['create_datetime']));
        $r = $q->fetchAll();
        print_r($r);
        $doChangeNextCall = 0;
        if(count($r)>0){
			foreach ($r as $key => $value) {
				if(!$doChangeNextCall){
					if($value['engineer']==$data['owner']){
						$this->changeNextCallStandby($prefix_table, $value['standby_buddy_sid']);
						$doChangeNextCall = 1;
					}
				}
			}
        }
        $this->changeStatusCaseToCheckedStandby($prefix_table, $data['sid']);
    }

    private function changeNextCallStandby($prefix_table, $buddy_no){
        $sql = "SELECT next_call FROM standby_buddy WHERE sid = :buddy_no";
        $q = $this->db->prepare($sql);
        $q->execute(array(':buddy_no'=>$buddy_no));
        $r = $q->fetch();
        $newNextCall = "1";
        if($r['next_call']=="1"){
            $newNextCall = "2";
        }
        $sql = "UPDATE standby_buddy SET next_call = :new_next_call, last_update_datetime = NOW() WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':new_next_call'=>$newNextCall,':sid'=>$buddy_no));
    }

    private function changeStatusCaseToCheckedStandby($prefix_table, $ticket_sid){
        $sql = "UPDATE ".$prefix_table."ticket SET check_standby = '1' WHERE sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
    }

    private function listCaseBeforeCheckStandby($prefix_table){
        $sql = "SELECT * FROM ticket WHERE check_standby = '0' ORDER BY create_datetime ASC";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $r = $q->fetchAll();
    }

    public function runAutoClosePm($prefix_table){
        $data = $this->listPmInProcess($prefix_table);
        $this->processUpdatePm($data, $prefix_table);
    }

    private function processUpdatePm($data,$prefix_table){
        foreach ($data as $key => $value) {
            if($value['customer_signated']=="1"){
                $this->updateStatusCase($prefix_table, $value['sid'], '5',$value['customer_signated']);
            }else{
                $this->updateStatusCase($prefix_table, $value['sid'], '2',$value['customer_signated']);
            }
        }
    }

    private function updateStatusCase($prefix_table, $sid, $status, $customer_signated){
        $sql = "UPDATE ".$prefix_table."ticket SET status = :status, updated_auto_closed_datetime = NOW() WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':status'=>$status,':sid'=>$sid));

        if($status=="5"){
            $worklog = "Auto Closed";
        }else{
            $worklog = "Auto Check Update stutus to Closed";
            if($customer_signated=="0"){
                $worklog .= ", This case have service report unsigned";
            }
        }
        $sql = "INSERT INTO ticket_status ( ticket_sid, case_status_sid, create_datetime, data_from, data_status, update_datetime, worklog, solution, solution_detail) VALUES (:ticket_sid, :case_status_sid, NOW(), :data_from, '1', NOW(), :worklog, :solution, :solution_detail)";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$sid,
            ':case_status_sid'=>$status,':data_from'=>"",':worklog'=>$worklog, ':solution'=>"", ':solution_detail'=>""
            )
        );
    }

    private function listPmInProcess($prefix_table){
        $sql = "SELECT T.sid sid, T.no_ticket,TT.customer_signated, TT.last_status FROM
            ".$prefix_table."ticket T
            INNER JOIN ".$prefix_table."tasks TT ON T.sid = TT.ticket_sid
            WHERE T.case_type = 'Preventive Maintenance' AND T.status <> '5'
            AND DATE_FORMAT(NOW(),'%Y-%m-%d') > DATE_FORMAT(T.updated_auto_closed_datetime,'%Y-%m-%d')
            AND TT.last_status > '300'
            ORDER BY T.sid ASC, TT.last_status DESC, TT.customer_signated DESC LIMIT 0,30";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $r = $q->fetchAll();
    }

 	public function updateCaseWorklog($prefix_table, $status){
       $sql = "update ".$prefix_table."ticket SET update_datetime = NOW() where status = :status AND refer_remedy_hd <> '' ";
       $q = $this->db->prepare($sql);
       $q->execute(array(':status'=>$status));
    }

    public function newTicket($prefix_table){
 		$sql = "SELECT T.*,(SELECT CS.name FROM case_status CS WHERE CS.sid = T.status LIMIT 0,1) status_name,
        (SELECT TS.worklog FROM ".$prefix_table."ticket_status TS WHERE T.sid = TS.ticket_sid ORDER BY TS.sid DESC LIMIT 0,1) worklog,
        E.engname
        FROM ".$prefix_table."ticket T
        LEFT JOIN ".$prefix_table."ticket_sla TSLA ON T.sid = TSLA.ticket_sid
        LEFT JOIN employee E ON T.owner = E.emailaddr
        WHERE LENGTH(T.refer_remedy_hd) = '8'

        AND T.case_type = 'Incident'
        AND T.sla_remedy NOT LIKE '%Entry does not exist in database%'
        AND (T.update_datetime > TSLA.updated_datetime OR TSLA.updated_datetime IS NULL) GROUP BY T.refer_remedy_hd LIMIT 0,10";
         // AND TSLA.name IS NULL
        // AND (T.status = '1' OR T.status = '2')
        // AND (T.status = '1' OR T.status = '2' OR T.status = '3' OR T.status = '4')
 		// $sql = "SELECT * FROM ".$table." WHERE refer_remedy_hd = 'HD212332' AND case_type = 'Incident'";
 		$q = $this->db->prepare($sql);
 		$q->execute();
 		$r = $q->fetchAll();
 		return $r;
 	}

    public function getTicket($prefix_table, $refer_remedy_hd){
        $sql = "SELECT T.*,(SELECT CS.name FROM case_status CS WHERE CS.sid = T.status LIMIT 0,1) status_name,
        (SELECT TS.worklog FROM ".$prefix_table."ticket_status TS WHERE T.sid = TS.ticket_sid ORDER BY TS.sid DESC LIMIT 0,1) worklog
        FROM ".$prefix_table."ticket T
        WHERE T.refer_remedy_hd = :refer_remedy_hd
        LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':refer_remedy_hd'=>$refer_remedy_hd));
        $r = $q->fetch();
        return $r;
    }

    public function firstDayDecember(){
        $sql = "SELECT * FROM tasks WHERE sid > '1626'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $engineer = $value['engineer'];
            $associate = $value['associate_engineer'];
            $associateArray = explode(",", $associate);

            for($i=0;$i<700;$i+=100) {
                $this->insertIntoTaskslog($engineer, $value,$i);
                if($associate!=""){
                    foreach ($associateArray as $k => $v) {
                        $this->insertIntoTaskslog($v,$value,$i);
                    }
                }
            }
        }
    }
    private function insertIntoTaskslog($email, $value, $status){
        // echo $email;
        // print_r($value);
        $sql = "INSERT INTO tasks_log
        (tasks_sid, engineer, create_by, create_datetime, datetime_to_report, appointment, expect_duration,expect_finish,status)
        VALUES
        (:tasks_sid, :engineer, :create_by, NOW(), NOW(), :appointment, :expect_duration,
        :expect_finish,:status)";

        $q = $this->db->prepare($sql);

        if($status<400){
            $create_datetime = $value['appointment'];
        }else{
            $create_datetime = $value['expect_finish'];
        }
        $q->execute(array(
            ':tasks_sid'=>$value['sid'],
            ':engineer'=>$email,
            ':create_by'=>$email,
            ':appointment'=>$value['appointment'],
            ':expect_duration'=>$value['expect_duration'],
            ':expect_finish'=>$value['expect_finish'],
            ':status'=>$status
            ));

    }

 	public function updateSLARemedy( $refer_remedy_hd , $stringSLA , $owner, $ticket_sid,$prefix){
        // echo $owner;
        if($owner!=""){
            $this->m->setTable($owner);
            $this->ticket_sid = $ticket_sid;

        	$sql = "UPDATE ".$prefix."ticket SET sla_remedy = :stringSLA, update_sla_remedy_datetime = NOW() WHERE refer_remedy_hd = :refer_remedy_hd";
        	$q = $this->db->prepare($sql);
        	$q->execute( array( ':stringSLA' => $stringSLA, ':refer_remedy_hd' => $refer_remedy_hd));

        	$slaRemedy = explode(",", $stringSLA);
            $slaRemedySubUse1 = array();
            foreach ($slaRemedy as $key => $value) {
                $slaSub = explode("||", $value);
                if(count($slaSub)>1){
                    $slaRemedySubUse2 = array();
                    foreach ($slaSub as $k => $v) {
                        $slaRemedySub = explode("=>", $v);
                        if($slaRemedySub[0]=="SLA_Name"){
                            $slaRemedySub[1] = str_replace("OneStop_","",$slaRemedySub[1]);
                            $slaRemedySub[1] = str_replace("_2012","",$slaRemedySub[1]);
                        }

                        if($slaRemedySub[0]=="SLA_Name"){
                            $slaRemedySub[1] = str_replace("FIRSTAccountOr7x24_","",$slaRemedySub[1]);
                            $slaRemedySub[1] = str_replace("_2011","",$slaRemedySub[1]);
                            $slaRemedySub[1] = str_replace("_2014","",$slaRemedySub[1]);
                        }

                        if($slaRemedySub[0]=="SLA_Due_Date"){
                            $slaDueDate = explode("T", $slaRemedySub[1]);
                            $datetime = explode("-", $slaDueDate[0]);
                            $datetime = ($datetime[0]-543)."/".$datetime[1]."/".$datetime[2];

                            $slaRemedySub[1] = $datetime." ".str_replace("+07:00", "", $slaDueDate[1]);
                        }
                        $slaRemedySubUse2[$k] =  array($slaRemedySub[0],$slaRemedySub[1]);
                    }

                    array_push(
                        $slaRemedySubUse1,
                        array(
                            $slaRemedySubUse2[0][0]=>$slaRemedySubUse2[0][1],
                            $slaRemedySubUse2[1][0]=>$slaRemedySubUse2[1][1],
                            $slaRemedySubUse2[2][0]=>$slaRemedySubUse2[2][1],
                            $slaRemedySubUse2[3][0]=>$slaRemedySubUse2[3][1]
                        )
                    );
                }
            }

            if(count($slaRemedySubUse1)>0){

                $this->array_sort_by_column($slaRemedySubUse1, 'SLA_Due_Date');

                $data['sla_remedy_use'] = array();
                $data['sla_remedy_use'] = $slaRemedySubUse1;
                if(count($data['sla_remedy_use'])>0){
	                $this->insertRequireSla($data,$refer_remedy_hd,$prefix);
	            }
            }
       	}
    }
    private function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
        	$sort_col[$key] = $row[$col];
        }
		array_multisort($sort_col, $dir, $arr);
    }

    private function insertRequireSla($data,$refer_remedy_hd,$prefix){

        // echo $refer_remedy_hd;
     //    echo "<br/>";
    	// echo "<pre>";
    	// print_r($data);
    	// echo "</pre>";

        $send = false;

        $sql = "DELETE FROM ticket_sla WHERE ticket_sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));


    	foreach ($data['sla_remedy_use'] as $key => $value) {
    		// echo $this->m->table_ticket_sla;

            $sql = "SELECT sid FROM ".$prefix."ticket_sla WHERE name = :name AND ticket_sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':name'=>$value['SLA_Name'],':ticket_sid'=>$this->ticket_sid));
            $r = $q->fetch();

            if(isset($r['sid']) && $r['sid']>0){
                $sql = "UPDATE ".$prefix."ticket_sla SET name = :name, due_datetime = :due_datetime, ticket_sid = :ticket_sid, status = :status, updated_datetime = NOW(), hd = :hd WHERE name = :name AND ticket_sid = :ticket_sid";
                $q = $this->db->prepare($sql);
                $r = $q->execute(array(':name'=>$value['SLA_Name'],':due_datetime'=>$value['SLA_Due_Date'],':ticket_sid'=>$this->ticket_sid,':status'=>$value['SLA_Status'],':hd'=>$refer_remedy_hd));
            }else{
                $sql = "INSERT INTO ".$prefix."ticket_sla (name, due_datetime, ticket_sid, status, uniq, created_datetime, updated_datetime,hd) VALUES (:name, :due_datetime, :ticket_sid, :status,:uniq, NOW(), NOW(), :hd)";
                $q = $this->db->prepare($sql);
                $r = $q->execute(array(':name'=>$value['SLA_Name'],':due_datetime'=>$value['SLA_Due_Date'],':ticket_sid'=>$this->ticket_sid,':status'=>$value['SLA_Status'],':uniq'=>$value['SLA_Name']."_".$this->ticket_sid,':hd'=>$refer_remedy_hd));
            }

            if($value['SLA_Status']=="In Process" && !$send){
                $sql = "UPDATE ".$prefix."ticket SET status = '2', update_datetime = NOW() WHERE refer_remedy_hd = :refer_remedy_hd ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':refer_remedy_hd'=>$refer_remedy_hd));

                // $this->sendNotify("autsakorn.t@firstlogic.co.th","Notify: ".$data['no_ticket']." ".$refer_remedy_hd." status is WIP");
                // $send = true;
            }else if($value['SLA_Status']=="Pending" && !$send){
                $sql = "UPDATE ".$prefix."ticket SET status = '4', update_datetime = NOW() WHERE refer_remedy_hd = :refer_remedy_hd ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':refer_remedy_hd'=>$refer_remedy_hd));

                // $this->sendNotify("autsakorn.t@firstlogic.co.th","Notify: ".$data['no_ticket']." ".$refer_remedy_hd." status is Pending");
                // $send = true;
            }
    	}
    }

    private function sendNotify($to,$message, $notification_sid=0){
        $this->datetime = date("d m Y H:i:s");
        $dataPost = array (
            'to' => $to,
            'message'=>$message,
            'notification_sid'=>$notification_sid
        );
        $dataPost = http_build_query($dataPost);
        $context_options = array (
            'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($dataPost) . "\r\n",
                'content' => $dataPost
            )
        );
        $context = stream_context_create($context_options);
        $url = "".HOST_NAME."/apis/v1/incident/sendNotification";
        if($message!=""){
            $html = file_get_html($url, false, $context)->plaintext;
        }
    }

    public function caseWaitSendToCCD(){
        // 2017-09-07T12:15:00+07:00
				// TSLA.diff_time_final
        $sql = "SELECT T.refer_remedy_hd, TS.*,
        TSLA.status, TSLA.sla_value, TSLA.sla_unit,
        DATE_FORMAT(TSLA.due_datetime, '%Y-%m-%dT%H:%i:%s+07:00') due_datetime,
        DATE_FORMAT(TSLA.case_create_datetime,'%Y-%m-%dT%H:%i:%s+07:00') case_create_datetime,
        DATE_FORMAT(TS.create_datetime,'%Y-%m-%dT%H:%i:%s+07:00') done_datetime,
        TIMESTAMPDIFF(SECOND, (SELECT ticket_sla.created_datetime FROM ticket_sla WHERE ticket_sla.ticket_sid = TS.ticket_sid ORDER BY ticket_sla.created_datetime ASC LIMIT 0,1), TS.create_datetime) AS diff_time_final
        FROM ticket_status TS
        LEFT JOIN ticket_sla TSLA ON TS.ticket_sid = TSLA.ticket_sid AND TS.case_status_sid = TSLA.case_status_sid
        INNER JOIN ticket T ON TS.ticket_sid = T.sid
        WHERE TS.data_from = '' AND TS.send_to_ccd = '0' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    private function selectStatusName($case_status_sid){
        $sql = "SELECT name_return_to_remedy FROM case_status WHERE sid = '".trim($case_status_sid)."'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if(isset($r['name_return_to_remedy']))
            return $r['name_return_to_remedy'];
        else
            return $case_status_sid;
    }

    private function selectReferRemedyHdFromTicketSid($ticket_sid){
        $sql = "SELECT T.refer_remedy_hd, T.team, GIT.name
        FROM ticket T LEFT JOIN gable_incharge_team GIT ON T.owner = GIT.email WHERE T.sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_sid));
        $r = $q->fetch();
        if(isset($r['refer_remedy_hd']) && $r['refer_remedy_hd']!=""){
            return $r;
        }else{
            return array('refer_remedy_hd'=>'','team'=>'','name'=>'');
        }
    }
    private function selectRemedyPassword($email){
        $sql = "SELECT remedy_password FROM user WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['remedy_password'];
    }

    private function selectEmpnoFromEmail($email){
        $sql = "SELECT empno FROM employee WHERE emailaddr = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['empno'];
    }

    private function getLast_Work_Info(){
        $sql = "SELECT worklog FROM ticket_status WHERE ticket_sid = :ticket_sid ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();
        if(isset($r['worklog']) && $r['worklog']!=""){
            return $r['worklog'];
        }
        return "";
    }
    private function getResolution(){
        $sql = "SELECT solution FROM ticket_status WHERE ticket_sid = :ticket_sid AND case_status_sid = 5 ORDER BY sid ASC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();
        if(isset($r['solution']) && $r['solution']!=""){
            return $r['solution'];
        }
        return "";
    }
    private function getPending_Finish(){
        $sql = "SELECT CASE WHEN expect_pending < NOW() THEN 0 ELSE 1 END result, DATE_FORMAT(expect_pending, '%Y-%m-%dT%H:%i:%s+07:00') expect_pending, DATE_FORMAT(NOW(), '%Y-%m-%dT%H:%i:%s+07:00') expect_pending_now FROM ticket_status WHERE ticket_sid = :ticket_sid AND case_status_sid = 4 ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();
        if(isset($r['expect_pending']) && $r['expect_pending']!=""){
            if($r['result']=="0"){
                return "";
            }else{
                return $r['expect_pending'];
            }
        }
        return "";
    }
    private function getOn_Site_Date(){
        $sql = "SELECT DATE_FORMAT(create_datetime, '%Y-%m-%dT%H:%i:%s+07:00') create_datetime FROM ticket_status WHERE ticket_sid = :ticket_sid AND case_status_sid = 7 ORDER BY sid ASC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();
        if(isset($r['create_datetime']) && $r['create_datetime']!=""){
            return $r['create_datetime'];
        }
        return "";
    }
    private function getWork_Around_Date(){
        $sql = "SELECT DATE_FORMAT(create_datetime, '%Y-%m-%dT%H:%i:%s+07:00') create_datetime FROM ticket_status WHERE ticket_sid = :ticket_sid AND case_status_sid = 3 ORDER BY sid ASC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();
        if(isset($r['create_datetime']) && $r['create_datetime']!=""){
            return $r['create_datetime'];
        }
        return "";
    }
    /// SEND SLA TO REMEDT //
    public function updateStatusCaseToRemedy($ticket_sid, $email, $work_log,$expect_pending, $case_status_sid, $solution="", $solution_detail="",$ticket_status_sid=0, $result_sla){

        echo "result_sla";
        echo "<pre>";
        print_r($result_sla);
        echo "</pre>";

        if(in_array($case_status_sid, array("1","2","4","5","7","8","10","11"))){

            $this->ticket_sid = $ticket_sid;
            $this->email = $email;
            $this->work_log = $work_log;

            if($expect_pending!="0000-00-00 00:00:00"){
                $this->expect_pending = str_replace(" ", "T", $expect_pending);
                $this->expect_pending .= "+07:00";
            }else{
                $this->expect_pending = "";
            }
            $this->status = $this->selectStatusName($case_status_sid);
            // echo $ticket_sid;
            // echo "<br/>";
            $selectReferRemedyHdFromTicketSid = $this->selectReferRemedyHdFromTicketSid($this->ticket_sid);

            echo "selectReferRemedyHdFromTicketSid";
            echo "<pre>";
            print_r($selectReferRemedyHdFromTicketSid);
            echo "</pre>";

            $this->refer_remedy_hd = $selectReferRemedyHdFromTicketSid['refer_remedy_hd'];
            $Assigned_Group = $selectReferRemedyHdFromTicketSid['team'];
            $Assignee = $selectReferRemedyHdFromTicketSid['name'];

            // echo "Remedy HD: ".strlen($this->refer_remedy_hd);
            // echo "<br/>";

            $remedy_password = "isylzjko";
            $empno = $this->selectEmpnoFromEmail($this->email);

            if($empno==""){
                // $remedy_password = "1234";
                $empno = "005686";
            }

            if(strlen($this->refer_remedy_hd)=='15'){
                require_once "GableWebservice.php";

                // 2           Response
                // 3           WorkAround
                // 4           Pending
                // 5           Resolution
                // 6
                // 7           Onsite
                // 8           Worklog
                // 9           Solution
                // 10          NoWorkAround
                // 11          NoOnsite
                if(in_array($case_status_sid, array("2", "3", "4", "5", "7", "10", "11"))){

                    if($case_status_sid!="4"){
                        $dataType['2'] = "Response";
                        $dataType['3'] = "WorkAround";
                        // $dataType['4'] = "Pending";
                        $dataType['5'] = "Resolution";
                        $dataType['7'] = "Onsite";
                        // $dataType['8'] = "Worklog";
                        $dataType['10'] = "WorkAround";
                        $dataType['11'] = "Onsite";

                        echo "refer_remedy_hd before send SLA: ".$this->refer_remedy_hd;

                        if($result_sla['status']!=""){
                            $dataReturn = updateSlaGable(
                                $empno,
                                $remedy_password,
                                $this->refer_remedy_hd,
                                $dataType[$case_status_sid],
                                $result_sla['status'],
                                $result_sla['sla_value']." ".$result_sla['sla_unit'],
                                $result_sla['due_datetime'],
                                $result_sla['case_create_datetime'],
                                $result_sla['done_datetime'],
                                intval($result_sla['diff_time_final']),
                                intval($result_sla['diff_time_final'])
                                );
                            $resultSendSLA = isset($dataReturn->Request_ID__c)?$dataReturn->Request_ID__c:$dataReturn;

                            $this->insertToLogResponseRemedy(
                                "Request_ID__c ".$resultSendSLA, ($result_sla['status'])?$result_sla['status']:"", $solution_detail);
                        }

                    }


                    if($result_sla['is_return_remedy']=="1"){
                        $updateCaseGable['2'] = 'In Progress';
                        $updateCaseGable['3'] = 'In Progress';
                        $updateCaseGable['7'] = 'In Progress';
                        $updateCaseGable['10'] = 'In Progress';
                        $updateCaseGable['11'] = 'In Progress';
                        $updateCaseGable['4'] = "Pending";
                        $updateCaseGable['5'] = "Resolved";
                        $statusReason['2'] = '';
                        $statusReason['3'] = '';
                        $statusReason['7'] = '';
                        $statusReason['10'] = '';
                        $statusReason['11'] = '';
                        $statusReason['4'] = 'Wait for Customer Readiness';
                        $statusReason['5'] = 'Closed by Workaround/Solution';

                        $Last_Work_Info = $this->getLast_Work_Info();
                        $Resolution = "";
                        if($case_status_sid=="5"){
                            $Resolution = ($this->getResolution())?$this->getResolution():$Last_Work_Info;
                        }

                        $Pending_Finish = $this->getPending_Finish();
                        $On_Site_Date = $this->getOn_Site_Date();
                        $Work_Around_Date = $this->getWork_Around_Date();

                        $dataReturn = updateCaseGable(
                            $empno, $remedy_password, $this->refer_remedy_hd,
                            $updateCaseGable[$case_status_sid],
                            $statusReason[$case_status_sid],
                            $Assigned_Group,
                            $Assignee,
                            $Last_Work_Info,
                            $Resolution,
                            $Pending_Finish,
                            $On_Site_Date,
                            $Work_Around_Date
                        );
                        // print_r($dataReturn);
                        $resultSendModifiedCaes = (isset($dataReturn->Last_Modified_Date)?$dataReturn->Last_Modified_Date:$dataReturn);
                        $this->insertToLogResponseRemedy("Last_Modified_Date ".$resultSendModifiedCaes,
                            $updateCaseGable[$case_status_sid]." ".$statusReason[$case_status_sid]." ".$Assigned_Group." ".$Assignee." ".$Last_Work_Info." ".$Resolution." ".$Pending_Finish." ".$On_Site_Date." ".$Work_Around_Date
                            , $solution_detail);
                    }

                }

                if($result_sla['is_return_remedy']=="1"){
                    $dataReturn = updateWorklogGable($empno,$remedy_password, $this->refer_remedy_hd, $this->work_log, "General Information", "Yes", "Internal", "Update from vSpace", "Enabled");
                    $resultSendWl = isset($dataReturn->Work_Log_ID)?$dataReturn->Work_Log_ID:$dataReturn;
                    $this->insertToLogResponseRemedy($resultSendWl, $solution, $solution_detail);
                }

                // if($resultSendWl!=""){
                $this->updateTicketStatusToSendToCcd($ticket_status_sid);
                // }

            }else if($this->refer_remedy_hd==""){
                $this->updateTicketStatusToSendToCcd2($ticket_status_sid);
            }
        }
    }

    private function insertToLogResponseRemedy($response_data, $solution, $solution_detail){
        $sql = "INSERT INTO response_remedy (response_data,create_datetime,other, create_by, stamp_sla, worklog, solution, solution_detail, expect_pending_finish) VALUES (:response_data, NOW(), :other, :create_by, :stamp_sla, :worklog, :solution, :solution_detail, :expect_pending_finish) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':response_data'=>$response_data,':other'=>$this->refer_remedy_hd,
                ':create_by'=>$this->email,':stamp_sla'=>$this->status, ':worklog'=>$this->work_log,
                ':solution'=>$solution, ':solution_detail'=>$solution_detail,':expect_pending_finish'=>$this->expect_pending
            ));
    }
    private function updateTicketStatusToSendToCcd($ticket_status_sid){

        $sql = "UPDATE ticket_status SET send_to_ccd = '1', update_datetime = NOW() WHERE sid = :ticket_status_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_status_sid'=>$ticket_status_sid));
    }
    private function updateTicketStatusToSendToCcd2($ticket_status_sid){
        $sql = "UPDATE ticket_status SET send_to_ccd = '1', update_datetime = NOW(), update_by = 'background process' WHERE sid = :ticket_status_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_status_sid'=>$ticket_status_sid));
    }

    public function listStandbyOtherProduct(){
        $sql = "SELECT * FROM standby_oncall_other_products WHERE standby_date >= '2017-01-01' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    public function insertToTableTasks($value){
        // print_r($value);
        $email = $value['email'];
        echo $email."<br/>";
        $start = $value['standby_date']." ".$value['time_start'];
        echo $start."<br/>";
        $end = $value['standby_date']." ".$value['time_end'];
        echo $end."<br/>";
        $sql = "SELECT TIMESTAMPDIFF(HOUR,'".$start."',DATE_ADD('".$end."',INTERVAL 1 minute) ) hours ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        $hours = $r['hours'];

        $sql = "INSERT INTO tasks (subject_service_report,appointment, expect_duration, engineer, create_datetime, create_by) VALUES (:subject_service_report,:appointment, :expect_duration, :engineer, NOW(), :create_by) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':subject_service_report'=>'Standby Other Product',':appointment'=>$start,':expect_duration'=>$hours,':engineer'=>$email,':create_by'=>'autsakorn.t@firstlogic.co.th'));

    }
}?>
