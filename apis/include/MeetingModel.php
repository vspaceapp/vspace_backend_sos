<?php

class MeetingModel{

    private $db, $dbObj;

    // FOR OLD DATABASE
    // private $tbl_meeting = "meeting";
    // private $tbl_service_report = "tbl_service_report";
  //   private $tbl_busy_employee_doc = "tbl_busy_employee_doc";
  //   private $tbl_customer_company = "tbl_customer_company";
  //   private $tbl_associate_engineer = "tbl_associate_engineer";
  //   private $tbl_busy_mlookup = "tbl_busy_mlookup";

    private $instructor;
    private $meeting_start;
    private $meeting_end;
    private $remark;
    private $status;
    private $join_time;

    private $switch_call,$continue_holiday;
    public $permission_role;
    private $email;

    function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->dbObj = $db = new DbConnect();
        $this->db = $db->connect();
        $this->email = $email;
        if($this->email!=""){
            $this->permission_role = $this->dbObj->permissionRoleByEmail($this->email); 
        }
    }

    // public function addMeeting($email, $subject, $type, $meeting_date, $room){
       
    //     $sql = "INSERT INTO meeting( subject, location, type, instructor, meeting_date, meeting_start, meeting_end, remark, status, create_by, create_datetime) VALUES ( :subject, :location, :type, :instructor, :meeting_date, :meeting_start, :meeting_end, :remark, :status, :create_by, NOW()";
    //     $q = $this->db->prepare($sql);
    //     $q->execute(array(
    //              ':subject' => $subject, 
    //              ':location' => $room, 
    //              ':type' => $type, 
    //              ':instructor' => $instructor, 
    //              ':meeting_date' => $meeting_date, 
    //              ':meeting_start' => $meeting_start, 
    //              ':meeting_end' => $meeting_end, 
    //              ':remark' => $remark, 
    //              ':status' => $status, 
    //              ':create_by' => $email
    //         ));
        
    // }

    public function insertTypeMeeting($email, $type_meeting, $start_time, $end_time, $invite_meeting){
        $sql = " INSERT INTO meeting_type( meeting_name, start_meeting, end_meeting, create_by, create_datetime) 
               VALUES ( :type_meeting, :start_time, :end_time, :create_by , NOW())";   
        $q = $this->db->prepare($sql);
        $q->execute(array(
                 ':type_meeting' => $type_meeting, 
                 ':start_time' => $start_time, 
                 ':end_time' => $end_time, 
                 ':create_by' => $email
            ));
        $lastInsertId = $this->db->lastInsertId();

        $data = $this->addAttendanceMeeting($email, $lastInsertId , $invite_meeting );

        return $sql;
    }

    private function addAttendanceMeeting($email, $lastInsertId, $invite_meeting){
        // $data = array();
        foreach ($invite_meeting as $key => $value) {
            $sql = "INSERT INTO meeting_type_join( meeting_sid, email, status, join_time, remark, create_by, create_datetime, update_datetime) 
                    VALUES ( :meeting_sid,:email, '1', '','', :create_by , NOW(), '' )";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                    ':meeting_sid' => $lastInsertId,
                    ':email' => $value,
                    ':create_by' => $email
                ));
        }

        
        
    }

    public function loadTypeMeeting(){
        $sql = "SELECT * FROM meeting_type ";   
        
        $q = $this->db->prepare($sql);
        $q->execute();
        $data = $q->fetchAll();

        foreach ($data as $key => $value) {
            $data[$key]['invite_meeting'] = $this->getConfigInviteMeeting($value['sid']);
        }
         return $data;
    }

    private function getConfigInviteMeeting( $type_meeting_sid ){
        $sql = "SELECT * FROM meeting_type_join WHERE meeting_sid = :type_meeting_sid ";
        $q = $this->db->prepare($sql);
        $q->execute( array(
                ':type_meeting_sid' => $type_meeting_sid
            ));
        $data = $q->fetchAll();
        return $data;
    }


    public function selectEmployee($email){
      $sql = "SELECT * FROM employee WHERE compid = (SELECT compid FROM employee WHERE emailaddr = :email) AND unitlev3 = (SELECT unitlev3 FROM employee WHERE emailaddr = :email) ";
      $q = $this->db->prepare($sql);
      $q->execute(array(':email'=>$email));
      $r = $q->fetchAll();
      return $r;

    }


}
?>