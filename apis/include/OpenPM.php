<?php
        function saveTicketSerial($ticket_sid, $serial, $db){
            foreach ($serial as $key => $value) {
                $sql = "INSERT INTO ticket_serial (ticket_sid, serial_detail, create_datetime, create_by) 
                        VALUES (:ticket_sid, :serial_detail, NOW(), :create_by) ";

                $q = $db->prepare($sql);
                $q->execute(array(':ticket_sid'=>$ticket_sid,':serial_detail'=>$value,':create_by'=>'sirirat.c@firstlogic.co.th'));
            }
        }

        function convertDate($datetime){
            
            if(substr($datetime,2,1)=="/"){
                $datetime = explode(" ", $datetime);
                $date = explode("/", $datetime[0]);
                return $date[2]."-".$date[1]."-".$date[0]." ".$datetime[1];
            }else{
                return $datetime;
            }
        }

        function saveTasks($ticketSid, $owner, $appointment, $db, $pm){
            $task_no = genTaskNo($db);
            $sql = "INSERT INTO tasks (no_task,ticket_sid,create_datetime) 
                    VALUES (:no_task,:ticket_sid,NOW()) ";
            $data = array(':no_task'=>$task_no,':ticket_sid'=>$ticketSid);
            saveTaskToDoPM($ticketSid, $db, $pm);///////// (5)
            
            $tasksSid = executeInsert($data, $db, $sql);
            saveTasksLog($tasksSid, $owner, $appointment, $db);
            
        }

        function saveTasksManual($ticketSid, $owner, $appointment, $db, $pm){
            $task_no = genTaskNo($db);
            $sql = "INSERT INTO tasks (no_task,ticket_sid,appointment,create_datetime, service_type) 
                    VALUES (:no_task,:ticket_sid,:appointment,NOW(),'1') ";
            $data = array(':no_task'=>$task_no,':ticket_sid'=>$ticketSid,':appointment'=>$appointment);
            if(count($pm)>0){
                saveTaskToDoPM($ticketSid, $db, $pm);///////// (5)
            }
            
            $tasksSid = executeInsert($data, $db, $sql);
            foreach ($owner as $key => $value) {
                saveTasksLog($tasksSid, $value, $appointment, $db);
            }
        }

        function saveTasksLog($tasksSid, $owner, $appointment, $db){
            $sql = "INSERT INTO tasks_log (tasks_sid,engineer,create_by,create_datetime,appointment,expect_finish,status) 
                    VALUES (:tasks_sid,:engineer,:create_by,NOW(),:appointment,:expect_finish,:status) ";
            $data = array(':tasks_sid'=>$tasksSid,':engineer'=>$owner,
                            ':create_by'=>"sirirat.c@firstlogic.co.th",
                            ':appointment'=>$appointment,
                            ':expect_finish'=>$appointment,
                            ':status'=>0);
            executeInsert($data,$db,$sql);
        }


    function saveTicket($param, $db){
        $sql = "INSERT INTO ticket 
                (no_ticket,via,create_datetime,create_by,receive_mail_datetime) 
                VALUES 
                (:no_ticket,:via,NOW(),:create_by,:receive_mail_datetime) ";
        
        return executeInsert($param, $db, $sql);
    }

    function saveTicketManual($param, $db){
        $sql = "INSERT INTO ticket 
                (no_ticket,via,create_datetime,create_by,receive_mail_datetime,case_type,subject,description,contract_no,prime_contract,project_name,serial_no,team,requester_full_name,requester_company_name,requester_phone,requester_mobile,requester_email,
                end_user_company_name,end_user_site,end_user_contact_name,end_user_phone,end_user_mobile,end_user_email) 
                VALUES 
                (:no_ticket,:via,NOW(),:create_by,NOW(),'Preventive Maintenance',:subject,:description,:contract_no,:prime_contract,:project_name,:serial_no,
                :team,:requester_full_name,:requester_company_name,:requester_phone,:requester_mobile,:requester_email,
                :end_user_company_name,:end_user_site,:end_user_contact_name,:end_user_phone,:end_user_mobile,:end_user_email) ";
        
        return executeInsert($param, $db, $sql);
    }

    function saveTicketLog($param, $db){

        $sql = "INSERT INTO ticket_log 
                (ticket_sid,owner,status,subject,description,contract_no,prime_contract,project_name,serial_no,
                team,requester_full_name,requester_company_name,requester_phone,requester_mobile,requester_email,
                end_user_company_name,end_user_site,end_user_contact_name,end_user_phone,end_user_mobile,end_user_email,
                case_type,source,note,urgency,create_datetime,create_by,due_datetime) 
                VALUES 
                (:ticket_sid,:owner,:status,:subject,:description,:contract_no,:prime_contract,:project_name,:serial_no,
                :team,:requester_full_name,:requester_company_name,:requester_phone,:requester_mobile,:requester_email,
                :end_user_company_name,:end_user_site,:end_user_contact_name,:end_user_phone,:end_user_mobile,:end_user_email,
                :case_type,:source,:note,:urgency,NOW(),:create_by,:due_datetime) ";
        
        return executeInsert($param, $db, $sql);
    }

    function saveTaskToDoPM($ticket_sid, $db, $pm){

        if(isset($pm['pmNetBu1']) && isset($pm['pmNetBu2']) && isset($pm['pmNetBu3']) && isset($pm['pmNetBu4'])){
            $sql = "INSERT INTO ticket_to_do_pm (create_by, create_datetime, ticket_sid,
                                 to_do_name_1, to_do_name_2, to_do_name_3, to_do_name_4,to_do_1, to_do_2, to_do_3, to_do_4)  
                            VALUES ('sirirat.c@firstlogic.co.th', NOW(), :ticket_sid, :name1, :name2,:name3,:name4,:do1,:do2,:do3,:do4); ";
                
            if($pm['pm1']['value']=="do"){
                $param = array(
                    ':ticket_sid'=>$ticket_sid,
                    ':name1'=>$pm['pm1']['name'],
                    ':name2'=>$pm['pm2']['name'],
                    ':name3'=>$pm['pm3']['name'],
                    ':name4'=>$pm['pm4']['name'],
                    ':do1'=>$pm['pm1']['value'],
                    ':do2'=>$pm['pm2']['value'],
                    ':do3'=>$pm['pm3']['value'],
                    ':do4'=>$pm['pm4']['value'],
                    );
            }else{
                $param = array(
                    ':ticket_sid'=>$ticket_sid,
                    ':name1'=>$pm['pmNetBu1']['name'],
                    ':name2'=>$pm['pmNetBu2']['name'],
                    ':name3'=>$pm['pmNetBu3']['name'],
                    ':name4'=>$pm['pmNetBu4']['name'],
                    ':do1'=>$pm['pmNetBu1']['value'],
                    ':do2'=>$pm['pmNetBu2']['value'],
                    ':do3'=>$pm['pmNetBu3']['value'],
                    ':do4'=>$pm['pmNetBu4']['value'],
                    );
            }
            return executeInsert($param, $db, $sql);
        }
    }

    function genTicketNo($db) {
        $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
        $queryY = $db->prepare($sql);
        $queryY->execute();

        $rY = $queryY->fetch(PDO::FETCH_ASSOC);
        $year = $rY['year'];

        $sql = "SELECT no_ticket 
            FROM ticket WHERE substring(no_ticket,1,4) = :initF ORDER BY no_ticket DESC LIMIT 0,1 ";
        $query = $db->prepare($sql);
        $query->execute(array(':initF'=>('HD'.$year)));
        $r = $query->fetch(PDO::FETCH_ASSOC);

        if ($r['no_ticket'] != "") {
            $no_ticket = substr($r['no_ticket'], 4);
            $no_ticket++;
        } else {
            $no_ticket = "000001";
        }
        $cLength = strlen($no_ticket);
        for ($i = $cLength; $i < 6; $i++) {
            $no_ticket = "0" . $no_ticket;
        }
        return "HD" .$year. $no_ticket;
    }

    function genTaskNo($db) {
        $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
        $queryY = $db->prepare($sql);
        $queryY->execute();

        $rY = $queryY->fetch(PDO::FETCH_ASSOC);
        $year = $rY['year'];

        $sql = "SELECT no_task 
            FROM tasks WHERE substring(no_task,1,4) = :initF ORDER BY no_task DESC LIMIT 0,1 ";
        $query = $db->prepare($sql);
        $query->execute(array(':initF'=>('SR'.$year)));

        // echo helper::debugPDO($sql, array(':initF'=>'VS'.$year));
        $r = $query->fetch(PDO::FETCH_ASSOC);
        
        if ($r['no_task'] != "") {
            $no_task = substr($r['no_task'], 4);
            $no_task++;
        } else {
            $no_task = "000001";
        }
        $cLength = strlen($no_task);
        for ($i = $cLength; $i < 6; $i++) {
            $no_task = "0" . $no_task;
        }
        return "SR".$year . $no_task;
    }


    function convertNametoEmail($incharge, $db) {
            $name = explode("(", strtolower($incharge));
            $sql = "SELECT emailaddr FROM employee WHERE emailaddr LIKE '" . trim($name[0]) . "%' ";

            
            $q = $db->prepare($sql);
            $q->execute();
            $result =$q->fetch();
            
            if ($result['emailaddr'] != "") {
                return $result['emailaddr'];
            } else {
                return $incharge;
            }
    }


    function executeInsert($data, $db, $sql){
        try {
            $q = $db->prepare($sql);

            // $db->beginTransaction(); 
            $q->execute($data);
            $lastInsertId = $db->lastInsertId(); 
            // $db->commit(); 

            return $lastInsertId;
        } catch(PDOExecption $e) { 
            $this->db->rollback(); 
            print "Error!: " . $e->getMessage() . "</br>"; 
        } 
    }

    function vPost($data){
        return str_replace("<br>", "", trim($data));
        // return htmlspecialchars_decode(trim($data));
        
    }

    function vPostText($data){
        return htmlentities($data, ENT_QUOTES, "UTF-8");
        // return nl2br(htmlentities(trim(addslashes($data))));
        // return htmlspecialchars_decode(trim(addslashes($data)));
        // return addslashes($data);
    }

        function prepareData($data, $date) {
            $data = str_replace("= &gt;", " to ", $data);
            $s = explode("&gt;", $data);
            foreach ($s as $k => $v) {
                if ($k % 2 == 0) {
                    $index = explode("&lt;", $v);
                } else {
                    $value = explode("&lt;", $v);
                }
                if ($index[1] == 'Appointed_time' || $index[1] == 'Create_Time') {
                    $value[0] = convertDatetime($value[0]);
                }
                $result[$index[1]] = $value[0];
                $result['receive_mail_datetime'] = convertReceiveDatetime($date);
            }
           // echo $data;
           echo "<pre>";
           print_r($result);
           echo "</pre>";
            insertToCasesDB($result, $data);
        }

        function insertToCasesDB($result, $raw) {
            $sql = "SELECT sid,no_task FROM tasks "
                    . "WHERE no_task = '" . trim($result['Service_Report_ID']) . "'";
            $q = mysql_query($sql);
            $r = mysql_fetch_assoc($q);
            if ($r['sid'] == "") {
                $sql = "INSERT INTO tasks "
                        . "( no_task, receive_mail_datetime) "
                        . "VALUES "
                        . "('" . trim($result['Service_Report_ID']) . "','" . $result['receive_mail_datetime'] . "')";
                echo mysql_query($sql);

                $sql = "SELECT sid FROM tasks "
                        . "WHERE no_task = '" . trim($result['Service_Report_ID']) . "'";
                $q = mysql_query($sql);
                $lastID = mysql_fetch_assoc($q);

                $service_report_id = $lastID['sid'];
                if ($service_report_id != "") {
                    echo $service_report_id;
                    insertDetail($service_report_id, $result);
                }
            } else {
                    
                    $service_report_id = $r['sid'];

                    if ($service_report_id != "") {
                        insertDetail($service_report_id, $result);
                    }
            }
        }

        function updateDetail($service_report_id, $result) {
            if ($service_report_id != "") {
                $sql = "UPDATE tasks_log SET "
                        . "subject = '" . trim(addslashes($result ['Symtom'])) . "' ,"
                        . "contract = '" . trim(addslashes($result ['Contract_No'])) . "',"
                        . "enduser_company = '" . trim(addslashes($result['End_User_Company'])) . "',"
                        . "enduser_address = '" . trim(addslashes($result['End_User_Site'])) . "',"
                        . "enduser_name = '" . trim(addslashes($result['End_User_Name'])) . "',"
                        . "enduser_phone = '" . trim(addslashes($result['End_User_Phone'])) . "',"
                        . "appointed = '" . trim(addslashes($result ['Appointed_time'])) . "',"
                        . "service_type = '" . trim(addslashes($result ['Service_Type'])) . "',"
                        . "expect_duration = '" . $result['Expect_Finish'] . ":00:00" . "',"
                        . "create_by = '" . $result['Submitted_by'] . "',"
                        . "serial = '" . trim(addslashes($result ['Serial'])) . "',"
                        . "detail = '" . trim(addslashes($result ['Detail'])) . "',"
                        . "step = '0' ,"
                        . "create_datetime = NOW() "
                        . "WHERE tasks_sid = '" . $service_report_id . "' AND step = '0' ";

                echo mysql_query($sql);
                insertServiceReportEngineerRaw($service_report_id, $result);
            }
        }

        function insertDetail($service_report_id, $result) {
            if ($service_report_id != "") {
              $sql = "INSERT INTO tasks_log "
                        . "(tasks_sid,froms,subject,hd,contract,"
                        . "enduser_company,enduser_address,enduser_name,enduser_phone,"
                        . "appointment,service_type,"
                        . "expect_duration,create_by,"
                        . "serial,detail,status,create_datetime) "
                        . "VALUES "
                        . "('" . $service_report_id . "','Remedy','" . trim(addslashes($result ['Symtom'])) . "','" . trim(addslashes($result ['Request_ID'])) . "','" . trim(addslashes($result ['Contract_No'])) . "',"
                         ."'" . trim(addslashes($result['End_User_Company'])) . "','" . trim(addslashes($result['End_User_Site'])) . "','" . trim(addslashes($result['End_User_Name'])) . "','" . trim(addslashes($result['End_User_Phone'])) . "',"
                        . "'" . trim(addslashes($result ['Appointed_time'])) . "','" . trim(addslashes($result ['Service_Type'])) . "',"
                        . "'" . $result['Expect_Finish'] . "', '" . getEmailFromEmpno($result['Submitted_by']) . "', "
                        . "'" . trim(addslashes($result ['Serial'])) . "', '" . trim(addslashes($result ['Detail'])) . "', 1, NOW())";
                echo mysql_query($sql);

                insertServiceReportEngineerRaw($service_report_id, $result);
            }
        }

        function insertServiceReportEngineerRaw($service_report_id, $result) {
            // $sql = "DELETE FROM tbl_srs_service_report_engineer_remedy "
            //         . "WHERE service_report_id = '" . $service_report_id . "'";
            // mysql_query($sql);
            if (trim($result['Assign_individual']) != "") {
                $engineerName = trim($result['Assign_individual']);

                insertServiceReportEngineer($service_report_id, $engineerName);
            }
            if (trim($result['Involved_Eng2']) != "") {
                $engineerName = trim($result['Involved_Eng2']);

                insertServiceReportEngineer($service_report_id, $engineerName);
            }
            if (trim($result['Involved_Eng3']) != "") {
                $engineerName = trim($result['Involved_Eng3']);

                insertServiceReportEngineer($service_report_id, $engineerName);
            }
            if (trim($result['Involved_Eng4']) != "") {
                $engineerName = trim($result['Involved_Eng4']);

                insertServiceReportEngineer($service_report_id, $engineerName);
            }
            if (trim($result['Involved_Eng5']) != "") {
                $engineerName = trim($result['Involved_Eng5']);

                insertServiceReportEngineer($service_report_id, $engineerName);
            }
        }

        function insertServiceReportEngineer($service_report_id, $engineerName) {
            $sql = "INSERT INTO tasks_engineer "
                    . " ( tasks_sid, engineer_email, status, create_datetime, create_by) "
                    . "VALUES "
                    . " ( '" . $service_report_id . "', "
                    . "'" . convertNametoEmail(trim($engineerName)) . "', '1', NOW(), 'System'  )";
            echo mysql_query($sql);
        }
        

        function getEmailFromEmpno($empno){
            $sql = "SELECT emailaddr FROM employee WHERE empno = '".$empno."' ";
            $q = mysql_query($sql);
            $result = mysql_fetch_assoc($q);
            if($result['emailaddr']!=""){
                return $result['emailaddr'];
            }else{
                return $empno;
            }
        }

        function convertDatetime($datetime) {
            $datetime = explode(" ", $datetime);
            $date = explode("/", $datetime[0]);
            $month = (strlen($date[1]) == '1') ? "0" . $date[1] : $date[1];
            $day = (strlen($date[0]) == '1') ? "0" . $date[0] : $date[0];
            return ( $date [2] - 543 ) . "-" . $month . "-" . $day . " "
                    . $datetime[1];
        }

        function convertReceiveDatetime($date) {
            $date = explode(", ", $date);
            $date = explode(" +", $date[1]);
            $date = explode(" ", $date[0]);
//            Thu, 04 Dec 2014 03:14:15 +0700 (GMT+07:00) 
            return $date [2] . "-" . convertMonth($date [1]) . "-" . $date[0] . " " . $date[3];
        }

        function convertMonth($month) {
            if ($month == "Jan") {
                return "01";
            } else if ($month == "Feb") {
                return "02";
            } else if ($month == "Mar") {
                return "03";
            } else if ($month == "Apr") {
                return "04";
            } else if ($month == "May") {
                return "05";
            } else if ($month == "Jun") {
                return "06";
            } else if ($month == "Jul") {
                return "07";
            } else if ($month == "Aug") {
                return "08";
            } else if ($month == "Sep") {
                return "09";
            } else if ($month == "Oct") {
                return "10";
            } else if ($month == "Nov") {
                return "11";
            } else if ($month == "Dec") {
                return "12";
            } else {
                return "";
            }
        }

?>