<?php
require_once __dir__."/../core/constant.php";
require_once __dir__."/../core/config.php";


class customer_satisfactionModel {
  private $objCons;
  private $db, $email;
  private $ticket_sid, $subject, $assessor_email, $assessment_status,$requested_amount ,
          $requester_name,  $assessment_datetime,  $satisfaction_result,$record_status;

  function __construct($email="") {
      require_once dirname(__FILE__) . '/db_connect.php';
      $this->objCons = new DbConnect();
      $this->db = $this->objCons->connect();
      $this->email = $email;

  }

  public function genURL($email,$ticket_sid,$evaluate_by){
    $strURL = HOST_NAME.'/satisfaction/index.html?email='.$email.'&ticket_sid='.$ticket_sid.'&evaluate_by='.$evaluate_by;
    return $strURL;
  }

  public function getTicketInfoByTicketsid($ticket_sid){
    $sql = "SELECT refer_remedy_hd,subject,owner FROM ticket WHERE sid = :ticket_sid";
    $q = $this->db->prepare($sql);
    $q->execute(array(':ticket_sid'=>$ticket_sid));
    $r = $q->fetch();
    if (!empty($r) && $r != null) {
      return $r;
    }else {
      return 'Not Found Data';
    }
  }

  public function checkEnableButton($ticket_sid){
    $sql = "SELECT ticket_sid FROM customer_satisfaction WHERE ticket_sid = :ticket_sid";
    $q = $this->db->prepare($sql);
    $q->execute(array(':ticket_sid'=>$ticket_sid));
    $r = $q->fetch();
    if (!empty($r) && $r != null) {
      return 'true';
    }else {
      return 'false';
    }
  }

  public function insertCussatHeader($ticket_sid,$evaluate_by,$email,$satisfaction_result,$type){
    try {
      $sql = "INSERT INTO customer_satisfaction (ticket_sid,evaluate_by,engineer,create_datetime,satisfaction_result,type) VALUES (:ticket_sid,:evaluate_by,:email,NOW(),:satisfaction_result,:type) ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
          ':ticket_sid'=>$ticket_sid,
          ':evaluate_by' => $evaluate_by,
          ':email' => $email,
          ':satisfaction_result' => $satisfaction_result,
          ':type' => $type
        ));
        return 'Insert Completed';
    } catch (\Exception $e) {
      return 'Insert Failed';
    }
  }

  public function insertCussatDetail($ticket_sid,$answer_satisfaction_sid,$note=null){
    try {
      $sql = "INSERT INTO customer_satisfaction_detail (ticket_sid,answer_satisfaction_sid,note,create_datetime) VALUES (:ticket_sid,:answer_satisfaction_sid,:note,NOW()) ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
          ':ticket_sid'=>$ticket_sid,
          ':answer_satisfaction_sid' => $answer_satisfaction_sid,
          ':note' => $note,
        ));
        return 'Insert Completed';
    } catch (\Exception $e) {
      return 'Insert Failed';
    }
  }

  public function countSendCussat($ticket_sid){
    $sql = "SELECT COUNT(ticket_sid) as amount FROM log_send_cussat WHERE ticket_sid = :ticket_sid";
    $q = $this->db->prepare($sql);
    $q->execute(array(':ticket_sid'=>$ticket_sid));
    $r = $q->fetch();
    if (!empty($r) && $r != null) {
      return $r['amount'];
    }else {
      return 0;
    }
  }

  public function insertLogSendMailCussat($ticket_sid,$send_by,$send_to){
    try {
      $sql = "INSERT INTO log_send_cussat (ticket_sid,send_by,send_datetime,send_to) VALUES (:ticket_sid,:send_by,NOW(),:send_to) ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
          ':ticket_sid'=>$ticket_sid,
          ':send_by' => $send_by,
          ':send_to' => $send_to
        ));
        return 'Insert Completed';
    } catch (\Exception $e) {
      return 'Insert Failed';
    }
  }

  public function getAnswerByType(){
    $sql = "SELECT sid,answer,type FROM master_answer_satisfaction WHERE record_status = 1";
    $q = $this->db->prepare($sql);
    $q->execute();
    $r = $q->fetchAll();
    if (!empty($r) && $r != null) {
      return $r;
    }else {
      return 'Not Found Data';
    }
  }

  public function initData($data){
      // $checkData = $this->checkRequireField($data);
      // if(!$checkData){
      //     return false;
      // }
      $this->ticket_sid = $data['ticket_sid'];
      $this->subject = $data['subject'];
      $this->assessor_email = $data['assessor_email'];
      $this->requester_name = $data['requester_name'];
      return true;
  }

  public function createCustomerSatisfaction(){
    try{
        $sql = "INSERT INTO customer_satisfaction
        (
          ticket_sid,
          subject,
          assessor_email,
          requester_name,
          create_datetime,
          update_datetime

        ) VALUES
        (
            :ticket_sid,
            :subject,
            :assessor_email,
            :requester_name,
            NOW(),
            NOW()
        )";
        $q = $this->db->prepare($sql);
        return $q->execute(
            array(
                ":ticket_sid"=>$this->ticket_sid,
                ":subject"=>$this->subject,
                ":assessor_email"=>$this->assessor_email,
                ":requester_name"=>$this->requester_name
            )
        );
    }catch(Exception $e){
        return false;
    }
  }

}
?>
