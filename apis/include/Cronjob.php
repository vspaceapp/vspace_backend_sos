<?php 
require_once dirname(__FILE__) . '/GenNoCaseSR.php';
class Cronjob{

	private $db;
	private $after_create_datetime;

	private $no_ticket;
	private $via;
	private $subject;
	private $case_type;
	private $create_by;
	private $case_id;
	private $urgency;
	private $requester_name;
	private $requester_phone;
	private $requester_mobile;
	private $requester_email;
	private $requester_company_name;
	private $enduser_site;
	private $enduser_name;
	private $enduser_phone;
	private $enduser_company_name;
	private $contract_no;
	private $serial_no;
	private $incharge;
	private $description;
	private $status;
	private $assign_time;
	private $report_by;
	private $create_time;
	private $prime_contract;
	private $first_assigned_time;
	private $work_log;
	private $no_task;

    private $m;
	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function importCase(){
    	$this->after_create_datetime = '2016-08-29 00:00:00';
    	$data = $this->selectCase();

    	foreach ($data as $key => $value) {

    		$this->no_ticket = genTicketNo($this->db);
    		$this->via = 'remedy';
    		$this->refer_remedy_hd = $value['case_id'];
    		$this->subject = $value['subject'];
    		$this->case_type = $value['case_type'];
    		$this->create_by = $value['create_by'];
    		$this->case_id = $value['case_id'];
    		$this->urgency = $value['urgency'];
    		$this->requester_name = $value['requester_name'];
    		$this->requester_phone = $value['requester_phone'];
    		$this->requester_mobile = $value['requester_mobile'];
    		$this->requester_email = $value['requester_email'];
    		$this->requester_company_name = $value['requester_company_name'];
    		$this->enduser_site = $value['enduser_site'];
    		$this->enduser_name = $value['enduser_name'];
    		$this->enduser_phone = $value['enduser_phone'];
    		$this->enduser_company_name = $value['enduser_company_name'];
    		$this->contract_no = $value['contract_no'];
    		$this->serial_no = $value['serial_no'];
    		$this->incharge = $value['incharge'];
    		$this->description = $value['description'];
    		$this->status = $value['status'];
    		$this->assign_time = $value['assign_time'];
    		$this->report_by = $value['report_by'];
    		$this->create_time = $value['create_time'];
    		$this->prime_contract = $value['prime_contract'];
    		$this->first_assigned_time = $value['first_assigned_time'];
    		$this->work_log = $value['work_log'];


    		$this->insertToTicket();

    	}
    	return $data;
    }

    private function selectCase(){
    	$sql = "SELECT * FROM test_create_case WHERE create_datetime >= '".$this->after_create_datetime."' ";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();
    	return $r;
    }

    private function insertToTicket(){
    	
		
    }

    public function importsr(){
    	
    	$this->after_create_datetime = '2016-08-29 00:00:00';
    	$data = $this->selectServiceReport();
    	return $data;
    }

    private function selectServiceReport(){
    	$sql = "SELECT * FROM test_create_sr WHERE create_datetime >= '".$this->after_create_datetime."' ";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();

    	return $r;
    }

    public function insertToTask(){
    	$this->no_task = genTaskNo($this->db);
    }

    public function LoadSLA($case_type){
        // AND 
         // (DATE_FORMAT(update_datetime,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d')  OR DATE_FORMAT(update_datetime,'%Y-%m-%d') = DATE_FORMAT(NOW() - INTERVAL 24 HOUR,'%Y-%m-%d') ) 
    	 $sql = "SELECT * FROM ticket WHERE case_type = :case_type AND refer_remedy_hd IS NOT NULL 
         AND status <> '5' 
         AND status <> '6' ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':case_type'=> $case_type ));
    	$r = $q->fetchAll();
    	return $r;
    }


    public function updateSLARemedy( $refer_remedy_hd , $stringSLA , $owner){
        if($owner!=""){
            $this->m->setTable($owner);

        	$sql = "UPDATE ".$this->m->table_ticket." SET sla_remedy = :stringSLA, update_sla_remedy_datetime = NOW() WHERE refer_remedy_hd = :refer_remedy_hd";
        	$q = $this->db->prepare($sql);
        	$q->execute( array( ':stringSLA' => $stringSLA, ':refer_remedy_hd' => $refer_remedy_hd));

            // $slaRemedy = explode(",", $stringSLA);
            // $slaRemedySubUse1 = array();
            // foreach ($slaRemedy as $key => $value) {
            //     $slaSub = explode("||", $value);
            //     if(count($slaSub)>1){
            //         $slaRemedySubUse2 = array();
            //         foreach ($slaSub as $k => $v) {
            //             $slaRemedySub = explode("=>", $v);
            //             if($slaRemedySub[0]=="SLA_Name"){
            //                 $slaRemedySub[1] = str_replace("OneStop_","",$slaRemedySub[1]);
            //                 $slaRemedySub[1] = str_replace("_2012","",$slaRemedySub[1]);
            //             }

            //             if($slaRemedySub[0]=="SLA_Name"){
            //                 $slaRemedySub[1] = str_replace("FIRSTAccountOr7x24_","",$slaRemedySub[1]);
            //                 $slaRemedySub[1] = str_replace("_2011","",$slaRemedySub[1]);
            //                 $slaRemedySub[1] = str_replace("_2014","",$slaRemedySub[1]);
            //             }

            //             if($slaRemedySub[0]=="SLA_Due_Date"){
            //                 $slaDueDate = explode("T", $slaRemedySub[1]);
            //                 $datetime = explode("-", $slaDueDate[0]);
            //                 $datetime = ($datetime[0]-543)."/".$datetime[1]."/".$datetime[2];

            //                 $slaRemedySub[1] = $datetime." ".str_replace("+07:00", "", $slaDueDate[1]);
            //             }
            //             $slaRemedySubUse2[$k] =  array($slaRemedySub[0],$slaRemedySub[1]);
            //         }

            //         array_push(
            //             $slaRemedySubUse1, 
            //             array(
            //                 $slaRemedySubUse2[0][0]=>$slaRemedySubUse2[0][1],
            //                 $slaRemedySubUse2[1][0]=>$slaRemedySubUse2[1][1],
            //                 $slaRemedySubUse2[2][0]=>$slaRemedySubUse2[2][1],
            //                 $slaRemedySubUse2[3][0]=>$slaRemedySubUse2[3][1]
            //             )
            //         );
            //     }
            // }

            // if(count($slaRemedySubUse1)>0){
            //     function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
            //         $sort_col = array();
            //         foreach ($arr as $key=> $row) {
            //             $sort_col[$key] = $row[$col];
            //         }
            //         array_multisort($sort_col, $dir, $arr);
            //     }

            //     array_sort_by_column($slaRemedySubUse1, 'SLA_Due_Date');

            //     $data['sla_remedy_use'] = array();
            //     $data['sla_remedy_use'] = $slaRemedySubUse1;
            // }
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
    	}

    }
}
?>