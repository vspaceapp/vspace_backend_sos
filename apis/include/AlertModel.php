<?php
error_reporting(-1);
ini_set('display_errors', 'On');
class AlertModel{

	private $db;
  private $m;
  private $role_sid, $email, $token, $permission_sid;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
  }

  public function loadNotificationGroup($data){
    try{
      $sql = "SELECT * FROM gable_alert GROUP BY group_code, assigned_group ORDER BY sid DESC ";
      $q = $this->db->prepare($sql);
      $q->execute();
      $r = $q->fetchAll();
      return array('status'=>1, 'message'=>'Data ok', 'data'=>$r);
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function loadNotificationDetail($data){
    try{
			if($data['group_code']==""){
				$sql = "SELECT * FROM gable_alert WHERE (group_code = :group_code OR group_code IS NULL) AND assigned_group = :assigned_group
				ORDER BY FIELD(status,'Open','Response','Onsite','Workaround','Resolve','Resolution','Pending','Cancelled') ";
			}else{
				$sql = "SELECT * FROM gable_alert WHERE group_code = :group_code AND assigned_group = :assigned_group
	      ORDER BY FIELD(status,'Open','Response','Onsite','Workaround','Resolve','Resolution','Pending','Cancelled') ";
			}

      $q = $this->db->prepare($sql);
      $q->execute(array(':group_code'=>$data['group_code'],':assigned_group'=>$data['assigned_group']));
      $r = $q->fetchAll();
      return array('status'=>1, 'message'=>'Ok, '.$sql, 'data'=>$r);
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }

	public function updateAlertByColumnValueSid($data){
		try{
			if(isset($data['column']) && isset($data['value']) && isset($data['sid']) && isset($data['email'])){
				$column = $data['column'];
				$value = $data['value'];
				$sid = $data['sid'];

				$sql = "UPDATE gable_alert SET ".$data['column']." = :value, updated_datetime = NOW() WHERE sid = :sid ";
				$q = $this->db->prepare($sql);
				$q->execute(array(':value'=>trim($value),':sid'=>$sid));
				return array('status'=>1, 'message'=>'Updated', 'data'=>array());
			}else{
				return array('status'=>0, 'message'=>'column, value, sid ,email is not null', 'data'=>array());
			}
		}catch(PDOException $e){
			return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
		}
	}

	public function addAlert($data){
		try{
			$contract = isset($data['contract'])?$data['contract']:'';
			$email = isset($data['email'])?$data['email']:'';
			$group_code = isset($data['group_code'])?$data['group_code']:'';
			$assigned_group = isset($data['assigned_group'])?$data['assigned_group']:'';
			$status = isset($data['status'])?$data['status']:'';
			if($email && $assigned_group && $status){
				$statusArray = explode(",", $status);
				foreach ($statusArray as $key => $value) {
					$sql = "INSERT INTO gable_alert (contract, group_code, assigned_group, status, created_datetime, created_by)
					VALUES (:contract, :group_code, :assigned_group, :status, NOW(), :created_by) ";
					$q = $this->db->prepare($sql);
					$q->execute(
						array(
							':contract'=>$contract,
							':group_code'=>$group_code,
							':assigned_group'=>$assigned_group,
							':status'=>trim($value),
							':created_by'=>$email
						));
				}
				return array('status'=>0, 'message'=>'Added', 'data'=>array());
			}else{
				return array('status'=>0, 'message'=>'email, group_code, assigned_group, status is not null', 'data'=>array());
			}
		}catch(PDOException $e){
			return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
		}
	}
}
?>
