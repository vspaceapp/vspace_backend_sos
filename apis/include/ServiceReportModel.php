<?php
require_once __dir__."/../core/constant.php";
class ServiceReportModel{

    private $db;
    private $project,
    $appointment_time,
    $appointment_date,
    $engineer,
    $subject,
    $description,
    $end_user_company_name,
    $end_user_site,
    $end_user_contact_name,
    $end_user_phone,
    $end_user_mobile,
    $end_user_email,
    $expect_time,
    $email,
    $type_service;

    private $appointment_datetime;
    private $project_name;
    private $customer;

    private $serial_no;

    private $via;

    private $caseNo;
    private $srNo;

    private $ticket_sid;
    private $task_sid;
    private $tasks_sid;

    private $contract_no;
    private $prime_contract = "";

    private $status;
    private $team;

    private $owner;

    private $case_type;
    private $source;
    private $note = "";
    private $urgency = "";

    private $requester_full_name = "",
    $requester_company_name = "",
    $requester_phone = "",
    $requester_mobile = "",
    $requester_email = "";

    private $expect_finish = "";

    private $receive_mail_datetime,
    $refer_remedy_hd,
    $refer_remedy_assigned_time,
    $refer_remedy_create_time,
    $refer_remedy_report_by;

    private $token;

    private $m;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function convertDatetimeAppFormatToDd($datetime){
        $datetime = explode(" ", $datetime);
        return $datetime = $datetime['3']."-".$this->getNumberMonthFromShortMonth($datetime['1'])."-".$datetime['2']." ".$datetime['4'];
    }

    public function dataSr($tasks_sid, $email, $token, $ticket_sid){
        $this->ticket_sid = $ticket_sid;
        $this->tasks_sid = $tasks_sid;
        $this->email = $email;
        $this->token = $token;

        $this->m->setTable($this->email);
        $data = $this->dataServiceReport();
        return $data;
    }

    public function changeToEmail($tasks_sid, $changeToEmail, $email, $token){
        // $this->m->setTable($email);

        $sql = "SELECT engineer FROM ".$this->m->table_tasks." WHERE sid = :tasks_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetch();
        $oldEngineer = $r['engineer'];

        $sql = "UPDATE ".$this->m->table_tasks." SET engineer = :engineer, updated_datetime = NOW(), updated_by = :updated_by WHERE sid = :tasks_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':engineer'=>$changeToEmail,':updated_by'=>$email,':tasks_sid'=>$tasks_sid));


        // UPDATE TO TABLE tasks_log
        // $sql = "UPDATE ".$this->m->table_tasks_log."
        // SET status = '-2', comment_status = status, update_datetime = NOW(), update_by = :update_by
        // WHERE tasks_sid = :tasks_sid AND engineer = :engineer ";
        // $q = $this->db->prepare($sql);
        // $q->execute(array(':update_by'=>$email,':tasks_sid'=>$tasks_sid,':engineer'=>$oldEngineer));

        $this->deleteEngineerFromTaskslog($email,$tasks_sid,$oldEngineer);

        return $this->addEngineerToTaskslog($changeToEmail, $email, $tasks_sid);
    }

    private function dataServiceReport(){
        $sql = "SELECT TASK.*,ST.name service_type_name FROM ".$this->m->table_tasks." TASK LEFT JOIN service_type ST on TASK.service_type = ST.sid WHERE TASK.sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$this->tasks_sid));
        $r = $q->fetch();
        $r['can_change_appointment'] = true;
        $r['can_change_end_user'] = true;
        $r['ticket_sid'] = $this->ticket_sid;


        $r['engineer_info'] = $this->m->getInfoEmployee($r['engineer']);
        $r['engineer_info']['email'] = $r['engineer'];

        if($r['file_name_signated']){
            $r['pdf_report'] = URL."pdf/".$r['no_task'].".pdf";
        }else{
            $r['pdf_report'] = "-";
        }
        $r['associate_engineer_info'] = array();
        if($r['associate_engineer']){
            $associate_engineer = explode(",", $r['associate_engineer']);
            foreach ($associate_engineer as $key => $value) {
                $r['associate_engineer_info'][$key] =  $this->m->getInfoEmployee($value);
                $r['associate_engineer_info'][$key]['email'] = $value;
            }
        }
        if(isset($r['last_status'])){
            if($r['last_status']==0){
                $r['status_name'] = "New";
            }else if($r['last_status']==100){
                $r['status_name'] = "Accepted";
            }else if($r['last_status']==200){
                $r['status_name'] = "On The way";
            }else if($r['last_status']==300){
                $r['status_name'] = "Working";
            }else if($r['last_status']==400){
                $r['status_name'] = "Closed";
                $r['can_change_appointment'] = false;
            }else if($r['last_status']==500){
                $r['status_name'] = "Send mail";
                $r['can_change_appointment'] = false;
            }else if($r['last_status']==600){
                $r['status_name'] = "Completed";
                $r['can_change_appointment'] = false;
            }else{
                $r['status_name'] = "";
                $r['can_change_appointment'] = false;
            }
        }
        return $r;
    }

    public function addAssociateEngineer($tasks_sid, $email, $token, $ticket_sid, $case_change_engineer){
        // $this->m->setTable($email);
        $associate_engineer = "";
        $sql = "SELECT associate_engineer FROM ".$this->m->table_tasks." WHERE sid = :tasks_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetch();

        if(isset($r['associate_engineer'])){
            $associate_engineer = $r['associate_engineer'];
        }

        foreach ($case_change_engineer as $key => $value) {
            if($associate_engineer!=""){
                $associate_engineer .= ",";
            }
            $associate_engineer .= $value['email'];
            $this->addEngineerToTaskslog($value['email'], $email, $tasks_sid);
        }

        // $sql = "UPDATE ".$this->m->table_tasks." SET associate_engineer = :associate_engineer, updated_by = :email, updated_datetime = NOW() WHERE sid = :tasks_sid";
        // $q = $this->db->prepare($sql);
        // $q->execute(array(':associate_engineer'=>$associate_engineer,':email'=>$email,':tasks_sid'=>$tasks_sid));
        $this->updateAssociateEngineerToTasks($associate_engineer, $email, $tasks_sid);
    }

    public function removeEngineerConfirm($tasks_sid, $email, $token, $ticket_sid, $emailRemove){
        // $this->m->setTable($email);
        $associate_engineer = "";
        $sql = "SELECT associate_engineer FROM ".$this->m->table_tasks." WHERE sid = :tasks_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetch();

        if(isset($r['associate_engineer'])){
            $associate_engineer = $r['associate_engineer'];
        }

        $associate_engineerOld = explode(",", $associate_engineer);
        $associate_engineerNew = array();
        $associate_engineerString = "";
        foreach ($associate_engineerOld as $key => $value) {
            if($value!=$emailRemove){
                array_push($associate_engineerNew, $value);

                if($associate_engineerString!=""){
                    $associate_engineerString .= ",";
                }
                $associate_engineerString .= $value;
                $this->addEngineerToTaskslog($value, $email, $tasks_sid);
            }else{
                $this->deleteEngineerFromTaskslog($email, $tasks_sid, $value);
            }
        }

        $this->updateAssociateEngineerToTasks($associate_engineerString, $email, $tasks_sid);
    }

    private function updateAssociateEngineerToTasks($associate_engineerString, $email, $tasks_sid){
        $sql = "UPDATE ".$this->m->table_tasks." SET associate_engineer = :associate_engineer, updated_by = :email, updated_datetime = NOW() WHERE sid = :tasks_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':associate_engineer'=>$associate_engineerString,':email'=>$email,':tasks_sid'=>$tasks_sid));
    }
    private function deleteEngineerFromTaskslog($email,$tasks_sid,$engineer){
        $sql = "UPDATE ".$this->m->table_tasks_log."
                SET comment_status = status, status = '-2',  update_datetime = NOW(), update_by = :update_by
                WHERE tasks_sid = :tasks_sid AND engineer = :engineer ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':update_by'=>$email,':tasks_sid'=>$tasks_sid,':engineer'=>$engineer));
    }

    private function addEngineerToTaskslog($engineer_email,$create_by,$tasks_sid,$taxi_fare=0,$lat=0,$lng=0,$status=0){
        $sql = "INSERT INTO ".$this->m->table_tasks_log." (tasks_sid,froms,engineer,create_by,create_datetime,appointment, expect_finish, status, taxi_fare,latitude,longitude,datetime_to_report)
                    SELECT tasks_sid,froms,:engineer_email, :create_by, NOW() ,appointment, expect_finish, :status, :taxi_fare,:latitude,:longitude,NOW()
                    FROM ".$this->m->table_tasks_log." WHERE tasks_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1 ";

        $q = $this->db->prepare($sql);
        $result = $q->execute(
                array(':engineer_email'=>$engineer_email,':create_by'=>$create_by,':status'=>$status,':tasks_sid'=>$tasks_sid,':taxi_fare'=>$taxi_fare,':latitude'=>$lat,':longitude'=>$lng)
            );
    }
    private function getNumberMonthFromShortMonth($shortMonth){
        if($shortMonth=="Jan"){
            return "01";
        }else if($shortMonth=="Feb"){
            return "02";
        }else if($shortMonth=="Mar"){
            return "03";
        }else if($shortMonth=="Apr"){
            return "04";
        }else if($shortMonth=="May"){
            return "05";
        }else if($shortMonth=="Jun"){
            return "06";
        }else if($shortMonth=="Jul"){
            return "07";
        }else if($shortMonth=="Aug"){
            return "08";
        }else if($shortMonth=="Sep"){
            return "09";
        }else if($shortMonth=="Oct"){
            return "10";
        }else if($shortMonth=="Nov"){
            return "11";
        }else if($shortMonth=="Dec"){
            return "12";
        }else{
            return "01";
        }
    }
    public function editAppointmentMobile($email, $tasks_sid, $datetime){
        if($email!=""){
            // $this->m->setTable($email);
        }
        $this->email = $email;
        $datetime = $this->convertDatetimeAppFormatToDd($datetime);
        $this->tasks_sid = $tasks_sid;
        return $this->editAppointmentDb($tasks_sid, $datetime);
    }

    public function editAppointmentMobile1($email, $tasks_sid, $datetime, $expect_duration=""){
        if($email!=""){
            // $this->m->setTable($email);
        }
        $this->email = $email;

        $this->tasks_sid = $tasks_sid;
        // return $datetime;
        return $this->editAppointmentDb($tasks_sid, $datetime, $expect_duration);
    }

    public function editAppointment($email, $tasks_sid, $date, $time){
        if($email!=""){
            // $this->m->setTable($email);
        }
        $this->email = $email;
        $date = $this->convertDateSlash($date);
        $datetime = $date." ".$time;
        $this->tasks_sid = $tasks_sid;
        return $this->editAppointmentDb($tasks_sid, $datetime);
    }

    private function editAppointmentDb($tasks_sid, $datetime, $expect_duration=""){
        // $sql = "SELECT expect_duration FROM tasks WHERE sid = :sid ";
        // $q = $this->db->prepare($sql);
        // $q->execute(array(':sid'=>$tasks_sid));
        // $r = $q->fetch();
        // $expect_duration = $r['expect_duration'];
        // return $datetime;
        if( strpos("0000-00-00", $datetime)>0){

        }else{
            if($expect_duration==""){
                $sql = "UPDATE ".$this->m->table_tasks." SET appointment = :appointment, updated_by = :email, updated_datetime = NOW(),
                expect_finish = ADDTIME(:appointment, expect_duration)
                WHERE sid = :tasks_sid ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':appointment'=>$datetime,':email'=>$this->email,':tasks_sid'=>$tasks_sid));

                $sql = "UPDATE ".$this->m->table_tasks_log." SET appointment = :appointment, expect_finish = ADDTIME(:appointment, expect_duration)
                WHERE tasks_sid = :tasks_sid ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':appointment'=>$datetime,':email'=>$this->email,':tasks_sid'=>$tasks_sid));
            }else{
                $sql = "UPDATE ".$this->m->table_tasks." SET appointment = :appointment, updated_by = :email, updated_datetime = NOW(),
                expect_finish = ADDTIME(:appointment, :expect_duration) , expect_duration = :expect_duration
                WHERE sid = :tasks_sid ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':appointment'=>$datetime,':expect_duration'=>$expect_duration,':email'=>$this->email,':tasks_sid'=>$tasks_sid));

                $sql = "UPDATE ".$this->m->table_tasks_log." SET appointment = :appointment, expect_finish = ADDTIME(:appointment, :expect_duration), expect_duration = :expect_duration
                WHERE tasks_sid = :tasks_sid ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':appointment'=>$datetime,':expect_duration'=>$expect_duration,':email'=>$this->email,':tasks_sid'=>$tasks_sid));
            }
        }
        $activity_sid = 3;
        return $this->insertActivityLog($activity_sid, $tasks_sid);
    }

    private function insertActivityLog($activity_sid, $tasks_sid){
        $sql = "INSERT INTO ".$this->m->table_activity_log." (create_by,create_datetime,activity_sid, tasks_sid) VALUES (:create_by, NOW(), :activity_sid, :tasks_sid ) ";
        $q = $this->db->prepare($sql);
        return $q->execute(
            array(':create_by'=>$this->email,
                ':activity_sid'=>$activity_sid,':tasks_sid'=>$tasks_sid));
    }

    public function editTimestamp($email, $tasks_log_sid, $date, $time){
        $this->email = $email;

        $date = $this->convertDateSlash($date);
        $datetime = $date." ".$time;

        $this->editTimestampDb($tasks_log_sid, $datetime);
    }

    private function editTimestampDb($tasks_log_sid, $datetime){
        $sql = "UPDATE ".$this->m->table_tasks_log." SET create_datetime = :datetime_ WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':datetime_'=>$datetime,':sid'=>$tasks_log_sid));

        $sql = "INSERT INTO ".$this->m->table_activity_log." (create_by,create_datetime,activity_sid, tasks_log_sid) VALUES (:create_by, NOW(), '5', :tasks_log_sid ) ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(':create_by'=>$this->email,':tasks_log_sid'=>$tasks_log_sid));

    }

    public function convertDateSlash($date){
        if($date!=""){
            $date = explode("/", $date);
            return $date[2]."-".$date[1]."-".$date[0];
        }
        return $date;
    }

    public function loadAllServiceReport(){
        $sql = "SELECT T.*, ST.name service_type_name FROM ".$this->table_tasks." T
                LEFT JOIN service_type ST ON T.service_type = ST.sid
                WHERE T.last_status BETWEEN 100 AND 600
                ORDER BY T.last_status DESC";

        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    function testModel(){
        $sql = "SELECT NOW() now ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r['now'];
    }

    function testCreateCase($subject,$case_type,$email){
        $this->subject = $subject;
        $this->case_type = $case_type;
        $this->email = $email;

        $sql = "INSERT INTO test_create_case (subject, case_type, create_by, create_datetime) VALUES (:subject, :case_type, :create_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':subject'=>$subject,':case_type'=>$case_type,':create_by'=>$email));
    }

    //case address insert to end_user_site
    // receive from createcase
    function executeCreateCase($subject, $description, $case_company, $case_address, $case_type, $urgency, $serial, $contract, $prime_contract, $note,$requester_name,$requester_phone,$requester_email,$requester_mobile,$requester_company,$end_user_name,$end_user_phone,$end_user_email,$end_user_mobile,$end_user_company,$email,$owner=""){

        $this->caseNo = genTicketNo($this->db);
        $this->via = "createcase";
        $this->email = $email;

        if($owner){
            $this->owner = $owner;
        }else{
            $this->owner = $email;
        }
        $this->status = "1";
        $this->subject = trim(addslashes($subject));
        $this->due_datetime = "";
        $this->description = trim(addslashes($description));
        $this->contract_no = trim($contract);
        $this->prime_contract = trim($prime_contract);
        $this->serial_no = trim($serial);
        $this->team = "SSS";
        $this->requester_full_name = $requester_name;
        $this->requester_phone = $requester_phone;
        $this->requester_mobile = $requester_mobile;
        $this->requester_email = $requester_email;
        $this->requester_company_name = $requester_company;
        $this->end_user_contact_name = $end_user_name;
        $this->end_user_phone = $end_user_phone;
        $this->end_user_email = $end_user_email;
        $this->end_user_mobile = $end_user_mobile;
        $this->end_user_company_name = $end_user_company;
        $this->end_user_site = $case_address;
        $this->case_company = $case_company;

        $this->case_type = $case_type;
        $this->note = $note;
        $this->urgency = $urgency;

        $this->insertTicketRemedy();
        $this->insertTicketLog();
        $this->insertSerial();

        require_once 'SendMail.php';
        // require_once '../../../api/application/core/controller.php';
        // require_once '../../../api/application/controller/sendmail.php';
        $objsendmail = new SendMail();
        $objsendmail->noticeEngineerCaseAssigned($this->ticket_sid);
        $objsendmail->sendToCustomerCaseOpened($this->ticket_sid);

    }

    //cron job get data from email
    function executeCreateCaseIncident($subject, $receive_mail_datetime, $case_id, $urgency, $requester_name, $requester_phone, $requester_mobile, $requester_email, $company, $end_user_site, $end_user_contact_name, $end_user_phone, $contract, $project_name, $serial_no, $incharge, $description, $status, $assigned_time, $report_by, $create_time, $prime_contract ){

        $this->caseNo = genTicketNo($this->db);
        $this->via = "remedy";
        $this->email = $this->helpGetEmailFromThainame($report_by);

        $this->receive_mail_datetime = $receive_mail_datetime;
        $this->refer_remedy_hd = $case_id;
        $this->refer_remedy_assigned_time = $assigned_time;
        $this->refer_remedy_create_time = $create_time;
        $this->refer_remedy_report_by = $report_by;

        $this->owner = $this->helpGetEmailFromThainame($incharge);
        $this->status = "1";
        $this->subject = trim(addslashes($subject));

        $this->due_datetime = "";
        $this->description = trim(addslashes($description));
        $this->contract_no = trim($contract);
        $this->prime_contract = trim($prime_contract);
        $this->project_name = $project_name;
        $this->serial_no = $serial_no;
        $this->team = "SSS";
        $this->requester_full_name = $requester_name;
        $this->requester_company_name = "";
        $this->requester_phone = $requester_phone;
        $this->requester_mobile = $requester_mobile;
        $this->requester_email = $requester_email;
        $this->end_user_company_name = $company;
        $this->end_user_site = $end_user_site;
        $this->end_user_contact_name = $end_user_contact_name;

        $end_user_infomation = explode("/", $end_user_phone);

        $this->end_user_phone = trim($end_user_infomation[0]);
        $this->end_user_mobile = $end_user_mobile;
        $this->end_user_email = trim($end_user_infomation[1]);
        $this->case_type = "Incident";

        $this->note = "";
        $this->urgency = $urgency;
        $this->case_company = $company;
        $this->insertTicketRemedy();
        $this->insertTicketLog();
        $this->insertSerial();
    }

    private function helpGetEmailFromThainame($thainame){
        $thainame = explode(" ", trim($thainame));
        $name = array();
        $n = 0;
        foreach ($thainame as $key => $value) {
            if($value!=""){
                $name[$n] = $value;
                $n++;
            }
        }
        $sql = "SELECT emailaddr FROM employee WHERE thaifirstname = :thaifirstname AND thailastname = :thailastname ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':thaifirstname'=>trim($name[0]),
            ':thailastname'=>trim($name[1])
            ));
        $r = $q->fetch();
        if($r['emailaddr']!=""){
            return $r['emailaddr'];
        }
        return trim($name[0])." ".trim($name[1]);
    }
    private function selectCase($sequence_id){
        $sql = "SELECT * FROM test_create_case WHERE sid = '".$sequence_id."' AND create_datetime >= '".$this->after_create_datetime."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    private function selectStatusNo($status){
    	$sql = "SELECT sid FROM case_status WHERE name = '".trim($status)."'";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetch();
    	if($r['sid'] && $r['sid']>0)
	    	return $r['sid'];
	    else
	    	return $status;
    }

	public function importCase($sequence_id){
        $this->after_create_datetime = '2016-08-29 00:00:00';
        $data = $this->selectCase($sequence_id);

        foreach ($data as $key => $value) {
            $this->caseNo = genTicketNo($this->db);
            $this->via = 'remedy';
            $this->refer_remedy_hd = $value['case_id'];
            $this->subject = $value['subject'];
            $this->case_type = $value['case_type'];
            $this->email =  $this->helpGetEmailFromThainame($value['report_by']);
            $this->case_id = $value['case_id'];
            $this->urgency = $value['urgency'];
            $this->requester_full_name = $value['requester_name'];
            $this->requester_phone = $value['requester_phone'];
            $this->requester_mobile = $value['requester_mobile'];
            $this->requester_email = $value['requester_email'];
            $this->requester_company_name = $value['requester_company_name'];
            $this->end_user_site = $value['enduser_site'];
            $this->end_user_contact_name = $value['enduser_name'];
            $this->end_user_phone = $value['enduser_phone'];
            $this->end_user_company_name = $value['enduser_company_name'];
            $this->end_user_email = $this->splitPhoneEmail($value['enduser_phone']);
            $this->contract_no = $value['contract_no'];
            $this->serial_no = $value['serial_no'];
            $this->owner = $this->helpGetEmailFromThainame($value['incharge']);
            $this->description = $value['description'];

            $this->case_status_sid = $this->status = $this->selectStatusNo($value['status']);
            $this->refer_remedy_assigned_time = $this->convertYearToInter($value['assigned_time']);
            $this->refer_remedy_report_by = $this->helpGetEmailFromThainame($value['report_by']);
            $this->receive_mail_datetime = $this->convertYearToInter($value['create_time']);
            $this->prime_contract = $value['prime_contract'];
            $this->first_assigned_time = $value['first_assigned_time'];
            $this->work_log = $value['work_log'];
            $this->due_datetime = '';
            $this->team = 'SSS';

            if($this->refer_remedy_hd){
                $sql = "SELECT * FROM ticket WHERE refer_remedy_hd = :refer_remedy_hd ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':refer_remedy_hd'=>$this->refer_remedy_hd));
                $r = $q->fetch();


                if($r['sid']>0){
                    $this->ticket_sid = $r['sid'];
                    // $this->setTicketSidFromRemedyHd();
                    if($this->ticket_sid>0){
                        $this->updateSla();
                    }
                }else{
                    $this->insertTicketRemedy();
                    $this->status = $value['status'];
                    $this->insertCaseFor7x24();
                }
                // if($this->status=="1"){

                // }else if($this->status>=2 && $this->status<=7){
                    //Must set ticket_sid before run updateSla
                // }
            }
        }
        return $data;
    }

    private function insertCaseFor7x24(){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_cms_cases WHERE hd = :hd ";
        $q = $this->db->prepare($sql);
        $q->execute( array( ':hd' => $this->refer_remedy_hd ));
        $r = $q->fetch();

        //check repeat case in tbl_cms_cases
        if( $r['id'] && $r['id'] > 0){
            //have in
        }else{
            //not have add to tbl_cms_cases
            $sql = "INSERT INTO flguploa_firstlogicis.tbl_cms_cases ( hd, contract_no, subject, assigned_time, create_datetime, receive_mail_datetime, status, serial_no, incharge, create_by, create_time)
                VALUES (:refer_remedy_hd, :contract_no, :subject, :refer_remedy_assigned_time, NOW(), :receive_mail_datetime, :status, :serial_no, :owner, :create_by, :refer_remedy_create_time)";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':refer_remedy_hd'=>$this->refer_remedy_hd,
                ':contract_no'=>$this->contract_no,
                ':subject'=>$this->subject,
                ':refer_remedy_assigned_time'=>$this->refer_remedy_assigned_time,
                ':receive_mail_datetime'=>$this->receive_mail_datetime,
                ':status'=>$this->status,
                ':serial_no'=>$this->serial_no,
                ':owner'=>($this->getEmpnoFromEmailOldProject($this->owner))?$this->getEmpnoFromEmailOldProject($this->owner):"-",
                ':create_by'=>$this->email,
                ':refer_remedy_create_time'=>$this->refer_remedy_create_time
                ));
        }
    }
    private function setTicketSidFromRemedyHd(){
    	$sql = "SELECT sid FROM ticket WHERE refer_remedy_hd = :refer_remedy_hd ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':refer_remedy_hd'=>$this->refer_remedy_hd));
    	$r = $q->fetch();
        if($r['sid'] && $r['sid']>0)
        	$this->ticket_sid = $r['sid'];
    }
   	private function convertYearToInter($datetime){

   		$datetime = explode("-", $datetime);
   		$newYear = $datetime[0]-543;

   		$datetime = $newYear."-".$datetime[1]."-".$datetime[2];
   		return $datetime;
   	}

   	private function splitPhoneEmail($end_user_phone){
   		$enduser_phone = explode("/", $end_user_phone);
   		return $mail = trim($enduser_phone[count($enduser_phone)-1]);
   	}

    private function insertTicketRemedy(){


    	        $sql = "INSERT INTO ticket
    	            (no_ticket, via, create_datetime, create_by, receive_mail_datetime,refer_remedy_hd, refer_remedy_assigned_time, refer_remedy_create_time, refer_remedy_report_by,owner,status,subject,due_datetime,description,
    	        contract_no,prime_contract,project_name,serial_no,team,requester_full_name,requester_company_name,requester_phone,requester_mobile,requester_email,end_user_company_name,end_user_site,end_user_contact_name,end_user_phone,end_user_mobile,end_user_email,case_type,source,note,urgency,case_company)
    	            VALUES
    	            (:no_ticket, :via, NOW(), :create_by, :receive_mail_datetime, :refer_remedy_hd, :refer_remedy_assigned_time, :refer_remedy_create_time, :refer_remedy_report_by,:owner,:status,:subject,:due_datetime,:description,:contract_no,:prime_contract,:project_name,:serial_no,:team,:requester_full_name,:requester_company_name,:requester_phone,:requester_mobile,:requester_email,:end_user_company_name,:end_user_site,:end_user_contact_name,:end_user_phone,:end_user_mobile,:end_user_email,:case_type,:source,:note,:urgency,:case_company) ";
    	        $q = $this->db->prepare($sql);
    	        $q->execute(array(
    	            ':no_ticket'=>$this->caseNo,
    	            ':via'=>$this->via,
    	            ':create_by'=>$this->email,
    	            ':receive_mail_datetime'=>$this->receive_mail_datetime,
    	            ':refer_remedy_hd'=>$this->refer_remedy_hd,
    	            ':refer_remedy_assigned_time'=>$this->refer_remedy_assigned_time,
    	            ':refer_remedy_create_time'=>$this->refer_remedy_create_time,
    	            ':refer_remedy_report_by'=>$this->refer_remedy_report_by,
    	            ':owner'=>$this->owner,
    	            ':status'=>$this->status,
    	            ':subject'=>$this->subject,
    	            ':due_datetime'=>$this->due_datetime,
    	            ':description'=>$this->description,
    	            ':contract_no'=>$this->contract_no,
    	            ':prime_contract' => $this->prime_contract,
    	            ':project_name'=> $this->project_name,
    	            ':serial_no'=>$this->serial_no,
    	            ':team'=>$this->team,
    	            ':requester_full_name'=>$this->requester_full_name,
    	            ':requester_company_name'=>$this->requester_company_name,
    	            ':requester_phone'=>$this->requester_phone,
    	            ':requester_mobile'=>$this->requester_mobile,
    	            ':requester_email'=>$this->requester_email,
    	            ':end_user_company_name'=>$this->end_user_company_name,
    	            ':end_user_site'=>$this->end_user_site,
    	            ':end_user_contact_name'=>$this->end_user_contact_name,
    	            ':end_user_phone'=>$this->end_user_phone,
    	            ':end_user_mobile'=>$this->end_user_mobile,
    	            ':end_user_email'=>$this->end_user_email,
    	            ':case_type'=>$this->case_type,
    	            ':source'=>$this->via,
    	            ':note'=>$this->note,
    	            ':urgency'=>$this->urgency,
    	            ':case_company'=>$this->case_company
    	            ));
    	        //set variable ticket_sid
    	        $this->ticket_sid = $this->db->lastInsertId();

    }

    private function insertSerial(){
        if($this->serial_no!=""){
            $sql = "INSERT INTO ticket_serial (ticket_sid, serial_detail, create_datetime, create_by) VALUES (:ticket_sid, :serial_detail, NOW(), :create_by) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':ticket_sid' => $this->ticket_sid,
                ':serial_detail'=> $this->serial_no,
                ':create_by' => $this->email
                ));
        }
    }

    function executeCreateCaseInstall($project,$appointment_time,$appointment_date,$engineer,$subject,$description,$end_user_site,$end_user_contact_name,$end_user_phone,$end_user_mobile,$end_user_email,$expect_time,$email,$type_service,$case_company="",$subject_service_report="",$end_user_company_name=""){

        //set variable
        $this->project = $project;
        $this->appointment_time = $appointment_time;
        $this->appointment_date = $appointment_date;
        $this->engineer = $engineer;
        $this->subject = addslashes($subject);
        $this->description = $description;
        $this->end_user_site = $end_user_site;
        $this->end_user_contact_name = $end_user_contact_name;
        $this->end_user_phone = $end_user_phone;
        $this->end_user_mobile = $end_user_mobile;
        $this->end_user_email = $end_user_email;
        $this->expect_time = $expect_time;
        $this->email = $email;
        $this->type_service = $type_service;
        $this->prime_contract = "";
        $this->via = "install_module";
        $this->case_type = "Install";
        $this->owner = $this->email;
        $this->status = '1';
        $this->due_datetime = "";
        $this->serial_no = "";
        $this->team = "SSS";
        //gen and set variable
        $this->caseNo = genTicketNo($this->db);
        $this->srNo = genTaskNo($this->db);
        $this->case_company = $case_company;

        $this->end_user_company_name = $end_user_company_name;

        $this->subject_service_report = $subject_service_report;
        $this->end_user_contact_name_service_report =  $end_user_contact_name;
        $this->end_user_phone_service_report = $end_user_phone;
        $this->end_user_mobile_service_report = $end_user_mobile;
        $this->end_user_email_service_report= $end_user_email;
        $this->end_user_company_name_service_report = $end_user_company_name;

        $this->prepareDataProject();
        $this->createCaseAndSR();
    }


    function executeCreateCaseImplement($project_owner_sid,$appointment_time,$appointment_date,$engineer,$subject,$description,$end_user_site,$end_user_contact_name,$end_user_phone,$end_user_mobile,$end_user_email,$expect_time,$email,$type_service,$case_company="",$subject_service_report="",$end_user_company_name=""){

        //set variable
        $this->project = $project_owner_sid;
        $this->appointment_time = $appointment_time;
        $this->appointment_date = $appointment_date;
        $this->engineer = $engineer;
        $this->subject = addslashes($subject);
        $this->description = $description;
        $this->end_user_site = $end_user_site;
        $this->end_user_contact_name = $end_user_contact_name;
        $this->end_user_phone = $end_user_phone;
        $this->end_user_mobile = $end_user_mobile;
        $this->end_user_email = $end_user_email;
        $this->expect_time = $expect_time;
        $this->email = $email;
        $this->type_service = $type_service;
        $this->prime_contract = "";
        $this->via = "implement_module";
        $this->case_type = "Implement";
        $this->owner = $this->email;
        $this->status = '1';
        $this->due_datetime = "";
        $this->serial_no = "";
        $this->team = "SSS";
        $this->case_company = $case_company;
        //gen and set variable

        $this->end_user_company_name = $end_user_company_name;

        $this->subject_service_report = $subject_service_report;
        $this->end_user_contact_name_service_report =  $end_user_contact_name;
        $this->end_user_phone_service_report = $end_user_phone;
        $this->end_user_mobile_service_report = $end_user_mobile;
        $this->end_user_email_service_report= $end_user_email;
        $this->end_user_company_name_service_report = $end_user_company_name;

        $this->caseNo = genTicketNo($this->db);
        $this->srNo = genTaskNo($this->db);
        $this->prepareDataProject();
        $this->createCaseAndSR();
    }

    private function prepareDataProject(){
        $sql = "SELECT P.* FROM project P LEFT JOIN project_owner PO ON P.sid = PO.project_sid WHERE PO.sid = :project_sid AND PO.owner = :owner ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$this->project,':owner'=>$this->email));

        $r = $q->fetch();
        $this->project_name = $r['name'];
        $this->contract_no = $r['contract'];
        $this->end_user_company_name = $this->customer = $r['customer'];
    }

    private function createCaseAndSR(){
        $this->insertTicket();
        $this->insertTicketLog();
        //end Process ticket
        $this->createSR();
        $this->createSrLog();
    }



    private function insertTicket(){
        $sql = "INSERT INTO ticket (no_ticket, via, create_datetime, create_by, receive_mail_datetime,owner,status,subject,due_datetime,description,
        contract_no,prime_contract,project_name,serial_no,team,requester_full_name,requester_company_name,requester_phone,requester_mobile,requester_email,end_user_company_name,end_user_site,end_user_contact_name,end_user_phone,end_user_mobile,end_user_email,case_type,source,note,urgency,project_owner_sid,case_company)
            VALUES (:no_ticket, :via, NOW(), :create_by, NOW(),:owner,:status,:subject,:due_datetime,:description,:contract_no,:prime_contract,:project_name,:serial_no,:team,:requester_full_name,:requester_company_name,:requester_phone,:requester_mobile,:requester_email,:end_user_company_name,:end_user_site,:end_user_contact_name,:end_user_phone,:end_user_mobile,:end_user_email,:case_type,:source,:note,:urgency,:project_owner_sid,:case_company) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':no_ticket'=>$this->caseNo,
            ':via'=>$this->via,
            ':create_by'=>$this->email,
            ':owner'=>$this->owner,
            ':status'=>$this->status,
            ':subject'=>$this->subject,
            ':due_datetime'=>$this->due_datetime,
            ':description'=>$this->description,
            ':contract_no'=>$this->contract_no,
            ':prime_contract' => $this->prime_contract,
            ':project_name'=> $this->project_name,
            ':serial_no'=>$this->serial_no,
            ':team'=>$this->team,
            ':requester_full_name'=>$this->requester_full_name,
            ':requester_company_name'=>$this->requester_company_name,
            ':requester_phone'=>$this->requester_phone,
            ':requester_mobile'=>$this->requester_mobile,
            ':requester_email'=>$this->requester_email,
            ':end_user_company_name'=>$this->end_user_company_name,
            ':end_user_site'=>$this->end_user_site,
            ':end_user_contact_name'=>$this->end_user_contact_name,
            ':end_user_phone'=>$this->end_user_phone,
            ':end_user_mobile'=>$this->end_user_mobile,
            ':end_user_email'=>$this->end_user_email,
            ':case_type'=>$this->case_type,
            ':source'=>$this->via,
            ':note'=>$this->note,
            ':urgency'=>$this->urgency,
            ':create_by'=>$this->email,
            ':project_owner_sid'=>$this->project,
            ':case_company'=>$this->case_company
            ));
        //set variable ticket_sid
        $this->ticket_sid = $this->db->lastInsertId();
    }

    private function insertTicketLog(){
        //use variable
        $this->ticket_sid;

        $sql = "INSERT INTO ticket_log
        (ticket_sid, owner, status, subject, due_datetime, description, contract_no, prime_contract, project_name, serial_no, team, requester_full_name, requester_company_name, requester_phone, requester_mobile, requester_email, end_user_company_name, end_user_site, end_user_contact_name, end_user_phone, end_user_mobile, end_user_email, case_type, source, note, urgency, create_datetime, create_by)
        VALUES
        (:ticket_sid, :owner, :status, :subject, :due_datetime, :description, :contract_no, :prime_contract, :project_name, :serial_no, :team, :requester_full_name, :requester_company_name, :requester_phone, :requester_mobile, :requester_email, :end_user_company_name, :end_user_site, :end_user_contact_name, :end_user_phone, :end_user_mobile, :end_user_email, :case_type, :source, :note, :urgency, NOW(), :create_by) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid,
            ':owner'=>$this->owner,
            ':status'=>$this->status,
            ':subject'=>$this->subject,
            ':due_datetime'=>$this->due_datetime,
            ':description'=>$this->description,
            ':contract_no'=>$this->contract_no,
            ':prime_contract' => $this->prime_contract,
            ':project_name'=> $this->project_name,
            ':serial_no'=>$this->serial_no,
            ':team'=>$this->team,
            ':requester_full_name'=>$this->requester_full_name,
            ':requester_company_name'=>$this->requester_company_name,
            ':requester_phone'=>$this->requester_phone,
            ':requester_mobile'=>$this->requester_mobile,
            ':requester_email'=>$this->requester_email,
            ':end_user_company_name'=>$this->end_user_company_name,
            ':end_user_site'=>$this->end_user_site,
            ':end_user_contact_name'=>$this->end_user_contact_name,
            ':end_user_phone'=>$this->end_user_phone,
            ':end_user_mobile'=>$this->end_user_mobile,
            ':end_user_email'=>$this->end_user_email,
            ':case_type'=>$this->case_type,
            ':source'=>$this->via,
            ':note'=>$this->note,
            ':urgency'=>$this->urgency,
            ':create_by'=>$this->email
            ));
    }

    public function createSrIncident($ticket_sid, $appointment,$appointment_time, $expect_time, $engineerAssign, $email, $type_service,$subject_service_report="",$end_user_contact_name_service_report,$end_user_phone_service_report,$end_user_mobile_service_report,$end_user_email_service_report,$end_user_company_name_service_report){
        $this->ticket_sid = $ticket_sid;
        // $appointment = explode(" ", $appointment);
        $this->appointment_date = $appointment;
        $this->appointment_time = $appointment_time;
        $this->expect_time = $expect_time;
        $this->expect_finish = "";
        $this->type_service = $type_service;
        $this->email = $email;
        $this->engineer = $engineerAssign;


        $this->subject_service_report = $subject_service_report;

        $this->end_user_contact_name_service_report = $end_user_contact_name_service_report;
        $this->end_user_phone_service_report = $end_user_phone_service_report;
        $this->end_user_mobile_service_report = $end_user_mobile_service_report;
        $this->end_user_email_service_report = $end_user_email_service_report;
        $this->end_user_company_name_service_report = $end_user_company_name_service_report;

        $this->srNo = genTaskNo($this->db);
        $this->createSR();
        $this->createSrLog();
    }


    public function executeCreateTaskImplement($appointment_date,$appointment_time,$engineer,$expect_time,$type_service,$ticket_sid, $email){
        $this->ticket_sid = $ticket_sid;
        $this->appointment_date = $appointment_date;
        $this->appointment_time = $appointment_time;
        $this->expect_time = $expect_time;
        $this->expect_finish = "";
        $this->type_service = $type_service;
        $this->email = $email;
        $this->engineer = $engineer;
        $this->srNo = genTaskNo($this->db);
        $this->createSR();
        $this->createSrLog();
    }


    public function executeCreateTaskInstall($appointment_date,$appointment_time,$engineer,$expect_time,$type_service,$ticket_sid, $email){
        $this->ticket_sid = $ticket_sid;
        $this->appointment_date = $appointment_date;
        $this->appointment_time = $appointment_time;
        $this->expect_time = $expect_time;
        $this->expect_finish = "";
        $this->type_service = $type_service;
        $this->email = $email;
        $this->engineer = $engineer;
        $this->srNo = genTaskNo($this->db);
        $this->createSR();
        $this->createSrLog();
    }

    private function createSR(){
        //use variable
        $this->ticket_sid;

            $status = "0";

        $sql = "INSERT INTO tasks
                (no_task, ticket_sid, create_by, create_datetime, appointment, expect_duration, expect_finish, service_type, last_status,subject_service_report,end_user_contact_name_service_report,end_user_phone_service_report,end_user_mobile_service_report,end_user_email_service_report,end_user_company_name_service_report)
                VALUES
                (:no_task, :ticket_sid, :create_by, NOW(), :appointment, :expect_duration, :expect_finish, :service_type, :last_status,:subject_service_report,:end_user_contact_name_service_report,:end_user_phone_service_report,:end_user_mobile_service_report,:end_user_email_service_report,:end_user_company_name_service_report) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':no_task'=>$this->srNo,
            ':ticket_sid'=>$this->ticket_sid,
            ':create_by'=>$this->email,
            ':appointment'=>$this->appointment_date." ".$this->appointment_time.":00",
            ':expect_duration'=>$this->expect_time,
            ':expect_finish'=>$this->expect_finish,
            ':service_type'=>$this->type_service,
            ':last_status'=>$status,
            ':subject_service_report'=>$this->subject_service_report,
            ':end_user_contact_name_service_report'=>$this->end_user_contact_name_service_report,
            ':end_user_phone_service_report'=>$this->end_user_phone_service_report,
            ':end_user_mobile_service_report'=>$this->end_user_mobile_service_report,
            ':end_user_email_service_report'=>$this->end_user_email_service_report,
            ':end_user_company_name_service_report'=>$this->end_user_company_name_service_report
            ));

        $task_sid = $this->db->lastInsertId();
        //set variable task_sid
        $this->task_sid = $task_sid;

    }
    private function createSrLog(){
        //use variable
        $this->task_sid;

        $engineer = explode(",",$this->engineer);

        foreach ($engineer as $key => $value) {

            if($value==$this->email){
                $status = "100";
            }else{
                $status = "0";
            }
            $sql = "INSERT INTO tasks_log
                (tasks_sid, froms, engineer, create_by, create_datetime, appointment, expect_duration, expect_finish, status, service_type)
                VALUES
                (:tasks_sid, :froms, :engineer, :create_by, NOW(), :appointment, :expect_duration, :expect_finish, :status, :service_type) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':tasks_sid'=>$this->task_sid,
                ':froms'=>'',
                ':engineer'=>$value,
                ':create_by'=>$this->email,
                ':appointment'=>$this->appointment_date." ".$this->appointment_time.":00",
                ':expect_duration'=>$this->expect_time,
                ':expect_finish'=>$this->expect_finish,
                ':status'=>$status,
                ':service_type'=>$this->type_service
                ));

        }

    }

    private function getRoleEmail(){
        $sql = "SELECT role_sid FROM user_role WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$this->emailForRule));
        $r = $q->fetch();
        $this->role = $r['role_sid'];
        return $r['role_sid'];
    }

    private function getRule(){
        // $sql = "SELECT R.rule FROM rule R WHERE R.name = :name_role ";
        // $q = $this->db->prepare($sql);
        // $q->execute(array(':name_role'=>$this->name_role));
        // $r = $q->fetch();
        // $rule = $r['rule'];
        // eval($role);
        $sqlC = "";
        if($this->role=="2" || $this->role=="4" || $this->role=="6"){

        }else if($this->emailForRule!='')  {
            $sqlC = " AND T.owner = '".$this->emailForRule."' ";
        }
        return $sqlC;
    }
    public function lists($type, $email="", $option = array(), $emailForRule="", $search=""){
        $this->email = $email;
        $this->emailForRule = $emailForRule;

        $sql = "SELECT T.*,E.thainame,E.empno,E.maxportraitfile,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile,
                CASE WHEN (SELECT tasks.sid FROM tasks INNER JOIN customer_signature_task ON tasks.sid = customer_signature_task.task_sid
                    INNER JOIN ticket ON tasks.ticket_sid = ticket.sid
                    WHERE 1 AND tasks.ticket_sid = T.sid AND ticket.case_type = 'Implement' LIMIT 0,1)  > 0
                    THEN
                        CONCAT('<a href=\'https://case.flgupload.com/apis/v1/mergepdf/mergePDF/',T.contract_no,'\'>PDF</a>')
                    ELSE '' END all_sr_implement,
                (SELECT tasks.appointment FROM tasks WHERE 1 AND tasks.ticket_sid = T.sid ORDER BY tasks.sid DESC LIMIT 0,1) last_appointment,
                CONCAT('<a href=\"javascript:void(0);\" onclick=\"openDetail(\'',T.sid,'\')\">View Detail</a>') viewdetail
                FROM ticket T
                    LEFT JOIN employee E ON E.emailaddr = T.owner
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                WHERE 1 ";
        if(isset($option['case_type'])){
            $sql .= "AND T.case_type IN (".$option['case_type'].") ";
        }else{
            $sql .= "AND T.case_type = '".$type."' ";
        }
        if($this->email!=""){
            $sql .= " AND (P.create_by = '".$this->email."' OR T.create_by = '".$this->email."' OR PO.create_by = '".$this->email."') ";
        }
        $role = $this->getRoleEmail();
        // $this->name_rule = "can_view_case";
        $sqlC = $this->getRule();
        if($sqlC!=""){
            $sql .= $sqlC;
        }

        if($search!=""){
            $sql .= " AND (T.subject LIKE '%".$search."%' OR T.no_ticket LIKE '%".$search."%' OR T.end_user_company_name LIKE '%".$search."%' OR T.contract_no LIKE '%".$search."%' ) ";
        }
        if(isset($option['sort'])){
            $sql .= " ORDER BY T.create_datetime ".$option['sort'];
        }else{
            $sql .= " ORDER BY T.create_datetime DESC";
        }
        // echo $sql;
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        // foreach ($r as $key => $value) {
        //     $r[$key]['sid'] = base64_encode($value['sid']);
        // }
        return $r;
    }
    public function remindSLA($type, $email="", $option = array()){
         $this->email = $email;

        $sql = "SELECT T.*,E.thainame,E.empno,E.maxportraitfile,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile
                FROM ticket T
                    LEFT JOIN employee E ON E.emailaddr = T.owner
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                WHERE 1 ";
        if(isset($option['case_type'])){
            $sql .= "AND T.case_type IN (".$option['case_type'].") ";
        }else{
            $sql .= "AND T.case_type = '".$type."' ";
        }
        if($this->email!=""){
            $sql .= " AND (P.create_by = '".$this->email."' OR T.create_by = '".$this->email."' OR PO.create_by = '".$this->email."') ";
        }

        if(isset($option['sort'])){
            $sql .= " ORDER BY T.create_datetime ".$option['sort'];
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $r[$key]['sla'] = $this->getDataIncidentSLA($value['sid'], $value['create_datetime']);
        }
        return $r;
    }

    public function getCaseDetail($ticket_sid){
        $sql = "SELECT T.*,E.thainame,E.maxportraitfile owner_picture
        	FROM ticket T INNER JOIN employee E ON T.owner = E.emailaddr WHERE T.sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetch();
        if($r['case_type']=="Incident"){
            $r['sla'] = $this->getDataIncidentSLA($ticket_sid,$r['create_datetime']);
        }
        if($r['serial_no']){
        	$serial_no = explode(",", $r['serial_no']);
        	// print_r($serial_no);
        	if(count($serial_no)>0){
        		$r['serial_no'] = array();
	        	foreach ($serial_no as $key => $value) {
	        		$r['serial_no'][$key]['serial'] = $value;
	        	}
	        }
        }
        return $r;
    }

    private function getDataIncidentSLA($ticket_sid, $create_datetime_sla){
        // CASE WHEN
        //         (SELECT
        //             (SELECT FDR_SUB.status FROM flow_detail_role FDR_SUB WHERE FDR_SUB.flow_detail_sid = FDR.flow_detail_sid ORDER BY create_datetime DESC LIMIT 0,1 )
        //             FROM flow_detail_role FDR WHERE FDR.flow_detail_sid = FD.sid AND FDR.role_sid = :role_sid GROUP BY FDR.flow_detail_sid
        //         ) = 1 THEN 1
        //         ELSE 0 END
        //         can_update_status,
        $modules_sid = "2";
        $sql = "SELECT FD.sla,CS.name case_status_name,CS.icon case_status_icon,CS.is_force, CS.sid case_status_sid,
                DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE) target_sla_time,

                (SELECT TS.create_datetime FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) done_sla_datetime,
                (SELECT TS.worklog FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) worklog,
                CASE
                WHEN
                    DATE_FORMAT(DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE),'%Y-%m-%d %H:%i:%s') < NOW() AND
                    (SELECT TS.sid FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) IS NULL AND
                    CS.is_force = '1'
                THEN 'Failed'
                WHEN
                    (SELECT TS.sid FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) IS NOT NULL AND
                    DATE_FORMAT(DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE),'%Y-%m-%d %H:%i:%s') < (SELECT TS.create_datetime FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1)
                THEN 'Failed'
                WHEN
                    (SELECT TS.sid FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) IS NOT NULL AND
                    DATE_FORMAT(DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE),'%Y-%m-%d %H:%i:%s') >= (SELECT TS.create_datetime FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1)
                THEN 'Passed'
                ELSE '-' END result
                FROM flow_detail FD
                INNER JOIN flow F ON FD.flow_sid = F.sid
                INNER JOIN case_status CS ON CS.sid = FD.case_status_sid
                WHERE F.status = '1' AND FD.status = '1'
                AND  (SELECT TS.create_datetime FROM ticket_status TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1 ) IS NULL
                AND F.modules_sid = :modules_sid ORDER BY FD.position_x ASC, FD.position_y ASC  ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':create_datetime_sla'=>$create_datetime_sla,
            ':ticket_sid'=>$ticket_sid,
            ':modules_sid'=>$modules_sid
            ));
        $r = $q->fetchAll();
        return $r;
    }

    public function changeAppointment($email, $appointment_date, $appointment_time, $task_sid){
        $this->setTaskSid($task_sid);
        $this->setAppointment($appointment_date, $appointment_time);
        $this->setEmail($email);

        return $this->updateAppointment();
    }

    public function changeExpectTime($email, $expect_time, $task_sid){
        $this->expect_time = $expect_time;
        $this->email = $email;
        $this->task_sid = $task_sid;
        return $this->updateExpectTime();
    }
    private function updateExpectTime(){
        $sql = "UPDATE tasks SET expect_duration = :expect_time, updated_by = :updated_by, updated_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':expect_time'=>$this->expect_time,
            ':updated_by'=>$this->email,
            ':sid'=>$this->task_sid
            ));
        $sql = "UPDATE tasks_log SET expect_duration = :expect_time WHERE tasks_sid = :tasks_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':expect_time'=>$this->expect_time,
            ':tasks_sid'=>$this->task_sid
            ));
        $sql = "SELECT expect_duration FROM tasks WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':sid'=>$this->getTaskSid()
            ));
        $r = $q->fetch();
        return $r['expect_duration'];
    }
    private function updateAppointment(){
        $sql = "UPDATE tasks SET appointment = :appointment, updated_by = :updated_by, updated_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':appointment'=>$this->getAppointment(),
            ':updated_by' => $this->getEmail(),
            ':sid'=>$this->getTaskSid()
            ));

        $sql = "UPDATE tasks_log SET appointment = :appointment WHERE tasks_sid = :task_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':appointment'=>$this->getAppointment(),
            ':task_sid'=>$this->getTaskSid()
            ));

        $sql = "SELECT DATE_FORMAT(appointment,'%d/%m/%Y %H:%i') appointment_datetime FROM tasks WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':sid'=>$this->getTaskSid()
            ));
        $r = $q->fetch();

        return $r['appointment_datetime'];
    }

    private function setTaskSid($task_sid){
        $this->task_sid = $task_sid;
    }
    private function getTaskSid(){
        return $this->task_sid;
    }

    private function setAppointment($appointment_date, $appointment_time){
        $appointment_date = explode("/", $appointment_date);
        $appointment_date = $appointment_date[2]."-".$appointment_date[1]."-".$appointment_date[0];

        $appointment_time = $appointment_time.":00";
        $this->appointment_datetime = $appointment_date." ".$appointment_time;
    }
    private function getAppointment(){
        return $this->appointment_datetime;
    }

    private function setEmail($email){
        $this->email = $email;
    }
    private function getEmail(){
        return $this->email;
    }

    public function initailUpdateWorklog($ticket_sid, $work_log, $email){
        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->work_log = addslashes($work_log);
        $this->updateWorklog();
    }
    private function updateWorklog(){
        $sql = "INSERT INTO ticket_worklog
            (ticket_sid,worklog,create_by,create_datetime) VALUES
            (:ticket_sid,:worklog,:create_by,NOW()) ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid,
            ':worklog'=>$this->work_log,
            ':create_by'=>$this->email
            ));
        // $sql = "SELECT * FROM ticket_worklog WHERE sid = '".$this->db->lastInsertId."'";
        // $q = $this->db->prepare($sql);
        // $q->execute();
        // return $q->fetch();
    }

    public function initailUpdateSlaAndWorklog($ticket_sid, $work_log, $case_status_sid, $email){
        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->work_log = addslashes($work_log);
        $this->case_status_sid = $case_status_sid;

        $this->updateSla();
    }

    private function updateSla(){
        $sql = "INSERT INTO ticket_status (ticket_sid, case_status_sid, create_datetime, create_by, data_status, worklog)
            VALUES (:ticket_sid, :case_status_sid, NOW(), :create_by, :data_status, :work_log) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid,
            ':case_status_sid'=>$this->case_status_sid,
            ':create_by'=>$this->email,
            ':data_status'=>'1',
            ':work_log'=>$this->work_log
        ));

        $sql = "UPDATE ticket SET status = :status, update_datetime = NOW(), updated_by = :email WHERE sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':status'=>$this->case_status_sid,':email'=>$this->email,':ticket_sid'=>$this->ticket_sid));
    }

    public function requestEndUserServiceReport($task_sid){
      $this->task_sid = $task_sid;
      $sql = "SELECT sid task_sid,
        CASE WHEN end_user_company_name_service_report IS NULL THEN '' ELSE end_user_company_name_service_report END end_user_company_name_service_report,
        CASE WHEN end_user_contact_name_service_report IS NULL THEN '' ELSE end_user_contact_name_service_report END end_user_contact_name_service_report,
        CASE WHEN end_user_mobile_service_report IS NULL THEN '' ELSE end_user_mobile_service_report END end_user_mobile_service_report,
        CASE WHEN end_user_phone_service_report IS NULL THEN '' ELSE end_user_phone_service_report END end_user_phone_service_report,
        CASE WHEN end_user_email_service_report IS NULL THEN '' ELSE end_user_email_service_report END end_user_email_service_report
      FROM tasks WHERE sid = :task_sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':task_sid'=>$this->task_sid
        ));
      return $q->fetch();
   }

   public function requestSubjectServiceReport($task_sid){
        $this->task_sid = $task_sid;
      $sql = "SELECT tasks.sid task_sid, tasks.service_type service_type, service_type.name service_type_name, service_type.icon service_type_icon,
        CASE WHEN subject_service_report IS NULL THEN '' ELSE subject_service_report END subject_service_report
      FROM tasks LEFT JOIN service_type ON tasks.service_type = service_type.sid WHERE tasks.sid = :task_sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':task_sid'=>$this->task_sid
        ));
      return $q->fetch();
   }

   public function editSubjectServiceReport($tasks_sid,$email,$subject_service_report, $service_type){
        $this->task_sid = $tasks_sid;
        $this->email = $email;
        $this->type_service = $service_type;
        $this->subject_service_report = $subject_service_report;
        $this->editSubjectServiceReportRunCommand();
   }

   private function editSubjectServiceReportRunCommand(){
    $sql = "UPDATE tasks SET subject_service_report = :subject_service_report,updated_by = :updated_by, updated_datetime = NOW(), service_type = :service_type
    WHERE sid = :task_sid  ";
    $q = $this->db->prepare($sql);
    $q->execute(array(
        ':subject_service_report'=>$this->subject_service_report,
        ':updated_by'=>$this->email,
        ':service_type'=>$this->type_service,
        ':task_sid'=>$this->task_sid
    ));

   }

   public function editEndUserServiceReport($tasks_sid,$email,$end_user_contact_name_service_report,$end_user_email_service_report,$end_user_mobile_service_report,$end_user_phone_service_report,$end_user_company_name_service_report){
        $this->task_sid = $tasks_sid;
        $this->email = $email;

        $this->end_user_contact_name_service_report =  $end_user_contact_name_service_report;
        $this->end_user_phone_service_report = $end_user_phone_service_report;
        $this->end_user_mobile_service_report = $end_user_mobile_service_report;
        $this->end_user_email_service_report= $end_user_email_service_report;
        $this->end_user_company_name_service_report = $end_user_company_name_service_report;

        $this->editEndUserServiceReportRunCommand();
   }
   private function editEndUserServiceReportRunCommand(){
    $sql = "UPDATE tasks SET end_user_contact_name_service_report = :end_user_contact_name_service_report,
    end_user_phone_service_report = :end_user_phone_service_report,
    end_user_mobile_service_report = :end_user_mobile_service_report,
    end_user_email_service_report = :end_user_email_service_report,
    end_user_company_name_service_report = :end_user_company_name_service_report, updated_by = :updated_by, updated_datetime = NOW()
    WHERE sid = :task_sid  ";
    $q = $this->db->prepare($sql);
    $q->execute(array(
        ':end_user_contact_name_service_report'=>$this->end_user_contact_name_service_report,
        ':end_user_phone_service_report'=>$this->end_user_phone_service_report,
        ':end_user_mobile_service_report'=>$this->end_user_mobile_service_report,
        ':end_user_email_service_report'=>$this->end_user_email_service_report,
        ':end_user_company_name_service_report'=>$this->end_user_company_name_service_report,
        ':updated_by'=>$this->email,
        ':task_sid'=>$this->task_sid
    ));

   }

   public function listServiceReport($ticket_sid){

        $this->ticket_sid = $ticket_sid;

        return $this->listServiceReports();
   }

   private function listServiceReports(){

        $sql = "SELECT * FROM tasks WHERE ticket_sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            if(file_exists(__dir__."/../../pdf/".$value['no_task'].'.pdf')){
                $r[$key]['pdf_report'] = "<a href='".HOST_NAME."/pdf/".$value['no_task'].".pdf' target='new'>PDF</a>";
            }else{
                $r[$key]['pdf_report'] = "-";
            }

            $engineer = $this->taskDetailOfTicket($value['sid']);
            $engineerData = "";
            foreach ($engineer as $k => $v) {
                if($v['last_status_task']>=0){
                    if(count($engineer)>1){
                        $engineerData .= "(".$v['thaifirstname'].") ";
                    }else{
                        $engineerData .= "".$v['thaifirstname'];
                    }
                }
            }
            $r[$key]['engineer'] = $engineerData;
            $r[$key]['engineer_raw'] = $engineer;

        }
        return $r;
   }

   private function taskDetailOfTicket($tasks_sid, $create_datetime =""){

        $sql = "SELECT TL.engineer,TL.tasks_sid,E.thaifirstname,E.thailastname,E.thainame,E.engfirstname,E.englastname,E.maxportraitfile picture,
                E.mobile,E.emailaddr email,(SELECT status FROM tasks_log WHERE TL.engineer = engineer AND TL.tasks_sid = tasks_sid ORDER BY sid DESC LIMIT 0,1) last_status_task,
                CASE (SELECT status FROM tasks_log WHERE TL.engineer = engineer AND TL.tasks_sid = tasks_sid ORDER BY sid DESC LIMIT 0,1)
                            WHEN '0' THEN 'Assign'
                            WHEN '-2' THEN 'Removed'
                            WHEN '-1' THEN 'Engineer Rejected'
                            WHEN '100' THEN 'Engineer Accepted'
                            WHEN '200' THEN 'Start the journey'
                            WHEN '300' THEN 'Onsite'
                            WHEN '400' THEN 'Closed job'
                            WHEN '500' THEN 'Send customer sign'
                            WHEN '600' THEN 'Finished'
                            ELSE ''
                        END status_display
                FROM tasks_log TL
                LEFT JOIN employee E ON E.emailaddr = TL.engineer
                WHERE TL.tasks_sid = :tasks_sid GROUP BY engineer ORDER BY TL.create_datetime ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetchAll();
        // foreach ($r as $key => $value) {
        //     $r[$key]['each_status_of_task_engineer'] = $this->eachStatusOfTaskEngineer($tasks_sid, $value['engineer']);
        // }
        return $r;
    }

     function updateSparePart($email,$part_number_defective,$part_serial_defective,$part_number,$part_serial,$description,$quantity,$sparepart_sid,$tasks_sid){
        $this->task_sid = $tasks_sid;
        $this->sparepart_sid = $sparepart_sid;
        $this->email = $email;

        $this->part_number_defective = $part_number_defective;
        $this->part_serial_defective = $part_serial_defective;
        $this->part_number = $part_number;
        $this->part_serial = $part_serial;
        $this->description = $description;
        $this->quantity = $quantity;

        $this->updateSparePartQuery();
    }

    private function updateSparePartQuery(){
        for ($i=0; $i < $this->quantity; $i++) {
            if($this->sparepart_sid>0){
                // UPDATE
                $sql = "UPDATE task_spare SET part_number_defective = :part_number_defective,
                part_serial_defective = :part_serial_defective,
                part_number = :part_number,
                part_serial = :part_serial,
                description = :description,
                update_datetime = NOW(),
                update_by = :email
                WHERE sid = :sparepart_sid ";

                $q = $this->db->prepare($sql);
                $q->execute(array(
                        ':part_number_defective'=>$this->part_number_defective,
                        ':part_serial_defective'=>$this->part_serial_defective,
                        ':part_number'=>$this->part_number,
                        ':part_serial'=>$this->part_serial,
                        ':description'=>$this->description,
                        ':email'=>$this->email,
                        ':sparepart_sid'=>$this->sparepart_sid
                    ));
            }else{
                //INSERT
                $sql = "INSERT INTO task_spare
                (part_number_defective,part_serial_defective,part_number,part_serial,description,create_datetime,create_by,task_sid,status,update_datetime,update_by)
                VALUES
                (:part_number_defective,:part_serial_defective,:part_number,:part_serial,:description,NOW(),:email,:task_sid,'1',NOW(),:updated_by) ";
                $q = $this->db->prepare($sql);
                $q->execute(array(
                        ':part_number_defective'=>$this->part_number_defective,
                        ':part_serial_defective'=>$this->part_serial_defective,
                        ':part_number'=>$this->part_number,
                        ':part_serial'=>$this->part_serial,
                        ':description'=>$this->description,
                        ':email'=>$this->email,
                        ':task_sid'=>$this->task_sid,
                        ':updated_by'=>$this->email
                    ));
            }
        }
    }

    public function deleteSparePart($email, $sparepart_sid){
        $this->email = $email;
        $this->sparepart_sid = $sparepart_sid;

        return $this->deleteSparePartQuery();
    }
    private function deleteSparePartQuery(){
        $sql = "UPDATE task_spare SET status = '-1', update_datetime = NOW(), update_by = :email WHERE sid = :sparepart_sid  ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':email'=>$this->email,
            ':sparepart_sid'=>$this->sparepart_sid
            ));
    }

    public function followup($email,$tasks_sid,$datetime){

        $this->email = $email;
        $this->task_sid = $tasks_sid;
        $this->datetime = $datetime;

        return $this->followupQuery();
    }

    private function followupQuery(){

        $this->addNewStatusTaskLog($this->email, $this->task_sid, '400', $this->email, "","","");

        $sql = "INSERT INTO task_followup_log (task_sid, followup_datetime, create_by, create_datetime) VALUES (:task_sid,:followup_datetime,:create_by,NOW()) ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':task_sid'=>$this->task_sid,
            ':followup_datetime'=>$this->datetime,
            ':create_by'=>$this->email
            ));
    }

    private function addNewStatusTaskLog($engineer_email, $tasks_sid, $status, $create_by, $taxi_fare ="", $lat="", $lng=""){
        $sql = "INSERT INTO tasks_log (tasks_sid,froms,engineer,create_by,create_datetime,appointment, expect_finish, status, taxi_fare,latitude,longitude)
                    SELECT tasks_sid,froms,:engineer_email, :create_by, NOW() ,appointment, expect_finish, :status, :taxi_fare,:latitude,:longitude
                    FROM tasks_log WHERE tasks_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1 ";

        $q = $this->db->prepare($sql);
        $result = $q->execute(
                array(':engineer_email'=>$engineer_email,':create_by'=>$create_by,':status'=>$status,':tasks_sid'=>$tasks_sid,':taxi_fare'=>$taxi_fare,':latitude'=>$lat,':longitude'=>$lng)
            );

        $sql = "UPDATE tasks SET last_status = :status, updated_by = :updated_by, updated_datetime = NOW() WHERE  sid = :sid ";
        $q = $this->db->prepare($sql);
        $r = $q->execute(array(':status'=>$status,':updated_by'=>$create_by,':sid'=>$tasks_sid));
    }

    public function importSr($sequence_id){
        $data = $this->selectSrFromSequenceId($sequence_id);

        $dataSrOld = $this->selectOldProject($data);
        $dataSrNew = $this->selectNewTask($data);

        if( $dataSrOld['no']  ){
            //have Old SR
        }else{
             $this->insertSrOldProject($data);
        }

        if( $dataSrNew['sr_refer_remedy']){
            //have Old SR
        }else{
            $this->insertNewTask($data);
        }
    }

    private function selectOldProject($data){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_service_report WHERE no = :no ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':no'=>$data['service_report_id']));
        return $r = $q->fetch();
    }

    private function insertSrOldProject($data){
        $sql = "INSERT INTO flguploa_firstlogicis.tbl_service_report
            (`no`, `company_id`, `type`, `times`, `customer_company_id`, `customer_company_branch_id`, `engineer_id`, `service_job_type_id`, `product_id`, `status`, `contract_no`, `request_no`, `approve_time`, `symtom`, `action`, `note`, `additive_mail`, `suggestion`, `appointment_time`, `expect_duration`, `follow`, `follow_date`, `follow_reason`, `has_replace_part`, `close_job_time`, `last_update`, `picture_path`, `contact_person_first_name`, `contact_person_last_name`, `contact_person_phone_1`, `contact_person_phone_2`, `contact_person_email`, `has_overscope`, `is_signed`, `is_back_home`, `is_keep_working`, `updatetimestamp`, `assign_times`, `last_notification_timestamp_onsite`, `last_notification_timestamp_close_job`, `last_notification_timestamp_customer_sign`, `last_notification_timestamp_finish`, `taxi_go_trip_fare`, `taxi_return_trip_fare`, `create_by`, `create_time`, `modified_by`, `modified_time`, `is_cancel`)
            VALUES
            (':no','1',
             ':type','1',
             ':customer_company_id',
             ':customer_company_branch_id',
             ':engineer_id',
             ':service_job_type_id',
             ':product_id',
             ':status',
             ':contract_no',
             ':request_no',
             ':approve_time',
             ':symtom',
             ':action',
             ':note',
             ':additive_mail',
             ':suggestion',
             ':appointment_time',
             ':expect_duration',
             ':follow',
             ':follow_date',
             ':follow_reason',
             ':has_replace_part',
             ':close_job_time',
             ':last_update',
             ':picture_path',
             ':contact_person_first_name',
             ':contact_person_last_name',
             ':contact_person_phone_1',
             ':contact_person_phone_2',
             ':contact_person_email',
             ':has_overscope',
             ':is_signed',
             ':is_back_home',
             ':is_keep_working',
             ':updatetimestamp',
             ':assign_times',
             ':last_notification_timestamp_onsite',
             ':last_notification_timestamp_close_job',
             ':last_notification_timestamp_customer_sign',
             ':last_notification_timestamp_finish',
             ':taxi_go_trip_fare',
             ':taxi_return_trip_fare',
             ':create_by', NOW(),
             ':modified_by', NOW(), '0') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':no'=>$data['service_report_id'],
            ':type'=>'',
            ':customer_company_id'=>$this->getEndUserCompanyOldProject($data['end_user_company']),
            ':customer_company_branch_id'=>$this->getSiteOldProject($data['end_user_site']),
            ':engineer_id'=>$this->getEmpnoOldProject($data['assign_individual']),
            ':service_job_type_id'=>'10',
            ':product_id'=>'',
            ':status'=>'0',
            ':contract_no'=>$data['contract_no'],
            ':request_no'=>$data['case_id'],
            ':approve_time'=>'',
            ':symtom'=>$data['symtom'],
            ':action'=>'',
            ':note'=>'',
            ':additive_mail'=>'',
            ':suggestion'=>'',
            ':appointment_time'=>$data['appointed_time'],
            ':expect_duration'=>'',
            ':follow'=>'',
            ':follow_date'=>'',
            ':follow_reason'=>'',
            ':has_replace_part'=>'',
            ':close_job_time'=>'',
            ':last_update'=>'',
            ':picture_path'=>'',
            ':contact_person_first_name'=>$data['end_user_name'],
            ':contact_person_last_name'=>'',
            ':contact_person_phone_1'=>$data['end_user_phone'],
            ':contact_person_phone_2'=>'',
            ':contact_person_email'=>$data['end_user_email'],
            ':has_overscope'=>'',
            ':is_signed'=>'',
            ':is_back_home'=>'',
            ':is_keep_working'=>'',
            ':updatetimestamp'=>'',
            ':assign_times'=>'',
            ':last_notification_timestamp_onsite'=>'',
            ':last_notification_timestamp_close_job'=>'',
            ':last_notification_timestamp_customer_sign'=>'',
            ':last_notification_timestamp_finish'=>'',
            ':taxi_go_trip_fare'=>'',
            ':taxi_return_trip_fare'=>'',
            ':create_by'=>'',
            ':modified_by'=>'',
            ));
        // $q->fetch();
    }

    private function selectNewTask($data){
        $sql = "SELECT * FROM tasks WHERE sr_refer_remedy = :sr_refer_remedy ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sr_refer_remedy'=>$data['service_report_id']));
        return $r = $q->fetch();
    }

    private function insertNewTask($data){
        if($this->getTicketSidFromHD($data['case_id'])){
        $sql = "INSERT INTO tasks ( no_task, sr_refer_remedy,  ticket_sid, create_by, create_datetime, appointment, expect_duration, expect_finish,  service_type_refer_remedy, subject_service_report, end_user_contact_name_service_report, end_user_phone_service_report,  end_user_email_service_report, end_user_company_name_service_report, last_status, updated_by ) VALUES (:no_task,:sr_refer_remedy, :ticket_sid, :create_by,NOW(), :appointment, :expect_duration, :expect_finish,
                     :service_type_refer_remedy, :subject_service_report, :end_user_contact_name_service_report, :end_user_phone_service_report,
                     :end_user_email_service_report, :end_user_company_name_service_report,
                     :last_status, :updated_by )";
            $q = $this->db->prepare($sql);
            $q->execute( array(
                ':no_task'=> genTaskNo($this->db),
                ':sr_refer_remedy'=> $data['service_report_id'],
                ':ticket_sid'=> $this->getTicketSidFromHD($data['case_id']),
                ':create_by'=> $this->helpGetEmailFromThainame($data['assign_individual']),
                ':appointment'=> $this->convertYearToInter($data['appointed_time']),
                ':expect_duration'=>$data['expect_finish'],
                ':expect_finish'=> $data['appointed_time'],
                ':service_type_refer_remedy' => $data['service_type'],
                ':subject_service_report' => $data['symtom'],
                ':end_user_contact_name_service_report' => $data['end_user_name'],
                ':end_user_phone_service_report' => $data['end_user_phone'],
                ':end_user_email_service_report' => $this->splitPhoneEmail($data['end_user_phone']),
                ':end_user_company_name_service_report' => $data['end_user_company'],
                ':last_status' => '',
                ':updated_by' => ''
            ));
        }
    }

    private function getEndUserCompanyOldProject($data){
        $sql = "SELECT * from flguploa_firstlogicis.tbl_customer_company where name_th = '".$data."'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;

    }
    private function getSiteOldProject($data){
        $sql = "SELECT * from flguploa_firstlogicis.tbl_customer_company_branch where address = '".$data."'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;
    }


    private function getEmpnoFromEmailOldProject($data){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_employee WHERE email = '".$data."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;
    }

    private function getTicketSidFromHD($data){ //find ticket_sid from HD
        $sql = "SELECT * FROM ticket WHERE refer_remedy_hd = :ticket_no";
        $q = $this->db->prepare($sql);
        $q->execute(array( ':ticket_no' => $data['case_id']));
        $r = $q->fetch();
        if($r['sid']){
            return $r['sid'];
        }
        return NULL;
    }

    private function getEmpnoOldProject($data){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_employee WHERE CONCAT(first_name_th,'  ',last_name_th) = '".$data."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;
    }
    private function selectSrFromSequenceId($sequence_id){
        $sql = "SELECT * FROM test_create_sr WHERE sid = '".$sequence_id."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $q->fetch();
    }

    public function insertToTestCreateHd($Subject,$Case_ID,$Urgency,$Requester_Name,$Requester_Phone,$Requester_Mobile,$Requester_EMail,$Requester_Company_Name,$EndUser_Site,$EndUser_Name,$EndUser_Phone,$EndUser_Company_Name,$Contract_No,$Serial_No,$Incharge,$Description,$Status,$Assigned_Time,$Report_By,$Create_Time,$Prime_Contract,$Case_Type,$First_Assigned_Time,$Work_Log,$Expect_Pending_Finish){
        // $objConnect = mysql_connect("127.0.0.1","flguploa","sss123") or die(mysql_error());

        // $objDB = mysql_select_db("flguploa_cases");
        // mysql_query("SET NAMES UTF8");
        // mysql_query("SET character_set_results=utf8");
        // mysql_query("SET character_set_client=utf8");
        // mysql_query("SET character_set_connection=utf8");

        // $strSQL = "SELECT sid FROM test_create_case WHERE case_id = '".$Case_ID."' ";
        // $objQuery = mysql_query($strSQL);
        // $row = mysql_fetch_assoc($objQuery);

        $strSQL = "INSERT INTO test_create_case
                (subject, case_type, create_by, create_datetime, case_id, urgency, requester_name, requester_phone, requester_mobile, requester_email, requester_company_name, enduser_site, enduser_name, enduser_phone, enduser_company_name, contract_no, serial_no, incharge, description, status, assigned_time, report_by, create_time, prime_contract, first_assigned_time, work_log,encoding,expect_pending_finish)
                VALUES
                (:subject,:case_type,'',NOW(),:Case_ID,:Urgency,:Requester_Name,:Requester_Phone,:Requester_Mobile,:Requester_EMail,:Requester_Company_Name,:EndUser_Site,:EndUser_Name,:EndUser_Phone,:EndUser_Company_Name,:Contract_No,:Serial_No,:Incharge,:Description,:Status,:Assigned_Time,:Report_By,:Create_Time,:Prime_Contract,:First_Assigned_Time,:Work_Log, :encoding,:Expect_Pending_Finish)";

        // $objQuery = mysql_query($strSQL) or die (mysql_error());
        $q = $this->db->prepare($strSQL);
        return $q->execute(array(
                ':subject'=>$Subject,
                ':case_type'=>$Case_Type,
                ':Case_ID'=>$Case_ID,
                ':Urgency'=>$Urgency,
                ':Requester_Name'=>$Requester_Name,
                ':Requester_Phone'=>$Requester_Phone,
                ':Requester_Mobile'=>$Requester_Mobile,
                ':Requester_EMail'=>$Requester_EMail,
                ':Requester_Company_Name'=>$Requester_Company_Name,
                ':EndUser_Site'=>$EndUser_Site,
                ':EndUser_Name'=>$EndUser_Name,
                ':EndUser_Phone'=>$EndUser_Phone,
                ':EndUser_Company_Name'=>$EndUser_Company_Name,
                ':Contract_No'=>$Contract_No,
                ':Serial_No'=>$Serial_No,
                ':Incharge'=>$Incharge,
                ':Description'=>$Description,
                ':Status'=>$Status,
                ':Assigned_Time'=>$Assigned_Time,
                ':Report_By'=>$Report_By,
                ':Create_Time'=>$Create_Time,
                ':Prime_Contract'=>$Prime_Contract,
                ':First_Assigned_Time'=>$First_Assigned_Time,
                ':Work_Log'=>$Work_Log,
                ':encoding'=>mb_detect_encoding($Assign_individual),
                ':Expect_Pending_Finish'=>$Expect_Pending_Finish,
            ));
    }

    public function checkRepeatHd(){
         $strSQL = "SELECT sid FROM test_create_case WHERE case_id = :case_id ";
         $q = $this->db->prepare($strSQL);
         $q->execute(array(':case_id'=>$Case_ID));
         $r = $q->fetch();
         return $r;
    }

    public function insertToTestCreateSr($Service_Report_ID,$Case_ID,$Symtom,$Contract_No,$End_User_Company,$End_User_Site,$End_User_Name,$End_User_Phone,$Assign_individual,$Involved_Eng2,$Involved_Eng3,$Involved_Eng4,$Involved_Eng5,$Appointed_time,$Service_Type,$Expect_Finish,$Submitted_by,$Serial,$Detail,$Status){

        $strSQL = "INSERT INTO test_create_sr (
        create_datetime, service_report_id, case_id, symtom, contract_no, end_user_company, end_user_site, end_user_name, end_user_phone, end_user_email, assign_individual, involved_eng2, involved_eng3, involved_eng4, involved_eng5, appointed_time, service_type, expect_finish, submitted_by, serial_no, detail, status, encoding)
        VALUES (
        NOW(),:Service_Report_ID,:Case_ID,:Symtom,:Contract_No,:End_User_Company,:End_User_Site,:End_User_Name,:End_User_Phone,'',:Assign_individual,:Involved_Eng2,:Involved_Eng3,:Involved_Eng4,:Involved_Eng5,:Appointed_time,:Service_Type,:Expect_Finish,:Submitted_by,:Serial_no,:Detail,:Status,:encoding)";

        $q = $this->db->prepare($strSQL);
        return $q->execute(array(
                ':Service_Report_ID'=>$Service_Report_ID,
                ':Case_ID'=>$Case_ID,
                ':Symtom'=>$Symtom,
                ':Contract_No'=>$Contract_No,
                ':End_User_Company'=>$End_User_Company,
                ':End_User_Site'=>$End_User_Site,
                ':End_User_Name'=>$End_User_Name,
                ':End_User_Phone'=>$End_User_Phone,
                ':End_User_Phone'=>$End_User_Phone,
                ':Assign_individual'=>$Assign_individual,
                ':Involved_Eng2'=>$Involved_Eng2,
                ':Involved_Eng3'=>$Involved_Eng3,
                ':Involved_Eng4'=>$Involved_Eng4,
                ':Involved_Eng5'=>$Involved_Eng5,
                ':Appointed_time'=>$Appointed_time,
                ':Service_Type'=>$Service_Type,
                ':Expect_Finish'=>$Expect_Finish,
                ':Submitted_by'=>$Submitted_by,
                'Serial_no'=>$Serial,
                'Detail'=>$Detail,
                'Status'=>$Status,
                ':encoding'=>mb_detect_encoding($Assign_individual),
            ));

    }

    public function servicereports($permission, $data){
        try{
            $search_refer_remedy_hd = isset($data['search_refer_remedy_hd'])?$data['search_refer_remedy_hd']:'';
            $filter_refer_remedy_hd = !empty($search_refer_remedy_hd)?" AND TICKET.refer_remedy_hd LIKE :filter_refer_remedy_hd ":'';
            if(isset($data['filter']) && $data['filter']=="onlyMe"){
                require_once "db_handler.php";

                $db_handler = new DbHandler();
                $r = $db_handler->listTaskWorking($data['email']);

                return $r;
            }else{


                $queryStatus = ' AND (
                T.customer_signated = 0 AND (
                    T.closed_datetime = "0000-00-00 00:00:00"
                    OR DATEDIFF(NOW(), T.closed_datetime) < 1
                    )
                ) ';

                if($data['status']==500){
                    //COMPLETED
                    $queryStatus = ' AND T.customer_signated = 1  ';
                }else if($data['status']==800){
                    //Unsigned
                    $queryStatus = ' AND (T.customer_signated = 0 AND T.closed_datetime <> "0000-00-00 00:00:00" ) ';
                }else if($data['status']==1000){
                    $queryStatus = '';
                }

                $permissionQuery = 'AND (T.engineer = :email OR T.associate_engineer LIKE "%'.$data['email'].'%") ';

                if(isset($data['filter']) && $data['filter']=="onlyMe"){

                }else{
                    if($permission=="0" || $permission=="0" || $permission=="" || $permission==""){

                    }else{
                        $permissionQuery = 'AND (T.engineer = :email OR T.associate_engineer LIKE "%'.$data['email'].'%" OR U.role_sid IN ('.$permission.')) ';
                    }
                }

                if($data['email']=='autsakorn.t@firstlogic.co.th'){
                    $permissionQuery = ' ';
                }

                $type = '';
                $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';
                $type = "";
                if(isset($data['type']) && $data['type']!=""){
                    $explodeType = explode(",",$data['type']);
                    foreach($explodeType as $key => $value){
                        if($key!==0){
                            $type .= ",";
                        }
                        $type .= "'".$value."'";
                    }
                }

                $queryType = '';
                if($type!==""){
                    $queryType = "AND TICKET.case_type IN (".$type.") ";
                }

                $search = "";
                if(isset($data['search']) && $data['search']!=""){
                    $search .= " AND (T.no_task LIKE '%".$data['search']."%'
                    OR T.subject_service_report LIKE '%".$data['search']."%'
                    OR U.formal_name LIKE '%".$data['search']."%'
                    OR U.email LIKE '%".$data['search']."%'
                    ) ";
                }

                $sql = 'SELECT T.sid,T.sid tasks_sid,T.create_datetime,T.appointment,T.subject_service_report,T.subject_service_report name,CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name,CASE WHEN T.customer_signated = "1" THEN "Done" ELSE "Process" END status_label  ';

                $sqlCreateDate = "";
                if(isset($data['startDate']) && isset($data['endDate'])){
                    $sqlCreateDate .= "AND (T.create_datetime BETWEEN '".$data['startDate']."' AND '".$data['endDate']."') ";
                }

                $sql .= ',TICKET.contract_no, TICKET.no_ticket, TICKET.refer_remedy_hd, T.no_task,T.path_service_report,T.customer_signated ';
                $sql .= ', TICKET.incident_location end_user_site';
                $sql .= ',CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END engineer_name ';
                $sql .= $create_datetime;
                $sql .= ',DATE_FORMAT(T.started_datetime, "%Y-%m-%d %H:%i") started_datetime ';
                $sql .= ',DATE_FORMAT(T.closed_datetime, "%Y-%m-%d %H:%i") closed_datetime ';
                $sql .= ', DATE_FORMAT(T.appointment,"%Y-%m-%d %H:%i") appointment ';
                $sql .= ',U.picture_profile engineer_pic ';
                $sql .= ',T.subject_service_report subject ';
                $sql .= ',T.end_user_contact_name_service_report end_user_name ';
                $sql .= ',T.service_report_address end_user_site ';
                $sql .= 'FROM tasks T ';
                $sql .= 'LEFT JOIN user U2 ON U2.email = T.create_by ';
                $sql .= 'LEFT JOIN ticket TICKET ON TICKET.sid = T.ticket_sid ';
                $sql .= 'LEFT JOIN user U ON U.email = T.engineer ';
                $sql .= 'WHERE 1 ';
                $sql .= $filter_refer_remedy_hd;
                $sql .= $permissionQuery;
                $sql .= 'AND appointment_type IN (0,1) ';
                $sql .= $queryStatus;
                $sql .= $queryType;
                $sql .= $search;
                $sql .= $sqlCreateDate;
                $sql .= 'ORDER BY T.appointment DESC LIMIT '.$data['dataStart'].','.$data['dataLimit'];
                $params = array(':email'=>$data['email']);
                !empty($search_refer_remedy_hd)&&$params[':filter_refer_remedy_hd'] = "%".$search_refer_remedy_hd."%";
                $q = $this->db->prepare($sql);
                $q->execute($params);
                return $q->fetchAll();
            }
        }catch(Expection $e){
            echo $e->message();
            return array();
        }
    }

    public function editServiceReport($authen, $data){
        $asso = '';
        if(isset($data['associate_engineer'])){
            foreach($data['associate_engineer'] as $key => $value){
                if($asso!=""){
                    $asso .= ",";
                }
                $asso .= $value['engineer'];
            }
        }
        try{
            $sql = "UPDATE tasks SET ";
            $sql .= "subject_service_report = :e1 ";
            $sql .= ",appointment = :e2 ";
            $sql .= ",expect_duration = :e3 ";
            $sql .= ",engineer = :e4 ";
            $sql .= ",end_user_contact_name_service_report = :e5 ";
            $sql .= ",end_user_email_service_report = :e6 ";
            $sql .= ",end_user_mobile_service_report = :e7 ";
            $sql .= ",end_user_phone_service_report = :e8 ";
            $sql .= ",end_user_company_name_service_report = :e9 ";
            $sql .= ",service_type = :e10 ";
            $sql .= ",associate_engineer = :e11 ";
            $sql .= ",updated_by = :e12 ";
            $sql .= ",updated_datetime = NOW() ";
            $sql .= ",service_report_address = :service_report_address ";
            $sql .= "WHERE sid = :e13 ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':e1'=>$data['subject_service_report'],
                ':e2'=>$data['appointment'],
                ':e3'=>$data['expect_duration'],
                ':e4'=>$data['engineer'],
                ':e5'=>$data['end_user_contact_name_service_report'],
                ':e6'=>$data['end_user_email_service_report'],
                ':e7'=>$data['end_user_mobile_service_report'],
                ':e8'=>$data['end_user_phone_service_report'],
                ':e9'=>$data['end_user_company_name_service_report'],
                ':e10'=>$data['service_type'],
                ':e11'=>$asso,
                ':e12'=>$authen['email'],
                ':service_report_address'=>$data['service_report_address'],
                ':e13'=>$data['sid']
            ));
        }catch(PDOException $e) {
            $error = $e->getMessage();
      	}
    }

    public function deletedEngieerFromTaskLogQuery($data, $authen){
        try{
            $sql = "UPDATE tasks_log SET deleted_engineer = engineer, deleted_engineer_datetime = NOW(),
            deleted_engineer_by = :e1, engineer = '' WHERE tasks_sid = :eK AND engineer <> '' ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':e1'=>$authen['email'],':eK'=>$data['sid']));
        }catch(PDOException $e) {
            echo $error = $e->getMessage();
      	}
    }
    public function addEngineerToTaskLogQuery($data, $authen){

        try{
            $sql = "INSERT INTO tasks_log
            (tasks_sid, engineer, create_by, create_datetime, datetime_to_report, appointment, expect_duration, status, request_taxi)
            VALUES (:e1, :e2, :e3, NOW(), NOW(), :e4, :e5, :e6, :e7) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':e1'=>$data['sid'],
                ':e2'=>$data['engineer'],
                ':e3'=>$authen['email'],
                ':e4'=>$data['appointment'],
                ':e5'=>$data['expect_duration'],
                ':e6'=>((isset($data['status_engineer']) && $data['status_engineer']!="")?$data['status_engineer']:0),
                ':e7'=>(($data['request_taxi'])?$data['request_taxi']:0)
            ));
        }catch(PDOException $e) {
            echo $error = $e->getMessage();
      	}
    }
    public function addEngineerToTasksEngineer($data, $authen){
        try{
            $sql = "INSERT INTO tasks_engineer
            (tasks_sid, engineer, request_taxi, status, create_datetime, updated_datetime)
            VALUES (:e1,:e2,:e3,0,NOW(),NOW()) ";
            $q = $this->db->prepare($sql);
            $q->execute(
                array(
                    ':e1'=>$data['sid'],
                    ':e2'=>$data['engineer'],
                    ':e3'=>((isset($data['request_taxi']))?$data['request_taxi']:0)
                )
            );
        }catch(PDOException $e) {
            echo $error = $e->getMessage();
      	}
    }
    public function addAssoEngineerToTaskLog($data, $authen){

            if(isset($data['associate_engineer']) && count($data['associate_engineer'])>0){
                foreach($data['associate_engineer'] as $key => $value){
                    $insertData['sid']=$data['sid'];
                    $insertData['engineer'] = $value['engineer'];
                    $insertData['appointment'] = $data['appointment'];
                    $insertData['expect_duration'] = $data['expect_duration'];
                    $insertData['status_enineer'] = $data['last_status'];
                    $insertData['request_taxi'] = $value['request_taxi'];

                    $this->addEngineerToTaskLogQuery($insertData, $authen);
                }
            }else{

            }

    }
    public function deleteAssoEngineer($data, $authen){
        try{
            $sql = "UPDATE tasks_engineer
            SET engineer_deleted = engineer,
            engineer = '', updated_datetime = NOW()
            WHERE tasks_sid = :tasks_sid AND engineer <> '' ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':tasks_sid'=>$data['sid']));
        }catch(PDOException $e) {
            $error = $e->getMessage();
      	}
    }
    public function addAssoEngineer($data, $authen){
        if(isset($data['associate_engineer'])){
            try{
                foreach($data['associate_engineer'] as $key => $value){
                    $sql = "INSERT INTO tasks_engineer
                    (tasks_sid, engineer, status, request_taxi,create_datetime, updated_datetime)
                    VALUES (:e1,:e2,:e3,:e4,NOW(),NOW())";
                    $q = $this->db->prepare($sql);
                    $q->execute(array(
                        ':e1'=>$data['sid'],
                        ':e2'=>$value['engineer'],
                        ':e3'=>"0",
                        ':e4'=>$value['request_taxi']
                    ));
                }
            }catch(PDOException $e) {
                $error = $e->getMessage();
            }
        }
    }

    public function engineerInTask($tasks_sid, $ignore_email){
        try{
            $sql = 'SELECT T.*,
                (SELECT status FROM tasks_log TL WHERE TL.tasks_sid = T.tasks_sid AND TL.engineer = T.engineer
                ORDER BY TL.sid DESC LIMIT 0,1) status_enineer ,
                CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END engineer_name,U.picture_profile engineer_pic ';
            $sql .= ' FROM tasks_log T LEFT JOIN user U ON T.engineer = U.email
                WHERE T.tasks_sid = :sid AND T.engineer != :engineer AND T.engineer <> "" GROUP BY T.engineer ';
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$tasks_sid,':engineer'=>$ignore_email));
            $r = $q->fetchAll();
            return $r;
            // db.query(sql,[tasks_sid, ignore_email], function(e, r, f){
            //     if(e) reject(e);
            //     else resolve(r);
            // });
        }catch(PDOException $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }
    public function serviceReportDetail($data){
         try{
            $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';

            $sql = 'SELECT T.*,DATE_FORMAT(T.appointment, "%Y-%m-%d %H:%i") appointment ';
            $sql .= $create_datetime;
            $sql .= ',DATE_FORMAT(T.started_datetime, "%Y-%m-%d %H:%i") started_datetime ';
            $sql .= ',DATE_FORMAT(T.closed_datetime, "%Y-%m-%d %H:%i") closed_datetime ';
            $sql .= ',CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END engineer_name ';
            $sql .= ',CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name ';
            $sql .= ',ST.name service_type_name ';
            $sql .= ',CASE WHEN T.started_datetime = "0000-00-00 00:00:00" AND T.last_status < 300 THEN 1 ELSE 0 END can_edit ';
            $sql .= ',(SELECT CASE WHEN TL.request_taxi IS NOT NULL THEN TL.request_taxi ELSE 0 END request_taxi FROM tasks_log TL WHERE TL.tasks_sid = T.sid AND TL.engineer = T.engineer ORDER BY TL.sid DESC LIMIT 0,1) request_taxi ';
            $sql .= ',(SELECT CASE WHEN TL.status IS NOT NULL THEN TL.status ELSE 0 END status FROM tasks_log TL WHERE TL.tasks_sid = T.sid AND TL.engineer = T.engineer ORDER BY TL.sid DESC LIMIT 0,1) status_enineer ';
            $sql .= ',TT.subject,TT.no_ticket,TT.case_type ticket_case_type, TT.refer_remedy_hd,TT.serial_no ticket_serial,TT.contract_no ';
            $sql .= 'FROM tasks T LEFT JOIN user U ON U.email = T.engineer ';
            $sql .= 'LEFT JOIN user U2 ON T.create_by = U2.email ';
            $sql .= 'LEFT JOIN service_type ST ON T.service_type = ST.sid ';
            $sql .= 'LEFT JOIN ticket TT ON T.ticket_sid = TT.sid ';
            $sql .= 'WHERE T.sid = :sid AND T.engineer <> "" ORDER BY T.appointment ASC ';

            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$data['tasks_sid']));
            $r = $q->fetch();
            $r['associate_engineer'] = $this->engineerInTask($data['tasks_sid'], $r['engineer']);

            $r['engineer_log_time_stamp'] = $this->logTimeStamp($data['tasks_sid'], $r['engineer']);
            $r['input_action'] = $this->getLastInputAction($data['tasks_sid']);
            $r['task_do_serial_pm'] = $this->taskDoSerialPM($data['tasks_sid']);

            foreach ($r['associate_engineer'] as $key => $value) {
                $tmp = $this->logTimeStamp($data['tasks_sid'], $value['engineer']);
                $r['associate_engineer'][$key]['engineer_log_time_stamp'] = $this->logTimeStamp($data['tasks_sid'], $value['engineer']);
            }
            return $r;
        }catch(PDOException $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }
    private function taskDoSerialPM($task_sid){
      $sql = "SELECT TDSP.*,TDSP.sid task_do_serial_pm_sid,TS.*,TS.sid ticket_serial_sid
      FROM task_do_serial_pm TDSP LEFT JOIN ticket_serial TS ON TDSP.ticket_serial_sid = TS.sid WHERE TDSP.task_sid = :task_sid";
      $q = $this->db->prepare($sql);
      $q->execute(array(':task_sid'=>$task_sid));
      $r = $q->fetchAll();
      return $r;
    }
    private function getLastInputAction($task_sid){
      $sql = "SELECT solution FROM task_input_action WHERE task_sid = :task_sid ORDER BY sid DESC LIMIT 0,1";
      $q = $this->db->prepare($sql);
      $q->execute(array(':task_sid'=>$task_sid));
      $r = $q->fetch();
      return $r['solution'];
    }
    private function logTimeStamp($tasks_sid, $email){
        $sql = "SELECT * FROM tasks_log TL WHERE TL.tasks_sid = :tasks_sid
        AND TL.engineer = :engineer ORDER BY TL.status ASC ";

        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid, ':engineer'=>$email));
        $r = $q->fetchAll();
        return $r;
    }

    public function editContactAfterClose($data, $tasks_sid){
        try {
            $sql = "UPDATE tasks
            SET end_user_contact_name_service_report = :e1,
            end_user_email_service_report = :e2,
            end_user_phone_service_report = :e3,
            end_user_mobile_service_report = :e4,
            end_user_company_name_service_report = :e5
            WHERE sid = :eK ";

            $q = $this->db->prepare($sql);
            $q->execute(
                array(
                ':e1'=>$data['end_user_contact_name_service_report'],
                ':e2'=>$data['end_user_email_service_report'],
                ':e3'=>$data['end_user_phone_service_report'],
                ':e4'=>$data['end_user_mobile_service_report'],
                ':e5'=>$data['end_user_company_name_service_report'],
                ':eK'=>$tasks_sid
                )
            );

        }catch(PDOException $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }

    public function deleteService($data){
        try{
            $sql = "UPDATE tasks SET appointment_type = '-1',updated_datetime = NOW(), updated_by = :email
            WHERE sid = :sid ";

            $q = $this->db->prepare($sql);
            $q->execute(
                array(
                    ':email'=>$data['email'],
                    ':sid'=>$data['sid']
                )
            );
        }catch(PDOException $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }

    public function findExpressReport($name, $c1, $c2){
        try{
            $sql = "SELECT T.no_task,TT.no_ticket,TT.subject,TT.end_user,TT.contract_no
            FROM tasks T
            LEFT JOIN ticket TT ON T.ticket_sid = TT.sid
            WHERE (appointment_type = 0 OR appointment_type = 1) AND
            appointment > '2017-04-01 00:00:00' AND engineer LIKE :email AND
            SUBSTR(TT.contract_no,1,1) <> :c1 AND SUBSTR(TT.contract_no,1,1) <> :c2 ";

            $q = $this->db->prepare($sql);
            $q->execute(
                array(
                    ':email'=>$name."%",
                    ':c1'=>$c1,
                    ':c2'=>$c2
                )
            );
            return $q->fetchAll();
        }catch(PDOException $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }

    public function reportSr($firstCharContract, $period_start, $period_end, $incharge_group){
        try{
          echo $incharge_group;
            $sql = "SELECT TI.contract_no,
            (SELECT customer FROM project P WHERE P.sid = TI.project_sid ORDER BY P.sid DESC LIMIT 0,1) customer,
            TI.end_user, T.no_task, T.subject_service_report subject, T.started_datetime, T.closed_datetime,
            U.formal_name eng1,
            (SELECT AU2.formal_name FROM tasks_engineer TE LEFT JOIN user AU2 ON AU2.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 1,1) eng2,
            (SELECT AU3.formal_name FROM tasks_engineer TE LEFT JOIN user AU3 ON AU3.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 2,1) eng3,
            (SELECT AU4.formal_name FROM tasks_engineer TE LEFT JOIN user AU4 ON AU4.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 3,1) eng4,
            (SELECT AU5.formal_name FROM tasks_engineer TE LEFT JOIN user AU5 ON AU5.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 4,1) eng5,
            (SELECT AU6.formal_name FROM tasks_engineer TE LEFT JOIN user AU6 ON AU6.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 5,1) eng6,
            (SELECT AU7.formal_name FROM tasks_engineer TE LEFT JOIN user AU7 ON AU7.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 6,1) eng7,
            (SELECT AU8.formal_name FROM tasks_engineer TE LEFT JOIN user AU8 ON AU8.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 7,1) eng8,
            (SELECT AU9.formal_name FROM tasks_engineer TE LEFT JOIN user AU9 ON AU9.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 8,1) eng9,
            (SELECT AU10.formal_name FROM tasks_engineer TE LEFT JOIN user AU10 ON AU10.email = TE.engineer WHERE TE.tasks_sid = T.sid
            ORDER BY TE.sid ASC LIMIT 9,1) eng10, U.email, GIT.group_name
            FROM tasks T LEFT JOIN ticket TI ON T.ticket_sid = TI.sid
            LEFT JOIN user U ON T.engineer = U.email OR T.engineer = U.email_2
            INNER JOIN gable_incharge_team GIT ON GIT.email = U.email OR GIT.email = U.email_2
            WHERE substr(TI.contract_no, 1, 2) = :firstCharContract
            AND T.started_datetime >= :period_start AND T.started_datetime <= :period_end
            AND T.engineer NOT LIKE 'autsakorn.t%'
            AND T.engineer NOT LIKE 'sarinaphat.t%'
            AND T.engineer NOT LIKE 'teerawut.p%'
            AND T.engineer NOT LIKE 'natnicha.d%'
            AND GIT.group_name LIKE :group_name
            AND (T.appointment_type = 0 OR T.appointment_type = 1)
            GROUP BY T.no_task ORDER BY T.started_datetime ASC ";
// -- AND GIT.group_name LIKE :group_name
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':firstCharContract'=>$firstCharContract,
                ':period_start'=>$period_start,
                ':period_end'=>$period_end,
                ':group_name'=>$incharge_group.'%'
                ));
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }

    public function serviceReportEditAddress($data){
      if(isset($data['address']) && isset($data['email']) && isset($data['service_report_sid'])){
        $sql = "UPDATE tasks SET service_report_address = :address, updated_by = :email, updated_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':address'=>$data['address'],':email'=>$data['email'],':sid'=>$data['service_report_sid']));

        $sql = "UPDATE customer_signature_task SET gen_pdf = 0 WHERE task_sid = :service_report_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':service_report_sid'=>$data['service_report_sid']));
        return array('message'=>'แก้ไขเรียบร้อยแล้ว');
      }else{
        return array('message'=>'ข้อมูลไม่เพียงพอในการแก้ไข');
      }
    }
}
?>
