<?php
class DaysModel{
	private $db;
	private $m;

	function __construct() {
        require_once 'db_connect.php';
        // opening db connection
        $this->m = new DbConnect();
        $this->db = $this->m->connect();
 	}

 	function listDays(){
 		$sql = "SELECT * FROM days ORDER BY day";
 		$q = $this->db->prepare($sql);
 		$q->execute();
 		return $q->fetchAll();
 	}

 	public function listHoliday(){
 		$sql = "SELECT * FROM tbl_holiday WHERE holiday LIKE '2017-%' ORDER BY holiday";
 		$q = $this->db->prepare($sql);
 		$q->execute();
 		return $r = $q->fetchAll();
 	}

 	public function genDays(){
 		$date = array();
        for($i=0;$i<400;$i++){
            $sql = "SELECT DATE_FORMAT(date_add('2017-01-01 00:00:00',interval ".$i." day),'%Y-%m-%d') date_Ymd,
                DATE_FORMAT(date_add('2017-01-01 00:00:00',interval ".$i." day),'%d.%b.%Y') date_dmY,
                CASE DATE_FORMAT(date_add('2017-01-01 00:00:00',interval ".$i."  day),'%w')  
                WHEN 0 THEN 'Sunday' 
                WHEN 1 THEN 'Monday' 
                WHEN 2 THEN 'Tueday' 
                WHEN 3 THEN 'Wednesday' 
                WHEN 4 THEN 'Thursday' 
                WHEN 5 THEN 'Friday'  
                ELSE 'Saturday' END date_w ";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetch();
			array_push($date,$r);
		}
		return $date;
 	}
 	public function insertDays($data){
 		$sql = "INSERT INTO days (day,name,is_holiday) VALUES (:day,:name,0) ";
 		$q = $this->db->prepare($sql);
 		$q->execute(array(':day'=>$data['date_Ymd'],':name'=>$data['date_w']));
 	}

 	public function updateDate($sid, $name, $is_holiday){
 		$sql = "UPDATE days SET  name = :name WHERE sid = :sid ";
 		$q = $this->db->prepare($sql);
 		$q->execute(array(':name'=>$name, ':sid'=>$sid));
 	}

 	public function standbyBuddy(){
 		$sql = "SELECT * FROM standby_buddy ";
 		$q = $this->db->prepare($sql);
 		$q->execute();
 		return $q->fetchAll();
 	}

 	public function insertAppointment($dataArray){
 		$sql = "INSERT INTO tasks (subject_service_report,engineer, appointment, expect_duration, appointment_type, days_sid, standby_buddy_sid, service_type, create_datetime, create_by, ticket_sid) VALUES (:subject_service_report,:engineer,:appointment,:expect_duration,:appointment_type,:days_sid, :standby_buddy_sid,'2',now(),'autsakorn.t@firstlogic.co.th','7398') ";
 		$q = $this->db->prepare($sql);

 		$appointment = $dataArray['day']." ".$dataArray['appointment'].":00";
 		$standby_buddy_sid = $dataArray['staff']['sid'];

 		$q->execute(array(
 				':subject_service_report'=>$dataArray['subject'],
 				':engineer'=>$dataArray['staff']['buddy_one'],
 				':appointment'=>$appointment,
 				':expect_duration'=>$dataArray['expect_duration'],
 				':appointment_type'=>'2',
 				':days_sid'=>$dataArray['days_sid'],
 				':standby_buddy_sid'=>$standby_buddy_sid,
 			));
 		$tasksSid = $this->db->lastInsertId();
 		$this->insertAppointmentLog($tasksSid, $dataArray['staff']['buddy_one'],$appointment, $dataArray['expect_duration']);

 		$q->execute(array(
	 			':subject_service_report'=>$dataArray['subject'],
 				':engineer'=>$dataArray['staff']['buddy_two'],
 				':appointment'=>$appointment,
 				':expect_duration'=>$dataArray['expect_duration'],
 				':appointment_type'=>'2',
 				':days_sid'=>$dataArray['days_sid'],
 				':standby_buddy_sid'=>$standby_buddy_sid,
 			));
 		$tasksSid = $this->db->lastInsertId();
 		$this->insertAppointmentLog($tasksSid, $dataArray['staff']['buddy_two'],$appointment, $dataArray['expect_duration']);

 	}

 	public function insertAppointmentLog($tasksSid, $engineer,$appointment, $expect_duration){
 		$sql = "INSERT INTO tasks_log (tasks_sid, engineer,create_datetime, create_by, appointment,expect_duration,status ) 
 		VALUES (:tasks_sid, :engineer, NOW(), :create_by, :appointment,:expect_duration,'0') ";
 		$q = $this->db->prepare($sql);
 		$q->execute(
 			array(
 				':tasks_sid'=>$tasksSid,
 				':engineer'=>$engineer,
 				':create_by'=>$engineer,
 				':appointment'=>$appointment,
 				':expect_duration'=>$expect_duration
 				)
 			);
 	}
}
?>