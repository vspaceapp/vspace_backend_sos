<?php
// include "/var/www/html/vlogic/public_html/apis/libs/simple_html_dom.php";
class CaseTypeModel{
    private $db;
    private $objCons;
    private $email;
    public $permission_role;
    function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->email = $email;
        if($this->email!=""){
            $this->permission_role = $this->objCons->permissionRoleByEmail($this->email); 
        }
    }

      public function getProjectParse($email){
        $sql = "SELECT * FROM project_parse WHERE prefix = :permission_role";   
        
        $q = $this->db->prepare($sql);
        $q->execute(array(':permission_role'=> $this->permission_role['prefix_table'] ));
        $data = $q->fetchAll();

        // $data = $this->addAttendanceMeeting($email, $lastInsertId , $invite_meeting );
        foreach ($data as $key => $value) {
            $data[$key]['case_type'] = $this->getCaseTypeOfProjectParse($value['sid']);
        }
         return $data;
    }



    public function listcasetype($email){

        $sql = "SELECT * FROM case_type CT WHERE CT.create_by = :email OR create_by ='' GROUP BY name";
        $q = $this->db->prepare($sql);           
        $q->execute(array(':email'=> $email));
        $data = $q->fetchAll();
        return $data;
    }


    public function insertCaseType($project_parse_sid, $case_type_name){
        if($case_type_name == ""){
            return;
        }
        $sql = "INSERT INTO case_type ( name, description, bu_team_sid, create_datetime, create_by, status, project_parse_sid)
                VALUES ( :name, :description, '1', NOW() , :create_by, '100', :project_parse_sid)";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':name'=> trim($case_type_name),
            ':description' => trim($case_type_name),
            ':create_by' => $this->email,
            ':project_parse_sid' => $project_parse_sid
        ));   

        $lastInsertId = $this->db->lastInsertId();

        return  $lastInsertId ;

    }

    public function editCasetype($case_type_sid, $case_type_name){
         if($case_type_name == ""){
            return;
        }
        $sql = "UPDATE case_type SET name = :case_type_name , description = :case_type_name WHERE sid = :case_type_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':case_type_name' => trim($case_type_name),
                ':case_type_sid' => $case_type_sid
            ));
    }

    public function deleteCasetype($case_type_sid){
        $sql = "DELETE FROM case_type WHERE sid = :case_type_sid";
        $q = $this->db->prepare($sql);
        $q->execute( array(
                ':case_type_sid' => $case_type_sid
            ));
    }


    private function getCaseTypeOfProjectParse($projectParsesid){
        $sql = "SELECT * FROM case_type CT    
                WHERE CT.project_parse_sid = :project_parse_sid GROUP BY CT.name";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_parse_sid'=> $projectParsesid  ));
        $r = $q->fetchAll();
        return $r;
    }


        

    public function getInfoEmployee($email){
        return $this->objCons->getInfoEmployee($email);
    }


    public function addCaseType($email, $case_type_name){

      if(!$this->checkRepeatCaseType($email, $case_type_name)){
           $sql = "INSERT INTO case_type (name, create_by, create_datetime) VALUES (:name, :create_by, NOW() ) ";
           $q = $this->db->prepare($sql);
           $q->execute(array(
            ':name' => $case_type_name,
            ':create_by'=>$email
            ));

      }
    }

    private function checkRepeatCaseType($email, $case_type_name){
        $sql = "SELECT * FROM case_type WHERE name = :case_type_name AND create_by = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':case_type_name'=> $case_type_name,
                          ':email' => $email
                           ));

        $r = $q->fetchAll();

        return $r;
    
    }
}
?>