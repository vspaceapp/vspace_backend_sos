<?php
/**
 *
 */
class ReportModel
{

  function __construct()
  {
    require_once __dir__.'/db_connect.php';
    // opening db connection
    $this->m = new DbConnect();
    $this->db = $this->m->connect();
  }

  public function getCusSatData($start,$end){
        $sql = "SELECT * FROM vw_report_cus_sat WHERE create_datetime BETWEEN :startdatetime AND :enddatetime";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':startdatetime'=>$start,
           ':enddatetime'=>$end
         ));
         $r = $q->fetchAll();
         return $r;
 }

   public function getProductCatReport($start,$end){
         $sql = "SELECT ticket.resolution_product_category_tier_1,ticket.resolution_product_category_tier_2,ticket.resolution_product_category_tier_3,
         Count(ticket.sid) AS Total_log,ROUND( AVG((ticket_sla.diff_time_final / 60)),1 ) as Average_Resolution_time,
          SUM(CASE WHEN ((ticket_sla.diff_time_final / 60) >= 0) AND ((ticket_sla.diff_time_final / 60) < 1) THEN 1 ELSE 0 END) as Hour_0_1,
          SUM(CASE WHEN ((ticket_sla.diff_time_final / 60) >= 1) AND ((ticket_sla.diff_time_final / 60) < 2) THEN 1 ELSE 0 END) as Hour_1_2,
          SUM(CASE WHEN ((ticket_sla.diff_time_final / 60) >= 2) AND ((ticket_sla.diff_time_final / 60) < 4) THEN 1 ELSE 0 END) as Hour_2_4,
          SUM(CASE WHEN ((ticket_sla.diff_time_final / 60) >= 4) AND ((ticket_sla.diff_time_final / 60) < 6) THEN 1 ELSE 0 END) as Hour_4_6,
          SUM(CASE WHEN ((ticket_sla.diff_time_final / 60) >= 6) AND ((ticket_sla.diff_time_final / 60) < 8) THEN 1 ELSE 0 END) as Hour_6_8,
          SUM(CASE WHEN ((ticket_sla.diff_time_final / 60) > 8) THEN 1 ELSE 0 END) as Hour_over_8
          FROM ticket INNER JOIN ticket_sla ON ticket_sla.ticket_sid = ticket.sid WHERE
          ticket.resolution_product_category_tier_1 <> '' AND ticket.create_datetime BETWEEN :startdatetime AND :enddatetime
           AND ticket.`status` = '5' AND ticket_sla.case_status_sid = '5'
            GROUP BY ticket.resolution_product_category_tier_1,ticket.resolution_product_category_tier_2,ticket.resolution_product_category_tier_3";
         $q = $this->db->prepare($sql);
         $q->execute(array(
           ':startdatetime'=>$start,
            ':enddatetime'=>$end
          ));
          $r = $q->fetchAll();
          return $r;
  }

  public function getTimeReport($start,$end,$status=Null){
    if ($status != '' || $status != NULL) {
        $sql = "SELECT * FROM vw_report_time_ticket_sla WHERE status = :status AND create_datetime BETWEEN :startdatetime AND :enddatetime";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':startdatetime'=>$start,
           ':enddatetime'=>$end,
           ':status'=>$status
         ));
         $r = $q->fetchAll();
         return $r;
    }else{
      $sql = "SELECT * FROM vw_report_time_ticket_sla WHERE create_datetime BETWEEN :startdatetime AND :enddatetime";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':startdatetime'=>$start,
         ':enddatetime'=>$end
       ));
       $r = $q->fetchAll();
       return $r;
    }

 }

  public function getCaseData($start,$end,$status=NULL){
      if ($status != '' || $status != NULL) {
        $sql = "SELECT * FROM vw_search_ticket WHERE create_datetime BETWEEN :startdatetime AND :enddatetime AND status = :status";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':startdatetime'=>$start,
           ':enddatetime'=>$end,
           ':status'=>$status
         ));
         $r = $q->fetchAll();
         if (count($r > 0)) {
           return $r;
         }else {
           return 'No data Found';
         }
      }else{
        $sql = "SELECT * FROM vw_search_ticket WHERE create_datetime BETWEEN :startdatetime AND :enddatetime";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':startdatetime'=>$start,
           ':enddatetime'=>$end
         ));
         $r = $q->fetchAll();
         return $r;
      }
 }

 public function getSumticketPerDaySplitHour($datesearch) {
          $sql = "SELECT CURDATE() AS DATE,
         SUM(CASE WHEN (hour(create_datetime) = 6 AND severity = 'Severity-1') then 1 else 0 end) as serv1_6,
         SUM(CASE WHEN (hour(create_datetime) = 7 AND severity = 'Severity-1') then 1 else 0 end) as serv1_7,
         SUM(CASE WHEN (hour(create_datetime) = 8 AND severity = 'Severity-1') then 1 else 0 end) as serv1_8,
         SUM(CASE WHEN (hour(create_datetime) = 9 AND severity = 'Severity-1') then 1 else 0 end) as serv1_9,
         SUM(CASE WHEN (hour(create_datetime) = 10 AND severity = 'Severity-1') then 1 else 0 end) as serv1_10,
         SUM(CASE WHEN (hour(create_datetime) = 11 AND severity = 'Severity-1') then 1 else 0 end) as serv1_11,
         SUM(CASE WHEN (hour(create_datetime) = 12 AND severity = 'Severity-1') then 1 else 0 end) as serv1_12,
         SUM(CASE WHEN (hour(create_datetime) = 13 AND severity = 'Severity-1') then 1 else 0 end) as serv1_13,
         SUM(CASE WHEN (hour(create_datetime) = 14 AND severity = 'Severity-1') then 1 else 0 end) as serv1_14,
         SUM(CASE WHEN (hour(create_datetime) = 15 AND severity = 'Severity-1') then 1 else 0 end) as serv1_15,
         SUM(CASE WHEN (hour(create_datetime) = 16 AND severity = 'Severity-1') then 1 else 0 end) as serv1_16,
         SUM(CASE WHEN (hour(create_datetime) = 17 AND severity = 'Severity-1') then 1 else 0 end) as serv1_17,
         SUM(CASE WHEN (hour(create_datetime) = 18 AND severity = 'Severity-1') then 1 else 0 end) as serv1_18,
         SUM(CASE WHEN (hour(create_datetime) = 19 AND severity = 'Severity-1') then 1 else 0 end) as serv1_19,
         SUM(CASE WHEN (hour(create_datetime) = 20 AND severity = 'Severity-1') then 1 else 0 end) as serv1_20,
         SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-1') then 1 else 0 end) as serv1_21,
         SUM(CASE WHEN (hour(create_datetime) = 22 AND severity = 'Severity-1') then 1 else 0 end) as serv1_22,
         SUM(CASE WHEN (hour(create_datetime) = 23 AND severity = 'Severity-1') then 1 else 0 end) as serv1_23,
         SUM(CASE WHEN (hour(create_datetime) = 6 AND severity = 'Severity-2') then 1 else 0 end) as serv2_6,
         SUM(CASE WHEN (hour(create_datetime) = 7 AND severity = 'Severity-2') then 1 else 0 end) as serv2_7,
         SUM(CASE WHEN (hour(create_datetime) = 8 AND severity = 'Severity-2') then 1 else 0 end) as serv2_8,
         SUM(CASE WHEN (hour(create_datetime) = 9 AND severity = 'Severity-2') then 1 else 0 end) as serv2_9,
         SUM(CASE WHEN (hour(create_datetime) = 10 AND severity = 'Severity-2') then 1 else 0 end) as serv2_10,
         SUM(CASE WHEN (hour(create_datetime) = 11 AND severity = 'Severity-2') then 1 else 0 end) as serv2_11,
         SUM(CASE WHEN (hour(create_datetime) = 12 AND severity = 'Severity-2') then 1 else 0 end) as serv2_12,
         SUM(CASE WHEN (hour(create_datetime) = 13 AND severity = 'Severity-2') then 1 else 0 end) as serv2_13,
         SUM(CASE WHEN (hour(create_datetime) = 14 AND severity = 'Severity-2') then 1 else 0 end) as serv2_14,
         SUM(CASE WHEN (hour(create_datetime) = 15 AND severity = 'Severity-2') then 1 else 0 end) as serv2_15,
         SUM(CASE WHEN (hour(create_datetime) = 16 AND severity = 'Severity-2') then 1 else 0 end) as serv2_16,
         SUM(CASE WHEN (hour(create_datetime) = 17 AND severity = 'Severity-2') then 1 else 0 end) as serv2_17,
         SUM(CASE WHEN (hour(create_datetime) = 18 AND severity = 'Severity-2') then 1 else 0 end) as serv2_18,
         SUM(CASE WHEN (hour(create_datetime) = 19 AND severity = 'Severity-2') then 1 else 0 end) as serv2_19,
         SUM(CASE WHEN (hour(create_datetime) = 20 AND severity = 'Severity-2') then 1 else 0 end) as serv2_20,
         SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-2') then 1 else 0 end) as serv2_21,
         SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-2') then 1 else 0 end) as serv2_22,
         SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-2') then 1 else 0 end) as serv2_23,
         SUM(CASE WHEN (hour(create_datetime) = 6 AND severity = 'Severity-3') then 1 else 0 end) as serv3_6,
         SUM(CASE WHEN (hour(create_datetime) = 7 AND severity = 'Severity-3') then 1 else 0 end) as serv3_7,
         SUM(CASE WHEN (hour(create_datetime) = 8 AND severity = 'Severity-3') then 1 else 0 end) as serv3_8,
         SUM(CASE WHEN (hour(create_datetime) = 9 AND severity = 'Severity-3') then 1 else 0 end) as serv3_9,
         SUM(CASE WHEN (hour(create_datetime) = 10 AND severity = 'Severity-3') then 1 else 0 end) as serv3_10,
         SUM(CASE WHEN (hour(create_datetime) = 11 AND severity = 'Severity-3') then 1 else 0 end) as serv3_11,
         SUM(CASE WHEN (hour(create_datetime) = 12 AND severity = 'Severity-3') then 1 else 0 end) as serv3_12,
         SUM(CASE WHEN (hour(create_datetime) = 13 AND severity = 'Severity-3') then 1 else 0 end) as serv3_13,
         SUM(CASE WHEN (hour(create_datetime) = 14 AND severity = 'Severity-3') then 1 else 0 end) as serv3_14,
         SUM(CASE WHEN (hour(create_datetime) = 15 AND severity = 'Severity-3') then 1 else 0 end) as serv3_15,
         SUM(CASE WHEN (hour(create_datetime) = 16 AND severity = 'Severity-3') then 1 else 0 end) as serv3_16,
         SUM(CASE WHEN (hour(create_datetime) = 17 AND severity = 'Severity-3') then 1 else 0 end) as serv3_17,
         SUM(CASE WHEN (hour(create_datetime) = 18 AND severity = 'Severity-3') then 1 else 0 end) as serv3_18,
         SUM(CASE WHEN (hour(create_datetime) = 19 AND severity = 'Severity-3') then 1 else 0 end) as serv3_19,
         SUM(CASE WHEN (hour(create_datetime) = 20 AND severity = 'Severity-3') then 1 else 0 end) as serv3_20,
         SUM(CASE WHEN (hour(create_datetime) = 21 AND severity = 'Severity-3') then 1 else 0 end) as serv3_21,
         SUM(CASE WHEN (hour(create_datetime) = 22 AND severity = 'Severity-3') then 1 else 0 end) as serv3_22,
         SUM(CASE WHEN (hour(create_datetime) = 23 AND severity = 'Severity-3') then 1 else 0 end) as serv3_23,
         SUM(CASE WHEN (hour(create_datetime) = 6  ) then 1 else 0 end) as sum_tk6,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 7 ) then 1 else 0 end) as sum_tk7,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 8 ) then 1 else 0 end) as sum_tk8,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 9 ) then 1 else 0 end) as sum_tk9,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 10 ) then 1 else 0 end) as sum_tk10,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 11 ) then 1 else 0 end) as sum_tk11,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 12 ) then 1 else 0 end) as sum_tk12,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 13 ) then 1 else 0 end) as sum_tk13,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 14 ) then 1 else 0 end) as sum_tk14,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 15 ) then 1 else 0 end) as sum_tk15,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 16 ) then 1 else 0 end) as sum_tk16,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 17 ) then 1 else 0 end) as sum_tk17,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 18 ) then 1 else 0 end) as sum_tk18,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 19 ) then 1 else 0 end) as sum_tk19,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 20 ) then 1 else 0 end) as sum_tk20,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 21 ) then 1 else 0 end) as sum_tk21,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 22 ) then 1 else 0 end) as sum_tk22,
         SUM(CASE WHEN (hour(create_datetime) BETWEEN 6 AND 23 ) then 1 else 0 end) as sum_tk23
         FROM ticket	WHERE date(create_datetime) = :datesearch
         GROUP BY cast(create_datetime as date)";
          // = CURDATE()
   $q = $this->db->prepare($sql);
   $q->execute(array(
     ':datesearch'=>$datesearch
    ));
   return $q->fetchAll();
 }

 public function getSumTeamTicket(){
   $sql = "SELECT team,
   SUM(CASE WHEN (`status` = 1 and `owner` <> 'Service Center') then 1 else 0 end) as Assigned,
   SUM(CASE WHEN (`status` = 2 ) then 1 else 0 end) as In_progress,
   SUM(CASE WHEN (`status` = 4 and `owner` <> 'nanthawat.s.m+sos.vspace.01@gmail.com') then 1 else 0 end) as Pending
   FROM ticket WHERE refer_remedy_hd >= 'INC000001029301' AND team <> 'Incident Coordinator' GROUP BY team";
   $q = $this->db->prepare($sql);
   $q->execute();
   return $q->fetchAll();
 }
}
 ?>
