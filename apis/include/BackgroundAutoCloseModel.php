<?php
require_once __dir__."/../../apis/core/constant.php";
/**
 *
 */
class BackgroundAutoCloseModel
{
  private $db;
	private $m;


  function __construct() {
        require_once __dir__.'/db_connect.php';
        // opening db connection
        $this->m = new DbConnect();
        $this->db = $this->m->connect();
 	}

  function listResolveAfter15days()
  {
    $sql = "SELECT sid FROM ticket WHERE `status` = 5 AND CURDATE() > DATE_ADD(update_datetime,INTERVAL 15 day) ORDER BY update_datetime ASC LIMIT 0,20";
    $q = $this->db->prepare($sql);
    $q->execute();
    return $r = $q->fetchAll();
  }
  function updateCloseTicket($ticket_sid)
  {
    $sql = "UPDATE ticket SET status = 6, update_datetime = NOW(),   updated_by = 'vspace_system' WHERE sid = :sid ";
    $q = $this->db->prepare($sql);
    $q->execute(array(':sid'=>$ticket_sid));
  }
}

?>
