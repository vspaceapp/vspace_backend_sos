<?php
class UserModel{

	private $db;

    private $email;
    private $tasks_sid;

    private $fcm_token;
    private $name;
    private $password;
    private $mobile;
    private $otp;
    private $user_general_sid;
    private $switchSendMailOtp = false;
    private $objCons;
    private $rootPathSignature = "/uploads/";
    private $urlPathSignature = "./uploads/";

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function selectUser(){
        $sql = "SELECT * FROM user WHERE email = 'detomas_25@hotmail.com' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r;
    }
    private function getContentFileTmp($tmpName){
        // $fp = fopen($tmpName, 'r');
        // $content = fread($fp, filesize($tmpName));
        $content = addslashes($tmpName);
        // fclose($fp);
        return $content;
    }
    private function convertToBase64($tmp_name){
        $type = pathinfo($tmp_name, PATHINFO_EXTENSION);
        $file_get_contents = file_get_contents($tmp_name);
        return 'data:image/' . $type . ';base64,' . base64_encode($file_get_contents);
    }

    public function updateProfile($file, $email, $token, $data, $file_picture_profile){
        $newFileName = '';
        if (isset($file['size']) && $file["size"] < 10000000) {
            $filename = $file["name"];
            $newArrayFileName = explode(".", $filename);
            $date = new DateTime(date('Y-m-d'));
            $date->setTime(date('H'), date('i'), date('s'));

                // $pt_type = isset($_POST['pt_type'])?$_POST['pt_type']:"";
                // $cd_id = isset($_POST['cd_id'])?$_POST['cd_id']:"";
            // $data['company_logo_blob'] = $this->getContentFileTmp($file["tmp_name"]);
            // $type = pathinfo($file["tmp_name"], PATHINFO_EXTENSION);
            // $file_get_contents = file_get_contents($file["tmp_name"]);
            // $data['base64'] = $this->convertToBase64($file['tmp_name']);

            // $this->updateDataUser_company_logo_blob($data);

            $newFileName = $date->format('Ymd') ."_". $date->format('His') . "_company_logo_". $token . "." . $newArrayFileName[count($newArrayFileName) - 1];
            if (move_uploaded_file($file["tmp_name"], "../../../uploads/" . $newFileName)) {
                    // $insertResult = $this->insertToDb($newFileName, $pt_type, $_COOKIE['us_id_update'], $cd_id);

                    //Resize dimention W 500
                    // $obj_pc->resizePicture($newFileName,500);

                    //compress quality = 40
                    // $obj_pc->compressPicture($newFileName,40);

                $data['company_logo'] = $this->urlPathSignature.$newFileName;
            }
        }

        if (isset($file_picture_profile['size']) && $file_picture_profile["size"] < 10000000) {
            $filename = $file_picture_profile["name"];
            $newArrayFileName = explode(".", $filename);
            $date = new DateTime(date('Y-m-d'));
            $date->setTime(date('H'), date('i'), date('s'));

            // $data['picture_profile_blob'] = $this->getContentFileTmp($file_picture_profile["tmp_name"]);
            // $data['picture_profile_base64'] = $this->convertToBase64($file_picture_profile['tmp_name']);
            // $this->updateDataUser_picture_profile_blob($data);

            $newFileName = $date->format('Ymd') ."_". $date->format('His') . "_file_picture_profile_". $token . "." . $newArrayFileName[count($newArrayFileName) - 1];
            if (move_uploaded_file($file_picture_profile["tmp_name"], "../../../uploads/" . $newFileName)) {

                $data['picture_profile'] = $this->urlPathSignature.$newFileName;
            }
        }
        // sleep(2);

        $this->updateDataUserManual($data);
    }


    // public function updateDataUser_picture_profile_blob($data){
    //     $sql = "UPDATE user SET
    //     picture_profile_base64 = :picture_profile_base64
    //     WHERE email = :email ";
    //     $q = $this->db->prepare($sql);
    //     $q->execute(array(
    //         ':picture_profile_base64'=>$data['picture_profile_base64'],
    //         ':email'=>$data['email']
    //         )
    //     );
    // }

    // public function updateDataUser_company_logo_blob($data){
    //     $sql = "UPDATE user SET
    //     company_logo_base64 = :company_logo_base64
    //     WHERE email = :email ";
    //     $q = $this->db->prepare($sql);
    //     $q->execute(array(
    //         ':company_logo_base64'=>$data['base64'],
    //         ':email'=>$data['email'],
    //         )
    //     );
    // }

    public function updateDataUserManual($data){
        $sql = "UPDATE user SET
        picture_profile = :picture_profile,
        compename = :compename,comptname = :comptname,
        company_logo = :company_logo,
        name = :name,
        mobile = :mobile
        WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':picture_profile'=>$data['picture_profile'],
            ':compename'=>$data['company'],
            ':comptname'=>$data['company'],
            ':mobile'=>$data['mobile'],
            ':email'=>$data['email'],
            ':name'=>$data['name'],
            ':company_logo'=>$data['company_logo']
            )
        );
    }

    public function listUser(){
        $sql = "SELECT U.name, U.email, U.email_2, E.englastname, CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) profile_picture,
        E.compename, E.comptname, E.thainame, U.picture_profile my_pic, U.company_logo
        FROM user U LEFT JOIN employee E ON U.email = E.emailaddr OR U.email_2 = E.emailaddr
        WHERE (U.company_logo = '' OR formal_name = '') ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    public function updateDataUser($data){
        if($data['my_pic']!=''){
            $sql = "UPDATE user SET
            compename = :compename, comptname = :comptname, initial = :initial, company_logo = :company_logo,
            formal_name = :formal_name
            WHERE email = :email OR email_2 = :email ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':compename'=>$data['compename'],
                ':comptname'=>$data['comptname'],
                ':initial'=>strtoupper(substr($data['name'],0,1)).strtoupper(substr($data['englastname'], 0,1)),
                ':email'=>$data['email'],
                ':company_logo'=>isset($data['company_logo'])?$data['company_logo']:'',
                ':formal_name'=>$data['thainame']
                )
            );
        }else{
            $sql = "UPDATE user SET picture_profile = :picture_profile,
            compename = :compename, comptname = :comptname, initial = :initial, company_logo = :company_logo,
            formal_name = :formal_name
            WHERE email = :email ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':picture_profile'=>$data['profile_picture'],
                ':compename'=>$data['compename'],
                ':comptname'=>$data['comptname'],
                ':initial'=>strtoupper(substr($data['name'],0,1)).strtoupper(substr($data['englastname'], 0,1)),
                ':email'=>$data['email'],
                ':company_logo'=>isset($data['company_logo'])?$data['company_logo']:'',
                ':formal_name'=>$data['thainame']
                )
            );
        }
        $rootUrlUploads = "./uploads/";
        if($data['company_logo']==''){
            $emailArray = explode("@",$data['email']);
            $company_logo = "";
            if($emailArray[1]=="firstlogic.co.th"){
                $company_logo = $rootUrlUploads."service_report_logo.gif";
            }else if($emailArray[1]=="g-able.com"){
                $company_logo = $rootUrlUploads."g-able.jpg";
            }else if($emailArray[1]=="cdg.co.th" || $emailArray[1]=="cdg.com"){
                $company_logo = $rootUrlUploads."CDG.png";
            }
            $sql = "UPDATE user SET company_logo = :company_logo
                    WHERE email = :email ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':company_logo'=>$company_logo,':email'=>$data['email']));
        }
    }

    public function saveSignature($email, $base64, $token){
        try{
            $sql = "SELECT DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') thistime ";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetch();



            $rootPath = "../../../";
            $filteredData = substr($base64, strpos($base64, ",") + 1);
            $unencodedData = base64_decode($filteredData);
            $file =  $r['thistime'].'_'.$token. '.png';
            file_put_contents($rootPath."uploads/". $file, $unencodedData);

            $this->updateSignature($email, $file);
            // sleep(2);

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function updateSignature($email, $file){
          $sql = "UPDATE user SET path_signature = :file WHERE email = :email ";
          $q = $this->db->prepare($sql);
          $q->execute(array(':file'=>$file,':email'=>$email));

          return true;
    }
    public function updateUser($email, $data){
        $sql = "UPDATE user_role SET role_sid = :role_sid, updated_datetime = NOW(), updated_by = :updated_by WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':role_sid'=>trim($data['role_sid']),':updated_by'=>$email,':email'=>$data['email']));

        $sql = "UPDATE user SET role_sid = :role_sid ";
        if(isset($data['email_2']) && $data['email_2']!=""){
            $sql .= ", email_2 = '".trim($data['email_2'])."' ";
        }
        $sql .= " WHERE email = :email";
        $q = $this->db->prepare($sql);
        return $q->execute(array(':role_sid'=>trim($data['role_sid']),':email'=>$data['email']));
    }

    public function users($email, $sort="", $sort_type="", $search=""){
        $this->email = $email;

        $sql = "SELECT UR.email, R.name role_name, R.description role_description, UR.role_sid role_sid, UL.update_datetime,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',maxportraitfile) pic_user,E.thainame, E.mobile, E.comptname
        FROM user UR
        LEFT JOIN role R ON UR.role_sid = R.sid
        LEFT JOIN user_location UL ON UL.email = UR.email
        LEFT JOIN employee E ON E.emailaddr = UR.email
        WHERE 1 AND (UR.active = '1' OR UR.active IS NULL) ";
        if($search!=""){
            $sql .= " AND (UR.email LIKE '%".$search."%' OR R.name LIKE '%".$search."%' OR UL.update_datetime LIKE '%".$search."%' OR E.thainame LIKE '%".$search."%') ";
        }
        if($sort!=""){
            $sql .= " ORDER BY ".$sort. " ".$sort_type;
        }
        // echo $sql;

        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function getLastLocationEmployee($employeeEmail){
        $sql = "SELECT * FROM user_location WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$employeeEmail));
        $r = $q->fetch();
        return $r;
    }

    public function listUserCanAddTask($email,$search="",$notEmail = array()){
        $sql = "SELECT UR.role_sid,R.can_assign FROM user_role UR LEFT JOIN role R ON UR.role_sid = R.sid WHERE UR.email = :email AND UR.data_status > 0";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        $role_sid = $r['role_sid'];
        $can_assign = $r['can_assign'];
        if($can_assign==""){
            $can_assign = $role_sid;
        }
        $sql = "SELECT E.*,UR.email email,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_employee,R.role_name_view
        FROM user_role UR LEFT JOIN employee E ON UR.email = E.emailaddr AND E.emailaddr <> ''
        LEFT JOIN role R ON R.sid = UR.role_sid
        WHERE (UR.email = :email OR UR.role_sid IN (".$can_assign.")) AND UR.data_status > 0 ";
        if($search!=""){
            $sql .= " AND (UR.email LIKE '%".$search."%' OR E.thainame LIKE '%".$search."%' ) ";
        }

        if(count($notEmail)>0){
            foreach ($notEmail as $key => $value) {
                $sql .= " AND UR.email <> '".$value."' ";
            }
        }

        $sql .= " ORDER BY R.sid DESC,E.thainame GROUP BY E.emailaddr ";
        // echo $sql;
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetchAll();
        return $r;
    }

    public function updateRegisterFCM($fcm_token, $email){
        $this->setFcmToken($fcm_token);
        $this->setEmail($email);

        $this->updateRegisterFCMDatabase();
    }

    public function getInformationUserViaToken($data = array()) {
        $user_sid = $this->getUserSidVidToken($data);
        return $user_sid;
    }

    public function getRemedyCategoryByType($categoryType = ""){
        $sql = "SELECT * FROM remedy_category WHERE category_type = :category_type" ;
        $q = $this->db->prepare($sql);
        $q->execute(array(":category_type"=>$categoryType));
        $r = $q->fetchAll();
        return $r;
    }

    public function updateRegisterOnlyReact($email, $registrationId){
        $sql = "UPDATE user SET gcm_registration_id = :registrationId, updated_datetime = NOW() WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':registrationId'=>$registrationId,
            ':email'=>$email
            ));
        $sql = "UPDATE user_location SET registration_id = :registration_id, updated_registration_id = NOW() WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':registration_id'=>$registrationId,
            ':email'=>$email
        ));
    }

    public function updateRegisterReact($email, $token, $code_version, $device_platform, $device_version, $registrationId){
        $sql = "UPDATE user SET gcm_registration_id = :registrationId, device_platform = :device_platform, device_version = :device_version, updated_datetime = NOW() WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':registrationId'=>$registrationId,
            ':device_platform'=>$device_platform,
            ':device_version'=>$device_version,
            ':email'=>$email
            ));
        $sql = "UPDATE user_location SET registration_id = :registration_id, updated_registration_id = NOW(), device_platform = :device_platform, code_version = :code_version WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':registration_id'=>$registrationId,
            ':device_platform'=>$device_platform,
            ':code_version'=>$code_version,
            ':email'=>$email
            ));
    }

    public function getInformationUser($data = array()) {
        try{
            $sid = (isset($data['sid'])) ? $data['sid'] : "";
            $sql = "SELECT user.role_sid role_sid,user.mobile, user.compename company, user.company_logo,
                    CONCAT('".$this->urlPathSignature."',user.path_signature) path_signature, user.path_signature path_signature_original,
                    user.sid, user.sid user_sid, user.name,user.lastname,E.thainame,DEP.sid department_sid,DEP.name department_name,
                    user.email,role.permission_view_report, role.permission_view_dashboard, permission_view_map, role.case_team,E.empno,E.maxportraitfile,role.name role_name,role.create_project_c_and_g,role.project_create_able,role.case_create_able,
                    user.picture_profile pic_full, user.initial, role.add_oracle_part aop, role.add_serial, role.add_inventory, role.sos_status, role.sos_ci_information "
                    . "FROM user "
                    . "LEFT JOIN user_role UR ON user.email = UR.email "
                    . "LEFT JOIN role ON UR.role_sid = role.sid "
                    . "LEFT JOIN employee E ON user.email = E.emailaddr "
                    . "LEFT JOIN department DEP ON role.department_sid = DEP.sid "
                    . "WHERE user.sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid' => $sid));
            $r = $q->fetch();
            $r['menus'] = $this->menusList($r['role_sid']);
            $r['menusV4'] = $this->menusListV4($r['role_sid']);

            $r['menusTab'] = $this->menusTab($r['role_sid']);
            $r['can_create_dummy_company'] = $this->canCreateDummyCompany($r['user_sid']);
            return $r;
        }catch(PDOException $e){
            echo $e;
            return array();
        }
    }

    private function menusTab($role_sid){
        try{
            $sql = "SELECT * FROM menus_tab WHERE role_sid = :role_sid ORDER BY sequence";
            $q = $this->db->prepare($sql);
            $q->execute(array(':role_sid'=>$role_sid));
            $r = $q->fetchAll();
            if(count($r)>0){
                return $r;
            }else{
                if($role_sid == "361"){// dev team
                    return array(
                        // array('name'=>'Project','param1'=>'0','iconWeb'=>''),
                        array('name'=>'Ticket','param1'=>'1','iconWeb'=>''),
                        array('name'=>'Service Report','param1'=>'2','iconWeb'=>''),
                        //array('name'=>'Inventory','param1'=>'4','iconWeb'=>'')
                    );
                }else{
                    return array(
                        // array('name'=>'Project','param1'=>'0','iconWeb'=>''),
                        array('name'=>'Ticket','param1'=>'1','iconWeb'=>''),
                        array('name'=>'Service Report','param1'=>'2','iconWeb'=>'')
                        //array('name'=>'Inventory','param1'=>'4','iconWeb'=>'')
                    );
                }
            }
        }catch(Exception $e){
            print_r($e);
        }
    }
    private function canCreateDummyCompany($userSid){
        $sql = "SELECT * FROM user_create_dummy_contract_company WHERE user_sid = :user_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':user_sid'=>$userSid));
        return $q->fetchAll();
    }

    private function menusListV4($role_sid){
        $sql = "SELECT M.link,M.name FROM menus_role MR INNER JOIN menus M ON M.sid = MR.menus_sid WHERE MR.role_sid = :role_sid ORDER BY M.sequence;";
        $q = $this->db->prepare($sql);
        $q->execute(array(':role_sid'=>$role_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function menusList($role_sid){
        $sql = "SELECT * FROM modules MODU WHERE 1 ORDER BY MODU.sequence ASC";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $sql = "SELECT M.* FROM menus_role MR
                    LEFT JOIN menus M ON MR.menus_sid = M.sid
                WHERE MR.role_sid = '".$role_sid."' AND M.modules_sid = '".$value['sid']."' ORDER BY M.sequence, M.name DESC ";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r[$key]['list'] = $q->fetchAll();
            // $r[$key]['sql'] = $sql;
        }
        return $r;
    }

    private function getUserSidVidToken($data = array()) {
        $token = (isset($data['token'])) ? $data['token'] : "";
        $email = (isset($data['email'])) ? $data['email'] : "";
        $sql = "SELECT * FROM token WHERE token = :token AND email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':token' => $token, ':email'=>$email));
        $r = $q->fetch();
        return $r['user_sid'];
    }

    private function updateRegisterFCMDatabase(){
        $sql = "UPDATE user SET gcm_registration_id = :fcm_token, updated_datetime = NOW() WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':fcm_token'=>$this->getFcmToken(),':email'=>$this->getEmail()
            ));
    }
    public function saveRemedyPassword($email, $remedy_user, $remedy_password){
        $sql = "UPDATE user SET remedy_user = :remedy_user, remedy_password = :remedy_password, remedy_updated_datetime = NOW() WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':remedy_user'=>$remedy_user,':remedy_password'=>$remedy_password,':email'=>$email));
    }
    public function getRemedyPassword($email){
        $sql = "SELECT * FROM user WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r;
    }
    public function getProfile($email){
        $sql = "SELECT E.*,CONCAT('".END_POINT_PATH_EMPLOYEE."',maxportraitfile) pic_employee,
        CONCAT('".$this->rootPathSignature."',U.path_signature) path_signature,
        U.path_signature path_signature_original, U.remedy_user, U.remedy_password
        FROM employee E
        LEFT JOIN user U ON E.emailaddr = U.email
        WHERE E.emailaddr = :emailaddr ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':emailaddr'=>$email));

        return $r = $q->fetch();
    }
    private function setFcmToken($fcm_token){
        $this->fcm_token = $fcm_token;
    }
    private function getFcmToken(){
        return $this->fcm_token;
    }

    private function setEmail($email){
        $this->email = $email;
    }
    private function getEmail(){
        return $this->email;
    }
    public function forgotPassword($data){
        $this->email = trim($data['email']);
        $userData;
        // if($this->objCons->isAccount($this->email)){
        $userData = $this->selectUserAccountByEmail();
        // }else{
        //     $userData = $this->selectUserGeneralByEmail();
        // }
        if(isset($userData['email'])){
            $this->executeOtp("registration");
            $objSendMail = new SendMail();
            $userData['otp'] = $this->otp;
            $objSendMail->sendToUserForgotPasswordWithOtp($userData);
        }
        return $userData;
    }

    public function confirmForgotPassword($data){
        $this->email = $data['email'];
        $this->password = md5($data['password']);
        $this->otp = $data['otp'];
        return $data = $this->selectOtpByEmail();
    }

    public function signUp($data){
        $this->email = $data['email'];
        $this->password = md5($data['password']);
        $this->name = $data['name'];
        $this->mobile = $data['mobile'];
        //check @ddress email

        //CASE EMAIL IS ACCOUNT
        if($this->objCons->isAccount($this->email) || 1){ // check  is email internal(g-able, firstlogic)
            $userAccount = $this->selectUserAccountByEmail();
            if(isset($userAccount['sid']) && $userAccount['sid']>0){ //check  email already exits?
                $this->user_sid = $userAccount['sid'];
                $this->switchSendMailOtp = false;
                if($userAccount['active']=="1"){ // already active mail
                    return "Mail Unavailable";
                }else if($userAccount['active']=="0"){
                    $this->switchSendMailOtp = true;
                    $this->updateToUser(); // update data to user table
                    $this->executeOtp("registration");
                }
            }else{
                $this->switchSendMailOtp = true;
                $this->insertToUser($this->getRoleAccount());
                $this->executeOtp("registration");
            }
            if($this->switchSendMailOtp){
                $objSendMail = new SendMail();
                $data['otp'] = $this->otp;
                $objSendMail->sendToUserRegisterWithOtp($data);
            }
            // return "Mail is account";
        }else{
        //CASE EMAIL IS GENERAL
            $userGeneral = $this->selectUserGeneralByEmail();
            if(isset($userGeneral['sid']) && $userGeneral['sid']>0){
                $this->user_general_sid = $userGeneral['sid'];
                $this->switchSendMailOtp = false;
                if($userGeneral['active']=="1"){
                    // CASE user active register repeat
                    return "Mail Unavailable";
                }else if($userGeneral['active']=="0"){
                    $this->switchSendMailOtp = true;
                    $this->updateToUserGeneral();
                    $this->executeOtp("registration");
                }
            }else{
                $this->switchSendMailOtp = true;
                $this->insertToUserGeneral();
                $this->executeOtp("registration");
            }
            if($this->switchSendMailOtp){
                $objSendMail = new SendMail();
                $data['otp'] = $this->otp;
                $objSendMail->sendToUserRegisterWithOtp($data);
            }
        }
    }


    private function getRoleAccount(){
        $sql = "SELECT * FROM user_role WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$this->email));
        $r = $q->fetch();
        if(isset($r['sid'])>0){
            return $r['role_sid'];
        }
        return "14";
    }
    private function insertToUser($role_sid="14"){
        $sql = "INSERT INTO user (name, role_sid, email , password ,mobile, updated_datetime) VALUES (:name, :role_sid, :email , :password ,:mobile, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':name'=>$this->name,':role_sid'=>$role_sid, ':email'=>$this->email,':password'=>$this->password, ':mobile'=>$this->mobile));
        // $this->user_general_sid = $this->db->lastInsertId();
    }
    public function comfirmOtp($data){
        $this->email = $data['email'];
        $this->password = md5($data['password']);
        $this->name = $data['name'];
        $this->mobile = $data['mobile'];
        $this->otp = $data['otp'];

        $resOtp = $this->selectOtpByEmail();
        // print_r($resOtp);
        if(isset($resOtp['sid']) && $resOtp['sid']>0){
            // $this->user_general_sid = $resOtp['user_general_sid'];
            $this->updateToUserGeneral(1); // not used ?
            $this->updateToUser(1);
            return true;
        }
        return false;
    }

    public function updatePassword($data){
        $this->email = $data['email'];
        $this->password = md5($data['password']);
        $this->updatePasswordUser(1);
        $this->updatePasswordUserGeneral(1);
    }

    private function updatePasswordUserGeneral($active = 0){
        $sql = "UPDATE user_general SET password = :password, updated_datetime = NOW(), active = :active WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':password'=>$this->password,':active'=>$active, ':email'=>$this->email));

    }

    private function updatePasswordUser($active = 0){
        $sql = "UPDATE user SET password = :password, updated_datetime = NOW(),  active = :active WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':password'=>$this->password,':active'=>$active, ':email'=>$this->email));
    }

    private function selectOtpByEmail($minute = 5){
        $sql = "SELECT OTP.*,TIMESTAMPDIFF(MINUTE,OTP.created_datetime, NOW()) minute
        FROM `user_otp` OTP
        WHERE 1 AND TIMESTAMPDIFF(MINUTE,OTP.created_datetime, NOW()) < :minute AND email = :email AND otp = :otp ORDER BY OTP.sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':minute'=>$minute,':email'=>$this->email,':otp'=>$this->otp));
        return $q->fetch();
    }

    private function selectUserGeneralByEmail(){
        $sql = "SELECT * FROM user_general WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$this->email));
        $r = $q->fetch();
        return $r;
    }
    private function selectUserAccountByEmail(){
        $sql = "SELECT * FROM user WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$this->email));
        $r = $q->fetch();
        return $r;
    }

    private function insertToUserGeneral($role_sid="14"){
        $sql = "INSERT INTO user_general (name, email , password ,mobile, updated_datetime, role_sid) VALUES (:name, :email , :password ,:mobile, NOW(), '14') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':name'=>$this->name,':email'=>$this->email,':password'=>$this->password, ':mobile'=>$this->mobile));
        $this->user_general_sid = $this->db->lastInsertId();
    }
    private function updateToUserGeneral($active = 0){
        $sql = "UPDATE user_general SET name = :name, password = :password, updated_datetime = NOW(), mobile = :mobile, active = :active WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':name'=>$this->name,':password'=>$this->password,':mobile'=>$this->mobile,':active'=>$active, ':email'=>$this->email));

    }

    private function updateToUser($active = 0){
        $sql = "UPDATE user SET name = :name, password = :password, updated_datetime = NOW(), mobile = :mobile, active = :active WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':name'=>$this->name,':password'=>$this->password,':mobile'=>$this->mobile,':active'=>$active, ':email'=>$this->email));
    }

    private function executeOtp($type){

        $this->otp = $this->genOtp();
        $this->type = $type;

        $this->insertUserOtp();
    }
    private function genOtp($lenght = 6 , $chars = '0123456789'){

        $chars_lenght = (strlen($chars)-1);
        // echo "chars_lenght ".$chars_lenght;

        $string = $chars{rand(0,$chars_lenght)};
        // echo "string ".$string;

        for($i = 1;$i<$lenght;$i = strlen($string))
        {
            $r = $chars{rand(0,$chars_lenght)};
            if ($r != $string{$i-1}){
                # code...
                $string .= $r;
            }
        }
        return $string;
    }

    private function insertUserOtp(){
        $sql = "INSERT INTO user_otp (email, otp, created_datetime, type) VALUES (:email, :otp, NOW(), :type) ";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(':email'=>$this->email,
                ':otp'=>$this->otp,
                ':type'=>$this->type
                )
            );
    }
}
