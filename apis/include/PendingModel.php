<?php
require_once __dir__."/WebServiceToRemedy/VSpaceToKBank.php";
require_once  __dir__."/CaseModel.php";
require_once __dir__."/../core/config.php";
class PendingModel{
	private $db;
	private $m;

	function __construct() {
        require_once 'db_connect.php';
        // opening db connection
        $this->m = new DbConnect();
        $this->db = $this->m->connect();
 	}

 	public function listStatusWaitKillPending(){
		$sql = "SELECT NOW() datenow ";
		$q = $this->db->prepare($sql);
		$q->execute();
		$r = $q->fetch();
		echo $r['datenow'];


 		$sql = "SELECT *,NOW() now FROM ticket_status WHERE wait_kill_pending = '1' AND expect_pending < NOW() ";
 		$q = $this->db->prepare($sql);
 		$q->execute();
 		return $r = $q->fetchAll();

 	}

 	public function killPending($data){

 		echo $currentStatus = $this->checkCurrentStatus($data);

 		if($currentStatus=="4"){
 			$vSpaceToKBank = new VSpaceToKBank();
 			$caseModel = new CaseModel();
 			$caseModel->ticket_sid = $data['ticket_sid'];
 			$ticketDetail = $caseModel->getTicketDetail(['case_type', 'sid']);
	 		$sql = "INSERT INTO ticket_status (ticket_sid, send_to_ccd, case_status_sid, create_datetime, create_by, data_from, data_status, update_datetime, worklog)
	 		VALUES (:ticket_sid, :send_to_ccd, :case_status_sid, NOW(), :create_by, '', '1', NOW(), :worklog) ";
	 		$q = $this->db->prepare($sql);
	 		$q->execute(array(
	 			':ticket_sid'=>$data["ticket_sid"],
	 			':send_to_ccd'=>0,
	 			':case_status_sid'=>2,
	 			':create_by'=>$data['create_by'],
	 			':worklog'=>'Case was automatically Work In Progress. From Expect Pending Finish '.$data['expect_pending']. ' (Done '.$data['now'].') '
	 			));
            $sql = "UPDATE ticket SET status = 2 WHERE sid = :sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=> $data['ticket_sid']));
            if($ticketDetail['case_type']=='Incident' && ENV == 'production'){
                $param = $vSpaceToKBank->prepareDataUpdateTicket(array(
                    'incident_number' => $caseModel->selectReferRemedyHdFromTicketSid($data['ticket_sid']),
                    'status' => $caseModel->selectStatusName(2),
                    'status_reason' => null,
                    'z1d_note' => null,
                    'z1d_action' => null,
                    'z1d_char01' => null,
                    'resolution' => null,
                    'resolution_category' => null,
                    'resolution_category_tier_2' => null,
                    'resolution_category_tier_3' => null
                ));
                $vSpaceToKBank->updateIncidentCase($param, "pending_system");
			}

	 	}

	 	$this->updateTicketStatusWaitKillPending($data);
		$this->calNewDueDate($data['ticket_sid']);

 	}


	public function calNewDueDate($ticket_sid){
		echo $caseCreated = $this->caseCreated($ticket_sid);
		$dataPending = $this->getTicketPending($ticket_sid);
		$listTicketSla = $this->listTicketSla($ticket_sid);

		// echo "<pre>";
		// print_r($dataPending);
		// echo "</pre>";
		$totalPendingMinute = $this->calTotalPendingMinute($dataPending);

		// echo "<pre>";
		// print_r($listTicketSla);
		// echo "</pre>";

		foreach ($listTicketSla as $key => $value) {
			$value['sla_value'];
			$value['sla_unit'];

			$new_due_date = $this->calDueDate($caseCreated, $value['sla_value'], $value['sla_unit'], $totalPendingMinute, $value);
			echo $new_due_date;
			echo "<pre>";
			print_r($value);
			echo "</pre>";

			$sql = "UPDATE ticket_sla SET due_datetime = :new_due_date, updated_datetime = NOW() WHERE sid = :sid ";
			$q = $this->db->prepare($sql);
			$q->execute(array(':new_due_date'=>$new_due_date,':sid'=>$value['sid']));
		}

	}

	private function calDueDate($caseCreated, $sla_value, $unit, $totalPendingMinute, $data){
		$unitC = "";
		if($unit=="Min"){
				$unitC = "minute";
		}
		if($unit=="Hour"){
				$unitC = "hour";
		}
		if($unit=="Day"){
				$unitC = "day";
		}
		if($unit=="NBD"){
				$unitC = "day";
		}

		$sql = "SELECT DATE_ADD('".$caseCreated."', INTERVAL ".$sla_value." ".$unitC.") new_due_date ";
		$q = $this->db->prepare($sql);
		$q->execute();
		$r = $q->fetch();
		$r['new_due_date'];

		$sql = "SELECT DATE_ADD('".$r['new_due_date']."', INTERVAL ".$totalPendingMinute." minute) new_due_date ";
		$q = $this->db->prepare($sql);
		$q->execute();
		$r = $q->fetch();
		return $r['new_due_date'];

	}
	public function listTicketSla($ticket_sid){
		$sql = "SELECT * FROM ticket_sla WHERE ticket_sid = :ticket_sid AND status = 'In Process'";
		$q = $this->db->prepare($sql);
		$q->execute(array(':ticket_sid'=>$ticket_sid));
		$r = $q->fetchAll();
		return $r;
	}

	private function getTicketPending($ticket_sid){
			$sql = "SELECT TS.*,
			(SELECT TSS.create_datetime next_status_create_datetime FROM ticket_status TSS
				WHERE TSS.ticket_sid = :ticket_sid AND TSS.case_status_sid <> '4' AND TSS.case_status_sid <> '8' AND TSS.sid > TS.sid
				ORDER BY TSS.sid ASC LIMIT 0,1) next_status_create_datetime
			FROM ticket_status TS
			WHERE ticket_sid = :ticket_sid AND TS.case_status_sid = 4 ORDER BY TS.sid ASC";
			$q = $this->db->prepare($sql);
			$q->execute(array(':ticket_sid'=>$ticket_sid));
			return $q->fetchAll();
	}

	private function caseCreated($ticket_sid){
		$sql = "SELECT create_datetime FROM ticket WHERE sid = :sid ";
		$q = $this->db->prepare($sql);
		$q->execute(array(':sid'=>$ticket_sid));
		$r = $q->fetch();
		if(isset($r['create_datetime'])){
			return $r['create_datetime'];
		} else {
			return "0000-00-00 00:00:00";
		}
	}

	private function calTotalPendingMinute($pendingData){
			$pendingTotalMinute = 0;
			foreach ($pendingData as $key => $value) {
					$pendingTotalMinute += intval($this->calDiffDatetime($value['create_datetime'], $value['next_status_create_datetime']));
			}
			return $pendingTotalMinute;
	}
	private function calDiffDatetime($first_datetime, $second_datetime){
			$sql = "SELECT TIMESTAMPDIFF(minute, :first_datetime, :second_datetime) datetime_diff ";
			$q = $this->db->prepare($sql);
			$q->execute(array(':first_datetime'=>$first_datetime,':second_datetime'=>$second_datetime));
			$r = $q->fetch();
			return $r['datetime_diff'];
	}

 	private function updateTicketStatusWaitKillPending($data){
 		$sql = "UPDATE ticket_status SET wait_kill_pending = 0, update_datetime = NOW()  WHERE sid = :sid ";
 		$q = $this->db->prepare($sql);
 		$q->execute(array(':sid'=>$data['sid']));
 	}

 	private function checkCurrentStatus($data){
 		$sql = "SELECT status FROM ticket WHERE sid = :sid ";
 		$q = $this->db->prepare($sql);
 		$q->execute(array(':sid'=>$data['ticket_sid']));
 		$r = $q->fetch();

 		if(isset($r['status']) && $r['status']!=""){
 			return $r['status'];
 		}else{
 			return "0";
 		}
 	}
}
?>
