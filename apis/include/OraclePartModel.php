<?php
class OraclePartModel{
  private $db;

  private $objCons;
  function __construct($email="") {
      require_once dirname(__FILE__) . '/db_connect.php';
      // opening db connection
      $this->objCons = new DbConnect();
      $this->db = $this->objCons->connect();
  }

  public function index($email){
      // $sql = "";

      // $q=$this->db->prepare($sql);
      // $q->execute(array(
      //   ':email'=>$email
      // ));
      // $r = $q->fetchAll();
      $r = array();
      return $r;
  }

  public function oraclePart($permission, $email, $data, $sort){
    
      // var arrayCriteria = [email,email,email];
      // $arrayCriteria = array(':email'=>$email);
      $arrayCriteria = array();

      $criteria = '';
      if(isset($data['ticket_sid']) && $data['ticket_sid']!=""){
        // arrayCriteria = [email,email,email, data.ticket_sid];
        // ':email'=>$email,
        $arrayCriteria = array(':ticket_sid'=>$data['ticket_sid']);
        $criteria = ' AND (TS.ticket_sid = :ticket_sid) ';

      }else if($data['start']!='Invalid date' && $data['start']!='' && $data['end']!='Invalid date' && $data['end'] !=''){
        $criteria = ' AND (TS.oracle_create_date >= "'.$data['start'].'" AND TS.oracle_create_date <= "'.$data['end'].'" )';
      }
      $permissionQuery = 'AND (T.owner = :email OR T.refer_remedy_report_by = :email OR T.create_by = :email) '; // ************************
      if($permission=="0" || $permission==0 || !$permission){

      }else{
        $permissionQuery = 'AND (U.role_sid IN ('.$permission.') OR T.owner = :email OR T.refer_remedy_report_by = :email 
        OR T.create_by = :email)';
      }

      $statusLabel =',CASE WHEN T.status = "5" THEN "Resolve" WHEN T.status = "4" THEN "Pending" ELSE "Process" END status_label ';
      $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';
      $sql = 'SELECT T.*,T.subject name, CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name ';
      $sql .= ',CASE WHEN U3.formal_name <> "" THEN U3.formal_name ELSE U3.name END report_by_name ';
      $sql .= $create_datetime;
      $sql .= ',T.end_user ';
      $sql .= ',CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END owner_thainame ';
      $sql .= $statusLabel;
      $sql .= ',U.picture_profile owner_picture_profile ';
      $sql .= ',"" expect_pending ';
      $sql .= ',TS.oracle_sr_type, TS.oracle_sr ';
      $sql .= ',CASE WHEN TS.oracle_create_date <> "0000-00-00 00:00:00" THEN DATE_FORMAT(TS.oracle_create_date,"%Y-%m-%d") ELSE null END oracle_create_date ';
      $sql .= ',TST.oracle_task,TSP.pn, TSP.part_description, TSP.spare_part_return_type ';
      $sql .= ',CASE WHEN request_spare_part_date <> "0000-00-00 00:00:00" THEN DATE_FORMAT(request_spare_part_date, "%Y-%m-%d") ELSE null END request_spare_part_date ';
      $sql .= ',CASE WHEN delivery_due_date <> "0000-00-00 00:00:00" THEN DATE_FORMAT(delivery_due_date, "%Y-%m-%d") ELSE null END delivery_due_date ';
      $sql .= ',CASE WHEN receive_spare_part_date <> "0000-00-00 00:00:00" THEN DATE_FORMAT(receive_spare_part_date, "%Y-%m-%d") ELSE null END receive_spare_part_date ';
      $sql .= ',CASE WHEN return_spare_part_date <> "0000-00-00 00:00:00" THEN DATE_FORMAT(return_spare_part_date, "%Y-%m-%d") ELSE null END return_spare_part_date ';
      $sql .= ',TSP.is_return, TS.sid ticket_spare_sid, TST.sid ticket_spare_task_sid, TSP.sid ticket_spare_part_sid ';
      $sql .= ',CASE WHEN request_spare_part_date <> "0000-00-00 00:00:00" AND delivery_due_date <> "0000-00-00 00:00:00" AND receive_spare_part_date <> "0000-00-00 00:00:00" AND return_spare_part_date <> "0000-00-00 00:00:00" AND spare_part_return_type <> "" AND is_return = 0 THEN 1 ELSE 0 END can_send_mail_return ';
      $sql .= 'FROM ticket_spare TS ';
      $sql .= 'LEFT JOIN ticket_spare_task TST ON TST.ticket_spare_sid = TS.sid ';
      $sql .= 'LEFT JOIN ticket_spare_part TSP ON TST.sid = TSP.ticket_spare_task_sid ';
      $sql .= 'LEFT JOIN ticket T ON TS.ticket_sid = T.sid ';
      $sql .= 'LEFT JOIN user U2 ON U2.email = T.create_by ';
      $sql .= 'LEFT JOIN user U ON U.email = T.owner ';
      $sql .= 'LEFT JOIN user U3 ON U3.email = T.refer_remedy_report_by ';
      $sql .= 'WHERE 1 ';
      // $sql .= $permissionQuery;
      $sql .= $criteria;
      $sql .= ' ORDER BY TS.sid '.$sort.' LIMIT 0,10000';

      $q = $this->db->prepare($sql);
      $q->execute($arrayCriteria);

      return $q->fetchAll();
  }
  public function addOracleBackline($data){
    if($data["oracle_sr"] == '' || $data['oracle_sr_type'] == ''){
			// ('addOracleBacklineQuery data empty');
			return array();
		}else{
      try{
        $sql = "INSERT INTO 
        ticket_spare 
        (ticket_sid, oracle_sr_type, oracle_sr, oracle_create_date, create_datetime, create_by) 
        VALUES 
        (:ticket_sid,:oracle_sr_type,:oracle_sr, :oracle_create_date,NOW(),:create_by) ";
        
        $q = $this->db->prepare($sql);

        $this->db->beginTransaction();
        $q->execute(array(
          ':ticket_sid'=>$data['ticket_sid'],
          ':oracle_sr_type'=>$data['oracle_sr_type'],
          ':oracle_sr'=>$data['oracle_sr'],
          ':oracle_create_date'=>$data['oracle_create_date'],
          ':create_by'=>$data['email']
        ));
        $data['ticket_spare_sid'] = $this->db->lastInsertId();
        $this->db->commit();
      
        $this->addOracleTaskQuery($data);

      }catch(PDOException $e){
        $this->db->rollback();
      }
		}
  }

  public function addOracleTaskQuery($data){
    
		if(!$data['ticket_spare_sid']){
			return array();
		}else{
			$sql = "INSERT INTO ticket_spare_task 
      (ticket_spare_sid, oracle_task, create_datetime, create_by) 
      VALUES 
      (:ticket_spare_sid,:oracle_task,NOW(),:create_by) ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':ticket_spare_sid'=>$data['ticket_spare_sid'],
        ':oracle_task'=>$data['task'],
        ':create_by'=>$data['email']
      ));
      $data['ticket_spare_task_sid'] = $this->db->lastInsertId();
      $this->addOraclePartQuery($data);
		}
  }
  public function addOraclePartQuery($data){
    
      if(!$data['ticket_spare_task_sid']){
          return array();
      }else{
        $sql = "INSERT INTO ticket_spare_part (
          ticket_spare_task_sid, pn, part_description, 
          create_datetime, create_by, request_spare_part_date,
          delivery_due_date, receive_spare_part_date, return_spare_part_date, 
          spare_part_return_type ) VALUES 
          (
            :ticket_spare_task_sid,
            :pn,
            :part_description,
            NOW(),
            :create_by,
            :request_spare_part_date,
            :delivery_due_date,
            :receive_spare_part_date,
            :return_spare_part_date,
            :spare_part_return_type
          ) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':ticket_spare_task_sid'=>$data['ticket_spare_task_sid'],
          ':pn'=>$data['pn'],
          ':part_description'=>$data['part_description'],
          ':create_by'=>$data['email'],
          ':request_spare_part_date'=>$data['request_spare_part_date'],
          ':delivery_due_date'=>$data['delivery_due_date'],
          ':receive_spare_part_date'=>$data['receive_spare_part_date'],
          ':return_spare_part_date'=>$data['return_spare_part_date'],
          ':spare_part_return_type'=>$data['spare_part_return_type']
        ));
      }
  }

  public function editOracleBacklineQuery($data){

      $sql = "UPDATE ticket_spare SET oracle_sr = :e1, 
      oracle_sr_type = :e2,
      oracle_create_date = :e3,
      update_datetime = NOW(), 
      update_by = :e4 WHERE sid = :ek ";

      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':e1'=>$data['oracle_sr'],
        ':e2'=>$data['oracle_sr_type'],
        ':e3'=>$data['oracle_create_date'],
        ':e4'=>$data['email'],
        ':ek'=>$data['ticket_spare_sid']
      ));
  }
  public function editOracleTaskQuery($data){

      $sql = "UPDATE ticket_spare_task SET 
      oracle_task = :oracle_task, 
      update_by = :update_by, 
      update_datetime = NOW() 
      WHERE sid = :ticket_spare_task_sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':oracle_task'=>$data['task'],
        ':update_by'=>$data['email'],
        ':ticket_spare_task_sid'=>$data['ticket_spare_task_sid']
      ));
  }
  public function editOraclePartQuery($data){
    
      $sql = "UPDATE ticket_spare_part SET 
      pn = :pn, 
      part_description = :part_description, 
      request_spare_part_date = :request_spare_part_date, 
      delivery_due_date = :delivery_due_date, 
      receive_spare_part_date = :receive_spare_part_date, 
      return_spare_part_date = :return_spare_part_date, 
      spare_part_return_type = :spare_part_return_type, 
      update_by = :update_by, 
      update_datetime = NOW() 
      WHERE sid = :sid ";

      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':pn'=>$data['pn'],
        ':part_description'=>$data['part_description'],
        ':request_spare_part_date'=>$data['request_spare_part_date'],
        ':delivery_due_date'=>$data['delivery_due_date'],
        ':receive_spare_part_date'=>$data['receive_spare_part_date'],
        ':return_spare_part_date'=>$data['return_spare_part_date'],
        ':spare_part_return_type'=>$data['spare_part_return_type'],
        ':update_by'=>$data['email'],
        ':sid'=>$data['ticket_spare_part_sid']
      ));
  }
  public function deleteOracleBacklineQuery($data){
 
      $sql = "DELETE FROM ticket_spare WHERE sid = :sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':sid'=>$data['ticket_spare_sid']
      ));
  }
  public function deleteOracleTaskQuery($data){
    
      $sql = "DELETE FROM ticket_spare_task WHERE sid = :sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':sid'=>$data['ticket_spare_task_sid']
      ));    
  }
  public function deleteOraclePartQuery($data){
    
      $sql = "DELETE FROM ticket_spare_part WHERE sid = :sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':sid'=>$data['ticket_spare_part_sid']
      ));    
  }
}
?>
