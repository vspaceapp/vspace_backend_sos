<?php
// include "/var/www/html/vlogic/public_html/apis/libs/simple_html_dom.php";
class CaseModifyModel{
    private $db;
    private $objCons;
    private $email;
    public $permission_role;
    function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->email = $email;
        if($this->email!=""){
            $this->permission_role = $this->objCons->permissionRoleByEmail($this->email); 
        }
    }

    public function modifySubject($email, $new_subject, $ticket_sid){
        $sql = "UPDATE ticket SET subject = :subject WHERE sid = :ticket_sid";   
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':subject'=> $new_subject,
            ':ticket_sid' => $ticket_sid ));
        // $r = $q->fetchAll();
         
    }

    public function modifyContactUser($email, $name, $mobile, $phone, $end_user_email, $company, $ticket_sid){

        $sql = "UPDATE ticket SET
                 end_user_contact_name = :name,
                 end_user_mobile = :mobile,
                 end_user_phone = :phone,
                 end_user_email = :end_user_email,
                 end_user_company_name = :company
                 WHERE sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':name' => $name,
            ':mobile' => $mobile,
            ':phone' => $phone,
            ':end_user_email' => $end_user_email,
            ':company' => $company,
            ':ticket_sid' => $ticket_sid
            ));
    }

    public function modifyRequester($email, $name, $mobile, $phone, $requester_email, $company, $ticket_sid){

        $sql = "UPDATE ticket SET
            requester_full_name = :name,
            requester_mobile = :mobile,
            requester_phone = :phone,
            requester_email = :requester_email,
            requester_company_name = :requester_company_name
            WHERE sid = :ticket_sid";

        $q = $this->db->prepare($sql);
        return $q->execute( array(
            ':name' => $name,
            ':mobile' => $mobile,
            ':phone' => $phone,
            ':requester_email' => $requester_email,
            ':requester_company_name' => $company,
            ':ticket_sid' => $ticket_sid
            ));
    }

    public function modifyDescription($email, $new_description, $ticket_sid){

        $sql = "UPDATE ticket SET description = :new_description WHERE sid = :ticket_sid";
         $q = $this->db->prepare($sql);
        return $q->execute( array(
            ':new_description' => $new_description,
            ':ticket_sid' => $ticket_sid
            ));
    }


    public function modifyOwner($email, $new_owner, $ticket_sid){

        $sql = "UPDATE ticket SET owner = :new_owner WHERE sid = :ticket_sid";
         $q = $this->db->prepare($sql);
         return $q->execute( array(
            ':new_owner' => $new_owner,
            ':ticket_sid' => $ticket_sid
            ));

    }



    public function modifyMandays($email, $man_days, $ticket_sid){

        $sql = "UPDATE ticket SET man_days = :man_days, updated_by = :email, update_datetime = NOW()  WHERE sid = :ticket_sid ";
         $q = $this->db->prepare($sql);
         return $q->execute( array(
            ':man_days' => $man_days,
            ':email' => $email,
            ':ticket_sid' => $ticket_sid
            ));

    }

    public function removeCase($email,$ticket_sid){
        if($this->checkTaskForCase($ticket_sid)>0){
            return "can not remove";
        }else{
            if($this->checkCaseCreateBy($ticket_sid, $email)>0){
                return "You can not remove this case";
            }else{
                $this->removeCaseAction($ticket_sid);
            }
        }
    }
    private function checkTaskForCase($ticket_sid){
        $sql = "SELECT * FROM tasks WHERE ticket_sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetchAll();
        return count($r);
    }
    private function removeCaseAction($ticket_sid){
        $sql = "DELETE FROM ticket WHERE sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
    }
    private function checkCaseCreateBy($ticket_sid, $email){
        $sql = "SELECT * FROM ticket WHERE sid = :sid AND create_by = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_sid,':email'=>$email));
        $r = $q->fetchAll();
    }

    // private function addAttendanceMeeting($email, $lastInsertId, $invite_meeting){
    //     // $data = array();
    //     foreach ($invite_meeting as $key => $value) {
    //         $sql = "INSERT INTO meeting_type_join( meeting_sid, email, status, join_time, remark, create_by, create_datetime, update_datetime) 
    //                 VALUES ( :meeting_sid,:email, '1', '','', :create_by , NOW(), '' )";
    //         $q = $this->db->prepare($sql);
    //         $q->execute(array(
    //                 ':meeting_sid' => $lastInsertId,
    //                 ':email' => $value,
    //                 ':create_by' => $email
    //             ));
    //     }

        
        
    // }


    public function getInfoEmployee($email){
        return $this->objCons->getInfoEmployee($email);
    }


}
?>