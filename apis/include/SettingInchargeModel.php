<?php
error_reporting(-1);
ini_set('display_errors', 'On');
class SettingInchargeModel{
	private $db;
  private $m;
  private $email, $token;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
  }
  public function selectData(){
    try{
        $sql = "SELECT * FROM gable_incharge_team ORDER BY sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return array('status'=>1, 'message'=>'Data Ok', 'data'=>$r);
    }catch(PDOException $e){
        return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function addData($data){
    try{
      $email_incharge = isset($data['email_incharge'])?$data['email_incharge']:'';
      $name = isset($data['name'])?$data['name']:'';
      $group_name = isset($data['group_name'])?$data['group_name']:'';
      $login_id = isset($data['login_id'])?$data['login_id']:'';
      $email = isset($data['email'])?$data['email']:'';
			$supervisor = isset($data['supervisor'])?$data['supervisor']:'';

      if($email_incharge && $name && $email && $group_name && $login_id){
        $sql = "INSERT INTO gable_incharge_team (email, name, group_name, login_id, created_by, created_datetime,supervisor)
        VALUES (:email_incharge, :name, :group_name, :login_id, :created_by, NOW(),:supervisor) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':email_incharge'=>$email_incharge,
          ':name'=>$name,
          ':group_name'=>$group_name,
          ':login_id'=>$login_id,
          ':created_by'=>$email,
					':supervisor'=>$supervisor
        ));
        return array('status'=>1, 'message'=>'Added', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'email_incharge, name, group_name, login_id, email ต้องไม่เป็นค่าว่าง', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function editData($data){
    try{
      if(isset($data['sid'])){
        $email_incharge = isset($data['email_incharge'])?$data['email_incharge']:'';
        $name = isset($data['name'])?$data['name']:'';
        $group_name = isset($data['group_name'])?$data['group_name']:'';
        $login_id = isset($data['login_id'])?$data['login_id']:'';
        $email = isset($data['email'])?$data['email']:'';
				$supervisor = isset($data['supervisor'])?$data['supervisor']:'';

        if($email_incharge && $name && $email && $group_name && $login_id){
          $sql = "UPDATE gable_incharge_team
          SET email = :email_incharge, name = :name,
          group_name = :group_name, updated_datetime = NOW(), updated_by = :email,
					login_id = :login_id, supervisor = :supervisor
					WHERE sid = :sid ";
          $q = $this->db->prepare($sql);
          $q->execute(
            array(
              ':email_incharge'=>$email_incharge,
              ':name'=>$name,
              ':group_name'=>$group_name,
              ':email'=>$email,
							':login_id'=>$login_id,
							':supervisor'=>$supervisor,
              ':sid'=>$data['sid']
            )
          );
          return array('status'=>1, 'message'=>'Updated', 'data'=>array());
        }else{
          return array('status'=>0, 'message'=>'ข้อมูล email_incharge, name, group_name, email, sid ต้องไม่เป็นค่าว่าง', 'data'=>array());
        }
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function deleteData($data){
    try{
      if(isset($data['sid'])){
        $email = isset($data['email'])?$data['email']:'';

        $sql = "UPDATE gable_incharge_team SET data_status = '-1', updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email,':sid'=>$data['sid']));
        return array('status'=>1, 'message'=>'Deleted', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
}
