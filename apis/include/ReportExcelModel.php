<?php
  /**
   *
   */
  class ReportExcelModel
  {
  public  function __construct(){
    require_once __dir__.'/db_connect.php';
    // opening db connection
    $this->m = new DbConnect();
    $this->db = $this->m->connect();
  }


   public function getRawDataTicketDaily($startdatetime,$enddatetime){
      $sql = "SELECT * from vw_report_ticket_daily WHERE create_datetime BETWEEN :startdatetime and :enddatetime";
      $q = $this->db->prepare($sql);
      $q->execute(array(':startdatetime'=>$startdatetime, ':enddatetime'=>$enddatetime));
      return $r = $q->fetchAll();
  }

  public function getRawDataServiceReportDaily($startdatetime,$enddatetime){
     $sql = "SELECT * from vw_report_service_report_daily WHERE create_datetime BETWEEN :startdatetime and :enddatetime";
     $q = $this->db->prepare($sql);
     $q->execute(array(':startdatetime'=>$startdatetime, ':enddatetime'=>$enddatetime));
     return $r = $q->fetchAll();
 }
}
?>
