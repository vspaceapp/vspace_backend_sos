<?php
require_once __dir__."/../core/constant.php";
require_once __dir__."/../core/config.php";
class MickModel {

    private $objCons;
    private $db, $email;

    function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->email = $email;
    }

    private function testConnect(){
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        $this->conn = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . "vspace_sos" . ';charset=' . DB_CHARSET, DB_USERNAME, DB_PASSWORD, $options);
        return $this->conn;
    }

    public function firstFunc($data){
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        try{
          if(isset($data['name']) && isset($data['lastname'])){
            $name = $data['name'];

            $sql = "INSERT INTO user (name, formal_name) VALUES (:name, :lastname) ";
            $q = $this->db->prepare($sql);
            return $q->execute(array(':name'=>$name,':lastname'=>$data['lastname']));
          }else{
            return "Error";
          }
        }catch(PDOException $e){
          echo $e->getMessage();
        }
        // return $data['name'];
    }

    public function testUpdateTicket($table_name,$params,$coor){
        $keys = array_keys($params);
        $sqlSet = "";
        foreach ($keys as $key){
            $sqlSet += $key+" = "+$params[$key];
        }
        $sql = "UPDATE :table_name SET ";
    }

    public function getUserByName($data){
      try{
        if(isset($data['name'])){
          $name = $data['name'];
          $sql = "SELECT * FROM user WHERE name = (:name)";
          $q = $this->db->prepare($sql);
          $q->execute(array(':name'=>$name));
          return $q->fetchAll();
        }else{
          return "Error";
        }

      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function getBlob(){
      try{

        $sql = "SELECT * FROM gable_worklog WHERE case_id = :case_id";
        $q = $this->testConnect()->prepare($sql);
        $q->execute(array(
            ':case_id'=>'INC000001016654'
        ));
        $r = $q->fetch();
        //echo $r['attachment_data'];
        //$r = base64_decode ( 'VGhpcyBpcyBhbiBlbmNvZGVkIHN0cmluZw==', true);
        //echo $r['attachment_data'];
        //header("Content-Type: image/png");
        //echo $r['attachment_data'];
        return $r;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    // test soap client update incident case kbank
    public function updateIncidentCase(
      $userName, $password, $Incident_Number, $Status, $Status_Reason, $z1D_Status_Reason,
      $Assigned_Support_Company, $Assigned_Support_Organization,$Assigned_Group,
      $Assignee,$Assigned_Group_ID,$Assignee_Login_ID,$z1D_Note,$Resolution_Category,
      $Resolution_Category_Tier_2,$Resolution_Category_Tier_3,$Resolution,$z1D_Action,$z1D_Char01
    ){
        $result = array();
        try{
            $params = array(
                'encoding' => 'UTF-8',
                'trace' => 1,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE
            );
            if(USE_LOCAL_XML){
                // use local xml file
                $dest = dirname(__FILE__) . DIRECTORY_SEPARATOR.REMEDY_FILENAME_XML_UPDATE_TICKET;
            }else{
                $dest = REMEDY_WEBSERVICE_URL_UPDATE_TICKET;
            }
            $client = new SoapClient($dest, $params);
            $dataHeader = array(
                "userName" => $userName,
                "password" => $password,
                "authentication"=>"?",
                "locale"=>"",
                "timeZone"=>"?"
            );
            $header = new SOAPHeader(REMEDY_NAMESPACE_UPDATE_TICKET, "AuthenticationInfo", $dataHeader,false);
            $client->__setSoapHeaders($header); //New_Set_Operation_UpdateIncident
        }catch(Exception $e){
            $result = array();
            $result['error message'] = $e->getMessage();
            if(USE_LOCAL_XML){
                $result['file_name'] = REMEDY_FILENAME_XML_UPDATE_TICKET;
            }else{
                $result['url'] = REMEDY_WEBSERVICE_URL_UPDATE_TICKET;
            }
            echo "Error(Create Soap Client) ".$e->getMessage()."</br>";
            exit();
        }
      $params = array(
                 "Incident_Number"=>$Incident_Number,
                 "Status"=>$Status,
                 "Status_Reason"=>$Status_Reason,
                 "z1D_Status_Reason"=>$z1D_Status_Reason,
                 "Assigned_Support_Company"=>$Assigned_Support_Company,
                 "Assigned_Support_Organization"=>$Assigned_Support_Organization,
                 "Assigned_Group"=>$Assigned_Group,
                 "Assignee"=>$Assignee,
                 "Assigned_Group_ID"=>$Assigned_Group_ID,
                "Assignee_Login_ID"=>$Assignee_Login_ID,
                "z1D_Note"=>$z1D_Note,
                "Resolution_Category"=>$Resolution_Category,
                "Resolution_Category_Tier_2"=>$Resolution_Category_Tier_2,
                "Resolution_Category_Tier_3"=>$Resolution_Category_Tier_3,
                "Resolution"=>$Resolution,
                "z1D_Action"=>$z1D_Action,
                "z1D_Char01"=>$z1D_Char01
        );
      try{
          $result['response'] = $client->New_Set_Operation_UpdateIncident($params);
          if(USE_LOCAL_XML){
              $result['file_name'] = REMEDY_FILENAME_XML_UPDATE_TICKET;
          }else{
              $result['url'] = REMEDY_WEBSERVICE_URL_UPDATE_TICKET;
          }
          $result['params'] = $params;
          return $result;
      }catch(Exception $e){
        $result['last_response'] = $client->__getLastResponse();
        $result['error_message'] = $e->getMessage();
        $result['params'] = $params;
        if(USE_LOCAL_XML){
            $result['file_name'] = REMEDY_FILENAME_XML_UPDATE_TICKET;
        }else{
            $result['url'] = REMEDY_WEBSERVICE_URL_UPDATE_TICKET;
        }
        return $result;
      }
    }


    public function testCallSoapSosWorklog(
        $ticket_id
        , $description
        , $attachment_name
        , $attachment_data
        , $attachment_size
        , $attachment_2
        , $attachment_3
        , $submitter
    ){

        try{
            $opts = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ));
            // SOAP 1.2 client
            $paramsClient = array(
                'encoding' => 'UTF-8',
                'trace' => 1,
                'exceptions' => 1,
                'stream_context' => stream_context_create($opts),
                'cache_wsdl' => WSDL_CACHE_NONE
            );
            $dest = SOAP_WS_URL_UPDATE_WORKLOG."?wsdl";
            $client = new SoapClient($dest, $paramsClient);
        }catch(Exception $e){
            return "Error while create soap client :".$e->getMessage();
        }
        try{
//            $paramsCall = array(
//                "ticket_id"=>$ticket_id
//                , "Detail_Description"=>$description
//                , "Attachment_Name"=>$attachment_name
//                , "Attachment_Data"=>$attachment_data
//                , "Attachment_OrigSize"=>$attachment_size
//                , "Attachment_2"=>$attachment_2
//                , "Attachment_3"=>$attachment_3
//                , "Submitter_Email"=>$submitter
//            );
            $res = $client->sos_worklog(
                  $ticket_id
                , $description
                , $attachment_name
                , $attachment_data
                , $attachment_size
                , $attachment_2
                , $attachment_3
                , $submitter
            );
            return $res;
        }catch (Exception $e){
            return "Error while call api :".$e->getMessage();
        }

    }

    // test soap client create/upadate worklog kbank
    public function createUpdateWorklog(
      $userName,
      $password,
      $Incident_Number,
      $Summary,
      $Note,
      $Work_Info_Type,
      $z2AF_Work_Log01_attachmentName,
      $z2AF_Work_Log01_attachmentData,
      $z2AF_Work_Log01_attachmentOrigSize,
      $z2AF_Work_Log02_attachmentName,
      $z2AF_Work_Log02_attachmentData,
      $z2AF_Work_Log02_attachmentOrigSize,
      $z2AF_Work_Log03_attachmentName,
      $z2AF_Work_Log03_attachmentData,
      $z2AF_Work_Log03_attachmentOrigSize,
      $Secure_Work_Log,
      $View_Access
    ){
        $result = array();
        try {

            $opts = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ));
            // SOAP 1.2 client
            $params = array(
                'encoding' => 'UTF-8',
                'trace' => 1,
                'exceptions' => 1,
                'stream_context' => stream_context_create($opts),
                'cache_wsdl' => WSDL_CACHE_NONE
            );

            if(USE_LOCAL_XML){
                // use local xml file
                $dest = dirname(__FILE__) . DIRECTORY_SEPARATOR.REMEDY_FILENAME_XML_UPDATE_WORKLOG;
            }else{
                $dest = REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG;
            }
            $client = new SoapClient($dest, $params);
            $dataHeader = array(
                "userName" => $userName,
                "password" => $password,
                "authentication" => "?",
                "locale" => "",
                "timeZone" => "?"
            );
            $header = new SOAPHeader(REMEDY_NAMESPACE_UPDATE_WORKLOG, "AuthenticationInfo", $dataHeader, false);
            $client->__setSoapHeaders($header);
        }catch (Exception $e) {
            $result['error message'] = $e->getMessage();
            if(USE_LOCAL_XML){
                $result['file_name'] = REMEDY_FILENAME_XML_UPDATE_TICKET;
            }else{
                $result['url'] = REMEDY_WEBSERVICE_URL_UPDATE_TICKET;
            }
            echo "Error(Create Soap Client) ".$e->getMessage()."</br>";
            exit();
        }
        $params = array(
            "Incident_Number"=>$Incident_Number,
            "Summary"=>$Summary,
            "Note"=>$Note,
            "Work_Log_Type"=>$Work_Info_Type,
            //"Work_Info_Type"=>$Work_Info_Type,
            "z2AF_Work_Log01_attachmentName"=>$z2AF_Work_Log01_attachmentName,
            "z2AF_Work_Log01_attachmentData"=>$z2AF_Work_Log01_attachmentData,
            "z2AF_Work_Log01_attachmentOrigSize"=>$z2AF_Work_Log01_attachmentOrigSize,
            "z2AF_Work_Log02_attachmentName"=>$z2AF_Work_Log02_attachmentName,
            "z2AF_Work_Log02_attachmentData"=>$z2AF_Work_Log02_attachmentData,
            "z2AF_Work_Log02_attachmentOrigSize"=>$z2AF_Work_Log02_attachmentOrigSize,
            "z2AF_Work_Log03_attachmentName"=>$z2AF_Work_Log03_attachmentName,
            "z2AF_Work_Log03_attachmentData"=>$z2AF_Work_Log03_attachmentData,
            "z2AF_Work_Log03_attachmentOrigSize"=>$z2AF_Work_Log03_attachmentOrigSize,
            "Secure_Work_Log"=>$Secure_Work_Log,
            "View_Access"=>$View_Access
        );
        try{
            $result['response'] = $client->New_Create_Operation_CreateIncidentWorkLog($params);
            if(USE_LOCAL_XML){
                $result['file_name'] = REMEDY_FILENAME_XML_UPDATE_WORKLOG;
            }else{
                $result['url'] = REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG;
            }
            $result['params'] = $params;
            return $result;
        }catch (Exception $e) {
            $result['last_response'] = $client->__getLastResponse();
            $result['error_message'] = $e->getMessage();
            $result['params'] = $params;
            if(USE_LOCAL_XML){
                $result['file_name'] = REMEDY_FILENAME_XML_UPDATE_WORKLOG;
            }else{
                $result['url'] = REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG;
            }
            return $result;
        }
    }

    // test soap client create incident case Kbank
    public function createIncidentCase(){
      //https://sos.vspace.in.th/soap/sos_case/index.php


      // data
        $ticket_id = "INC000000000010";
        $company = "KBank";
        $customer = "";
        $contract = "";
        $summary = "";
        $notes = "";
        $reported_source = "";
        $impact = "";
        $urgency = "";
        $priority = "";
        $incident_type = "";
        $assigned_group = "";
        $assignee = "";
        $vendor_group = "";
        $vendor_ticket_number = "";
        $status = "";
        $status_reason = "";
        $resolution = "";
        $employee_contract_id = "";
        $employee_first_name = "";
        $employee_last_name = "";
        $employee_phone_number = "";
        $employee_mobile_number = "";
        $employee_email = "";
        $department = "";
        $organization_title = "";
        $functional_title = "";
        $asset_id = "";
        $incident_location = "";
        $incident_address = "";
        $location_id = "";
        $location_type = "";
        $location_area = "";
        $branch_code = "";
        $grade = "";
        $vendor = "";
        $operational_categorization_tier_1 = "";
        $operational_categorization_tier_2 = "";
        $operational_categorization_tier_3 = "";
        $product_categorization_tier_1 = "";
        $product_categorization_tier_2 = "";
        $product_categorization_tier_3 = "";
        $product_categorization_product_name = "";
        $product_categorization_model_version = "";
        $product_categorization_manufacturer = "";
        $product_categorization_application_tier = "";
        $product_domain = "";
        $product_support_period = "";
        $target_sla = "";
        $reported_date = "";
        $responded_date = "";
        $submitter = "";
        $submit_date = "";
        $owner_group = "";
        $last_work_log = "";
      //
      try{
        $opts = array(
          'http' => array(
              'user_agent' => 'PHPSoapClient'
          )
        );
        $context = stream_context_create($opts);
        $wsdlUrl = "https://sos.vspace.in.th/soap/sos_case/index.php?wsdl";
        $soapClientOptions = array(
            'encoding'=>'utf-8',
            'stream_context' => $context,
            'cache_wsdl' => WSDL_CACHE_NONE
        );
        $client = new SoapClient($wsdlUrl, $soapClientOptions);
        // $dataHeader = array(
        //   "userName" => $userName,
        //   "password" => $password,
        //   "authentication"=>"?",
        //   "locale"=>"",
        //   "timeZone"=>"?"
        // );
        // $ns = "urn:KBANK__Update_Incident__WS001";
        // $header = new SOAPHeader($ns, "AuthenticationInfo", $dataHeader,false);
        // $client->__setSoapHeaders($header);
        $result = $data = $client->__soapCall('create', array(
          "ticket_id"=>$ticket_id,
           "company"=>$company,
           "customer"=>$customer,
           "contract"=>$contract,
           "summary"=>$summary,
           "notes"=>$notes,
           "reported_source"=>$reported_source,
           "impact"=>$impact,
           "urgency"=>$urgency,
           "priority"=>$priority,
           "incident_type"=>$incident_type,
           "assigned_group"=>$assigned_group,
           "assignee"=>$assignee,
           "vendor_group"=>$vendor_group,
           "vendor_ticket_number"=>$vendor_ticket_number,
           "status"=>$status,
           "status_reason"=>$status_reason,
           "resolution"=>$resolution,
           "employee_contract_id"=>$employee_contract_id,
           "employee_first_name"=>$employee_first_name,
           "employee_last_name"=>$employee_last_name,
           "employee_phone_number"=>$employee_phone_number,
           "employee_mobile_number"=>$employee_mobile_number,
           "employee_email"=>$employee_email,
           "department"=>$department,
           "organization_title"=>$organization_title,
           "functional_title"=>$functional_title,
           "asset_id"=>$asset_id,
           "incident_location"=>$incident_location,
           "incident_address"=>$incident_address,
           "location_id"=>$location_id,
           "location_type"=>$location_type,
           "location_area"=>$location_area,
           "branch_code"=>$branch_code,
           "grade"=>$grade,
           "vendor"=>$vendor,
           "operational_categorization_tier_1"=>$operational_categorization_tier_1,
           "operational_categorization_tier_2"=>$operational_categorization_tier_2,
           "operational_categorization_tier_3"=>$operational_categorization_tier_3,
           "product_categorization_tier_1"=>$product_categorization_tier_1,
           "product_categorization_tier_2"=>$product_categorization_tier_2,
           "product_categorization_tier_3"=>$product_categorization_tier_3,
           "product_categorization_product_name"=>$product_categorization_product_name,
           "product_categorization_model_version"=>$product_categorization_model_version,
           "product_categorization_manufacturer"=>$product_categorization_manufacturer,
           "product_categorization_application_tier"=>$product_categorization_application_tier,
           "product_domain"=>$product_domain,
           "product_support_period"=>$product_support_period,
           "target_sla"=>$target_sla,
           "reported_date"=>$reported_date,
           "responded_date"=>$responded_date,
           "submitter"=>$submitter,
           "submit_date"=>$submit_date,
           "owner_group"=>$owner_group,
           "last_work_log"=>$last_work_log
        ));
        return  $result;
      }catch(Exception $e){
        return $e->getMessage();
      }
    }
}
