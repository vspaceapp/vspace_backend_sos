<?php
// require "/home/flguploa/domains/flgupload.com/public_html/case/apis/include/db_handler.php";
// require "/home/flguploa/domains/flgupload.com/public_html/case/apis/include/PDFMerger/PDFMerger.php";
//require "/home/vlogic/sos/public_html/apis/include/PDFMerger/PDFMerger.php";
require __dir__."/../../apis/include/PDFMerger/PDFMerger.php";
// require "/var/www/html/vlogic/public_html/apis/include/MergePDF.php";
class MergePDF{

	private $db;
	private $contract_no;
	private $ticket_sid;
	private $task_sid;
	function __construct(){

		require dirname(__FILE__).'/db_connect.php';
		$db = new DbConnect();
		$this->db = $db->connect();
	}

    public function combinePDF($contract_no){

    		$this->contract_no = $contract_no;

        	$allTicket = $this->getAllTicket();

            $pdf = new PDFMerger();
	
            foreach ($allTicket as $key => $value) {
            	$this->ticket_sid = $value['sid'];
            	// $this->task_sid = $value['tasks'];

            	$listPdf = $allTicket[$key]['task'] = $this->getAllTask();

            	if(count($listPdf) > 0 ){            		
	            	foreach ($listPdf as $k => $v) {
	            		echo $v['no_task']."<br/>";
	            		if($v['path_service_report']!="" && file_exists('/home/vlogic/sos/public_html/pdf/'.$v['path_service_report']))
		            		$pdf->addPDF("/home/vlogic/sos/public_html/pdf/".$v['path_service_report'], 'all');
	            	}
            	}else{
            		// echo 'not have PDF file';
            	}
            }
            $pdf->merge('download', $this->contract_no.".pdf");
            return $allTicket;
	    }

	  	private function getAllTicket(){
	  		$sql = "SELECT * FROM ticket WHERE contract_no = :contract_no AND (case_type = 'Implement' OR case_type = 'Request' OR case_type = 'Install' ) ORDER BY sid ";
	  		$q = $this->db->prepare($sql);
	  		$q->execute(
	  			array(':contract_no'=>$this->contract_no)
	  		);
	  		return $q->fetchAll();
	  	}

	  	private function getAllTask(){
	  		$sql = "SELECT * FROM tasks WHERE ticket_sid = :ticket_sid ORDER BY sid ";
	  		$q = $this->db->prepare($sql);
	  		$q->execute(
	  			array(':ticket_sid'=>$this->ticket_sid)
	  		);

	  		$r = $q->fetchAll();

	  		foreach ($r as $key => $value) {
	  			if(file_exists('/home/vlogic/sos/public_html/pdf/'.$value['no_task'].'.pdf')){
                	$r[$key]['pdf_report'] = '/home/vlogic/sos/public_html/pdf/'.$value['no_task'].'.pdf';
	            }else{
	                $r[$key]['pdf_report'] = "";
	            }
	  		}
	  		return $r;
	  	}

	  	private function getAllPdf(){
	  		$sql = "SELECT * FROM customer_signature_task WHERE task_sid = :task_sid ";
	  		$q = $this->db->prepare($sql);
	  		$q->execute(
	  			array(':task_sid'=>$this->task_sid)
	  		);
	  		return $q->fetchAll();
	  	}

	  // 	public function combinePDF2system(){

            
   //          $pdf = new PDFMerger;

			// $pdf->addPDF('/home/flguploa/domains/flgupload.com/public_html/case/apis/include/PDFMerger/samplepdfs/F16-3017.pdf', 'ALL')
			// ->addPDF('/home/flguploa/domains/flgupload.com/public_html/case/apis/include/PDFMerger/samplepdfs/F16-3017new.pdf', 'ALL')
   //          ->merge('browser', '/home/flguploa/domains/flgupload.com/public_html/case/apis/include/PDFMerger/samplepdfs/TEST2.pdf');
   //          // return $allTicket;
	  //   }
}


?>