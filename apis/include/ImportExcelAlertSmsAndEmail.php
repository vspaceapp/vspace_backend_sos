<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
// define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '');
// date_default_timezone_set('Europe/London');

class ImportExcelAlertSmsAndEmail{
	private $db;
    private $role_sid, $email, $token, $permission_sid;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $db = new DbConnect();
        $this->db = $db->connect();
    }
    function uploadFile($email, $token, $file){
    	$strPath = ROOT_PATH."uploads/";
    	if (isset($file['size']) && $file["size"] < 10000000) {
            $filename = $file["name"];
            $newArrayFileName = explode(".", $filename);
            $date = new DateTime(date('Y-m-d'));
            $date->setTime(date('H'), date('i'), date('s'));
			$newFileName = $date->format('Ymd') ."_". $date->format('His') . "_notification_". $token . "." . $newArrayFileName[count($newArrayFileName) - 1];
            if (move_uploaded_file($file["tmp_name"], $strPath . $newFileName)) {
				return $strPath.$newFileName;
            }
            sleep(2);
        }
        return "";
    }

    function importData($file){
    	require_once '../../../PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';
    	if (!file_exists($file)) {
			exit($file . EOL);
		}
		// echo $file;
		$callStartTime = microtime(true);
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$numberOfSheet = $objPHPExcel->getSheetCount();
		for($i = 0;$i<4;$i++){
		
			$objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
			$highestRow = $objWorksheet->getHighestRow();
			// echo "<br/>";
			$highestColumn = $objWorksheet->getHighestColumn();
			// echo "<br/>";
			

			$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
			// echo "<pre>";
			// print_r($headingsArray);
			// echo "</pre>";
			$headingsArray = $headingsArray[1];

			$r = -1;
			$namedDataArray = array();
			for ($row = 2; $row <= $highestRow; ++$row) {
			    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
			 //    echo "<pre>";
				// print_r($dataRow);
				// echo "</pre>";
				// $headingsArray = $dataRow[$row];
			    // if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
			        ++$r;
			        foreach($headingsArray as $columnKey => $columnHeading) {

			        	$columnHeading = strtolower($columnHeading);
			        	$columnHeading = str_replace("  ", " ", $columnHeading);
			        	$columnHeading = str_replace(" ", "_", $columnHeading);

			            $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
			        }
			    // }
			}

			// echo "<pre>";
			// print_r($namedDataArray);
			// echo "</pre>";
			if($i==0){
				$this->importAlertToDb($namedDataArray);				
			}else if($i==1){
				$this->importAdm($namedDataArray);
			}else if($i==2){
				$this->importGable_tip_group_manager($namedDataArray);
			}else if($i==3){
				$this->import_gable_incharge_team($namedDataArray);
			}

		}
    }
    function import_gable_incharge_team($data){
    	$this->db->beginTransaction();
    	foreach ($data as $key => $value) {
    		$checkExist_gable_incharge_team = $this->checkExist_gable_incharge_team($value);
    		if($checkExist_gable_incharge_team=="0"){
    			$this->insert_gable_incharge_team($value);
    		}else{
    			$this->update_gable_incharge_team($value);
    		}
		}
    	$this->db->commit();
    }
    function checkExist_gable_incharge_team($data){
    	$sql = "SELECT id FROM gable_incharge_team WHERE id = :id ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':id'=>$data['id']));
    	$r = $q->fetch();
    	if(isset($r['id']) && $r['id']>0){
    		return $r['id'];
    	}else{
    		return "0";
    	}
    }
    function insert_gable_incharge_team($data){
    	$sql = "INSERT INTO gable_incharge_team 
    	(id, email, name, group_name, login_id, created_datetime, updated_datetime) 
    	VALUES (:id, :email, :name, :group_name, :login_id, NOW(), NOW()) ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':id'=>$data['id'],
    		':email'=>$data['email'],
    		':name'=>$data['name'],
    		':group_name'=>$data['group_name'],
    		':login_id'=>$data['login_id']
    		));
    }
    function update_gable_incharge_team($data){
    	$sql = "UPDATE gable_incharge_team SET 
    	email = :email, 
    	name = :name,
    	group_name = :group_name,
    	login_id = :login_id,
    	updated_datetime = NOW() 
    	WHERE id = :id";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':email'=>$data['email'],
    		':name'=>$data['name'],
    		':group_name'=>$data['group_name'],
    		':login_id'=>$data['login_id'],
    		':id'=>$data['id']
    		));
    }
    function importGable_tip_group_manager($data){
    	$this->db->beginTransaction();
    	foreach ($data as $key => $value) {
    		$checkExistTipGroupManager = $this->checkExistTipGroupManager($value);
    		if($checkExistTipGroupManager=="0"){
    			$this->insertTipGroupManager($value);
    		}else{
    			$this->updateTipGroupManager($value);
    		}
    	}
    	$this->db->commit();
    }
    function checkExistTipGroupManager($data){
    	$sql = "SELECT id FROM gable_tip_group_manager WHERE id = :id";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':id'=>$data['id']));
    	$r = $q->fetch();
    	if(isset($r['id']) && $r['id']>0){
    		return $r['id'];
    	}else{
    		return "0";
    	}
    }
    function insertTipGroupManager($data){
    	$sql = "INSERT INTO gable_tip_group_manager 
    	(id, type, item, product, group_name, service_mgr, mgr, created_datetime, updated_datetime) VALUES (:id, :type, :item, :product, :group_name, :service_mgr, :mgr, NOW(), NOW()) ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':id'=>$data['id'],
    		':type'=>$data['type'],
    		':item'=>$data['item'],
    		':product'=>$data['product'],
    		':group_name'=>$data['group_name'],
    		':service_mgr'=>$data['service_mgr'],
    		':mgr'=>$data['mgr']
    		));
    }
    function updateTipGroupManager($data){
    	$sql = "UPDATE gable_tip_group_manager SET 
    	type = :type,
    	item = :item,
    	product = :product,
    	group_name = :group_name,
    	service_mgr = :service_mgr,
    	mgr = :mgr,
    	updated_datetime = NOW() WHERE id = :id ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':type'=>$data['type'],
    		':item'=>$data['item'],
    		':product'=>$data['product'],
    		':group_name'=>$data['group_name'],
    		':service_mgr'=>$data['service_mgr'],
    		':mgr'=>$data['mgr'],
    		':id'=>$data['id']
    		));
    }

    function importAdm($data){
    	$this->db->beginTransaction();
    	foreach ($data as $key => $value) {
    		$checkExistAdm = $this->checkExistAdm($value);
    		if($checkExistAdm=="0"){
    			$this->insertAdm($value);
    		}else{
    			$this->updateAdm($value);
    		}
    	}
    	$this->db->commit();
    }
    function checkExistAdm($data){
    	$sql = "SELECT id FROM gable_adm WHERE id = :id ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':id'=>$data['id']));
    	$r = $q->fetch();
    	if(isset($r['id']) && $r['id']>0){
    		return $r['id'];
    	}else{
    		return "0";
    	}
    }
    function insertAdm($data){
    	$sql = "INSERT INTO gable_adm (id, group_code, role, email) VALUES (:id, :group_code, :role, :email) ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':id'=>$data['id'],
    		':group_code'=>$data['group_code'],
    		':role'=>$data['role'],
    		':email'=>$data['email']
    		));
    }
    function updateAdm($data){
    	$sql = "UPDATE gable_adm SET group_code = :group_code, role = :role, email = :email WHERE id = :id ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':id'=>$data['id'],
    		':group_code'=>$data['group_code'],
    		':role'=>$data['role'],
    		':email'=>$data['email']
    		));

    }
    function importAlertToDb($data){
    	$this->db->beginTransaction();
    	foreach ($data as $key => $value) {
    		$checkExistAlert = $this->checkExistAlert($value);
    		// echo $checkExistAlert."<br/>";
    		if($checkExistAlert=="0"){
    			$this->insertAlert($value);
    		}else{
    			$this->updateAlert($value);
    		}
    	}
    	$this->db->commit();
    }
    function insertAlert($data){
    	$sql = "INSERT INTO gable_alert 
    	(id, contract, group_code, assigned_group, status,
    	adm, adm_mgr, incharge, service_mgr, service_dept_mgr,
    	other1_email, other1, 
    	other2_email, other2,
    	other3_email, other3,
    	other4_email, other4,
    	other5_email, other5,
    	created_datetime, updated_datetime) 
    	VALUES 
    	(:id, :contract, :group_code, :assigned_group,:status,
    	:adm, :adm_mgr, :incharge, :service_mgr, :service_dept_mgr,
    	:other1_email, :other1,
    	:other2_email, :other2,
    	:other3_email, :other3,
    	:other4_email, :other4,
    	:other5_email, :other5,
    	now(), now()) ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':id'=>$data['id'],
    		':contract'=>$data['contract'],
    		':group_code'=>(($data['group_code']=="TRUE")?'TRUE':$data['group_code']),
    		':assigned_group'=>$data['assigned_group'],
    		':status'=>$data['status'],
    		':adm'=>$data['adm'],
    		':adm_mgr'=>$data['adm_mgr'],
    		':incharge'=>$data['incharge'],
    		':service_mgr'=>$data['service_mgr'],
    		':service_dept_mgr'=>$data['service_dept_mgr'],
    		':other1_email'=>$data['other1_email'],
    		':other1'=>$data['other1'],
    		':other2_email'=>$data['other2_email'],
    		':other2'=>$data['other2'],
    		':other3_email'=>$data['other3_email'],
    		':other3'=>$data['other3'],
    		':other4_email'=>$data['other4_email'],
    		':other4'=>$data['other4'],
    		':other5_email'=>$data['other5_email'],
    		':other5'=>$data['other5']
    		));
    }
    function updateAlert($data){
    	$sql = "UPDATE gable_alert SET 
    	contract = :contract,
    	group_code = :group_code,
    	assigned_group = :assigned_group,
    	status = :status,
    	adm = :adm,
    	adm_mgr = :adm_mgr,
    	incharge = :incharge,
    	service_mgr = :service_mgr,
    	service_dept_mgr = :service_dept_mgr,
    	other1_email = :other1_email,
    	other1 = :other1,
    	other2_email = :other2_email,
    	other2 = :other2,
    	other3_email = :other3_email,
    	other3 = :other3,
    	other4_email = :other4_email, 
    	other4 = :other4,
    	other5_email = :other5_email,
    	other5 = :other5,
    	updated_datetime = NOW() 
    	WHERE id = :id ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':contract'=>$data['contract'],
    		':group_code'=>(($data['group_code']=="TRUE")?'TRUE':$data['group_code']),
    		':assigned_group'=>$data['assigned_group'],
    		':status'=>$data['status'],
    		':adm'=>$data['adm'],
    		':adm_mgr'=>$data['adm_mgr'],
    		':incharge'=>$data['incharge'],
    		':service_mgr'=>$data['service_mgr'],
    		':service_dept_mgr'=>$data['service_dept_mgr'],
    		':other1_email'=>$data['other1_email'],
    		':other1'=>$data['other1'],
    		':other2_email'=>$data['other2_email'],
    		':other2'=>$data['other2'],
    		':other3_email'=>$data['other3_email'],
    		':other3'=>$data['other3'],
    		':other4_email'=>$data['other4_email'],
    		':other4'=>$data['other4'],
    		':other5_email'=>$data['other5_email'],
    		':other5'=>$data['other5'],
    		':id'=>$data['id']
    		));
    }
    function checkExistAlert($data){
    	$sql = "SELECT id FROM gable_alert WHERE id = :id";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':id'=>$data['id']));
    	$r = $q->fetch();
    	if(isset($r['id']) && $r['id']>0){
    		return $r['id'];
    	}else{
    		return "0";
    	}
    }
    function import(){

    	require_once '../../../PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

		$strPath = "../../../uploads/";
		$OpenFile = "Alert for SMSandEMAIL_28062017.xlsx";


		if (!file_exists($strPath.$OpenFile)) {
			exit($OpenFile . EOL);
		}
		// echo date('H:i:s') , " Load from Excel2007 file" , EOL;
		$callStartTime = microtime(true);
		$objPHPExcel = PHPExcel_IOFactory::load($strPath.$OpenFile);
		$numberOfSheet = $objPHPExcel->getSheetCount();
		for($i = 0;$i<1;$i++){
		
			$objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
			echo $highestRow = $objWorksheet->getHighestRow();
			echo "<br/>";
			echo $highestColumn = $objWorksheet->getHighestColumn();
			echo "<br/>";
			

			$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
			// echo "<pre>";
			// print_r($headingsArray);
			// echo "</pre>";
			$headingsArray = $headingsArray[1];

			$r = -1;
			$namedDataArray = array();
			for ($row = 1; $row <= $highestRow; ++$row) {
			    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
			    echo "<pre>";
				print_r($dataRow);
				echo "</pre>";
				$headingsArray = $dataRow[$row];
			    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
			        ++$r;
			        foreach($headingsArray as $columnKey => $columnHeading) {
			            $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
			        }
			    }
			}

			// echo "<pre>";
			// print_r($namedDataArray);
			// echo "</pre>";
			echo "<hr/>";
		}

    }
}

?>