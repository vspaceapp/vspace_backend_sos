<?php
class ProjectNoteModel{

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function listProjectNote(){
    	$sql = "SELECT * FROM project_note WHERE 1 ";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();
    	return $r;
    }

    public function insertProjectNote($project_name,$price,$prices_remaining,$sales,$create_by){
    	$sql = "INSERT INTO project_note (project_name, price, prices_remaining, sales,create_datetime, create_by) VALUES (:project_name, :price, :prices_remaining, :sales, NOW(), :create_by) ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':project_name'=>$project_name,
    		':price'=>$price,
    		':prices_remaining'=>$prices_remaining,
    		':sales'=>$sales,
    		':create_by'=>$create_by
    		));
    }

    //select *, col1, col2 FROM TABLE
    //insert into TABLE (col1,col2) values (:col1, :col2)
    // UPDATE TABLE SET col1 = 4, col2 = :col2 WHERE col3 = '1'
    // DELETE FROM TABLE WHERE col1 = '1'
}