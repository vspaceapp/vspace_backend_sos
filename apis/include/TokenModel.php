<?php

class TokenModel {

    private $db;
    private $objMaster;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objMaster = $db = new DbConnect();
        $this->db = $db->connect();
    }
    public function info($token, $email){
        $verifyToken = $this->verifyToken($token, $email);

        if($verifyToken['email']==$email){
            return $verifyToken;
        }else{
            return array();
        }
    }
    public function getEmpno($email){
        $sql = "SELECT empno FROM employee WHERE emailaddr = :emailaddr";
        $q = $this->db->prepare($sql);
        $q->execute(array(':emailaddr'=>$email));
        $r = $q->fetch();
        return $r['empno'];
    }
    public function verifyToken($token, $email){
        try{
            $sql = "SELECT T.user_sid,U.role_sid, U.picture_profile, U.email,
            CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END name
            FROM token T
            LEFT JOIN user U ON U.sid = T.user_sid
            WHERE T.token = :token
            ORDER BY T.sid LIMIT 0,1 ";

            $q = $this->db->prepare($sql);
            $q->execute(array(':token'=>$token));
            return $r = $q->fetch();

        }catch(PDOException $e) {
            $error = $e->getMessage();
      	}
    }

    public function permissionTicket($role_sid){
        try{
            $sql = "SELECT permission_ticket FROM role WHERE sid = :sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$role_sid));
            $r = $q->fetch();
            return $r['permission_ticket'];
        }catch(PDOException $e){
            $error = $e->getMessage();
        }
    }
    public function permissionTask($role_sid){
        try{
            $sql = "SELECT can_assign FROM role WHERE sid = :sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$role_sid));
            $r = $q->fetch();
            return $r['can_assign'];
        }catch(PDOException $e){
            $error = $e->getMessage();
        }
    }
    public function permissionProject($role_sid){
        try{
            $sql = "SELECT permission_project FROM role WHERE sid = :sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$role_sid));
            $r = $q->fetch();
            return $r['permission_project'];
        }catch(PDOException $e){
            $error = $e->getMessage();
        }
    }

    private function getTitleRoleName($role){
      $sql = "SELECT * FROM role WHERE sid = :sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(':sid'=>$role));
      return $r = $q->fetch();
    }

    public function userInRole($role){

      if($role!=""){
          $sql = "SELECT CASE WHEN U.formal_name <> '' THEN CONCAT(U.formal_name,' ',U.mobile) ELSE CONCAT(U.name,' ',U.mobile) END title,
          U.email, U.mobile, CASE WHEN U.formal_name <> '' THEN CONCAT(U.formal_name) ELSE CONCAT(U.name) END name,
          U.latitude, U.longitude,U.picture_profile,U.sid, U.updated_datetime, U.role_sid
            FROM user U WHERE U.role_sid IN (".$role.") AND U.active = 1 AND U.latitude IS NOT NULL AND U.latitude <> '' AND U.latitude <> 0 ORDER BY FIELD(U.role_sid,".$role."), U.email ASC ";
          $q = $this->db->prepare($sql);
          $q->execute(array(':role_sid'=>$role));
          $data = array();
          return $r = $q->fetchAll();
          // $currentRoleSid = 0;
          // foreach ($r as $key => $value) {
          //   // if($value['role_sid']!=$currentRoleSid){
          //   //   $title = $this->getTitleRoleName($value['role_sid']);
          //   //   $currentRoleSid = $value['role_sid'];
          //   //   array_push($data, array('title'=>$title['name'],'name'=>$title['name'],'email'=>''));
          //   // }
          //   array_push($data, $value);
          // }
          // return $data;
      }else{
        return array();
      }
    }

    public function createToken($data = array()) {
        $token = NULL;
        $repeat = FALSE;
        do {
            $email = isset($data['email']) ? $data['email'] : "";
            $ip_address = isset($data['ip_address']) ? $data['ip_address'] : "";

            $datetime = $this->getDateTime();

            $token = md5($email . $datetime . $ip_address);
            $repeat = $this->isRepeatToken($token);

        } while ($repeat);

        return $token;
    }

    public function getDateTime() {
        $sql = "SELECT DATE_FORMAT(now(),'%Y-%m-%d %H:%i:%s') datetimenow ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r['datetimenow'];
    }

    private function isRepeatToken($token) {
        $sql = "SELECT sid FROM token WHERE token = :token";
        $q = $this->db->prepare($sql);
        $q->execute(array(':token' => $token));
        $r = $q->fetch();

        if (isset($r['sid']) && $r['sid'] > 0) {
            sleep(1);
            return TRUE;
        }
        return FALSE;
    }

    public function insertToken($data = array()) {

        $user_sid = isset($data['user_sid']) ? $data['user_sid'] : "";
        $token = isset($data['token']) ? $data['token'] : "";
        $app_sid = isset($data['app_sid']) ? $data['app_sid'] : "";
        $status = isset($data['status']) ? $data['status'] : "";
        $ip_address = isset($data['ip_address']) ? $data['ip_address'] : "";
        $email = isset($data['email']) ? $data['email']:"";

        $sql = "INSERT INTO token (email,user_sid,token,start,expire,app_sid,status,ip_address) "
                . "VALUES (:email,:user_sid,:token,NOW(),DATE_ADD(NOW(),INTERVAL 60 DAY) ,:app_sid,:status,:ip_address) ";
        $param = array(
            ':email'=>$email,
            ':user_sid' => $user_sid, ':token' => $token,
            ':app_sid' => $app_sid, ':status' => $status,
            ':ip_address' => $ip_address
        );
        $q = $this->db->prepare($sql);
        $q->execute($param);
    }
    public function createTokenGeneral($data = array()) {
        $token = NULL;
        $repeat = FALSE;
        do {
            $user_id = isset($data['user_sid']) ? $data['user_sid'] : "";
            $ip_address = isset($data['ip_address']) ? $data['ip_address'] : "";

            $datetime = $this->getDateTime();

            $token = md5($user_id . $datetime . $ip_address . Helper::$encode_md5);
            $repeat = $this->isRepeatTokenGeneral($token);

        } while ($repeat);

        return $token;
    }

    private function isRepeatTokenGeneral($token) {
        $sql = "SELECT sid FROM user_general_token WHERE token = :token";
        $q = $this->db->prepare($sql);
        $q->execute(array(':token' => $token));
        $r = $q->fetch();

        if (isset($r['sid']) && $r['sid'] > 0) {
            sleep(1);
            return TRUE;
        }
        return FALSE;
    }

    public function insertTokenGeneral($data = array()) {

        $user_sid = isset($data['user_sid']) ? $data['user_sid'] : "";
        $token = isset($data['token']) ? $data['token'] : "";
        $app_sid = isset($data['app_sid']) ? $data['app_sid'] : "";
        $status = isset($data['status']) ? $data['status'] : "";
        $ip_address = isset($data['ip_address']) ? $data['ip_address'] : "";

        $sql = "INSERT INTO user_general_token (user_general_sid,token,start,expire,app_sid,status,ip_address) "
                . "VALUES (:user_sid,:token,NOW(),DATE_ADD(NOW(),INTERVAL 60 DAY) ,:app_sid,:status,:ip_address) ";
        $param = array(
            ':user_sid' => $user_sid, ':token' => $token,
            ':app_sid' => $app_sid, ':status' => $status,
            ':ip_address' => $ip_address
        );
        $q = $this->db->prepare($sql);
        $q->execute($param);
    }

    public function updateRegisterOnlyReact($email, $registrationId){
        $sql = "UPDATE user SET gcm_registration_id = :registrationId, updated_datetime = NOW() WHERE email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':registrationId'=>$registrationId,
            ':email'=>$email
            ));
        $sql = "UPDATE user_location SET registration_id = :registration_id, updated_registration_id = NOW() WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':registration_id'=>$registrationId,
            ':email'=>$email
        ));
    }

}
