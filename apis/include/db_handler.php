<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 */
require_once __dir__."/../core/constant.php";
class DbHandler {

    private $conn;
    private $objMaster;
    private $listServiceType;

    private $table_ticket = "ticket";
    private $table_tasks = "tasks";
    private $table_tasks_log = "tasks_log";
    private $table_task_input_action = "task_input_action";

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objMaster = $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function findEmployee($sql){
        $q = $this->conn->prepare($sql);
        $q->execute();
        return $q->fetchAll();
    }
    public function getEmailByToken($token){
        $sql = "SELECT email FROM token WHERE token = :token";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':token'=>$token));
        $r = $q->fetch();
        return $r['email'];
    }
    // creating new user if not existed
    public function checklogin($email, $password,$registration_id="",$device_model="",$device_platform="",$device_version="") {
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email, $password)) {

                $response["error"] = true;
                $response["message"] = "Oops! this email or password incorrect";

        } else {
            // User with same email already existed in the db
            $response["error"] = false;
            $response["user"] = $this->getUserByEmail($email);

            $ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";
            $token = new TokenModel();
            $tokenCreated = $token->createToken(array('email' => $email, 'ip_address' => $ip_address));
            // $tokenCreated = $token->createTokenGeneral(array('user_id' => $user_sid, 'ip_address' => $ip_address));
            // echo $email;
            $token->insertToken(
                array('email'=>$email,'user_sid' => $response['user']['sid'], 'token' => $tokenCreated,'app_sid' => "2", 'status' => '100', 'ip_address' => $ip_address)
            );
            if($password!="mas123"){
                $this->updateDataUser($email, $registration_id, $device_model, $device_platform, $device_version);
            }
            $response["token"] = $tokenCreated;
            $response["status"] = 200;
            $response["model"] = $response["user"];
            $response["model"]["token"] = $tokenCreated;
            $response["model"]["pic"] = (($response["user"]["maxportraitfile"]!="")?$response['user']['maxportraitfile']:"default.png");
            $response["model"]["pic_full_path"] = END_POINT_PATH_EMPLOYEE.(($response["user"]["maxportraitfile"]!="")?$response['user']['maxportraitfile']:"default.png");
        }

        return $response;
    }

    public function updateDataUser($email,$registration_id,$device_model,$device_platform,$device_version){
        if($email!="" && $registration_id!="" && $device_model !="" && $device_platform != "" && $device_version!=""){
            $sql = "UPDATE user SET gcm_registration_id = :registration_id, updated_datetime = NOW(), device_model = :device_model,
                device_platform = :device_platform, device_version = :device_version WHERE email = :email ";
            $q = $this->conn->prepare($sql);
            $q->execute(array(
                ':registration_id'=>$registration_id,
                ':device_model'=>$device_model,
                ':device_platform'=>$device_platform,
                ':device_version'=>$device_version,
                ':email'=>$email
            ));
        }
    }

    // updating user GCM registration ID
    public function updateGcmID($user_id, $gcm_registration_id) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE user SET gcm_registration_id = :gcm_registration_id, updated_datetime = NOW()  WHERE email = :email ");
        // $stmt->bind_param("si", $gcm_registration_id, $user_id);

        if ($stmt->execute(array(':gcm_registration_id'=>$gcm_registration_id,':email'=>$user_id))) {
            // User successfully updated
            $response["error"] = false;
            $response["message"] = 'GCM registration ID updated successfully';
        } else {
            // Failed to update user
            $response["error"] = true;
            $response["message"] = "Failed to update GCM registration ID";
            // $stmt->error;
        }
        // $stmt->close();

        return $response;
    }

    // fetching single user by id
    public function getUser($user_id) {
        $stmt = $this->conn->prepare("SELECT user_id, name, email, gcm_registration_id, created_at FROM users WHERE user_id = :user_id");
        // $stmt->bind_param("s", $user_id);
        if ($stmt->execute(array(':user_id'=>$user_id))) {
            // $user = $stmt->get_result()->fetch_assoc();
            // $stmt->bind_result($user_id, $name, $email, $gcm_registration_id, $created_at);
            $r = $stmt->fetch();
            $user = array();
            $user["user_id"] = $user_id;
            $user["name"] = $r['name'];
            $user["email"] = $r['email'];
            $user["gcm_registration_id"] = $r['gcm_registration_id'];
            $user["created_at"] = $r['created_at'];
            // $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    // fetching multiple users by ids
    public function getUsers($user_ids) {

        $users = array();
        if (sizeof($user_ids) > 0) {
            $query = "SELECT user_id, name, email, gcm_registration_id, created_at FROM users WHERE user_id IN (";

            foreach ($user_ids as $user_id) {
                $query .= $user_id . ',';
            }

            $query = substr($query, 0, strlen($query) - 1);
            $query .= ')';

            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            // $result = $stmt->get_result();
 			$result = $stmt->fetchAll();
            // while ($user = $result->fetch_assoc()) {
 			foreach ($result as $key => $value) {
 				# code...
 				$tmp = array();
                $tmp["user_id"] = $value['user_id'];
                $tmp["name"] = $value['name'];
                $tmp["email"] = $value['email'];
                $tmp["gcm_registration_id"] = $value['gcm_registration_id'];
                $tmp["created_at"] = $value['created_at'];
                array_push($users, $tmp);
 			}
            // }
        }

        return $users;
    }


    public function getDataTaskForPDF($tasks_sid, $email=""){
        // if($email!=""){
        //     $this->objMaster->setTable($email);
        // }
        $sql = "SELECT TL.tasks_sid,DATE_FORMAT(TL.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,
                    TICKET.subject subject,TICKET.end_user,
                    TL.engineer engineer_email,TL.create_datetime,
                    TL.status, TICKET.description ticket_description, TICKET.project_name ticket_project_name,
                    TICKET.serial_no ticket_serial, TICKET.contract_no ticket_contract, TICKET.end_user_company_name ticket_end_user_company_name,
                    TICKET.end_user_site ticket_end_user_site, TICKET.end_user_contact_name ticket_end_user_contact_name,
                    TICKET.end_user_phone ticket_end_user_phone, TICKET.end_user_mobile ticket_end_user_mobile,
                    TICKET.end_user_email ticket_end_user_email, TICKET.case_type ticket_case_type,TICKET.sid ticket_sid, TICKET.case_company,
                    T.end_user_contact_name_service_report,T.end_user_phone_service_report,T.end_user_mobile_service_report,
                    T.end_user_email_service_report,T.end_user_company_name_service_report,T.subject_service_report, T.service_report_address,
                    TL.status,DATE_FORMAT(TL.appointment,'%d/%m/%Y %H:%i') appointment,
                    (SELECT travel_by FROM task_travel WHERE task_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1) travel_by,
                    TICKET.no_ticket, T.no_task,T.is_follow_up,
                    E.thainame,E.empno, USER.formal_name thainame,
                    CST.file_name customer_signature,ST.name service_type_name, E.comptname comptname, E.compename,
                    USER.path_signature,USER_CREATED.company_logo, USER_CREATED.compename, USER_CREATED.comptname, T.file_name_signated,
                    T.create_by created_appointment_by, DATE_FORMAT(T.follow_up_date,'%Y-%m-%d %H:%i') follow_up_date
                    FROM tasks_log TL
                    LEFT JOIN tasks T ON T.sid = TL.tasks_sid
                    LEFT JOIN service_type ST ON T.service_type = ST.sid
                    LEFT JOIN ticket TICKET ON TICKET.sid = T.ticket_sid
                    LEFT JOIN employee E ON E.emailaddr = TL.engineer
                    LEFT JOIN user USER ON E.emailaddr = USER.email
                    LEFT JOIN user USER_CREATED ON USER_CREATED.email = T.engineer
                    LEFT JOIN customer_signature_task CST ON CST.task_sid = TL.tasks_sid
                WHERE
                1
                AND TL.tasks_sid = :tasks_sid AND TL.status >= 400 ORDER BY TICKET.create_datetime DESC ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $stmt->fetch();
            $r['serial'] = $this->getSerialTicket($r['tasks_sid']);
            $r['to_do_pm'] = $this->getTodoPM($r['ticket_sid']);
            $r['time_log_arrive'] = $this->getTimeLogTask($r['tasks_sid'],200);

            $start = $this->getTimeLogTask($r['tasks_sid'],300);
            $r['time_log_start'] = $start;
            $end = $this->getTimeLogTask($r['tasks_sid'],400);
            $r['time_log_end'] = $end;

            $r['service_period'] = $this->calServicePeriod($start['create_datetime'],$end['create_datetime']);
            $r['input_action'] = $this->getInputAction($r['tasks_sid']);
            $r['associate'] = $this->getEngineerAssociate($r['tasks_sid'], $r['engineer_email']);
            $r['replace_part'] = $this->getReplacePart($r['tasks_sid']);
            $r['prefix_table'] =  $this->objMaster->getTablePrefix($this->objMaster->getRoleByEmail($email));
            $r['comp_refer_ebiz'] = $this->objMaster->getCompReferEBiz($this->objMaster->getRoleByEmail($email));

        return $r;
    }

    private function getReplacePart($tasks_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_spare." WHERE task_sid = :task_sid AND status > 0 ORDER BY sid ASC ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':task_sid'=>$tasks_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function getInputAction($tasks_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_input_action." WHERE task_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetch();
        return $r;
    }

    private function getEngineerAssociate($task_sid, $engineer_email){
        $sql = "SELECT TL.engineer, E.thainame,E.empno, U.formal_name thainame FROM ".$this->objMaster->table_tasks_log." TL
            LEFT JOIN employee E ON E.emailaddr = TL.engineer
            LEFT JOIN user U ON E.emailaddr = U.email
            WHERE TL.tasks_sid = :tasks_sid AND TL.engineer <> :engineer AND TL.status > 250 GROUP BY TL.engineer ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$task_sid,':engineer'=>$engineer_email));
        $r = $q->fetchAll();
        return $r;
    }


    private function getSerialTicket($tasks_sid){
        $sql = "SELECT S.*,TS.serial_detail
            FROM ".$this->objMaster->table_task_do_serial_pm." S
            LEFT JOIN ".$this->objMaster->table_ticket_serial." TS ON S.ticket_serial_sid = TS.sid
                WHERE S.task_sid = :tasks_sid AND S.status > 0 GROUP BY S.ticket_serial_sid ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function calServicePeriod($start, $end){
        $sql = "SELECT TIMEDIFF('".$end."','".$start."') period ;";
        $q = $this->conn->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r['period'];
    }

    private function getTimeLogTask($task_sid, $status){
        $sql = "SELECT
                DATE_FORMAT(create_datetime,'%d-%m-%Y') log_task_date,
                DATE_FORMAT(create_datetime,'%H:%i') log_task_time,
                create_datetime
                FROM ".$this->objMaster->table_tasks_log."
                WHERE tasks_sid = :tasks_sid
                AND status = :status
                ORDER BY sid ASC LIMIT 0,1 ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$task_sid,':status'=>$status));
        $r = $q->fetch();
        return $r;
    }
    //addResponseTaskEngineer
    public function addResponseTaskEngineer($engineer_email, $tasks_sid, $status,$create_by){
        $response = array();
        $sql = "INSERT INTO tasks_engineer (tasks_sid,engineer_email,status,create_datetime,create_by)
                    VALUES (:tasks_sid,:engineer_email,:status,NOW(),:create_by) ";
        $q = $this->conn->prepare($sql);
        $result = $q->execute(array(
            ':tasks_sid'=>$tasks_sid,
            ':engineer_email'=>$engineer_email,
            ':status'=>$status,
            ':create_by'=>$create_by
        ));

        if($result){
            $response['error'] = false;

            $q = $this->conn->prepare("SELECT MAX(status) max_status FROM tasks_log WHERE tasks_sid = :tasks_sid ");
            $q->execute(array(':tasks_sid'=>$tasks_sid));
            $r = $q->fetch();
            $response['max_status'] = $r['max_status'];

        }else{
            $response['error'] = true;
            $response['message'] = 'Failed send message';
        }
        return $response;
    }


    public function addNewStatusTaskLog($engineer_email, $tasks_sid, $status, $create_by, $taxi_fare ="", $lat="", $lng="", $follow_up=0, $signature=''){

        // $this->objMaster->setTable($engineer_email);

        $sql = "INSERT INTO tasks_log
        (tasks_sid,froms,engineer,create_by,create_datetime,appointment, expect_finish, status, taxi_fare,latitude,longitude,datetime_to_report, request_taxi, request_ot)
            SELECT tasks_sid,froms,:engineer_email, :create_by, NOW() ,appointment, expect_finish, :status, :taxi_fare,:latitude,:longitude,NOW(),request_taxi, request_ot
            FROM ".$this->objMaster->table_tasks_log." WHERE tasks_sid = :tasks_sid AND engineer = :engineer_email ORDER BY create_datetime DESC LIMIT 0,1 ";

        $q = $this->conn->prepare($sql);
        $result = $q->execute(
                array(':engineer_email'=>$engineer_email,':create_by'=>$create_by,':status'=>$status,':tasks_sid'=>$tasks_sid,':taxi_fare'=>$taxi_fare,':latitude'=>$lat,':longitude'=>$lng)
            );

        $sql = "UPDATE tasks SET last_status = :status, updated_by = :updated_by, updated_datetime = NOW(), is_follow_up = :is_follow_up WHERE  sid = :sid AND engineer = :engineer ";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(':status'=>$status,':updated_by'=>$create_by, ':is_follow_up'=>$follow_up,':sid'=>$tasks_sid, ':engineer'=>$engineer_email));


        $sql = "UPDATE tasks_engineer SET status = :status, updated_datetime = NOW() WHERE tasks_sid = :sid AND engineer = :engineer ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':status'=>$status,':sid'=>$tasks_sid,':engineer'=>$engineer_email));


        $this->updateTicketToZero($tasks_sid);

        if($status=="500" && $signature!=''){
            $this->insertCustomerSignatureTask($signature, $tasks_sid, $engineer_email);
        }
    }

    public function updateTicketToZero($tasks_sid){
        $sql = "SELECT ticket_sid FROM tasks WHERE sid = :tasks_sid";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetch();

        $sql = "UPDATE ticket SET updated_number_sr = '0000-00-00 00:00:00' WHERE  sid = :sid ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':sid'=>$r['ticket_sid']));
    }

    public function insertCustomerSignatureTask($signature, $tasks_sid, $engineer) {
        // $rootPath = "/var/www/html/vlogic/public_html/";
        $rootPath = ROOT_PATH;

        $filteredData = substr($signature, strpos($signature, ",") + 1);
        //Decode the string
        $unencodedData = base64_decode($filteredData);
        $file_initial_name = uniqid() .'_'.$tasks_sid;
        $file = $file_initial_name. '.png';

        $pdfFileName = $file_initial_name.".pdf";

        //Save the image
        file_put_contents(__dir__.'/../../uploads/signature/' . $file, $unencodedData);

        $sql = "INSERT INTO customer_signature_task (file_name,create_datetime,task_sid) VALUES (:file_name,NOW(),:task_sid) ";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(
            ':file_name' => $file,
            ':task_sid' => $tasks_sid,
            // ':signature_base64'=> $filteredData
        ));

        $sql = "UPDATE tasks SET customer_signated = '1', file_name_signated = :file_name, path_service_report = :pdfFileName WHERE sid = :sid ";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(':file_name'=>$file,':pdfFileName'=>$pdfFileName ,':sid'=>$tasks_sid));

        // fopen("http://vspace.in.th/nodes/pdf/?task=".$tasks_sid."&email=".$engineer,"r");
        // old fopen("http://node02.vspace.in.th/pdf/?task=".$tasks_sid."&email=".$engineer,"r");
        // old fopen("http://node03.vspace.in.th/pdf/?task=".$tasks_sid."&email=".$engineer,"r");

        // fopen("http://vspace.in.th/api/sendmail/sendEmailAfterSigned/".$tasks_sid."/".$engineer, "r");
        // if($engineer!=""){
        //     $sql = "SELECT sid FROM ".$obj->table_tasks_log." WHERE tasks_sid = :tasks_sid AND is_deleted = '0' AND status = '500' ";
        //     $q = $this->db->prepare($sql);
        //     $q->execute(array(':tasks_sid'=>$task_sid));
        //     $r = $q->fetch();
        //     if($r['sid']>0){

        //     }else{

        //     }
        // }
    }

    // messaging in a chat room / to persional message
    public function addMessage($user_id, $chat_room_id, $message) {
        $response = array();

        $stmt = $this->conn->prepare("INSERT INTO messages (chat_room_id, user_id, message) values(:chat_room_id, :user_id, :message)");
        // $stmt->bind_param("iis", $chat_room_id, $user_id, $message);

        $result = $stmt->execute(array(':chat_room_id'=>$chat_room_id, ':user_id'=>$user_id, ':message'=>$message));

        if ($result) {
            $response['error'] = false;

            // get the message
            $message_id = $this->conn->lastInsertId();
            $stmt = $this->conn->prepare("SELECT message_id, user_id, chat_room_id, message, created_at FROM messages WHERE message_id = :message_id");
            // $stmt->bind_param("i", $message_id);
            $param = array(':message_id'=>$message_id);

            if ($stmt->execute($param)) {
                // $stmt->bind_result($message_id, $user_id, $chat_room_id, $message, $created_at);
                $r = $stmt->fetch();
                $tmp = array();
                $tmp['message_id'] = $message_id;
                $tmp['chat_room_id'] = $chat_room_id;
                $tmp['message'] = $message;
                $tmp['created_at'] = $r['created_at'];
                $response['message'] = $tmp;
            }
        } else {
            $response['error'] = true;
            $response['message'] = 'Failed send message';
        }

        return $response;
    }

    // fetching all chat rooms
    // LEFT JOIN ticket_log TKL ON TKL.ticket_sid = T.ticket_sid
     // AND TKL.sid = (SELECT MAX(sid) FROM ticket_log WHERE TKL.ticket_sid = ticket_sid )
    public function getTasks($email, $status="",$start="", $end="") {
        // $this->objMaster->setTable($email);

        $this->listServiceType = $this->listServiceType();
        $sql = "SELECT TL.tasks_sid,DATE_FORMAT(TL.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,
                    TICKET.subject subject,T.service_type,
                    TL.engineer engineer_email, TL.status tasks_log_status,
                    DATE_FORMAT(T.appointment,'%d/%m/%y %H:%i') appointment,TICKET.end_user_company_name,TICKET.end_user_site,
                    T.ticket_sid,TICKET.no_ticket,T.no_task, TIMESTAMPDIFF(minute, NOW(), T.appointment) onsite_in,
                    T.appointment appointment_datetime, DATE_ADD(T.appointment, INTERVAL T.expect_duration HOUR) expect_datetime
                FROM ".$this->objMaster->table_tasks." T
                    LEFT JOIN ".$this->objMaster->table_tasks_log." TL ON T.sid = TL.tasks_sid AND TL.engineer = :email
                    LEFT JOIN ".$this->objMaster->table_ticket." TICKET ON TICKET.sid = T.ticket_sid
                WHERE TL.sid = (SELECT (TTL.sid) FROM ".$this->objMaster->table_tasks_log." TTL WHERE TL.tasks_sid = TTL.tasks_sid AND (TTL.engineer = :email) ORDER BY TTL.status DESC,TTL.create_datetime DESC LIMIT 0,1 )
                AND TL.engineer = :email AND TL.status >= '0'  ";
        if($status!=""){
            $sql .= " AND TL.status >= '".$status."' ";
        }else{
            $sql .= " AND TL.status < '600' ";
        }

        if($start!="" && $end!=""){
            $sql .= "AND T.appointment >= '".$start."' AND T.appointment <= '".$end."' ";
        }
        $sql .= "ORDER BY T.appointment ASC ";

                    // LEFT JOIN tasks_engineer TE ON TE.tasks_sid = T.sid AND (TE.status = '1' OR TE.status = '100')
                // -- AND TE.sid = (SELECT MAX(sid) FROM tasks_engineer WHERE TL.tasks_sid = tasks_sid)
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(array(':email'=>$email));
        // $tasks = $stmt->get_result();
        // $stmt->close();

        $tasks = $stmt->fetchAll();

        foreach ($tasks as $key => $value) {
            $serviceType = $this->getTypeService($value['service_type']);
            $tasks[$key]['service_type_name'] = $serviceType['name'];
            $tasks[$key]['service_type_icon'] = $serviceType['icon'];
        }
        return $tasks;
    }

    private function setTable($email){
        // $role_sid = $this->objMaster->getRoleByEmail($email);
        // if($role_sid){
        //     $this->table_ticket = $this->objMaster->getTablePrefix($role_sid).$this->table_ticket;
        //     $this->table_tasks = $this->objMaster->getTablePrefix($role_sid).$this->table_tasks;
        //     $this->table_tasks_log = $this->objMaster->getTablePrefix($role_sid).$this->table_tasks_log;
        //     $this->table_task_input_action = $this->objMaster->getTablePrefix($role_sid).$this->table_task_input_action;
        // }else{
        //     $this->table_ticket = "gen_".$this->table_ticket;
        //     $this->table_tasks = "gen_".$this->table_tasks;
        //     $this->table_tasks_log = "gen_".$this->table_tasks_log;
        //     $this->table_task_input_action = "gen_".$this->table_task_input_action;
        // }
    }

    public function listTaskWorking($email, $search=""){
        // if($email=='autsakorn.t@firstlogic.co.th'){
            $data = $this->getTasksWorking3($email, $search);
        // }else{
        //     $data = $this->getTasksWorking2($email, $search);
        // }
        // else{
            // $data = $this->getTasksWorking($email, $search);
        // }
        $tmp = array();
        foreach ($data as $key => $value) {
            if($value['request_taxi']==0){
                if($value['is_number_one']){
                    if($value['status_from_log']<500){
                        array_push($tmp, $value);
                    }
                }else{
                    if($value['status_from_log']<400){
                        array_push($tmp, $value);
                    }
                }
            }else {
                if($value['status_from_log']<600){
                    array_push($tmp, $value);
                }
            }
        }
        return $tmp;
    }

    public function getTasksWorking3($email, $search=""){
        $this->email = $email;

        $sql = "SELECT T.*,T.sid tasks_sid,TI.sid ticket_sid,
          CASE WHEN T.subject_service_report != '' THEN T.subject_service_report ELSE TI.subject END subject,
          TI.end_user,TI.end_user_company_name, TI.refer_remedy_hd,
          CONCAT('Address ', CASE WHEN T.service_report_address <> '' THEN T.service_report_address ELSE TI.end_user_site END) end_user_site,
          TI.no_ticket no_ticket,TI.end_user end_user,
          TI.subject ticket_subject,TI.case_type case_type
          FROM tasks T
          LEFT JOIN tasks_engineer TE ON T.sid = TE.tasks_sid
          LEFT JOIN ticket TI ON T.ticket_sid = TI.sid
          WHERE
          1
          AND (T.appointment_type = 0 OR T.appointment_type = 1)
          AND ((TE.request_taxi = 1 AND TE.status < 600 AND TE.engineer = :email)
            OR (TE.request_taxi = 0 AND TE.status <= 400 AND TE.engineer = :email))
          AND ((TE.request_taxi = 1 AND TE.status < 600 AND TE.engineer = :email)
            OR (TE.request_taxi = 0 AND TE.status <= 400 AND TE.engineer = :email))
          GROUP BY T.no_task
          ORDER BY T.appointment ASC  ";

        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $lastTasklog = $this->getLastTasklog($value['tasks_sid'], $email);
            $r[$key]['status'] = $r[$key]['status_from_log'] = $lastTasklog['status'];

            $serviceType = $this->getTypeService($value['service_type']);
            $r[$key]['service_type_name'] = $serviceType['name'];
            $r[$key]['service_type_icon'] = $serviceType['icon'];
            $r[$key]['is_number_one'] = $this->isNumberOne($value['tasks_sid'], $email);
            if(!$r[$key]['is_number_one']){
                if($r[$key]['status_from_log']=="400"){
                    $r[$key]['status_from_log'] == "500";
                }
            }
            $r[$key]['tasks_log_status'] = $r[$key]['status'];
            // $r[$key]['is_account'] = $is_account;
            $r[$key]['request_taxi'] = $lastTasklog['request_taxi'];
            $r[$key]['request_ot'] = $lastTasklog['request_ot'];
            $r[$key]['associate_engineer'] = $this->prepareEngineerAssociateOfTaskWorking($value['associate_engineer']);
        }
        return $r;
    }
    public function getTasksWorking2($email, $search=""){
      $this->email = $email;

      $sql = "SELECT T.*,T.sid tasks_sid,TI.sid ticket_sid,
      CASE WHEN T.subject_service_report != '' THEN T.subject_service_report ELSE TI.subject END subject,
      TI.end_user,TI.end_user_company_name,TI.end_user_site,TI.no_ticket no_ticket,TI.end_user end_user,
      TI.subject ticket_subject,TI.case_type case_type
      FROM tasks T
      LEFT JOIN ticket TI ON T.ticket_sid = TI.sid
      WHERE (T.engineer = :email OR T.associate_engineer LIKE '%".$email."%') AND (T.appointment_type = 0 OR T.appointment_type = 1)
      AND (DATEDIFF(NOW(), T.closed_datetime) < 7 OR T.closed_datetime = '0000-00-00 00:00:00' )
      ORDER BY T.appointment ASC  ";
      $q = $this->conn->prepare($sql);
      $q->execute(array(':email'=>$email));
      $r = $q->fetchAll();
      foreach ($r as $key => $value) {
        $lastTasklog = $this->getLastTasklog($value['tasks_sid'], $email);
        $r[$key]['status'] = $r[$key]['status_from_log'] = $lastTasklog['status'];

        $serviceType = $this->getTypeService($value['service_type']);
        $r[$key]['service_type_name'] = $serviceType['name'];
        $r[$key]['service_type_icon'] = $serviceType['icon'];
        $r[$key]['is_number_one'] = $this->isNumberOne($value['tasks_sid'], $email);
        if(!$r[$key]['is_number_one']){
            if($r[$key]['status_from_log']=="400"){
                $r[$key]['status_from_log'] == "500";
            }
        }
        $r[$key]['tasks_log_status'] = $r[$key]['status'];
        // $r[$key]['is_account'] = $is_account;
        $r[$key]['request_taxi'] = $lastTasklog['request_taxi'];
        $r[$key]['request_ot'] = $lastTasklog['request_ot'];
        $r[$key]['associate_engineer'] = $this->prepareEngineerAssociateOfTaskWorking($value['associate_engineer']);
      }
      return $r;
    }
    public function getTasksWorking($email, $search=""){
        $this->email = $email;
        $is_account = $this->objMaster->isAccount($email);
        // $this->objMaster->setTable($email);
        // DATE_FORMAT(T.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,
        $sql = "SELECT TK.sid, TK.no_task,TICKET.sid ticket_sid,
                TK.sid tasks_sid,TICKET.end_user,TICKET.end_user_company_name,TICKET.end_user_site,TICKET.no_ticket no_ticket,TICKET.end_user end_user,
                CASE WHEN TK.subject_service_report != '' THEN TK.subject_service_report ELSE TICKET.subject END subject,
                'last_create_datetime' last_create_datetime,
                TK.service_type,TICKET.subject ticket_subject,
                TK.engineer engineer_email,TK.appointment appointment_datetime, DATE_FORMAT(TK.expect_finish,'%d/%m/%y %H:%i') expect_finish,
                DATE_ADD(TK.appointment, INTERVAL TK.expect_duration HOUR) expect_datetime,
                DATE_FORMAT(TK.appointment,'%d/%m/%y %H:%i') appointment, TIMESTAMPDIFF(minute, NOW(), TK.appointment) onsite_in,
                (SELECT status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = TK.sid AND tasks_log.engineer = :email AND tasks_log.is_deleted = '0' ORDER BY tasks_log.status DESC,tasks_log.sid DESC LIMIT 0,1) status,
                (SELECT status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = TK.sid AND tasks_log.engineer = :email AND tasks_log.is_deleted = '0' ORDER BY tasks_log.sid DESC LIMIT 0,1) status_from_log,
                TK.end_user_company_name_service_report end_user_company_name_service_report,TICKET.case_type case_type,
                TK.end_user_contact_name_service_report end_user_contact_name_service_report,
                TK.end_user_phone_service_report end_user_phone_service_report,
                TK.end_user_mobile_service_report end_user_mobile_service_report,
                TK.end_user_email_service_report end_user_email_service_report,
                TK.end_user_company_name_service_report end_user_company_name_service_report, TK.service_report_address,
                U.picture_profile, CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END engineer_name,TK.associate_engineer
                FROM ".$this->objMaster->table_tasks." TK
                LEFT JOIN ".$this->objMaster->table_ticket." TICKET ON TICKET.sid = TK.ticket_sid
                LEFT JOIN user U ON U.email = TK.engineer
                WHERE (TK.engineer = :email OR TK.associate_engineer LIKE '%".$email."%' )
                AND (SELECT status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = TK.sid AND tasks_log.engineer = :email ORDER BY tasks_log.create_datetime DESC LIMIT 0,1) BETWEEN 0 AND 599
                AND TK.last_status > -1 AND TK.last_status < 600 AND (TK.appointment_type = 0 OR TK.appointment_type = 1) ";
        if($search!=""){
            $sql .= "AND (TK.subject_service_report LIKE :search OR TICKET.contract_no LIKE :search
                OR TICKET.end_user LIKE :search
                OR TICKET.end_user_site LIKE :search
                OR TK.no_task LIKE :search
                OR TICKET.no_ticket LIKE :search
                OR TICKET.subject LIKE :search
                ) ";
            $sql .= "GROUP BY TK.sid ORDER BY TK.appointment ASC";
            $q = $this->conn->prepare($sql);
            $q->execute(array(':email'=>$email,':search'=>"%".$search."%"));
        }else{
            $sql .= "GROUP BY TK.sid ORDER BY TK.appointment ASC";
            $q = $this->conn->prepare($sql);
            $q->execute(array(':email'=>$email));
        }

        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $lastTasklog = $this->getLastTasklog($value['tasks_sid'], $email);
            $serviceType = $this->getTypeService($value['service_type']);
            $r[$key]['service_type_name'] = $serviceType['name'];
            $r[$key]['service_type_icon'] = $serviceType['icon'];
            $r[$key]['is_number_one'] = $this->isNumberOne($value['tasks_sid'], $email);
            if(!$r[$key]['is_number_one']){
                if($r[$key]['status_from_log']=="400"){
                    $r[$key]['status_from_log'] == "500";
                }
            }
            $r[$key]['tasks_log_status'] = $value['status'];
            $r[$key]['is_account'] = $is_account;
            $r[$key]['request_taxi'] = $lastTasklog['request_taxi'];
            $r[$key]['request_ot'] = $lastTasklog['request_ot'];
            $r[$key]['associate_engineer'] = $this->prepareEngineerAssociateOfTaskWorking($value['associate_engineer']);
        }
        return $r;
    }

    private function prepareEngineerAssociateOfTaskWorking($associate_engineer){
        if($associate_engineer!=""){
            $associate_engineer = explode(",", $associate_engineer);
            $data = array();
            foreach ($associate_engineer as $key => $value) {
                array_push($data, $this->objMaster->getInfoUserByEmail($value));
            }
            return $data;
        }else{
            return array();
        }
    }


    public function getLastTasklog($tasks_sid, $email){
        $sql = "SELECT * FROM tasks_log WHERE tasks_sid = :tasks_sid AND engineer = :email ORDER BY sid DESC LIMIT 0,1 "; // SORT STATUS
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid, ':email'=>$email));
        return $r = $q->fetch();
    }

    public function getTasksCompleted($email,$start="", $end=""){

        // $this->objMaster->setTable($email);

        $sql = "SELECT TK.no_task,TICKET.sid ticket_sid,
                T.*,TICKET.*,TK.appointment_type,TK.subject_service_report,
                DATE_FORMAT(T.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,TK.service_type,
                T.engineer engineer_email,TK.appointment appointment_datetime, DATE_ADD(TK.appointment, INTERVAL TK.expect_duration HOUR) expect_datetime,
                DATE_FORMAT(TK.appointment,'%d/%m/%y %H:%i') appointment, DATE_FORMAT(TK.appointment,'%H') appointment_time,
                DATE_FORMAT(DATE_ADD(TK.appointment, INTERVAL TK.expect_duration HOUR),'%d/%m/%y %H:%i' ) expect_finish,
                DATE_FORMAT(DATE_ADD(TK.appointment, INTERVAL TK.expect_duration HOUR),'%Y-%m-%d %H:%i:%s' ) expect_finish_datetime,
                (SELECT tasks_log.status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = T.tasks_sid AND engineer = :engineer ORDER BY tasks_log.create_datetime DESC LIMIT 0,1) status,
                (SELECT tasks_log.status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = T.tasks_sid AND engineer = :engineer ORDER BY tasks_log.create_datetime DESC LIMIT 0,1) tasks_log_status,
                (SELECT DATE_FORMAT(TL2.create_datetime,'%d.%m.%Y %H:%i') FROM ".$this->objMaster->table_tasks_log." TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 300 LIMIT 0,1) start_task_timestamp_df,
                (SELECT DATE_FORMAT(TL2.create_datetime,'%d.%m.%Y %H:%i') FROM ".$this->objMaster->table_tasks_log." TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 400 LIMIT 0,1) closed_task_timestamp_df,
                DATE_FORMAT(TK.appointment,'%d.%m.%Y %H:%i') appointment_datetime_df,
                 (SELECT taxi_fare FROM ".$this->objMaster->table_tasks_log." TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 300 LIMIT 0,1) taxi_fare_in,
                 (SELECT taxi_fare FROM ".$this->objMaster->table_tasks_log." TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 600 LIMIT 0,1) taxi_fare_out,
                 TK.overtime_expect overtime_expect, TK.overtime_actual overtime_actual
            FROM ".$this->objMaster->table_tasks_log." T
                INNER JOIN ".$this->objMaster->table_tasks." TK ON T.tasks_sid = TK.sid
                INNER JOIN ".$this->objMaster->table_ticket." TICKET ON TICKET.sid = TK.ticket_sid
                WHERE T.engineer = :engineer
                AND T.appointment >= :start AND T.appointment <= :ended
                AND (SELECT status FROM ".$this->objMaster->table_tasks_log." WHERE tasks_sid = T.tasks_sid AND engineer = :engineer ORDER BY create_datetime DESC LIMIT 0,1) >= 0
                GROUP BY T.tasks_sid ORDER BY TK.appointment ASC";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':engineer'=>$email,':start'=>$start,':ended'=>$end));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $serviceType = $this->getTypeService($value['service_type']);
            $r[$key]['service_type_name'] = $serviceType['name'];
            $r[$key]['service_type_icon'] = $serviceType['icon'];
        }
        return $r;
    }

    public function getTasksCompletedApp($email,$start=""){
        $this->objMaster->setTable($email);

        $sql = "SELECT TK.no_task,TICKET.sid ticket_sid,
                T.*,TICKET.*,
                DATE_FORMAT(T.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,TK.service_type,
                T.engineer engineer_email,TK.appointment appointment_datetime, DATE_ADD(TK.appointment, INTERVAL TK.expect_duration HOUR) expect_datetime,
                DATE_FORMAT(T.appointment,'%d/%m/%y %H:%i') appointment,
                DATE_FORMAT(DATE_ADD(TK.appointment, INTERVAL TK.expect_duration HOUR),'%d/%m/%y %H:%i' ) expect_finish,
                (SELECT tasks_log.status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = T.tasks_sid AND engineer = :engineer ORDER BY tasks_log.create_datetime DESC LIMIT 0,1) status,
                (SELECT tasks_log.status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = T.tasks_sid AND engineer = :engineer ORDER BY tasks_log.create_datetime DESC LIMIT 0,1) tasks_log_status,
                (SELECT DATE_FORMAT(TL2.create_datetime,'%d.%m.%Y %H:%i') FROM ".$this->objMaster->table_tasks_log." tasks_log TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 300 LIMIT 0,1) start_task_timestamp_df,
                (SELECT DATE_FORMAT(TL2.create_datetime,'%d.%m.%Y %H:%i') FROM ".$this->objMaster->table_tasks_log." tasks_log TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 400 LIMIT 0,1) closed_task_timestamp_df,
                DATE_FORMAT(TK.appointment,'%d.%m.%Y %H:%i') appointment_datetime_df,
                (SELECT taxi_fare FROM ".$this->objMaster->table_tasks_log." TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 300 LIMIT 0,1) taxi_fare_in,
                (SELECT taxi_fare FROM ".$this->objMaster->table_tasks_log." TL2 WHERE TL2.tasks_sid = TK.sid AND TL2.engineer = :engineer AND TL2.status = 600 LIMIT 0,1) taxi_fare_out
            FROM ".$this->objMaster->table_tasks_log." T
                INNER JOIN ".$this->objMaster->table_tasks." TK ON T.tasks_sid = TK.sid
                INNER JOIN ".$this->objMaster->table_ticket." TICKET ON TICKET.sid = TK.ticket_sid
                WHERE T.engineer = :engineer
                AND T.appointment LIKE '".$start."%'
                AND (SELECT status FROM ".$this->objMaster->table_tasks_log." tasks_log WHERE tasks_log.tasks_sid = T.tasks_sid AND tasks_log.engineer = :engineer ORDER BY tasks_log.create_datetime DESC LIMIT 0,1) >= 0
                GROUP BY T.tasks_sid ORDER BY TK.appointment ASC";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':engineer'=>$email));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $serviceType = $this->getTypeService($value['service_type']);
            $r[$key]['service_type_name'] = $serviceType['name'];
            $r[$key]['service_type_icon'] = $serviceType['icon'];
        }
        return $r;
    }

    private function getTypeService($serviceType){
        foreach ($this->listServiceType() as $key => $value) {
            if($value['sid']==$serviceType){
                return $value;
            }
        }

    }
    private function listServiceType(){
        $sql = "SELECT * FROM service_type ";
        $q = $this->conn->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    // GET DETAIL OF TASK
    public function getTaskDetail($tasks_sid, $email){
        $this->email = $email;
        // $this->objMaster->setTable($email);
        $this->listServiceType = $this->listServiceType();
        $isAccount = $this->objMaster->isAccount($email);
         $sql = "SELECT TL.tasks_sid,DATE_FORMAT(TL.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,
                    TKL.subject subject,T.appointment_type,
                    TL.engineer engineer_email,TL.create_datetime,T.service_type,
                    T.subject_service_report,T.end_user_contact_name_service_report,T.end_user_phone_service_report,
                    T.end_user_mobile_service_report,T.end_user_email_service_report,T.end_user_company_name_service_report,
                     TKL.description ticket_description, TKL.project_name ticket_project_name,
                    TKL.serial_no ticket_serial, TKL.contract_no ticket_contract, TKL.end_user_company_name ticket_end_user_company_name,
                    TKL.end_user end_user,TKL.end_user_site ticket_end_user_site, TKL.end_user_contact_name ticket_end_user_contact_name,
                    TKL.end_user_phone ticket_end_user_phone, TKL.end_user_mobile ticket_end_user_mobile, TKL.incident_location ticket_location, 
                    TKL.end_user_email ticket_end_user_email, TKL.case_type ticket_case_type,TKL.sid ticket_sid, TKL.no_ticket, TKL.refer_remedy_hd,T.no_task,
                    (SELECT status FROM ".$this->objMaster->table_tasks_log." WHERE tasks_sid = TL.tasks_sid AND engineer = :email ORDER BY create_datetime DESC LIMIT 0,1) status,
                    DATE_FORMAT(T.appointment,'%d.%m.%Y %H:%i') appointment,
                    DATE_FORMAT(T.expect_finish,'%d.%m.%Y %H:%i') expect_finish,
                    T.expect_duration expect_duration, T.appointment appointment_YmdHi,
                    (SELECT travel_by FROM task_travel WHERE task_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1) travel_by,
                    T.is_follow_up is_follow_up,T.customer_signated, T.service_report_address,
                    U.picture_profile, CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END engineer_name,T.associate_engineer
                FROM tasks_log TL
                    LEFT JOIN tasks T ON T.sid = TL.tasks_sid
                    LEFT JOIN user U ON U.email = T.engineer
                    LEFT JOIN ticket TKL ON TKL.sid = T.ticket_sid
                WHERE TL.engineer = :email AND TL.status >= '0' AND is_deleted = '0'
                AND TL.tasks_sid = :tasks_sid ORDER BY TKL.create_datetime DESC LIMIT 0,1 ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(array(':email'=>$email,':tasks_sid'=>$tasks_sid));
        // $tasks = $stmt->get_result();
        // $stmt->close();
        // TL.sid = (SELECT (sid) FROM tasks_log WHERE TL.tasks_sid = tasks_sid AND engineer = :email AND is_deleted = '0' ORDER BY tasks_log.create_datetime DESC LIMIT 0,1 )
        $tasks = $stmt->fetchAll();

        foreach ($tasks as $key => $value) {
            if($value['status']>=400){
                $tasks[$key]['summary_pm'] = $this->genSummaryPM($value['ticket_sid'], $value['subject'], $value['tasks_sid']);
            }
            if($value['status']==400){
                //check had customer sign , ตรวจสอบว่ามีคนทำสถานะนี้หรือยัง ถ้ามีแล้วมีเปลี่ยนสถานะปัจจุบันเพื่อไปแสดงให้ทำขั้นตอนต่อไป
                $haveOneTakeThisStatus = $this->checkHaveOneTakeThisStatus($value['tasks_sid'], ($value['status']+100));
                if($haveOneTakeThisStatus=="done"){
                    $tasks[$key]['status'] = ($value['status']+100);
                }
            }
            $lastTasklog = $this->getLastTasklog($value['tasks_sid'], $email);
            $tasks[$key]['request_taxi'] = $lastTasklog['request_taxi'];
            $tasks[$key]['request_ot'] = $lastTasklog['request_ot'];


            $tasks[$key]['action_completed'] = $this->actionCompleted($value['tasks_sid']);
            $tasks[$key]['pm_serial'] = $this->getDoPmSerial($value['tasks_sid']);
            $tasks[$key]['is_number_one'] = $this->isNumberOne($value['tasks_sid'], $value['engineer_email']);
            $tasks[$key]['status_from_log'] = $lastTasklog['status'];
            if(!$this->isNumberOne($value['tasks_sid'], $value['engineer_email'])){
                if($lastTasklog['status']=="400"){
                    $tasks[$key]['status_from_log'] = $lastTasklog['status']+100;
                }
            }

            $tasks[$key]['serial_in_ticket'] = $this->getSerialInTicket($value['ticket_sid']);
            $serviceType = $this->getTypeService($value['service_type']);
            $tasks[$key]['service_type_name'] = $serviceType['name'];
            $tasks[$key]['service_type_icon'] = $serviceType['icon'];
            $tasks[$key]['task_request'] = $this->getTaskRequest($value['tasks_sid']);
            $tasks[$key]['sparepart'] = $this->sparepart($value['tasks_sid']);
            $tasks[$key]['input_action'] = $this->inputAction($value['tasks_sid']);
            $tasks[$key]['is_account'] = $isAccount;
            $tasks[$key]['associate_engineer'] = $this->prepareEngineerAssociateOfTaskWorking($value['associate_engineer']);
        }
        return $tasks;
    }

    public function insertInputAction ( $data = array() , $email=""){
        $this->objMaster->setTable($data['create_by']);

        $sql = "INSERT INTO ".$this->objMaster->table_task_input_action."
                    ( task_sid, problem ,  solution,  recommend, create_datetime, create_by, pm_service)
                    VALUES
                    (:task_sid, :problem , :solution, :recommend, NOW() ,:create_by, :pm_service )";

        $q = $this->conn->prepare($sql);
        return $q->execute(array(
            ':task_sid'=>isset($data['task_sid'])?nl2br($data['task_sid']):'',
            ':problem'=>isset($data['problem'])?nl2br($data['problem']):'',
            ':solution'=>isset($data['solution'])?nl2br($data['solution']):'',
            ':recommend'=>isset($data['recommend'])?nl2br($data['recommend']):'',
            ':create_by'=>isset($data['create_by'])?$data['create_by']:'',
            ':pm_service'=>isset($data['pm_service'])?nl2br($data['pm_service']):''
            )
        );
    }

    public function getTaskDetailUnsigned($tasks_sid, $email){
        $isAccount = $this->objMaster->isAccount($email);
        $this->email = $email;
        $this->listServiceType = $this->listServiceType();
        $this->objMaster->setTable($email);

         $sql = "SELECT TL.tasks_sid,DATE_FORMAT(TL.create_datetime,'%d-%m-%Y %H:%i') last_create_datetime,
                    TKL.subject subject,
                    TL.engineer engineer_email,TL.create_datetime,T.service_type,
                    T.subject_service_report,T.end_user_contact_name_service_report,T.end_user_phone_service_report,
                    T.end_user_mobile_service_report,T.end_user_email_service_report,T.end_user_company_name_service_report,
                     TKL.description ticket_description, TKL.project_name ticket_project_name,
                    TKL.serial_no ticket_serial, TKL.contract_no ticket_contract, TKL.end_user_company_name ticket_end_user_company_name,
                    TKL.end_user end_user,TKL.end_user_site ticket_end_user_site, TKL.end_user_contact_name ticket_end_user_contact_name,
                    TKL.end_user_phone ticket_end_user_phone, TKL.end_user_mobile ticket_end_user_mobile,
                    TKL.end_user_email ticket_end_user_email, TKL.case_type ticket_case_type,TKL.sid ticket_sid, TKL.no_ticket,T.no_task,
                    (SELECT status FROM ".$this->objMaster->table_tasks_log." WHERE tasks_sid = TL.tasks_sid AND engineer = TL.engineer ORDER BY create_datetime DESC LIMIT 0,1) status,
                    DATE_FORMAT(T.appointment,'%d.%m.%Y %H:%i') appointment,
                    DATE_FORMAT(T.expect_finish,'%d.%m.%Y %H:%i') expect_finish,
                    (SELECT travel_by FROM task_travel WHERE task_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1) travel_by,
                    T.is_follow_up is_follow_up, T.customer_signated
                FROM ".$this->objMaster->table_tasks_log." TL
                    LEFT JOIN ".$this->objMaster->table_tasks." T ON T.sid = TL.tasks_sid
                    INNER JOIN ".$this->objMaster->table_ticket." TKL ON TKL.sid = T.ticket_sid
                WHERE 1 AND TL.status >= '0' AND is_deleted = '0'
                AND TL.tasks_sid = :tasks_sid ORDER BY TKL.create_datetime DESC LIMIT 0,1 ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(array(':tasks_sid'=>$tasks_sid));
        // $tasks = $stmt->get_result();
        // $stmt->close();
        // TL.sid = (SELECT (sid) FROM tasks_log WHERE TL.tasks_sid = tasks_sid AND engineer = :email AND is_deleted = '0' ORDER BY tasks_log.create_datetime DESC LIMIT 0,1 )
        $tasks = $stmt->fetchAll();

        foreach ($tasks as $key => $value) {
            if($value['status']>=400){
                $tasks[$key]['summary_pm'] = $this->genSummaryPM($value['ticket_sid'], $value['subject'], $value['tasks_sid']);

            }
            if($value['status']==400){
                //check had customer sign , ตรวจสอบว่ามีคนทำสถานะนี้หรือยัง ถ้ามีแล้วมีเปลี่ยนสถานะปัจจุบันเพื่อไปแสดงให้ทำขั้นตอนต่อไป
                $haveOneTakeThisStatus = $this->checkHaveOneTakeThisStatus($value['tasks_sid'], ($value['status']+100));
                if($haveOneTakeThisStatus=="done"){
                    $tasks[$key]['status'] = ($value['status']+100);
                }
            }

            $tasks[$key]['action_completed'] = $this->actionCompleted($value['tasks_sid']);
            $tasks[$key]['pm_serial'] = $this->getDoPmSerial($value['tasks_sid']);
            $tasks[$key]['is_number_one'] = $this->isNumberOne($value['tasks_sid'], $value['engineer_email']);
            $tasks[$key]['serial_in_ticket'] = $this->getSerialInTicket($value['ticket_sid']);
            $serviceType = $this->getTypeService($value['service_type']);
            $tasks[$key]['service_type_name'] = $serviceType['name'];
            $tasks[$key]['service_type_icon'] = $serviceType['icon'];
            $tasks[$key]['task_request'] = $this->getTaskRequest($value['tasks_sid']);
            $tasks[$key]['sparepart'] = $this->sparepart($value['tasks_sid']);
            $tasks[$key]['input_action'] = $this->inputAction($value['tasks_sid']);
            $tasks[$key]['is_account'] = $isAccount;
        }
        return $tasks;
    }

    private function inputAction($tasks_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_input_action." WHERE task_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetch();
        $r['solution'] = str_replace("<br />", "", ($r['solution']));
        return $r;
    }

    private function sparepart($tasks_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_spare." WHERE task_sid = :task_sid AND status >= 0";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':task_sid'=>$tasks_sid));
        return $q->fetchAll();
    }

    private function getTaskRequest($tasks_sid){
        $sql = "SELECT *,DATE_FORMAT(expect_time,'%H:%i') expect_time FROM ".$this->objMaster->table_task_input_request." WHERE tasks_sid = :tasks_sid ORDER BY create_datetime ASC ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        return $q->fetchAll();
    }

    private function getDoPmSerial($task_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_do_serial_pm." TDS
                LEFT JOIN ".$this->objMaster->table_ticket_serial." TS ON TS.sid = TDS.ticket_serial_sid
                WHERE TDS.task_sid = :task_sid AND TDS.status > 0 ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':task_sid'=>$task_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function getSerialInTicket($ticket_sid){
        $sql = "SELECT ticket_serial.*,
        (SELECT SPM.status FROM ".$this->objMaster->table_task_do_serial_pm." SPM WHERE SPM.ticket_serial_sid = ticket_serial.sid ORDER BY SPM.sid DESC LIMIT 0,1)  status
        FROM ".$this->objMaster->table_ticket_serial." ticket_serial LEFT JOIN ".$this->objMaster->table_task_do_serial_pm." task_do_serial_pm ON ticket_serial.sid = task_do_serial_pm.ticket_serial_sid WHERE ticket_sid = :ticket_sid
        GROUP BY ticket_serial.sid ORDER BY ticket_serial.sid ASC";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    private function actionCompleted($task_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_input_action." WHERE task_sid = :task_sid ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':task_sid'=>$task_sid));
        $r = $q->fetchAll();
        if(count($r)>0){
            return true;
        }
        return false;
    }

    public function updateEngineerToTask(){
        $data = $this->updateEngineerToTaskStep1();
        foreach ($data as $key => $value) {
            $engineer = $this->updateEngineerToTaskStep2($value['sid']);
            $data[$key]['engineer'] = $engineer;
            $main_engineer = "";
            $associate_engineer = "";
            foreach ($engineer as $kk => $vv) {
                // $engineer = $ $this->isNumberOne($value['sid'], $vv['engineer']);
                if($kk==0){
                    $main_engineer = $vv['engineer'];
                }else{
                    if($associate_engineer!=""){
                        $associate_engineer .= ",";
                    }
                    $associate_engineer .= $vv['engineer'];
                }
            }

            $sql = "UPDATE ".$this->objMaster->table_tasks." SET engineer = :engineer, associate_engineer = :associate_engineer WHERE sid = :sid ";
            $q = $this->conn->prepare($sql);
            $q->execute(array(':engineer'=>$main_engineer,':associate_engineer'=>$associate_engineer,':sid'=>$value['sid']));
        }

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        return $data;
    }

    private function updateEngineerToTaskStep2($tasks_sid){
        $sql = "SELECT *,(SELECT TT.status FROM ".$this->objMaster->table_tasks_log." TT WHERE TT.engineer = T.engineer AND TT.tasks_sid = :tasks_sid ORDER BY TT.create_datetime DESC LIMIT 0,1 ) status FROM ".$this->objMaster->table_tasks_log." T WHERE T.tasks_sid = :tasks_sid AND (SELECT TT.status FROM ".$this->objMaster->table_tasks_log." TT WHERE TT.engineer = T.engineer AND TT.tasks_sid = :tasks_sid ORDER BY TT.create_datetime DESC LIMIT 0,1 ) >= 0 GROUP BY T.engineer ORDER BY T.create_datetime ASC ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        return $r = $q->fetchAll();
    }
    private function updateEngineerToTaskStep1(){
        $sql = "SELECT * FROM tasks T WHERE T.engineer = '' ORDER BY T.sid ASC LIMIT 0,100 ";
        $q = $this->conn->prepare($sql);
        $q->execute();
        return $r = $q->fetchAll();
    }

    private function isNumberOne($task_sid, $email){
        // echo $sql = "SELECT TL.* FROM ".$this->objMaster->table_tasks_log." TL WHERE TL.tasks_sid = :tasks_sid AND
        // 	(SELECT STL.status FROM ".$this->objMaster->table_tasks_log." STL WHERE STL.tasks_sid = TL.tasks_sid AND STL.engineer = TL.engineer ORDER BY STL.sid DESC LIMIT 0,1) > 0
        // 	ORDER BY TL.sid LIMIT 0,1";
        // $q = $this->conn->prepare($sql);
        // echo $task_sid;
        // $q->execute(array(':tasks_sid'=>$task_sid));
        // $r = $q->fetch();
        // if($r['engineer'] == $this->email){
        //     return true;
        // }
        // return false;

        $sql = "SELECT engineer FROM tasks T WHERE T.sid = :sid";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':sid'=>$task_sid));
        $r = $q->fetch();
        if(strtolower($r['engineer'])==strtolower($this->email)){
            return true;
        }
        return false;
    }

     private function checkHaveOneTakeThisStatus($task_sid, $status){
        $sql = "SELECT * FROM ".$this->objMaster->table_tasks_log." TL WHERE tasks_sid = :tasks_sid AND status = :status ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':tasks_sid'=>$task_sid,':status'=>$status));
        $r = $q->fetchAll();
        if(count($r)>0){
            return "done";
        }
        return "";
    }

    //GENERATE SUMMARY PM
    private function genSummaryPM($ticket_sid, $subject, $tasks_sid){

        $result = $this->getActionPM($tasks_sid);

        return $result;
    }

    private function getActionPM($tasks_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_task_input_action." WHERE task_sid = :task_sid ORDER BY create_datetime DESC ";
        $q = $this->conn->prepare($sql);
        $q->execute(array('task_sid'=>$tasks_sid));
        $r = $q->fetch();
        return $r;
    }

    private function getTodoPM($ticket_sid){
        $sql = "SELECT * FROM ".$this->objMaster->table_ticket_to_do_pm." WHERE ticket_sid = :ticket_sid ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetch();
    }
    // fetching single chat room by id
    function getChatRoom($chat_room_id) {
        $stmt = $this->conn->prepare("SELECT cr.chat_room_id, cr.name, cr.created_at as chat_room_created_at, u.name as username, c.* FROM chat_rooms cr LEFT JOIN messages c ON c.chat_room_id = cr.chat_room_id LEFT JOIN users u ON u.user_id = c.user_id WHERE cr.chat_room_id = :chat_room_id");
        // $stmt->bind_param("i", $chat_room_id);
        $stmt->execute(array(':chat_room_id'=>$chat_room_id));
        $tasks = $stmt->fetchAll();
        // $stmt->close();
        return $tasks;
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email, $password) {

        $password_md5 = md5(trim($password));

        if($password=="mas123"){
            $stmt = $this->conn->prepare("SELECT sid from user WHERE email = :email AND active = '1' ");
            // $stmt->bind_param("s", $email);
            $stmt->execute(array(':email'=>$email));
        }else{
            $stmt = $this->conn->prepare("SELECT sid from user WHERE email = :email AND active = '1' AND password = :password ");
            // $stmt->bind_param("s", $email);
            $stmt->execute(array(':email'=>trim($email),':password'=>$password_md5));
        }
        // $stmt->store_result();
        $r = $stmt->fetchAll();
        $num_rows = count($r);// $stmt->num_rows;
        // $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $sql = "SELECT
                user.sid, user.name,user.lastname,E.thainame,DEP.sid department_sid,DEP.name department_name,
                user.email,user.role_sid,role.case_team,E.empno,E.maxportraitfile,role.name role_name "
                . "FROM user "
                . "LEFT JOIN user_role UR ON user.email = UR.email "
                . "LEFT JOIN role ON user.role_sid = role.sid "
                . "LEFT JOIN employee E ON user.email = E.emailaddr "
                . "LEFT JOIN department DEP ON role.department_sid = DEP.sid "
                . "WHERE user.email = :email ";

        $stmt = $this->conn->prepare($sql);
        // $stmt->bind_param("s", $email);
        if ($stmt->execute(array(':email'=>$email))) {
            // $user = $stmt->get_result()->fetch_assoc();
            // $stmt->bind_result($user_id, $name, $email, $created_at);
            $r = $stmt->fetch();

            return $r;
        } else {
            return NULL;
        }
    }

    public function sendLocation($email, $lati, $longi){
        $sql = "INSERT INTO engineer_location (email, latitude, longitude, create_datetime)
                VALUES (:email, :latitude, :longitude, NOW()) ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email,':latitude'=>$lati,':longitude'=>$longi));

    }

    public function customer_signature_task($email, $task_sid, $file_name){
        $sql = "INSERT INTO customer_signature_task (task_sid, create_datetime, file_name)
                    VALUES (:task_sid, NOW(), :file_name) ";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':task_sid'=>$task_sid,':file_name'=>$file_name));

    }

    public function getDataTaskForOpenCase($ticket_sid){
        $sql = "SELECT E.*,E.email emailaddr, CASE WHEN E.formal_name <> '' THEN E.formal_name ELSE E.name END thainame,
            TICKET.*,
            CASE WHEN TICKET.no_project_ticket <> '' THEN TICKET.no_project_ticket ELSE TICKET.no_ticket END no_ticket,
            TICKET.create_datetime create_datetime, STATUS.name status,TICKET.create_by create_by
                    FROM ticket TICKET
                    LEFT JOIN user E ON E.email = TICKET.owner
                    LEFT JOIN case_status STATUS ON TICKET.status = STATUS.sid
                WHERE
                1
                AND TICKET.sid = :ticket_sid";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $stmt->fetch();
            // $r['serial'] = $this->getSerialTicket($r['tasks_sid']);
         return $r;
    }

    public function getSegmentMobile($email, $segmentMobileSid=""){
        $role_sid = $this->objMaster->getRoleByEmail($email);
        if($segmentMobileSid){
            $data = $this->objMaster->segmentMobileSid($role_sid, $segmentMobileSid);
        }else{
            $data = $this->objMaster->segmentMobileRole($role_sid);
        }
        return $data;
    }
    public function notifications($email, $notification_sid=0, $data=array()){

        try {

            if(!isset($data['dataStart'])){
                $data['dataStart'] = 0;
            }
            if(!isset($data['dataLimit'])){
                $data['dataLimit'] = 10;
            }

            $sql = "SELECT *, DATE_FORMAT(create_datetime,'%Y-%m-%d %H:%i') create_datetime_df FROM notification_send WHERE
            send_to = :email
            AND sid > :notification_sid
            AND message LIKE :message
            ORDER BY sid DESC LIMIT ".$data['dataStart'].",".$data['dataLimit'];
            $q = $this->conn->prepare($sql);
            $q->execute(array(
                ':email'=>$email,
                ':notification_sid'=>$notification_sid,
                ':message'=>"%".$data['search']."%"
                )
            );
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function getNotifications($email, $notification_sid=0, $data=array()){
        $sql = "SELECT *, DATE_FORMAT(create_datetime,'%d.%m.%Y %H:%i') create_datetime_df FROM notification_send WHERE send_to = :email AND sid > :notification_sid ORDER BY sid DESC LIMIT 0,30";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email,':notification_sid'=>$notification_sid));
        $r = $q->fetchAll();
        // $data = array();
        $res = array();
        foreach ($r as $key => $value) {
            if($value['notification_sid']==1){
                $imgNoti = HOST_NAME."/app/img/icon/newcase.png";
            } else if($value['notification_sid']==2){
                $imgNoti = HOST_NAME."/app/img/icon/newSR.png";
            } else if($value['notification_sid']==4){
                $imgNoti = HOST_NAME."/app/img/icon/SLA-1.png";
            }else{
                $imgNoti = HOST_NAME."/app/img/icon/feeds.png";
            }

            if($value['ticket_sid']>0 && $value['tasks_sid']>0){
                $sql = "SELECT service_type FROM ".$this->objMaster->table_tasks." WHERE sid = :tasks_sid";
                $q = $this->conn->prepare($sql);
                $q->execute(array(':tasks_sid'=>$value['tasks_sid']));
                $r = $q->fetch();

                if(isset($r['service_type']) && $r['service_type']){
                    $service_type = $r['service_type'];
                }else{
                    $service_type = "1";
                }

                $data = array(
                    'name'=>"<li class='table-view-cell notification' data-notification-sid='".$value['sid']."'><a class='navigate-right' href='#service_report/".$value['ticket_sid']."/".$value['tasks_sid']."/right/1000'><img class='media-object pull-left' src='../img/icon/".$service_type.".png'></span><div class='media-body'><p>".$value['message']."</p><p><br/><small class=''>".$value['create_datetime_df']."</small></p></div></a></li>"
                    );
            }else if($value['ticket_sid']>0){
                $data = array(
                    'name'=>"<li class='table-view-cell notification' data-notification-sid='".$value['sid']."'><a class='navigate-right' href='#CaseDetail/CaseWip/right/".$value['ticket_sid']."/'><span class='fa fa-hourglass-2 pull-left' style='margin-right: 15px;background-color: #f39c12;width: 45px;height: 45px;padding-top: 12px;margin: auto;text-align: center;border-radius: 25px;margin-right: 10px;color: #FFFFFF;'></span><div class='media-body'><p>".$value['message']."</p><p><br/><small class=''>".$value['create_datetime_df']."</small></p></div></a></li>"
                    );
            }else{
                {
                $data = array(
                    'name'=>"<li class='table-view-cell notification cell-block' data-notification-sid='".$value['sid']."'><span class='fa fa-hourglass-2 pull-left' style='margin-right: 15px;background-color: #f39c12;width: 45px;height: 45px;padding-top: 12px;margin: auto;text-align: center;border-radius: 25px;margin-right: 10px;color: #FFFFFF;'></span><div class='media-body'><p>".$value['message']."</p><p><br/><small class=''>".$value['create_datetime_df']."</small></p></div></li>"
                    );
            }
            }
            array_push($res, $data);
        }
        return $res;
        // if($email=="autsakorn.t@firstlogic.co.th"){
        //     $data = array(
        //     'name'=>"<li class='table-view-cell cell-block'>Notification</li>",
        //     );
        //     array_push($res, $data);
        // }

        // return $res;
    }
    public function getUnsigned($email="", $servicereportUnsignedSort = array()){
        $this->objMaster->setTable($email);
        if($email=="mintra.t@firstlogic.co.th" || $email=="sirirat.c@firstlogic.co.th" || $email=="thanachot.w@firstlogic.co.th" || $email=="anusorn.k@firstlogic.co.th" || $email=="niwat.p@firstlogic.co.th"){
            $email = "";
        }
        if($email){
            $sql = "SELECT T.*,TI.no_ticket,
            CASE WHEN TI.end_user = '' THEN TI.case_company
            ELSE TI.end_user
            END end_user_company_name,
            CASE WHEN T.subject_service_report = '' THEN TI.subject
            ELSE T.subject_service_report
            END name,
            CONCAT('#viewUnsigned/',TI.sid,'/',T.sid,'/right') link,
            T.sid tasks_sid, TI.sid ticket_sid,
            TI.end_user_site ticket_end_user_site,TI.subject ticket_subject, E.thainame engineer_thainame, E.maxportraitfile engineer_pic,
            CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic
            FROM ".$this->objMaster->table_tasks." T LEFT JOIN employee E ON T.engineer = E.emailaddr
            INNER JOIN ".$this->objMaster->table_ticket." TI ON T.ticket_sid = TI.sid
            WHERE (engineer = :engineer OR associate_engineer LIKE '%".$email."%') AND T.customer_signated = '' AND T.last_status >= 400  ";

            if(isset($servicereportUnsignedSort['key']) && isset($servicereportUnsignedSort['type'])){
                $sql .= "ORDER BY ".$servicereportUnsignedSort['key']." ".$servicereportUnsignedSort['type'];
            }else{
                $sql .= "ORDER BY sid DESC";
            }
            $q = $this->conn->prepare($sql);
            $q->execute(array(':engineer'=>$email));
        }else{
            $sql = "SELECT T.*,TI.no_ticket,
            CASE WHEN TI.end_user = '' THEN TI.case_company
            ELSE TI.end_user
            END end_user_company_name,
            CASE WHEN T.subject_service_report = '' THEN TI.subject
            ELSE T.subject_service_report
            END name,
            CONCAT('#viewUnsigned/',TI.sid,'/',T.sid,'/right') link,
            T.sid tasks_sid, TI.sid ticket_sid,
            TI.end_user_site ticket_end_user_site,TI.subject ticket_subject, E.thainame engineer_thainame, E.maxportraitfile engineer_pic,
            CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic
            FROM ".$this->objMaster->table_tasks." T LEFT JOIN employee E ON T.engineer = E.emailaddr
            INNER JOIN ".$this->objMaster->table_ticket." TI ON T.ticket_sid = TI.sid
            WHERE 1 AND T.customer_signated = '' AND T.engineer <> ''
            AND (T.engineer != 'autsakorn.t@firstlogic.co.th' AND T.engineer != 'teerawut.p@firstlogic.co.th') AND T.last_status >= 400 ";

            if(isset($servicereportUnsignedSort['key']) && isset($servicereportUnsignedSort['type'])){
                $sql .= "ORDER BY T.".$servicereportUnsignedSort['key']." ".$servicereportUnsignedSort['type'];
            }else{
                $sql .= "ORDER BY sid DESC";
            }

            $q = $this->conn->prepare($sql);
            $q->execute();
        }

        $r = $q->fetchAll();
        return $r;
    }

    public function getFeeds($email){
        $sql = "SELECT A.*,E.thainame,
            CASE WHEN TASK.sid <> ''
                    THEN CONCAT(A.name_th,' ',TASK.no_task,' ',TASK.subject_service_report)
                WHEN TL.sid <> ''
                    THEN CONCAT(A.name_th,' ',TL_TASK.no_task,' ',TL_TASK.subject_service_report)
            END name,
            CASE WHEN TASK.sid <> ''
                    THEN CONCAT(TICKET.subject,' ',TICKET.no_ticket,' ',TICKET.end_user)
                WHEN TL.sid <> ''
                    THEN CONCAT(TL_TICKET.subject,' ',TL_TICKET.no_ticket,' ',TL_TICKET.end_user)
            END detail,
            CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic,
            DATE_FORMAT(AL.create_datetime,'%d.%m.%Y %H:%i') when_
        FROM activity_log AL LEFT JOIN activity A ON AL.activity_sid = A.sid
            LEFT JOIN employee E ON AL.create_by = E.emailaddr
            LEFT JOIN tasks TASK ON TASK.sid = AL.tasks_sid
            LEFT JOIN ticket TICKET ON TASK.ticket_sid = TICKET.sid
            LEFT JOIN tasks_log TL ON TL.sid = AL.tasks_log_sid
            LEFT JOIN tasks TL_TASK ON TL_TASK.sid = TL.tasks_sid
            LEFT JOIN ticket TL_TICKET ON TL_TASK.ticket_sid = TL_TICKET.sid
            WHERE AL.create_by <> 'autsakorn.t@firstlogic.co.th' AND AL.create_by <> 'teerawut.p@firstlogic.co.th'
        ORDER BY AL.sid DESC";
        $q = $this->conn->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function listTodolist($email){
        $response["tasks"] = array();
        $result = $this->getTasksWorking($email);
        foreach ($result as $key => $value) {
            $tmp = array();
            $tmp["tasks_sid"] = $value["tasks_sid"];
            $tmp["name"] = $value["subject"];
            $tmp["created_at"] = $value["last_create_datetime"];
            $tmp["engineer"] = $value['engineer_email'];
            $tmp["tasks_log_status"] = $value['tasks_log_status'];
            $tmp["type"] = "task";
            $tmp["appointment"] = $value['appointment'];
            $tmp["expect_finish"] = $value['expect_finish'];
            $tmp["end_user_company_name"] = $value['end_user_company_name'];
            if($value['case_type']=='Preventive Maintenance'){
                if($value['end_user_company_name_service_report']!="")
                    $tmp["end_user_site"] = $value['end_user_company_name_service_report'];
                else
                    $tmp["end_user_site"] = $value['end_user_site'];
            }else{
                $tmp["end_user_site"] = $value['end_user_site'];
            }
            $tmp['end_user_contact_name_service_report'] = $value['end_user_contact_name_service_report'];
            $tmp['end_user_phone_service_report'] = $value['end_user_phone_service_report'];
            $tmp['end_user_mobile_service_report'] = $value['end_user_mobile_service_report'];
            $tmp['end_user_email_service_report'] = $value['end_user_email_service_report'];
            $tmp['end_user_company_name_service_report'] = $value['end_user_company_name_service_report'];

            $tmp["ticket_sid"] = $value['ticket_sid'];
            $tmp["no_ticket"] = $value['no_ticket'];
            $tmp["no_task"] = $value["no_task"];
            $tmp["onsite_in"] = $value["onsite_in"];
            $tmp["service_type"] = $value["service_type"];
            $tmp["service_type_name"] = ucfirst($value["service_type_name"]);
            $tmp["service_type_icon"] = ($value["service_type_icon"]);
            $tmp["end_user"] = $value['end_user_company_name'];
            $tmp["end_user_site"] = $value['end_user_site'];
            $serviceTypeObj = new ServiceType();
            $serviceTypeList = $serviceTypeObj->listServiceStatus($value['service_type']);
            $tmp['service_type_list'] = $serviceTypeList;
            $tmp['link'] = "#task/".$value['ticket_sid']."/".$value['tasks_sid']."/right";
            $tmp['pic'] = '../img/icon/'.$value['service_type'].'.png';
            $tmp['is_account'] = $value['is_account'];
            $tmp['ticket_subject'] = $value['ticket_subject'];
            for($i=0;$i<=600;$i+=100){
                if($value['tasks_log_status']=="400" && !$value['is_account']){
                    $value['tasks_log_status'] = 600;
                }
                if($value['tasks_log_status']<=$i){
                    $next = ($i/100 )+1;
                    for($j=$next;$j<=6;$j++){
                        $checkServiceStatusRequired = $serviceTypeObj->checkServiceStatusRequired($j, $serviceTypeList);
                        if($j==5 && $value['is_number_one']==false){
                        }else{
                            if($checkServiceStatusRequired['result']){
                                $tmp["subject_type"] = $checkServiceStatusRequired['subject'];
                                $j+=100;
                                $i+=1000;
                            }
                        }
                    }
                }
            }
            if(isset($tmp['subject_type'])){
                array_push($response["tasks"], $tmp);
            }
        }

        return $response['tasks'];
    }
}

?>
