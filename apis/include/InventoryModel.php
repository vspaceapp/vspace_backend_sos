<?php
class InventoryModel{
    private $db;
    private $objCons;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function index(){
        try{
            $sql = "SELECT INV.*,C.name category_name, T.name type_name,  I.name item_name, CC.contact_name owner_name  
            FROM inventory INV 
            LEFT JOIN category C ON INV.category = C.sid 
            LEFT JOIN type T ON INV.type = T.sid 
            LEFT JOIN item I ON INV.item = I.sid 
            LEFT JOIN contact CC ON INV.owner_sid = CC.sid 
            ORDER BY create_datetime DESC";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function category(){
        try{
            $sql = "SELECT * FROM category ORDER BY name";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function addInventory($data){
        try{
            $sql = "INSERT INTO inventory 
            (address, branch_name, brand, category, 
            create_by, item, model, pn, 
            province, quantity, serial_no, 
            store_name, type, zone, create_datetime,owner_sid
            ) 
            VALUES 
            (:address, :branch, :brand, :category, 
            :create_by, :item, :model, :pn, 
            :province, :quantity, :serial_no, 
            :store_name, :type, :zone, NOW(), :owner_sid) ";
            
            $q = $this->db->prepare($sql);
            return $q->execute(array(
                ':address'=>$data['address'],
                ':branch'=>$data['branch'],
                ':brand'=>$data['brand'],
                ':category'=>$data['category'], 
                ':create_by'=>$data['email'],
                ':item'=>$data['item'],
                ':model'=>$data['model'],
                ':pn'=>$data['pn'], 
                ':province'=>$data['province'],
                ':quantity'=>$data['quantity'],
                ':serial_no'=>$data['serialNumber'], 
                ':store_name'=>$data['storeName'],
                ':type'=>$data['type'],
                ':zone'=>$data['zone'],
                ':owner_sid'=>$data['owner']
            ));
            
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function cti(){
        return array(
            'category'=>$this->categorys(),
            'type'=>$this->types(),
            'item'=>$this->items(),
            'contact'=>$this->contact()
        );
    }
    private function contact(){
        try{
            $sql = "SELECT * FROM contact WHERE email LIKE '%kasikorn%' GROUP BY email ORDER BY email ASC";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    private function categorys(){
        try{
            $sql = "SELECT * FROM category ORDER BY sid ";
            $q = $this->db->prepare($sql);
            $q->execute();
            return $q->fetchAll();
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    private function types(){
        try{
            $sql = "SELECT * FROM type ORDER BY name ";
            $q = $this->db->prepare($sql);
            $q->execute();
            return $q->fetchAll();
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    private function items(){
        try{
            $sql = "SELECT * FROM item ORDER BY name ";
            $q = $this->db->prepare($sql);
            $q->execute();
            return $q->fetchAll();
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function ciInformation($data){
        try{
            $ticketInfo = $this->ticketInfo($data['ticket_sid']);
            $data = $this->ciInformationQuery($ticketInfo['end_user_email']);
            return $data;
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    
    public function ticketInfo($ticket_sid){
        try{
            $sql = "SELECT * FROM ticket WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$ticket_sid));
            $r = $q->fetch();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    private function ciInformationQuery($email){
        try{
            $sql = "SELECT INV.*,C.name category_name, T.name type_name,  I.name item_name, CC.contact_name owner_name  
            FROM inventory INV 
            LEFT JOIN category C ON INV.category = C.sid 
            LEFT JOIN type T ON INV.type = T.sid 
            LEFT JOIN item I ON INV.item = I.sid 
            LEFT JOIN contact CC ON INV.owner_sid = CC.sid 
            WHERE CC.email = :email 
            ORDER BY create_datetime DESC";
            $q = $this->db->prepare($sql);
            $q->execute(array(':email'=>$email));
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function findItemLikeThis($data){
         try{
            $sql = "SELECT INV.*,C.name category_name, T.name type_name,  I.name item_name, CC.contact_name owner_name  
            FROM inventory INV 
            LEFT JOIN category C ON INV.category = C.sid 
            LEFT JOIN type T ON INV.type = T.sid 
            LEFT JOIN item I ON INV.item = I.sid 
            LEFT JOIN contact CC ON INV.owner_sid = CC.sid 
            WHERE T.sid = :type_sid AND INV.sid <> :inventory_sid 
            ORDER BY create_datetime DESC";
            $q = $this->db->prepare($sql);
            $q->execute(array(':type_sid'=>$data['type'],':inventory_sid'=>$data['inventory_sid']));
            $r = $q->fetchAll();
            return $r;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}
?>