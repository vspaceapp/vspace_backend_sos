<?php
class WSCdgModel{

	private $db;
   

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->objCons = $db = new DbConnect();
        $this->db = $db->connect();
    }


    function selectFromWS( $res){

        // echo "<pre>";
        // print_r($res['Result']);
        // echo "</pre>";
        $resultWS = array();
        $resultVSpace = array();
        // echo count($res['Result']);

        exit();
        // for loop check status employee resign 
        foreach ($res['Result'] as $key => $value) {
                if( $value['EmpType'] !== 'T'  && $value['TerminType'] !== 'R' 
                    && $value['TerminType'] !== 'L'  && $value['TerminType'] !== 'E'  
                    && $value['TerminType'] !== 'C'  && $value['TerminType'] !== 'D'  
                    && $value['TerminType'] !== 'P' && $value['TerminType'] !== 'T' ){
                    // count($value);
                    array_push($resultWS, $value);
                    $result = $this->selectEmployeeByGroupIDandEmpno( '', $value['EmpNo']);
                    array_push($resultVSpace, $result);

                    if(isset($result['empno']) && $result['empno']!=""){
                        $this->updateEmployee( $value );
                        // echo "<pre>";
                        // print_r($value);
                        // echo "</pre>";
                        // echo "update ".$value['ThaiName']."<br/>";
                    }else{
                        // $this->insertEmployee();
                        // echo "insert ".$value['ThaiName']."<br/>";
                            // echo "<pre>";
                            // print_r($value);
                            // echo "</pre>";
                    }                    
                }
        }


        // echo "<pre>";
        // print_r($resultWS);
        // echo "</pre><hr/>";

        //             echo "<pre>";
        //             print_r($resultVSpace);
        //             echo "</pre>";
        
    }


    function selectEmployeeByGroupIDandEmpno($group, $empno){
        $sql = "SELECT * FROM employee WHERE  empno = :empno ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':empno'=>$empno));
        $r = $q->fetch();
        return $r;

    }
    function manageEmployee($data){
        if(trim($data['TerminType'])==""){
            echo $data['Email']."<br/>";
            if(trim($data['Email'])!=""){
                echo $checkEmail = $this->checkEmail(trim($data['Email']));

                if($checkEmail){
                    //UPDATE
                    $this->updateEmployee($data);
                }else{
                    // INSERT
                    $this->insertEmployee($data);
                }
            }
        }else{
            $this->terminType($data);
        }
    }

    private function terminType($data){
        $sql = "UPDATE employee SET termin_type = :termin_type, updated_datetime = NOW() WHERE empno = :empno";
        $q = $this->db->prepare($sql);
        $q->execute(array(':termin_type'=>$data['TerminType'],':empno'=>$data['EmpNo']));
    }
    private function checkEmail($email){
        $this->db->beginTransaction();
        $sql = "SELECT sid, emailaddr FROM employee WHERE emailaddr = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        $this->db->commit();
        print_r($r);
        if(isset($r['sid']) && $r['sid']>0){
            return true;
        }else{
            return false;
        }
    }
    function updateEmployee($data){
        $sql = "UPDATE employee 
                SET compid= :compid,compcde=:compcde, unitename=:unitename, unitid= :unitid, empno= :empno,
                nickname=:nickname, engname=:engname, 
                sex=:sex, thainame=:thainame, position=:position, extcallno= :extcallno, emailaddr= :emailaddr,
                employdate=:employdate, extno=:extno, birthdate=:birthdate, resigndate=:resigndate,
                servcde=:servcde, thaitname=:thaitname, compename=:compename,
                engtname= :engtname,  mobile=:mobile, unitlev3=:unitlev3, unitlev4=:unitlev4,
                comptname=:comptname, unitlev1=:unitlev1, unitlev2=:unitlev2, group_code=:group_code,
                unitlev5=:unitlev5, unitlev6=:unitlev6, updated_datetime=NOW(),termin_type=:termin_type 
                WHERE  emailaddr= :emailaddr";
        $this->db->beginTransaction();
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':compid'=>$data['compid'],
            ':compcde'=>$data['compcde'],
            ':unitename'=>$data['unitename'],
            ':unitid'=>$data['UnitID'],
            ':empno'=>$data['EmpNo'],
            ':nickname'=>$data['NickName'],
            ':engname'=>$data['EngName'],
            // ':engfirstname'=>$data['engfirstname'],
            // ':thailastname'=>$data['thailastname'],
            // ':cardno'=>$data['cardno'],
            ':sex'=>$data['Sex'],
            ':thainame'=>$data['ThaiName'],
            // ':floor'=>$data['floor'],
            ':position'=>$data['PositionEDesc'],
            ':extcallno'=>$data['extno'],
            ':emailaddr'=>trim($data['Email']),
            // ':org'=>$data['org'],
            ':employdate'=>$data['Employdate'], // need to be convert ************************
            ':extno'=>$data['extno'],
            ':birthdate'=>$data['BirthDate'],// need to be convert ************************
            ':resigndate'=>$data['ResignDate'],
            // ':thaifirstname'=>$data['thaifirstname'],
            ':servcde'=>$data['unitlev1'],
            ':thaitname'=>$data['ThaiTName'],
            ':compename'=>$data['compename'],
            // ':englastname'=>$data['englastname'],
            ':engtname'=>$data['EngTName'],
            // ':maxportraitfile'=>$data['maxportraitfile'],*********************************
            ':mobile'=>$data['Mobile'],
            ':unitlev3'=>$data['unitlev3'],
            ':unitlev4'=>$data['unitlev4'],
            ':comptname'=>$data['comptname'],
            ':unitlev1'=>$data['unitlev1'],
            ':unitlev2'=>$data['unitlev2'],
            ':group_code'=>$data['groupid'],
            ':unitlev5'=>$data['unitlev5'],
            ':unitlev6'=>$data['unitlev6'],
            ':termin_type'=>$data['TerminType']
            )
        );
        $this->db->commit();
        // echo $sql;
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

    }

    function insertEmployee($data){

        $sql = "INSERT INTO employee 
                (compid,compcde, unitename, unitid, empno,
                nickname, engname, 
                sex, thainame, position, extcallno, 
                emailaddr,
                employdate, extno, birthdate, resigndate,
                servcde, thaitname, compename,
                engtname,mobile, unitlev3, unitlev4,
                comptname, unitlev1, unitlev2, group_code,
                unitlev5, unitlev6, create_datetime,updated_datetime,termin_type)
                VALUES (
                :compid,:compcde,:unitename,:unitid,:empno,:nickname,:engname,:sex,:thainame,
                :position,:extcallno,:emailaddr,:employdate,:extno,:birthdate,:resigndate,:servcde,
                :thaitname,:compename,:engtname,:mobile,:unitlev3,:unitlev4,:comptname,:unitlev1,
                :unitlev2,:group_code,:unitlev5,:unitlev6,NOW(),NOW(),:termin_type
                )";
        $this->db->beginTransaction();
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':compid'=>$data['compid'],
            ':compcde'=>$data['compcde'],
            ':unitename'=>$data['unitename'],
            ':unitid'=>$data['UnitID'],
            ':empno'=>$data['EmpNo'],
            ':nickname'=>$data['NickName'],
            ':engname'=>$data['EngName'],
            // ':engfirstname'=>$data['engfirstname'],
            // ':thailastname'=>$data['thailastname'],
            // ':cardno'=>$data['cardno'],
            ':sex'=>$data['Sex'],
            ':thainame'=>$data['ThaiName'],
            // ':floor'=>$data['floor'],
            ':position'=>$data['PositionEDesc'],
            ':extcallno'=>$data['extno'],
            ':emailaddr'=>trim($data['Email']),
            // ':org'=>$data['org'],
            ':employdate'=>$data['Employdate'], // need to be convert ************************
            ':extno'=>$data['extno'],
            ':birthdate'=>$data['BirthDate'],// need to be convert ************************
            ':resigndate'=>$data['ResignDate'],
            // ':thaifirstname'=>$data['thaifirstname'],
            ':servcde'=>$data['unitlev1'],
            ':thaitname'=>$data['ThaiTName'],
            ':compename'=>$data['compename'],
            // ':englastname'=>$data['englastname'],
            ':engtname'=>$data['EngTName'],
            // ':maxportraitfile'=>$data['maxportraitfile'],*********************************
            ':mobile'=>$data['Mobile'],
            ':unitlev3'=>$data['unitlev3'],
            ':unitlev4'=>$data['unitlev4'],
            ':comptname'=>$data['comptname'],
            ':unitlev1'=>$data['unitlev1'],
            ':unitlev2'=>$data['unitlev2'],
            ':group_code'=>$data['groupid'],
            ':unitlev5'=>$data['unitlev5'],
            ':unitlev6'=>$data['unitlev6'],
            ':termin_type'=>$data['TerminType']
            )
        );
        $this->db->commit();
    }

    function insertContract($data){

    }
}

// INSERT INTO contract(sid, 
// prime_contract,
//  project_name, 
//  buyer_name, 
//  customer_group, 
//  end_user_code, 
//  end_user_name, 
//  end_user_address, 
//  contract_start_date, 
//  contract_end_date, 
//  first_level_support, 
//  project_manager, 
//  supplier_contact, 
//  supplier_name, 
//  supplier_phone, 
//  vendor_start_date, 
//  vendor_end_date, 
//  sla_condition, 
//  sla_type, 
//  asset_id,
//   asset_description, 
//   serial_number, 
//   bar_code, 
//   service_start_date, 
//   service_end_date, 
//   period_of_service, 
//   service_type, 
//   add_on_service, 
//   contract_no, 
//   create_datetime) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15],[value-16],[value-17],[value-18],[value-19],[value-20],[value-21],[value-22],[value-23],[value-24],[value-25],[value-26],[value-27],[value-28],[value-29],[value-30])



// INSERT INTO employee( compid, compcde, unitename, unitid, empno, nickname, engname, engfirstname, thailastname, cardno, sex, thainame, floor, position, extcallno, emailaddr, org, employdate, extno, birthdate, resigndate, thaifirstname, servcde, thaitname, compename, englastname, engtname, maxportraitfile, mobile, unitlev3, unitlev4, comptname, unitlev1, unitlev2, group_code, c_telephone, unitlev5, unitlev6, create_datetime) 
//     VALUES (:compid, :compcde, :unitename, :unitid, :empno, :nickname, :engname, :sex, :thainame, :position, :extcallno, :emailaddr, :employdate, :extno, :birthdate, :resigndate,
// :servcde
// :thaitname
// :compename
// :engtname
// :mobile
// :unitlev3
// :unitlev4
// :comptname
// :unitlev1
// :unitlev2
// :group_code
// :unitlev5
// :unitlev6 )