<?php 
class BugReportModel{
	private $db;
    private $sid, $issue_topic, $issue_subject, $issue_detail, $issue_picture, $severity_level, $create_by ;
    // private $type;

	function __construct() {
        require_once 'db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function saveBugReport( $email, $subject, $topic = "" , $detail = "", $picture = "", $severity = ""){

        $this->create_by = $email;
        $this->issue_topic = $topic;
        $this->issue_subject = $subject;
        $this->issue_detail = $detail;
        $this->issue_picture = $picture;
        $this->severity_level = $severity;
        return $this->insertBugReport();
    }

    public function selectBugReport(){
    	$sql = " SELECT * FROM bug_report " ;
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	return $r = $q->fetch();
    }

    public function insertBugReport(){
        $sql = "INSERT INTO bug_report( issue_topic, issue_subject, issue_detail, issue_picture, severity_level, create_by, create_datetime) 
                VALUES ( :issue_topic, :issue_subject, :issue_detail, :issue_picture, :severity_level, :create_by, NOW() ) " ;
        $q = $this->db->prepare($sql);
        return $q->execute( array(
                ':issue_topic' => $this->issue_topic, 
                ':issue_subject' => $this->issue_subject, 
                ':issue_detail' => $this->issue_detail, 
                ':issue_picture' => $this->issue_picture, 
                ':severity_level' => $this->severity_level, 
                ':create_by' => $this->create_by
            ));         
    }

    public function updateBugReport(){
        $sql = " UPDATE bug_report SET issue_topic = :issue_topic, issue_subject = :issue_subject, issue_detail = :issue_detail,
                    issue_picture = :issue_picture ,severity_level = :severity_level, create_by = :create_by, update_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        return $q->execute( array(
                ':issue_topic' => $this->issue_topic,
                ':issue_subject' => $this->issue_subject,
                ':issue_detail' => $this->issue_detail,
                ':issue_picture' => $this->issue_picture,
                ':severity_level' => $this->severity_level,
                ':create_by' => $this->create_by,
                ':sid' => $this->sid
        ));         
    }

    public function deleteBugReport(){
        $sql = " DELETE FROM bug_report WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        return $q->execute( array(
                ':sid' => $this->sid
        ));         
    }


}

?>