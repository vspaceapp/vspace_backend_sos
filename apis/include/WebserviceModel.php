<?php
error_reporting(0);
ini_set('display_errors', 0);
ini_set('max_execution_time', -1);
  class WebServiceModel
  {
  public  function __construct(){
    require_once __dir__.'/db_connect.php';
    // opening db connection
    $this->m = new DbConnect();
    $this->db = $this->m->connect();
  }


   public function getTicketdata($startdatetime,$enddatetime){
      $sql = "SELECT * from vw_search_ticket WHERE create_datetime BETWEEN :startdatetime and :enddatetime";
      $q = $this->db->prepare($sql);
      $q->execute(array(':startdatetime'=>$startdatetime, ':enddatetime'=>$enddatetime));
      $r = $q->fetchAll();
      if (empty($r)){
        return null;
      }else {
       return $r;
      }
  }

  public function getSLAdata($startdatetime,$enddatetime){
     $sql = "SELECT * from vw_ticket_sla WHERE updated_datetime BETWEEN :startdatetime and :enddatetime";
     $q = $this->db->prepare($sql);
     $q->execute(array(':startdatetime'=>$startdatetime, ':enddatetime'=>$enddatetime));
     $r = $q->fetchAll();
     if (empty($r)){
       return null;
     }else {
      return $r;
     }
  }

  public function getTicketStatusdata($startdatetime,$enddatetime){
     $sql = "SELECT * from vw_ticket_status WHERE update_datetime BETWEEN :startdatetime and :enddatetime";
     $q = $this->db->prepare($sql);
     $q->execute(array(':startdatetime'=>$startdatetime, ':enddatetime'=>$enddatetime));
     $r = $q->fetchAll();
     if (empty($r)){
       return null;
     }else {
      return $r;
     }
  }
  public function getTicketOwnerdata($startdatetime,$enddatetime){
     $sql = "SELECT * from vw_ticket_owner WHERE change_datetime BETWEEN :startdatetime and :enddatetime";
     $q = $this->db->prepare($sql);
     $q->execute(array(':startdatetime'=>$startdatetime, ':enddatetime'=>$enddatetime));
     $r = $q->fetchAll();
     if (empty($r)){
       return null;
     }else {
      return $r;
     }
   }






}
?>
