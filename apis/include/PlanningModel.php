<?php

class PlanningModel{

	private $db, $dbObj;
 	private $day;
 	private $engineer;

 	// FOR OLD DATABASE
 	private $tbl_busy_employee = "tbl_busy_employee";
 	private $tbl_service_report = "tbl_service_report";
    private $tbl_busy_employee_doc = "tbl_busy_employee_doc";
    private $tbl_customer_company = "tbl_customer_company";
    private $tbl_associate_engineer = "tbl_associate_engineer";
    private $tbl_busy_mlookup = "tbl_busy_mlookup";

    private $start;
    private $end;
    // FOR OLD
    private $employeeID = "";
    private $engineer_no, $engineer_no_holiday;
    private $numberEngineerBuddy;
    private $switch_call,$continue_holiday;
    public $permission_role;
    function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->dbObj = $db = new DbConnect();
        $this->db = $db->connect();
        $this->email = $email;
        if($this->email!=""){
            $this->permission_role = $this->dbObj->permissionRoleByEmail($this->email);
        }
    }

    public function continuteHoliday($beforeDay, $today){
        if($beforeDay==true && $today==true){
            return true;
        }
        return false;
    }

    public function clearOldStandby($start_date, $email=""){
        //STEP 1 DELETE DATA in day current in loop
        $sql = "DELETE FROM standby_night_ace WHERE standby_date >= :start_date";
        $q = $this->db->prepare($sql);
        $q->execute(array(':start_date'=>$start_date));
    }
    public function saveStandby($data, $email=""){
        if($email!=""){
            $this->email = $email;
        }

        foreach ($data as $key => $value) {
            $this->save7x24($value);
        }
    }
    private function save7x24($data){

        $standby_date = $data['Day'];
        $sequence = "";
        $employee_id = "";
        $email = "";
        $buddy_no = $data['buddy-sid'];
        $time_start = "";
        $time_end = "";
        for ($i=0; $i < 2; $i++) {
            if($i==0){
                $email = $data['staffOne'];
                $employee_id = $data['staffEmpnoOne'];
            }else{
                $email = $data['staffTwo'];
                $employee_id = $data['staffEmpnoTwo'];
            }

            if(!$data['isHoliday']){
                $time_start_1 = "17:00";
                $time_end_1 = "23:59";

                $time_start_2 = "00:00";
                $time_end_2 = "07:59";
            }else{
                $time_start_1 = "08:00";
                $time_end_1 = "23:59";

                $time_start_2 = "00:00";
                $time_end_2 = "07:59";
            }

            $sql = "INSERT INTO standby_night_ace (standby_date,sequence,employee_id,email,buddy_no,time_start,time_end,create_by,create_time) VALUES (:standby_date,:sequence,:employee_id,:email,:buddy_no,:time_start,:time_end,:create_by,NOW()) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':standby_date'=>$standby_date,':sequence'=>$sequence,':employee_id'=>$employee_id,':email'=>$email,':buddy_no'=>$buddy_no,':time_start'=>$time_start_1,':time_end'=>$time_end_1,':create_by'=>$this->email));

            $sql = "INSERT INTO standby_night_ace (standby_date,sequence,employee_id,email,buddy_no,time_start,time_end,create_by,create_time) VALUES (date_add(:standby_date, interval 1 day) ,:sequence,:employee_id,:email,:buddy_no,:time_start,:time_end,:create_by,NOW()) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':standby_date'=>$standby_date,':sequence'=>$sequence,':employee_id'=>$employee_id,':email'=>$email,':buddy_no'=>$buddy_no,':time_start'=>$time_start_2,':time_end'=>$time_end_2,':create_by'=>$this->email));
        }
    }

    private function hasStandby7x24($data, $sequence){
        $sql = "SELECT id FROM standby_night_ace WHERE standby_date = :standby_date AND sequence = :sequence AND time_start = :time_start AND time_end = :time_end ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':standby_date'=>$data['date'],':sequence'=>$sequence,':time_start'=>$data['time_start'],':time_end'=>$data['time_end']));
        $r = $q->fetch();

        if(isset($r['id']) && $r['id']>0){
            return true;
        }
        return false;
    }
    public function updateTableStandby($data){
        for($i=1;$i<=2;$i++){
            if($this->hasStandby7x24($data, $i)){
                $sql = "UPDATE standby_night_ace SET email = :email, modified_time = NOW(), sequence = :sequence, employee_id = :empno, buddy_no = :buddy_no WHERE standby_date = :standby_date AND sequence = :sequence AND time_start = :time_start AND time_end = :time_end ";
                $q = $this->db->prepare($sql);
                if($i==1){
                    $res = $q->execute(
                        array(':email'=>$data['engineer']['email_buddy_one'],
                            ':empno'=>$data['engineer']['empno_one'],
                            ':buddy_no'=>$data['engineer']['sid'],
                            ':standby_date'=>$data['date'],
                            ':sequence'=>$i,
                            ':time_start'=>$data['time_start'],
                            ':time_end'=>$data['time_end']
                            )
                        );
                }else{
                    $res = $q->execute(
                        array(':email'=>$data['engineer']['email_buddy_two'],
                            ':empno'=>$data['engineer']['empno_two'],
                            ':buddy_no'=>$data['engineer']['sid'],
                            ':standby_date'=>$data['date'],
                            ':sequence'=>$i,
                            ':time_start'=>$data['time_start'],
                            ':time_end'=>$data['time_end']
                            )
                        );
                }
            }else{
                    $sql = "INSERT INTO standby_night_ace (email, employee_id, buddy_no, standby_date, sequence, time_start, time_end, modified_time, create_time) VALUES (:email, :empno, :buddy_no, :standby_date, :sequence, :time_start, :time_end, NOW(), NOW()) ";
                    $q = $this->db->prepare($sql);
                    if($i==1){
                        $res = $q->execute(
                        array(':email'=>$data['engineer']['email_buddy_one'],
                            ':empno'=>$data['engineer']['empno_one'],
                            ':buddy_no'=>$data['engineer']['sid'],
                            ':standby_date'=>$data['date'],
                            ':sequence'=>$i,
                            ':time_start'=>$data['time_start'],
                            ':time_end'=>$data['time_end']
                            )
                        );
                    }else{
                        $res = $q->execute(
                            array(':email'=>$data['engineer']['email_buddy_two'],
                                ':empno'=>$data['engineer']['empno_two'],
                                ':buddy_no'=>$data['engineer']['sid'],
                                ':standby_date'=>$data['date'],
                                ':sequence'=>$i,
                                ':time_start'=>$data['time_start'],
                                ':time_end'=>$data['time_end']
                                )
                            );
                    }
            }

        }
    }
    public function generate7x24($start, $end, $engineer_no,$switch_call,$continue_holiday,$engineer_no_holiday=0){
        $this->engineer_no = $engineer_no-=1;
        $this->switch_call = $switch_call;
        $this->continue_holiday = $continue_holiday;

        $this->engineer_no_holiday = $engineer_no_holiday;
        $this->engineer_no_holiday -=2;

        $this->start = $this->dbObj->convertDateSlashForDb($start);
        $this->end = $this->dbObj->convertDateSlashForDb($end);
        $listDate = $this->sheduleStandbyGen();

        $listBuddy = $this->buddy();
        $this->numberEngineerBuddy = count($listBuddy);

        $data = array();

        $type_gen = 'splitHoliday';
        if($engineer_no_holiday>0){
            $data = $this->splitHoliday($listBuddy, $listDate, $data);
        }else{
            $data = $this->runContinue($listBuddy, $listDate, $data);
            $type_gen = 'runContinue';
        }
        return array($listBuddy,$listDate,$data,$type_gen);
    }

    public function splitHoliday($listBuddy, $listDate,$data){
        $dayCumulative = 0;
        $currentLoopHoliday = false;
        foreach ($listDate as $key => $value) {
            $dayCumulative++;

            if(isset($listDate[$key-1]) && !$listDate[$key-1]['holiday']){
                $value['holiday'] = false;
            }
            if($value['holiday']){
                if(!$currentLoopHoliday){
                    $this->engineer_no_holiday++;

                    $timeStart1 = "00:00";
                    $timeEnd1 = "07:59";
                    $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart1,'time_end'=>$timeEnd1,'engineer'=>$listBuddy[$this->engineer_no]);
                    array_push($data, $temp);
                    $this->updateTableStandby($temp);
                }else{
                    $timeStart1 = "00:00";
                    $timeEnd1 = "07:59";
                    $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart1,'time_end'=>$timeEnd1,'engineer'=>$listBuddy[$this->engineer_no_holiday]);
                    array_push($data, $temp);
                    $this->updateTableStandby($temp);
                }
                // if(isset($listDate[$key+1]['holiday']) && $this->continuteHoliday($listDate[$key]['holiday'], $listDate[$key+1]['holiday'])){
                    $this->engineer_no_holiday = $this->engineer_no_holiday%$this->numberEngineerBuddy;
                // }
                $timeStart2 = "08:00";$timeEnd2 = "23:59";
                $temp = array(
                    'date'=>$value['date_Ymd'],
                    'holiday'=>$value['holiday'],
                    'time_start'=>$timeStart2,
                    'time_end'=>$timeEnd2,
                    'engineer'=>$listBuddy[$this->engineer_no_holiday]
                );
                array_push($data, $temp);
                $this->updateTableStandby($temp);

                $currentLoopHoliday = true;
            }else{

                if($currentLoopHoliday){
                    $timeStart1 = "00:00";$timeEnd1 = "07:59";
                    $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart1,'time_end'=>$timeEnd1,'engineer'=>$listBuddy[$this->engineer_no_holiday]);
                    array_push($data, $temp);
                    $this->updateTableStandby($temp);
                }else{
                    $timeStart1 = "00:00";$timeEnd1 = "07:59";
                    $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart1,'time_end'=>$timeEnd1,'engineer'=>$listBuddy[$this->engineer_no]);
                    array_push($data, $temp);
                    $this->updateTableStandby($temp);
                }

                if($dayCumulative>=$this->switch_call){
                    $this->engineer_no++;$this->engineer_no = $this->engineer_no%$this->numberEngineerBuddy;
                    $dayCumulative = 0;
                }
                $timeStart2 = "17:00";$timeEnd2 = "23:59";
                $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart2,'time_end'=>$timeEnd2,'engineer'=>$listBuddy[$this->engineer_no]);
                array_push($data, $temp);
                $this->updateTableStandby($temp);
                $currentLoopHoliday = false;
            }
        }
        return $data;
    }

    public function runContinue($listBuddy, $listDate,$data){
        $dayCumulative = 0;
        foreach ($listDate as $key => $value) {
            $dayCumulative++;
            if($value['holiday']){
                $timeStart1 = "00:00";
                $timeEnd1 = "07:59";
                $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart1,'time_end'=>$timeEnd1,'engineer'=>$listBuddy[$this->engineer_no],);
                array_push($data, $temp);
                $this->updateTableStandby($temp);
                if(($key-1)>=0 && $this->continue_holiday){
                    if(!$this->continuteHoliday($listDate[$key-1]['holiday'], $listDate[$key]['holiday'])){
                        if($dayCumulative>=$this->switch_call){
                            $this->engineer_no++;$this->engineer_no = $this->engineer_no%$this->numberEngineerBuddy;
                            $dayCumulative = 0;
                        }
                    }
                }else{
                    if($dayCumulative>=$this->switch_call){
                        $this->engineer_no++;$this->engineer_no = $this->engineer_no%$this->numberEngineerBuddy;
                        $dayCumulative = 0;
                    }
                }
                $timeStart2 = "08:00";$timeEnd2 = "23:59";
                $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart2,'time_end'=>$timeEnd2,'engineer'=>$listBuddy[$this->engineer_no]);
                array_push($data, $temp);
                $this->updateTableStandby($temp);
            }else{
                $timeStart1 = "00:00";$timeEnd1 = "07:59";
                $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart1,'time_end'=>$timeEnd1,'engineer'=>$listBuddy[$this->engineer_no]);
                array_push($data, $temp);
                $this->updateTableStandby($temp);
                if($dayCumulative>=$this->switch_call){
                    $this->engineer_no++;$this->engineer_no = $this->engineer_no%$this->numberEngineerBuddy;
                    $dayCumulative = 0;
                }
                $timeStart2 = "17:00";$timeEnd2 = "23:59";
                $temp = array('date'=>$value['date_Ymd'],'holiday'=>$value['holiday'],'time_start'=>$timeStart2,'time_end'=>$timeEnd2,'engineer'=>$listBuddy[$this->engineer_no]);
                array_push($data, $temp);
                $this->updateTableStandby($temp);
            }
        }
        return $data;
    }
    public function buddy(){
        $sql = "SELECT SB.*,
        E.thainame thainame_buddy_one,E.emailaddr email_buddy_one, E.mobile mobile_buddy_one, E.empno empno_one,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_buddy_one,
        E2.thainame thainame_buddy_two,E2.emailaddr email_buddy_two, E2.mobile mobile_buddy_two, E2.empno empno_two,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',E2.maxportraitfile) pic_buddy_two,
        CASE next_call WHEN 1 THEN 1 ELSE '' END next_call_is_one,
        CASE next_call WHEN 2 THEN 1 ELSE '' END next_call_is_two
            FROM standby_buddy SB
            LEFT JOIN employee E ON SB.buddy_one = E.emailaddr
            LEFT JOIN employee E2 ON SB.buddy_two = E2.emailaddr ORDER BY SB.sid ASC ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }
    private $forTimeSheet;
    public function sheduleTimeline($start, $end, $forTimeSheet=0){
        $this->forTimeSheet = $forTimeSheet;
        $this->start = $start;
        $this->end = $end;

        $data = $this->sheduleStandbyGen();
        return $data;
    }

    public function sheduleTimelineInmonth($month, $addMonth, $forTimeSheet=0){
        $this->forTimeSheet = $forTimeSheet;
        $this->month = $month;
        $this->start = $month."-01";
        if($addMonth){
            $sql = "SELECT DATE_ADD('".$this->start."', INTERVAL ".$addMonth." MONTH) start_date, DATE_FORMAT(DATE_ADD('".$this->start."', INTERVAL ".$addMonth." MONTH),'%M-%Y') month";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetch();
            $this->start = $r['start_date'];
            $this->month = $r['month'];
        }

        $sql = "SELECT DATE_ADD('".$this->start."', INTERVAL 31 DAY) end_date, DATE_FORMAT('".$this->start."','%M %Y') month ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        $this->end = $r['end_date'];
        $this->month = $r['month'];

        $data = $this->sheduleStandbyGen();
        return array('data'=>$data,'month'=>$this->month);
    }

    public function sheduleStandbyGen(){

        $date = array();

        $sql = "SELECT DATEDIFF('".$this->end."','".$this->start."') AS DiffDate;";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();

        $length = $r['DiffDate'];

        for($i=0;$i<$length;$i++){
            $sql = "SELECT
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%d %b %Y') day,
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%Y-%m-%d') date_Ymd,
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%Y') PeriodYear,
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%m') PeriodMonth,
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%d') d,
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%Y-%m-%d') WorkDay,
                DATE_FORMAT(date_add(:start,interval ".$i." day),'%d.%b.%Y') date_dmY,
                DATE_FORMAT(date_add(:start,interval ".($i+1)." day),'%Y-%m-%d') next_date_Ymd,
                DATE_FORMAT(date_add(:start,interval ".($i+1)." day),'%d.%b.%Y') next_date_dmY,
                CASE DATE_FORMAT(date_add(:start,interval ".$i."  day),'%w')
                WHEN 0 THEN 'Sun'
                WHEN 1 THEN 'Mon'
                WHEN 2 THEN 'Tue'
                WHEN 3 THEN 'Wed'
                WHEN 4 THEN 'Thu'
                WHEN 5 THEN 'Fri'
                ELSE 'Sat' END date_w ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':start'=>$this->start));
            $r = $q->fetch();
            $this->day = $r['date_Ymd'];
            $holiday = $this->isHoliday();
            if(!$this->forTimeSheet){
                array_push($date, array(
                    'date_Ymd'=>$r['date_Ymd'],
                    'date_dmY'=>$r['date_dmY'],
                    'date_w'=>$r['date_w'],
                    'next_date_Ymd'=>$r['next_date_Ymd'],
                    'next_date_dmY'=>$r['next_date_dmY'],
                    'day'=>$r['day'],
                    // 'standby7x24'=>$this->standbyAll7x24(),
                    // 'otherProducts'=>$this->standbyAllOtherProducts(),
                    'holiday'=>$holiday['isHoliday'],
                    'holiday_desc'=>$holiday['desc'],
                    'ISWorkDay'=>(($holiday)?0:1),
                    'PeriodYear'=>$r['PeriodYear'],
                    'PeriodMonth'=>$r['PeriodMonth'],
                    'WorkDay'=>$r['WorkDay'],
                    'd'=>$r['d']
                    ));
            }else{
                array_push($date, array(
                    'date_Ymd'=>$r['date_Ymd'],
                    'date_w'=>$r['date_w'],
                    'next_date_Ymd'=>$r['next_date_Ymd'],
                    // 'next_date_dmY'=>$r['next_date_dmY'],
                    'is_work_day'=>(($holiday)?0:1),
                    'year'=>$r['PeriodYear'],
                    'month'=>$r['PeriodMonth'],
                    'WorkDay'=>$r['WorkDay'],
                    'd'=>$r['d']
                    ));
            }
        }
        return $date;
    }

    public function genYear($year, $month){
        $date = array();
        $startDate = $year."-".$month."-01 00:00:00";
        for($i=0;$i<31;$i++){
            $sql = "SELECT DATE_FORMAT(date_add('".$startDate."',interval ".$i." day),'%Y-%m-%d') date_Ymd,
                DATE_FORMAT(date_add('".$startDate."',interval ".$i." day),'%d.%b.%Y') date_dmY,
                DATE_FORMAT(date_add('".$startDate."',interval ".$i." day),'%d') date_day,
                CASE DATE_FORMAT(date_add('".$startDate."',interval ".$i."  day),'%w')
                WHEN 0 THEN 'Sun'
                WHEN 1 THEN 'Mon'
                WHEN 2 THEN 'Tue'
                WHEN 3 THEN 'Wed'
                WHEN 4 THEN 'Thu'
                WHEN 5 THEN 'Fri'
                ELSE 'Sat' END date_w ";
            $q = $this->db->prepare($sql);
            $q->execute();
            $r = $q->fetch();
            $this->day = $r['date_Ymd'];
            $holiday = $this->isHoliday();
            array_push($date, array(
                'date_Ymd'=>$r['date_Ymd'],
                'date_dmY'=>$r['date_dmY'],
                'date_day'=>$r['date_day'],
                'date_w'=>$r['date_w'],
                'standby7x24'=>$this->standbyAll7x24(),
                'otherProducts'=>$this->standbyAllOtherProducts(),
                'holiday'=>$holiday['isHoliday'],
                'holiday_desc'=>$holiday['desc']
                ));
        }
        return $date;
    }
    public function sheduleStandby(){
        $date = array();

        $sql = "SELECT *,
        DATE_FORMAT(day,'%Y-%m-%d') date_Ymd,
        DATE_FORMAT(day,'%d.%b.%Y') date_dmY,
        DATE_FORMAT(day,'%W') date_w
        FROM days WHERE day >= DATE_FORMAT(NOW(),'%Y-%m-%d') ORDER BY day ASC LIMIT 0,200";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $this->day = $value['date_Ymd'];
            array_push($date, array(
                'date_Ymd'=>$value['date_Ymd'],
                'date_dmY'=>$value['date_dmY'],
                'date_w'=>$value['date_w'],
                'standby7x24'=>$this->standbyAll7x24(),
                'otherProducts'=>$this->standbyAllOtherProducts(),
                'holiday'=>(($value['is_holiday']==0)?false:true),
                'holiday_desc'=>$value['name']
            ));
        }


        return $date;
    }

		public function sheduleStandbyReact(){
        $date = array();

        $sql = "SELECT *,
        DATE_FORMAT(day,'%Y-%m-%d') date_Ymd,
        DATE_FORMAT(day,'%d.%b.%Y') date_dmY,
        DATE_FORMAT(day,'%W') date_w
        FROM days WHERE day >= DATE_FORMAT(NOW(),'%Y-%m-%d') ORDER BY day ASC LIMIT 0,200";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $this->day = $value['date_Ymd'];
            array_push($date, array(
                'date_Ymd'=>$value['date_Ymd'],
                'date_dmY'=>$value['date_dmY'],
                'date_w'=>$value['date_w'],
                // 'standby7x24'=>$this->standbyAll7x24(),
                'otherProducts'=>$this->groupConcatOther(),
                'holiday'=>(($value['is_holiday']==0)?false:true),
                'holiday_desc'=>$value['name'],
								'groupConcat7x24'=>$this->groupConcat7x24()
            ));
        }


        return $date;
    }

		private function groupConcat7x24(){
			$sql = "SELECT GROUP_CONCAT(E.thainame SEPARATOR ', ') standby_7x24
			FROM tasks S
			LEFT JOIN employee E ON E.emailaddr = S.engineer
			WHERE DATE_FORMAT(S.appointment,'%Y-%m-%d') = :standby_date AND appointment_type = '2' ORDER BY appointment ";
			$q = $this->db->prepare($sql);
			$q->execute(array(':standby_date'=>$this->day));
			$r = $q->fetch();
			return $r['standby_7x24'];
		}

		private function groupConcatOther(){
			$sql = "SELECT GROUP_CONCAT(CONCAT(E.thainame,' ',DATE_FORMAT(S.appointment,'%H:%i')) SEPARATOR ', ') standby_other
				FROM tasks S
				LEFT JOIN employee E ON E.emailaddr = S.engineer
				WHERE DATE_FORMAT(S.appointment,'%Y-%m-%d') = :standby_date AND appointment_type = '4' ORDER BY S.appointment DESC";
			$q = $this->db->prepare($sql);
			$q->execute(array(':standby_date'=>$this->day));
			$r = $q->fetch();
				return $r['standby_other'];
		}

    public function sheduleStandbyForCallcenter(){
        $date = array();

        $sql = "SELECT *,
        DATE_FORMAT(day,'%Y-%m-%d') date_Ymd,
        DATE_FORMAT(day,'%d.%b.%Y') date_dmY,
        DATE_FORMAT(day,'%W') date_w
        FROM days WHERE day >= DATE_FORMAT(NOW(),'%Y-%m-%d') ORDER BY day ASC LIMIT 0,2";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $this->day = $value['date_Ymd'];
            array_push($date, array(
                'date_Ymd'=>$value['date_Ymd'],
                'date_dmY'=>$value['date_dmY'],
                'date_w'=>$value['date_w'],
                'standby7x24'=>$this->standbyAll7x24(),
                'otherProducts'=>$this->selectOtherProduct(),
                'holiday'=>(($value['is_holiday']==0)?false:true),
                'holiday_desc'=>$value['name']
            ));
        }


        return $date;
    }

    public function standby7x24Calendar($email, $start, $end){
        $data = array();

        $sql = "SELECT *,DATE_FORMAT(CONCAT(standby_date,' ',time_start),'%H:%i') start_time_df,
        DATE_FORMAT(CONCAT(standby_date,' ',time_end),'%H:%i') time_end_df
        FROM standby_night_ace WHERE CONCAT(standby_date,' ',time_start) > :start AND CONCAT(standby_date,' ',time_end) < :end_time AND email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':start'=>$start,':end_time'=>$end,':email'=>$email));
        $data = $q->fetchAll();
        return $data;
    }
    public function isHoliday(){
        $sql = "SELECT id,description_th FROM tbl_holiday WHERE holiday = :day AND (data_status = 'Y' OR data_status = '1') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':day'=>$this->day));
        $r = $q->fetch();
        if(isset($r['id']) && $r['id']>0){
            return array('isHoliday'=>true,'desc'=>$r['description_th']);
        }
        return array('isHoliday'=>false,'desc'=>'');
    }
    public function standbyAll7x24(){
        $sql = "SELECT S.*,
        DATE_FORMAT(S.appointment,'%H:%i') time_start,
        DATE_FORMAT(DATE_ADD(S.appointment, INTERVAL S.expect_duration hour),'%H:%i(%Y-%m-%d)') time_end,
        E.nickname, E.thainame, E.emailaddr,E.mobile,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic,
        (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s') FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case
        FROM tasks S
        LEFT JOIN employee E ON E.emailaddr = S.engineer
        WHERE DATE_FORMAT(S.appointment,'%Y-%m-%d') = :standby_date AND appointment_type = '2' ORDER BY appointment ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':standby_date'=>$this->day));
        return $q->fetchAll();
    }

		public function selectOtherProduct(){
			$sql = "SELECT S.*,
			DATE_FORMAT(S.appointment,'%H:%i') time_start,
			DATE_FORMAT(DATE_ADD(S.appointment, INTERVAL S.expect_duration hour),'%H:%i(%Y-%m-%d)') time_end,
			E.nickname, E.thainame, E.emailaddr,E.mobile,
			CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic,
			(SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s')
			FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case
			FROM tasks S
			LEFT JOIN employee E ON E.emailaddr = S.engineer
			WHERE DATE_FORMAT(S.appointment,'%Y-%m-%d') = :standby_date AND appointment_type = '4' ORDER BY appointment ";
			$q = $this->db->prepare($sql);
			$q->execute(array(':standby_date'=>$this->day));
			return $q->fetchAll();
		}

    public function standbyAllOtherProducts(){
        $sql = "SELECT S.*,DATE_FORMAT(S.time_start,'%H:%i') time_start, DATE_FORMAT(S.time_end,'%H:%i') time_end, CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,
        E.nickname, E.thainame, E.emailaddr,E.mobile,CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic,
        (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s') FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case
        FROM standby_oncall_other_products S
        LEFT JOIN employee E ON E.empno = S.employee_id
        WHERE standby_date = :standby_date ORDER BY time_start ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':standby_date'=>$this->day));
        return $q->fetchAll();
    }
    public function standby7x24(){
        $sql = "SELECT DATE_FORMAT(NOW(),'%H') hours ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        // $r['hours'] = 18;
        if($r['hours']>7 && $r['hours']<=23){
            $sql = "SELECT
            SB.next_call,
            S.*,E.nickname, S.engineer email,
            E.thainame, E.emailaddr,E.mobile,E.maxportraitfile,
            CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,
            (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s')
                FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case,
            DATE_FORMAT(S.appointment,'%Y-%m-%d %H:%i') time_start,
            DATE_FORMAT(DATE_ADD(S.appointment,INTERVAL S.expect_duration hour),'%Y-%m-%d %H:%i') time_end
            FROM tasks S
            LEFT JOIN employee E ON E.emailaddr = S.engineer
            LEFT JOIN standby_buddy SB ON SB.sid = S.standby_buddy_sid
            WHERE DATE_FORMAT(S.appointment,'%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')
            AND S.appointment_type = '2'

             ORDER BY S.sid ";
        }else if($r['hours']>=0 && $r['hours']<8){
            $sql = "SELECT SB.next_call,
            S.*,E.nickname, S.engineer email, E.thainame, E.emailaddr,E.mobile,E.maxportraitfile,
            CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,
            (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s')
                FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case,
            DATE_FORMAT(S.appointment,'%Y-%m-%d %H:%i') time_start,
            DATE_FORMAT(DATE_ADD(S.appointment,INTERVAL S.expect_duration hour),'%Y-%m-%d %H:%i') time_end
            FROM tasks S
            LEFT JOIN employee E ON E.emailaddr = S.engineer
            LEFT JOIN standby_buddy SB ON SB.sid = S.standby_buddy_sid
            WHERE
            (
                DATE_FORMAT(DATE_ADD(S.appointment, INTERVAL S.expect_duration hour),'%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')
            )
            AND S.appointment_type = '2'
            ORDER BY S.sid ";
        }

        // AND S.time_end > DATE_FORMAT(NOW(), '%H:%i')
        // AND S.time_end > DATE_FORMAT(NOW(), '%H:%i')

        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        $min = 10000000000000000000000000;
        foreach ($r as $key => $value) {
            // if($key==0){
            //     $min = $value['last_create_case'];
            // }
            $r[$key]['last_case'] = $this->getTicket7x24($value['email']);

            if($value['last_create_case']<$min){
                $min = $value['last_create_case'];
                $r[$key]['star'] = true;
                if($key>0)
                    $r[$key-1]['star'] = false;
            }else{
                $r[$key]['star'] = false;
            }
        }

        if(isset($r[0]['next_call'])){
            if($r[0]['next_call']=="1"){
                $r[0]['star'] = 1;
                $r[1]['star'] = 0;
            }else{
                $r[0]['star'] = 0;
                $r[1]['star'] = 1;
            }
        }
        return $r;
    }

    private function getTicket7x24($email){
        $this->dbObj->setTable($email);
        $h = date('H');

        if($h<8){
            $sql = "SELECT *,DATE_FORMAT(T.create_datetime, '%d.%m.%Y %H:%i') create_datetime_df FROM ".$this->dbObj->table_ticket." T WHERE T.owner = :email AND T.via = 'remedy'
            AND T.create_datetime > CONCAT(DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY),'%Y-%m-%d'),' ','16:30:00')
            ORDER BY sid DESC LIMIT 0,5";
            $q = $this->db->prepare($sql);
            $q->execute(array(':email'=>$email));
            $r = $q->fetchAll();
        }else{
            $sql = "SELECT *,DATE_FORMAT(T.create_datetime, '%d.%m.%Y %H:%i') create_datetime_df FROM ".$this->dbObj->table_ticket." T WHERE T.owner = :email AND T.via = 'remedy'
            AND T.create_datetime > CONCAT( DATE_FORMAT(NOW(), '%Y-%m-%d'),' ','16:30:00')
            ORDER BY sid DESC LIMIT 0,5";
            $q = $this->db->prepare($sql);
            $q->execute(array(':email'=>$email));
            $r = $q->fetchAll();
        }

        return $r;
    }

    // (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s') FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case

    public function standbyAce(){

        $data = array(
            0=>array('position'=>'standby_day_ace_first'),
            1=>array('position'=>'standby_day_ace_second'),
            2=>array('position'=>'standby_day_ace_third'),
            3=>array('position'=>'standby_day_ace_fourth'),
            4=>array('position'=>'standby_day_ace_fifth'),
            5=>array('position'=>'standby_consultant','display_name_position'=>'Standby Consultant (ACE)'),
            6=>array('position'=>'standby_bay','display_name_position'=>'Standby BAY (ธนาคารกรุงศรีอยุธยา) ')
            );

        foreach ($data as $key => $value) {
            $data[$key]['data'] = $this->listAce($value['position']);
        }
        return $data;
    }
    private function listAce($position){
        $sql = "SELECT S.*,E.nickname, E.thainame, E.emailaddr,E.mobile,E.maxportraitfile , CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full, E.emailaddr email
        FROM standby_7x24 S
        INNER JOIN employee E ON E.empno = ".$position."
        WHERE standby_date = DATE_FORMAT(NOW(), '%Y-%m-%d')
        AND time_start < DATE_FORMAT(NOW(), '%H:%i') AND time_end > DATE_FORMAT(NOW(), '%H:%i') ";
        // ORDER BY last_create_case ";

        $q = $this->db->prepare($sql);
        $q->execute(array(':position'=>$position));
        $r = $q->fetch();
        if($position=="standby_bay"){
            $r['display_position'] = 1;
        }else if($position=="standby_consultant"){
            $r['display_position'] = 1;
        }else{
            $r['display_position'] = 0;
        }

        return $r;
    }

    public function standbyOtherProduct(){
         $sql = "SELECT DATE_FORMAT(NOW(),'%H') hours ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();

        if($r['hours']>7 && $r['hours']<17){
            $sql = "SELECT S.*,E.nickname, E.thainame, E.emailaddr,E.mobile,E.maxportraitfile, CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,E.emailaddr email,
            (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s') FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case
            FROM standby_oncall_other_products S
            LEFT JOIN employee E ON E.empno = S.employee_id
            WHERE standby_date = DATE_FORMAT(NOW(), '%Y-%m-%d')
            AND time_end > DATE_FORMAT(NOW(), '%H:%i') ORDER BY last_create_case ";
        }else{
            $sql = "SELECT S.*,E.nickname, E.thainame, E.emailaddr,E.mobile,E.maxportraitfile, CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,E.emailaddr email,
            (SELECT DATE_FORMAT(create_datetime,'%Y%m%d%H%i%s') FROM ticket T WHERE T.owner = E.emailaddr ORDER BY T.create_datetime DESC LIMIT 0,1 ) last_create_case
            FROM standby_oncall_other_products S
            LEFT JOIN employee E ON E.empno = S.employee_id
            WHERE standby_date = DATE_FORMAT(NOW(), '%Y-%m-%d')
            AND time_start < DATE_FORMAT(NOW(), '%H:%i') AND time_end > DATE_FORMAT(NOW(), '%H:%i') ORDER BY last_create_case ";
        }

        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        $min = 10000000000000000000000000;
        foreach ($r as $key => $value) {

            if($value['last_create_case']<$min){
                $min = $value['last_create_case'];
                $r[$key]['star'] = true;
                if($key>0)
                    $r[$key-1]['star'] = false;
            }else{
                $r[$key]['star'] = false;
            }
        }
        return $r;
    }
    private $viewDateYMD;
    public function planningDayViewByCanassign($day, $email="", $viewDateYMD=""){
        $this->viewDateYMD = $viewDateYMD;
        $this->day = $day;
        $month = explode("-", $day);
        $this->month = $month['1']."-".$month['2'];
        // $listEngineer = $this->listEngineer();
        $this->email = $email;
        $userModel = new UserModel();
        $search = "";
        $listUserCanAddTask = $userModel->listUserCanAddTask($email,$search);
        $this->dbObj->setTable($this->email);
        foreach ($listUserCanAddTask as $key => $value) {
            $this->engineer = $value['email'];
            $this->employeeID = $value['empno'];
            $listUserCanAddTask[$key]['pic_engineer'] = $value['pic_employee'];
            $listUserCanAddTask[$key]['tasks'] = $this->tasks();
            // $listUserCanAddTask[$key]['taskOld'] = $this->taskOld();
            $listUserCanAddTask[$key]['listBusyEmployeeByDay'] = $this->listBusyEmployeeByDay();
            $listUserCanAddTask[$key]['ace'] = $this->listStandbyAce();
            $listUserCanAddTask[$key]['consultAce'] = $this->listStandbyConsultAce();
            $listUserCanAddTask[$key]['standby7x24'] = array();//$this->listStandby7x24();
            $listUserCanAddTask[$key]['otherProducts'] = $this->otherProducts();
            // $listUserCanAddTask[$key]['implement'] = $this->implement();
            $listUserCanAddTask[$key]['numberTaskThisMonth'] = $this->numberTaskThisMonth($value['email']);
        }

        return $listUserCanAddTask;

    }

    private function numberTaskThisMonth($engineer){
        $sql = "(SELECT COUNT(DISTINCT(T.appointment)) number_service FROM tasks_log T WHERE T.engineer = :engineer
                    AND (DATE_FORMAT(T.appointment,'%m-%Y') = :day))";
        $q = $this->db->prepare($sql);
        $q->execute(array(':engineer'=>$engineer,':day'=>$this->month));
        $r = $q->fetch();
        return $r['number_service'];
    }

    public function planningDayView($day){

    	$this->day = $day;
        $dayExplode = explode("-", $this->day);
        $this->viewDateYMD = $dayExplode[2]."-".$dayExplode[1]."-".$dayExplode[0];
    	$listEngineer = $this->listEngineer();

    	foreach ($listEngineer as $key => $value) {
    		$this->engineer = $value['email'];
    		$this->employeeID = $value['empno'];

    		$listEngineer[$key]['tasks'] = $this->tasks();
    		$listEngineer[$key]['taskOld'] = $this->taskOld();
    		$listEngineer[$key]['listBusyEmployeeByDay'] = $this->listBusyEmployeeByDay();
            $listEngineer[$key]['ace'] = $this->listStandbyAce();
            $listEngineer[$key]['standby7x24'] = $this->listStandby7x24();
            $listEngineer[$key]['otherProducts'] = $this->otherProducts();
            $listEngineer[$key]['implement'] = $this->implement();
    	}

    	return $listEngineer;

    }

    public function standby7x24OnDay($email, $day){
        $this->day = $day;
        $this->email = $email;
        $data = $this->listStandby7x24FormatYmd();
        return $data;
    }

    private function listStandby7x24FormatYmd(){
        $sql = "SELECT *,TIMESTAMPDIFF(hour, CONCAT(standby_date,' ',time_start), DATE_ADD(CONCAT(standby_date,' ',time_end),INTERVAL 1 minute ) ) expect_duration,
            TIME_FORMAT(time_start,'%H') start_h
            FROM standby_night_ace WHERE DATE_FORMAT(standby_date,'%Y-%m-%d') = :day AND (email = :email)";
        $q = $this->db->prepare($sql);
        $q->execute(array(':day'=>$this->day,':email'=>$this->engineer));
        $r = $q->fetchAll();
        return $r;
    }

    private function listStandby7x24(){
        $sql = "SELECT *,TIMESTAMPDIFF(hour, CONCAT(standby_date,' ',time_start), DATE_ADD(CONCAT(standby_date,' ',time_end),INTERVAL 1 minute ) ) expect_duration,
            TIME_FORMAT(time_start,'%H') start_h
            FROM standby_night_ace WHERE DATE_FORMAT(standby_date,'%d-%m-%Y') = :day AND (email = :email)";
        $q = $this->db->prepare($sql);
        $q->execute(array(':day'=>$this->day,':email'=>$this->engineer));
        $r = $q->fetchAll();
        return $r;
    }
    private function otherProducts(){
        $sql = "SELECT *,TIMESTAMPDIFF(hour, CONCAT(standby_date,' ',time_start), DATE_ADD(CONCAT(standby_date,' ',time_end),INTERVAL 1 minute) ) expect_duration,
            TIME_FORMAT(time_start,'%H') start_h
            FROM standby_oncall_other_products WHERE DATE_FORMAT(standby_date,'%d-%m-%Y') = :day AND (email = :email)";
        $q = $this->db->prepare($sql);
        $q->execute(array(':day'=>$this->day,':email'=>$this->engineer));
        $r = $q->fetchAll();
        return $r;
    }

    public function getConsultAce($email, $day){
        $dayExplode = explode("-", $day);
        $date = $dayExplode["2"]."-".$dayExplode["1"]."-".$dayExplode["0"];
        $this->day = $date;
        $this->engineer = $email;
        $data = $this->listStandbyConsultAce();
        if(is_array($data))
            return $data;
        else
            return array();
    }
     private function listStandbyConsultAce(){
        $sql = "SELECT *,TIMESTAMPDIFF(hour, CONCAT(standby_date,' ',time_start), CONCAT(standby_date,' ',time_end)) expect_duration,
            TIME_FORMAT(time_start,'%h') start_h
            FROM standby_7x24 WHERE DATE_FORMAT(standby_date,'%d-%m-%Y') = :day AND (consult_ace_email  = :email)";
        $q = $this->db->prepare($sql);
        $q->execute(array(':day'=>$this->day,':email'=>$this->engineer));
        $r = $q->fetch();
        return $r;
    }
    private function listStandbyAce(){
        $sql = "SELECT *,TIMESTAMPDIFF(hour, CONCAT(standby_date,' ',time_start), CONCAT(standby_date,' ',time_end)) expect_duration,
            TIME_FORMAT(time_start,'%h') start_h
            FROM standby_7x24 WHERE DATE_FORMAT(standby_date,'%d-%m-%Y') = :day AND (ace_1_email = :email OR ace_2_email = :email OR ace_3_email = :email OR ace_4_email = :email OR ace_5_email = :email)";
        $q = $this->db->prepare($sql);
        $q->execute(array(':day'=>$this->day,':email'=>$this->engineer));
        $r = $q->fetch();
        return $r;
    }
     private function listBusyEmployeeByDay(){
        $busyDate = $this->day;

         $sql = "SELECT *,BE.id as BEID, TIME_FORMAT(busy_start_time,'%H') start_h,
            TIMESTAMPDIFF(hour, CONCAT(date_busy,' ',busy_start_time), CONCAT(date_busy,' ',busy_end_time)) expect_duration FROM " . $this->tbl_busy_employee . " AS BE "
                . "LEFT JOIN " . $this->tbl_busy_mlookup . " AS BM ON BE.type_busy_id = BM.id "
                . "WHERE BE.data_status = 'Y' AND DATE_FORMAT(BE.date_busy,'%d-%m-%Y') = '" . $busyDate . "' "
                . "AND BE.email = '" . $this->engineer . "' "
                . "AND ((BE.approve = 'Y') OR (BE.approve = 'W') OR BE.approve = '' OR BE.approve IS NULL) ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        return $r;
    }

    public function whereEmployee(){

        $listEngineer = $this->listEngineer();
        foreach ($listEngineer as $key => $value) {
            $listEngineer[$key]['where'] = $this->positionEmployee($value['email']);
        }
        return $listEngineer;
    }

    private function positionEmployee($email){
        $sql = "SELECT latitude,longitude,create_datetime FROM tasks_log WHERE create_by = :email AND (latitude !='' AND longitude != '') ORDER BY sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));

        return $q->fetch();
    }

    private function getEmpno(){
        $sql = "SELECT empno FROM employee WHERE emailaddr = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$this->engineer));
        $r = $q->fetch();
        return $r['empno'];
    }
    public function leaveAndBussyList($email, $start, $end){
        $this->engineer = $email;
        $this->employeeID = $this->getEmpno();
        $this->start = $start;
        $this->end = $end;
        $taskOld = $this->taskOldFromToEnd();
        $leave = $this->listBusyEmployeeByDayFromToEnd();
        return array('oldTask'=>$taskOld,'leave'=>$leave);
    }

    public function engineer($team = ""){
        return $this->listEngineer($team);
    }

    private function listEngineer($team = ""){
    	$sql = "SELECT UR.email,E.engname,E.thainame,E.empno, R.role_name_view role_name, R.role_name_view,E.maxportraitfile pic_engineer,
                 CONCAT( SUBSTR( REPLACE( E.mobile,  '-',  '' ) , 1, 3 ) ,  '-', SUBSTR( REPLACE( E.mobile,  '-',  '' ) , 4, 3 ) ,  '-', SUBSTR( REPLACE( E.mobile,  '-',  '' ) , 7, 4 ) ) mobile,
                (SELECT COUNT(DISTINCT S.no) FROM tbl_service_report S
                    LEFT JOIN tbl_associate_engineer A ON S.no = A.service_report_no
                    WHERE (S.engineer_id = E.empno OR A.engineer_id = E.empno)
                    AND (DATE_FORMAT(S.appointment_time,'%m') = DATE_FORMAT(NOW(),'%m')) AND (DATE_FORMAT(S.appointment_time,'%Y') = DATE_FORMAT(NOW(),'%Y'))
                    AND S.status >= 2) AS number_sr_this_month,
                (SELECT COUNT(DISTINCT(T.appointment)) FROM tasks_log T WHERE UR.email = T.engineer
                    AND (DATE_FORMAT(T.appointment,'%m') = DATE_FORMAT(NOW(),'%m')) AND (DATE_FORMAT(T.appointment,'%Y') = DATE_FORMAT(NOW(),'%Y'))
                    AND (SELECT status FROM tasks_log WHERE tasks_sid = T.tasks_sid AND engineer = T.engineer
                        ORDER BY sid DESC LIMIT 0,1) >= 0) AS number_task_this_month
    			FROM user_role UR
    				LEFT JOIN role R ON UR.role_sid = R.sid
    				LEFT JOIN employee E ON UR.email = E.emailaddr
    			WHERE 1
    			AND UR.data_status > 0 ";
        if($team==""){
            $sql .= "AND ((R.sid > 7 AND R.sid < 13) OR (R.sid = 5) OR (R.sid = 1))";
        }else{
            $sql .= "AND R.sid IN (".$team.") ";
        }

        $sql .= "ORDER BY R.name ASC, E.thainame ASC   ";

    	$q = $this->db->prepare($sql);
    	$q->execute();
    	return $r = $q->fetchAll();
    }
// OR DATE_FORMAT(T.expect_finish,'%d-%m-%Y') = :day
    private function tasks(){
    	$sql = "SELECT T.appointment,T.create_by, DATE_FORMAT(T.appointment,'%H') appointment_h,
                DATE_FORMAT(T.appointment,'%d-%m-%Y') appointment_date_df,
                DATE_FORMAT(T.appointment,'%d/%m/%y %H:%i') appointment_view,
                T.expect_duration,
                (SELECT COUNT(sid) FROM ".$this->dbObj->table_ticket_to_do_pm." WHERE ticket_sid = TicL.sid ) number_server,
    			(SELECT status FROM ".$this->dbObj->table_tasks_log." WHERE tasks_sid = TL.tasks_sid AND engineer = TL.engineer ORDER BY sid DESC LIMIT 0,1 ) status,
    			T.no_task,
    			TicL.subject ticket_subject, TicL.end_user_company_name ticket_end_user_company_name, TicL.end_user_site ticket_end_user_site,
    			TicL.end_user_contact_name ticket_end_user_contact_name, TicL.end_user_phone ticket_end_user_phone, TicL.end_user_email ticket_end_user_email,
                TicL.case_type case_type, T.service_type, ST.icon,
                T.sid tasks_sid, TicL.sid ticket_sid,TicL.no_ticket
    			FROM ".$this->dbObj->table_tasks_log." TL
    			INNER JOIN ".$this->dbObj->table_tasks." T ON TL.tasks_sid = T.sid
    			LEFT JOIN ".$this->dbObj->table_ticket." TicL ON TicL.sid = T.ticket_sid
                LEFT JOIN service_type ST ON T.service_type = ST.sid
    			WHERE 1
    			AND (T.appointment LIKE '".$this->viewDateYMD."%' OR T.expect_finish LIKE '".$this->viewDateYMD."%' )
    			AND TL.engineer = :engineer
    			AND
    			(SELECT SubTL.status FROM ".$this->dbObj->table_tasks_log." SubTL WHERE SubTL.tasks_sid = TL.tasks_sid AND SubTL.engineer = TL.engineer ORDER BY sid DESC LIMIT 0,1 ) >= 0
    			GROUP BY TL.tasks_sid
    			";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(
    		':engineer'=>$this->engineer
    		));
    	$r = $q->fetchAll();
    	return $r;
    }

    private function taskOld(){
    	$date = explode("-", $this->day);

    	$sql = "SELECT DISTINCT(SR.no),DATE_FORMAT(NOW(),'%H %i %d %m %Y') AS now, "
                . "IF(NOW()>DATE_ADD(SR.appointment_time,INTERVAL hour(SR.expect_duration) hour) && SR.close_job_time IS NULL, 'Not close job','closed') critical,"
                . "SR.appointment_time appointment, DATE_FORMAT(SR.appointment_time,'%H') appointment_h,
                DATE_FORMAT(SR.appointment_time,'%d') appointment_d,
                TIME_FORMAT(SR.expect_duration,'%H') number_hour, "
                . "SR.expect_duration,SR.suggestion, "
                . "SR.customer_company_id customer_company_id,TBLCC.name_en customer_company_name,"
                . "SR.additive_mail,SR.no as SRID,SR.engineer_id as engineer_id,"
                . "CASE SR.status WHEN '0' THEN 'Service Report ใหม่' "
                . "WHEN '1' THEN 'กำลังรออนุมัติ OT' "
                . "WHEN '2' THEN 'ได้รับมอบหมาย (พร้อมดาวน์โหลด)' "
                . "WHEN '3' THEN 'ยอมรับ' "
                . "WHEN '4' THEN 'กำลังเดินทางไปทำงาน' "
                . "WHEN '5' THEN 'อยู่ที่ site' "
                . "WHEN '6' THEN 'กำลังทำงาน' "
                . "WHEN '7' THEN 'ปิดงาน รอลูกค้าเซ็น' "
                . "WHEN '8' THEN 'Finished' "
                . "WHEN '9' THEN 'Completed' END caseStatus,"
                . "SR.status AS status, "
                . "SR.close_job_time AS closeJobTime,TS.service_step_no step, TS.time ts_start_time, "
                . "DATE_FORMAT(TS.time,'%d/%m/%y %H:%i') tooltip_start_time, "
                . "DATE_FORMAT(DATE_ADD(DATE_ADD(SR.appointment_time,INTERVAL hour(SR.expect_duration) hour),INTERVAL minute(SR.expect_duration) minute),'%d/%m/%y %H:%i') tooltip_expect_time, "
                . "DATE_FORMAT(SR.appointment_time,'%d/%m/%y %H:%i') tooltip_appointment_time,"
                . "DATE_FORMAT(SR.close_job_time,'%d/%m/%y %H:%i') tooltip_close_job_time "
                . "FROM " . $this->tbl_service_report . " AS SR "
                . "LEFT JOIN " . $this->tbl_associate_engineer . " AS AE "
                . "ON SR.no = AE.service_report_no "
                . "LEFT JOIN tbl_service_timestamp TS ON TS.service_report_no = SR.no AND (TS.service_step_no = '5' OR TS.service_step_no = '4' )
                LEFT JOIN tbl_customer_company TBLCC ON .SR.customer_company_id = TBLCC.id "
                . "WHERE "
                . "(DATE_FORMAT(SR.appointment_time,'%Y%m%d') <= '" . ($date[2] . "" . $date[1] . "" . $date[0]) . "' "
                . "AND "
                . "(DATE_FORMAT(DATE_ADD(DATE_ADD(SR.appointment_time,INTERVAL hour(SR.expect_duration) hour),INTERVAL minute(SR.expect_duration) minute),'%Y%m%d') >= '" . ($date[2] . "" . $date[1] . "" . $date[0]) . "' "
                . "OR DATE_FORMAT(SR.close_job_time,'%Y%m%d') >= '" . ($date[2] . "" . $date[1] . "" . $date[0]) . "' )"
                . ") "
                . "AND "
//                . "MONTH(SR.appointment_time) = '" . $date[1] . "' AND "
//                . "YEAR(SR.appointment_time) = '" . $date[2] . "') AND "
                . "(SR.engineer_id = '" . $this->employeeID . "' OR AE.engineer_id = '" . $this->employeeID . "' ) "
                . "AND SR.status > 0 "
                . "AND SR.is_cancel = 0 "
                . "ORDER BY SR.appointment_time";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        return $r;
    }

    private function taskOldFromToEnd(){
        // $date = explode("-", $this->day);

        $sql = "SELECT DISTINCT(SR.no),DATE_FORMAT(NOW(),'%H %i %d %m %Y') AS now,SR.contract_no contract_no, "
                . "IF(NOW()>DATE_ADD(SR.appointment_time,INTERVAL hour(SR.expect_duration) hour) && SR.close_job_time IS NULL, 'Not close job','closed') critical,"
                . "SR.appointment_time appointment, DATE_FORMAT(SR.appointment_time,'%H') appointment_h,
                DATE_FORMAT(SR.appointment_time,'%d') appointment_d,SR.symtom subject,
                TIME_FORMAT(SR.expect_duration,'%H') number_hour, "
                . "SR.expect_duration,SR.suggestion,SR.request_no, "
                . "SR.customer_company_id customer_company_id,TBLCC.name_en customer_company_name,"
                . "SR.additive_mail,SR.no as SRID,SR.engineer_id as engineer_id,"
                . "CASE SR.status WHEN '0' THEN 'Service Report ใหม่' "
                . "WHEN '1' THEN 'กำลังรออนุมัติ OT' "
                . "WHEN '2' THEN 'ได้รับมอบหมาย (พร้อมดาวน์โหลด)' "
                . "WHEN '3' THEN 'ยอมรับ' "
                . "WHEN '4' THEN 'กำลังเดินทางไปทำงาน' "
                . "WHEN '5' THEN 'อยู่ที่ site' "
                . "WHEN '6' THEN 'กำลังทำงาน' "
                . "WHEN '7' THEN 'ปิดงาน รอลูกค้าเซ็น' "
                . "WHEN '8' THEN 'Finished' "
                . "WHEN '9' THEN 'Completed' END caseStatus,"
                . "SR.status AS status, "
                . "SR.close_job_time AS closeJobTime,TS.service_step_no step, TS.time ts_start_time, "
                . "DATE_FORMAT(TS.time,'%d/%m/%y %H:%i') tooltip_start_time, "
                . "DATE_FORMAT(DATE_ADD(DATE_ADD(SR.appointment_time,INTERVAL hour(SR.expect_duration) hour),INTERVAL minute(SR.expect_duration) minute),'%d/%m/%y %H:%i') tooltip_expect_time, "
                . "DATE_FORMAT(SR.appointment_time,'%d/%m/%y %H:%i') tooltip_appointment_time,"
                . "DATE_FORMAT(SR.close_job_time,'%d/%m/%y %H:%i') tooltip_close_job_time "
                . "FROM " . $this->tbl_service_report . " AS SR "
                . "LEFT JOIN " . $this->tbl_associate_engineer . " AS AE "
                . "ON SR.no = AE.service_report_no "
                . "LEFT JOIN tbl_service_timestamp TS ON TS.service_report_no = SR.no AND (TS.service_step_no = '5' OR TS.service_step_no = '4' )
                LEFT JOIN tbl_customer_company TBLCC ON .SR.customer_company_id = TBLCC.id "
                . "WHERE "
                . "DATE_FORMAT(SR.appointment_time,'%Y-%m-%d') = '" . $this->start . "' "
                // . "AND SR.appointment_time <= '".$this->end."' "
//                . "MONTH(SR.appointment_time) = '" . $date[1] . "' AND "
//                . "YEAR(SR.appointment_time) = '" . $date[2] . "') AND "
                . "AND (SR.engineer_id = '" . $this->employeeID . "' OR AE.engineer_id = '" . $this->employeeID . "' ) "
                . "AND SR.status > 0 "
                . "AND SR.is_cancel = 0 "
                . "ORDER BY SR.appointment_time";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        return $r;
    }



    private function listBusyEmployeeByDayFromToEnd(){
        $busyDate = $this->day;

         $sql = "SELECT *,BE.id as BEID, DATE_FORMAT(CONCAT(BE.date_busy,' ',BE.busy_start_time),'%H') appointment_time,
                TIMESTAMPDIFF(HOUR,CONCAT(BE.date_busy,' ',BE.busy_start_time), CONCAT(BE.date_busy,' ',BE.busy_end_time)) expect_duration,
                  DATE_FORMAT(CONCAT(BE.date_busy,' ',BE.busy_start_time),'%d.%m.%Y %H:%i') starttime,
                  DATE_FORMAT(CONCAT(BE.date_busy,' ',BE.busy_end_time),'%d.%m.%Y %H:%i') endtime
                 FROM " . $this->tbl_busy_employee . " AS BE "
                . "LEFT JOIN " . $this->tbl_busy_mlookup . " AS BM ON BE.type_busy_id = BM.id "
                . "WHERE BE.data_status = 'Y' AND DATE_FORMAT(BE.date_busy,'%Y-%m-%d') >= '" . $this->start . "' AND  DATE_FORMAT(BE.date_busy,'%Y-%m-%d') <= '".$this->end."' "
                . "AND BE.email = '" . $this->engineer . "' "
                . "AND ((BE.approve = 'Y') OR (BE.approve = 'W') OR BE.approve = '' OR BE.approve IS NULL) ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        return $r;
    }

    private function implement(){

    }

    private function installation(){

    }

    private function incident(){

    }

    private function busylist7x24(){

    }

    private function busylistACE(){

    }


}
?>
