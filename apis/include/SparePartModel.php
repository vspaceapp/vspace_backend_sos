<?php 
class SparePartModel{
	private $m;
	private $db;
	private $email;

	function __construct($email) {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->email = $email;

        $this->db = $db->connect();
        if($this->email!=""){
        	$this->m->setTable($this->email);
    	}
    }

    public function listOracleSrAll(){
    	$sql = "SELECT TP.*, TT.*,TSP.*,T.no_ticket, T.owner,CS.name case_status,T.end_user,T.create_datetime case_create_datetime 
    	FROM ".$this->m->table_ticket_spare." TP 
    	LEFT JOIN ".$this->m->table_ticket_spare_task." TT ON TT.ticket_spare_sid = TP.sid 
    	LEFT JOIN ".$this->m->table_ticket_spare_part." TSP ON TT.sid = TSP.ticket_spare_task_sid 
    	INNER JOIN ".$this->m->table_ticket." T ON TP.ticket_sid = T.sid 
    	LEFT JOIN case_status CS ON T.status = CS.sid 
    	ORDER BY TP.oracle_create_date DESC";	
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();
    	return $r;
    }
}
?>