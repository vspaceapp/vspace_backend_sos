<?php
function genNoProjectTicket($project_sid, $contract_no, $db){
    $dataTicket = getTicketByProjectSid($project_sid, $contract_no, $db);
    $noProjectTicket = "";
    if($contract_no!="" && $project_sid!="" && $project_sid!="0"){
        foreach ($dataTicket as $k => $v) {
            $no = ($k+1);

            $strlen = strlen($no);
            if($strlen<3){
                for ($i=$strlen; $i < 3; $i++) { 
                    $no = "0".$no;
                }
            }

            $noProjectTicket = $contract_no."-".$no;
        }
    }
    return $noProjectTicket;
}
function getTicketByProjectSid($project_sid, $contract_no, $db){
    $sql = "SELECT sid ticket_sid, contract_no, project_sid, owner,create_by FROM ticket 
        WHERE 1 
        AND via = 'createcase'
        AND contract_no = :contract_no 
        ORDER BY sid ASC";
    $q = $db->prepare($sql);
    $q->execute(array(':contract_no'=>$contract_no));
    return $q->fetchAll();
}

function genTicketNo($db) {
    $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
    $queryY = $db->prepare($sql);
    $queryY->execute();

    $rY = $queryY->fetch(PDO::FETCH_ASSOC);
    $year = $rY['year'];
    $prefix = "HD";
    $sql = "SELECT no_ticket FROM ticket 
    WHERE substring(no_ticket,1,4) = :initF ORDER BY no_ticket DESC LIMIT 0,1 ";
    $query = $db->prepare($sql);
    $query->execute(array(':initF'=>($prefix.$year)));
    $r = $query->fetch(PDO::FETCH_ASSOC);

    if ($r['no_ticket'] != "") {
        $no_ticket = substr($r['no_ticket'], 4);
        $no_ticket++;
    } else {
        $no_ticket = "000001";
    }
    $cLength = strlen($no_ticket);
    for ($i = $cLength; $i < 6; $i++) {
        $no_ticket = "0" . $no_ticket;
    }
    return $prefix .$year. $no_ticket;
}

function genTaskNo($db) {
    $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
    $queryY = $db->prepare($sql);
    $queryY->execute();
    $prefix = "SR";
    $rY = $queryY->fetch(PDO::FETCH_ASSOC);
    $year = $rY['year'];

    $sql = "SELECT no_task FROM tasks 
    WHERE substring(no_task,1,4) = :initF ORDER BY no_task DESC LIMIT 0,1 ";
    $query = $db->prepare($sql);
    $query->execute(array(':initF'=>($prefix.$year)));
    // echo helper::debugPDO($sql, array(':initF'=>'VS'.$year));
    $r = $query->fetch(PDO::FETCH_ASSOC);
        
    if ($r['no_task'] != "") {
        $no_task = substr($r['no_task'], 4);
        $no_task++;
    } else {
        $no_task = "000001";
    }
    $cLength = strlen($no_task);
    for ($i = $cLength; $i < 6; $i++) {
        $no_task = "0" . $no_task;
    }
    return $prefix.$year . $no_task;
}

function genTicketNoGeneral($db) {
    $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
    $queryY = $db->prepare($sql);
    $queryY->execute();
    $prefix = "TK";

    $rY = $queryY->fetch(PDO::FETCH_ASSOC);
    $year = $rY['year'];

    $sql = "SELECT no_ticket FROM gen_ticket 
    WHERE substring(no_ticket,1,4) = :initF ORDER BY no_ticket DESC LIMIT 0,1 ";
    $query = $db->prepare($sql);
    $query->execute(array(':initF'=>($prefix.$year)));
    $r = $query->fetch(PDO::FETCH_ASSOC);

    if ($r['no_ticket'] != "") {
        $no_ticket = substr($r['no_ticket'], 4);
        $no_ticket++;
    } else {
        $no_ticket = "000001";
    }
    $cLength = strlen($no_ticket);
    for ($i = $cLength; $i < 6; $i++) {
        $no_ticket = "0" . $no_ticket;
    }
    return $prefix .$year. $no_ticket;
}

function genTaskNoGeneral($db) {
    $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
    $queryY = $db->prepare($sql);
    $queryY->execute();
    $prefix = "ST";
    $rY = $queryY->fetch(PDO::FETCH_ASSOC);
    $year = $rY['year'];

    $sql = "SELECT no_task FROM gen_tasks 
    WHERE substring(no_task,1,4) = :initF ORDER BY no_task DESC LIMIT 0,1 ";
    $query = $db->prepare($sql);
    $query->execute(array(':initF'=>($prefix.$year)));
    // echo helper::debugPDO($sql, array(':initF'=>'VS'.$year));
    $r = $query->fetch(PDO::FETCH_ASSOC);
        
    if ($r['no_task'] != "") {
        $no_task = substr($r['no_task'], 4);
        $no_task++;
    } else {
        $no_task = "000001";
    }
    $cLength = strlen($no_task);
    for ($i = $cLength; $i < 6; $i++) {
        $no_task = "0" . $no_task;
    }
    return $prefix.$year . $no_task;
}
?>