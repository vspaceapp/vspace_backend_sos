<?php
class OvertimeModel{

	private $db;

	private $type;
	private $email;

	function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }
    public function processTaxifareTicket(){
        $data = $this->selectTicketUnupdateTaxifare();
        echo count($data);
        echo "<br/>";
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        foreach ($data as $key => $value) {
            $tasks = $this->selectTasksByTicketSid($value['ticket_sid']);
            echo $value['ticket_sid'];
            echo "<pre>";
            print_r($tasks);        
            echo "</pre>";
            $totalTaxi = 0;
            foreach ($tasks as $k => $v) {
                $totalTaxi += $v['taxi_fare_total'];
            }
            $this->updateTaxifareTicket($totalTaxi, $value['ticket_sid']);
        }
        
    }
    public function updateTaxifareTicket($taxi_fare_total, $ticket_sid){
        $sql = "UPDATE ticket SET taxi_fare_total = :taxi_fare_total, update_taxi_fare = NOW() WHERE sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':taxi_fare_total'=>$taxi_fare_total,':ticket_sid'=>$ticket_sid));
    }

    public function selectTasksByTicketSid($ticket_sid){
        $sql = "SELECT taxi_fare_total FROM tasks WHERE ticket_sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    public function selectTicketUnupdateTaxifare(){
        $sql = "SELECT T.sid ticket_sid FROM ticket T 
        WHERE T.update_taxi_fare = '0000-00-00 00:00:00' 
        ORDER BY T.sid DESC LIMIT 0,10000";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function processTaxifare($prefix){
        $data = $this->selectServiceReportUnupdateTaxifare($prefix);
        foreach ($data as $key => $value) {
            $taxi_fare_total = $this->calTaxifareTotalByTaskSid($prefix, $value['sid']);
            $this->updateTaxifareByTaskSid($prefix, $value['sid'],$taxi_fare_total);
        }
    }

    private function updateTaxifareByTaskSid($prefix, $tasks_sid, $taxi_fare_total){
        $sql = "UPDATE ".$prefix."tasks SET taxi_fare_total = :taxi_fare_total, updated_taxi_fare = NOW() WHERE sid = :tasks_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':taxi_fare_total'=>$taxi_fare_total,':tasks_sid'=>$tasks_sid));

    }

    private function calTaxifareTotalByTaskSid($prefix, $tasks_sid){
        $taxi_fare_total = 0;
        $data = $this->selectTimestampServiceReportByTaskSid($prefix, $tasks_sid);
        foreach ($data as $key => $value) {
            $taxi_fare_total += $value['taxi_fare'];
        }
        return $taxi_fare_total;
    }
    private function selectTimestampServiceReportByTaskSid($prefix, $tasks_sid){
        $sql = "SELECT * FROM ".$prefix."tasks_log WHERE tasks_sid = :tasks_sid AND (status = '300' OR status = '600') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetchAll();
        return $r;
    }
    private function selectServiceReportUnupdateTaxifare($prefix){
        $sql = "SELECT * FROM ".$prefix."tasks WHERE updated_datetime >= updated_taxi_fare LIMIT 0,30";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    public function processOvertime($prefix){
    	$data = $this->selectServiceReport($prefix);

    	foreach ($data as $key => $value) {
    		$res = $this->calNumberMinuteOt($value, "expect");
    		$this->updateOvertimeExpect($prefix,$value,$res['numberMinuteOT']);
    	}
    }

    private function updateOvertimeExpect($prefix,$data, $numberMinuteOT){
    	$sql = "UPDATE ".$prefix."tasks SET overtime_expect = :overtime, overtime_expect_updated = NOW() WHERE sid = :sid ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':overtime'=>$numberMinuteOT,':sid'=>$data['sid']));
    }

    public function selectServiceReport($prefix){
    	$sql = "SELECT * FROM ".$prefix."tasks WHERE updated_datetime >= overtime_expect_updated LIMIT 0,30";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetchAll();
    	return $r;
    }

	public function calNumberMinuteOt($data,$type=""){
		// foreach ($data as $k => $v) {
            $numberMinuteOT = 0;
            if($type=="expect"){
                $minute = $this->countHour($data['appointment'], $data['expect_finish']);
            }else{
                $minute = $this->countHour($this->decreaseTime($data['appointment']), $this->increaseTime($data['expect_finish']));
            }
            for ($i = 0; $i < $minute; $i+=15) {
                $sql = "SELECT DATE_ADD('" . $data['appointment'] . "',INTERVAL " . $i . " minute) minuteWork";
                // $r = Yii::app()->db->createCommand($sql)->queryRow();
                $q = $this->db->prepare($sql);
                $q->execute();
                $r = $q->fetch();
                if ($this->checkHourIsOT($r['minuteWork']) == 'OT') {
                    $numberMinuteOT++;
                }
            }
			$numberMinuteOT/=4;
			$data['numberMinuteOT'] = $numberMinuteOT;
 			// $data[$k]['hourOT'] = $numberMinuteOT; // ยังไม่ได้คูณกับกับ จำนวน engineer
        	// $data[$k]['numberHourOT'] = $numberMinuteOT * ($v['numberAssoEngineerPerSR'] + $v['numberEngineerPerSR']);
    	// }
        return array('data'=>$data,'numberMinuteOT'=>$numberMinuteOT);
    }

    private function checkHourIsOT($dateTime) {
        if ($this->isHoliday($dateTime)) {
            return "OT";
        }
        if ($this->isDurationTimeWork($dateTime)) {
            return "OT";
        }
        return "NotOT";
    }

    private function countHour($start, $end) {
        $sql = "SELECT TIMESTAMPDIFF(minute,'" . $start . "','" . $end . "') minute ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r['minute'];
    }

    private function isDurationTimeWork($date) {// ตรวจสอบว่าเป็นเวลา ทำงานโอทีไหม
        $date = explode(" ", $date);
        $time = explode(":", $date[1]);
        if ($time[0] >= 8 && $time[0] < 17) {
            return FALSE;
        }
        return TRUE;
    }

    private function isHoliday($date) {
        $day = explode(" ", $date);
        $sql = "SELECT id FROM tbl_holiday WHERE holiday = '" . $day[0] . "'";
        // $r = Yii::app()->db->createCommand($sql)->queryRow();
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();

        if (!empty($r['id'])) {
            return TRUE;
        }
        $day = explode("-", $day[0]);
        $r = date("l", mktime(0, 0, 0, $day[1], $day[2], $day[0]));
        if ($r == 'Saturday' || $r == 'Sunday') {
            return TRUE;
        }
        return FALSE;
    }

    private function decreaseTime($date) {
        $sql = "SELECT MINUTE('" . $date . "') minute";
        // $r = Yii::app()->db->createCommand($sql)->queryRow();
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();

        if ($r['minute'] >= 45) {
            $decrease = $r['minute'] - 45;
        } else if ($r['minute'] > 30) {
            $decrease = $r['minute'] - 30;
        } else if ($r['minute'] > 15) {
            $decrease = $r['minute'] - 15;
        } else if ($r['minute'] > 0) {
            $decrease = $r['minute'] - 0;
        } else {
            $decrease = 0;
        }
        $sql = "SELECT DATE_SUB('" . $date . "',INTERVAL " . $decrease . " MINUTE) result";
        // $result = Yii::app()->db->createCommand($sql)->queryRow();
        $q = $this->db->prepare($sql);
        $q->execute();
        $result = $q->fetch();
        return $result['result'];
    }

    private function increaseTime($date) {
        $sql = "SELECT MINUTE('" . $date . "') minute";
        // $r = Yii::app()->db->createCommand($sql)->queryRow();
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();    
        if ($r['minute'] >= 45) {
            $increase = 60 - $r['minute'];
        } else if ($r['minute'] > 30) {
            $increase = 45 - $r['minute'];
        } else if ($r['minute'] > 15) {
            $increase = 30 - $r['minute'];
        } else if ($r['minute'] > 0) {
            $increase = 15 - $r['minute'];
        } else {
            $increase = 0;
        }
        $sql = "SELECT DATE_ADD('" . $date . "',INTERVAL " . $increase . " MINUTE) result";
        // $result = Yii::app()->db->createCommand($sql)->queryRow();
        $q = $this->db->prepare($sql);
        $q->execute();
        $result = $q->fetch();
        return $result['result'];
    }

}
?>