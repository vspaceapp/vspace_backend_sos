<?php
require_once __dir__."/../core/constant.php";
require_once __dir__."/SlaModel.php";
class CaseModel{
    private $db;
    public $due_datetime, $incident_location, $incident_address, $product_categorization_tier_1,
    $product_categorization_tier_2, $product_categorization_tier_3, $resolution_product_category_tier_1,
    $resolution_product_category_tier_2, $resolution_product_category_tier_3, $request_type, $number_of_device, $team_id, $additional_status;
    public $project,
    $appointment_time,
    $appointment_date,
    $engineer,
    $subject,
    $description,
    $end_user_company_name,
    $end_user_site,
    $end_user_contact_name,
    $end_user_phone,
    $end_user_mobile,
    $end_user_email,
    $expect_time,
    $email,
    $type_service,
    $send_to_ccd;
    public $end_user = "";
    private $service_address = "";

    private $appointment_datetime;
    private $project_name;
    private $customer;

    public $serial_no;

    public $via;

    public $caseNo;
    private $srNo;
    public $ticket_sid;
    private $task_sid;

    public $contract_no;
    private $prime_contract = "";

    private $status;
    public $team;

    public $owner;

    public $case_type;
    private $source;
    public $note = "";
    private $urgency = "";

    private $requester_full_name = "",
    $requester_company_name = "",
    $requester_phone = "",
    $requester_mobile = "",
    $requester_email = ""
    ;

    private $expect_finish = "";

    public $receive_mail_datetime,
    $refer_remedy_hd,
    $refer_remedy_assigned_time,
    $refer_remedy_create_time,
    $refer_remedy_report_by;

    //Implement 17/6/2016 9:41 By Mas
    public $case_company = "";

    public $case_status_sid = "";
    public $work_log = "";

    private $subject_service_report;
    private $end_user_contact_name_service_report;
    private $end_user_phone_service_report;
    private $end_user_mobile_service_report;
    private $end_user_email_service_report;
    private $end_user_company_name_service_report;

    private $role;
    private $emailForRule;

    private $sparepart_sid;
    private $part_number_defective;
    private $part_serial_defective;
    private $part_number;
    private $part_serial;
    private $quantity;

    private $datetime;

    public $expect_pending = "0000-00-00 00:00:00";

    private $is_follow_up = "0";

    public $solution = "";
    private $solution_detail = "";
    public $status_reason = "";
    private $objCons;
    public $data_from = ""; // THIS IS field OF TABLE TICKET_STATUS

    private $table_ticket = "ticket";
    private $table_ticket_log = "ticket_log";
    private $table_ticket_status = "ticket_status";
    private $table_ticket_owner = "ticket_owner";
    private $table_ticket_serial = "ticket_serial";
    private $table_ticket_to_do_pm = "ticket_to_do_pm";

    private $table_tasks = "tasks";
    private $table_tasks_log = "tasks_log";
    private $table_task_do_serial_pm = "task_do_serial_pm";
    private $table_task_followup_log = "task_followup_log";
    private $table_task_input_action = "task_input_action";
    private $table_task_input_request = "task_input_request";
    private $table_task_spare = "task_spare";

    private $sr = "";
    private $task = "";
    private $type = "";
    private $oracle_create_date = "";
    private $request_spare_part_date = "";
    private $delivery_due_date = "";
    private $receive_spare_part_date = "";
    private $site_area = "";

    private $model = "";
    // SEE ME IN LINE 82 private $part_number= "";
    private $part_description= "";

    private $ticket_spare_sid;
    private $ticket_spare_task_sid;
    private $ticket_spare_part_sid;

    private $can_create_new_sr;
    private $can_change_status;

    private $staff;
    public $permission_role;
    public $noProjectTicket = "";
    private $man_hours = 0;
    public $is_return_remedy = 1;

    function __construct($email="", $refer_remedy_hd="", $caseNo="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        require_once __DIR__."/../libs/simple_html_dom.php";
        // opening db connection
        $this->objCons = new DbConnect();
        $this->db = $this->objCons->connect();
        $this->email = $email;
        $this->refer_remedy_hd = $refer_remedy_hd;
        $this->caseNo = $caseNo;
        if($this->email!=""){
            $this->permission_role = $this->objCons->permissionRoleByEmail($this->email);
        }
    }

    private $project_owner_sid = "";
    private $project_sid = "";
    private function findProjectSid($project_owner_sid){
        $sql = "SELECT project_sid FROM project_owner WHERE sid = :project_owner_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_owner_sid'=>$project_owner_sid));
        $r = $q->fetch();
        if(isset($r['project_sid']) && $r['project_sid']>0 ){
            return $r['project_sid'];
        }else{
            return "0";
        }
    }

    public function getTotalServiceReport(){
        $sql = "SELECT number_sr as total_sr FROM ticket where sid = :sid limit 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':sid'=>$this->ticket_sid
        ));
        $r = $q->fetch();
        return $r['total_sr'];
    }

    public function checkCanResolveTicket(){
        $total_sr = $this->getTotalServiceReport();
        $number_sr_done = $this->getNumberServiceReportDone();
        if($total_sr <=0 || $total_sr != $number_sr_done){
            return 0;
        }else{
            return 1;
        }
    }

    public function getNumberServiceReportDone(){
        $sql = "SELECT count(ticket_sid) as number_sr_done FROM tasks WHERE ticket_sid = :ticket_sid and file_name_signated != '' and file_name_signated is not NULL";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid
        ));
        $r = $q->fetch();
        return $r['number_sr_done'];
    }

    function updateOwner($ticket_sid, $new_owner, $submitter){
        $sql = "SELECT R.name from user_role UR
            left join role R on UR.role_sid = R.sid
            where UR.email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':email'=> $new_owner
        ));
        $r = $q->fetch();

        $sql = "UPDATE ".$this->table_ticket." SET owner = :owner, team = :team WHERE sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':owner'=>$new_owner,
            ':team'=>$r['name'],
            ':ticket_sid'=>$ticket_sid
        ));


        return $q->rowCount();
    }

    public function getTicketStatusDetailFromSid($ticket_status_sid){
        $sql = "SELECT ticket_sid, path_file_attach, original_file_name FROM ticket_status where sid = :ticket_status_sid";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(':ticket_status_sid'=>$ticket_status_sid)
        );
        return $q->fetch();
    }

    function getDefaultOwnerByRoleSid($role_sid){
        try{
            $sql = "SELECT U.email as owner, R.name as team FROM user U 
                JOIN role R ON R.sid = U.role_sid
                WHERE R.sid = :sid and U.user_type = 1
                ORDER BY U.sid ASC limit 0,1;";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$role_sid));
            $r = $q->fetch();
            return $r;
        }catch (Exception $e){
            return null;
        }
    }

    public function isDuplicateTicket(){
        $sql = "SELECT count(sid) as count_duplicate FROM ticket where refer_remedy_hd = :refer_remedy_hd";
        $q = $this->db->prepare($sql);
        $q->execute(array(":refer_remedy_hd"=>$this->refer_remedy_hd));
        $r = $q->fetch();
        return $r['count_duplicate']>0;
    }

    // new create ticket method
    public function insertRequestTicket(){
        try{
            $this->db->beginTransaction();
            $this->caseNo = $this->genNumberTicket($this->db);
            $this->case_status_sid = 1;
            $sql = "INSERT INTO ticket 
                (
                    no_ticket,
                    refer_remedy_hd,
                    no_project_ticket,
                    owner,
                    status,
                    create_datetime,
                    update_datetime,
                    project_sid,
                    project_owner_sid,
                    via,
                    create_by,
                    subject,
                    due_datetime,
                    description,
                    contract_no,
                    serial_no,
                    team,
                    end_user_company_name,
                    end_user,
                    end_user_site,
                    end_user_contact_name,
                    end_user_phone,
                    end_user_mobile,
                    end_user_email,
                    case_type,
                    case_company,
                    note,
                    incident_location,
                    incident_address,
                    product_categorization_tier_1,
                    product_categorization_tier_2,
                    product_categorization_tier_3,
                    request_type,
                    resolution_product_category_tier_1,
                    resolution_product_category_tier_2,
                    resolution_product_category_tier_3,
                    number_of_device
                ) 
                VALUES (
                        :no_ticket,
                        :refer_remedy_hd,
                        :no_project_ticket,
                        :owner,
                        :status,
                        NOW(),
                        NOW(),
                        :project_sid,
                        :project_owner_sid,
                        :via,
                        :create_by,
                        :subject,
                        :due_datetime,
                        :description,
                        :contract_no,
                        :serial_no,
                        :team,
                        :end_user_company_name,
                        :end_user,
                        :end_user_site,
                        :end_user_contact_name,
                        :end_user_phone,
                        :end_user_mobile,
                        :end_user_email,
                        :case_type,
                        :case_company,
                        :note,
                        :incident_location,
                        :incident_address,
                        :product_categorization_tier_1,
                        :product_categorization_tier_2,
                        :product_categorization_tier_3,
                        :request_type,
                        :resolution_product_category_tier_1,
                        :resolution_product_category_tier_2,
                        :resolution_product_category_tier_3,
                        :number_of_device
                )";
            $q = $this->db->prepare($sql);
            $r = $q->execute(
                array(
                    ":no_ticket"=>$this->caseNo,
                    ":refer_remedy_hd"=>$this->refer_remedy_hd,
                    ":no_project_ticket"=>$this->noProjectTicket,
                    ":owner"=>$this->owner,
                    ":status"=>$this->case_status_sid,
                    ":project_sid"=>$this->project_sid,
                    ":project_owner_sid"=>$this->project_owner_sid,
                    ":via"=>$this->via,
                    ":create_by"=> '',
                    ":subject"=>$this->subject,
                    ":due_datetime"=>$this->due_datetime,
                    ":description"=>$this->description,
                    ":contract_no"=>$this->contract_no,
                    ":serial_no"=>$this->serial_no,
                    ":team"=>$this->team,
                    ":end_user_company_name"=>$this->end_user_company_name,
                    ":end_user"=>$this->end_user,
                    ":end_user_site"=>$this->end_user_site,
                    ":end_user_contact_name"=>$this->end_user_contact_name,
                    ":end_user_phone"=>$this->end_user_phone,
                    ":end_user_mobile"=>$this->end_user_mobile,
                    ":end_user_email"=>$this->end_user_email,
                    ":case_type"=>$this->case_type,
                    ":case_company"=>$this->case_company,
                    ":note"=>$this->note,
                    ":incident_location"=>$this->incident_location,
                    ":incident_address"=>$this->incident_address,
                    ":product_categorization_tier_1"=>$this->product_categorization_tier_1,
                    ":product_categorization_tier_2"=>$this->product_categorization_tier_2,
                    ":product_categorization_tier_3"=>$this->product_categorization_tier_3,
                    ":request_type"=>$this->request_type,
                    ":resolution_product_category_tier_1"=>$this->resolution_product_category_tier_1,
                    ":resolution_product_category_tier_2"=>$this->resolution_product_category_tier_2,
                    ":resolution_product_category_tier_3"=>$this->resolution_product_category_tier_3,
                    ":number_of_device"=>$this->number_of_device
                )
            );
            $this->db->commit();
            return $r;
        }catch(Exception $e){
            $this->db->rollBack();
            return false;
        }

    }


    // old create ticket method
    function receiveCasemanagementCreateCases($data, $email){
        $this->status = "1";

        $this->caseNo = $this->caseNo;
        $this->via = "createcase";
        $this->email = $email;

        // $this->objCons->setTable($this->email);
        $this->owner = isset($data['owner']['email'])?$data['owner']['email']:$email;

        $this->subject = trim(addslashes(isset($data['subject'])?$data['subject']:""));
        $this->due_datetime = "";
        $this->description = trim(addslashes(isset($data['detail'])?$data['detail']:""));
        $this->contract_no = trim(isset($data['contract_no'])?$data['contract_no']:"");
        $this->prime_contract = trim(isset($data['prime_contract'])?$data['prime_contract']:"");
        $this->serial_no = trim(isset($data['serial_no'])?$data['serial_no']:"");

        $this->team = $this->getTeamByEmail($this->owner);
        $this->requester_full_name = isset($data['requester']['name'])?$data['requester']['name']:"";
        $this->requester_phone = isset($data['requester']['phone'])?$data['requester']['phone']:"";
        $this->requester_mobile = isset($data['requester']['mobile'])?$data['requester']['mobile']:"";
        $this->requester_email = isset($data['requester']['email'])?$data['requester']['email']:"";
        $this->requester_company_name = isset($data['requester']['company'])?$data['requester']['company']:"";

        $this->end_user_contact_name = isset($data['enduser']['name'])?$data['enduser']['name']:"";;
        $this->end_user_phone = isset($data['enduser']['phone'])?$data['enduser']['phone']:"";;
        $this->end_user_email = isset($data['enduser']['email'])?$data['enduser']['email']:"";;
        $this->end_user_mobile = isset($data['enduser']['mobile'])?$data['enduser']['mobile']:"";;
        $this->end_user_company_name = isset($data['enduser']['company'])?$data['enduser']['company']:"";;

        $this->end_user_site = isset($data['enduser_address'])?$data['enduser_address']:"";
        $this->end_user = isset($data['enduser_case'])?$data['enduser_case']:"";

        $this->case_type = isset($data['case_type'])?$data['case_type']:"";
        $this->urgency = isset($data['urgency'])?$data['urgency']:"";
        $this->site_area = isset($data['site_area'])?$data['site_area']:"";

        $this->project_owner_sid = isset($data['project_owner_sid'])?$data['project_owner_sid']:"";
        $this->project_sid = isset($data['project_sid'])?$data['project_sid']:"";

        $this->man_hours = isset($data['man_hours'])?$data['man_hours']:0;
        $this->noProjectTicket = "";//genNoProjectTicket($this->project_sid, $this->contract_no, $this->db);

        $this->insertTicketRemedy();

        $this->insertTicketLog();
        $this->insertSerial();

        // require_once 'SendMail.php';
        // require_once '../../../api/application/core/controller.php';
        // require_once '../../../api/application/controller/sendmail.php';
        // $objsendmail = new SendMail();
        // $objsendmail->noticeEngineerCaseAssigned($this->ticket_sid);
        // $objsendmail->sendToCustomerCaseOpened($this->ticket_sid);
        return array('ticket_sid'=>$this->ticket_sid,'case_no'=>(($this->noProjectTicket)?$this->noProjectTicket:$this->caseNo) );
    }
    function testModel(){
        $sql = "SELECT NOW() now ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r['now'];
    }

    function testCreateCase($subject,$case_type,$email){
        $this->subject = $subject;
        $this->case_type = $case_type;
        $this->email = $email;

        $sql = "INSERT INTO test_create_case (subject, case_type, create_by, create_datetime) VALUES (:subject, :case_type, :create_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':subject'=>$subject,':case_type'=>$case_type,':create_by'=>$email));
    }

    //case address insert to end_user_site
    // receive from createcase
    function executeCreateCase($subject, $description, $case_company, $case_address, $case_type, $urgency, $serial, $contract, $prime_contract, $note,$requester_name,$requester_phone,$requester_email,$requester_mobile,$requester_company,$end_user_name,$end_user_phone,$end_user_email,$end_user_mobile,$end_user_company,$email,$owner=""){

        $this->caseNo = genTicketNo($this->db);
        $this->via = "createcase";
        $this->email = $email;

        if($owner){
            $this->owner = $owner;
        }else{
            $this->owner = $email;
        }
        $this->status = "1";
        $this->subject = trim(addslashes($subject));
        $this->due_datetime = "";
        $this->description = trim(addslashes($description));
        $this->contract_no = trim($contract);
        $this->prime_contract = trim($prime_contract);
        $this->serial_no = trim($serial);
        $this->team = $this->getTeamFromInchargeByEmail($this->owner);
        $this->requester_full_name = trim($requester_name);
        $this->requester_phone = trim($requester_phone);
        $this->requester_mobile = trim($requester_mobile);
        $this->requester_email = trim($requester_email);
        $this->requester_company_name = trim($requester_company);
        $this->end_user_contact_name = trim($end_user_name);
        $this->end_user_phone = trim($end_user_phone);
        $this->end_user_email = trim($end_user_email);
        $this->end_user_mobile = trim($end_user_mobile);
        $this->end_user_company_name = trim($end_user_company);
        $this->end_user_site = trim($case_address);
        $this->case_company = trim($case_company);
        $this->end_user = trim($case_company);
        $this->case_type = trim($case_type);
        $this->note = $note;
        $this->urgency = $urgency;

        $this->insertTicketRemedy();
        $this->insertTicketLog();
        $this->insertSerial();

        // require_once 'SendMail.php';
        // require_once '../../../api/application/core/controller.php';
        // require_once '../../../api/application/controller/sendmail.php';
        // $objsendmail = new SendMail();
        // $objsendmail->noticeEngineerCaseAssigned($this->ticket_sid);
        // $objsendmail->sendToCustomerCaseOpened($this->ticket_sid);

    }

    //cron job get data from email
    function executeCreateCaseIncident($subject, $receive_mail_datetime, $case_id, $urgency, $requester_name, $requester_phone, $requester_mobile, $requester_email, $company, $end_user_site, $end_user_contact_name, $end_user_phone, $contract, $project_name, $serial_no, $incharge, $description, $status, $assigned_time, $report_by, $create_time, $prime_contract ){

        $this->caseNo = genTicketNo($this->db);
        $this->via = "remedy";
        $this->email = $this->helpGetEmailFromThainame($report_by);

        $this->receive_mail_datetime = $receive_mail_datetime;
        $this->refer_remedy_hd = $case_id;
        $this->refer_remedy_assigned_time = $assigned_time;
        $this->refer_remedy_create_time = $create_time;
        $this->refer_remedy_report_by = $report_by;

        $this->owner = $this->helpGetEmailFromThainame($incharge);
        $this->status = "1";
        $this->subject = trim(addslashes($subject));

        $this->due_datetime = "";
        $this->description = trim(addslashes($description));
        $this->contract_no = trim($contract);
        $this->prime_contract = trim($prime_contract);
        $this->project_name = $project_name;
        $this->serial_no = $serial_no;
        $this->team = $this->getTeamFromInchargeByEmail($this->owner);
        $this->requester_full_name = trim($requester_name);
        $this->requester_company_name = "";
        $this->requester_phone = trim($requester_phone);
        $this->requester_mobile = trim($requester_mobile);
        $this->requester_email = trim($requester_email);
        $this->end_user_company_name = trim($company);
        $this->end_user_site = trim($end_user_site);
        $this->end_user_contact_name = trim($end_user_contact_name);

        $end_user_infomation = explode("/", $end_user_phone);

        $this->end_user_phone = trim($end_user_infomation[0]);
        $this->end_user_mobile = trim($end_user_mobile);
        $this->end_user_email = trim($end_user_infomation[1]);
        $this->case_type = "Incident";

        $this->note = "";
        $this->urgency = trim($urgency);
        $this->case_company = trim($company);
        $this->insertTicketRemedy();
        $this->insertTicketLog();
        $this->insertSerial();
    }

    private function helpGetEmailFromThainame($thainame){
        $thainame = explode(" ", trim($thainame));
        $name = array();
        $n = 0;
        foreach ($thainame as $key => $value) {
            if($value!=""){
                $name[$n] = $value;
                $n++;
            }
        }
        $sql = "SELECT emailaddr FROM employee WHERE thaifirstname = :thaifirstname AND thailastname = :thailastname ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':thaifirstname'=>trim($name[0]),
            ':thailastname'=>trim($name[1])
            ));
        $r = $q->fetch();
        if($r['emailaddr']!=""){
            return $r['emailaddr'];
        }
        return trim($name[0])." ".trim($name[1]);
    }
    private function selectCase($sequence_id){
        $sql = "SELECT * FROM test_create_case WHERE sid = '".$sequence_id."' AND create_datetime >= '".$this->after_create_datetime."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }

    private function selectStatusNo($status){
    	$sql = "SELECT sid FROM case_status WHERE name = '".trim($status)."'";
    	$q = $this->db->prepare($sql);
    	$q->execute();
    	$r = $q->fetch();
    	if($r['sid'] && $r['sid']>0)
	    	return $r['sid'];
	    else
	    	return $status;
    }

    public function selectStatusName($case_status_sid){
        $sql = "SELECT name_return_to_remedy FROM case_status WHERE sid = '".trim($case_status_sid)."'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if(isset($r['name_return_to_remedy']))
            return $r['name_return_to_remedy'];
        else
            return $case_status_sid;
    }

    public function setSerialNumber($serial_no){
        $this->serial_no = $serial_no;
    }

    public function setTicketSid($ticket_sid){
        $this->ticket_sid = $ticket_sid;
    }

    private function helpGetEmailFromEmpno($empno){
        $sql = "SELECT emailaddr FROM employee WHERE empno = :empno ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':empno'=>$empno));
        $r = $q->fetch();
        if(isset($r['emailaddr']) && $r['emailaddr']!=""){
            return $r['emailaddr'];
        }else{
            return $empno;
        }
    }
	public function importCase($sequence_id){
        $this->after_create_datetime = '2016-08-29 00:00:00';
        $data = $this->selectCase($sequence_id);

        foreach ($data as $key => $value) {
            $this->caseNo = genTicketNo($this->db);
            $this->via = 'remedy';
            $this->refer_remedy_hd = $value['case_id'];
            $this->subject = $value['subject'];
            $this->case_type = $value['case_type'];
            if($data['status']=="Assigned"){
                $this->email =  $this->helpGetEmailFromThainame($value['report_by']);
            }else{
                $this->email = $this->helpGetEmailFromEmpno($value['last_modified_by']);
            }
            $this->case_id = $value['case_id'];
            $this->urgency = $value['urgency'];
            $this->requester_full_name = $value['requester_name'];
            $this->requester_phone = $value['requester_phone'];
            $this->requester_mobile = $value['requester_mobile'];
            $this->requester_email = $value['requester_email'];
            $this->requester_company_name = $value['requester_company_name'];
            $this->end_user_site = $value['enduser_site'];
            $this->end_user_contact_name = $value['enduser_name'];
            $this->end_user_phone = $value['enduser_phone'];
            $this->end_user_company_name = $value['enduser_company_name'];
            $this->end_user_email = $this->splitPhoneEmail($value['enduser_phone']);
            $this->contract_no = $value['contract_no'];
            $this->serial_no = $value['serial_no'];
            $this->owner = $this->helpGetEmailFromThainame($value['incharge']);
            $this->description = $value['description'];

            $this->case_status_sid = $this->status = $this->selectStatusNo($value['status']);
            $this->refer_remedy_assigned_time = $this->convertYearToInter($value['assigned_time']);
            $this->refer_remedy_report_by = $this->helpGetEmailFromThainame($value['report_by']);
            $this->receive_mail_datetime = $this->convertYearToInter($value['create_time']);
            $this->prime_contract = $value['prime_contract'];
            $this->first_assigned_time = $value['first_assigned_time'];
            $this->work_log = $value['work_log'];
            $this->due_datetime = '';
            $this->end_user = $value['enduser_company_name'];
            $this->case_company = $value['enduser_company_name'];
            $this->team = $this->getTeamFromInchargeByEmail($this->owner);

            $this->data_from = "remedy";

            if($value['expect_pending_finish']!=""){
                $this->expect_pending = $value['expect_pending_finish'];
            }

            $this->objCons->setTable($this->owner);

            if($this->refer_remedy_hd){
                $sql = "SELECT * FROM ".$this->objCons->table_ticket." WHERE refer_remedy_hd = :refer_remedy_hd ";
                $q = $this->db->prepare($sql);
                $q->execute(array(':refer_remedy_hd'=>$this->refer_remedy_hd));
                $r = $q->fetch();

                //UPDATE
                if($r['sid']>0){
                    $this->ticket_sid = $r['sid'];
                    // $this->setTicketSidFromRemedyHd();
                    if($this->ticket_sid>0){
                        $this->updateSla();
                        $this->updateOwnerCase();
                    }
                //CREATE NEW
                }else{
                    $this->insertTicketRemedy();
                    $this->updateSla();
                    $this->updateOwnerCase();
                    $this->status = $value['status'];
                    // $this->insertCaseFor7x24();
                }
                // if($this->status=="1"){

                // }else if($this->status>=2 && $this->status<=7){
                    //Must set ticket_sid before run updateSla
                // }
            }
        }
        return $data;
    }

    private function insertCaseFor7x24(){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_cms_cases WHERE hd = :hd ";
        $q = $this->db->prepare($sql);
        $q->execute( array( ':hd' => $this->refer_remedy_hd ));
        $r = $q->fetch();

        //check repeat case in tbl_cms_cases
        if( $r['id'] && $r['id'] > 0){
            //have in
        }else{
            //not have add to tbl_cms_cases
            $sql = "INSERT INTO flguploa_firstlogicis.tbl_cms_cases ( hd, contract_no, subject, assigned_time, create_datetime, receive_mail_datetime, status, serial_no, incharge, create_by, create_time)
                VALUES (:refer_remedy_hd, :contract_no, :subject, :refer_remedy_assigned_time, NOW(), :receive_mail_datetime, :status, :serial_no, :owner, :create_by, :refer_remedy_create_time)";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':refer_remedy_hd'=>$this->refer_remedy_hd,
                ':contract_no'=>$this->contract_no,
                ':subject'=>$this->subject,
                ':refer_remedy_assigned_time'=>$this->refer_remedy_assigned_time,
                ':receive_mail_datetime'=>$this->receive_mail_datetime,
                ':status'=>$this->status,
                ':serial_no'=>$this->serial_no,
                ':owner'=>($this->getEmpnoFromEmailOldProject($this->owner))?$this->getEmpnoFromEmailOldProject($this->owner):"-",
                ':create_by'=>$this->email,
                ':refer_remedy_create_time'=>$this->refer_remedy_create_time
                ));
        }
    }
    public function getTicketSidFromRemedyInc($remedyInc){
        $sql = "SELECT sid FROM ticket WHERE refer_remedy_hd = :refer_remedy_hd ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':refer_remedy_hd'=>$remedyInc));
        $r = $q->fetch();
        if(isset($r['sid']) && $r['sid']>0){
            return $r['sid'];
        }else{
            return "0";
        }
    }
    private function setTicketSidFromRemedyHd(){
    	$sql = "SELECT sid FROM ".$this->objCons->table_ticket." WHERE refer_remedy_hd = :refer_remedy_hd ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':refer_remedy_hd'=>$this->refer_remedy_hd));
    	$r = $q->fetch();
        if($r['sid'] && $r['sid']>0)
        	$this->ticket_sid = $r['sid'];
    }
   	private function convertYearToInter($datetime){

   		$datetime = explode("-", $datetime);
   		$newYear = $datetime[0]-543;

   		$datetime = $newYear."-".$datetime[1]."-".$datetime[2];
   		return $datetime;
   	}

   	private function splitPhoneEmail($end_user_phone){
   		$enduser_phone = explode("/", $end_user_phone);
   		return $mail = trim($enduser_phone[count($enduser_phone)-1]);
   	}

    private function isRepeatCreateCase(){
        $sql = "SELECT sid FROM ".$this->objCons->table_ticket."
        WHERE subject = :subject
        AND owner = :owner AND TIMESTAMPDIFF(MINUTE,create_datetime,NOW()) < 5
        AND contract_no = :contract_no ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':subject'=>$this->subject,
            ':owner'=>$this->owner,
            ':contract_no'=>$this->contract_no
            ));
        $r = $q->fetch();
        if(isset($r['sid']) && $r['sid']>0){
            return true;
        }
        return false;
    }
    private function insertTicketRemedy(){
        $canCreateCase = false;
        if($this->via=="createcase"){
            $repeatCreateCase = $this->isRepeatCreateCase();
            if(!$repeatCreateCase){
                $canCreateCase = true;
            }
        }else{
            $canCreateCase = true;
        }

        if($canCreateCase){
    	   $sql = "INSERT INTO ticket
    	       (no_ticket, via, create_datetime, create_by, receive_mail_datetime,refer_remedy_hd, refer_remedy_assigned_time, refer_remedy_create_time, refer_remedy_report_by,owner,status,subject,due_datetime,description,
    	       contract_no,prime_contract,project_name,serial_no,team,requester_full_name,requester_company_name,requester_phone,requester_mobile,requester_email,
               end_user_company_name,end_user_site,end_user_contact_name,end_user_phone,end_user_mobile,end_user_email,case_type,source,note,urgency,case_company,end_user,
               first_owner,project_owner_sid, project_sid, site_area,no_project_ticket,man_days)
    	           VALUES
    	       (:no_ticket, :via, NOW(), :create_by, :receive_mail_datetime, :refer_remedy_hd, :refer_remedy_assigned_time, :refer_remedy_create_time, :refer_remedy_report_by,:owner,:status,:subject,:due_datetime,:description,:contract_no,:prime_contract,:project_name,:serial_no,:team,
                :requester_full_name,:requester_company_name,:requester_phone,:requester_mobile,:requester_email,:end_user_company_name,:end_user_site,:end_user_contact_name,:end_user_phone,:end_user_mobile,:end_user_email,:case_type,:source,:note,:urgency,:case_company,:end_user,
                :first_owner,:project_owner_sid, :project_sid, :site_area,:no_project_ticket,:man_days) ";
    	   $q = $this->db->prepare($sql);
    	   $q->execute(array(
    	        ':no_ticket'=>$this->caseNo,
    	        ':via'=>$this->via,
    	        ':create_by'=>$this->email,
    	        ':receive_mail_datetime'=>$this->receive_mail_datetime,
    	        ':refer_remedy_hd'=>$this->refer_remedy_hd,
    	        ':refer_remedy_assigned_time'=>$this->refer_remedy_assigned_time,
    	        ':refer_remedy_create_time'=>$this->refer_remedy_create_time,
    	        ':refer_remedy_report_by'=>$this->refer_remedy_report_by,
    	        ':owner'=>$this->owner,
    	        ':status'=>$this->status,
    	        ':subject'=>$this->subject,
    	        ':due_datetime'=>$this->due_datetime,
    	        ':description'=>$this->description,
    	        ':contract_no'=>$this->contract_no,
    	        ':prime_contract' => $this->prime_contract,
    	        ':project_name'=> $this->project_name,
    	        ':serial_no'=>$this->serial_no,
    	        ':team'=>$this->team,
    	        ':requester_full_name'=>$this->requester_full_name,
    	        ':requester_company_name'=>$this->requester_company_name,
    	        ':requester_phone'=>$this->requester_phone,
    	        ':requester_mobile'=>$this->requester_mobile,
    	        ':requester_email'=>$this->requester_email,
    	        ':end_user_company_name'=>$this->end_user_company_name,
    	        ':end_user_site'=>$this->end_user_site,
    	        ':end_user_contact_name'=>$this->end_user_contact_name,
    	        ':end_user_phone'=>$this->end_user_phone,
    	        ':end_user_mobile'=>$this->end_user_mobile,
    	        ':end_user_email'=>$this->end_user_email,
    	        ':case_type'=>$this->case_type,
    	        ':source'=>$this->via,
    	        ':note'=>$this->note,
    	        ':urgency'=>$this->urgency,
    	        ':case_company'=>$this->case_company,
                ':end_user'=>$this->end_user,
                ':first_owner'=>$this->owner,
                ':project_owner_sid'=>$this->project_owner_sid,
                ':project_sid'=>$this->project_sid,
                ':site_area'=>$this->site_area,
                ':no_project_ticket'=>$this->noProjectTicket,
                ':man_days'=>$this->man_hours
    	   ));
        }
    	//set variable ticket_sid
    	$this->ticket_sid = $this->db->lastInsertId();

    }

    private function insertTicketGeneral(){
        $sql = "INSERT INTO gen_ticket (no_ticket,create_by,create_datetime) VALUES (:no_ticket, :create_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':no_ticket'=>$this->caseNo,':create_by'=>$this->email));
        $this->ticket_sid = $this->db->lastInsertId();
    }
    private function insertSerial(){
        if($this->serial_no!=""){
            $sql = "INSERT INTO ".$this->objCons->table_ticket_serial." (ticket_sid, serial_detail, create_datetime, create_by) VALUES (:ticket_sid, :serial_detail, NOW(), :create_by) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':ticket_sid' => $this->ticket_sid,
                ':serial_detail'=> $this->serial_no,
                ':create_by' => $this->email
                ));
        }
    }

    // [duplicate method] เปลี่ยนไปใช้ getTeamByEmail
    private function getTeamFromInchargeByEmail($email){
      $sql = "SELECT group_name FROM gable_incharge_team WHERE email = :email ";
      $q = $this->db->prepare($sql);
      $q->execute(array(':email'=>$email));
      $r = $q->fetch();
      if(isset($r['group_name']) && $r['group_name']!=""){
        return $r['group_name'];
      }
      return "Other";
    }
    function executeCreateCaseInstall($project,$appointment_time,$appointment_date,$engineer,$subject,$description,$end_user_site,$end_user_contact_name,$end_user_phone,$end_user_mobile,$end_user_email,$expect_time,$email,$type_service,$case_company="",$subject_service_report="",$end_user_company_name=""){

        //set variable
        $this->project = $project;
        $this->appointment_time = $appointment_time;
        $this->appointment_date = $appointment_date;
        $this->engineer = $engineer;
        $this->subject = addslashes($subject);
        $this->description = $description;
        $this->end_user_site = $end_user_site;
        $this->end_user_contact_name = $end_user_contact_name;
        $this->end_user_phone = $end_user_phone;
        $this->end_user_mobile = $end_user_mobile;
        $this->end_user_email = $end_user_email;
        $this->expect_time = $expect_time;
        $this->email = $email;
        $this->type_service = $type_service;
        $this->prime_contract = "";
        $this->via = "install_module";
        $this->case_type = "Install";
        $this->owner = $this->email;
        $this->status = '1';
        $this->due_datetime = "";
        $this->serial_no = "";
        $this->team = $this->getTeamFromInchargeByEmail($this->owner);
        //gen and set variable
        $this->caseNo = genTicketNo($this->db);
        $this->srNo = genTaskNo($this->db);
        $this->case_company = $case_company;

        $this->end_user_company_name = $end_user_company_name;

        $this->subject_service_report = $subject_service_report;
        $this->end_user_contact_name_service_report =  $end_user_contact_name;
        $this->end_user_phone_service_report = $end_user_phone;
        $this->end_user_mobile_service_report = $end_user_mobile;
        $this->end_user_email_service_report= $end_user_email;
        $this->end_user_company_name_service_report = $end_user_company_name;

        $this->prepareDataProject();
        $this->createCaseAndSR();
    }


    function executeCreateCaseImplement($project_owner_sid,$appointment_time,$appointment_date,$engineer,$subject,$description,$end_user_site,$end_user_contact_name,$end_user_phone,$end_user_mobile,$end_user_email,$expect_time,$email,$type_service,$case_company="",$subject_service_report="",$end_user_company_name=""){

        //set variable
        $this->project = $project_owner_sid;
        $this->appointment_time = $appointment_time;
        $this->appointment_date = $appointment_date;
        $this->engineer = $engineer;
        $this->subject = addslashes($subject);
        $this->description = $description;
        $this->end_user_site = $end_user_site;
        $this->end_user_contact_name = $end_user_contact_name;
        $this->end_user_phone = $end_user_phone;
        $this->end_user_mobile = $end_user_mobile;
        $this->end_user_email = $end_user_email;
        $this->expect_time = $expect_time;
        $this->email = $email;
        $this->type_service = $type_service;
        $this->prime_contract = "";
        $this->via = "implement_module";
        $this->case_type = "Implement";
        $this->owner = $this->email;
        $this->status = '1';
        $this->due_datetime = "";
        $this->serial_no = "";
        $this->team = $this->getTeamFromInchargeByEmail($this->owner);
        $this->case_company = $case_company;
        $this->end_user = $case_company;
        //gen and set variable

        $this->end_user_company_name = $end_user_company_name;

        $this->subject_service_report = $subject_service_report;
        $this->end_user_contact_name_service_report =  $end_user_contact_name;
        $this->end_user_phone_service_report = $end_user_phone;
        $this->end_user_mobile_service_report = $end_user_mobile;
        $this->end_user_email_service_report= $end_user_email;
        $this->end_user_company_name_service_report = $end_user_company_name;

        $this->caseNo = genTicketNo($this->db);
        $this->srNo = genTaskNo($this->db);
        $this->prepareDataProject();
        $this->createCaseAndSR();
    }

    private function prepareDataProject(){
        $sql = "SELECT P.* FROM project P LEFT JOIN project_owner PO ON P.sid = PO.project_sid WHERE PO.sid = :project_sid AND PO.owner = :owner ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$this->project,':owner'=>$this->email));

        $r = $q->fetch();
        $this->project_name = $r['name'];
        $this->contract_no = $r['contract'];
        $this->end_user_company_name = $this->customer = $r['customer'];
    }

    private function createCaseAndSR(){
        $this->insertTicket();
        $this->insertTicketLog();
        //end Process ticket
        $this->createSR();
        $this->createSrLog();
    }



    private function calExpectDuration(){
        $sql = "SELECT TIMESTAMPDIFF(HOUR, :start_time, :end_time) expect_duration ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':start_time'=>$this->appointment,':end_time'=>$this->expect_finish));
        $r = $q->fetch();
        return $r['expect_duration'];
    }

    private function insertTicket(){
        $sql = "INSERT INTO ".$this->objCons->table_ticket." (no_ticket, via, create_datetime, create_by, receive_mail_datetime,owner,status,subject,due_datetime,description,
        contract_no,prime_contract,project_name,serial_no,team,requester_full_name,requester_company_name,requester_phone,requester_mobile,requester_email,end_user_company_name,end_user_site,end_user_contact_name,end_user_phone,end_user_mobile,end_user_email,case_type,source,note,urgency,project_owner_sid,case_company)
            VALUES (:no_ticket, :via, NOW(), :create_by, NOW(),:owner,:status,:subject,:due_datetime,:description,:contract_no,:prime_contract,:project_name,:serial_no,:team,:requester_full_name,:requester_company_name,:requester_phone,:requester_mobile,:requester_email,:end_user_company_name,:end_user_site,:end_user_contact_name,:end_user_phone,:end_user_mobile,:end_user_email,:case_type,:source,:note,:urgency,:project_owner_sid,:case_company) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':no_ticket'=>$this->caseNo,
            ':via'=>$this->via,
            ':create_by'=>$this->email,
            ':owner'=>$this->owner,
            ':status'=>$this->status,
            ':subject'=>$this->subject,
            ':due_datetime'=>$this->due_datetime,
            ':description'=>$this->description,
            ':contract_no'=>$this->contract_no,
            ':prime_contract' => $this->prime_contract,
            ':project_name'=> $this->project_name,
            ':serial_no'=>$this->serial_no,
            ':team'=>$this->team,
            ':requester_full_name'=>$this->requester_full_name,
            ':requester_company_name'=>$this->requester_company_name,
            ':requester_phone'=>$this->requester_phone,
            ':requester_mobile'=>$this->requester_mobile,
            ':requester_email'=>$this->requester_email,
            ':end_user_company_name'=>$this->end_user_company_name,
            ':end_user_site'=>$this->end_user_site,
            ':end_user_contact_name'=>$this->end_user_contact_name,
            ':end_user_phone'=>$this->end_user_phone,
            ':end_user_mobile'=>$this->end_user_mobile,
            ':end_user_email'=>$this->end_user_email,
            ':case_type'=>$this->case_type,
            ':source'=>$this->via,
            ':note'=>$this->note,
            ':urgency'=>$this->urgency,
            ':create_by'=>$this->email,
            ':project_owner_sid'=>$this->project,
            ':case_company'=>$this->case_company
            ));
        //set variable ticket_sid
        $this->ticket_sid = $this->db->lastInsertId();
    }

    private function insertTicketLog(){
        //use variable
        $this->ticket_sid;

        $sql = "INSERT INTO ".$this->objCons->table_ticket_log."
        (ticket_sid, owner, status, subject, due_datetime, description, contract_no, prime_contract, project_name, serial_no, team, requester_full_name, requester_company_name, requester_phone, requester_mobile, requester_email, end_user_company_name, end_user_site, end_user_contact_name, end_user_phone, end_user_mobile, end_user_email, case_type, source, note, urgency, create_datetime, create_by)
        VALUES
        (:ticket_sid, :owner, :status, :subject, :due_datetime, :description, :contract_no, :prime_contract, :project_name, :serial_no, :team, :requester_full_name, :requester_company_name, :requester_phone, :requester_mobile, :requester_email, :end_user_company_name, :end_user_site, :end_user_contact_name, :end_user_phone, :end_user_mobile, :end_user_email, :case_type, :source, :note, :urgency, NOW(), :create_by) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid,
            ':owner'=>$this->owner,
            ':status'=>$this->status,
            ':subject'=>$this->subject,
            ':due_datetime'=>$this->due_datetime,
            ':description'=>$this->description,
            ':contract_no'=>$this->contract_no,
            ':prime_contract' => $this->prime_contract,
            ':project_name'=> $this->project_name,
            ':serial_no'=>$this->serial_no,
            ':team'=>$this->team,
            ':requester_full_name'=>$this->requester_full_name,
            ':requester_company_name'=>$this->requester_company_name,
            ':requester_phone'=>$this->requester_phone,
            ':requester_mobile'=>$this->requester_mobile,
            ':requester_email'=>$this->requester_email,
            ':end_user_company_name'=>$this->end_user_company_name,
            ':end_user_site'=>$this->end_user_site,
            ':end_user_contact_name'=>$this->end_user_contact_name,
            ':end_user_phone'=>$this->end_user_phone,
            ':end_user_mobile'=>$this->end_user_mobile,
            ':end_user_email'=>$this->end_user_email,
            ':case_type'=>$this->case_type,
            ':source'=>$this->via,
            ':note'=>$this->note,
            ':urgency'=>$this->urgency,
            ':create_by'=>$this->email
            ));
    }

    public function createSrIncident($ticket_sid, $appointment,$appointment_time, $expect_time, $engineerAssign, $email, $type_service,$subject_service_report="",$end_user_contact_name_service_report,$end_user_phone_service_report,$end_user_mobile_service_report,$end_user_email_service_report,$end_user_company_name_service_report){
        $this->ticket_sid = $ticket_sid;
        // $appointment = explode(" ", $appointment);
        $this->appointment_date = $appointment;
        $this->appointment_time = $appointment_time;
        $this->expect_time = $expect_time;
        $this->expect_finish = "";
        $this->type_service = $type_service;
        $this->email = $email;
        $this->engineer = $engineerAssign;


        $this->subject_service_report = $subject_service_report;

        $this->end_user_contact_name_service_report = $end_user_contact_name_service_report;
        $this->end_user_phone_service_report = $end_user_phone_service_report;
        $this->end_user_mobile_service_report = $end_user_mobile_service_report;
        $this->end_user_email_service_report = $end_user_email_service_report;
        $this->end_user_company_name_service_report = $end_user_company_name_service_report;

        $this->srNo = genTaskNo($this->db);
        $this->createSR();
        $this->createSrLog();
    }


    public function executeCreateTaskImplement($appointment_date,$appointment_time,$engineer,$expect_time,$type_service,$ticket_sid, $email){
        $this->ticket_sid = $ticket_sid;
        $this->appointment_date = $appointment_date;
        $this->appointment_time = $appointment_time;
        $this->expect_time = $expect_time;
        $this->expect_finish = "";
        $this->type_service = $type_service;
        $this->email = $email;
        $this->engineer = $engineer;
        $this->srNo = genTaskNo($this->db);
        $this->createSR();
        $this->createSrLog();
    }


    public function executeCreateTaskInstall($appointment_date,$appointment_time,$engineer,$expect_time,$type_service,$ticket_sid, $email){
        $this->ticket_sid = $ticket_sid;
        $this->appointment_date = $appointment_date;
        $this->appointment_time = $appointment_time;
        $this->expect_time = $expect_time;
        $this->expect_finish = "";
        $this->type_service = $type_service;
        $this->email = $email;
        $this->engineer = $engineer;
        $this->srNo = genTaskNo($this->db);
        $this->createSR();
        $this->createSrLog();
    }

    public function createServiceReportGeneral($email, $data){

        $role_sid = $this->objCons->getRoleByEmail($email);
        if($role_sid && 1==2){
            $this->table_ticket = $this->objCons->getTablePrefix($role_sid).$this->table_ticket;
            $this->table_tasks = $this->objCons->getTablePrefix($role_sid).$this->table_tasks;
            $this->table_tasks_log = $this->objCons->getTablePrefix($role_sid).$this->table_tasks_log;
            $this->table_task_input_action = $this->objCons->getTablePrefix($role_sid).$this->table_task_input_action;
        }else{
            $this->objCons->table_ticket = "gen_".$this->table_ticket;
            $this->objCons->table_tasks = "gen_".$this->table_tasks;
            $this->objCons->table_tasks_log = "gen_".$this->table_tasks_log;
            $this->objCons->table_task_input_action = "gen_".$this->table_task_input_action;
        }
        $this->caseNo = genTicketNoGeneral($this->db);

        // $this->ticket_sid = "1";
        $this->email = $email;
        $this->end_user_contact_name_service_report = $data['end_user']['name'];
        $this->end_user_phone_service_report = $data['end_user']['phone'];
        $this->end_user_mobile_service_report = $data['end_user']['mobile'];
        $this->end_user_email_service_report = $data['end_user']['email'];
        $this->end_user_company_name_service_report = $data['end_user']['company'];
        $this->type_service = $data['type_sid'];
        $this->subject_service_report = $data['subject'];
        $this->engineer = $data['engineer'];
        if(isset($data['appointment_datetime']) && isset($data['expect_duration'])){
            $this->appointment = isset($data['appointment_datetime'])?($data['appointment_datetime']):"";
            $this->expect_finish = $this->addAppointmentWidthExpectDuration($this->appointment, $data['expect_duration']['hours'], $data['expect_duration']['minutes']);

            $appointment = explode(" ", $this->appointment);
            $this->appointment_date = $appointment[0];
            $this->appointment_time = $appointment[1];
            $this->expect_time = (isset($data['expect_duration']['hours'])?$data['expect_duration']['hours']:"00").(isset($data['expect_duration']['minutes'])?":".$data['expect_duration']['minutes']:":00");
        }else{
            $this->appointment = "0000-00-00 00:00:00";
            $this->expect_finish = "0000-00-00 00:00:00";
            $this->expect_time = "0";
        }
            $this->insertTicketGeneral();
            $this->srNo = genTaskNoGeneral($this->db);
            $this->createSR();
            $this->createSrLog($this->engineer);
        if(!$this->checkCreateRepeat()){
            $this->callLinkSendNotiWhenCreatedSr($this->engineer);
        }
        return true;
    }

    public function createServiceReport($ticket_sid,$email, $data){

        $this->objCons->setTable($email);

        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->end_user_contact_name_service_report = $data['end_user']['name'];
        $this->end_user_phone_service_report = $data['end_user']['phone'];
        $this->end_user_mobile_service_report = $data['end_user']['mobile'];
        $this->end_user_email_service_report = $data['end_user']['email'];
        $this->end_user_company_name_service_report = $data['end_user']['company'];
        $this->type_service = $data['type_sid'];
        $this->subject_service_report = $data['subject'];
        $this->engineer = $data['engineer'];
        if(isset($data['appointment_datetime']) && isset($data['expect_finish_datetime'])){
            if(isset($data['ref']) && $data['ref']=="webapp"){
                $this->appointment = $this->convertDatetimeWebFormatToDb($data['appointment_datetime']);
                $this->expect_finish = $this->convertDatetimeWebFormatToDb($data['expect_finish_datetime']);
            }else{
                $this->appointment = isset($data['appointment_datetime'])?$this->convertDatetimeAppFormatToDd($data['appointment_datetime']):"";
                $this->expect_finish = isset($data['expect_finish_datetime'])?$this->convertDatetimeAppFormatToDd($data['expect_finish_datetime']):"";
            }
            $appointment = explode(" ", $this->appointment);
            $this->appointment_date = $appointment[0];
            $this->appointment_time = $appointment[1];

            $this->expect_time = $this->calExpectDuration();
        }else{
             $this->appointment = "0000-00-00 00:00:00";
            $this->expect_finish = "0000-00-00 00:00:00";
            $this->expect_time = "0";
        }
        if(!$this->checkCreateRepeat()){
            $this->srNo = genTaskNo($this->db);
            $this->createSR();
            $this->createSrLog($this->engineer);
            $this->callLinkSendNotiWhenCreatedSr($this->engineer, $this->ticket_sid,$this->task_sid);
        }
        return true;
    }

    public function createServiceReport2($ticket_sid,$email, $data){
        $this->objCons->setTable($email);

        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->end_user_contact_name_service_report = $data['end_user']['name'];
        $this->end_user_phone_service_report = $data['end_user']['phone'];
        $this->end_user_mobile_service_report = $data['end_user']['mobile'];
        $this->end_user_email_service_report = $data['end_user']['email'];
        $this->end_user_company_name_service_report = $data['end_user']['company'];
        $this->type_service = $data['type_sid'];
        $this->subject_service_report = $data['subject'];
        $this->engineer = $data['engineer'];
        if(isset($data['appointment_datetime']) && isset($data['expect_finish_datetime'])){
            if(isset($data['ref']) && $data['ref']=="webapp"){
                $this->appointment = $this->convertDatetimeWebFormatToDb($data['appointment_datetime']);
                $this->expect_finish = $this->convertDatetimeWebFormatToDb($data['expect_finish_datetime']);
            }else{
                $this->appointment = isset($data['appointment_datetime'])?$this->convertDatetimeAppFormatToDd($data['appointment_datetime']):"";
                $this->expect_finish = isset($data['expect_finish_datetime'])?$this->convertDatetimeAppFormatToDd($data['expect_finish_datetime']):"";
            }
            $appointment = explode(" ", $this->appointment);
            $this->appointment_date = $appointment[0];
            $this->appointment_time = $appointment[1];
            $this->expect_time = $this->calExpectDuration();
        }else{
             $this->appointment = "0000-00-00 00:00:00";
            $this->expect_finish = "0000-00-00 00:00:00";
            $this->expect_time = "0";
        }
        if(!$this->checkCreateRepeat()){
            $this->srNo = genTaskNo($this->db);
            $this->createSR();
            $engineer = array();
            foreach ($this->engineer as $key => $value) {
                array_push($engineer, $value['email']);
            }
            $this->engineer = $engineer;
            $this->createSrLog($this->engineer);
            $this->callLinkSendNotiWhenCreatedSr($this->engineer, $this->ticket_sid,$this->task_sid);
        }
        return true;
    }

    private function addAppointmentWidthExpectDuration($datetime, $hours, $minutes){
        $sql = "SELECT DATE_ADD(:datetime_, INTERVAL :hours HOUR) new_datetime ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':datetime_'=>$datetime,':hours'=>$hours));
        $r = $q->fetch();
        $datetime = $r['new_datetime'];

        $sql = "SELECT DATE_ADD(:datetime_, INTERVAL :minutes MINUTE) new_datetime ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':datetime_'=>$datetime,':minutes'=>$minutes));
        $r = $q->fetch();
        $datetime = $r['new_datetime'];
        return $datetime;
    }
    public function createServiceReport3($ticket_sid,$email, $data){
        // $this->objCons->setTable($email);

        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->end_user_contact_name_service_report = $data['end_user']['name'];
        $this->end_user_phone_service_report = $data['end_user']['phone'];
        $this->end_user_mobile_service_report = $data['end_user']['mobile'];
        $this->end_user_email_service_report = $data['end_user']['email'];
        $this->end_user_company_name_service_report = $data['end_user']['company'];
        $this->service_address = isset($data['address'])?$data['address']:"";
        $this->type_service = $data['type_sid'];
        $this->subject_service_report = $data['subject'];
        $this->engineer = $data['engineer'];

        if(isset($data['appointment_datetime']) && isset($data['expect_duration'])){
            $this->appointment = isset($data['appointment_datetime'])?($data['appointment_datetime']):"";
            $this->expect_finish = $this->addAppointmentWidthExpectDuration($this->appointment, $data['expect_duration']['hours'], $data['expect_duration']['minutes']);

            $appointment = explode(" ", $this->appointment);
            $this->appointment_date = $appointment[0];
            $this->appointment_time = $appointment[1];
            $this->expect_time = (isset($data['expect_duration']['hours'])?$data['expect_duration']['hours']:"00").(isset($data['expect_duration']['minutes'])?":".$data['expect_duration']['minutes']:":00");
        }else{
            $this->appointment = "0000-00-00 00:00:00";
            $this->expect_finish = "0000-00-00 00:00:00";
            $this->expect_time = "0";
        }

        if(!$this->checkCreateRepeat()){
            $this->srNo = genTaskNo($this->db);
            $this->createSR();
            $this->createSrLog($this->engineer);
            $this->callLinkSendNotiWhenCreatedSr($this->engineer, $this->ticket_sid,$this->task_sid);
        }
        return true;
    }
    public function debugToDb($debug){
      try{
        $sql = "INSERT INTO debug (debug) VALUES (:debug) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':debug'=>$debug));
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }
    public function createServiceReportReact($ticket_sid,$email, $data){
        // $this->objCons->setTable($email);

        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->end_user_contact_name_service_report = $data['end_user']['name'];
        $this->end_user_phone_service_report = $data['end_user']['phone'];
        $this->end_user_mobile_service_report = $data['end_user']['mobile'];
        $this->end_user_email_service_report = $data['end_user']['email'];
        $this->end_user_company_name_service_report = $data['end_user']['company'];
        $this->service_address = isset($data['address'])?$data['address']:"";
        $this->type_service = $data['type_sid'];
        $this->subject_service_report = $data['subject'];
        $this->engineer = $data['engineer'];
        $this->staff = isset($data['staff'])?$data['staff']:array();

       // $this->debugToDb("Debug appointment_datetime ".$data['appointment_datetime']);

        if(isset($data['appointment_datetime']) && isset($data['expect_duration'])){
            $this->appointment = isset($data['appointment_datetime'])?($data['appointment_datetime']):"";
            $this->expect_finish = $this->addAppointmentWidthExpectDuration($this->appointment, $data['expect_duration']['hours'], $data['expect_duration']['minutes']);

            $appointment = explode(" ", $this->appointment);
            $this->appointment_date = $appointment[0];
            $this->appointment_time = $appointment[1];
            $this->expect_time = (isset($data['expect_duration']['hours'])?$data['expect_duration']['hours']:"00").(isset($data['expect_duration']['minutes'])?":".$data['expect_duration']['minutes']:":00");
        }else{
            $this->appointment = "0000-00-00 00:00:00";
            $this->expect_finish = "0000-00-00 00:00:00";
            $this->expect_time = "0";
        }

        if(!$this->checkCreateRepeat()){
            $this->srNo = genTaskNo($this->db);
            $this->createSR();
            $this->createSrLogReact($this->engineer);
            $this->callLinkSendNotiWhenCreatedSr($this->engineer, $this->ticket_sid,$this->task_sid);
        }
        return true;
    }

    private function checkCreateRepeat(){
        $sql = "SELECT T.sid FROM ".$this->objCons->table_tasks." T WHERE T.create_by = :create_by AND T.ticket_sid = :ticket_sid AND TIMESTAMPDIFF(MINUTE,T.create_datetime,NOW()) < 2 AND subject_service_report = :subject_service_report ORDER BY T.sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':create_by'=>$this->email, ':ticket_sid'=>$this->ticket_sid,':subject_service_report'=>trim($this->subject_service_report)));
        $r = $q->fetch();
        if($r['sid']>0){
            return true;
        }
        return false;
    }

    private function createSR(){
        //use variable
        $this->ticket_sid;
        $status = "0";

        $sql = "INSERT INTO ".$this->objCons->table_tasks."
                (no_task, ticket_sid, create_by, create_datetime, appointment, expect_duration, expect_finish, service_type, last_status,subject_service_report,end_user_contact_name_service_report,end_user_phone_service_report,end_user_mobile_service_report,end_user_email_service_report,end_user_company_name_service_report,service_report_address)
                VALUES
                (:no_task, :ticket_sid, :create_by, NOW(), :appointment, :expect_duration, :expect_finish, :service_type, :last_status,:subject_service_report,:end_user_contact_name_service_report,:end_user_phone_service_report,:end_user_mobile_service_report,:end_user_email_service_report,:end_user_company_name_service_report,:service_report_address) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':no_task'=>$this->srNo,
            ':ticket_sid'=>$this->ticket_sid,
            ':create_by'=>$this->email,
            ':appointment'=>$this->appointment_date." ".$this->appointment_time.":00",
            ':expect_duration'=>$this->expect_time,
            ':expect_finish'=>$this->expect_finish,
            ':service_type'=>$this->type_service,
            ':last_status'=>$status,
            ':subject_service_report'=>trim($this->subject_service_report),
            ':end_user_contact_name_service_report'=>trim($this->end_user_contact_name_service_report),
            ':end_user_phone_service_report'=>trim($this->end_user_phone_service_report),
            ':end_user_mobile_service_report'=>trim($this->end_user_mobile_service_report),
            ':end_user_email_service_report'=>trim($this->end_user_email_service_report),
            ':end_user_company_name_service_report'=>trim($this->end_user_company_name_service_report),
            ':service_report_address'=>trim($this->service_address)
            ));

        $task_sid = $this->db->lastInsertId();
        //set variable task_sid
        $this->task_sid = $task_sid;

    }
    private function createSrLog($engineer = array()){
        //use variable
        $this->task_sid;
        if(count($engineer)<1){
            $engineer = explode(",",$this->engineer);
        }
        $request_taxi = "0";
        if($this->type_service=="1" || $this->type_service=="5"){
            $request_taxi = "1";
        }
        $main_engineer = "";
        $associate_engineer = "";
        foreach ($engineer as $key => $value) {
            if($key==0){
                $main_engineer = $value;
            }else{
                if($associate_engineer!=""){
                    $associate_engineer .= ",";
                }
                $associate_engineer .= $value;
            }

            if($value==$this->email){
                $status = "100";
            }else{
                $status = "0";
            }
            $sql = "INSERT INTO ".$this->objCons->table_tasks_log."
                (tasks_sid, froms, engineer, create_by, create_datetime, appointment, expect_duration, expect_finish, status, service_type, request_taxi)
                VALUES
                (:tasks_sid, :froms, :engineer, :create_by, NOW(), :appointment, :expect_duration, :expect_finish, :status, :service_type, :request_taxi) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':tasks_sid'=>$this->task_sid,
                ':froms'=>'',
                ':engineer'=>$value,
                ':create_by'=>$this->email,
                ':appointment'=>$this->appointment_date." ".$this->appointment_time.":00",
                ':expect_duration'=>$this->expect_time,
                ':expect_finish'=>$this->expect_finish,
                ':status'=>$status,
                ':service_type'=>$this->type_service,
                ':request_taxi'=>$request_taxi
            ));

            // if($key>0){
                $sql = "INSERT INTO
                    tasks_engineer (tasks_sid,engineer, status, create_datetime, create_by, request_taxi, primary_engineer, updated_datetime)
                    VALUES (:tasks_sid, :engineer, :status, NOW(), :create_by, :request_taxi,:primary_engineer, NOW()) ";
                $q = $this->db->prepare($sql);
                $q->execute(array(
                    ':tasks_sid'=>$this->task_sid,
                    ':engineer'=>$this->value,
                    ':status'=>$status,
                    ':create_by'=>$this->email,
                    ':request_taxi'=>$request_taxi,
                    ':primary_engineer'=>($key==0)?"1":"0"
                    ));
            // }
        }
        $this->updateTasksSetEngineerAndAssociateEngineerBySid($main_engineer, $associate_engineer);
    }

    private function createSrLogReact($engineer = array()){
        //use variable
        $this->task_sid;
        if(count($engineer)<1){
            $engineer = explode(",",$this->engineer);
        }
        $main_engineer = "";
        $associate_engineer = "";
        foreach ($engineer as $key => $value) {
            if($key==0){
                $main_engineer = $value;
            }else{
                if($associate_engineer!=""){
                    $associate_engineer .= ",";
                }
                $associate_engineer .= $value;
            }

            $request_ot = 1;
            $request_taxi = 1;
            foreach ($this->staff as $kk => $vv) {
                if($vv['email']==$value){
                    if(!isset($vv['request_taxi']) || !$vv['request_taxi']){
                        $request_taxi = 0;
                    }
                    if(!isset($vv['request_ot']) || !$vv['request_ot']){
                        $request_ot = 0;
                    }
                }
            }
            if($value==$this->email){
                $status = "100";
            }else{
                $status = "0";
            }
            $sql = "INSERT INTO ".$this->objCons->table_tasks_log."
                (tasks_sid, froms, engineer, create_by, create_datetime, appointment, expect_duration, expect_finish, status, service_type, request_ot, request_taxi)
                VALUES
                (:tasks_sid, :froms, :engineer, :create_by, NOW(), :appointment, :expect_duration, :expect_finish, :status, :service_type, :request_ot, :request_taxi) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':tasks_sid'=>$this->task_sid,
                ':froms'=>'',
                ':engineer'=>$value,
                ':create_by'=>$this->email,
                ':appointment'=>$this->appointment_date." ".$this->appointment_time.":00",
                ':expect_duration'=>$this->expect_time,
                ':expect_finish'=>$this->expect_finish,
                ':status'=>$status,
                ':service_type'=>$this->type_service,
                ':request_ot'=>$request_ot,
                ':request_taxi'=>$request_taxi
            ));

            // if($key>0){
                $sql = "INSERT INTO
                    tasks_engineer (tasks_sid,engineer, status, create_datetime, create_by, request_taxi, primary_engineer,updated_datetime)
                    VALUES (:tasks_sid, :engineer, :status, NOW(), :create_by, :request_taxi,:primary_engineer,NOW()) ";
                $q = $this->db->prepare($sql);
                $q->execute(array(
                    ':tasks_sid'=>$this->task_sid,
                    ':engineer'=>$value,
                    ':status'=>$status,
                    ':create_by'=>$this->email,
                    ':request_taxi'=>$request_taxi,
                    ':primary_engineer'=>($key==0)?"1":"0"
                    ));
            // }
        }
        $this->updateTasksSetEngineerAndAssociateEngineerBySid($main_engineer, $associate_engineer);
        $this->updateTicketNumberSr($this->ticket_sid);

    }

    public function updateTicketNumberSr($ticket_sid){
        $sql = "UPDATE ticket SET number_sr = (SELECT count(TASK.sid) FROM tasks TASK WHERE TASK.ticket_sid = :ticket_sid and appointment_type != -1), updated_number_sr = NOW() WHERE sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
    }

    public function callLinkSendNotiWhenCreatedSr($engineer, $ticket_sid=0, $tasks_sid=0,$project_sid=0){
        //SEND NOTIFICATION WHEN CREATE SR
        $main_engineer = "";
        $associate_engineer = "";
        foreach ($engineer as $key => $value) {
            if($key==0){
                $main_engineer = $value;
            }else{
                if($associate_engineer!=""){
                    $associate_engineer .= ",";
                }
                $associate_engineer .= $value;
            }

            $message = 'New '.$this->srNo.", ".$this->subject_service_report.", ".$this->appointment_date." ".$this->appointment_time;
            $data = array ('to' => $value, 'message' => $message,'notification_sid'=>"2",'ticket_sid'=>$ticket_sid,'tasks_sid'=>$tasks_sid,'project_sid'=>$project_sid);
            $this->sendNoti($data['to'],$data['message'], $data['notification_sid'], $data['ticket_sid'], $data['tasks_sid'], $data['project_sid']);
        }
        if(1==2){
            $message = $this->email." Created ".$this->srNo.", ".$this->subject_service_report.", ".$this->appointment_date.", ".$this->appointment_time." ".$main_engineer.(($associate_engineer)?", ".$associate_engineer:"");
            $data = array ('to' => 'autsakorn.t@firstlogic.co.th', 'message' => $message,'notification_sid'=>"2",'ticket_sid'=>$ticket_sid,'tasks_sid'=>$tasks_sid,'project_sid'=>$project_sid);
            $data = http_build_query($data);
            $context_options = array (
                        'http' => array (
                            'method' => 'POST',
                            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                                . "Content-Length: " . strlen($data) . "\r\n",
                            'content' => $data
                            )
                        );
            $context = stream_context_create($context_options);
            $handle = fopen(HOST_NAME."/apis/v1/incident/sendNotification", "r", false, $context);
        }

    }
    public function sendNoti($to, $message, $notification_sid=0,$ticket_sid=0,$tasks_sid=0,$project_sid=0){
        $this->objCons->sendNotificationAction($to,$message, $notification_sid,$ticket_sid,$tasks_sid,$project_sid);
    }


    private function updateTasksSetEngineerAndAssociateEngineerBySid($main_engineer,$associate_engineer){
        $sql = "UPDATE ".$this->objCons->table_tasks." SET engineer = :engineer, associate_engineer = :associate_engineer WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':engineer'=>$main_engineer,':associate_engineer'=>$associate_engineer,':sid'=>$this->task_sid));
    }

    private function getRoleEmail(){
        $sql = "SELECT role_sid FROM user_role WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$this->emailForRule));
        $r = $q->fetch();
        $this->role = $r['role_sid'];
        return $r['role_sid'];
    }

    private function getRule(){
        // $sql = "SELECT R.rule FROM rule R WHERE R.name = :name_role ";
        // $q = $this->db->prepare($sql);
        // $q->execute(array(':name_role'=>$this->name_role));
        // $r = $q->fetch();
        // $rule = $r['rule'];
        // eval($role);
        $sqlC = "";
        if($this->role=="2" || $this->role=="4" || $this->role=="6" || $this->role=="1" || $this->role == "7"){

        }
        // else if($this->role=="1"){
        //     $sqlC = " AND (T.create_by = '".$this->emailForRule."' OR T.owner = '".$this->emailForRule."' ) ";
        // }
        else if($this->emailForRule!='')  {
            $sqlC = " AND T.owner = '".$this->emailForRule."' ";

        }
        return $sqlC;
    }
    // private function setTable($email){
    // 	$role_sid = $this->objCons->getRoleByEmail($email);
    //     if($role_sid){
    //         $this->table_ticket = $this->objCons->getTablePrefix($role_sid).$this->table_ticket;
    //         $this->table_ticket_log = $this->objCons->getTablePrefix($role_sid).$this->table_ticket_log;
    //         $this->table_ticket_status = $this->objCons->getTablePrefix($role_sid).$this->table_ticket_status;
    //         $this->table_ticket_owner = $this->objCons->getTablePrefix($role_sid).$this->table_ticket_owner;
    //         $this->table_ticket_serial = $this->objCons->getTablePrefix($role_sid).$this->table_ticket_serial;
    //         $this->table_ticket_to_do_pm = $this->objCons->getTablePrefix($role_sid).$this->table_ticket_to_do_pm;

    //         $this->table_tasks = $this->objCons->getTablePrefix($role_sid).$this->table_tasks;
    //         $this->table_tasks_log = $this->objCons->getTablePrefix($role_sid).$this->table_tasks_log;
    //         $this->table_task_do_serial_pm = $this->objCons->getTablePrefix($role_sid).$this->table_task_do_serial_pm;
    //         $this->table_task_followup_log = $this->objCons->getTablePrefix($role_sid).$this->table_task_followup_log;
    //         $this->table_task_input_action = $this->objCons->getTablePrefix($role_sid).$this->table_task_input_action;
    //         $this->table_task_input_request = $this->objCons->getTablePrefix($role_sid).$this->table_task_input_request;
    //         $this->table_task_spare = $this->objCons->getTablePrefix($role_sid).$this->table_task_spare;

    //     }else{
    //         $this->table_ticket = "gen_".$this->table_ticket;
    //         $this->table_ticket_log = "gen_".$this->table_ticket_log;
    //         $this->table_ticket_status = "gen_".$this->table_ticket_status;
    //         $this->table_ticket_owner = "gen_".$this->table_ticket_owner;
    //         $this->table_ticket_serial = "gen_".$this->table_ticket_serial;
    //         $this->table_ticket_to_do_pm = "gen_".$this->table_ticket_to_do_pm;

    //         $this->table_tasks = "gen_".$this->table_tasks;
    //         $this->table_tasks_log = "gen_".$this->table_tasks_log;
    //         $this->table_task_do_serial_pm = "gen_".$this->table_task_do_serial_pm;
    //         $this->table_task_followup_log = "gen_".$this->table_task_followup_log;
    //         $this->table_task_input_action = "gen_".$this->table_task_input_action;
    //         $this->table_task_input_request = "gen_".$this->table_task_input_request;
    //         $this->table_task_spare = "gen_".$this->table_task_spare;

    //     }
    // }

    public function taskDetail($email, $task_sid){
        $this->tasks_sid = $task_sid;
        $this->email = $email;
        $this->objCons->setTable($this->email);

        $sql = "SELECT TL.*, CONCAT('คุณ',E.thainame) create_by_thainame, E.mobile create_by_mobile,
        CONCAT('คุณ',EE.thainame) engineer_thainame, EE.mobile engineer_mobile,
        SS.label_activity service_status_name FROM ".$this->objCons->table_tasks_log." TL
        LEFT JOIN employee E ON E.emailaddr = TL.create_by
        LEFT JOIN employee EE ON EE.emailaddr = TL.engineer
        LEFT JOIN service_status SS ON TL.status = SS.task_status
        WHERE TL.tasks_sid = :tasks_sid ORDER BY TL.create_datetime ASC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$task_sid));
        $r = $q->fetchAll();
        return $r;
    }

    public function taxiRequest($email, $filter){
        $this->email = $email;
        // $this->objCons->setTable($this->email);

        if(isset($filter['start']) && isset($filter['end'])){
            $sql = "SELECT T.*,TT.end_user_site, TT.end_user,E.thainame, U.formal_name thainame,TT.contract_no FROM tasks T
                LEFT JOIN user_role UR ON UR.email = T.engineer
                LEFT JOIN user U ON T.engineer = U.email
                LEFT JOIN ticket TT ON TT.sid = T.ticket_sid
                LEFT JOIN employee E ON T.engineer = E.emailaddr
                WHERE T.taxi_fare_total >= 0 ";

                $sql .= " AND DATE_FORMAT(T.appointment,'%Y-%m-%d') >= :start AND DATE_FORMAT(T.appointment,'%Y-%m-%d') <= :end  ";

                $can_assign = $this->objCons->getCanAssign($this->email);

                $sql .= " AND UR.role_sid IN (".$can_assign.") ";

                if(isset($filter['staff'])){
                    $sql .= " AND (T.engineer = '".$filter['staff']."' OR T.associate_engineer LIKE '%".$filter['staff']."%') ";
                }
                $sql .= " ORDER BY T.appointment ASC,updated_taxi_fare DESC LIMIT 0,1000";
                $q = $this->db->prepare($sql);
                $q->execute(array(':start'=>$filter['start'],':end'=>$filter['end']));

        }else{
            $sql = "SELECT T.*,TT.end_user_site, TT.end_user,E.thainame,U.formal_name thainame, TT.contract_no FROM tasks T
            LEFT JOIN user_role UR ON UR.email = T.engineer
            LEFT JOIN user ON T.engineer = U.email
            LEFT JOIN ticket TT ON TT.sid = T.ticket_sid
            LEFT JOIN employee E ON T.engineer = E.emailaddr
            WHERE taxi_fare_total > 0 ";
            if(isset($filter['year']) && isset($filter['month'])){
                $sql .= " AND DATE_FORMAT(T.appointment,'%Y-%m') = :filter ";
            }
            $can_assign = $this->objCons->getCanAssign($this->email);
            $sql .= " AND UR.role_sid IN (".$can_assign.") ";
            $sql .= " ORDER BY T.appointment ASC ,updated_taxi_fare DESC LIMIT 0,1000";
            $q = $this->db->prepare($sql);

            if(isset($filter['year']) && isset($filter['month'])){
                $q->execute(array(':filter'=>$filter['year']."-".$filter['month']));
            }else{
                $q->execute();
            }
        }

        $r = $q->fetchAll();
        return $r;
    }
    public function lists($type, $email="", $option = array(), $emailForRule="", $search="", $type_view="" , $owner="", $sort = array()){
        $this->email = $email;
        $this->emailForRule = $emailForRule;
        $this->objCons->setTable($this->emailForRule);

        $this->permission_role = $this->objCons->permissionRoleByEmail($this->emailForRule);

        // echo $this->table_ticket;
        $sql = "SELECT T.*,CASE WHEN T.end_user !='' THEN T.end_user ELSE T.case_company END end_user,
                    E.thainame,E.empno,E.maxportraitfile,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile,
                CASE WHEN (SELECT tasks.sid FROM tasks INNER JOIN customer_signature_task ON tasks.sid = customer_signature_task.task_sid
                    INNER JOIN ".$this->objCons->table_ticket." ticket ON tasks.ticket_sid = ticket.sid
                    WHERE 1 AND tasks.ticket_sid = T.sid AND ticket.case_type = 'Implement' LIMIT 0,1)  > 0
                    THEN
                        CONCAT('<a href=\'https://case.flgupload.com/apis/v1/mergepdf/mergePDF/',T.contract_no,'\'>PDF</a>')
                    ELSE '' END all_sr_implement,
                (SELECT tasks.appointment FROM ".$this->objCons->table_tasks." tasks WHERE 1 AND tasks.ticket_sid = T.sid ORDER BY tasks.sid DESC LIMIT 0,1) last_appointment,
                CONCAT('<a href=\"javascript:void(0);\" onclick=\"openDetail(\'',T.sid,'\')\">View Detail</a>') viewdetail,
                CASE T.status WHEN '1' THEN 'New' WHEN '2' THEN 'WIP' WHEN '3' THEN 'WorkAround' WHEN '4' THEN 'Pending' WHEN '5' THEN 'Resolved' WHEN '10' THEN 'No WorkAround' ELSE 'In Process' END status_name,
                    DATE_FORMAT(T.create_datetime, '%d.%m.%Y %H:%i') create_datetime_df
                FROM ".$this->objCons->table_ticket." T
                    LEFT JOIN employee E ON E.emailaddr = T.owner
                    LEFT JOIN ".$this->objCons->table_project_owner." PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN ".$this->objCons->table_project." P ON P.sid = PO.project_sid
                    LEFT JOIN user_role UR ON UR.email = T.owner
                WHERE 1 AND T.owner IS NOT NULL ";

        // $option['case_type'] .= ",POC";

        if($type_view == "all"){
            // $sql
        }else if($type_view=="resolved"){
            $sql .= " AND (T.status = 5 OR T.status = 6 ) ";
            if($this->email!=""){
                $sql .= " AND (P.create_by = '".$this->email."' OR T.create_by = '".$this->email."' OR PO.create_by = '".$this->email."') ";
            }
        }else if($type_view=="missed"){
            $sql .= " AND (T.sla_remedy LIKE '%Missed%' OR T.sla_remedy LIKE '%missed%' ) ";
        }else{
            $sql .= " AND T.status <> 5 AND T.status <> 6 ";
            if($this->email!=""){
                $sql .= " AND (P.create_by = '".$this->email."' OR T.create_by = '".$this->email."' OR PO.create_by = '".$this->email."') ";
            }
        }

        if($owner!="autsakorn.t@firstlogic.co.th"){
            $sql .= " AND T.owner <> 'autsakorn.t@firstlogic.co.th' ";
        }

        // $role = $this->getRoleEmail();
        // $sqlC = $this->getRule();
        // if($sqlC!=""){
        //     $sql .= $sqlC;
        // }
        // $can_assign = $this->objCons->getCanAssign($this->email);
        if($owner=="group"){
            $can_assign = $this->objCons->getCanAssign($this->emailForRule);
            $sql .= " AND UR.role_sid IN (".$can_assign.") ";

            if(isset($option['case_type'])){
                if($this->permission_role['comp_refer_ebiz']=="cdgm"){
                    $option['case_type'] .= ",'Business Requirements Discovery','Technical Requirements Discovery','POC'";
                }
                $sql .= "AND T.case_type IN (".$option['case_type'].") ";
            }else{
                // $type .= ",POC";
                $sql .= "AND T.case_type = '".$type."' ";
            }
        }else if($owner!=""){
            $sql .= " AND (T.first_owner = '".$owner."' OR T.owner = '".$owner."' ) ";

        }else{
            $role = $this->getRoleEmail();
            $sqlC = $this->getRule();
            if($sqlC!=""){
                $sql .= $sqlC;
            }
        }

        if($search!=""){
            $sql .= " AND (T.subject LIKE '%".$search."%' OR T.no_ticket LIKE '%".$search."%' OR T.end_user_company_name LIKE '%".$search."%' OR T.contract_no LIKE '%".$search."%' OR T.refer_remedy_hd LIKE '%".$search."%' OR E.thainame LIKE '%".$search."%' OR T.end_user LIKE '%".$search."%' OR T.serial_no LIKE '%".$search."%') ";
        }
        if(isset($sort['name'])){
            $sql .= " ORDER BY ".$sort['name']." ".$sort['type'];
            // echo $sql;
        }else{
            if(isset($option['sort'])){
                $sql .= " ORDER BY T.create_datetime ".$option['sort'];
            }else{
                $sql .= " ORDER BY T.create_datetime DESC";
            }
        }
        // echo $sql;
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {
            $r[$key]['target_sla'] = array();
            if($value['case_type']=="Incident"){
                if(strpos($value['sla_remedy'], 'Missed')){
                    $r[$key]['missed'] = true;
                    $r[$key]['target_sla'] = $this->getTargetSLA($value['sid'],$value['refer_remedy_hd']);
                }else{
                    $r[$key]['missed'] = false;
                }
            }
        }
        // return $can_assign;
        return $r;
    }
    public function remindSLA($type, $email="", $option = array()){

        $this->objCons->setTable($email);

        $this->email = $email;

        $sql = "SELECT T.*,E.thainame,E.empno,E.maxportraitfile,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile
                FROM ".$this->objCons->table_ticket." T
                    LEFT JOIN employee E ON E.emailaddr = T.owner
                    LEFT JOIN ".$this->objCons->table_project_owner." PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN ".$this->objCons->table_project." P ON P.sid = PO.project_sid
                WHERE 1 ";
        if(isset($option['case_type'])){
            $sql .= "AND T.case_type IN (".$option['case_type'].") ";
        }else{
            $sql .= "AND T.case_type = '".$type."' ";
        }
        if($this->email!=""){
            $sql .= " AND (P.create_by = '".$this->email."' OR T.create_by = '".$this->email."' OR PO.create_by = '".$this->email."') ";
        }

        if(isset($option['sort'])){
            $sql .= " ORDER BY T.create_datetime ".$option['sort'];
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $r[$key]['sla'] = $this->getDataIncidentSLA($value['sid'], $value['create_datetime']);
        }
        return $r;
    }
    private function getTicketStatusWorklogRemedy($ticket_sid){
        $sql = "SELECT T.*,E.thainame create_by_thainame,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) create_by_pic,
        E.mobile create_by_mobile FROM ".$this->objCons->table_ticket_status." T
        LEFT JOIN employee E ON T.create_by = E.emailaddr AND E.emailaddr <> ''
        WHERE T.ticket_sid = :ticket_sid AND T.data_from = 'remedy'
         ORDER BY sid DESC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }
    private function getTicketStatusWorklog($ticket_sid){
        $sql = "SELECT T.*,E.thainame create_by_thainame,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) create_by_pic,
        E.mobile create_by_mobile FROM ".$this->objCons->table_ticket_status." T
        LEFT JOIN employee E ON T.create_by = E.emailaddr AND E.emailaddr <> ''
        WHERE T.ticket_sid = :ticket_sid
         ORDER BY sid DESC  ";
         // AND T.data_from = 'remedy'
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    private function getTargetSLA($ticket_sid, $refer_remedy_hd){
        $sql = "SELECT * FROM ".$this->objCons->table_ticket_sla." WHERE ticket_sid = :ticket_sid ORDER BY due_datetime ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }
    public function getCaseDetail($ticket_sid, $email="", $token=""){

        if($email!=""){
            $this->objCons->setTable($email);
        }
        $this->email = $email;
        $sql = "SELECT T.*,E.thainame,E.maxportraitfile owner_picture,E.mobile owner_mobile, DATE_FORMAT(T.create_datetime,'%d.%m.%Y %H:%i') create_datetime_df, CS.name status_name
        	FROM ".$this->objCons->table_ticket." T LEFT JOIN employee E ON T.owner = E.emailaddr LEFT JOIN case_status CS ON CS.sid = T.status
            WHERE T.sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetch();
        if(($r['case_type']=="Incident" || $r['case_type']=="Question" || $r['case_type']=="Request") && $r['refer_remedy_hd']!=""){
            // $r['sla'] = $this->getDataIncidentSLA($ticket_sid,$r['create_datetime']); OLD Algorithm
            $r['sla'] = $this->getTargetSLA($ticket_sid, $r['refer_remedy_hd']);

            if($r['refer_remedy_hd']){
                $r['expect_pending_finish'] = $this->getExpectPendingFinish($r['refer_remedy_hd']);
            }
        }
        if($r['refer_remedy_hd']!=""){
            $r['work_log_remedy'] = $this->getTicketStatusWorklogRemedy($ticket_sid);
        }else{
            $r['work_log_remedy'] = $this->getTicketStatusWorklog($ticket_sid);
        }


        if(isset($r['serial_no']) && $r['serial_no']){
        	$serial_no = explode(",", $r['serial_no']);
        	// print_r($serial_no);
        	if(count($serial_no)>0){
        		$r['serial_no'] = array();
	        	foreach ($serial_no as $key => $value) {
	        		$r['serial_no'][$key]['serial'] = $value;
	        	}
	        }
        }

        if($r['status']==5){
            $this->can_create_new_sr = $r['can_create_new_sr'] = false;
            $this->can_change_status = $r['can_change_status'] = false;
        }else{
            $this->can_create_new_sr = $r['can_create_new_sr'] = true;
            $this->can_change_status = $r['can_change_status'] = true;
        }

        $r['service_report'] = $this->listServiceReport($r['sid']);
        return $r;
    }

    private function getExpectPendingFinish($refer_remedy_hd){
        $sql = "SELECT expect_pending_finish FROM test_create_case WHERE case_id = :case_id AND status = 'Pending' AND expect_pending_finish <> '' ORDER BY sid DESC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':case_id'=>$refer_remedy_hd));
        $r = $q->fetch();

        if(isset($r['expect_pending_finish']) && $r['expect_pending_finish']){
            $expect_pending_finish = explode("T", $r['expect_pending_finish']);
            $datetime = explode("-", $expect_pending_finish[0]);
            $datetime = ($datetime[0]-543)."/".$datetime[1]."/".$datetime[2];

            $expect_pending_finish = $datetime." ".str_replace("+07:00", "", $expect_pending_finish[1]);
            return $expect_pending_finish;
        }
        return 0;
    }
    private function getDataIncidentSLA($ticket_sid, $create_datetime_sla){
        // CASE WHEN
        //         (SELECT
        //             (SELECT FDR_SUB.status FROM flow_detail_role FDR_SUB WHERE FDR_SUB.flow_detail_sid = FDR.flow_detail_sid ORDER BY create_datetime DESC LIMIT 0,1 )
        //             FROM flow_detail_role FDR WHERE FDR.flow_detail_sid = FD.sid AND FDR.role_sid = :role_sid GROUP BY FDR.flow_detail_sid
        //         ) = 1 THEN 1
        //         ELSE 0 END
        //         can_update_status,
        $modules_sid = "2";
        $sql = "SELECT FD.sla,CS.name case_status_name,CS.icon case_status_icon,CS.is_force, CS.sid case_status_sid,
                DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE) target_sla_time,

                (SELECT TS.create_datetime FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) done_sla_datetime,
                (SELECT TS.worklog FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) worklog,
                CASE
                WHEN
                    DATE_FORMAT(DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE),'%Y-%m-%d %H:%i:%s') < NOW() AND
                    (SELECT TS.sid FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) IS NULL AND
                    CS.is_force = '1'
                THEN 'Failed'
                WHEN
                    (SELECT TS.sid FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) IS NOT NULL AND
                    DATE_FORMAT(DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE),'%Y-%m-%d %H:%i:%s') < (SELECT TS.create_datetime FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1)
                THEN 'Failed'
                WHEN
                    (SELECT TS.sid FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1) IS NOT NULL AND
                    DATE_FORMAT(DATE_ADD(:create_datetime_sla, INTERVAL FD.sla MINUTE),'%Y-%m-%d %H:%i:%s') >= (SELECT TS.create_datetime FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1)
                THEN 'Passed'
                ELSE '-' END result
                FROM flow_detail FD
                INNER JOIN flow F ON FD.flow_sid = F.sid
                INNER JOIN case_status CS ON CS.sid = FD.case_status_sid
                WHERE F.status = '1' AND FD.status = '1'
                AND  (SELECT TS.create_datetime FROM ".$this->objCons->table_ticket_status." TS WHERE TS.case_status_sid = CS.sid AND TS.ticket_sid = :ticket_sid AND TS.case_status_sid > 0 ORDER BY TS.sid ASC LIMIT 0,1 ) IS NULL
                AND F.modules_sid = :modules_sid ORDER BY FD.position_x ASC, FD.position_y ASC  ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':create_datetime_sla'=>$create_datetime_sla,
            ':ticket_sid'=>$ticket_sid,
            ':modules_sid'=>$modules_sid
            ));
        $r = $q->fetchAll();
        return $r;
    }

    public function changeAppointment($email, $appointment_date, $appointment_time, $task_sid){
        $this->objCons->setTable($email);

        $this->setTaskSid($task_sid);
        $this->setAppointment($appointment_date, $appointment_time);
        $this->setEmail($email);

        return $this->updateAppointment();
    }

    public function changeExpectTime($email, $expect_time, $task_sid){
        $this->objCons->setTable($email);

        $this->expect_time = $expect_time;
        $this->email = $email;
        $this->task_sid = $task_sid;
        return $this->updateExpectTime();
    }
    private function updateExpectTime(){
        $sql = "UPDATE ".$this->objCons->table_tasks." SET expect_duration = :expect_time, updated_by = :updated_by, updated_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':expect_time'=>$this->expect_time,
            ':updated_by'=>$this->email,
            ':sid'=>$this->task_sid
            ));
        $sql = "UPDATE ".$this->objCons->table_tasks_log." SET expect_duration = :expect_time WHERE tasks_sid = :tasks_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':expect_time'=>$this->expect_time,
            ':tasks_sid'=>$this->task_sid
            ));
        $sql = "SELECT expect_duration FROM ".$this->objCons->table_tasks." WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':sid'=>$this->getTaskSid()
            ));
        $r = $q->fetch();
        return $r['expect_duration'];
    }
    private function updateAppointment(){
        $sql = "UPDATE ".$this->objCons->table_tasks." SET appointment = :appointment, updated_by = :updated_by, updated_datetime = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':appointment'=>$this->getAppointment(),
            ':updated_by' => $this->getEmail(),
            ':sid'=>$this->getTaskSid()
            ));

        $sql = "UPDATE ".$this->objCons->table_tasks_log." SET appointment = :appointment WHERE tasks_sid = :task_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':appointment'=>$this->getAppointment(),
            ':task_sid'=>$this->getTaskSid()
            ));

        $sql = "SELECT DATE_FORMAT(appointment,'%d/%m/%Y %H:%i') appointment_datetime FROM ".$this->objCons->table_tasks." WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':sid'=>$this->getTaskSid()
            ));
        $r = $q->fetch();

        return $r['appointment_datetime'];
    }

    private function setTaskSid($task_sid){
        $this->task_sid = $task_sid;
    }
    private function getTaskSid(){
        return $this->task_sid;
    }

    private function setAppointment($appointment_date, $appointment_time){
        $appointment_date = explode("/", $appointment_date);
        $appointment_date = $appointment_date[2]."-".$appointment_date[1]."-".$appointment_date[0];

        $appointment_time = $appointment_time.":00";
        $this->appointment_datetime = $appointment_date." ".$appointment_time;
    }
    private function getAppointment(){
        return $this->appointment_datetime;
    }

    private function setEmail($email){
        $this->email = $email;
    }
    private function getEmail(){
        return $this->email;
    }

    public function initailUpdateWorklog($ticket_sid, $work_log, $email){
        $this->objCons->setTable($email);
        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->work_log = addslashes($work_log);
        $this->updateWorklog();
    }
    private function updateWorklog(){
        return 0;
        $sql = "INSERT INTO ticket_worklog
            (ticket_sid,worklog,create_by,create_datetime) VALUES
            (:ticket_sid,:worklog,:create_by,NOW()) ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid,
            ':worklog'=>$this->work_log,
            ':create_by'=>$this->email
            ));
        // $sql = "SELECT * FROM ticket_worklog WHERE sid = '".$this->db->lastInsertId."'";
        // $q = $this->db->prepare($sql);
        // $q->execute();
        // return $q->fetch();
    }

    public function getTeamByEmail($email){
        $sql = "SELECT R.name FROM user U
                left join role R on R.sid = U.role_sid
                where email = :email";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $result = $q->fetch();
        return $result['name'];
    }

    public function getStatusTicket(){
        $sql = "SELECT status FROM ticket WHERE refer_remedy_hd = :refer_remedy_hd OR no_ticket = :no_ticket";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':refer_remedy_hd'=>$this->refer_remedy_hd
            ,':no_ticket'=>$this->caseNo));
        $result = $q->fetch();
        return $result['status'];
    }

    //[new][nanth]
    public function genNumberTicket($conn){
        $sql = "SELECT DATE_FORMAT(NOW(),'%y') year ";
        $q = $conn->prepare($sql);
        $q->execute();
        $result = $q->fetch();
        $year = $result['year'];

        $sql = "SELECT no_ticket FROM ticket
        ORDER BY no_ticket DESC LIMIT 0,1 FOR UPDATE";
        $q = $this->db->prepare($sql);
        $q->execute();
        $result = $q->fetch();
        $number = substr($result['no_ticket'],2);// 1700
        $number +=1;
        $number = substr($number,2);// 000
        $number = strval($number);
        return "HD".$year.$number;
    }

    public function initailUpdateSlaAndWorklog($ticket_sid, $work_log, $case_status_sid, $email, $expect_pending="0000-00-00 00:00:00", $solution="",
                                               $solution_detail="", $send_to_ccd = 0, $is_return_remedy = 1, $status_reason, $serial_no, $resolution_product_category_tier_1, $resolution_product_category_tier_2, $resolution_product_category_tier_3, $additional_status = 0){

        // $this->objCons->setTable($email);
        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->work_log = ($work_log);
        $doing_case_status_sid = $this->case_status_sid = $case_status_sid;
        $this->expect_pending = $expect_pending;

        $this->solution = ($solution);
        $this->solution_detail = ($solution_detail);
        $this->status_reason = $status_reason;
        $this->send_to_ccd = $send_to_ccd;
        $this->serial_no = $serial_no;
        $this->resolution_product_category_tier_1 = $resolution_product_category_tier_1;
        $this->resolution_product_category_tier_2 = $resolution_product_category_tier_2;
        $this->resolution_product_category_tier_3 = $resolution_product_category_tier_3;
        $this->additional_status = $additional_status;
        $this->refer_remedy_hd = $this->selectReferRemedyHdFromTicketSid($this->ticket_sid);

        $original_is_return_remedy = $this->is_return_remedy = $is_return_remedy;
        // คำนวน sla สำหรับ status ก่อนหน้าทั้งหมด
        // เตรียมยกเลิก
        $in_case_status_sid = "";
        if($this->case_status_sid=='3'){
            $in_case_status_sid = "'2','7'";
        }else if($this->case_status_sid=='5'){
            $in_case_status_sid = "'2','7','3'";
        }else if($this->case_status_sid=='7'){
            $in_case_status_sid = "'2'";
        }
        if($in_case_status_sid!=""){
            $slaOnsiteInProcess = $this->selectSlaWhereStatusInProcess($in_case_status_sid); // ดึง sla ก่อนหน้าทั้งหมดมาเตรียมคำนวน
            foreach ($slaOnsiteInProcess as $key => $value) {
                $this->case_status_sid = $value['case_status_sid'];
                $this->is_return_remedy = 2;
                $this->updateSla();
                $this->processCalSla();
                $this->updateTicketAlert();
            }
        }
        //

        $this->case_status_sid = $doing_case_status_sid;
        $this->is_return_remedy = $original_is_return_remedy;

        $this->updateSla();
        $this->processCalSla();
        $this->updateTicketAlert();
        $this->updateResultSlaToTicketColumnSlaRemedy();
        if(isset($this->serial_no)){
            $this->updateSerialNumber();
        }
        if($this->case_status_sid == '5'){
            $this->updateResolutionProductCategory();
        }
        if(isset($this->additional_status)){
            $this->updateAdditionalStatus();
        }
    }

    public function updateAdditionalStatus(){
        $sql = "UPDATE ticket SET additional_status = :additional_status WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(
                ':sid'=>$this->ticket_sid,
                ':additional_status'=>$this->additional_status
            )
        );
    }

    // function update serial number of ticket
    public function updateSerialNumber(){
        $sql = "UPDATE ticket SET serial_no = :serial_no WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ":sid"=>$this->ticket_sid,
            ":serial_no"=>$this->serial_no
        ));
    }
    // function update resolution_category_tier 1-3
    public function updateResolutionProductCategory(){
        $sql = "UPDATE ticket SET resolution_product_category_tier_1 = :resolution_product_category_tier_1, resolution_product_category_tier_2 = :resolution_product_category_tier_2,
                resolution_product_category_tier_3 = :resolution_product_category_tier_3
                WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute((array(
            ":sid"=>$this->ticket_sid,
            ":resolution_product_category_tier_1"=>$this->resolution_product_category_tier_1,
            ":resolution_product_category_tier_2"=>$this->resolution_product_category_tier_2,
            ":resolution_product_category_tier_3"=>$this->resolution_product_category_tier_3
        )));
    }


    private function updateResultSlaToTicketColumnSlaRemedy(){
      try{
        $sql = "UPDATE `ticket` SET sla_remedy = (SELECT GROUP_CONCAT(ticket_sla.status SEPARATOR ', ') FROM ticket_sla
        WHERE ticket_sla.ticket_sid = ticket.sid), update_sla_remedy_datetime = NOW() WHERE ticket.sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
      }catch(PDOException $e){

      }
    }
    private function selectSlaWhereStatusInProcess($in_case_status_sid){
        $sql = "SELECT * FROM ticket_sla WHERE ticket_sid = :ticket_sid AND status = 'In Process'
        AND case_status_sid IN (".$in_case_status_sid.") ORDER BY sid ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetchAll();
        return $r;
    }

    public function updateChangeAssignedTicket($ticket_sid, $work_log, $case_status_sid, $email, $expect_pending="0000-00-00 00:00:00", $solution="", $solution_detail=""){
        $this->ticket_sid = $ticket_sid;
        $this->email = $email;
        $this->work_log = ($work_log);
        $this->case_status_sid = $case_status_sid;
        $this->expect_pending = $expect_pending;

        $this->solution = ($solution);
        $this->solution_detail = ($solution_detail);
        $this->send_to_ccd = "1";
        $this->refer_remedy_hd = $this->selectReferRemedyHdFromTicketSid($this->ticket_sid);

        $this->processCalSla();
        $this->updateResultSlaToTicketColumnSlaRemedy();
    }

    private function updateTicketAlert(){
        $sql = "UPDATE ticket_alert SET in_queue = 2 WHERE ticket_sid = :ticket_sid AND case_status_sid = :case_status_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid,':case_status_sid'=>$this->case_status_sid));
    }

    private function stopPendingBefore(){
        $sql = "SELECT * FROM ticket_status WHERE ticket_sid = :ticket_sid AND case_status_sid = '4' AND wait_kill_pending = '1' ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();

        if(isset($r['sid']) && $r['sid']>0){
            $sql = "INSERT INTO ticket_status (ticket_sid, send_to_ccd, case_status_sid, create_datetime, create_by, data_status, expect_pending, update_datetime, update_by, worklog, solution, solution_detail, wait_kill_pending, is_return_remedy) VALUES (:ticket_sid, :send_to_ccd, :case_status_sid, NOW(),
                :create_by, :data_status, :expect_pending, NOW(), :update_by, :worklog, :solution, :solution_detail, :wait_kill_pending, :is_return_remedy) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                'ticket_sid'=>$this->ticket_sid,
                ':send_to_ccd'=>'1',
                ':case_status_sid'=>'2',
                ':create_by'=>$this->email,
                ':data_status'=>'1',
                ':expect_pending'=>'0000-00-00 00:00:00',
                ':update_by'=>$this->email,
                ':worklog'=>'หยุด Pending เพราะมีการเวลาเปลี่ยน Pending',
                ':solution'=>'',
                ':solution_detail'=>'',
                ':wait_kill_pending'=>'0',
                ':is_return_remedy'=>'1'
                ));
            $sql = "UPDATE ticket_status SET wait_kill_pending = '0' WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$r['sid']));
        }
    }

    private function clearWaitKillPending(){
        $sql = "UPDATE ticket_status SET wait_kill_pending = '0' WHERE ticket_sid = :ticket_sid AND wait_kill_pending = '1' ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
    }

//    private function updateTicket($ticket){
//        $this->stopPending($ticket);
//        $this->updateTicketStatus($ticket);
//    }

    // new
    private function stopPending($ticket_sid){
        $sql = "UPDATE ticket_status
                SET wait_kill_pending = '0'
                WHERE ticket_sid = :ticket_sid AND wait_kill_pending = '1' ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
    }

    public function getTicketDetail($field = ['*']){
        $sql = "SELECT ".implode(", ",$field)." FROM ticket WHERE sid = :ticket_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        return $q->fetch();
    }

    // new
    public function updateTicketStatus($ticket){ // new
        $vSpaceToKBank = new VSpaceToKBank();
        $sql = "INSERT INTO ticket_status (ticket_sid, case_status_sid, create_datetime, create_by, data_status, worklog, expect_pending, solution, solution_detail, data_from, wait_kill_pending, send_to_ccd, is_return_remedy)
            VALUES (:ticket_sid, :case_status_sid, NOW(), :create_by, :data_status, :work_log, :expect_pending,:solution, :solution_detail, :data_from, :wait_kill_pending, :send_to_ccd, :is_return_remedy) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$ticket['ticket_sid'],
            ':case_status_sid'=>$ticket['case_status_sid'],
            ':create_by'=>$ticket['email'],
            ':data_status'=>'1',
            ':work_log'=>$ticket['worklog'],
            ':expect_pending'=>isset($ticket['expect_pending'])?$ticket['expect_pending']:"0000-00-00 00:00:00",
            ':solution'=>isset($ticket['solution'])?$ticket['solution']:"",
            ':solution_detail'=>isset($ticket['solution_detail'])?$ticket['solution_detail']:"",
            ':data_from'=>isset($ticket['data_from'])?$ticket['data_from']:"",
            ':wait_kill_pending'=> isset($ticket['wait_kill_pending'])?$ticket['wait_kill_pending']:0,
            ':send_to_ccd'=>$ticket['send_to_ccd'],
            ':is_return_remedy'=>isset($ticket['is_return_remedy'])?$ticket['is_return_remedy']:null
        ));

        $sql = "UPDATE ticket SET status = :status, update_datetime = NOW(), updated_by = :email WHERE sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':status'=>$ticket['case_status_sid'],
            ':email'=>$ticket['email'],
            ':ticket_sid'=>$ticket['ticket_sid'],
        ));

        $params = array(
            'submitter' => $ticket['email'],
            'incident_number' => $this->selectReferRemedyHdFromTicketSid($ticket['ticket_sid']),
            'status' => $this->selectStatusName($ticket['status_to_remedy']),
            'status_reason' => $ticket['status_reason'],
            'assigned_support_company' => $ticket['assigned_support_company'],
            'assigned_support_organization' => $ticket['assigned_support_organization'],
            'assigned_group' => $ticket['assigned_group'],
            'assigned_group_id' => $ticket['assigned_group_id'],
            'assignee' => $ticket['assignee'],
            'assignee_login_id' => $ticket['assignee_login_id'],
            'z1d_note' => $ticket['z1d_note'],
            'z1d_action' => $ticket['z1d_action' ],
            'z1d_char01' => $ticket['z1d_char01'],
            'resolution' => $ticket['solution_detail'],
            'resolution_category' => $ticket['resolution_category'],
            'resolution_category_tier_2' => $ticket['resolution_category_tier_2'],
            'resolution_category_tier_3' => $ticket['resolution_category_tier_3']
        );
        $res_remedy = $vSpaceToKBank->updateIncidentCase(
            $params
        );
        return $res_remedy;
    }

    public function updateSla(){

        // 4 = pending
        $wait_kill_pending = "0";
        if($this->case_status_sid=="4"){
            $wait_kill_pending = "1";
            $this->stopPendingBefore();
        }
        if($this->case_status_sid!="4" && $this->case_status_sid!="8" ){
            $this->clearWaitKillPending();
        }

        $sql = "INSERT INTO ".$this->objCons->table_ticket_status." (ticket_sid, case_status_sid, create_datetime, create_by, data_status, worklog, expect_pending, solution, solution_detail, data_from, wait_kill_pending, send_to_ccd, is_return_remedy, status_reason, additional_status)
            VALUES (:ticket_sid, :case_status_sid, NOW(), :create_by, :data_status, :work_log, :expect_pending,:solution, :solution_detail, :data_from, :wait_kill_pending, :send_to_ccd, :is_return_remedy, :status_reason, :additional_status) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':ticket_sid'=>$this->ticket_sid,
            ':case_status_sid'=>$this->case_status_sid,
            ':create_by'=>$this->email,
            ':data_status'=>'1',
            ':work_log'=>($this->work_log),
            ':expect_pending'=>$this->expect_pending,
            ':solution'=>($this->solution),
            ':solution_detail'=>($this->solution_detail),
            ':data_from'=>$this->data_from,
            ':wait_kill_pending'=>$wait_kill_pending,
            ':send_to_ccd'=>$this->send_to_ccd,
            ':is_return_remedy'=>$this->is_return_remedy,
            ':status_reason'=>$this->status_reason,
            ':additional_status'=>$this->additional_status
        ));

        if($this->case_status_sid != 8){ // not equal worklog
            $sql = "UPDATE ".$this->objCons->table_ticket." SET status = :status, update_datetime = NOW(), updated_by = :email WHERE sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':status'=>$this->case_status_sid,':email'=>$this->email,':ticket_sid'=>$this->ticket_sid));
        }
    }



    private function processCalSla(){ // ticket_sid, case_status_sid
        $sql = "SELECT TS.* FROM ticket_sla TS WHERE TS.ticket_sid = :ticket_sid ORDER BY sid ASC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $slas = $q->fetchAll();

        $create_datetime = $this->getCreateDatetimeSlaForTicket();// req ticket_sid
        $ticketStatus = $this->getTicketStatus();//

        $done_datetime = $ticketStatus[0];
        $pendingData = $ticketStatus[1];
        $ticket_status_sid = $ticketStatus[2];

        if($this->case_status_sid=="10"){ // no work around
            $this->case_status_sid = "3";
        }else if($this->case_status_sid=="11"){// no on site
            $this->case_status_sid = "7";
        }

        $diffDatetime = $this->calDiffDatetime($create_datetime, $done_datetime);
        foreach ($slas as $key => $sla) {
            if($sla['case_status_sid']==$this->case_status_sid){
                $this->calAndUpdateResultSla(
                    $sla['sid'], $sla, $create_datetime, $done_datetime, $diffDatetime, $pendingData, $ticket_status_sid
                );
            }
        }
    }

    private function calDiffDatetime($first_datetime, $second_datetime){
        $sql = "SELECT TIMESTAMPDIFF(minute, :first_datetime, :second_datetime) datetime_diff ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':first_datetime'=>$first_datetime,':second_datetime'=>$second_datetime));
        $r = $q->fetch();
        return $r['datetime_diff'];
    }
    public function setTicketSet($ticket_sid){
      $this->ticket_sid = $ticket_sid;
    }
    public function getCreateDatetimeSlaForTicket(){
        $sql = "SELECT created_datetime FROM ticket_sla WHERE ticket_sid = :ticket_sid ORDER BY created_datetime ASC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetch();
        return $r['created_datetime'];
    }

    private function getCreateDatetimeTicket(){
        $sql = "SELECT create_datetime FROM ticket WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$this->ticket_sid));
        $r = $q->fetch();
        return $r['create_datetime'];
    }


    private function getTicketStatus(){
        $sql = "SELECT TS.* FROM ticket_status TS WHERE ticket_sid = :ticket_sid AND case_status_sid = :case_status_sid ORDER BY sid ASC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid, ':case_status_sid'=>$this->case_status_sid));
        $r = $q->fetch();
        if(isset($r['create_datetime']) && $r['create_datetime']!=""){

            $pendingData = $this->getTicketPending($r['sid']);
            return array($r['create_datetime'], $pendingData, $r['sid']);
        }else{
            return array("0000-00-00 00:00:00", array(), 0);
        }
    }

    private function getTicketPending($beforeTicketStatusSid){
        $sql = "SELECT TS.*,
        (SELECT TSS.create_datetime next_status_create_datetime FROM ticket_status TSS WHERE TSS.ticket_sid = :ticket_sid AND TSS.case_status_sid <> '4' AND TSS.case_status_sid <> '8' AND TSS.sid > TS.sid ORDER BY TSS.sid ASC LIMIT 0,1) next_status_create_datetime
        FROM ticket_status TS
        WHERE TS.sid < :beforeTicketStatusSid AND ticket_sid = :ticket_sid AND TS.case_status_sid = 4 ORDER BY TS.sid ASC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':beforeTicketStatusSid'=>$beforeTicketStatusSid,':ticket_sid'=>$this->ticket_sid));
        return $q->fetchAll();
    }
    private function calAndUpdateResultSla($ticket_sla_sid, $data_ticket_sla, $create_datetime, $done_datetime, $diffDatetime, $pendingData, $ticket_status_sid){
        $minute;
        if($data_ticket_sla['sla_unit']=="Hour"){
            $minute = $data_ticket_sla['sla_value']*60;
        }else if($data_ticket_sla['sla_unit']=="Day" || $data_ticket_sla['sla_unit']=="NBD"){
            $minute = $data_ticket_sla['sla_value']*60*24;
        }else{
            $minute = $data_ticket_sla['sla_value'];
        }
        $pendingTotalMinute = $this->calTotalPendingMinute($pendingData);

        $r = array();
        // ยกเเลิก จนกว่า due datetime จะคำนวนได้ถูกต้อง
//        if ($done_datetime<=$data_ticket_sla['due_datetime']) {
//            $r['sla_result'] = 'Met';//.$minute.' '.$diffDatetime.' '.$pendingTotalMinute;
//        }else{
//            $r['sla_result'] = 'Missed';//.$minute.' '.$diffDatetime.' '.$pendingTotalMinute;
//        }

        if($minute>=($diffDatetime-$pendingTotalMinute)){
            $r['sla_result'] = 'Met';//.$minute.' '.$diffDatetime.' '.$pendingTotalMinute;
        }else{
            $r['sla_result'] = 'Missed';//.$minute.' '.$diffDatetime.' '.$pendingTotalMinute;
        }


        $sql = "UPDATE ticket_sla SET status = :status, updated_datetime = NOW(), case_create_datetime = :case_create_datetime, done_datetime = :done_datetime, diff_time = :diff_time, diff_time_final = :diff_time_final, pending_total_minute = :pending_total_minute, ticket_status_sid = :ticket_status_sid WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':status'=>$r['sla_result'],
            ':case_create_datetime'=>$create_datetime,
            ':done_datetime'=>$done_datetime,
            ':diff_time'=>$diffDatetime,
            ':diff_time_final'=>($diffDatetime-$pendingTotalMinute),
            ':pending_total_minute'=>$pendingTotalMinute,
            ':ticket_status_sid'=>$ticket_status_sid,
            ':sid'=>$ticket_sla_sid
            )
        );

    }

    private function calTotalPendingMinute($pendingData){
        $pendingTotalMinute = 0;
        foreach ($pendingData as $key => $value) {
            $pendingTotalMinute += intval($this->calDiffDatetime($value['create_datetime'], $value['next_status_create_datetime']));
        }
        return $pendingTotalMinute;
    }

    private function updateOwnerCase(){

        $sql = "INSERT INTO ".$this->objCons->table_ticket_owner." (ticket_sid, owner, create_datetime) VALUES (:ticket_sid, :owner, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid,':owner'=>$this->owner));

        $sql = "UPDATE ".$this->objCons->table_ticket." SET owner = :owner, update_datetime = NOW(), updated_by = :email,
        subject = :subject, case_type = :case_type, contract_no = :contract_no, serial_no = :serial_no, description = :description WHERE sid = :ticket_sid ";

        $q = $this->db->prepare($sql);
        $q->execute(array(':owner'=>$this->owner,':email'=>$this->email,
            'subject'=>$this->subject,
            'case_type'=>$this->case_type,
            'contract_no'=>$this->contract_no,
            'serial_no'=>$this->serial_no,
            'description'=>$this->description,
            ':ticket_sid'=>$this->ticket_sid));
    }

    public function requestEndUserServiceReport($task_sid, $email=""){
        if($email!=""){
            $this->objCons->setTable($email);
        }
      $this->task_sid = $task_sid;
      $sql = "SELECT sid task_sid,
        CASE WHEN end_user_company_name_service_report IS NULL THEN '' ELSE end_user_company_name_service_report END end_user_company_name_service_report,
        CASE WHEN end_user_contact_name_service_report IS NULL THEN '' ELSE end_user_contact_name_service_report END end_user_contact_name_service_report,
        CASE WHEN end_user_mobile_service_report IS NULL THEN '' ELSE end_user_mobile_service_report END end_user_mobile_service_report,
        CASE WHEN end_user_phone_service_report IS NULL THEN '' ELSE end_user_phone_service_report END end_user_phone_service_report,
        CASE WHEN end_user_email_service_report IS NULL THEN '' ELSE end_user_email_service_report END end_user_email_service_report
      FROM ".$this->objCons->table_tasks." WHERE sid = :task_sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':task_sid'=>$this->task_sid
        ));
      return $q->fetch();
   }

   public function requestSubjectServiceReport($task_sid, $email=""){
        if($email!=""){
            $this->objCons->setTable($email);
        }
        $this->task_sid = $task_sid;
      $sql = "SELECT tasks.sid task_sid, tasks.service_type service_type, service_type.name service_type_name, service_type.icon service_type_icon,
        CASE WHEN subject_service_report IS NULL THEN '' ELSE subject_service_report END subject_service_report
      FROM ".$this->objCons->table_tasks." LEFT JOIN service_type ON tasks.service_type = service_type.sid WHERE tasks.sid = :task_sid ";
      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':task_sid'=>$this->task_sid
        ));
      return $q->fetch();
   }

   public function editSubjectServiceReport($tasks_sid,$email,$subject_service_report, $service_type){
        if($email!=""){
            $this->objCons->setTable($email);
        }
        $this->task_sid = $tasks_sid;
        $this->email = $email;
        $this->type_service = $service_type;
        $this->subject_service_report = $subject_service_report;
        $this->editSubjectServiceReportRunCommand();
   }

   private function editSubjectServiceReportRunCommand(){
    $sql = "UPDATE ".$this->objCons->table_tasks." SET subject_service_report = :subject_service_report,updated_by = :updated_by, updated_datetime = NOW(), service_type = :service_type
    WHERE sid = :task_sid  ";
    $q = $this->db->prepare($sql);
    $q->execute(array(
        ':subject_service_report'=>$this->subject_service_report,
        ':updated_by'=>$this->email,
        ':service_type'=>$this->type_service,
        ':task_sid'=>$this->task_sid
    ));

   }

   public function editEndUserServiceReport($tasks_sid,$email,$end_user_contact_name_service_report,$end_user_email_service_report,$end_user_mobile_service_report,$end_user_phone_service_report,$end_user_company_name_service_report){
        if($email!=""){
            $this->objCons->setTable($email);
        }

        $this->task_sid = $tasks_sid;
        $this->email = $email;

        $this->end_user_contact_name_service_report =  $end_user_contact_name_service_report;
        $this->end_user_phone_service_report = $end_user_phone_service_report;
        $this->end_user_mobile_service_report = $end_user_mobile_service_report;
        $this->end_user_email_service_report= $end_user_email_service_report;
        $this->end_user_company_name_service_report = $end_user_company_name_service_report;

        $this->editEndUserServiceReportRunCommand();
   }
   private function editEndUserServiceReportRunCommand(){
    $sql = "UPDATE ".$this->objCons->table_tasks." SET end_user_contact_name_service_report = :end_user_contact_name_service_report,
    end_user_phone_service_report = :end_user_phone_service_report,
    end_user_mobile_service_report = :end_user_mobile_service_report,
    end_user_email_service_report = :end_user_email_service_report,
    end_user_company_name_service_report = :end_user_company_name_service_report, updated_by = :updated_by, updated_datetime = NOW()
    WHERE sid = :task_sid  ";
    $q = $this->db->prepare($sql);
    $q->execute(array(
        ':end_user_contact_name_service_report'=>$this->end_user_contact_name_service_report,
        ':end_user_phone_service_report'=>$this->end_user_phone_service_report,
        ':end_user_mobile_service_report'=>$this->end_user_mobile_service_report,
        ':end_user_email_service_report'=>$this->end_user_email_service_report,
        ':end_user_company_name_service_report'=>$this->end_user_company_name_service_report,
        ':updated_by'=>$this->email,
        ':task_sid'=>$this->task_sid
    ));

   }

   public function listServiceReport($ticket_sid){

        $this->ticket_sid = $ticket_sid;

        return $this->listServiceReports();
   }

   private function listServiceReports(){

        $sql = "SELECT *,DATE_FORMAT(appointment,'%d.%m.%Y %H:%i') appointment, DATE_FORMAT(expect_finish,'%d.%m.%Y %H:%i') expect_finish,
        (SELECT solution FROM ".$this->table_task_input_action." TIA WHERE TIA.task_sid = tasks.sid ORDER BY sid DESC LIMIT 0,1) action_solution
          FROM ".$this->objCons->table_tasks." tasks WHERE tasks.ticket_sid = :ticket_sid AND tasks.last_status > -1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$this->ticket_sid));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            if(file_exists(ROOT_PATH.'pdf/'.$value['no_task'].'.pdf')){
                $r[$key]['pdf_report'] = "<a href='".URL."pdf/".$value['no_task'].".pdf' target='new'>PDF</a>";
            }else{
                $r[$key]['pdf_report'] = "-";
            }

            $engineer = $this->taskDetailOfTicket($value['sid']);
            $engineerData = "";
            foreach ($engineer as $k => $v) {
                if($v['last_status_task']>=0){
                    if(count($engineer)>1){
                        if($v['thaifirstname']!=""){
                            $engineerData .= "(".$v['thaifirstname'].") ";
                        }else{
                            $engineerData .= "(".$v['engineer'].") ";
                        }
                    }else{
                        if($v['thaifirstname']!=""){
                            $engineerData .= "".$v['thaifirstname'];
                        }else{
                            $engineerData .= "".$v['engineer'];
                        }
                    }
                }
            }
            $r[$key]['engineer'] = $engineerData;
            $r[$key]['engineer_raw'] = $engineer;

            $projectModel = new ProjectModel();
            $r[$key]['timestamp'] = $projectModel->listTaskDetail($value['sid'], $this->ticket_sid, $this->email);
            $r[$key]['sql'] = $sql;

            $r[$key]['can_change_status'] = $this->can_change_status;
            $r[$key]['can_create_new_sr'] = $this->can_create_new_sr;

            if($this->can_create_new_sr || 1){
                $r[$key]['html_app'] = '<li class="table-view-cell media"><a class="navigate-right" href="#service_report/'.$this->ticket_sid.'/'.$value['sid'].'/right/other">
                                          <img class="media-object pull-left" src="../img/icon/'.$value['service_type'].'.png">
                                          <div class="media-body">
                                              <p>'.$value['no_task'].'</p>
                                              <p>Subject <span class="pull-right">'.$value['subject_service_report'].'</span></p>
                                              <p>Appointment <span class="pull-right">'.$value['appointment'].'</span></p>
                                              <p>
                                                <div class="row">
                                                    <div class="col-xs-2"><p>Engineer</p></div>
                                                    <div class="col-xs-10">
                                                        <div class="pull-right"><p>'.$r[$key]['engineer'].'</p></div>
                                                    </div>
                                                </div>
                                              </p>
                                          </div>
                                        </a></li>';
            }else{
                $r[$key]['html_app'] = '<li class="table-view-cell  cell-block">
                                          <img class="media-object pull-left" src="http://placehold.it/42x42">
                                          <div class="media-body">
                                              <p>'.$value['no_task'].'</p>
                                              <p>Subject <span class="pull-right">'.$value['subject_service_report'].'</span></p>
                                              <p>Appointment <span class="pull-right">'.$value['appointment'].'</span></p>
                                              <p>
                                                <div class="row">
                                                    <div class="col-xs-2"><p>Engineer</p></div>
                                                    <div class="col-xs-10">
                                                        <div class="pull-right"><p>'.$r[$key]['engineer'].'</p></div>
                                                    </div>
                                                </div>
                                              </p>
                                          </div>
                                        </li>';
            }

        }
        return $r;
   }

   private function taskDetailOfTicket($tasks_sid, $create_datetime =""){

        $sql = "SELECT TL.engineer,TL.tasks_sid,E.thaifirstname,E.thailastname,E.thainame,E.engfirstname,E.englastname,E.maxportraitfile picture,
                E.mobile,E.emailaddr email,(SELECT status FROM ".$this->objCons->table_tasks_log." WHERE TL.engineer = engineer AND TL.tasks_sid = tasks_sid ORDER BY sid DESC LIMIT 0,1) last_status_task,
                CASE (SELECT status FROM ".$this->objCons->table_tasks_log." WHERE TL.engineer = engineer AND TL.tasks_sid = tasks_sid ORDER BY sid DESC LIMIT 0,1)
                            WHEN '0' THEN 'Assign'
                            WHEN '-2' THEN 'Removed'
                            WHEN '-1' THEN 'Engineer Rejected'
                            WHEN '100' THEN 'Engineer Accepted'
                            WHEN '200' THEN 'Start the journey'
                            WHEN '300' THEN 'Onsite'
                            WHEN '400' THEN 'Closed job'
                            WHEN '500' THEN 'Send customer sign'
                            WHEN '600' THEN 'Finished'
                            ELSE ''
                        END status_display
                FROM ".$this->objCons->table_tasks_log." TL
                LEFT JOIN employee E ON E.emailaddr = TL.engineer AND E.emailaddr <> ''
                WHERE TL.tasks_sid = :tasks_sid GROUP BY engineer ORDER BY TL.create_datetime ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetchAll();
        // foreach ($r as $key => $value) {
        //     $r[$key]['each_status_of_task_engineer'] = $this->eachStatusOfTaskEngineer($tasks_sid, $value['engineer']);
        // }
        return $r;
    }

    public function updateSparePart($email,$part_number_defective,$part_serial_defective,$part_number,$part_serial,$description,$quantity,$sparepart_sid,$tasks_sid){
        $this->task_sid = $tasks_sid;
        $this->sparepart_sid = $sparepart_sid;
        $this->email = $email;
         if($email!=""){
            $this->objCons->setTable($email);
        }

        $this->part_number_defective = $part_number_defective;
        $this->part_serial_defective = $part_serial_defective;
        $this->part_number = $part_number;
        $this->part_serial = $part_serial;
        $this->description = $description;
        $this->quantity = $quantity;

        return $this->updateSparePartQuery();
    }

    private function updateSparePartQuery(){
        for ($i=0; $i < $this->quantity; $i++) {
            if($this->sparepart_sid>0){
                // UPDATE
                $sql = "UPDATE ".$this->objCons->table_task_spare." SET part_number_defective = :part_number_defective,
                part_serial_defective = :part_serial_defective,
                part_number = :part_number,
                part_serial = :part_serial,
                description = :description,
                update_datetime = NOW(),
                update_by = :email
                WHERE sid = :sparepart_sid ";

                $q = $this->db->prepare($sql);
                $q->execute(array(
                        ':part_number_defective'=>$this->part_number_defective,
                        ':part_serial_defective'=>$this->part_serial_defective,
                        ':part_number'=>$this->part_number,
                        ':part_serial'=>$this->part_serial,
                        ':description'=>$this->description,
                        ':email'=>$this->email,
                        ':sparepart_sid'=>$this->sparepart_sid
                    ));

            }else{
                //INSERT
                $sql = "INSERT INTO ".$this->objCons->table_task_spare."
                (part_number_defective,part_serial_defective,part_number,part_serial,description,create_datetime,create_by,task_sid,status,update_datetime,update_by)
                VALUES
                (:part_number_defective,:part_serial_defective,:part_number,:part_serial,:description,NOW(),:email,:task_sid,'1',NOW(),:updated_by) ";
                $q = $this->db->prepare($sql);
                $q->execute(array(
                        ':part_number_defective'=>$this->part_number_defective,
                        ':part_serial_defective'=>$this->part_serial_defective,
                        ':part_number'=>$this->part_number,
                        ':part_serial'=>$this->part_serial,
                        ':description'=>$this->description,
                        ':email'=>$this->email,
                        ':task_sid'=>$this->task_sid,
                        ':updated_by'=>$this->email
                    ));
            }
        }
        if($this->sparepart_sid>0){
            return $this->sparepart_sid;
        }else{
            return $this->db->lastInsertId();
        }
    }

    public function deleteSparePart($email, $sparepart_sid){
        $this->email = $email;
        $this->sparepart_sid = $sparepart_sid;
        if($email!=""){
            $this->objCons->setTable($email);
        }
        return $this->deleteSparePartQuery();
    }
    private function deleteSparePartQuery(){
        $sql = "UPDATE ".$this->objCons->table_task_spare." SET status = '-1', update_datetime = NOW(), update_by = :email WHERE sid = :sparepart_sid  ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':email'=>$this->email,
            ':sparepart_sid'=>$this->sparepart_sid
            ));
    }

    public function followup($email,$tasks_sid,$datetime,$source){
        if($email!=""){
            $this->objCons->setTable($email);
        }
        $this->email = $email;
        $this->task_sid = $tasks_sid;

        if($source=="webApp"){
            $this->datetime = $this->convertDatetimeWebFormatToDb($datetime);
        }else{
            $this->datetime = $this->convertDatetimeAppFormatToDd($datetime);
        }

        return $this->followupQuery();
    }

    private function followupQuery(){

        $this->addNewStatusTaskLog($this->email, $this->task_sid, '400', $this->email, "","","");

        $sql = "INSERT INTO ".$this->objCons->table_task_followup_log." (task_sid, followup_datetime, create_by, create_datetime) VALUES (:task_sid,:followup_datetime,:create_by,NOW()) ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(
            ':task_sid'=>$this->task_sid,
            ':followup_datetime'=>$this->datetime,
            ':create_by'=>$this->email
            ));
    }

    private function addNewStatusTaskLog($engineer_email, $tasks_sid, $status, $create_by, $taxi_fare ="", $lat="", $lng=""){
        $sql = "INSERT INTO ".$this->objCons->table_tasks_log." (tasks_sid,froms,engineer,create_by,create_datetime,appointment, expect_finish, status, taxi_fare,latitude,longitude)
                    SELECT tasks_sid,froms,:engineer_email, :create_by, NOW() ,appointment, expect_finish, :status, :taxi_fare,:latitude,:longitude
                    FROM ".$this->objCons->table_tasks_log." WHERE tasks_sid = :tasks_sid ORDER BY sid DESC LIMIT 0,1 ";

        $q = $this->db->prepare($sql);
        $result = $q->execute(
                array(':engineer_email'=>$engineer_email,':create_by'=>$create_by,':status'=>$status,':tasks_sid'=>$tasks_sid,':taxi_fare'=>$taxi_fare,':latitude'=>$lat,':longitude'=>$lng)
            );

        $sql = "UPDATE ".$this->objCons->table_tasks." SET last_status = :status, updated_by = :updated_by, updated_datetime = NOW(),is_follow_up = :is_follow_up WHERE  sid = :sid ";
        $q = $this->db->prepare($sql);
        $r = $q->execute(array(':status'=>$status,':updated_by'=>$create_by,':is_follow_up'=>$this->is_follow_up,':sid'=>$tasks_sid));
    }

    public function importSr($sequence_id){
        $data = $this->selectSrFromSequenceId($sequence_id);

        $dataSrOld = $this->selectOldProject($data);
        $dataSrNew = $this->selectNewTask($data);

        if( $dataSrOld['no']  ){
            //have Old SR
        }else{
             $this->insertSrOldProject($data);
        }

        if( $dataSrNew['sr_refer_remedy']){
            //have Old SR
        }else{
            $this->insertNewTask($data);
        }
    }

    private function selectOldProject($data){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_service_report WHERE no = :no ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':no'=>$data['service_report_id']));
        return $r = $q->fetch();
    }

    private function insertSrOldProject($data){
        $sql = "INSERT INTO flguploa_firstlogicis.tbl_service_report
            (`no`, `company_id`, `type`, `times`, `customer_company_id`, `customer_company_branch_id`, `engineer_id`, `service_job_type_id`, `product_id`, `status`, `contract_no`, `request_no`, `approve_time`, `symtom`, `action`, `note`, `additive_mail`, `suggestion`, `appointment_time`, `expect_duration`, `follow`, `follow_date`, `follow_reason`, `has_replace_part`, `close_job_time`, `last_update`, `picture_path`, `contact_person_first_name`, `contact_person_last_name`, `contact_person_phone_1`, `contact_person_phone_2`, `contact_person_email`, `has_overscope`, `is_signed`, `is_back_home`, `is_keep_working`, `updatetimestamp`, `assign_times`, `last_notification_timestamp_onsite`, `last_notification_timestamp_close_job`, `last_notification_timestamp_customer_sign`, `last_notification_timestamp_finish`, `taxi_go_trip_fare`, `taxi_return_trip_fare`, `create_by`, `create_time`, `modified_by`, `modified_time`, `is_cancel`)
            VALUES
            (':no','1',
             ':type','1',
             ':customer_company_id',
             ':customer_company_branch_id',
             ':engineer_id',
             ':service_job_type_id',
             ':product_id',
             ':status',
             ':contract_no',
             ':request_no',
             ':approve_time',
             ':symtom',
             ':action',
             ':note',
             ':additive_mail',
             ':suggestion',
             ':appointment_time',
             ':expect_duration',
             ':follow',
             ':follow_date',
             ':follow_reason',
             ':has_replace_part',
             ':close_job_time',
             ':last_update',
             ':picture_path',
             ':contact_person_first_name',
             ':contact_person_last_name',
             ':contact_person_phone_1',
             ':contact_person_phone_2',
             ':contact_person_email',
             ':has_overscope',
             ':is_signed',
             ':is_back_home',
             ':is_keep_working',
             ':updatetimestamp',
             ':assign_times',
             ':last_notification_timestamp_onsite',
             ':last_notification_timestamp_close_job',
             ':last_notification_timestamp_customer_sign',
             ':last_notification_timestamp_finish',
             ':taxi_go_trip_fare',
             ':taxi_return_trip_fare',
             ':create_by', NOW(),
             ':modified_by', NOW(), '0') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':no'=>$data['service_report_id'],
            ':type'=>'',
            ':customer_company_id'=>$this->getEndUserCompanyOldProject($data['end_user_company']),
            ':customer_company_branch_id'=>$this->getSiteOldProject($data['end_user_site']),
            ':engineer_id'=>$this->getEmpnoOldProject($data['assign_individual']),
            ':service_job_type_id'=>'10',
            ':product_id'=>'',
            ':status'=>'0',
            ':contract_no'=>$data['contract_no'],
            ':request_no'=>$data['case_id'],
            ':approve_time'=>'',
            ':symtom'=>$data['symtom'],
            ':action'=>'',
            ':note'=>'',
            ':additive_mail'=>'',
            ':suggestion'=>'',
            ':appointment_time'=>$data['appointed_time'],
            ':expect_duration'=>'',
            ':follow'=>'',
            ':follow_date'=>'',
            ':follow_reason'=>'',
            ':has_replace_part'=>'',
            ':close_job_time'=>'',
            ':last_update'=>'',
            ':picture_path'=>'',
            ':contact_person_first_name'=>$data['end_user_name'],
            ':contact_person_last_name'=>'',
            ':contact_person_phone_1'=>$data['end_user_phone'],
            ':contact_person_phone_2'=>'',
            ':contact_person_email'=>$data['end_user_email'],
            ':has_overscope'=>'',
            ':is_signed'=>'',
            ':is_back_home'=>'',
            ':is_keep_working'=>'',
            ':updatetimestamp'=>'',
            ':assign_times'=>'',
            ':last_notification_timestamp_onsite'=>'',
            ':last_notification_timestamp_close_job'=>'',
            ':last_notification_timestamp_customer_sign'=>'',
            ':last_notification_timestamp_finish'=>'',
            ':taxi_go_trip_fare'=>'',
            ':taxi_return_trip_fare'=>'',
            ':create_by'=>'',
            ':modified_by'=>'',
            ));
        // $q->fetch();
    }

    private function selectNewTask($data){
        $sql = "SELECT * FROM ".$this->objCons->table_tasks." WHERE sr_refer_remedy = :sr_refer_remedy ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sr_refer_remedy'=>$data['service_report_id']));
        return $r = $q->fetch();
    }

    private function insertNewTask($data){
        if($this->getTicketSidFromHD($data['case_id'])){
        $sql = "INSERT INTO ".$this->objCons->table_tasks." ( no_task, sr_refer_remedy,  ticket_sid, create_by, create_datetime, appointment, expect_duration, expect_finish,  service_type_refer_remedy, subject_service_report, end_user_contact_name_service_report, end_user_phone_service_report,  end_user_email_service_report, end_user_company_name_service_report, last_status, updated_by ) VALUES (:no_task,:sr_refer_remedy, :ticket_sid, :create_by,NOW(), :appointment, :expect_duration, :expect_finish,
                     :service_type_refer_remedy, :subject_service_report, :end_user_contact_name_service_report, :end_user_phone_service_report,
                     :end_user_email_service_report, :end_user_company_name_service_report,
                     :last_status, :updated_by )";
            $q = $this->db->prepare($sql);
            $q->execute( array(
                ':no_task'=> genTaskNo($this->db),
                ':sr_refer_remedy'=> $data['service_report_id'],
                ':ticket_sid'=> $this->getTicketSidFromHD($data['case_id']),
                ':create_by'=> $this->helpGetEmailFromThainame($data['assign_individual']),
                ':appointment'=> $this->convertYearToInter($data['appointed_time']),
                ':expect_duration'=>$data['expect_finish'],
                ':expect_finish'=> $data['appointed_time'],
                ':service_type_refer_remedy' => $data['service_type'],
                ':subject_service_report' => trim($data['symtom']),
                ':end_user_contact_name_service_report' => trim($data['end_user_name']),
                ':end_user_phone_service_report' => trim($data['end_user_phone']),
                ':end_user_email_service_report' => trim($this->splitPhoneEmail($data['end_user_phone'])),
                ':end_user_company_name_service_report' => trim($data['end_user_company']),
                ':last_status' => '',
                ':updated_by' => ''
            ));
        }
    }

    private function getEndUserCompanyOldProject($data){
        $sql = "SELECT * from flguploa_firstlogicis.tbl_customer_company where name_th = '".$data."'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;

    }
    private function getSiteOldProject($data){
        $sql = "SELECT * from flguploa_firstlogicis.tbl_customer_company_branch where address = '".$data."'";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;
    }


    private function getEmpnoFromEmailOldProject($data){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_employee WHERE email = '".$data."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;
    }

    private function getTicketSidFromHD($data){ //find ticket_sid from HD
        $sql = "SELECT * FROM ".$this->objCons->table_ticket." WHERE refer_remedy_hd = :ticket_no";
        $q = $this->db->prepare($sql);
        $q->execute(array( ':ticket_no' => $data['case_id']));
        $r = $q->fetch();
        if($r['sid']){
            return $r['sid'];
        }
        return NULL;
    }

    private function getEmpnoOldProject($data){
        $sql = "SELECT * FROM flguploa_firstlogicis.tbl_employee WHERE CONCAT(first_name_th,'  ',last_name_th) = '".$data."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        if($r['id']){
            return $r['id'];
        }
        return NULL;
    }
    private function selectSrFromSequenceId($sequence_id){
        $sql = "SELECT * FROM test_create_sr WHERE sid = '".$sequence_id."' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        return $q->fetch();
    }

    public function insertToTestCreateHd($Subject,$Case_ID,$Urgency,$Requester_Name,$Requester_Phone,$Requester_Mobile,$Requester_EMail,$Requester_Company_Name,$EndUser_Site,$EndUser_Name,$EndUser_Phone,$EndUser_Company_Name,$Contract_No,$Serial_No,$Incharge,$Description,$Status,$Assigned_Time,$Report_By,$Create_Time,$Prime_Contract,$Case_Type,$First_Assigned_Time,$Work_Log,$Expect_Pending_Finish, $Last_Modified_By=""){
        // $objConnect = mysql_connect("127.0.0.1","flguploa","sss123") or die(mysql_error());

        // $objDB = mysql_select_db("flguploa_cases");
        // mysql_query("SET NAMES UTF8");
        // mysql_query("SET character_set_results=utf8");
        // mysql_query("SET character_set_client=utf8");
        // mysql_query("SET character_set_connection=utf8");

        // $strSQL = "SELECT sid FROM test_create_case WHERE case_id = '".$Case_ID."' ";
        // $objQuery = mysql_query($strSQL);
        // $row = mysql_fetch_assoc($objQuery);

        $strSQL = "INSERT INTO test_create_case
                (subject, case_type, create_by, create_datetime, case_id, urgency, requester_name, requester_phone, requester_mobile, requester_email, requester_company_name, enduser_site, enduser_name, enduser_phone, enduser_company_name, contract_no, serial_no, incharge, description, status, assigned_time, report_by, create_time, prime_contract, first_assigned_time, work_log,encoding,expect_pending_finish,last_modified_by)
                VALUES
                (:subject,:case_type,'',NOW(),:Case_ID,:Urgency,:Requester_Name,:Requester_Phone,:Requester_Mobile,:Requester_EMail,:Requester_Company_Name,:EndUser_Site,:EndUser_Name,:EndUser_Phone,:EndUser_Company_Name,:Contract_No,:Serial_No,:Incharge,:Description,:Status,:Assigned_Time,:Report_By,:Create_Time,:Prime_Contract,:First_Assigned_Time,:Work_Log, :encoding,:Expect_Pending_Finish,:Last_Modified_By)";

        // $objQuery = mysql_query($strSQL) or die (mysql_error());
        $q = $this->db->prepare($strSQL);
        return $q->execute(array(
                ':subject'=>$Subject,
                ':case_type'=>$Case_Type,
                ':Case_ID'=>$Case_ID,
                ':Urgency'=>$Urgency,
                ':Requester_Name'=>$Requester_Name,
                ':Requester_Phone'=>$Requester_Phone,
                ':Requester_Mobile'=>$Requester_Mobile,
                ':Requester_EMail'=>$Requester_EMail,
                ':Requester_Company_Name'=>$Requester_Company_Name,
                ':EndUser_Site'=>$EndUser_Site,
                ':EndUser_Name'=>$EndUser_Name,
                ':EndUser_Phone'=>$EndUser_Phone,
                ':EndUser_Company_Name'=>$EndUser_Company_Name,
                ':Contract_No'=>$Contract_No,
                ':Serial_No'=>$Serial_No,
                ':Incharge'=>$Incharge,
                ':Description'=>$Description,
                ':Status'=>$Status,
                ':Assigned_Time'=>$Assigned_Time,
                ':Report_By'=>$Report_By,
                ':Create_Time'=>$Create_Time,
                ':Prime_Contract'=>$Prime_Contract,
                ':First_Assigned_Time'=>$First_Assigned_Time,
                ':Work_Log'=>$Work_Log,
                ':encoding'=>mb_detect_encoding($Assign_individual),
                ':Expect_Pending_Finish'=>$Expect_Pending_Finish,
                ':Last_Modified_By'=>$Last_Modified_By
            ));
    }

    public function checkRepeatHd(){
         $strSQL = "SELECT sid FROM test_create_case WHERE case_id = :case_id ";
         $q = $this->db->prepare($strSQL);
         $q->execute(array(':case_id'=>$Case_ID));
         $r = $q->fetch();
         return $r;
    }

    public function insertToTestCreateSr($Service_Report_ID,$Case_ID,$Symtom,$Contract_No,$End_User_Company,$End_User_Site,$End_User_Name,$End_User_Phone,$Assign_individual,$Involved_Eng2,$Involved_Eng3,$Involved_Eng4,$Involved_Eng5,$Appointed_time,$Service_Type,$Expect_Finish,$Submitted_by,$Serial,$Detail,$Status){

        $strSQL = "INSERT INTO test_create_sr (
        create_datetime, service_report_id, case_id, symtom, contract_no, end_user_company, end_user_site, end_user_name, end_user_phone, end_user_email, assign_individual, involved_eng2, involved_eng3, involved_eng4, involved_eng5, appointed_time, service_type, expect_finish, submitted_by, serial_no, detail, status, encoding)
        VALUES (
        NOW(),:Service_Report_ID,:Case_ID,:Symtom,:Contract_No,:End_User_Company,:End_User_Site,:End_User_Name,:End_User_Phone,'',:Assign_individual,:Involved_Eng2,:Involved_Eng3,:Involved_Eng4,:Involved_Eng5,:Appointed_time,:Service_Type,:Expect_Finish,:Submitted_by,:Serial_no,:Detail,:Status,:encoding)";

        $q = $this->db->prepare($strSQL);
        return $q->execute(array(
                ':Service_Report_ID'=>$Service_Report_ID,
                ':Case_ID'=>$Case_ID,
                ':Symtom'=>$Symtom,
                ':Contract_No'=>$Contract_No,
                ':End_User_Company'=>$End_User_Company,
                ':End_User_Site'=>$End_User_Site,
                ':End_User_Name'=>$End_User_Name,
                ':End_User_Phone'=>$End_User_Phone,
                ':End_User_Phone'=>$End_User_Phone,
                ':Assign_individual'=>$Assign_individual,
                ':Involved_Eng2'=>$Involved_Eng2,
                ':Involved_Eng3'=>$Involved_Eng3,
                ':Involved_Eng4'=>$Involved_Eng4,
                ':Involved_Eng5'=>$Involved_Eng5,
                ':Appointed_time'=>$Appointed_time,
                ':Service_Type'=>$Service_Type,
                ':Expect_Finish'=>$Expect_Finish,
                ':Submitted_by'=>$Submitted_by,
                'Serial_no'=>$Serial,
                'Detail'=>$Detail,
                'Status'=>$Status,
                ':encoding'=>mb_detect_encoding($Assign_individual),
            ));

    }

    public function convertDatetimeAppFormatToDd($datetime){
        $datetime = explode(" ", $datetime);
        return $datetime = $datetime['3']."-".$this->getNumberMonthFromShortMonth($datetime['1'])."-".$datetime['2']." ".$datetime['4'];
    }

    public function convertDatetimeWebFormatToDb($datetime){
        $datetime = explode(" ", $datetime);
        $date = explode("/", $datetime[0]);
        return $date['2']."-".$date['1']."-".$date['0']." ".$datetime['1'];
    }
    private function getNumberMonthFromShortMonth($shortMonth){
        if($shortMonth=="Jan"){
            return "01";
        }else if($shortMonth=="Feb"){
            return "02";
        }else if($shortMonth=="Mar"){
            return "03";
        }else if($shortMonth=="Apr"){
            return "04";
        }else if($shortMonth=="May"){
            return "05";
        }else if($shortMonth=="Jun"){
            return "06";
        }else if($shortMonth=="Jul"){
            return "07";
        }else if($shortMonth=="Aug"){
            return "08";
        }else if($shortMonth=="Sep"){
            return "09";
        }else if($shortMonth=="Oct"){
            return "10";
        }else if($shortMonth=="Nov"){
            return "11";
        }else if($shortMonth=="Dec"){
            return "12";
        }else{
            return "01";
        }
    }

    public function caseWaitSendToCCD(){
        $sql = "SELECT * FROM ticket_status WHERE data_from = '' AND send_to_ccd = '0' ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return $r;
    }


    /// SEND SLA TO REMEDT //
    public function updateStatusCaseToRemedy($ticket_sid, $email, $work_log,$expect_pending, $case_status_sid, $solution="", $solution_detail=""){
        if($email!=""){
            $this->objCons->setTable($email);
        }
        if($case_status_sid=="2" || $case_status_sid=="3" || $case_status_sid=="4" || $case_status_sid=="5" || $case_status_sid=="7" || $case_status_sid=="8" || $case_status_sid=="10" || $case_status_sid=="11"){
            $this->ticket_sid = $ticket_sid;
            $this->email = $email;
            $this->work_log = $work_log;

            if($expect_pending!=""){
                $this->expect_pending = str_replace(" ", "T", $expect_pending);
                $this->expect_pending .= "+07:00";
            }else{
                $this->expect_pending = "";
            }
            $this->status = $this->selectStatusName($case_status_sid);

            $this->refer_remedy_hd = $this->selectReferRemedyHdFromTicketSid($this->ticket_sid);
            $remedy_password = $this->selectRemedyPassword($this->email);
            $empno = $this->selectEmpnoFromEmail($this->email);

            if($remedy_password=="" && $empno==""){
                $remedy_password = "1234";
                $empno = "005686";
            }


            //UPDATE 24/11/2016 By Mas.
            $dataPost = array (
                'Expect_Pending_Finish' => $this->expect_pending,
                'StampSLA_Mobile'=>$this->status,
                'Work_Log'=>$this->work_log,
                'Case_ID'=>$this->refer_remedy_hd,
                'userName'=>$empno,
                'password'=>$remedy_password,
                'Solution'=>$solution,
                'Solution_Detail'=>$solution_detail
                );
            $dataPost = http_build_query($dataPost);
            $context_options = array (
                'http' => array (
                    'method' => 'POST',
                    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                        . "Content-Length: " . strlen($dataPost) . "\r\n",
                    'content' => $dataPost
                )
            );
            $context = stream_context_create($context_options);

            // require_once dirname(__FILE__) . '/ws_remedy/updateStatusCase.php';
            // $resultRemedy = updateStatusCaseToRemedy($this->status,$this->work_log,$this->refer_remedy_hd,$this->expect_pending, $empno, $remedy_password, $solution, $solution_detail);
            // ?StampSLA_Mobile=".$this->status."&Work_Log=".$this->work_log."&Case_ID=".$this->refer_remedy_hd."&Expect_Pending_Finish=".str_replace("+", "|", $this->expect_pending)."&userName=".$empno."&password=".$remedy_password."&Solution=".$solution."&Solution_Detail=".$solution_detail
            $urlToRemedy = "http://flgupload.com/updateSlaToRemedy.php";
            // echo $urlToRemedy;
            $html = file_get_html($urlToRemedy, false, $context)->plaintext;

            // print_r($resultRemedy);
            // if(isset($resultRemedy->Modified_Date) && $resultRemedy->Modified_Date){

            // }else{
            if($this->refer_remedy_hd!=""){
                $sql = "INSERT INTO response_remedy (response_data,create_datetime,other, create_by, stamp_sla, worklog, solution, solution_detail, expect_pending_finish) VALUES (:response_data, NOW(), :other, :create_by, :stamp_sla, :worklog, :solution, :solution_detail, :expect_pending_finish) ";
                $q = $this->db->prepare($sql);
                $q->execute(array(
                    ':response_data'=>$html,':other'=>$this->refer_remedy_hd,
                    ':create_by'=>$this->email,':stamp_sla'=>$this->status, ':worklog'=>$this->work_log,
                     ':solution'=>$solution, ':solution_detail'=>$solution_detail,':expect_pending_finish'=>$this->expect_pending
                     ));
            }
            // }
        }
    }

    private function selectRemedyPassword($email){
        $sql = "SELECT remedy_password FROM user WHERE email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['remedy_password'];
    }

    private function selectEmpnoFromEmail($email){
        $sql = "SELECT empno FROM employee WHERE emailaddr = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetch();
        return $r['empno'];
    }

    public function selectReferRemedyHdFromTicketSid($ticket_sid){
        $sql = "SELECT refer_remedy_hd FROM ".$this->objCons->table_ticket." WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_sid));
        $r = $q->fetch();
        return $r['refer_remedy_hd'];
    }

    public function createOracleSr($email, $case_oracle_sr, $token, $ticket_sid, $spare_sid=""){
        $data['ticket_sid'] = $ticket_sid;
        $data['sr'] = $case_oracle_sr['sr']['no_sr'];
        $data['model'] = $case_oracle_sr['sr']['model'];
        $data['type'] = $case_oracle_sr['sr']['type'];
        $data['oracle_create_date'] = $case_oracle_sr['sr']['created_date'];

        $data['task'] = $case_oracle_sr['task']['no_task'];

        $data['part_number'] = $case_oracle_sr['part']['part_number'];
        $data['request_spare_part_date'] = $case_oracle_sr['part']['request_spare_part_date'];
        $data['delivery_due_date'] = $case_oracle_sr['part']['delivery_due_date'];
        $data['part_description'] = $case_oracle_sr['part']['part_description'];
        $data['receive_spare_part_date'] =$case_oracle_sr['part']['receive_spare_part_date'];
        $data['part_quality'] =$case_oracle_sr['part']['part_quantity'];

        $data['spare_sid'] = $spare_sid;
        $data['spare_task_sid'] = "";
        $data['spare_part_sid'] = "";
        $this->createPart($email, $data, $token);

    }
    public function createOracleOnlyPart($email, $data, $token, $ticket_sid, $spare_sid, $spare_task_sid){
        $data['ticket_sid'] = $ticket_sid;

        $data['part_number'] = $data['part_number'];
        $data['request_spare_part_date'] = $data['request_spare_part_date'];
        $data['delivery_due_date'] = $data['delivery_due_date'];
        $data['part_description'] = $data['part_description'];
        $data['receive_spare_part_date'] =$data['receive_spare_part_date'];
        $data['part_quality'] =$data['part_quantity'];

        $data['spare_sid'] = $spare_sid;
        $data['spare_task_sid'] = $spare_task_sid;
        $data['spare_part_sid'] = "";
        $this->createPart($email, $data, $token);
    }
    public function createPart($email, $data, $token){

        $this->objCons->setTable($email);
        $this->ticket_sid = $data['ticket_sid'];
        $this->email = $email;
        $this->sr = $data['sr'];
        $this->task = $data['task'];
        $this->type = $data['type'];
        $this->oracle_create_date = $data['oracle_create_date'];
        $this->request_spare_part_date = $data['request_spare_part_date'];
        $this->delivery_due_date = $data['delivery_due_date'];
        $this->model = $data['model'];
        $this->part_number = $data['part_number'];
        $this->part_description = $data['part_description'];
        $this->receive_spare_part_date = $data['receive_spare_part_date'];

        $this->ticket_spare_sid = $data['spare_sid'];
        $this->ticket_spare_task_sid = $data['spare_task_sid'];

        $this->ticket_spare_part_sid = $data['spare_part_sid'];

        if($this->ticket_spare_task_sid){
	        $this->addOraclePart($data['part_quality']);
        }else if($this->ticket_spare_sid){
        	$res = $this->addOracleTask();
        	if($res){
	            $this->addOraclePart($data['part_quality']);
        	}
        }else{
	        $res = $this->addOracleSr();
	        if($res){
	            $res = $this->addOracleTask();
	            if($res){
	            	$this->addOraclePart($data['part_quality']);
	            }
	        }
	    }
    }

    public function editPart($email, $data, $token){
    	$this->objCons->setTable($email);
    	$field = $data['field'];
    	$value = $data['value_change'];
    	$sid = $data['spare_part_sid'];

    	$sql = "UPDATE ".$this->objCons->table_ticket_spare_part." SET ".$field." = :value, update_by = :update_by, update_datetime = NOW() WHERE sid = :sid ";
    	$q = $this->db->prepare($sql);
    	return $q->execute(array(':value'=>$value,':update_by'=> $email,':sid'=>$sid));
    }
	public function editSpareTask($email, $data, $token){
		$this->objCons->setTable($email);

		$this->objCons->setTable($email);
    	$field = $data['field'];
    	$value = $data['value_change'];
    	$sid = $data['spare_task_sid'];

    	$sql = "UPDATE ".$this->objCons->table_ticket_spare_task." SET ".$field." = :value, update_by = :update_by, update_datetime = NOW() WHERE sid = :sid ";
    	$q = $this->db->prepare($sql);
    	$q->execute(array(':value'=>$value,':update_by'=> $email,':sid'=>$sid));
	}

    public function addOracleSr(){

        if($this->sr!="" && !$this->checkCreateOracleSrRepeat()){
            $sql = "INSERT INTO ".$this->objCons->table_ticket_spare." (ticket_sid, oracle_sr_type, oracle_sr, oracle_create_date, model, create_datetime, create_by, update_datetime, update_by) VALUES (:ticket_sid, :oracle_sr_type, :oracle_sr, :oracle_create_date, :model, NOW(), :create_by, NOW(), :update_by) ";

            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':ticket_sid'=>$this->ticket_sid,
                ':oracle_sr_type'=>$this->type,
                ':oracle_sr'=>$this->sr,
                ':oracle_create_date'=>$this->oracle_create_date,
                ':model'=>$this->model,
                ':create_by'=>$this->email,
                ':update_by'=>$this->email
                ));
            $this->ticket_spare_sid = $this->db->lastInsertId();
            return true;
        }
        return false;
    }


    public function addOracleTask(){
        if($this->ticket_spare_sid!="" && !$this->checkCreateOracleTaskRepeat() && $this->task !=""){
            $sql = "INSERT INTO ".$this->objCons->table_ticket_spare_task." (ticket_spare_sid, oracle_task, create_datetime, create_by, update_datetime, update_by)
            VALUES (:ticket_spare_sid, :oracle_task, NOW(), :create_by, NOW(), :update_by) ";

            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':ticket_spare_sid'=>$this->ticket_spare_sid,
                ':oracle_task'=>$this->task,
                ':create_by'=>$this->email,
                ':update_by'=>$this->email
                ));
            $this->ticket_spare_task_sid = $this->db->lastInsertId();
            return true;
        }
        return false;
    }
    public function addOraclePart($part_quality){
        if($this->ticket_spare_task_sid!="" && !$this->checkCreateOraclePartRepeat() && $this->part_number !=""){
            $sql = "INSERT INTO ".$this->objCons->table_ticket_spare_part." (ticket_spare_task_sid, pn, part_description, create_datetime, create_by, update_datetime, update_by,request_spare_part_date, delivery_due_date,receive_spare_part_date)
            VALUES (:ticket_spare_task_sid, :pn, :part_description, NOW(), :create_by, NOW(), :update_by, :request_spare_part_date,:delivery_due_date,:receive_spare_part_date) ";

            for ($i=0; $i < $part_quality; $i++) {
            	$q = $this->db->prepare($sql);
	            $q->execute(array(
	                ':ticket_spare_task_sid'=>$this->ticket_spare_task_sid,
	                ':pn'=>$this->part_number,
	                ':part_description'=>$this->part_description,
	                ':create_by'=>$this->email,
	                ':update_by'=>$this->email,
	                ':request_spare_part_date'=>$this->request_spare_part_date,
	                ':delivery_due_date'=>$this->delivery_due_date,
	                ':receive_spare_part_date'=>$this->receive_spare_part_date
	            ));
            }

            return true;
        }
        return false;
    }

    public function updatePart(){

    }

    private function checkCreateOracleSrRepeat(){
        $sql = "SELECT T.sid FROM ".$this->objCons->table_ticket_spare." T WHERE T.create_by = :create_by AND T.ticket_sid = :ticket_sid AND TIMESTAMPDIFF(MINUTE,T.create_datetime,NOW()) < 2 AND oracle_sr = :oracle_sr ORDER BY T.sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':create_by'=>$this->email, ':ticket_sid'=>$this->ticket_sid,':oracle_sr'=>trim($this->sr)));
        $r = $q->fetch();
        if($r['sid']>0){
            return true;
        }
        return false;
    }

    private function checkCreateOracleTaskRepeat(){
        $sql = "SELECT T.sid FROM ".$this->objCons->table_ticket_spare_task." T WHERE T.create_by = :create_by AND T.ticket_spare_sid = :ticket_spare_sid AND TIMESTAMPDIFF(MINUTE,T.create_datetime,NOW()) < 2 AND oracle_task = :oracle_task ORDER BY T.sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':create_by'=>$this->email, ':ticket_spare_sid'=>$this->ticket_spare_sid,':oracle_task'=>trim($this->task)));
        $r = $q->fetch();
        if($r['sid']>0){
            return true;
        }
        return false;
    }
    private function checkCreateOraclePartRepeat(){
        $sql = "SELECT T.sid FROM ".$this->objCons->table_ticket_spare_part." T WHERE T.create_by = :create_by AND T.ticket_spare_task_sid = :ticket_spare_task_sid AND TIMESTAMPDIFF(MINUTE,T.create_datetime,NOW()) < 2 AND pn = :pn ORDER BY T.sid DESC LIMIT 0,1 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':create_by'=>$this->email, ':ticket_spare_task_sid'=>$this->ticket_spare_task_sid,':pn'=>trim($this->part_number)));
        $r = $q->fetch();
        if($r['sid']>0){
            return true;
        }
        return false;
    }

    public function listOracleSr($email,$ticket_sid, $token){
        $this->objCons->setTable($email);

        $sql = "SELECT * FROM ".$this->objCons->table_ticket_spare." WHERE ticket_sid = :ticket_sid ORDER BY create_datetime DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    public function viewOracleSr($email, $spare_sid, $token, $ticket_sid, $spare_task_sid=""){
        $this->objCons->setTable($email);
        if($spare_task_sid!=""){
            return $this->listPartInTask($spare_task_sid, $ticket_sid);
        }
        $sql = "SELECT *,(select oracle_sr from ".$this->objCons->table_ticket_spare." TTS where TTS.sid = :sid) oracle_sr FROM ".$this->objCons->table_ticket_spare_task." ST WHERE ticket_spare_sid = :sid ORDER BY ST.create_datetime DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$spare_sid));

        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $r[$key]['part'] = $this->listPartInTask($value['sid'], $ticket_sid);
            $r[$key]['ticket_sid'] = $ticket_sid;
        }
        return $r;
    }

    public function viewDataOracleSr($email, $spare_sid, $token, $ticket_sid, $spare_task_sid=""){
        $this->objCons->setTable($email);

        $sql = "SELECT * FROM ".$this->objCons->table_ticket_spare." WHERE sid = :sid ORDER BY create_datetime DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$spare_sid));
        return $q->fetch();
    }

    public function viewDataOracleTask($email, $spare_sid, $token, $ticket_sid, $spare_task_sid=""){
        $this->objCons->setTable($email);

        $sql = "SELECT * FROM ".$this->objCons->table_ticket_spare_task." WHERE sid = :sid ORDER BY create_datetime DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$spare_task_sid));
        return $q->fetch();
    }

    public function partIsReturn($email, $array_part_sid){
        $this->objCons->setTable($email);
        foreach ($array_part_sid as $key => $value) {
            $sql = "UPDATE ".$this->objCons->table_ticket_spare_part." SET is_return = '1' WHERE sid = :sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$value));
        }
    }
    public function viewOraclePart($email, $spare_sid, $token, $ticket_sid, $spare_task_sid=""){
        $sql = "SELECT SP.*, DATE_FORMAT(request_spare_part_date,'%Y-%m-%d') request_spare_part_date,
                    DATE_FORMAT(delivery_due_date,'%Y-%m-%d') delivery_due_date,
                    DATE_FORMAT(receive_spare_part_date,'%Y-%m-%d') receive_spare_part_date,
                    DATE_FORMAT(return_spare_part_date,'%d-%m-%Y') return_spare_part_date,
                    E.emailaddr email, E.thainame thainame
            FROM ".$this->objCons->table_ticket_spare_part." SP
             LEFT JOIN employee E ON SP.create_by = E.emailaddr
             WHERE ticket_spare_task_sid = :spare_task_sid AND spare_part_return_type <> '' AND return_spare_part_date <> '0000-00-00 00:00:00' AND is_return = '0'
             ORDER BY SP.return_spare_part_date DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':spare_task_sid'=>$spare_task_sid));
        return $r = $q->fetchAll();
    }

    public function listPartInTask($ticket_spare_task_sid, $ticket_sid){
        $sql = "SELECT SP.*, DATE_FORMAT(request_spare_part_date,'%Y-%m-%d') request_spare_part_date,
        DATE_FORMAT(delivery_due_date,'%Y-%m-%d') delivery_due_date,
        DATE_FORMAT(receive_spare_part_date,'%Y-%m-%d') receive_spare_part_date,
        DATE_FORMAT(return_spare_part_date,'%Y-%m-%d') return_spare_part_date,
        E.emailaddr email, E.thainame thainame
         FROM ".$this->objCons->table_ticket_spare_part." SP
         LEFT JOIN employee E ON SP.create_by = E.emailaddr
         WHERE ticket_spare_task_sid = :ticket_spare_task_sid ORDER BY SP.create_datetime DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_spare_task_sid'=>$ticket_spare_task_sid));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
        	$r[$key]['ticket_sid'] = $ticket_sid;
        }
        return $r;
    }

    public function getInfoEmployee($email){
        return $this->objCons->getInfoEmployee($email);
    }

    public function oraclePart($ticket_spare_part_sid){
        $this->updateStatusPartIsReturn($ticket_spare_part_sid);

        $sql = "SELECT *,DATE_FORMAT(TSP.return_spare_part_date,'%d-%m-%Y') return_spare_part_date_format_subject FROM ticket_spare_part TSP
        LEFT JOIN ticket_spare_task TST ON TST.sid = TSP.ticket_spare_task_sid
        LEFT JOIN ticket_spare TS ON TST.ticket_spare_sid = TS.sid
        WHERE TSP.sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_spare_part_sid));
        return $q->fetch();
    }

    private function updateStatusPartIsReturn($ticket_spare_part_sid){
        $sql = "UPDATE ticket_spare_part SET is_return = 1 WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_spare_part_sid));

    }
    //[used to get list ticket that user can see!]
    public function tickets($permission, $data, $email, $search , $role_sid=0, $search_refer_remedy_hd = ''){
        try{
            $queryStatus = 'AND T.status IN (1,2,3,4,7,8,9,10,11) ';
            $filter_refer_remedy_hd = !empty($search_refer_remedy_hd)?" AND T.refer_remedy_hd LIKE :filter_refer_remedy_hd ":'';

            if($data['status']==500){
                $queryStatus = 'AND T.status IN (5, 6, 12) ';
            }else if($data['status']==800){
                $queryStatus = 'AND (T.sla_remedy LIKE "%missed%" OR T.sla_remedy LIKE "%miss%" ) ';
            }else if($data['status']==400){
                $queryStatus = 'AND T.status IN (4) ';
            }else if($data['status']==1000){
                $queryStatus = '';
            }else if($data['status']==1){
                $queryStatus = 'AND T.status IN (1,2,4)';
            }

            $permissionQuery = ' AND (T.owner = :owner OR T.refer_remedy_report_by = :refer_remedy_report_by OR T.create_by = :create_by OR U.email_2 = :owner OR U.email = :owner) ';
            if($permission=="0" || $permission==0 || !$permission){

            }else{
                $permissionQuery = 'AND (U.role_sid IN ('.$permission.') OR T.owner = :owner OR U.email_2 = :owner OR T.refer_remedy_report_by = :refer_remedy_report_by OR T.create_by = :create_by OR U.email = :owner)';
            }

            $searchQuery = '';
            if(!empty($search)){
                $searchQuery = ' AND (T.subject LIKE :search OR T.no_ticket LIKE :search OR T.refer_remedy_hd LIKE :search OR U.formal_name LIKE :search OR U.email LIKE :search) ';
            }

            $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';

            // print_r($data);
            $type = "";
            if(isset($data['type']) && $data['type']!=""){
                $explodeType = explode(",",$data['type']);
                foreach($explodeType as $key => $value){
                    if($key!==0){
                        $type .= ",";
                    }
                    $type .= "'".$value."'";
                }
            }

            $queryType = '';
            if($type!==""){
                $queryType = "AND case_type IN (".$type.") ";
            }
            $statusLabel =',
            CASE WHEN T.status = "0" THEN "New" 
                 WHEN T.status = "1" THEN "Assigned" 
                 WHEN T.status = "2" THEN "In Progress" 
                 WHEN T.status = "4" THEN "Pending" 
                 WHEN T.status = "5" THEN "Resolved" 
                 WHEN T.status = "6" THEN "Closed"
            ELSE "Process" END status_label ';

            // 500 resolve? close?, 1000 all , 800, miss sla, 400 pending, 1 process
            $sqlCreateDate = "";
            if(isset($data['startDate']) && isset($data['endDate'])){
                $sqlCreateDate .= "AND (T.create_datetime BETWEEN '".$data['startDate']."' AND '".$data['endDate']."') ";
            }


            // select * from ticket t left join user u on t.email_owner = u.email
            // where u.email = email_request_data or u.role_sid in array permission
            $sql = 'SELECT T.sid, T.note, T.owner, T.sid ticket_sid,T.end_user,T.refer_remedy_hd,T.no_ticket,T.subject,T.contract_no,T.case_type,T.create_datetime,T.subject name, T.incident_location,
            T.request_type, T.number_of_device, T.additional_status, T.severity, CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name ';
            $sql .= ', (SELECT count(ticket_sid) FROM tasks WHERE ticket_sid = T.sid and file_name_signated != "" and file_name_signated is not NULL AND appointment_type!= -1) numberSrDone';
            $sql .= ',CASE WHEN U3.formal_name <> "" THEN U3.formal_name ELSE U3.name END report_by_name ';
            $sql .= $create_datetime;
            $sql .= ',T.end_user,T.end_user_site ';
            $sql .= ',CASE WHEN U.email IS NULL THEN T.owner ELSE (CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END) END owner_thainame ';
            $sql .= $statusLabel;
            $sql .= ',U.picture_profile owner_picture_profile ';
            $sql .= ',"" expect_pending ';
            $sql .= ',T.number_sr number_service_report, T.team ';
            $sql .= 'FROM ticket T ';
            $sql .= 'LEFT JOIN user U2 ON U2.email = T.create_by ';
            $sql .= 'LEFT JOIN user U ON U.email = T.owner OR T.owner = U.email_2 ';
            $sql .= 'LEFT JOIN user U3 ON U3.email = T.refer_remedy_report_by ';
            $sql .= 'WHERE 1 ';
            $sql .= $filter_refer_remedy_hd;
            // filter tickek remedy INC >= INC000001029300 and Request Ticket (not INC)
            $sql .= 'AND ((refer_remedy_hd  not between "0" and "INC000001029300") or refer_remedy_hd is null) ';
            $sql .= $permissionQuery;
            $sql .= $queryStatus;
            $sql .= $queryType;
            $sql .= $searchQuery;
//            $sql .= $search;
            $sql .= $sqlCreateDate;
            $sql .= 'ORDER BY T.sid DESC LIMIT '.$data['dataStart'].','.$data['dataLimit'].' ';
            $params = array(
                ':owner'=>$email,
                ':refer_remedy_report_by'=>$email,
                ':create_by'=>$email
            );
            !empty($search_refer_remedy_hd)&&$params[':filter_refer_remedy_hd'] = "%".$search_refer_remedy_hd."%";
            !empty($search)&&$params[':search'] = "%".$search."%";
            $q = $this->db->prepare($sql);
            $q->execute($params);
            $r = $q->fetchAll();

            return $r;
        }catch(PDOException $e) {
            $error = $e->getMessage();
      	}
    }


    public function isAdm($email){
        $sql = "SELECT * FROM gable_adm GADM LEFT JOIN user U ON U.email = GADM.email OR U.email_2 = GADM.email WHERE U.email = :email OR U.email_2 = :email ";;
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email));
        $r = $q->fetchAll();
        $groupCode = "";
        foreach ($r as $key => $value) {
            $groupCode .= "'".$value['group_code']."'";
            if(count($r)!=$key+1){
                $groupCode .= ",";
            }
        }
        return $groupCode;
    }

    public function ticketType($permission, $data, $email, $search){
        try{
            $queryStatus = 'AND T.status IN (1,2,3,4,7,8,9,10,11) ';
            if($data['status']===500){
                $queryStatus = 'AND T.status IN (5) ';
            }else if($data['status']===800){
                $queryStatus = 'AND (T.sla_remedy LIKE "%missed%" OR T.sla_remedy LIKE "%miss%" ) ';
            }
            $permissionQuery = 'AND (T.owner = :owner OR T.refer_remedy_report_by = :refer_remedy_report_by OR T.create_by = :create_by)';
            if($permission=="0" || $permission===0 || !$permission){

            }else{
                $permissionQuery = 'AND (U.role_sid IN ('.$permission.') OR T.owner = :owner OR T.refer_remedy_report_by = :refer_remedy_report_by OR T.create_by = :create_by)';
            }

            $statusLabel =',CASE WHEN T.status = "5" THEN "Resolve" WHEN T.status = "6" THEN "Closed" WHEN T.status = "4" THEN "Pending" ELSE "Process" END status_label ';

            $sql = 'SELECT DISTINCT(T.case_type) ';
            $sql .= 'FROM ticket T ';
            $sql .= 'LEFT JOIN user U2 ON U2.email = T.create_by ';
            $sql .= 'LEFT JOIN user U ON U.email = T.owner ';
            $sql .= 'LEFT JOIN user U3 ON U3.email = T.refer_remedy_report_by ';
            $sql .= 'WHERE 1 ';
            $sql .= $permissionQuery;
            $sql .= $queryStatus;
            $sql .= 'ORDER BY T.sid DESC ';

            $q = $this->db->prepare($sql);
            $q->execute(array(':owner'=>$email,':refer_remedy_report_by'=>$email,':create_by'=>$email));
            $r = $q->fetchAll();

            $res = array();
            foreach($r as $key => $value){
                array_push($res,$value['case_type']);
            }
            return $res;
        }catch(PDOException $e) {
            $error = $e->getMessage();
      	}
    }

    public function engineerInTask($tasks_sid, $ignore_email){

        $sql = 'SELECT T.*, (SELECT status FROM tasks_log TL WHERE TL.tasks_sid = T.tasks_sid AND TL.engineer = T.engineer ORDER BY TL.sid DESC LIMIT 0,1) status_enineer ,CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END engineer_name,U.picture_profile engineer_pic ';
        $sql .= ' FROM tasks_log T LEFT JOIN user U ON T.engineer = U.email WHERE T.tasks_sid = :tasks_sid AND T.engineer <> :ignore_email AND T.engineer <> "" GROUP BY T.engineer ';
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid,':ignore_email'=>$ignore_email));
        $r = $q->fetchAll();
        return $r;
    }
    public function taskOfTicketQuery($ticket_sid){
        try{
            $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';
            $sql = 'SELECT T.*,DATE_FORMAT(T.appointment, "%Y-%m-%d %H:%i") appointment ';
            $sql .= $create_datetime;
            $sql .= ',DATE_FORMAT(T.started_datetime, "%Y-%m-%d %H:%i") started_datetime ';
            $sql .= ',DATE_FORMAT(T.closed_datetime, "%Y-%m-%d %H:%i") closed_datetime ';
            $sql .= ',CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END engineer_name ';
            $sql .= ',CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name ';
            $sql .= ',ST.name service_type_name ';
            $sql .= ',CASE WHEN T.started_datetime = "0000-00-00 00:00:00" THEN 1 ELSE 0 END can_edit ';
            $sql .= ',(SELECT CASE WHEN TL.request_taxi IS NOT NULL THEN TL.request_taxi ELSE 0 END request_taxi FROM tasks_log TL WHERE TL.tasks_sid = T.sid AND TL.engineer = T.engineer ORDER BY TL.sid DESC LIMIT 0,1) request_taxi ';
            $sql .= ',(SELECT CASE WHEN TL.status IS NOT NULL THEN TL.status ELSE 0 END status FROM tasks_log TL WHERE TL.tasks_sid = T.sid AND TL.engineer = T.engineer ORDER BY TL.sid DESC LIMIT 0,1) status_enineer ';
            $sql .= 'FROM tasks T LEFT JOIN user U ON U.email = T.engineer ';
            $sql .= 'LEFT JOIN user U2 ON T.create_by = U2.email ';
            $sql .= 'LEFT JOIN service_type ST ON T.service_type = ST.sid ';
            $sql .= 'WHERE T.ticket_sid = :ticket_sid AND T.engineer <> ""
            AND (T.appointment_type = 0 OR T.appointment_type = 1) ORDER BY T.appointment ASC ';

            $q = $this->db->prepare($sql);
            $q->execute(array(':ticket_sid'=>$ticket_sid));
            $r = $q->fetchAll();

            foreach($r as $key=>$value){
                $r[$key]['associate_engineer'] = $this->engineerInTask($value['sid'], $value['engineer']);
            }
            return $r;
        }catch(PDOException $e){
            echo $e;
            return array();
        }
    }

    public function cManHours($ticket_sid, $new_man_hours, $email){
        try{
            $sql = "UPDATE ticket SET man_days = :man_hours WHERE sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':man_hours'=>$new_man_hours,':ticket_sid'=>$ticket_sid));

        }catch(PDOException $e){
            echo $e;
            return array();
        }
    }

    public function cEndUser($ticket_sid,$email,$eu_name,$eu_email,$eu_mobile,$eu_phone,$eu_company){
        try{
            $sql = "UPDATE ticket SET
            end_user_company_name = :end_user_company_name,
            end_user_contact_name = :end_user_contact_name,
            end_user_email = :end_user_email,
            end_user_mobile = :end_user_mobile,
            end_user_phone = :end_user_phone
            WHERE sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':end_user_company_name'=>$eu_company,
                ':end_user_contact_name'=>$eu_name,
                ':end_user_email'=>$eu_email,
                ':end_user_mobile'=>$eu_mobile,
                ':end_user_phone'=>$eu_phone,
                ':ticket_sid'=>$ticket_sid
            ));
        }catch(PDOException $e){
            echo $e;
            return array();
        }
    }

    public function sosUpdateStatus($data){
        try{
            $sql = "SELECT refer_remedy_hd FROM ticket WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$data['ticket_sid']));
            $r = $q->fetch();

            if(isset($r['refer_remedy_hd']) && $r['refer_remedy_hd']!=""){
                $IncidentID = $r['refer_remedy_hd'];

                $userName = "Devteam";
                $password = "P@ssw0rd";

                // $IncidentID = "INC000000000310";
                // $Status = "Resolved";
                $Status = $data['status'];

                $StatusReason = $data['reason'];
                $Resolution = $data['resolution'];

                // $StatusReason = "Infrastructure Change Created";
                // $Resolution = "ทดสอบอัพเดทเคส";

                $option = array('encoding'=>'utf-8');
                $client = new SoapClient('http://52.221.165.78/arsys/WSDL/public/remedy81vspace/Helpdeskset',$option);
                $headerbody = array("userName" => $userName, "password" => $password,"authentication"=>"?","locale"=>"?","timeZone"=>"?");

                $ns = "urn:Helpdeskset";

                $header = new SOAPHeader($ns, "AuthenticationInfo", $headerbody,false);
                $client->__setSoapHeaders($header);
                $result = $data = $client->Setfields(array(
                    "IncidentID"=>$IncidentID,
                    "Status"=>$Status,
                    "StatusReason"=>$StatusReason,
                    "Resolution"=>$Resolution
                ));
            }
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function cEditNote($email, $ticket_sid, $note){
        try{
            $sql = "UPDATE ticket SET note = :note, update_datetime = NOW(), updated_by = :email WHERE sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            return $q->execute(array(':note'=>$note,':email'=>$email,':ticket_sid'=>$ticket_sid));

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function cEditAddress($email, $ticket_sid, $address){
        try{
            $sql = "UPDATE ticket SET end_user_site = :end_user_site, update_datetime = NOW(), updated_by = :email WHERE sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            return $q->execute(array(':end_user_site'=>$address,':email'=>$email,':ticket_sid'=>$ticket_sid));

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function notificationDetail($notification_sid){
        try{
            $sql = "SELECT * FROM notification_send WHERE sid = :notification_sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':notification_sid'=>$notification_sid));
            return $q->fetch();
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function updateTicketStatusFollowup($tasks_sid, $follow_up_date, $follow_up_time, $follow_up){
        $sql = "UPDATE tasks SET
        is_follow_up = :follow_up,
        follow_up_date = :follow_up_date,
        follow_up_time = :follow_up_time
        WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':follow_up'=>$follow_up,
            ':follow_up_date'=>$follow_up_date,
            ':follow_up_time'=>$follow_up_time,
            ':sid'=>$tasks_sid
        ));
    }

    public function checkAuthorizeForUpdate($ticket_sid)
    {
      $sql = "SELECT ticket_sid,user_sid,owner,role_sid,user_type FROM vw_checkauthticket WHERE ticket_sid = :ticket_sid";
      $q = $this->db->prepare($sql);
      $q->execute(array(':ticket_sid'=>$ticket_sid));
      $r = $q->fetch();
  if ($q->rowCount() > 0) {
        return $r;
      }else {
        return "Null";
      }
    }
    public function insertOwnerLog($ticket_sid,$new_owner,$submitter)
    {
      $sql = "INSERT INTO ticket_owner (ticket_sid,owner,create_datetime,create_by) VALUES (:ticket_sid,:owner,NOW(),:submitter)";
      $q = $this->db->prepare($sql);
      $q->execute(array(':ticket_sid'=>$ticket_sid,':owner'=>$new_owner,':submitter'=>$submitter));
    }
    public function checkAUTHSernior($email)
    {
      $sql = "SELECT user_type FROM user WHERE email = :email";
      $q = $this->db->prepare($sql);
      $q->execute(array(':email'=>$email));
      $r = $q->fetch();
      return $r['user_type'];
    }
}
?>
