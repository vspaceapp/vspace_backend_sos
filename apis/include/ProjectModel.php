<?php
class ProjectModel{
	private $db;
 	private $day;
 	private $engineer;
    // FOR OLD
    private $employeeID = "";

    private $contract;
    private $company;
    private $project_name;
    private $vendor;
    private $project_type;
    private $owner;
    private $start;
    private $end;
    private $man_day;
    private $man_hour;

    private $project_sid;
    private $lastInsertId;
    private $data_status;
    private $prime_contract;
    private $end_user;
    private $scope;
    private $projectOwner;
    private $end_user_address,$description;
    private $subject;
    private $sequence;
    private $m;
    private $can_assign;
    public $permission_role;
    private $phase = "Post Sale";
    private $projectForCompany = "";
    private $sale = "";
    function __construct($email="") {

        $this->email = $email;
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
        if($this->email!=""){
            $this->permission_role = $this->m->permissionRoleByEmail($this->email);
        }
    }

    public function createChecklist($email, $ticket_sid, $new_checklist,$within, $unit){

        $permissionChecklist = $this->permissionChecklistLevel2($email, false, $ticket_sid);
        if($permissionChecklist['result']){
            $sql = "SELECT MAX(sequence) sequence_max FROM ticket_need_checklist WHERE ticket_sid = :ticket_sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':ticket_sid'=>$ticket_sid));
            $r = $q->fetch();

            if(!isset($r['sequence_max'])){
                $r['sequence_max'] = 1;
            }

            $sql = "INSERT INTO ticket_need_checklist (name, result, create_datetime, create_by, ticket_sid, sequence, within, unit, target_datetime)
            VALUES
            (:name, 0, NOW(), :email, :ticket_sid, :sequence, :within, :unit, :target_datetime ) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':name'=>$new_checklist,':email'=>$email,':ticket_sid'=>$ticket_sid,':sequence'=>$r['sequence_max'], ':within'=>$within,
                ':unit'=>$unit,
                ':target_datetime'=>$this->calTarget_datetime($ticket_sid, $within, $unit)
                ));

            $permissionChecklist['data'] = $this->caseNeedChecklist($ticket_sid);
            return $permissionChecklist;

        }else{
            return $permissionChecklist;
        }

    }



    public function calTarget_datetime($ticket_sid, $within, $unit){
        if($within && $unit){
            $sql = "SELECT DATE_ADD(create_datetime, INTERVAL ".$within." ".$unit.") target_datetime FROM ticket WHERE sid = :ticket_sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(':ticket_sid'=>$ticket_sid));
            $r = $q->fetch();
            return $r['target_datetime'];
        }else{
            return "0000-00-00 00:00:00";
        }
    }
    public function doChecklist($email, $checklist_sid, $value){
        $permissionChecklist = $this->permissionChecklistLevel1($email, $checklist_sid);
        if($permissionChecklist['result']){
            try{
                $sql = "UPDATE ticket_need_checklist SET result = :value,
                update_datetime = NOW(), do_datetime = NOW(), update_by = :email WHERE sid = :checklist_sid ";

                $q = $this->db->prepare($sql);
                $result = $q->execute(array(':value'=>$value,':email'=>$email, ':checklist_sid'=>$checklist_sid));

                $this->insertChecklistDoLog($checklist_sid, $value, $email);

                return array('result'=>$result,'message'=>'Successed');
            }catch(Exception $e){
                return $e->getMessage();
            }
        }else{
            return $permissionChecklist;
        }
    }

    private function insertChecklistDoLog($checklist_sid, $result, $email){
        $sql = "INSERT INTO ticket_need_checklist_do_log
        (ticket_need_checklist_sid, result, do_datetime, create_by)
        VALUES (:ticket_need_checklist_sid, :result,NOW(), :create_by) ";

        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_need_checklist_sid'=>$checklist_sid,':result'=>$result,':create_by'=>$email));
    }

    public function permissionChecklistLevel1($email, $checklist_sid){
        $dataCheckList = $this->selectCheckListBySid($checklist_sid);
        $dataTicket = $this->selectTicketBySid($dataCheckList['ticket_sid']);

        if($dataTicket['owner'] == $email || $dataTicket['create_by']== $email){
            return array('result'=>true,'message'=>'Successed');
        }else{
            return array('result'=>false,'message'=>'Permission Denied');
        }
    }

    public function permissionChecklistLevel2($email, $checklist_sid=null, $ticket_sid=null){
        if($ticket_sid){
            $dataTicket = $this->selectTicketBySid($ticket_sid);
            if($dataTicket['create_by'] == $email){
                return array('result'=>true,'message'=>'Successed');
            }else{
                return array('result'=>false,'message'=>'Permission Denied');
            }
        }else if($checklist_sid){
            $dataCheckList = $this->selectCheckListBySid($checklist_sid);
            $dataTicket = $this->selectTicketBySid($dataCheckList['ticket_sid']);
            if($dataTicket['create_by'] == $email){
                return array('result'=>true,'message'=>'Successed');
            }else{
                return array('result'=>false,'message'=>'Permission Denied');
            }
        }else{
            return array('result'=>false,'message'=>'Permission Denied');
        }
    }


    public function selectCheckListBySid($sid){
        try{
            $sql = "SELECT * FROM ticket_need_checklist WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$sid));
            return $q->fetch();
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function selectTicketBySid($sid){
        try{
            $sql = "SELECT * FROM ticket WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$sid));
            return $q->fetch();
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function updateChecklist($email, $ticket_sid, $new_checklist, $checklist_sid, $within, $unit){
        $permissionChecklist = $this->permissionChecklistLevel2($email, $checklist_sid);
        if($permissionChecklist['result']){
            $sql = "UPDATE ticket_need_checklist SET name = :new_checklist, update_by = :email, update_datetime = NOW(), within = :within, unit = :unit, target_datetime = :target_datetime WHERE sid = :sid ";
             $q = $this->db->prepare($sql);
            $q->execute(array(':new_checklist'=>$new_checklist,':sid'=>$checklist_sid,':email'=>$email, ':within'=>$within,
            ':unit'=>$unit, ':target_datetime'=>$this->calTarget_datetime($ticket_sid, $within, $unit)));
            $permissionChecklist['data'] = $this->caseNeedChecklist($ticket_sid);
            return $permissionChecklist;
        }else{
            return $permissionChecklist;
        }
    }

    public function deleteChecklist($email, $ticket_sid, $checklist_sid){

        $permissionChecklist = $this->permissionChecklistLevel2($email, $checklist_sid);
        if($permissionChecklist['result']){
            $sql = "UPDATE ticket_need_checklist SET update_by = :email, update_datetime = NOW(), data_status = '-1' WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':sid'=>$checklist_sid,':email'=>$email));
            $permissionChecklist['data'] = $this->caseNeedChecklist($ticket_sid);
            return $permissionChecklist;
        }else{
            return $permissionChecklist;
        }
    }

    public function createProject1($data, $email){

        $this->email = $email;
        $this->projectOwner = $data['engineer'];
        $this->contract = isset($data['contract_no'])?$data['contract_no']:"";
        $this->project_type = isset($data['type'])?$data['type']:"Implement";
        $this->phase = isset($data['project_status'])?$data['project_status']:"";
        $this->project_name = $data['contract_info']['project_name'];
        $this->end_user = $data['contract_info']['end_user_name'];
        $this->end_user_address = $data['contract_info']['end_user_address'];
        $this->description = isset($data['contract_info']['description'])?$data['contract_info']['description']:"";
        $this->customer = isset($data['contract_info']['customer'])?$data['contract_info']['customer']:"";
        $this->subject = isset($data['subject'])?$data['subject']:"";
        $this->start = ($data['start']);
        $this->end = ($data['end']);

        if(isset($data['projectForCompany'])){
            $this->projectForCompany = $data['projectForCompany'];
        }
        $this->man_day = $data['man_day'];
        $this->man_hour = $data['man_hour'];
        $this->sale = $data['sale'];

        // require_once 'SendMail.php';

        $this->insertToProject();
        $this->insertToProjectOwner1();
    }

    public function createProject($data, $email){

        $this->email = $email;
        $this->projectOwner = $data['engineer'];
        $this->contract = isset($data['contract_no'])?$data['contract_no']:"";
        $this->project_type = $data['type'];
        $this->project_status = isset($data['project_status'])?$data['project_status']:"";

        $this->project_name = $data['contract_info']['project_name'];
        $this->end_user = $data['contract_info']['end_user_name'];
        $this->end_user_address = $data['contract_info']['end_user_address'];
        $this->description = $data['contract_info']['description'];

        $this->subject = $data['subject'];
        $this->start = $this->convertDateToDb($data['start_project']);
        $this->end = $this->convertDateToDb($data['end_project']);

        $this->man_day = $data['man_day'];
        $this->man_hour = $data['man_hour'];

        // require_once 'SendMail.php';

        $this->insertToProject();
        $this->insertToProjectOwner();
    }

    public function planCreate($data, $email){

        // if($data['plan_name_other']!=""){
        //     $data['plan_name'] = $data['plan_name_other'];
        // }
        $this->project_sid = $data['project_sid'];
        $this->sequence = $this->getMaxSequence();

        $data_status = 1;

        if(isset($data['sid']) && $data['sid']!=""){
            $sql = "UPDATE project_plan SET name = :name, other = :other, detail = :detail, man_days = :man_days, man_hours = :man_hours, sequence = :sequence, updated_by = :updated_by, updated_datetime = NOW()  WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':name'=>$data['plan_name'],
                ':other'=>$data['plan_name_other'],
                ':detail'=>$data['plan_name_detail'],
                ':man_days'=>$data['dataMan']['days'],
                ':man_hours'=>$data['dataMan']['hours'],
                ':sequence'=>$data['sequence'],
                ':updated_by'=>$email,
                ':sid'=>$data['sid']
                ));
        }else{
            $sql = "INSERT INTO project_plan (project_sid, contract_no, name, other, detail, create_by, create_datetime, man_days, man_hours, sequence, data_status)
            VALUES (:project_sid, :contract_no, :name, :other, :detail, :create_by, NOW(), :man_days, :man_hours, :sequence, :data_status) ";

            $q = $this->db->prepare($sql);
            $q->execute(array(':project_sid'=>$data['project_sid'],':contract_no'=>$data['contract_no'],':name'=>$data['plan_name'],':other'=>$data['plan_name_other'],
                ':detail'=>$data['plan_name_detail'],':create_by'=>$email,':man_days'=>$data['dataMan']['days'],':man_hours'=>$data['dataMan']['hours'],':sequence'=>$this->sequence,':data_status'=>$data_status));
        }
        return $data;
    }

    public function planProject($project_sid, $contract_no){
        $sql = "SELECT * FROM project_plan WHERE project_sid = :project_sid AND data_status > 0 ORDER BY sequence ASC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function getMaxSequence(){
        $sql = "SELECT MAX(sequence) max_sequence FROM project_plan WHERE project_sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$this->project_sid));
        $r = $q->fetch();
        return $r['max_sequence']+1;
    }

    private function convertDateToDb($date){
        if($date!=""){
            $date = explode("/", $date);
            return $date[2]."-".$date[1]."-".$date[0];
        }else{
            return "0000-00-00";
        }
    }

    private function insertToProject(){
        if($this->project_type!="Learning"){
            $name = $this->project_name;
        }else{
            $name = $this->subject;
        }

        $sql = "INSERT INTO project (name,customer,end_user,project_type,contract,vendor,
					create_datetime,create_by,data_status,end_user_address,phase,project_for_company,
					sales,project_start,project_end, mgr_owner, owner)
        VALUES (:name,:customer,:end_user,:project_type,:contract,:vendor,
					NOW(),:create_by,:data_status,:end_user_address,:phase,:project_for_company,
					:sales,:project_start,:project_end,
            :mgr_owner, :owner) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':name'=>$name,
                ':customer'=>$this->customer,
                ':end_user'=>$this->end_user,
                ':project_type'=>$this->project_type,
                ':contract'=>$this->contract,
                ':vendor'=>"",
                ':create_by'=>$this->email,
                ':data_status'=>"1",
                ':end_user_address'=>$this->end_user_address,
                ':phase'=>$this->phase,
                ':project_for_company'=>$this->projectForCompany,
                ':sales'=>$this->sale,
                ':project_start'=>$this->start,
                ':project_end'=>$this->end,
                ':mgr_owner'=>(isset($this->projectOwner[0]['email'])?$this->projectOwner[0]['email']:$this->email),
                ':owner'=>(isset($this->projectOwner[0]['email'])?$this->projectOwner[0]['email']:$this->email)
            ));
        $this->lastInsertId = $this->db->lastInsertId();

        if($this->project_type=="Learning"){
            $this->updateContractProjectIntra();
        }
    }

    private function updateContractProjectIntra(){
        $project_sid = $this->lastInsertId;
        $contract = "LEARNING-PROJECT-".$this->lastInsertId;

        $sql = "UPDATE project SET contract = :contract WHERE sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract'=>$contract, ':project_sid'=>$project_sid));

    }

    private function insertToProjectOwner(){
        $project_sid = $this->lastInsertId;
        foreach ($this->projectOwner as $key => $value) {
           $owner = $value;
           $sql = "INSERT INTO project_owner (project_sid,owner,project_start,project_end,man_days,man_hours,status,create_datetime,create_by) VALUES (:project_sid,:owner,:project_start,:project_end,:man_days,:man_hours,:status,NOW(),:create_by) ";
           $q = $this->db->prepare($sql);
           $q->execute(array(
                ':project_sid'=>$project_sid,
                ':owner'=>$owner['email'],
                ':project_start'=>$this->start,
                ':project_end'=>$this->end,
                ':man_days'=>$this->man_day,
                ':man_hours'=>$this->man_hour,
                ':status'=>'1',
                ':create_by'=>$this->email
            ));
        }
        // $objsendmail = new SendMail();
        // $objsendmail->sendToEngineerProjectAssigned($project_sid);
    }

    private function insertToProjectOwner1(){
        $project_sid = $this->lastInsertId;
        // echo "<pre>";
        // print_r($this->projectOwner);
        // echo "</pre>";

        foreach ($this->projectOwner as $key => $value) {
           $owner = $value;
           if(isset($owner['email'])){
               $sql = "INSERT INTO project_owner (project_sid,owner,project_start,project_end,man_days,man_hours,status,create_datetime,create_by) VALUES (:project_sid,:owner,:project_start,:project_end,:man_days,:man_hours,:status,NOW(),:create_by) ";
               $q = $this->db->prepare($sql);
               $q->execute(array(
                    ':project_sid'=>$project_sid,
                    ':owner'=>$owner['email'],
                    ':project_start'=>$this->start,
                    ':project_end'=>$this->end,
                    ':man_days'=>$owner['man_day'],
                    ':man_hours'=>"",
                    ':status'=>'1',
                    ':create_by'=>$this->email
                ));
           }
        }
				require_once 'SendMailProject.php';

				$projectDetail = $this->projectDetail2($project_sid);

				$message = 'New Project <br/><br/>';
				$message .= 'Project name: '.$projectDetail['name'].'<br/>';
				$message .= 'Contract: '.$projectDetail['contract'].'<br/>';
				$message .= 'End user: '.$projectDetail['end_user'].'<br/>';
				$message .= 'Customer: '.$projectDetail['customer'].'<br/>';
				$message .= 'Mgr owner: '.$projectDetail['mgr_owner'].'<br/><br/>';
				$message .= 'ขั้นตอนต่อไปกำหนด Man hours และ Owner ของ Project<br/>';
				$subject = 'New Project '.$projectDetail['name'].' รอกำหนด Man hours และ Owner ของ Project<br/>';

        $objSendMail = new SendMailProject();
        $objSendMail->noticeMailNewProject($projectDetail['mgr_owner'], $message, $subject, $projectDetail['create_by']);

    }

    public function executeProject( $email,$contract,$company,$project_name,$vendor="",$project_type,$project_sid="", $data_status="",$prime_contract="", $end_user="",$projectOwner){

        $this->projectOwner = $projectOwner;
        $this->email = $email;
        $this->contract = trim($contract);
        $this->company = trim($company);
        $this->project_name = trim($project_name);
        $this->vendor = $vendor;
        $this->project_type = $project_type;

        // $this->owner = $owner;
        // $this->start = $start;
        // $this->end = $end;
        // $this->man_day = $man_day;
        // $this->scope = $scope;

        $this->project_sid = $project_sid;
        $this->data_status = $data_status;

        $this->prime_contract = $prime_contract;
        $this->end_user = $end_user;

        return $this->runExecuteProject();
    }

    private function runExecuteProject(){
        if($this->project_sid==""){
            $sql = "INSERT INTO project
            (name, customer, project_type, contract, vendor, create_by, create_datetime, prime_contract, end_user)
            VALUES
            (:name, :customer, :project_type, :contract, :vendor, :create_by, NOW(), :prime_contract, :end_user)";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':name'=> $this->project_name,
                ':customer'=>$this->company,
                // ':man_days'=>$this->man_day,
                // ':project_start'=>$this->start,
                // ':project_end'=>$this->end,
                ':project_type'=>$this->project_type,
                ':contract'=>$this->contract,
                ':vendor'=>$this->vendor,
                ':create_by'=>$this->email,
                ':prime_contract'=>$this->prime_contract,
                ':end_user'=>$this->end_user
                ));
            $this->lastInsertId = $this->db->lastInsertId();
            $this->projectOwner();
            return "AddNewProject";
        }else{
            $sql = "UPDATE project SET
                    name = :name,
                    customer = :customer,
                    project_type = :project_type,
                    contract = :contract,
                    vendor = :vendor,
                    updated_datetime = NOW(),
                    updated_by = :updated_by,
                    data_status = :data_status,
                    prime_contract = :prime_contract,
                    end_user = :end_user
                    WHERE sid = :sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':name'=> $this->project_name,
                ':customer' => $this->company,
                ':project_type'=>$this->project_type,
                ':contract'=>$this->contract,
                ':vendor'=>$this->vendor,
                ':updated_by'=>$this->email,
                ':data_status'=>$this->data_status,
                ':prime_contract'=>$this->prime_contract,
                ':end_user'=>$this->end_user,
                ':sid'=>$this->project_sid
                ));
            $this->lastInsertId = $this->project_sid;
            $this->projectOwner();
            return "UpdateProject";
        }
    }

    public function projectData($project_sid){
        $this->setProjectSid($project_sid);
        return $this->projectDataByProjectSid();
    }

    private function projectDataByProjectSid(){
        $sql = "SELECT P.*, P.name project, P.customer company
                FROM project P WHERE P.sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$this->getProjectSid()));
        $r = $q->fetch();
        $r['list_owner'] = $this->listProjectOwner();
        return $r;
    }

    private function projectOwner(){
        $this->changeStatusOwner("-1");
        if(is_array($this->projectOwner)){
            foreach ($this->projectOwner as $key => $value) {
                $this->setOwner($value['owner']);
                $informationProjectOwner = $this->informationProjectOwner();
                $this->scope = $value['scope'];
                $this->man_day = $value['man_hours'];
                $this->start = $value['start_project'];
                $this->end = $value['end_project'];
                if($this->owner!=""){
                    if($informationProjectOwner['sid']>0){
                        $sql = "UPDATE project_owner SET man_days = :man_days, scope = :scope, project_start = :project_start, project_end = :project_end, status = :status
                                WHERE sid = :sid ";
                        $q = $this->db->prepare($sql);
                        $q->execute(array(
                            ':man_days' => $this->man_day,
                            ':scope' => $this->scope,
                            ':project_start' => $this->start,
                            ':project_end' => $this->end,
                            ':status' => '1',
                            ':sid' => $informationProjectOwner['sid']
                            ));
                    }else{
                        $sql = "INSERT INTO project_owner (project_sid, owner, create_datetime, create_by, man_days,project_start,project_end,status)
                                VALUES (:project_sid, :owner, NOW(), :create_by, :man_days, :project_start, :project_end,:status) ";
                        $q = $this->db->prepare($sql);
                        $q->execute(array(':project_sid'=>$this->lastInsertId,':owner'=>$this->owner,':create_by'=>$this->email,':man_days'=>$this->man_day,':project_start'=>$this->start,':project_end'=>$this->end,':status'=>'1'));
                    }
                }
            }
        }
    }
    private function changeStatusOwner($status){
        $sql = "UPDATE project_owner SET status = :status WHERE project_sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_sid'=>$this->project_sid,
                ':status'=>$status
            ));
    }

    private function deleteOwnerFromProject(){
        $sql = "UPDATE project_owner SET status = '-1' WHERE project_sid = :project_sid AND owner = :owner ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':project_sid'=>$this->project_sid,
            ':owner'=>$this->owner
            ));
    }

    private function informationProjectOwner(){
        $sql = "SELECT * FROM project_owner WHERE project_sid = :project_sid AND owner = :owner ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':project_sid'=>$this->project_sid,
            ':owner'=>$this->getOwner()
        ));
        return $q->fetch();
    }
    private function listOwnerThisProject(){
        $sql = "SELECT * FROM project_owner WHERE project_sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':project_sid'=>$this->project_sid
        ));
        return $q->fetchAll();
    }
    public function executeUpdateManDays($owner="", $type="", $search="", $email="", $limit=""){
        $sql = "SELECT P.sid project_sid,PO.*,P.detail,P.end_user_address,P.sid project_sid, P.contract contract, P.prime_contract,P.project_type,P.customer,P.name name,P.vendor vendor,P.end_user, P.name project, P.customer company, CONCAT(E.thainame,' (', PO.owner,')' ) engineer,PO.owner owner, PO.scope, PO.sid project_owner_sid,P.use_minutes useMinutes, PO.sid project_owner_sid
                FROM project P
                LEFT JOIN project_owner PO ON PO.project_sid = P.sid AND PO.status > 0
                LEFT JOIN employee E ON PO.owner = E.emailaddr AND E.emailaddr != ''
                WHERE 1 AND data_status = '1' ";
        if($owner!=""){
            $sql .= "AND PO.owner = '".$owner."' ";
        }
        if($type!="" && $type!="all"){
            $sql .= "AND P.project_type = '".$type."' ";
        }

        // if($email!="autsakorn.t@firstlogic.co.th"){
        //     $sql .= " AND PO.owner <> 'autsakorn.t@firstlogic.co.th' ";
        // }
        if($search!=""){
            $sql .= " AND (P.end_user LIKE '%".$search."%' OR P.end_user_address LIKE '%".$search."%' OR P.contract LIKE '%".$search."%' OR P.prime_contract LIKE '%".$search."%' OR P.customer LIKE '%".$search."%' OR P.name LIKE '%".$search."%' OR P.project_type LIKE '%".$search."%' ) ";
        }
        $sql .= "  GROUP BY P.sid ORDER BY P.sid DESC ";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        // echo "<pre>";
        // print_r($r);
        // echo "</pre>";
        foreach ($r as $key => $value) {
            $this->setProjectSid($value['project_sid']);
            $r[$key]['list_owner'] = $this->listProjectOwner();
            $projectDetail = $this->projectDetail($this->project_sid);
            $r[$key]['man_hours_total'] = $projectDetail['man_hours_total'];
            $r[$key]['projectDetail'] = $projectDetail;
            $listCaseOfContract = $this->listCaseOfContract($value['contract'], $value['project_sid']);
            $useMinutes = 0;
            foreach ($listCaseOfContract as $k => $v) {
                foreach ($v['task'] as $kk => $vv) {
                    $cal_man_hours = $vv['cal_man_hours'];
                    // Change requirement
                    $cal_man_hours = 100;
                    foreach ($vv['timestamp'] as $kkk => $vvv) {
                        $useMinutes += ($vvv['use_minutes']*$cal_man_hours)/100;
                    }
                }
            }
            $r[$key]['useMinutes'] = $useMinutes;
            $this->updateMinute($value['project_sid'], $useMinutes);
        }
        return $r;
    }
    public function lists($owner="", $type="", $search="", $email="", $limit="", $phase=""){
        $sql = "SELECT P.sid project_sid,PO.*,P.detail,P.end_user_address,P.sid project_sid, P.contract contract, P.prime_contract,P.project_type,P.customer,P.name name,P.vendor vendor,P.end_user, P.name project, P.customer company, CONCAT(E.thainame,' (', PO.owner,')' ) engineer,PO.owner owner, PO.scope, PO.sid project_owner_sid,P.use_minutes useMinutes
                FROM project P
                LEFT JOIN project_owner PO ON PO.project_sid = P.sid AND PO.status > 0
                LEFT JOIN employee E ON PO.owner = E.emailaddr AND E.emailaddr != ''
                WHERE 1 AND P.data_status = '1' ";
        if($owner!=""){
            $sql .= "AND PO.owner = '".$owner."' OR P.create_by = '".$owner."' ";
        }
        if($type!="" && $type!="all"){
            // $sql .= "AND P.project_type = '".$type."' ";
        }

        if($email!="autsakorn.t@firstlogic.co.th"){
            $sql .= " AND PO.owner <> 'autsakorn.t@firstlogic.co.th' ";
        }
        if($search!=""){
            $sql .= " AND (P.end_user LIKE '%".$search."%' OR P.end_user_address LIKE '%".$search."%' OR P.contract LIKE '%".$search."%' OR P.prime_contract LIKE '%".$search."%' OR P.customer LIKE '%".$search."%' OR P.name LIKE '%".$search."%' OR P.project_type LIKE '%".$search."%' ) ";
        }
        if($phase!=""){
            // $sql .= " AND P.phase = '".$phase."' ";
        }
        $sql .= "  GROUP BY P.contract ORDER BY P.sid DESC ";
        if($limit!=""){
            $sql .= " LIMIT ".$limit.", 1 ";
        }
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {
            $this->setProjectSid($value['project_sid']);
            $r[$key]['list_owner'] = $this->listProjectOwner();
            $projectDetail = $this->projectDetail($this->project_sid);
            $r[$key]['man_hours_total'] = $projectDetail['man_hours_total'];
            $r[$key]['projectDetail'] = $projectDetail;
            // if($email=="autsakorn.t@firstlogic.co.th" && 1==1){
            //     $listCaseOfContract = $this->listCaseOfContract($value['contract']);
            //     $useMinutes = 0;
            //     foreach ($listCaseOfContract as $k => $v) {
            //         foreach ($v['task'] as $kk => $vv) {
            //             $cal_man_hours = $vv['cal_man_hours'];
            //             foreach ($vv['timestamp'] as $kkk => $vvv) {
            //                 $useMinutes += ($vvv['use_minutes']*$cal_man_hours)/100;
            //             }
            //         }
            //     }
            //     $r[$key]['useMinutes'] = $useMinutes;
            //     $this->updateMinute($value['project_sid'], $useMinutes);
            // }
        }
        return $r;
    }

    public function updateMinute($sid, $useMinutes){
        $sql = "UPDATE project SET use_minutes = :use_minutes, updated_use_minutes = NOW() WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':use_minutes'=>$useMinutes,':sid'=>$sid));
    }

    public function listCaseOfProject($project_sid, $type){
        $sql = "SELECT T.*,CASE WHEN T.no_project_ticket <> '' THEN T.no_project_ticket ELSE T.no_ticket END no_ticket,E.thainame owner_thainame,E.empno owner_empno,E.maxportraitfile owner_path,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile,
                    CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,
                     U.picture_profile pic_full,
                     CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END owner_thainame,
                     U_C.picture_profile created_pic_full, U_C.name created_name, U_C.email created_email,
                (SELECT tasks.appointment FROM tasks WHERE 1 AND tasks.ticket_sid = T.sid ORDER BY tasks.sid DESC LIMIT 0,1) last_appointment
                FROM ticket T
                    LEFT JOIN user U ON U.email = T.owner AND U.email <> ''
                    LEFT JOIN employee E ON E.emailaddr = T.owner AND E.emailaddr <> ''
                    LEFT JOIN user U_C ON U_C.email = T.create_by
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                WHERE 1 AND T.owner IS NOT NULL AND T.project_sid = :project_sid AND T.case_type = :type ORDER BY T.sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid,':type'=>$type));
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {
            $r[$key]['task'] = $this->listTaskOfCase($value['sid']);
            $r[$key]['need_checklist'] = $this->caseNeedChecklist($value['sid']);

            if($r[$key]['refer_remedy_hd']){
                $r[$key]['sla_remedy_array'] = $this->getTargetSLA($value['sid'], $value['refer_remedy_hd']);
            }else{
                $r[$key]['sla_remedy_array'] = array();
            }
            $r[$key]['contactPeople'] = $this->contactPeople($value['contract_no'], $value['end_user']);
        }
        return $r;
    }

    public function ticketItem($email, $status, $in_role_sid = 0, $search=""){
        // E.thainame owner_thainame,E.empno owner_empno,E.maxportraitfile owner_path,
        $sql = "SELECT T.*,CASE WHEN T.no_project_ticket <> '' THEN T.no_project_ticket ELSE T.no_ticket END no_ticket,U.role_sid,
                    CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END owner_thainame,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile,
                    U.picture_profile pic_full,U.picture_profile owner_path,
                    U_C.picture_profile created_pic_full, U_C.name created_name, U_C.email created_email,
                (SELECT tasks.appointment FROM tasks WHERE 1 AND tasks.ticket_sid = T.sid ORDER BY tasks.sid DESC LIMIT 0,1) last_appointment
                FROM ticket T
                    LEFT JOIN user U ON U.email = T.owner AND U.email <> ''
                    LEFT JOIN user U_C ON U_C.email = T.create_by
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                    LEFT JOIN case_status CS ON T.status = CS.sid
                WHERE 1
                AND T.owner IS NOT NULL
                AND (T.owner = :email OR T.create_by = :email OR U.role_sid IN (".$in_role_sid."))
                AND T.status IN (".$status.") ";
        if($search!=""){
            $sql .= " AND (T.subject LIKE :search
                OR T.refer_remedy_hd LIKE :search
                OR T.no_ticket LIKE :search
                OR U.email LIKE :search
                OR U.formal_name LIKE :search
                OR U.name LIKE :search
                OR T.end_user LIKE :search
                OR T.end_user_site LIKE :search
                OR T.case_type LIKE :search
                OR T.contract_no LIKE :search
                OR CS.description LIKE :search
                ) ";
            $sql .= "ORDER BY T.create_datetime DESC ";
            $q = $this->db->prepare($sql);
            $q->execute(
                array(
                    ':email'=>$email,
                    ':search'=>"%".$search."%"
                    ));
        }else{
            $sql .= "ORDER BY T.create_datetime DESC ";
            $q = $this->db->prepare($sql);
            $q->execute(array(':email'=>$email));
        }


        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $r[$key]['task'] = array();//$this->listTaskOfCase($value['sid']);
            $r[$key]['need_checklist'] = $this->caseNeedChecklist($value['sid']);

            if($r[$key]['refer_remedy_hd']){
                $r[$key]['sla_remedy_array'] = $this->getTargetSLA($value['sid'], $value['refer_remedy_hd']);
                $r[$key]['worklog'] = $this->getTicketStatusWorklogRemedy($value['sid']);
            }else{
                $r[$key]['sla_remedy_array'] = array();
                $r[$key]['worklog'] = $this->getTicketStatusWorklog($value['sid']);
            }
            $r[$key]['contactPeople'] = $this->contactPeople($value['contract_no'], $value['end_user']);
        }

        return $r;
    }

    public function selectContactPeople($contract, $end_user, $dataRequesterEnduser){

        $queryNotRequester = "";
        if(isset($dataRequesterEnduser[0]['email'])){
            $queryNotRequester = " AND email != '".$dataRequesterEnduser[0]['email']."' ";
        }
        $queryNotEndUser = "";
        if(isset($dataRequesterEnduser[1]['email'])){
            $queryNotEndUser = " AND email != '".$dataRequesterEnduser[1]['email']."' ";
        }

        $sql = "SELECT contact_name name, email, mobile, phone, company, contact_no contract_no FROM contact
        WHERE (contact_no = :contract OR end_user LIKE :end_user) ".$queryNotRequester." ".$queryNotEndUser." GROUP BY email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract'=>$contract, ':end_user'=>"%".$end_user."%"));
        return $r = $q->fetchAll();
    }

    public function contactPeople($contract, $end_user, $ticket_sid=0){

        $data = array();
        if($ticket_sid>0){
            $data = $this->enduserAndRequesterTicket($ticket_sid);
        }

        $r = $this->selectContactPeople($contract, $end_user, $data);
        // print_r($data);
        if(count($r)>5 && $ticket_sid>0){
            $r = $this->selectContactFromOldSr($ticket_sid, $data);
            // print_r($r);
        }
        $return = array_merge($data, $r);
        // print_r($return);
        return $return;

    }

    public function selectContactFromOldSr($ticket_sid, $dataRequesterEnduser){

        $queryNotRequester = "";
        if(isset($dataRequesterEnduser[0]['email'])){
            $queryNotRequester = " AND end_user_email_service_report != '".$dataRequesterEnduser[0]['email']."' ";
        }
        $queryNotEndUser = "";
        if(isset($dataRequesterEnduser[1]['email'])){
            $queryNotEndUser = " AND end_user_email_service_report != '".$dataRequesterEnduser[1]['email']."' ";
        }
        $sql = "SELECT
        end_user_email_service_report email,
        end_user_contact_name_service_report name,
        end_user_phone_service_report phone,
        end_user_mobile_service_report mobile,
        end_user_company_name_service_report company FROM tasks
        WHERE ticket_sid = :sid ".$queryNotRequester." ".$queryNotEndUser."
        GROUP BY end_user_email_service_report
        ORDER BY sid DESC";

        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$ticket_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function enduserAndRequesterTicket($ticket_sid){
        $data = array();

        $r = $this->selectRequesterTicket($ticket_sid);
        if(count($r)>0){
            array_push($data, $r[0]);
        }

        $r = $this->selectEndUserTicket($ticket_sid, $r);
        if(count($r)>0){
            array_push($data, $r[0]);
        }
        return $data;
    }

    private function selectEndUserTicket($ticket_sid, $requester){

        if(isset($requester[0]['email'])){
            $sql = "SELECT end_user_contact_name name, end_user_phone phone, end_user_mobile mobile, end_user_email email, end_user_company_name company FROM ticket
            WHERE sid = :ticket_sid AND end_user_email <> ''
            AND end_user_email != '".$requester[0]['email']."' ";
        }else{
            $sql = "SELECT end_user_contact_name name, end_user_phone phone, end_user_mobile mobile, end_user_email email, end_user_company_name company FROM ticket WHERE sid = :ticket_sid AND end_user_email <> '' ";
        }

        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetchAll();
        return $r;
    }
    private function selectRequesterTicket($ticket_sid){
        $sql = "SELECT requester_full_name name, requester_company_name company, requester_phone phone, requester_mobile mobile, requester_email email FROM ticket WHERE sid = :ticket_sid AND requester_email <> '' ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetchAll();
        return $r;
    }

    public function genPriority($urgency, $impact){
        $urgency = strtolower($urgency);
        $impact = strtolower($impact);
        $priority = "low";

        if($urgency=="high" && $impact=="high"){
            $priority = "critical";
        }else if($urgency=="high" && $impact=="normal"){
            $priority = "high";
        }else if($urgency=="high" && $impact=="low"){
            $priority = "normal";
        }else if($urgency=="normal" && $impact=="high"){
            $priority = "high";
        }else if($urgency=="normal" && $impact=="normal"){
            $priority = "normal";
        }else if($urgency=="normal" && $impact=="low"){
            $priority = "low";
        }else if($urgency=="low" && $impact=="high"){
            $priority = "normal";
        }else if($urgency=="low" && $impact=="normal"){
            $priority = "low";
        }else if($urgency=="low" && $impact=="low"){
            $priority = "low"; // planning
        }
        return $priority;

    }

    public function ticketDetail($ticket_sid, $email){
        $sql = "SELECT T.*, T.no_ticket,E.thainame owner_thainame,E.empno owner_empno,E.maxportraitfile owner_path,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile,
                    CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,
                     U.picture_profile pic_full,
                    U_C.picture_profile created_pic_full,U_C.name created_name, U_C.email created_email, T.refer_remedy_report_by,
                (SELECT tasks.appointment FROM tasks WHERE 1 AND tasks.ticket_sid = T.sid ORDER BY tasks.sid DESC LIMIT 0,1) last_appointment
                FROM ticket T
                    LEFT JOIN user U ON U.email = T.owner AND U.email <> ''
                    LEFT JOIN employee E ON E.emailaddr = T.owner AND E.emailaddr <> ''
                    LEFT JOIN user U_C ON U_C.email = T.create_by
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                WHERE 1 AND T.owner IS NOT NULL AND T.sid = :ticket_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetch();

        if(isset($r['sid'])){


            $r['task'] = $this->listTaskOfCase($r['sid']);
            $r['need_checklist'] = $this->caseNeedChecklist($r['sid']);
            $r['priority'] = $r['urgency'];

            if(strlen($r['refer_remedy_hd'])=="8"){
                $r['sla_remedy_array'] = $this->getTargetSLA(trim($r['sid']), $r['refer_remedy_hd']);
                $r['worklog'] = $this->getTicketStatusWorklogRemedy($r['sid']);
            } else if(strlen($r['refer_remedy_hd'])=="15"){

                $r['priority'] = ucfirst($this->genPriority($r['urgency'], $r['impact']));

                $r['sla_remedy_array'] = $this->getTargetSLA($r['sid'], $r['refer_remedy_hd']);
                $r['worklog'] = $this->getTicketStatusWorklog($r['sid']);
            }else{
                $r['sla_remedy_array'] = $this->getTargetSLA($r['sid'], $r['refer_remedy_hd']);
                $r['worklog'] = $this->getTicketStatusWorklog($r['sid']);
            }

            if($r['status']=="4"){
                $r['expect_pending'] = $this->expectPending($ticket_sid);
            }

            // if($r['case_type']=="Incident"){
                $r['alert'] = $this->ticketAlert($ticket_sid);
            // }else{
            //     $r['alert'] = array();
            // }
            $r['contactPeople'] = $this->contactPeople($r['contract_no'], $r['end_user']);
        }
        return $r;
    }
    private function ticketAlert($ticket_sid){
        $sql = "SELECT *,UCASE(REPLACE(role,'_',' ')) role,
        UCASE(type) type,
        CASE type_alert WHEN 'before' THEN CONCAT('ก่อนตก SLA ', value,' ',unit) WHEN 'after' OR 'more' THEN CONCAT('หลังตก SLA ',value,' ',unit) ELSE '' END _when
         FROM ticket_alert WHERE ticket_sid = :ticket_sid ORDER BY sid ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetchAll();
        return $r;
    }
    private function expectPending($ticket_sid){
        $sql= "SELECT date_format(expect_pending,'%Y-%m-%d-%H-%i-%s') expect_pending FROM ticket_status WHERE ticket_sid = :ticket_sid AND expect_pending != '0000-00-00 00:00:00' ORDER BY sid DESC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetch();
        $expect_pending = explode("-",$r['expect_pending']);
        if(isset($expect_pending[0]) && isset($expect_pending[1]) && isset($expect_pending[2]) && isset($expect_pending[3]) && isset($expect_pending[4]) && isset($expect_pending[5])){
            if($expect_pending[0]>2500){
                $year = ($expect_pending[0]-543);
            }else{
                $year = $expect_pending[0];
            }
            $data = $year."/".$expect_pending[1]."/".$expect_pending[2]." ".$expect_pending['3'].":".$expect_pending['4'].":".$expect_pending['5'];
            return $data;
        }else{
            return $r['expect_pending'];
        }
    }
    private function getTicketStatusWorklogRemedy($ticket_sid){
        $sql = "SELECT T.*,U.formal_name created_name,
        U.picture_profile created_pic,
        U.mobile create_by_mobile,date_format(T.create_datetime,'%Y-%d-%d %H:%i') create_datetime_df
        FROM ticket_status T
            LEFT JOIN user U ON T.create_by = U.email AND U.email <> ''
        WHERE T.ticket_sid = :ticket_sid AND T.data_from = 'remedy'
         ORDER BY sid ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }
    private function getTicketStatusWorklog($ticket_sid){
        $sql = "SELECT T.*,U.formal_name created_name,
        U.picture_profile created_pic,
        U.mobile create_by_mobile,date_format(T.create_datetime,'%Y-%d-%d %H:%i') create_datetime_df,
        CONCAT((CASE T.case_status_sid WHEN '2' THEN '[In Progress]'
					WHEN '3' THEN '[Workaround]' WHEN '4' THEN '[Pending]'
					WHEN '7' THEN '[Onsite]' WHEN '5' THEN '[Resolved]'
					WHEN '8' THEN '[Work detail]' WHEN '10' THEN '[No Workaround]' WHEN '11' THEN '[No Onsite]'
					ELSE '' END),' ',T.worklog) worklog, solution, solution_detail
        FROM ticket_status T
            LEFT JOIN user U ON T.create_by = U.email AND U.email <> ''
        WHERE T.ticket_sid = :ticket_sid
         ORDER BY sid ASC  ";
         // AND T.data_from = 'remedy'
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    private function getTargetSLA($ticket_sid, $refer_remedy_hd){
//        if(strlen($refer_remedy_hd)=='15'){
//            $sql = "SELECT * FROM ticket_sla WHERE ticket_sid = :ticket_sid AND status NOT LIKE '%DELETED%'
//						AND case_status_sid > 0 ORDER BY sid ASC ";
//            $q = $this->db->prepare($sql);
//            $q->execute(array(':ticket_sid'=>$ticket_sid));
//        }else{
//            $sql = "SELECT * FROM ticket_sla WHERE ticket_sid = :ticket_sid ORDER BY due_datetime ASC ";
//            $q = $this->db->prepare($sql);
//            $q->execute(array(':ticket_sid'=>$ticket_sid));
//        }

        $sql = "SELECT * FROM ticket_sla WHERE ticket_sid = :ticket_sid AND status NOT LIKE '%DELETED%'
                    AND case_status_sid > 0 ORDER BY sid ASC ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));


        return $q->fetchAll();
    }

    public function caseNeedChecklist($ticket_sid){
        $sql = "SELECT * FROM ticket_need_checklist WHERE ticket_sid = :ticket_sid AND data_status > '-1' ORDER BY sequence ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        return $q->fetchAll();
    }

    public function listCaseOfProjectByType($project_sid){
        $sql = "SELECT DISTINCT(T.case_type) type
                FROM ticket T
                    LEFT JOIN employee E ON E.emailaddr = T.create_by
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                WHERE 1 AND T.owner IS NOT NULL AND T.project_sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        $r = $q->fetchAll();

        // foreach ($r as $key => $value) {
        //     $r[$key]['task'] = $this->listTaskOfCase($value['sid']);
        // }
        return $r;
    }

    // CASE WHEN (SELECT tasks.sid FROM tasks INNER JOIN customer_signature_task ON tasks.sid = customer_signature_task.task_sid
    //                 INNER JOIN ticket ON tasks.ticket_sid = ticket.sid
    //                 WHERE 1 AND tasks.ticket_sid = T.sid AND ticket.case_type = 'Implement' LIMIT 0,1)  > 0
    //                 THEN
    //                     CONCAT('<a href=\'https://case.flgupload.com/apis/v1/mergepdf/mergePDF/',T.contract_no,'\'>PDF</a>')
    //                 ELSE '' END all_sr_implement,

    public function listCaseOfContract($contract_no, $project_sid){
        $sql = "SELECT T.*,CASE WHEN T.no_project_ticket <> '' THEN T.no_project_ticket ELSE T.no_ticket END no_ticket,E.thainame,E.empno,E.maxportraitfile,
                    CONCAT(CASE WHEN T.end_user_phone != '' THEN T.end_user_phone ELSE '' END,' ',CASE WHEN T.end_user_mobile != '' THEN T.end_user_mobile ELSE '' END) end_user_phone_mobile,
                (SELECT tasks.appointment FROM tasks WHERE 1 AND tasks.ticket_sid = T.sid ORDER BY tasks.sid DESC LIMIT 0,1) last_appointment
                FROM ticket T
                    LEFT JOIN employee E ON E.emailaddr = T.create_by
                    LEFT JOIN project_owner PO ON PO.sid = T.project_owner_sid
                    LEFT JOIN project P ON P.sid = PO.project_sid
                WHERE 1 AND T.owner IS NOT NULL AND T.contract_no = :contract_no AND T.project_sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':contract_no'=>$contract_no,':project_sid'=>$project_sid));
        $r = $q->fetchAll();

        foreach ($r as $key => $value) {
            $r[$key]['task'] = $this->listTaskOfCase($value['sid']);
        }
        return $r;
    }

    public function projectContact($project_sid){
        $sql = "SELECT * FROM project_contact WHERE project_sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        $r = $q->fetchAll();
        return $r;
    }

    public function projectDetail($project_sid, $email="", $token=""){
        $this->project_sid = $project_sid;

        $sql = "SELECT PO.man_days, PO.man_hours, PO.owner, E.thainame, E.engname,E.maxportraitfile pic,
        P.name , P.end_user,
				P.project_start,
				P.project_end,
				P.project_type,E.mobile,P.contract contract_no,P.end_user_address end_user_address,P.sid project_sid,
        CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full,
        U.picture_profile pic_full,
        PO.status project_owner_status,P.data_status,
        (SELECT detail FROM project WHERE sid = PO.project_sid ORDER BY sid DESC LIMIT 0,1) detail_project,PO.sid project_owner_sid,
        P.phase,PO.status project_owner_status, P.create_by create_project_by, E_P.thainame craeate_project_thainame,
        DATE_FORMAT(P.create_datetime, '%d.%m.%Y %H:%i') create_datetime_df,
        CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END thainame,
        CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END engname
        FROM project_owner PO LEFT JOIN employee E ON PO.owner = E.emailaddr
        LEFT JOIN project P ON P.sid = PO.project_sid
        LEFT JOIN employee E_P ON P.create_by = E_P.emailaddr
        LEFT JOIN user U ON U.email = PO.owner AND U.email <> ''
        WHERE project_sid = :project_sid AND PO.status > 0 ORDER BY PO.create_datetime ASC, PO.status DESC";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$this->project_sid));

        $r = $q->fetchAll();
        $data = array();
        $data['man_hours_total'] = 0;
        if(isset($r[0]['detail_project'])){
            $data['detail_project'] = $r[0]['detail_project'];
        }else{
            $data['detail_project'] = "";
        }
        $data['owner'] = array();
        foreach ($r as $key => $value) {
            if($value['project_owner_status']>0){
                $data['man_hours_total'] += $value['man_hours'];
                $data['man_hours_total'] += $value['man_days']*8;
            }
            $data['craeate_project_thainame'] = $value['craeate_project_thainame'];
            $data['create_datetime_df'] = $value['create_datetime_df'];

            if(isset($r[0])){
                $r[0]['project_contact'] = $this->projectContact($r[0]['project_sid']);
                $r[0]['project_permission'] = $this->getPermissionProjectCreator($value['create_project_by']);
                $data['project_detail'] = $r[0];
            }
            array_push($data['owner'], array(
                'thainame'=>$value['thainame'],
                'engname'=>$value['engname'],
                'email'=>$value['owner'],
                'mobile'=>$value['mobile'],
                'pic'=>$value['pic'],'pic_full'=>$value['pic_full'],
                'man_hours'=>$value['man_hours'],'man_days'=>$value['man_days'],
                'project_owner_status'=>$value['project_owner_status'],
                'project_owner_sid'=>$value['project_owner_sid'],
                'project_sid'=>$value['project_sid']
                )
            );
        }

        // $r['man_hours'] += ($r['man_days'] * 8);
        return $data;
    }

    private function getPermissionProjectCreator($emailCreatorProject){
        $sql = "SELECT * FROM role R LEFT JOIN user U ON U.role_sid = R.sid WHERE U.email = :email ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$emailCreatorProject));
        $r = $q->fetch();
        return $r['project_create_able'];
    }


    public function listTaskOfCase($ticket_sid){
        $sql = "SELECT T.*,ST.name service_type_name,ST.cal_man_hours,
         U.picture_profile staff_pic , CASE WHEN U.formal_name <> ''
         THEN U.formal_name ELSE U.name END staff_name
        FROM tasks T
        LEFT JOIN user U ON U.email = T.engineer
        LEFT JOIN service_type ST ON T.service_type = ST.sid
        WHERE T.ticket_sid = :ticket_sid AND T.engineer <> '' ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':ticket_sid'=>$ticket_sid));
        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $r[$key]['timestamp'] = $this->listTaskDetail($value['sid']);

            if(file_exists(ROOT_PATH.'pdf/'.$value['no_task'].'.pdf')){
                $r[$key]['pdf_report'] = "<a href='".URL."pdf/".$value['no_task'].".pdf' target='new'>PDF</a>";
            }else{
                $r[$key]['pdf_report'] = "-";
            }

            if($r[$key]['last_status']<300){
                $r[$key]['can_change_appointment'] = 1;
            }else{
                $r[$key]['can_change_appointment'] = 0;
            }
        }
        return $r;
    }

    public function listTaskDetail($tasks_sid, $ticket_sid="", $email=""){
        if($email!=""){
            $this->m->setTable($email);
        }
        $sql = "SELECT T.tasks_sid,T.sid tasks_log_started_sid,DATE_FORMAT(T.create_datetime,'%d.%m.%Y %H:%i') start_job, T.engineer,
            T.taxi_fare taxi_fare_1,
            E.thainame, E.engname, E.maxportraitfile pic,'".$ticket_sid."' ticket_sid,
            (SELECT DATE_FORMAT(create_datetime,'%d.%m.%Y %H:%i') FROM ".$this->m->table_tasks_log." TT WHERE TT.tasks_sid = :tasks_sid AND TT.status = 400 AND TT.engineer = T.engineer LIMIT 0,1) close_job,
            (SELECT sid FROM ".$this->m->table_tasks_log." TT WHERE TT.tasks_sid = :tasks_sid AND TT.status = 400 AND TT.engineer = T.engineer LIMIT 0,1) tasks_log_closed_sid,
             (SELECT taxi_fare FROM ".$this->m->table_tasks_log." TT WHERE TT.tasks_sid = :tasks_sid AND TT.status = 600 AND TT.engineer = T.engineer LIMIT 0,1) taxi_fare_2,
             TIMESTAMPDIFF(MINUTE,T.create_datetime, (SELECT create_datetime FROM ".$this->m->table_tasks_log." TT WHERE TT.tasks_sid = :tasks_sid AND TT.status = 400 AND TT.engineer = T.engineer LIMIT 0,1)) use_minutes,
             CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic
        FROM ".$this->m->table_tasks_log." T LEFT JOIN employee E ON T.engineer = E.emailaddr AND E.emailaddr <> ''
        WHERE T.tasks_sid = :tasks_sid AND T.status = 300 GROUP BY T.engineer ORDER BY T.sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':tasks_sid'=>$tasks_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function listProjectOwner(){
        $sql = "SELECT E.thainame,PO.*,PO.project_start start_project, PO.project_end end_project
                FROM
                    project_owner PO
                LEFT JOIN employee E ON PO.owner = E.emailaddr AND E.emailaddr != ''
                WHERE PO.project_sid = :project_sid AND PO.status > 0 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$this->getProjectSid()));
        $r = $q->fetchAll();
        return $r;
    }

    public function setMandayProjectImplement($project_sid, $engineer, $man_day, $start_project, $end_project, $email){
        if($start_project){
            $start_project = explode("-", $start_project);
            $start_project = $start_project[2]."-".$start_project[1]."-".$start_project[0];
        }
        if($end_project){
            $end_project = explode("-", $end_project);
            $end_project = $end_project[2]."-".$end_project[1]."-".$end_project[0];
        }
        $sql = "UPDATE project SET
            man_days = :man_days,
            project_start = :project_start,
            project_end = :project_end,
            updated_datetime = NOW(),
            updated_by = :updated_by
            WHERE sid = :sid";
        $q = $this->db->prepare($sql);
        $q->execute(
                array(
                    ':man_days'=>$man_day,
                    ':project_start'=>$start_project,
                    ':project_end'=>$end_project,
                    ':updated_by'=>$email,
                    ':sid' => $project_sid
                    )
            );
        $sql = "INSERT INTO project_owner (project_sid, owner, create_datetime, create_by, project_start, project_end)
                VALUES (:project_sid, :owner, NOW(), :create_by, :project_start, :project_end) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_sid' => $project_sid,
                ':owner' => $engineer,
                ':create_by' => $email,
                ':project_start'=>$start_project,
                ':project_end' =>$end_project
            ));
    }
    public function listmyprojects($email, $project_type=""){
        $data = array();
        $lists = $this->lists($email, $project_type);
        // foreach ($lists as $key => $value) {
        //     array_push($data, $value);
        // }
        return $lists;
    }

    public function listnewprojects(){
        $data = array();
        $lists = $this->lists();
        foreach ($lists as $key => $value) {
            if($value['project_type']==""){
                array_push($data, $value);
            }
        }
        return $data;
    }

    public function listProjectInstallWaitSetManday(){
        $data = array();
        $lists = $this->lists();
        foreach ($lists as $key => $value) {
            if($value['project_type']=="Install" && ($value['man_days']=="0" || $value['man_days']=="" || $value['owner']=="") ){
                array_push($data, $value);
            }
        }
        return $data;
    }

    public function listProjectImplementWaitSetManday(){
        $data = array();
        $lists = $this->lists();
        foreach ($lists as $key => $value) {
            if($value['project_type']=="Implement" && ($value['man_days']=="0" || $value['man_days']=="" || $value['owner']=="")){
                array_push($data, $value);
            }
        }
        return $data;
    }


    public function setProjectType($project_sid, $project_type, $email){
        $sql = "UPDATE project SET
            project_type = :project_type,
            updated_by = :updated_by,
            updated_datetime = NOW()
            WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        return $q->execute(array(':project_type'=>$project_type,':updated_by'=>$email,':sid'=>$project_sid));
    }

    public function addProject($data, $email){
        return 0;
        if(isset($data['owner']) && $data['owner']!=""){
            $owner = explode('(',$data['owner']);
            $owner = explode(')', $owner[1]);
            $owner = $owner[0];
        }else{
            $owner = "";
        }

        if(!isset($data['customer'])){
            $data['customer'] = "";
        }

        if(!isset($data['name'])){
            $data['name'] = "";
        }

        if(!isset($data['contract'])){
            $data['contract'] = "";
        }

        if(!isset($data['vendor'])){
            $data['vendor'] = "";
        }
        if($data['sid']!=""){
            $sql = "UPDATE project SET
                    name = :name,
                    owner = :owner,
                    customer = :customer,
                    contract = :contract,
                    updated_datetime = NOW(),
                    updated_by = :updated_by,
                    vendor = :vendor
                    WHERE sid = :sid ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':name'=>$data['name'],
                ':owner'=>$owner,
                ':customer'=>$data['customer'],
                ':contract'=>$data['contract'],
                ':updated_by'=>$email,
                ':vendor' => $data['vendor'],
                ':sid'=>$data['sid']
                ));
        }else{
            $sql = "INSERT INTO project
                    (name,customer, contract, create_datetime, create_by, owner, vendor)
                    VALUES
                    (:name, :customer, :contract, NOW(), :create_by, :owner, :vendor) ";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':name'=>$data['name'],
                ':customer'=>$data['customer'],
                ':contract'=>$data['contract'],
                ':create_by'=>$email,
                ':owner' => $owner,
                ':vendor' => $data['vendor']
                ));
        }
    }

    private function setProjectSid($project_sid){
        $this->project_sid = $project_sid;
    }

    private function getProjectSid(){
        return $this->project_sid;
    }

    private function setOwner($owner){
        $this->owner = $owner;
    }
    private function getOwner(){
        return $this->owner;
    }

    public function plan($project_sid, $contract_no){
        $res = array();
        $this->project_sid = $project_sid;
        $this->contract = $contract_no;
        $data = $this->projectData($project_sid);
        $this->project_type = $data['project_type'];

        $res = $this->listPlan();
        return $res;

    }

    private function listPlan(){
        $sql = "SELECT *, '".$this->project_sid."' project_sid, '".$this->contract."' contract_no FROM plan WHERE type = 'all' OR type = :type ORDER BY sequence";
        $q = $this->db->prepare($sql);
        $q->execute(array(':type'=>$this->project_type));
        $r = $q->fetchAll();
        return $r;
    }

    private $order_by;
    private $search;
    public function listProject($email, $token, $order_by = array(), $search=""){
        $this->email = $email;
        $this->token = $token;
        $this->search = $search;
        $this->order_by = $order_by;
        $this->can_assign = $this->m->getPermissionProject($email);
        return $this->selectProject();
    }

    private function allManHours($project_sid){
        $sql = "SELECT man_days, man_hours FROM project_owner WHERE project_sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        $r = $q->fetchAll();
        $man_days = 0;
        foreach ($r as $key => $value) {
            $man_days += $value['man_days']*8;
            $man_days += $value['man_hours'];
        }
        return $man_days;
    }
    private function selectProject(){

        if($this->can_assign!=""){
            $sqlCondition = " (U.role_sid IN (".$this->can_assign.") OR PO.owner = '".$this->email."' OR P.create_by = '".$this->email."') ";
        }else{
            $sqlCondition = " PO.owner = '".$this->email."' ";
        }
        $sql = "SELECT P.*,DATE_FORMAT(P.create_datetime,'%d.%m.%Y') create_datetime_df, PO.sid project_owner_sid
        FROM project P
        LEFT JOIN project_owner PO ON P.sid = PO.project_sid
        LEFT JOIN user U ON PO.owner = U.email WHERE 1 AND P.data_status = '1' AND PO.status = '1' AND ";
        $sql .= $sqlCondition;
        if($this->search!=""){
            $sql .= "AND
            (P.contract LIKE :search
                OR P.name LIKE :search
                OR P.end_user LIKE :search
                OR PO.owner LIKE :search ) ";
        }
        $sql .= "GROUP BY P.sid ";
        if(isset($this->order_by['column']) && isset($this->order_by['type'])){
            $sql .= " ORDER BY ".$this->order_by['column']." ".$this->order_by['type'];
        }else{
            $sql .= " ORDER BY P.create_datetime DESC ";
        }
        $q = $this->db->prepare($sql);
        if($this->search!=""){
            $q->execute(array(':search'=>"%".$this->search."%"));
        }else{
            $q->execute();
        }

        $r = $q->fetchAll();
        foreach ($r as $key => $value) {
            $r[$key]['use_hours'] = floor(intval($value['use_minutes']) / 60);
            $r[$key]['use_minutes_mod'] = (intval($value['use_minutes']) % 60);
            $r[$key]['use_minutes_mod'] = (intval($r[$key]['use_minutes_mod'] + "") == 1) ? "0" + ($r[$key]['use_minutes_mod']) : $r[$key]['use_minutes_mod'];
            $r[$key]['allManHours'] = $this->allManHours($value['sid']);
            $r[$key]['owner'] = $this->selectProjectOwner($value['sid']);
        }
        return $r;
    }

    private function selectProjectOwner($project_sid){
        $sql = "SELECT E.emailaddr, CONCAT('".END_POINT_PATH_EMPLOYEE."',E.maxportraitfile) pic_full, E.thainame, E.engname, U.email,
        U.picture_profile pic_full
        FROM project_owner PO
        LEFT JOIN employee E ON E.emailaddr = PO.owner AND E.emailaddr <> ''
        LEFT JOIN user U ON U.email = PO.owner
        WHERE PO.project_sid = :project_sid AND PO.status > 0";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        return $q->fetchAll();
    }

    public function listUserCanAddTask($email){
        $this->email = $email;
        return $this->m->listUserCanAddTask($email);
    }

    public function listUserCanAddProject($email){
        $this->email = $email;
        return $this->m->listUserCanAddProject($email);
    }

    public function projects($permission_project, $email, $status, $search, $data=array()){
        try{
            $queryStatus = '';
            if($status==1){
                $queryStatus = " AND (data_status = '1' OR data_status = '100' OR data_status = '200' OR data_status = '300') ";
            }else if($status==1000){
                $queryStatus = " AND data_status > 0 ";
            }else if($status=="100"){
                $queryStatus = " AND data_status > '100' ";
            }else if($status=="200"){
                $queryStatus = " AND data_status = '200' ";
            }else if($status=="300"){
                $queryStatus = " AND data_status > '300' ";
            }else if($status=="500"){
                $queryStatus = " AND data_status = '400' ";
            }else {
                $queryStatus = " AND data_status = '400' ";
            }
            $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';

            $permissionQuery = 'AND (T.mgr_owner = :email OR PO.owner = :email OR T.create_by = :email) ';
            if($permission_project!="0" && $permission_project!="" && $permission_project){
                $permissionQuery = 'AND (T.mgr_owner = :email OR PO.owner = :email OR T.create_by = :email OR U.role_sid IN ('.$permission_project.') ) ';
            }
            $searchQuery = '';
            if($search!==""){
                $searchQuery = ' AND (T.name LIKE "%'.$search.'%" OR T.create_by LIKE  "%'.$search.'%" ';
                $searchQuery .= ' OR T.contract LIKE "%'.$search.'%" OR T.end_user LIKE "%'.$search.'%" ) ';
            }

            $search = "";
            if(isset($data['search']) && $data['search']!=""){
                $search .= " AND (T.name LIKE '%".$data['search']."%'
                OR T.contract LIKE '%".$data['search']."%'
                ) ";
            }


            $sqlCreateDate = "";
            if(isset($data['startDate']) && isset($data['endDate'])){
                $sqlCreateDate .= "AND (T.create_datetime BETWEEN '".$data['startDate']."' AND '".$data['endDate']."') ";
            }


            $sql = 'SELECT T.*,T.contract contract_no,
						CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name,
						CASE
							WHEN T.data_status = "400" THEN "Done"
							WHEN T.data_status = "200" THEN "Wait approve plan"
							WHEN T.data_status = "1" THEN "New"
							WHEN T.data_status = "300" THEN "Process"
							WHEN T.data_status = "100" THEN "Wait Planning"
							ELSE "Unknow" END status_label,
            CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END owner_name ';
            $sql .= $create_datetime;
            $sql .= ',T.end_user ';
						$sql .= ', CASE WHEN U3.formal_name <> "" THEN U3.formal_name ELSE U3.name END mgr_owner_name  ';
            // $sql .= ',U.picture_profile picture_profile ';
            $sql .= 'FROM project T ';
            $sql .= 'LEFT JOIN project_owner PO ON T.sid = PO.project_sid AND PO.status > -1 ';
            $sql .= 'LEFT JOIN user U ON T.owner = U.email ';
            $sql .= 'LEFT JOIN user U2 ON U2.email = T.create_by ';
						$sql .= 'LEFT JOIN user U3 ON T.mgr_owner = U3.email OR T.mgr_owner = U3.email_2 ';
            $sql .= 'WHERE 1 AND T.data_status > -1 ';
            $sql .= $permissionQuery;
            $sql .= $searchQuery;
            $sql .= $search;
            $sql .= $queryStatus;
            // $sql .= $sqlCreateDate;
            $sql .= 'GROUP BY T.sid DESC LIMIT '.$data['dataStart'].','.$data['dataLimit'];

            $q = $this->db->prepare($sql);
            $q->execute(
                array(
                    ':email'=>$email,
                    ':status'=>$queryStatus
                    )
            );
            $r = $q->fetchAll();


            return $r;
        }catch(Exception $e){
            echo ($e);
            return array();
        }
		// query = db.query(sql,[email,email,queryStatus],function(e, r, f){
		// 	if(e) reject(e);
		// 	else resolve(r);
		// });
    }

    public function projectDetail2($project_sid, $email=""){
        $sql = "SELECT P.*,
        P.project_start,
        P.project_end,
        U.name created_name, P.contract,
				CASE WHEN U2.formal_name <> '' THEN U2.formal_name ELSE U2.name END mgr_owner_name, P.mgr_owner,
				CASE WHEN U3.formal_name <> '' THEN U3.formal_name ELSE U3.name END owner_name, P.owner owner_email
         FROM project P
        LEFT JOIN user U ON P.create_by = U.email
				LEFT JOIN user U2 ON P.mgr_owner = U2.email
				LEFT JOIN user U3 ON P.owner = U3.email
        WHERE P.sid = :sid ";

        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$project_sid));
        $r = $q->fetch();

        $r['owner'] = array(0=>array('owner_name'=>$r['owner_name'],'owner_email'=>$r['owner_email']));//$this->projectOwner2($project_sid);
				// $r['mgr_owner'] = $this->projectMgrOwner($project_sid);
        $r['case_of_project'] = $this->caseOfTicket($project_sid);

        $r['project_plan'] = $this->getProjectPlan($project_sid);

				$r['permission'] = $this->permissionProjectDetail($email, $r['owner_email'], $r['mgr_owner'], $r['create_by'], $r['data_status']);
        return $r;
    }

		private function permissionApprovePlan($viewer, $mgr_owner, $project_status){
			if($viewer==$mgr_owner && $project_status=="200"){
				return true;
			}else{
				return false;
			}
		}

		private function permissionProjectDetail($viewer, $owner, $mgr_owner, $creater, $project_status){
			$permission = array(
				array(
					'edit_contract'=>true,
					'edit_address'=>true,
					'edit_man_hour'=>true,
					'edit_period'=>true,
					'edit_owner'=>true,
					'edit_mgr_owner'=>true,
					'edit_plan'=>true,
					'edit_detail'=>true,
					'close_project'=>true,
					'delete_project'=>true,
					'approve_plan'=>$this->permissionApprovePlan($viewer, $mgr_owner, $project_status)
				),
				array(
					'edit_contract'=>false,
					'edit_address'=>false,
					'edit_man_hour'=>false,
					'edit_period'=>false,
					'edit_owner'=>false,
					'edit_mgr_owner'=>false,
					'edit_plan'=>true,
					'edit_detail'=>false,
					'close_project'=>true,
					'delete_project'=>true,
					'approve_plan'=>$this->permissionApprovePlan($viewer, $mgr_owner, $project_status)
				),
				array(
					'edit_contract'=>false,
					'edit_address'=>false,
					'edit_man_hour'=>false,
					'edit_period'=>false,
					'edit_owner'=>false,
					'edit_mgr_owner'=>false,
					'edit_plan'=>false,
					'edit_detail'=>false,
					'close_project'=>false,
					'delete_project'=>false,
					'approve_plan'=>$this->permissionApprovePlan($viewer, $mgr_owner, $project_status)
				)
			);
			if(1==1 || $viewer=="nanthawat.s@firstlogic.co.th" || $viewer==$creater || $viewer==$mgr_owner){
				return $permission[0];
			}else if($viewer==$owner){
				return $permission[1];
			}else{
				return $permission[2];
			}
		}

    private function getProjectPlan($project_sid){
        $sql = "SELECT * FROM project_plan WHERE project_sid = :project_sid ORDER BY sequence";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        $r = $q->fetchAll();
        return $r;
    }

    public function editManHour($project_sid, $man_hours, $email){
        $sql = "UPDATE project
        SET man_hours = :man_hours, updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':man_hours'=>$man_hours,':email'=>$email,':sid'=>$project_sid));
    }
		public function projectMgrOwner($project_sid){

		}
    public function projectOwner2($project_sid){
        // try{
        //     $sql = "SELECT PO.sid owner_sid,CASE WHEN U.formal_name <> '' THEN U.formal_name ELSE U.name END owner_name,
        //     U.picture_profile owner_pic, U.email owner_email
        //     FROM project_owner PO LEFT JOIN user U ON PO.owner = U.email
        //     WHERE PO.project_sid = :project_sid AND PO.status > -1 ORDER BY PO.sid DESC LIMIT 0,1 ";
        //     $q = $this->db->prepare($sql);
        //     $q->execute(array(':project_sid'=>$project_sid));
        //     return $q->fetchAll();
        // }catch(Exception $e){
        //     echo ($e);
        //     return array();
        // }
    }

    public function caseOfTicket($project_sid){
        try{
            $create_datetime = ',DATE_FORMAT(T.create_datetime,"%Y-%m-%d %H:%i") create_datetime ';
            $statusLabel =',CASE WHEN T.status = "5" THEN "Resolve" WHEN T.status = "4" THEN "Pending" ELSE "Process" END status_label ';
            $sql = 'SELECT T.*,T.subject name, CASE WHEN U2.formal_name <> "" THEN U2.formal_name ELSE U2.name END create_by_name ';
            $sql .= $create_datetime;
            $sql .= ',T.end_user ';
            $sql .= ',CASE WHEN U.formal_name <> "" THEN U.formal_name ELSE U.name END owner_thainame ';
            $sql .= $statusLabel;
            $sql .= ',U.picture_profile owner_picture_profile ';
            $sql .= ',T.number_sr number_service_report ';
            $sql .= 'FROM ticket T ';
            $sql .= 'LEFT JOIN user U2 ON U2.email = T.create_by ';
            $sql .= 'LEFT JOIN user U ON U.email = T.owner ';
            $sql .= 'WHERE 1 ';
            $sql .= 'AND T.project_sid = :project_sid ';
            $sql .= 'ORDER BY sid ASC LIMIT 0,10000';
            $q = $this->db->prepare($sql);
            $q->execute(array(':project_sid'=>$project_sid));
            return $q->fetchAll();
        }catch(Exception $e){
            echo ($e);
            return array();
        }
    }

    public function addNewContractToProjectContractQuery($create_by, $project_sid){
        $sql = "SELECT * FROM project WHERE sid = :sid ORDER BY sid DESC LIMIT 0,1";
        $q = $this->db->prepare($sql);
        $q->execute(array(':sid'=>$project_sid));
        $data = $q->fetch();


        $sql = "INSERT INTO project_contract (project_sid, project_for_company,name, phase, detail, customer, end_user, man_days, project_start, project_end, project_type, sales, sales2, contract, prime_contract,vendor, so_no, pr_no,est_po_date,data_status,end_user_address, progress, updated_progress, create_by, create_datetime) VALUES (:project_sid, :project_for_company, :name, :phase, :detail, :customer, :end_user, :man_days, :project_start, :project_end, :project_type, :sales, :sales2, :contract, :prime_contract, :vendor, :so_no, :pr_no, :est_po_date, :data_status, :end_user_address, :progress, :updated_progress, :create_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_sid'=>$data['sid'],
                ':project_for_company'=>$data['project_for_company'],
                ':name'=>$data['name'],
                ':phase'=>$data['phase'],
                ':detail'=>$data['detail'],
                ':customer'=>$data['customer'],
                ':end_user'=>$data['end_user'],
                ':man_days'=>$data['man_days'],
                ':project_start'=>$data['project_start'],
                ':project_end'=>$data['project_end'],
                ':project_type'=>$data['project_type'],
                ':sales'=>$data['sales'],
                ':sales2'=>$data['sales2'],
                ':contract'=>$data['contract'],
                ':prime_contract'=>$data['prime_contract'],
                ':vendor'=>$data['vendor'],
                ':so_no'=>$data['so_no'],
                ':pr_no'=>$data['pr_no'],
                ':est_po_date'=>$data['est_po_date'],
                ':data_status'=>$data['data_status'],
                ':end_user_address'=>$data['end_user_address'],
                ':progress'=>$data['progress'],
                ':updated_progress'=>$data['updated_progress'],
                ':create_by'=>$create_by
            ));

    }
    public function updateContractToProjectQuery($updated_by, $project_sid,
		$newContractNo, $project_name, $end_user_name, $end_user_address, $customer){

				if($project_sid && $newContractNo && $updated_by){
	        $sql = "UPDATE project SET
	        contract = :contract,
	        updated_by = :updated_by,
	        updated_datetime = NOW(),
	        customer = :customer,
	        end_user = :end_user,
	        end_user_address = :end_user_address,
	        name = :name
	        WHERE sid = :sid ";
	        $q = $this->db->prepare($sql);
	        $q->execute(array(
	            ':contract'=>$newContractNo,
	            ':updated_by'=>$updated_by,
	            ':customer'=>$customer,
	            ':end_user'=>$end_user_name,
	            ':end_user_address'=>$end_user_address,
	            ':name'=>$project_name,
	            ':sid'=>$project_sid
	        ));
					return array('status'=>1,'message'=>'Updated');
				}else{
					return array('status'=>0,'message'=>'project_sid, email, newContractNo is not null');
				}
    }

    public function projectSavePlan($data){
        $email = $data['email'];
        $plan = $data['data'];
        $project_sid = $data['project_sid'];

        $maxSequence = 0;
        foreach ($plan as $key => $value) {
						if($value['status']<300){
	            $project_plan_sid = $this->projectPlanExist($project_sid, $key);
	            if($project_plan_sid>0){
	                $this->updatePlan($value, $key, $project_sid, $email, $project_plan_sid);
	            }else{
	                $this->insertNewPlan($value, $key, $project_sid, $email);
	            }
						}
            $maxSequence = $key;
        }

        $this->deletePlanMoreThanSequence($project_sid, $maxSequence);
    }


    public function projectPlanOpenedCase($data){
        $email = $data['email'];

        $project_plan_sid = $data['project_plan_sid'];
        $sql = "UPDATE project_plan SET status = '500', updated_datetime = NOW(), updated_by = :email WHERE sid = :project_plan_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email,':project_plan_sid'=>$project_plan_sid));
    }


    public function projectSendPlan($data){
        $email = ($data['email'])?$data['email']:'';
        $project_sid = ($data['project_sid'])?$data['project_sid']:'';

				if($project_sid && $email){
	        $sql = "UPDATE project_plan
	        SET status = :status, updated_by = :email, updated_datetime = NOW() WHERE project_sid = :project_sid AND status < '500' ";
	        $q = $this->db->prepare($sql);
	        $q->execute(array(':status'=>'200',':email'=>$email, ':project_sid'=>$project_sid));

					$sql = "UPDATE project SET data_status = '200' WHERE sid = :project_sid ";
					$q = $this->db->prepare($sql);
					$q->execute(array(':project_sid'=>$project_sid));

					$this->messageConsiderProjectPlan($project_sid);
				}
    }

		private function messageConsiderProjectPlan($project_sid){
			$projectDatail = $this->projectDetail2($project_sid);
			$message = 'เข้าตรวจสอบและอนุมัติ Plan <br/><br/>';
			$message .= 'Project name: '.$projectDatail['name'].'<br/>';
			$message .= 'Contract: '.$projectDatail['contract'].'<br/>';
			$message .= 'End user: '.$projectDatail['end_user'].'<br/>';
			$message .= 'Customer: '.$projectDatail['customer'].'<br/><br/>';
			if(isset($projectDatail['owner']['0']['owner_name'])){
				$message .= 'Owner: '.$projectDatail['owner']['0']['owner_name'].'<br/>';
			}
			$message .= 'Status: รออนุมัติ Plan'.'<br/>';

			$subject = 'Project '.$projectDatail['name'].' รออนุมัติ Plan';
			require_once 'SendMailProject.php';
			$obj = new SendMailProject();
			$obj->noticeMailConsiderProjectPlan($projectDatail['mgr_owner'], $message, $subject);
		}


    private function deletePlanMoreThanSequence($project_sid, $maxSequence){
        $sql = "DELETE FROM project_plan WHERE project_sid = :project_sid AND sequence > :maxSequence ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid,':maxSequence'=>$maxSequence));
    }
    private function projectPlanExist($project_sid, $sequence){
        $sql = "SELECT * FROM project_plan WHERE project_sid = :project_sid AND sequence = :sequence ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid,':sequence'=>$sequence));
        $r = $q->fetch();
        if(isset($r['sid']) && $r['sid']>0){
            return $r['sid'];
        }else{
            return '0';
        }
    }
    private function insertNewPlan($data, $sequence, $project_sid, $create_by){
        $sql = "INSERT INTO project_plan
        (subject, project_sid, man_hours, sequence, create_by, create_datetime, updated_by, updated_datetime, status)
        VALUES
        (:subject, :project_sid, :man_hours, :sequence, :create_by, NOW(), :updated_by, NOW(), :status) ";
        $q = $this->db->prepare($sql);
        $q->execute(
            array(
                ':subject'=>$data['subject'],':project_sid'=>$project_sid,':man_hours'=>$data['man_hours'],
                ':sequence'=>$sequence,':create_by'=>$create_by,':updated_by'=>$create_by,':status'=>'1'
                )
            );

    }
    private function updatePlan($data, $sequence, $project_sid, $create_by, $project_plan_sid){
        $sql = "UPDATE project_plan SET subject = :subject, man_hours = :man_hours, project_sid = :project_sid, sequence = :sequence, updated_by = :email, updated_datetime = NOW(), status = '1' WHERE sid = :project_plan_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':subject'=>$data['subject'],':man_hours'=>$data['man_hours'],':project_sid'=>$project_sid,':sequence'=>$sequence,':email'=>$create_by,':project_plan_sid'=>$project_plan_sid));
    }

    public function deleteProject($data){
			if(isset($data['project_sid'])){
		        $isExistCaseIsProcess = $this->isExistCaseIsProcess($data);
		        $isExistProjectPlatUnused = $this->isExistProjectPlatUnused($data);
		        $status = 0;
		        if(count($isExistCaseIsProcess)>0){
		            return array('message'=>'ไม่สามารถลบ Project ได้ เนื่องจาก มีการเปิดเคสแล้ว', 'status'=>$status);
		        }

		        if(count($isExistProjectPlatUnused)>0){
		            return array('message'=>'ไม่สามารถปิด Project ได้ เนื่องจาก มีการสร้าง Plan แล้ว ', 'status'=>$status);
		        }

		        $sql = "UPDATE project SET data_status = '800' WHERE sid = :project_sid ";
		        $q = $this->db->prepare($sql);
		        $q->execute(array(':project_sid'=>$data['project_sid']));
		        $status = 1;
		        return array('message'=>'ลบ Project แล้ว', 'status'=>$status);
			}else{
				return array('message'=>'Required sid', 'status'=>"0");
			}
    }

    private function isExistCaseIsProcess($data){
        $sql = "SELECT * FROM ticket WHERE project_sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$data['project_sid']));
        $r = $q->fetchAll();
        return $r;
    }

    private function isExistProjectPlatUnused($data){
        $sql = "SELECT * FROM project_plan WHERE project_sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$data['project_sid']));
        $r = $q->fetchAll();
        return $r;
    }
}
?>
