<?php
error_reporting(-1);
ini_set('display_errors', 'On');
class SettingMapAlertModel{
	private $db;
  private $m;
  private $email, $token;
	function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->email = $email;
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
  }
  public function selectData(){
    try{
        $sql = "SELECT * FROM gable_map_group ORDER BY sid DESC";
        $q = $this->db->prepare($sql);
        $q->execute();
        $r = $q->fetchAll();
        return array('status'=>1, 'message'=>'Data Ok', 'data'=>$r);
    }catch(PDOException $e){
        return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function addData($data){
    try{
      $map_value = isset($data['map_value'])?$data['map_value']:'';
      $map_to = isset($data['map_to'])?$data['map_to']:'';
      $for_task = "alert";

      $email = isset($data['email'])?$data['email']:'';

      if($map_value && $map_to && $email){
        $sql = "INSERT INTO gable_map_group (map_value, map_to, for_task, created_by, created_datetime)
        VALUES (:map_value, :map_to, :for_task, :created_by, NOW()) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
          ':map_value'=>$map_value,
          ':map_to'=>$map_to,
          ':for_task'=>$for_task,
          ':created_by'=>$email
        ));
        return array('status'=>1, 'message'=>'Added', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'map_value, map_to, email ต้องไม่เป็นค่าว่าง', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function editData($data){
    try{
      if(isset($data['sid'])){
        $map_value = isset($data['map_value'])?$data['map_value']:'';
        $map_to = isset($data['map_to'])?$data['map_to']:'';
        $for_task = "alert";

        $email = isset($data['email'])?$data['email']:'';

        if($map_value && $map_to && $email){
          $sql = "UPDATE gable_map_group
          SET map_value = :map_value, map_to = :map_to,
          updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
          $q = $this->db->prepare($sql);
          $q->execute(
            array(
              ':map_value'=>$map_value,
              ':map_to'=>$map_to,
              ':email'=>$email,
              ':sid'=>$data['sid']
            )
          );
          return array('status'=>1, 'message'=>'Updated', 'data'=>array());
        }else{
          return array('status'=>0, 'message'=>'ข้อมูล map_value, map_to, email ต้องไม่เป็นค่าว่าง', 'data'=>array());
        }
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
  public function deleteData($data){
    try{
      if(isset($data['sid'])){
        $email = isset($data['email'])?$data['email']:'';

        $sql = "UPDATE gable_map_group SET data_status = '-1', updated_datetime = NOW(), updated_by = :email WHERE sid = :sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':email'=>$email,':sid'=>$data['sid']));
        return array('status'=>1, 'message'=>'Deleted', 'data'=>array());
      }else{
        return array('status'=>0, 'message'=>'Required sequence id', 'data'=>array());
      }
    }catch(PDOException $e){
      return array('status'=>0, 'message'=>$e->getMessage(), 'data'=>array());
    }
  }
}
