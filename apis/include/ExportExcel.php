<?php
ob_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('Europe/London');

function exportReportSr($data, $secondChar, $start, $end, $group_name){

	/** Include PHPExcel */
	require_once dirname(__FILE__) . '../../../PHPExcel-1.8/Classes/PHPExcel.php';
	// Create new PHPExcel object
	echo date('H:i:s') , " Create new PHPExcel object" , EOL;
	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()->setCreator("Mintra")
							 ->setLastModifiedBy("Mintra")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

	// Add some data, we will use printing features
	echo date('H:i:s') , " Add some data" , EOL;
	$i = 1;
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, "Contract");
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, "Customer");
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, "End user");
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, "Sr No");
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, "Subject");
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, "Started_datetime");
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, "Closed_datetime");
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, "Eng1");
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, "Eng2");
	$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, "Eng3");
	$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, "Eng4");
	$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, "Eng5");
	$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, "Eng6");
	$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, "Eng7");
	$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, "Eng8");
	$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, "Eng9");
	$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, "Eng10");

	foreach ($data as $key => $value) {
		$i = $key+2;
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value['contract_no']);
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value['customer']);
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value['end_user']);
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value['no_task']);
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value['subject']);
		$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $value['started_datetime']);
		$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $value['closed_datetime']);
		$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $value['eng1']);
		$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value['eng2']);
		$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $value['eng3']);
		$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $value['eng4']);
		$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $value['eng5']);
		$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $value['eng6']);
		$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $value['eng7']);
		$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $value['eng8']);
		$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $value['eng9']);
		$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $value['eng10']);
	}

	// Rename sheet
	echo date('H:i:s') . " Rename sheet\n";
	$objPHPExcel->getActiveSheet()->setTitle('My Customer');


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Save Excel 2007 file
	echo date('H:i:s') . " Write to Excel2007 format\n";
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$strFileName = "service_report_".$secondChar."_".$start."-".$end."_".$group_name.".xlsx";
	$objWriter->save($strFileName);
	// echo dirname(__FILE__);

	// Echo memory peak usage
	echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

	// Echo done
	echo date('H:i:s') . " Done writing file.\r\n";
	echo "<br/>";
	echo "<a href='".URL."apis/v1/report/".$strFileName."'>Download</a>";
}

?>
