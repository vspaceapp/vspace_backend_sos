<?php
class ProjectModifyModel{
	private $db;
    private $objCons;
    private $email;
    public $permission_role;
    private $m;
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->m = $db = new DbConnect();
        $this->db = $db->connect();
    }

    public function addStaffAgain($email, $project_owner_sid, $project_sid, $new_staff_email){
        $beforeData = $this->getInfoProjectOwner($project_owner_sid);
        $project_start = $beforeData['project_start'];
        $project_end = $beforeData['project_end'];
        $man_days = $beforeData['man_days'];
        $man_hours = $beforeData['man_hours'];

        $sql = "INSERT INTO project_owner (owner, project_sid, project_start, project_end, man_days, man_hours, status, create_by, create_datetime) VALUES (:new_staff_email,:project_sid,:project_start,:project_end, :man_days,:man_hours,'1', :email,NOW() ) ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':new_staff_email' => $new_staff_email,
            ':project_sid' => $project_sid,
            ':project_start' =>  $project_start,
            ':project_end' => $project_end,
            ':man_days' => $man_days,
            ':man_hours' => $man_hours,
            ':email' => $email
            // ':permission_role'=> $this->permission_role['prefix_table']
            ));
    }

		public function changeOwner($email, $project_sid, $new_staff_email){
			if($email && $project_sid && $new_staff_email){
				try{
					$sql = "UPDATE project SET owner = :owner, updated_by = :updated_by, updated_datetime = NOW() WHERE sid = :project_sid ";
					$q = $this->db->prepare($sql);
					$q->execute(array(':owner'=>$new_staff_email, ':project_sid'=>$project_sid, ':updated_by'=>$email));

					$sql = "INSERT INTO project_owner (owner, project_sid, create_by, create_datetime)
					VALUES ( :new_staff_email, :project_sid, :email, NOW() ) ";
					$q = $this->db->prepare($sql);
					$q->execute(array(':new_staff_email'=>$new_staff_email, ':project_sid'=>$project_sid, ':email'=>$email));

					require_once 'SendMailProject.php';
					require_once 'ProjectModel.php';

					$objProjectModel = new ProjectModel();
					$projectDatail = $objProjectModel->projectDetail2($project_sid);

					$message = 'ได้รับมอบหมายเป็น Project owner<br/><br/>';
					$message .= 'Project name: '.$projectDatail['name'].'<br/>';
					$message .= 'Contract: '.$projectDatail['contract'].'<br/>';
					$message .= 'End user: '.$projectDatail['end_user'].'<br/>';
					$message .= 'Customer: '.$projectDatail['customer'].'<br/><br/>';
					$subject = 'ได้รับมอบหมายเป็น Project owner '.$projectDatail['name'].' '.$projectDatail['contract'];

					$objMail = new SendMailProject();
					$objMail->noticeMailNewProject($new_staff_email, $message, $subject, $email);
					
					return array('status'=>1, 'data'=>'Ok', 'message'=>'Changed owner');
				}catch(PDOException $e){
					return array('status'=>1, 'data'=>'', 'message'=>$e->getMessage());
				}
			}else{
				return array('status'=>0, 'data'=>'ข้อมูล email, project_sid, new_staff_email ต้องไม่เป็นค่าว่าง', 'message'=>'Fild');
			}
		}

		public function changeMgrOwner($email, $project_sid, $new_mgr_owner){
				if($email && $project_sid && $new_mgr_owner){
					try{
						$sql = "UPDATE project SET mgr_owner = :new_mgr_owner, updated_by = :updated_by, updated_datetime = NOW()
						WHERE sid = :project_sid ";
						$q = $this->db->prepare($sql);
						$q->execute(array(
							':new_mgr_owner'=>$new_mgr_owner,
							':updated_by'=>$email,
							':project_sid'=>$project_sid
						));
						return array('status'=>1, 'data'=>'Ok', 'message'=>'Changed mgr owner');
					}catch(PDOException $e){
						return array('status'=>1, 'data'=>'', 'message'=>$e->getMessage());
					}
				}else{
					return array('status'=>0, 'data'=>'ข้อมูล email, project_sid, new_staff_email ต้องไม่เป็นค่าว่าง', 'message'=>'Fild');
				}
		}

    public function addStaff($email, $project_sid, $new_staff_email){
        $beforeData = $this->checkRepeatOwner($new_staff_email, $project_sid);
        $project_start = "";
        $project_end = "";
        $man_days = "";
        $man_hours = "";

        if(!$beforeData){
            $sql = "INSERT INTO project_owner (owner, project_sid, project_start, project_end, man_days, man_hours, status, create_by, create_datetime) VALUES (:new_staff_email,:project_sid,:project_start,:project_end, :man_days,:man_hours,'1', :email,NOW() ) ";
            $q = $this->db->prepare($sql);
            return $q->execute(array(
                ':new_staff_email' => $new_staff_email,
                ':project_sid' => $project_sid,
                ':project_start' =>  $project_start,
                ':project_end' => $project_end,
                ':man_days' => $man_days,
                ':man_hours' => $man_hours,
                ':email' => $email
                // ':permission_role'=> $this->permission_role['prefix_table']
                ));

        }else{
            return false;
        }
    }


    private function checkRepeatOwner($email, $project_sid){
        $sql = "SELECT * FROM project_owner WHERE project_sid = :project_sid AND owner = :email AND status > 0 ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_sid' => $project_sid,
                ':email'=>$email
        ));
        $r = $q->fetch();
        return $r;
    }
    private function getInfoProjectOwner($project_owner_sid){
        $sql = "SELECT * FROM project_owner WHERE sid = :project_owner ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_owner' => $project_owner_sid
        ));
        $r = $q->fetch();
        return $r;
    }

    public function deleteStaff($email, $project_owner_sid){

        $sql = "UPDATE project_owner SET status = '-1', updated_datetime = NOW(), updated_by = :email WHERE sid = :project_owner_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':email' => $email,
            ':project_owner_sid' => $project_owner_sid
        ));
    }


    public function deleteStaffReact($email, $staff_email, $project_sid){

        $sql = "UPDATE project_owner SET status = '-1', updated_datetime = NOW(), updated_by = :email WHERE project_sid = :project_sid AND owner = :staff_email";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':email' => $email,
            ':project_sid' => $project_sid,
            ':staff_email'=>$staff_email
        ));
    }



    public function changeManday($project_owner_sid, $email, $man_days_new){
        $sql = "UPDATE project_owner SET man_days = :man_days, updated_datetime = NOW(), updated_by = :email WHERE sid = :project_owner_sid ";

        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':man_days' => $man_days_new,
            ':email' => $email,
            ':project_owner_sid' => $project_owner_sid
        ));
    }

    public function deleteProject($project_sid, $email){
        if(count($this->ticketInProject($project_sid))<1){
            $sql = "UPDATE project SET data_status = '-1', updated_by = :email, updated_datetime = NOW() WHERE sid = :project_sid";
            $q = $this->db->prepare($sql);
            $q->execute(array(
                ':email' => $email,
                ':project_sid' => $project_sid
            ));
        }
    }

    private function ticketInProject($project_sid){
        $sql = "SELECT * FROM ticket WHERE project_sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$project_sid));
        $r = $q->fetchAll();
        return $r;
    }

    private function isExistCaseIsProcess($data){
        $sql = "SELECT * FROM ticket WHERE project_sid = :project_sid AND (status != '5' AND status != '6') ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$data['project_sid']));
        $r = $q->fetchAll();
        return $r;
    }

    private function isExistProjectPlatUnused($data){
        $sql = "SELECT * FROM project_plan WHERE project_sid = :project_sid AND status != '500' ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':project_sid'=>$data['project_sid']));
        $r = $q->fetchAll();
        return $r;
    }

    public function closeProject($project_sid, $email, $remark){
        $status = 0;

        $isExistCaseIsProcess = $this->isExistCaseIsProcess(array('project_sid'=>$project_sid));
        if(count($isExistCaseIsProcess)>0){
            return array('message'=>'ไม่สามารถปิด Project ได้ เนื่องจาก มีเคสที่ In Process', 'status'=>$status);
        }

        $isExistProjectPlatUnused = $this->isExistProjectPlatUnused(array('project_sid'=>$project_sid));
        if(count($isExistProjectPlatUnused)>0){
            return array('message'=>'ไม่สามารถปิด Project ได้ เนื่องจาก มี Plan ที่ยังไม่ได้ใช้', 'status'=>$status);
        }

        $sql = "UPDATE project SET data_status = '400', updated_by = :email, remark_close_project = :remark, updated_datetime = NOW() WHERE sid = :project_sid";
        $q = $this->db->prepare($sql);
        $q->execute(array(
            ':email' => $email,
            ':project_sid' => $project_sid,
            ':remark'=>$remark
        ));
        $status = 1;
        return array('message'=>'ปิด Project แล้ว', 'status'=>$status);
    }

    public function addProjectContact($project_sid,$email, $token,$end_user_name, $end_user_email,$end_user_mobile,$end_user_phone,$end_user_company){

        $sql = "INSERT INTO project_contact (project_sid, name, email, mobile, phone, company, create_datetime, create_by) VALUES (:project_sid, :name, :email, :mobile, :phone, :company, NOW(), :create_by)";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_sid'=>$project_sid,
                ':name'=>$end_user_name,
                ':email'=>$end_user_email,
                ':mobile'=>$end_user_mobile,
                ':phone'=>$end_user_phone,
                ':company'=>$end_user_company,
                ':create_by'=>$email
            ));
    }

    public function updateProjectContact($project_contact_sid, $end_user_name, $end_user_email, $end_user_mobile, $end_user_phone, $end_user_company, $email, $token){

        $sql = "UPDATE project_contact SET name = :end_user_name, email = :end_user_email, mobile = :end_user_mobile, phone = :end_user_phone, company = :end_user_company, update_by = :update_by, update_datetime = NOW() WHERE sid = :project_contact_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                ':project_contact_sid'=>$project_contact_sid,
                ':end_user_name'=>$end_user_name,
                ':end_user_email'=>$end_user_email,
                ':end_user_mobile'=>$end_user_mobile,
                ':end_user_phone'=>$end_user_phone,
                ':end_user_company'=>$end_user_company,
                ':update_by'=>$email


                ));
    }


    public function editPeriod($data){
        $status = 0;
        if(!isset($data['project_sid'])){
            return array('message'=>'เกิดข้อผิดพลาด ไม่ข้อมูลไม่เพียงพอ','status'=>$status);
        }

        if(!isset($data['project_start']) || $data['project_start']=="0000-00-00"){
            return array('message'=>'เกิดข้อผิดพลาด ตรวจสอบข้อมูล Start Period','status'=>$status);
        }
        if(!isset($data['project_end']) || $data['project_end']=="0000-00-00"){
            return array('message'=>'เกิดข้อผิดพลาด ตรวจสอบข้อมูล Start Period','status'=>$status);
        }
				try{
	        $sql = "UPDATE project SET project_start = :project_start, project_end = :project_end,
					updated_datetime = NOW(), updated_by = :email WHERE sid = :project_sid ";
	        $q = $this->db->prepare($sql);
	        $q->execute(array(
						':project_start'=>$data['project_start'],
						':project_end'=>$data['project_end'],
						':email'=>$data['email'],
						':project_sid'=>$data['project_sid']
					));
	        $status = 1;
	        return array('message'=>'แก้ไขข้อมูลแล้ว','status'=>$status);
				}catch(PDOException $e){
					return array('message'=>$e->getMessage(),'status'=>"0");
				}
    }
    public function editAddress($data){
        $status = 0;
        if(!isset($data['project_sid'])){
            return array('message'=>'เกิดข้อผิดพลาด ไม่ข้อมูลไม่เพียงพอ','status'=>$status);
        }

        if(!isset($data['address']) || $data['address'] ==""){
            return array('message'=>'เกิดข้อผิดพลาด ตรวจสอบข้อมูล Address','status'=>$status);
        }

        $sql = "UPDATE project SET end_user_address = :address, updated_datetime = NOW() , updated_by = :email
        WHERE sid = :project_sid ";
        $q = $this->db->prepare($sql);
        $q->execute(array(':address'=>$data['address'],':email'=>$data['email'],':project_sid'=>$data['project_sid']));
        $status = 1;
        return array('message'=>'แก้ไขข้อมูลแล้ว','status'=>$status);
    }
		public function approvePlan($project_sid, $email){
			try{
				$sql = "UPDATE project SET data_status = '300', updated_by = :email, updated_datetime = NOW() WHERE sid = :project_sid ";
				$q = $this->db->prepare($sql);
				$q->execute(array(':project_sid'=>$project_sid,':email'=>$email));

				$sql = "UPDATE project_plan SET status = '300', updated_by = :email, updated_datetime = NOW()
				WHERE project_sid = :project_sid AND status = '200' ";
				$q = $this->db->prepare($sql);
				$q->execute(array(':email'=>$email,':project_sid'=>$project_sid));

				return array('status'=>1,'message'=>'Approved', 'data'=>array());
			}catch(PDOException $e){
					return array('status'=>0,'message'=>$e->getMessage(), 'data'=>array());
			}
		}
		public function rejectPlan($project_sid, $email, $reason_reject_plan){
			try {
				$sql = "UPDATE project SET data_status = '100', updated_by = :email, updated_datetime = NOW(), reason_reject_plan = :reason_reject_plan
				WHERE sid = :project_sid ";
				$q = $this->db->prepare($sql);
				$q->execute(array(':project_sid'=>$project_sid,':email'=>$email,':reason_reject_plan'=>$reason_reject_plan));

				$sql = "UPDATE project_plan SET status = '100', updated_by = :email, updated_datetime = NOW()
				WHERE project_sid = :project_sid AND status = '200' ";
				$q = $this->db->prepare($sql);
				$q->execute(array(':email'=>$email,':project_sid'=>$project_sid));

				return array('status'=>1,'message'=>'Rejected', 'data'=>array());
			}catch(PDOException $e){
				return array('status'=>0,'message'=>$e->getMessage(), 'data'=>array());
			}
		}
}
?>
