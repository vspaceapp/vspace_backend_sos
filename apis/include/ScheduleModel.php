<?php
class ScheduleModel{
  private $db;

  private $objCons;
  function __construct($email="") {
      require_once dirname(__FILE__) . '/db_connect.php';
      // opening db connection
      $this->objCons = new DbConnect();
      $this->db = $this->objCons->connect();
  }

  public function index($email){
    $sql = "SELECT sid id, :group as 'group' ,appointment start_time, DATE_ADD(appointment, INTERVAL expect_duration HOUR) end_time, subject_service_report title";
    $sql .= "FROM tasks";
    $sql .= "WHERE (engineer = :engineer OR associate_engineer LIKE :associate_engineer)";
    $sql .= "AND appointment >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -30 day),'%Y-%m-%d 00:00:00')";
    $q = $this->db->prepare($sql);
    $q->execute(array(
      ':engineer'=>$email,
      ':associate_engineer'=>$email,
    ));
    $r = $q->fetchAll();
    return $r;
  }
  private function getTitleRoleName($role){
    $sql = "SELECT * FROM role WHERE sid = :sid ";
    $q = $this->db->prepare($sql);
    $q->execute(array(':sid'=>$role));
    return $r = $q->fetch();
  }

  public function setItemsSchedule($data){
    $groups = array();
    $items = array();
    $currentRoleSid = 0;
    foreach($data as $key => $value){
      $groups_sid = ($key+1);
      $items = $this->getItems($value, $groups_sid, $items);
      if($value['role_sid']!=$currentRoleSid){
        $titleRoleName = $this->getTitleRoleName($value['role_sid']);
        array_push($groups, array(
          'id'=>$groups_sid.'-'.$value['role_sid'],
          'title'=>$titleRoleName['name'],
          'email'=>"",
          'items'=>""
        ));
        $currentRoleSid = $value['role_sid'];
      }
      array_push($groups, array(
        'id'=>$groups_sid,
        'title'=>$value['title'],
        'email'=>$value['email'],
        'items'=>$items
      ));

    }

    return array('groups'=>$groups,'items'=>$items);
  }

  private function getItems($data, $groups_sid, $items){

    $sql = "SELECT T.sid,TE.sid tasks_engineer_sid,
    UNIX_TIMESTAMP(appointment) start_time,
    UNIX_TIMESTAMP(DATE_ADD(appointment, INTERVAL expect_duration HOUR)) end_time,
    CONCAT(no_task,' ',(SELECT end_user FROM ticket TICKET WHERE TICKET.sid = T.ticket_sid ORDER BY sid DESC LIMIT 0,1)) title
    FROM tasks T
    LEFT JOIN tasks_engineer TE ON T.sid = TE.tasks_sid
    WHERE TE.engineer = :email
    AND T.appointment >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -30 day),'%Y-%m-%d 00:00:00')
    GROUP BY T.no_task ";
    $q = $this->db->prepare($sql);
    $q->execute(array(':email'=>$data['email']));
    $r = $q->fetchAll();
    foreach($r as $key=>$value){
      $tmp = array();
      $tmp['id'] = intval($value['sid'])."-".($key+1)."-".intval($value['tasks_engineer_sid']);
      $tmp['group'] = intval($groups_sid);
      $tmp['start_time'] = intval($value['start_time'])*1000;
      $tmp['end_time'] = intval($value['end_time'])*1000;
      $tmp['title'] = $value['title'];
      $tmp['canMove'] = false;
      $tmp['canResize'] = false;
      array_push($items, $tmp);
    }

    $sql = "SELECT B.*,
    UNIX_TIMESTAMP(CONCAT(B.date_busy,' ',B.busy_start_time)) start_time,
    UNIX_TIMESTAMP(CONCAT(B.date_busy,' ',B.busy_end_time)) end_time,
    BM.name title
    FROM tbl_busy_employee B
    LEFT JOIN tbl_busy_mlookup BM ON B.type_busy_id = BM.id
    WHERE B.date_busy >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -30 day),'%Y-%m-%d 00:00:00') AND B.email = :email ";
    $q = $this->db->prepare($sql);
    $q->execute(array(':email'=>$data['email']));
    $r = $q->fetchAll();
    foreach($r as $key=>$value){
      $tmp = array();
      $tmp['id'] = intval($value['id'])."-".(($key+1)*10)."-1000";
      $tmp['group'] = intval($groups_sid);
      $tmp['start_time'] = intval($value['start_time'])*1000;
      $tmp['end_time'] = intval($value['end_time'])*1000;
      $tmp['title'] = $value['title'];
      $tmp['canMove'] = false;
      $tmp['canResize'] = false;
      array_push($items, $tmp);
    }
    return $items;
  }

  public function viewTimesheet($data){
    //
    $currentMonth = $this->getCurrentMonth();
    $result = $this->genTimesheet($data, $currentMonth);
    $result['timesheet'] = $this->timesheet($data['email']);
    return $result;
  }

  private function genTimesheet($data, $currentMonth){
    $summaryServiceReport = $this->summaryServiceReport($data, $currentMonth);
    $summaryReceiveCase = $this->summaryReceiveCase($data, $currentMonth);
    $summaryCloseCase = $this->summaryCloseCase($data, $currentMonth);

    $result = array(
      'email'=>$data['email'],
      'currentMonth'=>$currentMonth,
      'summaryServiceReport'=>$summaryServiceReport,
      'summaryReceiveCase'=>$summaryReceiveCase,
      'summaryCloseCase'=>$summaryCloseCase,
      'service_report_sid'=>$this->joinArraykeySid($summaryServiceReport),
      'receive_case_sid'=>$this->joinArraykeySid($summaryReceiveCase),
      'close_case_sid'=>$this->joinArraykeySid($summaryCloseCase)
      );

    $this->processTimesheet($result);
  }
  private function joinArraykeySid($data){
    $result = "";
    foreach ($data as $key => $value) {
      $result .= $value['sid'];
      if(count($data)!=$key+1){
        $result .= ",";
      }
    }
    return $result;
  }
  private function timesheet($email){
    $this->db->beginTransaction();
    $sql = "SELECT * FROM timesheet WHERE email = :email ORDER BY month DESC";
    $q = $this->db->prepare($sql);
    $q->execute(array(':email'=>$email));
    $this->db->commit();
    return $q->fetchAll();
  }

  // private function summaryTimesheet($data, $currentMonth){
  //   $service = $this->summaryServiceReport($data, $currentMonth);
  //   return $service;
  // }


  private function summaryServiceReport($data, $currentMonth){
    $this->db->beginTransaction();
    $sql = "SELECT DISTINCT(T.sid) FROM tasks T LEFT JOIN tasks_engineer TE ON T.sid = TE.tasks_sid
    WHERE TE.engineer = :email AND DATE_FORMAT(appointment,'%Y-%m') = :month AND (appointment_type = 0 OR appointment_type = 1) ";

    $q = $this->db->prepare($sql);
    $q->execute(array(':email'=>$data['email'],':month'=>$currentMonth));
    $r = $q->fetchAll();
    $this->db->commit();
    return $r;
  }


  private function summaryReceiveCase($data, $currentMonth){
    $this->db->beginTransaction();
    $sql = "SELECT DISTINCT(T.sid) FROM ticket_status T INNER JOIN ticket TK ON T.ticket_sid = TK.sid
    WHERE TK.owner = :email AND DATE_FORMAT(T.create_datetime,'%Y-%m') = :currentMonth AND T.case_status_sid = '2' ";
    $q = $this->db->prepare($sql);
    $q->execute(array(':email'=>$data['email'],':currentMonth'=>$currentMonth));
    $r = $q->fetchAll();
    $this->db->commit();
    return $r;
  }

  private function summaryCloseCase($data, $currentMonth){
    $this->db->beginTransaction();
    $sql = "SELECT DISTINCT(T.sid) FROM ticket_status T INNER JOIN ticket TK ON T.ticket_sid = TK.sid
    WHERE TK.owner = :email AND DATE_FORMAT(T.create_datetime,'%Y-%m') = :currentMonth
    AND T.case_status_sid = '5'
    ";
    $q = $this->db->prepare($sql);
    $q->execute(array(':email'=>$data['email'],':currentMonth'=>$currentMonth));
    $r = $q->fetchAll();
    $this->db->commit();
    return $r;
  }

  private function processTimesheet($result){
    $timesheetSid = $this->checkExistTimesheet($result);
    if($timesheetSid){
      $this->updateTimesheet($result, $timesheetSid);
    }else{
      $this->insertTimesheet($result);
    }
  }
  private function updateTimesheet($result, $timesheetSid){
    $this->db->beginTransaction();
    $sql = "UPDATE timesheet SET service_report = :service_report,
    receive_case = :receive_case, close_case = :close_case,
    service_report_sid = :service_report_sid,
    receive_case_sid = :receive_case_sid,
    close_case_sid = :close_case_sid,
    updated_datetime = NOW() WHERE sid = :sid ";
    $q = $this->db->prepare($sql);
    $q->execute(array(
      ':service_report'=>count($result['summaryServiceReport']),
      ':receive_case'=>count($result['summaryReceiveCase']),
      ':close_case'=>count($result['summaryCloseCase']),
      ':service_report_sid'=>$result['service_report_sid'],
      ':receive_case_sid'=>$result['receive_case_sid'],
      ':close_case_sid'=>$result['close_case_sid'],
      ':sid'=>$timesheetSid));
    $this->db->commit();
  }
  private function insertTimesheet($result){
    $this->db->beginTransaction();
    $sql = "INSERT INTO timesheet (month, email, service_report, updated_datetime, receive_case, close_case, service_report_sid, receive_case_sid, close_case_sid)
    VALUES (:month, :email, :service_report, NOW() , :receive_case , :close_case, :service_report_sid, :receive_case_sid, :close_case_sid) ";
    $q = $this->db->prepare($sql);
    $q->execute(array(
      ':month'=>$result['currentMonth'],
      ':email'=>$result['email'],
      ':service_report'=>count($result['summaryServiceReport']),
      ':receive_case'=>count($result['summaryReceiveCase']),
      ':close_case'=>count($result['summaryCloseCase']),
      ':service_report_sid'=>$result['service_report_sid'],
      ':receive_case_sid'=>$result['receive_case_sid'],
      ':close_case_sid'=>$result['close_case_sid']
      ));
    $this->db->commit();
  }
  private function checkExistTimesheet($result){
    $this->db->beginTransaction();
    $sql = "SELECT * FROM timesheet WHERE month = :month AND email = :email";
    $q = $this->db->prepare($sql);
    $q->execute(array(':month'=>$result['currentMonth'],':email'=>$result['email']));
    $this->db->commit();
    $r = $q->fetch();
    if(isset($r['sid']) && $r['sid']>0){
      return $r['sid'];
    }else{
      return false;
    }
  }

  private function getCurrentMonth(){
    $this->db->beginTransaction();
    $sql = "SELECT DATE_FORMAT(NOW(),'%Y-%m') month ";
    $q = $this->db->prepare($sql);
    $q->execute();
    $r = $q->fetch();
    $this->db->commit();
    return $r['month'];
    // return "2017-07";
  }

  public function createTimesheet($data){
    return $this->genTimesheet($data, $data['month']);
  }
}
?>
