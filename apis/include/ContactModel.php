<?php 
class ContactModel {
	private $db;
    private $prefix;
    private $contact_name,$email,$mobile,$phone,$company,$contact,$end_user,$type,$latitude,$longitude,$contact_no,$address,$serial_no;
    // private $type;

	function __construct() {
        require_once 'db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->db = $db->connect();
    }

    // prefix "", "mvg_", "tcs_"
    public function runContact($prefix){
        // "mvg_"
        $this->prefix = $prefix;

        $data = $this->selectRequesterTicket();
        $this->type = "requester";
        $this->processForTableContact($data);
            
        $data = $this->selectEndUserTicket();
        $this->type = "contact case";
        $this->processForTableContact($data);

        $data = $this->selectEndUserTask();
        $this->type = "end user service report";
        $this->processForTableContact($data);
    
    }

    private function processForTableContact($data){
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        foreach ($data as $key => $value) {

            $this->contact_name = $value['contact_name'];
            $this->email = trim($value['email']);
            $this->mobile = trim($value['mobile']);
            $this->phone = trim($value['phone']);
            $this->company = trim($value['company']);
            $this->contact = "";
            $this->end_user = trim($value['end_user']);
            
            $this->latitude = "";
            $this->longitude = "";
            $this->contact_no = trim($value['contract_no']);
            $this->address = "";
            $this->serial_no = "";
            $selectContact = $this->selectContact();
            if(isset($selectContact['sid']) && $selectContact['sid']>0){
                $this->updateContact();
            }else{
                $this->insertContact();
            }
        }
    }
    public function selectRequesterTicket(){
        $sql = "SELECT *,requester_full_name contact_name, requester_email email, requester_phone phone, requester_mobile mobile, requester_company_name company, end_user end_user, contract_no  
        FROM ".$this->prefix."ticket WHERE requester_email <> '' ORDER BY sid DESC LIMIT 0,5";
        $q = $this->db->prepare($sql);
        $q->execute( array());
        return $r = $q->fetchAll();
    }
    public function selectEndUserTicket(){
        $sql = "SELECT *,end_user_contact_name contact_name, end_user_email email, end_user_phone phone, end_user_mobile mobile, end_user_company_name company, end_user end_user, contract_no 
        FROM ".$this->prefix."ticket WHERE end_user_email <> '' ORDER BY sid DESC LIMIT 0,5";
        $q = $this->db->prepare($sql);
        $q->execute( array());
        return $r = $q->fetchAll();
    }
    public function selectEndUserTask(){
        $sql = "SELECT *,TT.end_user_contact_name_service_report contact_name, TT.end_user_email_service_report email, TT.end_user_phone_service_report phone, TT.end_user_mobile_service_report mobile, TT.end_user_company_name_service_report company, T.end_user end_user, T.contract_no  contract_no 
        FROM ".$this->prefix."tasks TT LEFT JOIN ".$this->prefix."ticket T ON T.sid = TT.ticket_sid WHERE TT.end_user_email_service_report <> '' ORDER BY TT.sid DESC LIMIT 0,5";
        $q = $this->db->prepare($sql);
        $q->execute( array());
        return $r = $q->fetchAll();
    }
    public function selectContact(){
    	$sql = " SELECT sid FROM contact WHERE email = :email AND contact_no = :contact_no" ;
    	$q = $this->db->prepare($sql);
    	$q->execute( array(':email' => $this->email,':contact_no'=>$this->contact_no));
    	return $r = $q->fetch();
    }

    public function insertContact(){
        $sql = " INSERT INTO contact (prefix, contact_name, email, mobile, phone, company, end_user, type, created_datetime, updated_datetime, latitude,    longitude,contact_no,address,serial_no) 
                 VALUES (:prefix, :contact_name, :email, :mobile, :phone, :company, :end_user, :type, NOW(), NOW(), :latitude, :longitude, :contact_no, :address, :serial_no ) " ;
        $q = $this->db->prepare($sql);
        return $q->execute( array(
                ':prefix' => $this->prefix,
                ':contact_name' => $this->contact_name,
                ':email' => $this->email,
                ':mobile' => $this->mobile, 
                ':phone' => $this->phone, 
                ':company' => $this->company, 
                ':end_user' => $this->end_user, 
                ':type' => $this->type,
                ':latitude' =>  $this->latitude,
                ':longitude' => $this->longitude ,
                ':contact_no' => $this->contact_no,
                ':address' => $this->address,
                ':serial_no' => $this->serial_no
            ));         
    }

    public function updateContact(){
        $sql = " UPDATE contact SET prefix = :prefix, contact_name = :contact_name, email = :email, mobile = :mobile, phone = :phone, company = :company, end_user = :end_user, type = :type, updated_datetime = NOW(), latitude = :latitude, longitude = :longitude, contact_no = :contact_no, address = :address, serial_no = :serial_no 
            WHERE email = :email AND contact_no = :contact_no";
        $q = $this->db->prepare($sql);
        return $q->execute( array(
                ':prefix' => $this->prefix,
                ':contact_name' => $this->contact_name,
                ':email' => $this->email,
                ':mobile' => $this->mobile, 
                ':phone' => $this->phone, 
                ':company' => $this->company,
                ':end_user' => $this->end_user, 
                ':type' => $this->type,
                ':latitude' =>  $this->latitude,
                ':longitude' => $this->longitude ,
                ':contact_no' => $this->contact_no,
                ':address' => $this->address,
                ':serial_no' => $this->serial_no
        ));         
    }


}

?>