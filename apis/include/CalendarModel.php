<?php
class CalendarModel{
  private $db;

  private $objCons;
  function __construct($email="") {
      require_once dirname(__FILE__) . '/db_connect.php';
      // opening db connection
      $this->objCons = new DbConnect();
      $this->db = $this->objCons->connect();
  }

  public function index($email,$start, $to){

      $sql = "SELECT T.*, DATE_FORMAT(T.appointment, '%Y-%m-%d %H:%i') appointment ";
      $sql .= ", DATE_FORMAT(T.appointment, '%Y-%m-%d') appointment_date ";
      $sql .= ",DATE_FORMAT(T.started_datetime, '%Y-%m-%d %H:%i') started_datetime ";
      $sql .= ",DATE_FORMAT(T.closed_datetime, '%Y-%m-%d %H:%i') closed_datetime ";
      $sql .= ",DATE_FORMAT(T.started_datetime, '%Y-%m-%d') started_date ";
      $sql .= ",DATE_FORMAT(T.closed_datetime, '%Y-%m-%d') closed_date ";
      $sql .= "FROM tasks T ";
      $sql .= "WHERE ((T.engineer = :engineer) ";
      $sql .= "OR (T.associate_engineer LIKE :associate_engineer)) ";
      $sql .= "AND ((T.appointment > :start AND T.appointment < :to ) ";
      $sql .= "OR (T.started_datetime > :start AND T.started_datetime < :to ) ";
      $sql .= "OR (T.closed_datetime > :start AND T.closed_datetime < :to)) ";
      $sql .= "GROUP BY T.sid ";

      $q = $this->db->prepare($sql);
      $q->execute(array(
        ':engineer'=>$email,
        ':associate_engineer'=>"%".$email."%",
        ':start'=>$start,
        ':to'=>$to
      ));
      $r = $q->fetchAll();
      return $r;
  }
}
?>
