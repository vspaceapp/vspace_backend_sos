<?php
/**
* 
*/
class ReportEngineerModel{
	
	 function __construct($email="") {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $this->dbObj = $db = new DbConnect();
        $this->db = $db->connect();
        $this->email = $email;
        if($this->email!=""){
            $this->permission_role = $this->dbObj->permissionRoleByEmail($this->email); 
        }
    }

    public function addReport($email_engineer, $problem, $solution, $other, $by){
       
       	$sql = "INSERT INTO report_engineer( email, problem, solution, other, create_by, create_datetime) 
       			VALUES ( :email_engineer, :problem, :solution, :other, :create_by, NOW())";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                 ':email_engineer' => $email_engineer,
                 ':problem' => $problem, 
                 ':solution' => $solution,
                 ':other' => $other,
                 ':create_by' => $by
            ));
        
    }

     public function selectReport($email_engineer){
       
        $sql = "SELECT * FROM report_engineer  ";
        $q = $this->db->prepare($sql);
        $q->execute(array(
                 ':email_engineer' => $email_engineer
            ));
        $r = $q->fetchAll();
        return $r;
        
    }

}


?>