<?php
/**
 * Created by PhpStorm.
 * User: nanth
 * Date: 2/19/2018
 * Time: 04:34 PM
 */

/**
 * WEB SERVICE REMEDY CONFIG UAT & Production
 *  !! don't delete comment code !!
 *
 */

require_once __dir__."/config.php";

    if(ENV == 'production'){
        // production env.

        $endPointInfo =  array(
            'support_group_id'=>'SGP000000002409',
            'support_company'=>'KPro',
            'support_organization'=>'Endpoint Service Management',
            'support_group_name'=> 'EndPoint Service Center'
        );

        $incidentInfo = array(
            'support_group_id'=>'SGP000000000334',
            'support_company'=>'KBANK',
            'support_organization'=>'Incident Management Team',
            'support_group_name'=> 'Incident Coordinator'
        );

        define('USE_LOCAL_XML', true);
        define('REMEDY_WEBSERVICE_URL_UPDATE_TICKET', "https://110.170.223.182/arsys/WSDL/public/PPRDWITSMAPP01/KBANK_UpdateIncident_WS");
        define('REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG', "https://110.170.223.182/arsys/WSDL/public/PPRDWITSMAPP01/KBANK_CreateIncident_WorkLog_WS");
        define('REMEDY_FILENAME_XML_UPDATE_TICKET', "PD_KBANK_UpdateIncident_WS.wsdl.xml");
        define('REMEDY_NAMESPACE_UPDATE_TICKET', "urn:KBANK_UpdateIncident_WS");
        define('REMEDY_FILENAME_XML_UPDATE_WORKLOG', "PD_KBANK_CreateIncident_WorkLog_WS.wsdl.xml");
        define('REMEDY_NAMESPACE_UPDATE_WORKLOG', "urn:KBANK_CreateIncident_WorkLog_WS");

        define('REMEDY_ASSIGNEE_PERSON_ID', 'PPL000000044664');
        define('REMEDY_USER', "sos.vspace");
        define('REMEDY_PASS', "password");
        define('REMEDY_ASSIGNEE_NAME', "SOS VSpace");
        define('REMEDY_ASSIGNEE_LOGIN_ID', 'sos.vspace');

        define('REMEDY_ENDPOINT_INFO', serialize($endPointInfo));
        define('REMEDY_INCIDENT_CO_INFO', serialize($incidentInfo));

        /**
         * URL SOAP SERVER and Host
         */
        define('HOST_NAME', "https://sos.vspace.in.th");
        define('SOAP_WS_URL_UPDATE_WORKLOG', 'https://sos.vspace.in.th/soap/sos_worklog/index.php');
        define('SOAP_WS_URL_UPDATE_TICKET', 'https://sos.vspace.in.th/soap/sos_case/index.php');

    }else if(ENV == 'UAT'){
        // UAT env.

        $endPointInfo = array(
            'support_group_id'=>'SGP000000000317',
            'support_company'=>'KBANK',
            'support_organization'=>'IF Service Center',
            'support_group_name'=> 'EndPoint Service Center'
        );

        $incidentInfo = array(
            'support_group_id'=>'SGP000000000334',
            'support_company'=>'KBANK',
            'support_organization'=>'Incident Management Team',
            'support_group_name'=> 'Incident Coordinator'
        );

        define('USE_LOCAL_XML', true);
        define('REMEDY_WEBSERVICE_URL_UPDATE_TICKET', "https://203.146.225.190/arsys/WSDL/public/cwn-as2424ssl-test_app_1/KBANK__Update_Incident__WS001");
        define('REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG', "https://203.146.225.190/arsys/WSDL/public/cwn-as2424ssl-test_app_1/KBANK_CreateIncident_WorkLog_WS001");
        define('REMEDY_FILENAME_XML_UPDATE_TICKET', "UAT_KBANK__Update_Incident__WS001.wsdl.xml");
        define('REMEDY_NAMESPACE_UPDATE_TICKET', "urn:KBANK__Update_Incident__WS001");
        define('REMEDY_FILENAME_XML_UPDATE_WORKLOG', "UAT_KBANK_CreateIncident_WorkLog_WS001.wsdl.xml");
        define('REMEDY_NAMESPACE_UPDATE_WORKLOG', "urn:KBANK_CreateIncident_WorkLog_WS001");

        define('REMEDY_ASSIGNEE_NAME', "SOS VSpace");
        define('REMEDY_ASSIGNEE_LOGIN_ID', 'sos.vspace');
        define('REMEDY_ASSIGNEE_PERSON_ID', 'PPL000000052639');
        define('REMEDY_USER', "sos.vspace");
        define('REMEDY_PASS', "vpassword");
        define('REMEDY_ENDPOINT_INFO', serialize($endPointInfo));
        define('REMEDY_INCIDENT_CO_INFO', serialize($incidentInfo));


        /**
         * URL SOAP SERVER and Host
         */
        define('HOST_NAME', "https://sosuat.vspace.in.th");
        define('SOAP_WS_URL_UPDATE_WORKLOG', 'https://sosuat.vspace.in.th/soap/sos_worklog/index.php');
        define('SOAP_WS_URL_UPDATE_TICKET', 'https://sosuat.vspace.in.th/soap/sos_case/index.php');

    }else if(ENV == 'DEV'){
        $endPointInfo = array(
            'support_group_id'=>'SGP000000000317',
            'support_company'=>'KBANK',
            'support_organization'=>'IF Service Center',
            'support_group_name'=> 'EndPoint Service Center'
        );

        $incidentInfo = array(
            'support_group_id'=>'SGP000000000334',
            'support_company'=>'KBANK',
            'support_organization'=>'Incident Management Team',
            'support_group_name'=> 'Incident Coordinator'
        );

        define('USE_LOCAL_XML', true);
        define('REMEDY_WEBSERVICE_URL_UPDATE_TICKET', "https://203.146.225.190/arsys/WSDL/public/cwn-as2424ssl-test_app_1/KBANK__Update_Incident__WS001");
        define('REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG', "https://203.146.225.190/arsys/WSDL/public/cwn-as2424ssl-test_app_1/KBANK_CreateIncident_WorkLog_WS001");
        define('REMEDY_FILENAME_XML_UPDATE_TICKET', "UAT_KBANK__Update_Incident__WS001.wsdl.xml");
        define('REMEDY_NAMESPACE_UPDATE_TICKET', "urn:KBANK__Update_Incident__WS001");
        define('REMEDY_FILENAME_XML_UPDATE_WORKLOG', "UAT_KBANK_CreateIncident_WorkLog_WS001.wsdl.xml");
        define('REMEDY_NAMESPACE_UPDATE_WORKLOG', "urn:KBANK_CreateIncident_WorkLog_WS001");

        define('REMEDY_ASSIGNEE_NAME', "SOS VSpace");
        define('REMEDY_ASSIGNEE_LOGIN_ID', 'sos.vspace');
        define('REMEDY_ASSIGNEE_PERSON_ID', 'PPL000000052639');
        define('REMEDY_USER', "sos.vspace");
        define('REMEDY_PASS', "vpassword");
        define('REMEDY_ENDPOINT_INFO', serialize($endPointInfo));
        define('REMEDY_INCIDENT_CO_INFO', serialize($incidentInfo));


        /**
         * URL SOAP SERVER and Host
         */
         define('HOST_NAME', "https://sosdev.vspace.in.th");
         define('SOAP_WS_URL_UPDATE_WORKLOG', 'https://sosdev.vspace.in.th/soap/sos_worklog/index.php');
         define('SOAP_WS_URL_UPDATE_TICKET', 'https://sosdev.vspace.in.th/soap/sos_case/index.php');
    }else if(ENV == 'LOCAL'){
      $endPointInfo = array(
          'support_group_id'=>'SGP000000000317',
          'support_company'=>'KBANK',
          'support_organization'=>'IF Service Center',
          'support_group_name'=> 'EndPoint Service Center'
      );

      $incidentInfo = array(
          'support_group_id'=>'SGP000000000334',
          'support_company'=>'KBANK',
          'support_organization'=>'Incident Management Team',
          'support_group_name'=> 'Incident Coordinator'
      );

      define('USE_LOCAL_XML', true);
      define('REMEDY_WEBSERVICE_URL_UPDATE_TICKET', "https://203.146.225.190/arsys/WSDL/public/cwn-as2424ssl-test_app_1/KBANK__Update_Incident__WS001");
      define('REMEDY_WEBSERVICE_URL_UPDATE_WORKLOG', "https://203.146.225.190/arsys/WSDL/public/cwn-as2424ssl-test_app_1/KBANK_CreateIncident_WorkLog_WS001");
      define('REMEDY_FILENAME_XML_UPDATE_TICKET', "UAT_KBANK__Update_Incident__WS001.wsdl.xml");
      define('REMEDY_NAMESPACE_UPDATE_TICKET', "urn:KBANK__Update_Incident__WS001");
      define('REMEDY_FILENAME_XML_UPDATE_WORKLOG', "UAT_KBANK_CreateIncident_WorkLog_WS001.wsdl.xml");
      define('REMEDY_NAMESPACE_UPDATE_WORKLOG', "urn:KBANK_CreateIncident_WorkLog_WS001");

      define('REMEDY_ASSIGNEE_NAME', "SOS VSpace");
      define('REMEDY_ASSIGNEE_LOGIN_ID', 'sos.vspace');
      define('REMEDY_ASSIGNEE_PERSON_ID', 'PPL000000052639');
      define('REMEDY_USER', "sos.vspace");
      define('REMEDY_PASS', "vpassword");
      define('REMEDY_ENDPOINT_INFO', serialize($endPointInfo));
      define('REMEDY_INCIDENT_CO_INFO', serialize($incidentInfo));


      /**
       * URL SOAP SERVER and Host
       */
      define('HOST_NAME', "http://localhost/vspace_backend_sos_dev");
      define('SOAP_WS_URL_UPDATE_WORKLOG', 'http://localhost/vspace_backend_sos_dev/soap/sos_worklog/index.php');
      define('SOAP_WS_URL_UPDATE_TICKET', 'http://localhost/vspace_backend_sos_dev/soap/sos_case/index.php');
    }





    define('DEFAULT_ROLE_SID_ASSIGNED_TICKET', 321);








/**
 * Error Message Constant
 * */
    define('ERROR_AUTH_1_MSG', "You are not authorized this Ticket");
    define('ERROR_FILE_NOT_FOUND', "File not found");
    define('ERROR_REQUIRES_REPORT_CLOSING', "All service reports must be closed.");

?>
