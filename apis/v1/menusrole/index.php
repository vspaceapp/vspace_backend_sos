<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/FCM.php';
require_once '../../include/SendMail.php';
// require_once '../../include/db_handler.php';
require_once '../../include/UserModel.php';
require_once '../../include/RoleModel.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
$app->post('/template', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    
    $template = template();

    $response = array();
    $response["error"] = false;
    $response["template"] = $template;
    echoRespnse(200, $response);
});
$app->post('/templateRoleSetting', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    
    if($email=="autsakorn.t@firstlogic.co.th" || $email=="teerawut.p@firstlogic.co.th" || $email=="sarinaphat.t@firstlogic.co.th"){
        $template = templateRoleSetting();
    }else{
        $template = "<div>Permission Denied</div>";
    }

    $response = array();
    $response["error"] = false;
    $response["template"] = $template;
    echoRespnse(200, $response);
});
$app->post('/viewSettingRole', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $role_sid = $app->request->post('role_sid');

    $roleModel = new RoleModel();

    $data = $roleModel->viewSettingRole($role_sid, $email, $token);

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/managePermission', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $role_sid = $app->request->post('role_sid');
    $column = "";
    if($app->request->post('column')){
        $column = $app->request->post('column');
    }

    $type = $app->request->post('type');
    $permission_sid = $app->request->post('permission_sid');
    $value = $app->request->post('value');

    $roleModel = new RoleModel();

    $data = $roleModel->managePermission($type, $value, $role_sid, $permission_sid, $email, $token,$column);

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

function template(){
    // <li class="table-view-cell table-view-cell">Item 2</li>
    //                 <li class="table-view-divider">Divider</li>
    //                 <li class="table-view-cell">Item 3</li>

    // <button class="btn btn-link btn-nav pull-left"><span class="icon icon-left-nav"></span> Left</button>
    // <footer class="row"><div class="col-md-12" style="margin-left:10px;">Icons go here</div></footer>
    // <footer class="row"><div class="col-md-12">Some status text here</div></footer>
    $html = '<style> .split-view header {border-bottom: 1px solid #ccc;} </style><article class="split-view row">
          <section class="left col col-md-3">
            <header class="row">
              <h1 class="title">Role</h1>
            </header>
            <section class="body row scroll-y" style="overflow-y: scroll;">
                <div class="col-md-12" style="margin-left:10px;">
                  <ul class="table-view">
                    {{#each role}}
                    <li class="table-view-cell"><a href="javascript:void(0);" data-role-sid="{{sid}}" class="navigate-right"><div><p>{{name}} ({{sid}})</p></div></a></li>
                    {{/each}}
                  </ul>
                </div>
            </section>
            
          </section>
          <section class="right col col-md-9 content-show">
          </section>
        </article>
       ';

    return $html;
}

function templateRoleSetting(){
    $html = '<header class="row">
                <div class="col-md-12">
                  <!--<button class="btn btn-link btn-nav pull-right"><i class="fa fa-lg fa-bars"></i></button>-->
                  <h1 class="title">{{dataRole.name}}</h1>
                </div>
            </header>
            <section class="body row scroll-y" style="overflow-y: scroll;">
                <div class="col-xs-6">
                    <div class="scroll-y" style="height:200px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h6>User (Table user) <span class="pull-right">Status (1 = active, 0 = deactivate)</span></h6>
                        <ul>
                            {{#each userInRole}}
                                <li><img src="{{picture}}" style="width:24px;" />{{email}}, {{org}} <span class="pull-right">{{active}}</span></li>
                            {{/each}}
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="scroll-y" style="height:200px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h6>User (Table user_role) </h6>
                        <ul>
                        {{#each userRoleInRole}}
                            <li><img src="{{picture}}" style="width:24px;" /> {{email}}, {{org}}</li>
                        {{/each}}
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="scroll-y" style="height:320px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h5>Role Setting</h5>
                        <ul class="table-view">
                            <li><div><p>Name <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="name" type="text" value="{{dataRole.name}}" /></span></p></div></li>
                            <li><div><p>Name view <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="role_name_view" type="text" value="{{dataRole.role_name_view}}" /></span></p></div></li>
                            <li><div><p>Prefix <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="prefix_table" type="text" value="{{dataRole.prefix_table}}" /></span></p></div></li>
                            
                            <li><div><p>Comp Refer EBiz <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="comp_refer_ebiz" type="text" value="{{dataRole.comp_refer_ebiz}}" /></span></p></div></li>

                            <li><div><p>Can Assign/View Case Group <span style="width:100%;" class="pull-right"><input style="width:100%;" data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="can_assign" type="text" value="{{dataRole.can_assign}}" /></span></p></div></li>
                            <li><div><p>Permission Project <span style="width:100%;" class="pull-right"><input style="width:100%;" data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="permission_project" type="text" value="{{dataRole.permission_project}}" /></span></p></div></li>
                            <li><div><p>Permission Ticket <span style="width:100%;" class="pull-right"><input style="width:100%;" data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="permission_ticket" type="text" value="{{dataRole.permission_ticket}}" /></span></p></div></li>
                            <li><div><p>Supervisor role sid <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="supervisor_role_sid" type="text" value="{{dataRole.supervisor_role_sid}}" /></span></p></div></li>

                            <li><div><p>Root Supervisor role sid <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="root_supervisor_role_sid" type="text" value="{{dataRole.root_supervisor_role_sid}}" /></span></p></div></li>

                            <li><div><p>Is Supervisor role sid <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="is_a_supervisor_role_sid" type="text" value="{{dataRole.is_a_supervisor_role_sid}}" /></span></p></div></li>


                            <li><div><p>Escaration To <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="escaration_to" type="text" value="{{dataRole.escaration_to}}" /></span></p></div></li>

                        </ul>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="scroll-y" style="height:320px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h5>Role Setting</h5>
                        <ul class="table-view">
                            <li><div><p>Dev Mode <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="dev_mode" type="number" value="{{dataRole.dev_mode}}" /></span></p></div></li>
                            <li><div><p>Create Case <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="case_create_able" type="text" value="{{dataRole.case_create_able}}" /></span></p></div></li>
                            <li><div><p>Create Project <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="project_create_able" type="text" value="{{dataRole.project_create_able}}" /></span></p></div></li>
                            <li><div><p>Create Project C&G<span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="create_project_c_and_g" type="text" value="{{dataRole.create_project_c_and_g}}" /></span></p></div></li>
                            
                            <li><div><p>Create Service Report (0=can not, 1= when is owner case, 2 = all case) <span class="pull-right"><input data-type="role" data-permission-sid="{{dataRole.sid}}" data-column="can_create_service" type="text" value="{{dataRole.can_create_service}}" /></span></p></div></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="scroll-y" style="height:600px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h5>Menu</h5>
                        <ul class="table-view">
                        {{#each menu}}
                            <li class="table-view-cell"><div><p><input type="checkbox" {{#if value_set}} checked {{/if}}" data-type="menu" data-permission-sid="{{sid}}" class="toggle" /> {{name}} ({{sid}})
                            </p></div>
                            <!--<div class="toggle {{#if value_set}} active {{/if}}" data-type="menu" data-permission-sid="{{sid}}"><div class="toggle-handle">
                                </div></div>-->
                            </li>
                        {{/each}}
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div  class="body scroll-y" style="height:150px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h5>Dashboard</h5>
                        <ul class="table-view">
                        {{#each dashboard}}
                            <li class="table-view-cell"><div><p><input type="checkbox" {{#if value_set}} checked {{/if}}" data-type="dashboard" data-permission-sid="{{sid}}" class="toggle" /> {{name}}</p></div>
                                <!--<div class="toggle {{#if value_set}} active {{/if}}" data-type="dashboard" data-permission-sid="{{sid}}"><div class="toggle-handle">
                                </div></div>-->
                            </li>
                        {{/each}}
                        </ul>
                    </div>
                    <div  class="scroll-y" style="height:200px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h5>Menu Mobile</h5>
                        <ul class="table-view">
                        {{#each menu_mobile}}
                            <li class="table-view-cell"><div><p><input type="checkbox" {{#if value_set}} checked {{/if}}" data-type="menu_mobile" data-permission-sid="{{sid}}" class="toggle" /> {{name}}</p></div>
                                
                                <!--<div class="toggle {{#if value_set}} active {{/if}}" data-type="menu_mobile" data-permission-sid="{{sid}}"><div class="toggle-handle">
                                </div></div>-->
                            </li>
                        {{/each}}
                        </ul>
                    </div>
                    <div  class="scroll-y" style="height:300px;overflow-y:scroll;padding:10px; border:1px solid #c1c1c1;">
                        <h5>Segment</h5>
                        <ul class="table-view">
                        {{#each segment}}
                            <li class="table-view-cell"><div><p><input type="checkbox" {{#if value_set}} checked {{/if}}" data-type="segement" data-permission-sid="{{sid}}" class="toggle" /> {{name}}</p></div>
                                
                                <!--<div class="toggle {{#if value_set}} active {{/if}}" data-type="segment" data-permission-sid="{{sid}}"><div class="toggle-handle">
                                </div></div>-->
                            </li>
                        {{/each}}
                        </ul>
                    </div>
                </div>
            </section>';
    return $html;
}
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });



/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>