<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ServiceReportModifyModel.php';
require_once '../../include/GenNoCaseSR.php';
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/cancelSr', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');


    $obj = new ServiceReportModifyModel($email);
    $permission_role = $obj->permission_role;
    $data = $obj->cancelSR($tasks_sid, $email);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/changeSubject', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $subject_service_report = $app->request->post('subject_service_report');


    $obj = new ServiceReportModifyModel($email);
    $permission_role = $obj->permission_role;
    $data = $obj->changeSubject($subject_service_report, $tasks_sid, $email);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/changeServiceType',function() use ($app){
	verifyRequiredParams(array('email'));
	$new_service_type = $app->request->post('new_service_type'); 
	$tasks_sid = $app->request->post('tasks_sid');
	$email = $app->request->post('email');

	$obj = new ServiceReportModifyModel($email);
	$data = $obj->changeServiceType($new_service_type,$tasks_sid,$email);

	$response = array();
	$response['error'] = false;
	$response['message'] = false;
	$response['data'] = $data;
	echoRespnse(200, $response);
});

$app->post('/getStatusTasks',function() use ($app){
    verifyRequiredParams(array('email'));
    $tasks_sid =$app->request->post('tasks_sid');
    $email = $app->request->post('email');

    $obj = new ServiceReportModifyModel($email);
    $data = $obj->getStatusTasks($tasks_sid,$email);

    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);

});

$app->post('/inputAction', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email'); 
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $input_action = $app->request->post('input_action'); 
    $input_action_sid = $app->request->post('input_action_sid');

    $obj = new ServiceReportModifyModel($email);
    $data = $obj->inputAction($email,$token,$tasks_sid,$input_action,$input_action_sid);

    $response = array();
    $response['error'] = false;
    $response['message'] = "ok";
    $response['data'] = array('email'=>$email, 'tasks_sid'=>$tasks_sid, 'input_action'=>$input_action, 'input_action_sid'=>$input_action_sid);
    echoRespnse(200, $response);


});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    $n = 0;
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $field = str_replace("_", " ", $field);
            $error_fields .= '- ' .ucfirst($field) . '<br/>';
            $n++;
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s)<br/>' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>