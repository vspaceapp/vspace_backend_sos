<?php
require_once "../../include/OraclePartModel.php";

$index = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new OraclePartModel();

        $data = $obj->index($request['email']);
        $response = array();
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$oraclePart = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new OraclePartModel();

        $data = $obj->oraclePart(0,$request['email'],$request,'ASC');
        $response = array();
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$addOracleBackline = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new OraclePartModel();
        $obj->addOracleBackline($request);

        $data = $obj->oraclePart(0,$request['email'],$request,'ASC');

        $response = array();
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$editOracleBackline = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new OraclePartModel();

        $data = $obj->editOracleBacklineQuery($request);
        $data = $obj->editOracleTaskQuery($request);
        $data = $obj->editOraclePartQuery($request);

        $data = $obj->oraclePart(0,$request['email'],$request,'ASC');

        $response = array();
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$deleteOracleBackline = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new OraclePartModel();

        $data = $obj->deleteOracleBacklineQuery($request);
        $data = $obj->deleteOracleTaskQuery($request);
        $data = $obj->deleteOraclePartQuery($request);

        $data = $obj->oraclePart(0,$request['email'],$request,'ASC');
        $response = array();
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};