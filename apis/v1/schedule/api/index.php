<?php
require_once "../../include/ScheduleModel.php";
require_once "../../include/TokenModel.php";
$index = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new ScheduleModel();
        $tokenObj = new TokenModel();
        $permission = 0;
        $info = $tokenObj->info($request['token'], $request['email']);

        $response = array();
        if(isset($info['role_sid'])){
            $permission = $tokenObj->permissionTask($info['role_sid']);

            $group = $tokenObj->userInRole($permission);
            $data = $obj->setItemsSchedule($group);

            $response['data'] = $data;
        }

        // $data = $obj->index($request['email']);

        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$timesheet = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);
        $response['request'] = $request;
        $response['error'] = false;
        $obj = new ScheduleModel();
        $data = $obj->viewTimesheet($request);

        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$createTimesheet = function () use ($app){
    try{
        parse_str($app->request()->getBody(),$request);
        $response['request'] = $request;
        $response['error'] = false;
        $obj = new ScheduleModel();
        $data = $obj->createTimesheet($request);

        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$timelineDetail = function () use ($app){
    try{
        parse_str($app->request()->getBody(),$request);
        $response['request'] = $request;
        $response['error'] = false;
        // $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};