<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/FCM.php';
require_once '../../include/SendMail.php';
require_once '../../include/db_handler.php';
require_once '../../include/ProjectModel.php';
require_once '../../include/CaseModel.php';
require_once '../../include/UserModel.php';
require_once '../../include/PlanningModel.php';
require_once '../../include/LocationModel.php';
require_once '../../include/MeetingModel.php';
require_once '../../include/ServiceType.php';
require_once '../../include/TokenModel.php';
require_once '../../include/SlaModel.php';
require_once '../../include/BugReportModel.php';
require_once '../../view/MoreSystemView.php';
require_once '../../view/MoreSystemViewKaew.php';
require_once '../../view/SparePartView.php';
require_once '../../view/ServiceReportCreateView.php';

require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/loadEmployee',function() use ($app){
    require_once '../../view/MeetingServiceView.php';
    $email = $app->request->post('by');
    $token = $app->request->post('token');
    $windowHeight = $app->request->post('windowHeight');
   

  
    $obj = new MeetingModel();
    $data = $obj->selectEmployee($email);
    
    $response = array();
    $response["error"] = false;
    // $response["template"] = $template;
    $response["data"] = $data;
    echoRespnse(200, $response);
});


$app->post('/loadTypeMeeting',function() use ($app){
    require_once '../../view/MeetingServiceView.php';
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $windowHeight = $app->request->post('windowHeight');
   
    $obj = new MeetingModel();
    $data = $obj->loadTypeMeeting();
    
    $response = array();
    // $response["error"] = false;
    // // $response["template"] = $template;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

 
$app->post('/addTypeMeeting',function() use ($app){
    require_once '../../view/MeetingServiceView.php';
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $type_meeting = $app->request->post('type_meeting');
    $start_time = $app->request->post('start_time');
    $end_time = $app->request->post('end_time');
    $invite_meeting = $app->request->post('invite_meeting');
    
    $windowHeight = $app->request->post('windowHeight');
   
    $obj = new MeetingModel();
    $data = $obj->insertTypeMeeting($email, $type_meeting, $start_time, $end_time, $invite_meeting);
    
    $response = array();
    $response["error"] = false;
    // // $response["template"] = $template;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

 
$app->post('/addMeeting',function() use ($app){
    require_once '../../view/MeetingServiceView.php';
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $type_meeting = $app->request->post('type_meeting');
    $start_time = $app->request->post('start_time');
    $end_time = $app->request->post('end_time');
    $invite_meeting = $app->request->post('invite_meeting');
    
    $windowHeight = $app->request->post('windowHeight');
   
    // $obj = new MeetingModel();
    // $data = $obj->insertTypeMeeting($email, $type_meeting, $start_time, $end_time, $invite_meeting);
    
    $response = array();
    $response["error"] = false;
    // // $response["template"] = $template;
    $response["data"] = $data;
    echoRespnse(200, $response);
});


$app->post('/templateRequestTaxiDetail',function() use ($app){
    require_once '../../view/RequestTaxiView.php';
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = "";
    
    if($app->request->post('tasks_sid')){
        $tasks_sid = $app->request->post('tasks_sid');
    }
   
    $obj = new CaseModel();
    $data = $obj->taskDetail($email, $tasks_sid);
    $requestTaxiView = new RequestTaxiView();
    $template = $requestTaxiView->viewDetail($email, $data);
    $response = array();
    $response["error"] = false;
    $response["template"] = $template;
    $response["data"] = $data;
    echoRespnse(200, $response);
});



$app->post('/loadTemplateProjectManagementApp', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $order_by = $app->request->post('order_by');
    $search = $app->request->post('search');
    $windowHeight = $app->request->post('windowHeight');

    require_once '../../view/ProjectManagementView.php';
    
    $obj = new ProjectModel($email);
    $permission_role = $obj->permission_role;
    $data = $obj->listProject($email, $token, $order_by, $search);

    $canAssignTo = $obj->listUserCanAddProject($email);
    $response = array();
    $response["error"] = false;

    $viewObj = new ProjectManagementView($email);
    $response["template"] = $viewObj->templateProjectManagementApp($email, $data, $order_by, $search, $permission_role,$canAssignTo,$windowHeight);
    $response['data'] = $data;
    $response['permission_role'] = $permission_role;
    $response['canAssignTo'] = $canAssignTo;
    echoRespnse(200, $response);
});
$app->post('/templateProjectDetail', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $project_sid = $app->request->post('project_sid');
    $contract_no = $app->request->post('contract');
    // $search = $app->request->post('search');
    $tabName = $app->request->post('tabName');

    require_once '../../view/ProjectManagementView.php';
    
    $obj = new ProjectModel();
    $project_detail = $obj->projectDetail($project_sid,$email, $token);
    $data = $obj->listCaseOfContract($contract_no, $project_detail['project_detail']['project_owner_sid']);
    $engineer = array();
    $timesTotal = 0;
    foreach ($data as $key => $value) {
        foreach ($value['task'] as $k => $v) {
            foreach ($v['timestamp'] as $kk => $vv) {
                $timesTotal++;
                $cal['use_hours'] = floor(intval($vv['use_minutes']) / 60);
                $cal['use_minutes_mod'] = (intval($vv['use_minutes']) % 60);
                $cal['use_minutes_mod'] = (intval($cal['use_minutes_mod'] + "") == 1) ? "0" + ($cal['use_minutes_mod']) : $cal['use_minutes_mod'];
                    
                if(!in_array($vv['engineer'], $engineer)){
                    // $temp = array();

                    $temp = array(
                        'engineer'=>$vv['engineer'],
                        'use_minutes'=>$vv['use_minutes'],
                        'thainame'=>$vv['thainame'],
                        'engname'=>$vv['engname'],
                        // 'mobile'=>$vv['mobile'],
                        'pic'=>$vv['pic'], 
                        'times'=>1,
                        'taxi'=>($vv['taxi_fare_1']+$vv['taxi_fare_2']),
                        'use_hours'=>$cal['use_hours'],
                        'use_minutes_mod'=>$cal['use_minutes_mod']
                        );
                    if(!repeatEngineer($engineer,$vv['engineer'])){
                        array_push($engineer, $temp);
                    }else{
                        $engineer = updateUseMinute($engineer, $temp['engineer'], $vv['use_minutes'], $vv);
                    }
                }
            }
        }
    }

    $response = array();
    $response["error"] = false;

    $viewObj = new ProjectManagementView($email);

    $response['data'] = $data;
    $project_detail['timesTotal'] = $timesTotal;
    $response['project_detail'] = $project_detail;
    $response['engineer'] = $engineer;

    $response["template"] = $viewObj->templateProjectDetail($email, $token, $data, $engineer, $project_detail,$tabName);
    echoRespnse(200, $response);
});
function repeatEngineer($engineerArray, $engineer){
    foreach ($engineerArray as $key => $value) {
        if($value['engineer']==$engineer){
            return true;
        }
    }
    return false;
}
function updateUseMinute($engineerArray, $engineer, $useMinutes, $data){
    foreach ($engineerArray as $key => $value) {
        if($value['engineer']==$engineer){
            $engineerArray[$key]['use_minutes']+= $useMinutes;
            $engineerArray[$key]['times']++;
            $engineerArray[$key]['taxi'] +=($data['taxi_fare_1']+$data['taxi_fare_2']);
        }
    }
    return $engineerArray;
}

$app->post('/loadTemplateProjectmanagement', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $order_by = $app->request->post('order_by');
    $search = $app->request->post('search');
    $windowHeight = $app->request->post('windowHeight');

    require_once '../../view/ProjectManagementView.php';
    
    $obj = new ProjectModel($email);
    $permission_role = $obj->permission_role;
    $data = $obj->listProject($email, $token, $order_by, $search);

    $canAssignTo = $obj->listUserCanAddProject($email);
    $response = array();
    $response["error"] = false;

    $viewObj = new ProjectManagementView($email);
    $response["template"] = $viewObj->templateProjectManagement($email, $data, $order_by, $search, $permission_role,$canAssignTo,$windowHeight);
    $response['data'] = $data;
    $response['permission_role'] = $permission_role;
    $response['canAssignTo'] = $canAssignTo;
    echoRespnse(200, $response);
});
$app->post('/loadTemplateCalendarYear', function() use ($app){

    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $windowHeight = $app->request->post('windowHeight');

    require_once '../../view/PlanningServiceView.php';
    $viewObj = new PlanningServiceView($email);

    $planningModel = new PlanningModel();
    $dataDateYear = array();
    
    for ($i=1; $i <= 12; $i++) { 
        $dataDate = $planningModel->genYear('2017',$i);
        array_push($dataDateYear, $dataDate); 
    }
    
    $response = array();
    $response["error"] = false;
    $response["template"] = $viewObj->templateCalendarYear($dataDateYear,$windowHeight);
    $response['email'] = $email;
    $response['dataDateYear'] = $dataDateYear;
    echoRespnse(200, $response);
});

$app->post('/loadTemplatePlanningService', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $viewDate = $app->request->post('viewDate');
    $windowHeight = $app->request->post('windowHeight');

    require_once '../../view/PlanningServiceView.php';
    $planningObj = new PlanningModel();

    $viewObj = new PlanningServiceView($email);

    if($viewDate==""){
        $viewDate = date('Y-m-d');
    }

    $viewDate = explode("-", $viewDate);
    $viewDate = $viewDate['2'].'-'.$viewDate['1'].'-'.$viewDate['0'];
    $planningDayViewData = $planningObj->planningDayViewByCanassign($viewDate, $email);

    $response = array();
    $response["error"] = false;
    $response["template"] = $viewObj->templatePlanningService($email,$viewDate,$planningDayViewData,$windowHeight);
    $response['planningDayViewData'] = $planningDayViewData;
    echoRespnse(200, $response);
});

$app->post('/templateStandby',function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $moreSystemView = new MoreSystemView($email);
    $response = array();
    $response["error"] = false;

    $planningModel = new PlanningModel($email);
    $permission_role = $planningModel->permission_role;

    $dataDateYear = array();
    $dataDate = $planningModel->genYear('2017','01');
    array_push($dataDateYear, $dataDate); 
    $dataDate = $planningModel->genYear('2017','02');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','03');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','04');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','05');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','06');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','07');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','08');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','09');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','10');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','11');
    array_push($dataDateYear, $dataDate);
    $dataDate = $planningModel->genYear('2017','12');
    array_push($dataDateYear, $dataDate);
    
    $buddy = $planningModel->buddy();
    $response["template"] = $moreSystemView->templateStandbyView($email,$dataDateYear, $buddy, $permission_role);
    $response['email'] = $email;
    $response['dataDate'] = $dataDateYear;
    $response['buddy'] = $buddy;
    $response['permission_role'] = $permission_role;
    echoRespnse(200, $response);

});
$app->post('/SlaSystemLoadRule', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $rule_sid = "";
    if($app->request->post('rule_sid')){
        $rule_sid = $app->request->post('rule_sid');
    }
    require_once '../../view/SlaView.php';
    $response = array();
    $response["error"] = false;
    $slaView = new SlaView($email);

    $slaModel = new SlaModel($email);
    $ruleSla = $slaModel->getRuleSlaBySid($rule_sid);
    $response["template"] = $slaView->templateRuleSlaView($email, $ruleSla);
    $response['email'] = $email;
    $response['ruleSla'] = $ruleSla;
    echoRespnse(200, $response);
});

$app->post('/SlaSystemView', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $search = "";
    $sort = "";
    $sort_type = "";
    if($app->request->post('search')){
        $search = $app->request->post('search');
    }
    if($app->request->post('sort')){
        $sort = $app->request->post('sort');
    }
    if($app->request->post('sort_type')){
        $sort_type = $app->request->post('sort_type');
    }

    $response = array();
    $response["error"] = false;
    $moreSystemView = new MoreSystemView($email);

    $slaModel = new SlaModel($email);
    $ruleSla = $slaModel->getRuleSla($search,$sort,$sort_type);
    $response["template"] = $moreSystemView->templateSlaSystemView($email, $ruleSla,$search);
    $response['email'] = $email;
    $response['ruleSla'] = $ruleSla;
    echoRespnse(200, $response);
});
$app->post('/CreateNewSlaRuleView', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $response = array();
    $response["error"] = false;
    $moreSystemView = new MoreSystemView($email);
    $response["template"] = $moreSystemView->templateCreateNewSlaRuleView($email);
    $response['email'] = $email;
    echoRespnse(200, $response);
});

$app->post('/template', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $response = array();
    $response["error"] = false;
    $moreSystemView = new MoreSystemView($email);
    $response["template"] = $moreSystemView->template();
    echoRespnse(200, $response);
});
$app->post('/templateAllApplication', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $response = array();
    $response["error"] = false;

    $moreSystemView = new MoreSystemView($email);

    $userModel = new UserModel();
    $dataProfile = $userModel->getProfile($email);

    $response["template"] = $moreSystemView->templateAllApplication($email,$dataProfile,$dataProfile,array(),array());
    echoRespnse(200, $response);
});
$app->post('/templateStaffSignature', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $userModel = new UserModel();
    $data = $userModel->getProfile($email);

    $response = array();
    $response["error"] = false;

    $moreSystemView = new MoreSystemView($email);
    $response["template"] = $moreSystemView->templateStaffSignature($data);
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/savesignature', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $base64 = $app->request->post('base64');
    $data = array('base64'=>$base64);
    $response = array();
    $response["error"] = false;
    $model = new UserModel();
    $model->saveSignature($email, $base64, $token);

    // $moreSystemView = new MoreSystemView($email);
    // $response["template"] = $moreSystemView->templateStaffSignature($data);
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/templateProfile', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $userModel = new UserModel();
    $data = $userModel->getProfile($email);

    $response = array();
    $response["error"] = false;

    $moreSystemView = new MoreSystemView($email);
    $response["template"] = $moreSystemView->templateProfile($data);
    echoRespnse(200, $response);
});
$app->post('/templateSyncRemedy', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $userModel = new UserModel();
    // $data = $userModel->getProfile($email);
    $data = $userModel->getRemedyPassword($email);

    $moreSystemView = new MoreSystemView($email);

    $response = array();
    $response["error"] = false;
    $response["template"] = $moreSystemView->templateSyncRemedy($data);
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/templateBugReport', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $userModel = new UserModel();
    // $data = $userModel->getProfile($email);
    $data = $userModel->getRemedyPassword($email);

    $moreSystemView = new MoreSystemView($email);

    $response = array();
    $response["error"] = false;
    $response["template"] = $moreSystemView->templateBugReport($data, $email);
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/templateCaseWip', function() use ($app){
    verifyRequiredParams(array('email','token'));
    $data = array();
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $case_status = "";
    if($app->request->post('case_status')){
    	$case_status = $app->request->post('case_status');
    }
    $owner_type = "";
    $search = "";
    if($app->request->post('case_view')){
        $case_view = $app->request->post('case_view');

        if(isset($case_view['search'])){
            $search = $case_view['search'];
        }
        $owner_type = $case_view['owner'];
    }

    $filter_case = $app->request->post('case_filter_cases');
    if($filter_case!=""){
        $filter_case_string = "";
        $filter_case = explode(",", $filter_case);
        foreach ($filter_case as $key => $value) {
            if($filter_case_string!=""){
                $filter_case_string .= ",";
            }
            if($value=="pm"){
                $filter_case_string .= "'Preventive Maintenance'";
            }else{
                $filter_case_string .= "'".ucfirst($value)."'";
            }
        }
        $filter_case = $filter_case_string;
    }else{
        $filter_case = "'Incident','Request','Question','Implement','Install','Preventive Maintenance'";
    }
    $obj = new CaseModel();

    $options = array(
        'case_type'=>$filter_case,
        'sort'=>'DESC'
    );

	$type_view = "active";
    if($case_status=="5"){
    	$type_view = "resolved";
    }else if ($case_status=="99"){
        $type_view = "missed";
    }

    if($owner_type=="myself"){
        $owner = $email;
    }else if($owner_type=="group"){
        $owner = $owner_type;
    }else{
        $owner = "";
    }
    $data = $obj->lists('Incident',"",$options, $email,$search,$type_view, $owner);

    $moreSystemView = new MoreSystemView($email);

    $response = array();
    $response["error"] = false;
    $response["template"] = $moreSystemView->templateCaseWip($data, $email, $type_view);
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/templateCaseSettingFilter', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $returnBack = $app->request->post('returnBack');
    $data = array();
    // $userModel = new UserModel();
    // $data = $userModel->getProfile($email);
    // $data = $userModel->getRemedyPassword($email);

    $moreSystemView = new MoreSystemViewKaew($email);

    $response = array();
    $response["error"] = false;
    $response["template"] = $moreSystemView->templateCaseSettingFilter($data, $email, $returnBack);
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/templateFormPart',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    $view = new SparePartView($email);
    $response = array();
    $response["error"] = false;
    $template = "";
   
    $template = $view->templateCreateOracleSrStep4($data);
   
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/templateCreateOracleSr',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');
    $view = new SparePartView($email);
    $response = array();
    $response["error"] = false;
    $template = "";
    if(!isset($data['sr']['type']) || $data['sr']['type']==""){
        $template = $view->templateCreateOracleSrStep1($data);
    }else if(!isset($data['sr']['no_sr']) || $data['sr']['no_sr']=="" || $data['sr']['model']=="" || $data['sr']['created_date']==""){
        $template = $view->templateCreateOracleSrStep2($data);
    }else if(!isset($data['task']) || $data['task']['no_task']==""){
        $template = $view->templateCreateOracleSrStep3($data);
    }else if(!isset($data['part']) 
        || $data['part']['part_number']=="" 
        || $data['part']['part_description']=="" 
        || $data['part']['request_spare_part_date']=="" 
        || $data['part']['delivery_due_date']=="" 
        || $data['part']['receive_spare_part_date']=="" 
        || $data['part']['part_quantity']==""
        ){
        $template = $view->templateCreateOracleSrStep4($data);
    }else{
        $template = $view->templateCreateOracleSrStep5($data);
    }
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/templateCreateOracleTask',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');
    $spare_sid = "";
    if($app->request->post('spare_sid')){
        $spare_sid = $app->request->post('spare_sid');
    }
    $view = new SparePartView($email);
    $response = array();
    $response["error"] = false;
    $template = "";
    if(!isset($data['task']) || $data['task']['no_task']==""){
        $template = $view->templateCreateOracleSrStep3($data);
    }else if(!isset($data['part']) 
        || $data['part']['part_number']=="" 
        || $data['part']['part_description']=="" 
        || $data['part']['request_spare_part_date']=="" 
        || $data['part']['delivery_due_date']=="" 
        || $data['part']['receive_spare_part_date']=="" 
        || $data['part']['part_quantity']==""
        ){
        $template = $view->templateCreateOracleSrStep4($data);
    }else{
        $template = $view->templateCreateOracleSrStep5($data);
    }
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/loadTemplateSendMailReturnPart', function() use ($app){
    $email = "";
    $token = "";
    $ticket_sid = "";
    $spare_sid = "";
    $spare_task_sid = "";
    if($app->request->post('email')){
        $email = $app->request->post('email');
    }
    if($app->request->post('spare_sid')){
            $spare_sid = $app->request->post('spare_sid');
    }
    if($app->request->post('spare_task_sid')){
        $spare_task_sid = $app->request->post('spare_task_sid');   
    }
    $obj = new CaseModel();

    $data_oracle_sr = $obj->viewDataOracleSr($email, $spare_sid, $token, $ticket_sid, $spare_task_sid);
    $data_oracle_task = $obj->viewDataOracleTask($email, $spare_sid, $token, $ticket_sid, $spare_task_sid);
    $data = $obj->viewOraclePart($email, $spare_sid, $token, $ticket_sid, $spare_task_sid);
    $view = new SparePartView($email);
    $contact = $obj->getInfoEmployee($email);
    $template = $view->templateSendMailReturnPart($email, $data_oracle_sr, $data_oracle_task, $data, $contact);
    $response = array();
    $response['data_oracle_sr'] = $data_oracle_sr;
    $response['data_oracle_task'] = $data_oracle_task;
    $response['template'] = $template;
    $response['data'] = $data;
    $response['contact'] = $contact;
    echoRespnse(200, $response);
});

$app->post('/sendMailToDHL', function() use ($app){
    $email = "";
    $email_subject = "";
    $email_message = "";
    if($app->request->post('email')){
        $email = $app->request->post('email');
    }
    if($app->request->post('email_subject')){
        $email_subject = $app->request->post('email_subject');
    }
    if($app->request->post('email_message')){
        $email_message = $app->request->post('email_message');   
    }
    $array_part_sid = "";
    if($app->request->post('array_part_sid')){
        $array_part_sid = $app->request->post('array_part_sid');
    }
    $cc_to = "";
    if($app->request->post('cc_to')){
        $cc_to = $app->request->post('cc_to');
    }
    $response = array();
    $obj = new CaseModel();
    $return = $obj->partIsReturn($email,$array_part_sid);
    
    $sendMail = new SendMail();
    $sendMail->sendMailToDHL($email, $email_message, $email_subject, $cc_to);

    $response['error'] = false;
    $response['email_subject'] = $email_subject;
    $response['email_message'] = $email_message;
    $response['array_part_sid'] = $array_part_sid;
    echoRespnse(200, $response);
});
$app->post('/templateViewOracleSr', function() use ($app){
    $email = "";
    $token = "";
    $ticket_sid = "";
    $spare_sid = "";
    $spare_task_sid = "";
    $data = array();

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    $template = "";

    if($app->request->post('email')){
        $email = $app->request->post('email');
    }
    if($app->request->post('token')){
        $token = $app->request->post('token');
    }
    if($app->request->post('ticket_sid')){
        $ticket_sid = $app->request->post('ticket_sid');
    }
    if($app->request->post('spare_sid')){
        $spare_sid = $app->request->post('spare_sid');
    }
    if($app->request->post('spare_task_sid')){
        $spare_task_sid = $app->request->post('spare_task_sid');   
    }
    $obj = new CaseModel();
    $data = $obj->viewOracleSr($email, $spare_sid, $token, $ticket_sid, $spare_task_sid);

    $view = new SparePartView($email);
    if($spare_task_sid){
        $template = $view->templateSparePart($data,$spare_sid,$ticket_sid, $spare_task_sid);
    }else{
        $template = $view->templateSpareTask($data,$spare_sid,$ticket_sid);
    }
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/EditPart', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;

    $obj = new CaseModel();
    $obj->editPart($email, $data, $token);
    echoRespnse(200, $response);
});

$app->post('/EditSpareTask', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;

    $obj = new CaseModel();
    $obj->editSpareTask($email, $data, $token);
    echoRespnse(200, $response);
});
$app->post('/CreateOracleOnlyPart',function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');
    $ticket_sid = $app->request->post('ticket_sid');
    $spare_sid = "";
    
    $part_number = $app->request->post('part_number');
    $part_description = $app->request->post('part_description');
    $request_spare_part_date = $app->request->post('request_spare_part_date');
    $delivery_due_date = $app->request->post('delivery_due_date');
    $receive_spare_part_date = $app->request->post('receive_spare_part_date');
    $part_quantity = $app->request->post('part_quantity');
    $spare_task_sid = $app->request->post('spare_task_sid');
    $ticket_sid = $app->request->post('ticket_sid');
    $data = array(
        'sr'=>"",
        'task'=>"",
        'type'=>"",
        'oracle_create_date'=>"",
        'model'=>"",
        'part_number'=>$part_number,
        'part_description'=>$part_description,
        'request_spare_part_date'=>$request_spare_part_date,
        'delivery_due_date' =>$delivery_due_date,
        'receive_spare_part_date'=>$receive_spare_part_date,
        'part_quantity'=>$part_quantity
        );

    if($app->request->post('spare_sid')){
        $spare_sid = $app->request->post('spare_sid');
    }
    // $data = array();
    $model = new CaseModel();

    $view = new SparePartView($email);
    $response = array();
    $template = "";
    $response["error"] = false;
    if($spare_sid!="" && $spare_task_sid!=""){
        $model->createOracleOnlyPart($email, $data, $token, $ticket_sid, $spare_sid, $spare_task_sid);
    }
    
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);

});
$app->post('/CreateOraclePart',function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');
    $ticket_sid = $app->request->post('ticket_sid');
    $spare_sid = "";
    if($app->request->post('spare_sid')){
        $spare_sid = $app->request->post('spare_sid');
    }
    // $data = array();
    $model = new CaseModel();

    $view = new SparePartView($email);
    $response = array();
    $template = "";
    $response["error"] = false;
    if(!isset($data['sr']['type']) || $data['sr']['type']==""){
        $template = $view->templateCreateOracleSrStep1($data);
    }else if(!isset($data['sr']['no_sr']) || $data['sr']['no_sr']=="" || $data['sr']['model']=="" || $data['sr']['created_date']==""){
        $template = $view->templateCreateOracleSrStep2($data);
    }else{
        $model->createOracleSr($email, $data, $token, $ticket_sid,$spare_sid);
    }
    
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);

});
$app->post('/templateCreateOracleConfirm',function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    // $data = array();
    $view = new SparePartView($email);
    $response = array();
    $template = "";
    $response["error"] = false;
    if(!isset($data['sr']['type']) || $data['sr']['type']==""){
        $template = $view->templateCreateOracleSrStep1($data);
    }else if(!isset($data['sr']['no_sr']) || $data['sr']['no_sr']=="" || $data['sr']['model']=="" || $data['sr']['created_date']==""){
        $template = $view->templateCreateOracleSrStep2($data);
    }else{
        $template = $view->templateCreateOracleSrStep5($data);
    }
    
    $response["template"] = $template;
    $response['data'] = $data;
    echoRespnse(200, $response);

});

$app->post('/templateCaseDetail', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $returnBack = $app->request->post('returnBack');
    $ticket_sid = $app->request->post('ticket_sid');
    $view_page = $app->request->post('view_page');
    if(!$view_page){
        $view_page = "1";
    }
    $type_request = "";
    if($app->request->post('type_request')){
        $type_request = $app->request->post('type_request');
    }   

    $data = array();
    $moreSystemView = new MoreSystemView($email);

    $obj = new CaseModel();
    $data = $obj->getCaseDetail($ticket_sid, $email, $token);

    $response = array();
    $response["error"] = false;

    if($type_request=="web"){
        $response["template"] = $moreSystemView->templateCaseDetailWeb($data, $email, $returnBack,$view_page);
    }else{
        $response["template"] = $moreSystemView->templateCaseDetail($data, $email, $returnBack,$view_page);
    }
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/templateCreateServiceReport', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $returnBack = $app->request->post('returnBack');
    $ticket_sid = $app->request->post('ticket_sid');
    $view_page = $app->request->post('view_page');
    $case_sr_of_ticket = $app->request->post('case_sr_of_ticket');
    $search = "";
    $data = array();
    $moreSystemView = new ServiceReportCreateView($email);

    $type_request = "";
    if($app->request->post('type_request')){
        $type_request = $app->request->post('type_request');
    }

    $request_page = "";
    if($app->request->post('request_page')){
        $request_page = $app->request->post('request_page');
    }

    $response = array();
    $response["error"] = false;
    $obj = new CaseModel();
    $dataCase = $obj->getCaseDetail($ticket_sid, $email, $token);
    
    if($request_page=="manual_person"){
        $response["template"] = $moreSystemView->serviceReportEndUserCreateNew($data, $email, $returnBack,$dataCase, $type_request);
    }else if($case_sr_of_ticket['end_user']['email']==""){
        require_once '../../include/EnduserModel.php';
        $endUserModel = new EnduserModel();
        $data = $endUserModel->listEndUser20161215($dataCase['contract_no'],$email,$ticket_sid);
        $response["template"] = $moreSystemView->serviceReportEndUserChoose($data, $email, $returnBack,$dataCase, $type_request);
    }else if($case_sr_of_ticket['subject']==""){
        $response["template"] = $moreSystemView->serviceReportSubject($data, $email, $returnBack,$dataCase, $type_request);
    }else if($case_sr_of_ticket['address']==""){
        $response["template"] = $moreSystemView->serviceReportAddress($data, $email, $returnBack,$dataCase, $type_request);
    }else if($case_sr_of_ticket['type_sid']==""){
        $response["template"] = $moreSystemView->serviceReportType($data, $email, $returnBack,$dataCase, $type_request);
    }else if($case_sr_of_ticket['appointment_datetime']==""){
        $response["template"] = $moreSystemView->serviceReportAppointment($data, $email, $returnBack,$dataCase, $type_request);
    }else if($case_sr_of_ticket['engineer']==""){
        $userModel = new UserModel();
        $listUserCanAddTask = $userModel->listUserCanAddTask($email,$search);

        $createInfo = $obj->getInfoEmployee($email);
        $response["template"] = $moreSystemView->serviceReportEngineer($listUserCanAddTask, $email, $returnBack,$dataCase,$createInfo, $type_request);
    }else{
        $engineer = array();
        foreach ($case_sr_of_ticket['engineer'] as $key => $value) {
            if($value!=""){
                $dataEngineer = $obj->getInfoEmployee($value);
                array_push($engineer, $dataEngineer);
            }
        }
        $case_sr_of_ticket['engineer_info'] = $engineer;
        $response["template"] = $moreSystemView->confirmServiceReport($data, $email, $returnBack,$dataCase,$case_sr_of_ticket, $type_request);
    }

    $response['data'] = $data;
    $response['dataCase'] = $dataCase;
    echoRespnse(200, $response);
});

$app->post('/templateSLA', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $ticket_sid = $app->request->post('ticket_sid');
    $sla_page = $app->request->post('sla_page');
    $step = $app->request->post('step');
    $worklog = $app->request->post('worklog');
    $type_request = $app->request->post('type_request');

    require_once '../../view/SlaView.php';
    $slaView = new SlaView($email);
    $response = array();
    $response["error"] = false;
    $response["template"] = $slaView->templateSLA($ticket_sid,$sla_page,$worklog, $step,$type_request);
    $response['data'] = $sla_page;
    echoRespnse(200, $response);
});
$app->post('/templatePortfolio', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $userModel = new UserModel();
    // $data = $userModel->getProfile($email);
    $viewEmail = $email;
    $month = 12;
    if(($month-1)<1 && 0){
        $monthStart = 12 - ($month);
    }else{
        $monthStart = $month;        
    }
    if(($month+1)>12){
        $monthEnd = ($month+1)%12;
    }else{
        $monthEnd = ($month+1);
    }

    if(strlen($monthStart)<2){
        $monthStart = "0".$monthStart;
    }
    if(strlen($monthEnd)<2){
        $monthEnd = "0".$monthEnd;
    }

    $start = "2016-".($monthStart)."-01";
    if($month==12){
        $end = "2017-".($monthEnd)."-01";
    }else{
        $end = "2016-".($monthEnd)."-01";
    }
    $db = new DbHandler();
    
    $data = array();

    $obj = new PlanningModel();
    $timeline = $obj->sheduleTimeline($start, $end);
    $leave = array();
    $blockWidth = 8.33;
    foreach ($timeline as $k => $v) {
        if(!isset($timeline[$k]['timeline'])){
            $timeline[$k]['timeline'] = array();            
        }
        $timeline[$k]['timeline_real'] = array();
        $timeline[$k]['day'] = $v['date_w'].", ".$v['day'];
        $task = $db->getTasksCompleted($viewEmail, $v['date_Ymd'], $v['next_date_Ymd']);
        foreach ($task as $key => $value) {
            array_push($timeline[$k]['timeline_real'], $value);
            $tmp = array();
            $tmp['name'] = $value['no_task'].", ".$value['contract_no'];//."<br/>Taxi (".$value['taxi_fare_in'].",".$value['taxi_fare_out'].")";
            $tmp['link'] = "/pages/dashboard/#casedetail/".$value['ticket_sid'];
            $tmp['full_name'] = $value['no_task']."<br/>".$value['contract_no']." ".$value['no_ticket']."<br/>".$value['subject']."<br/>".(($value['end_user'])?$value['end_user']:$value['case_company'])."<br/>".$value['appointment_datetime_df']."(".$value['expect_duration'].")<br/>".$value['start_task_timestamp_df']." - ".$value['closed_task_timestamp_df'].(($value['closed_task_timestamp_df'])?"<br/>Taxi (".$value['taxi_fare_in'].",".$value['taxi_fare_out'].")":"");
            $tmp['type'] = "ida practice";
            $tmp['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
            $tmp['_start'] = $value['appointment_time']/2*$blockWidth;
            $tmp['_full_position'] = "";
            if($tmp['_start']>75){
                $tmp['_full_position'] = "left";
            }
            if($tmp['_start']<25){
                $tmp['_full_position'] = "right";
            }
            if($k==0){
                $tmp['_full_position'] = $tmp['_full_position']." bottom";
            }
            if(($k+1)==count($timeline)){
                $tmp['_full_position'] = $tmp['_full_position']." top";   
            }
            //Bug Preventive Maintenance
            if($value['expect_duration']<=0){
                $value['expect_duration'] = 2;
            }
            $tmp['_width'] = $value['expect_duration']/2*$blockWidth;
            if($tmp['_start']+$tmp['_width']>100){
                $tmpContinue = array();
                $tmpContinue['_width'] = ($tmp['_start']+$tmp['_width'])-100;
                $tmp['_width'] = 100-$tmp['_start'];
                if(isset($timeline[$k+1])){
                    $tmpContinue['name'] = $value['no_task']." ".$value['contract_no'];
                    $tmpContinue['full_name'] = $value['no_task']."<br/>".$value['contract_no']." ".$value['no_ticket']."<br/>".$value['subject']."<br/>".$value['appointment_datetime_df']."(".$value['expect_duration'].")<br/>".$value['start_task_timestamp_df']." - ".$value['closed_task_timestamp_df'];
                    $tmpContinue['link'] = "/pages/dashboard/#casedetail/".$value['ticket_sid'];
                    $tmpContinue['type'] = "ida practice";
                    $tmpContinue['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
                    $tmpContinue['_start'] = 0;
                    $tmpContinue['_full_position'] = "";
                    if($tmpContinue['_start']<25){
                        $tmpContinue['_full_position'] = "right";
                    }
                    if($k==0){
                        $tmpContinue['_full_position'] = $tmpContinue['_full_position']." bottom";
                    }
                    if(($k+1)==count($timeline)){
                        $tmpContinue['_full_position'] = $tmpContinue['_full_position']." top";   
                    }

                    $tmpContinue['_width'] = $tmpContinue['_width'];///2*$blockWidth;
                    $timeline[$k+1]['timeline'] = array();
                    array_push($timeline[$k+1]['timeline'], $tmpContinue);
                }
            }
            array_push($timeline[$k]['timeline'], $tmp);
        }

        $leaveAndBussyList = $obj->leaveAndBussyList($viewEmail, $v['date_Ymd'], $v['date_Ymd']);
        foreach ($leaveAndBussyList['leave'] as $key => $value) {
            array_push($leave, $value);
            $tmp = array();
            $tmp['name'] = $value['description'];
            // $tmp['link'] = "http://case.flgupload.com/pages/dashboard/#casedetail/".$value['ticket_sid'];
            $tmp['full_name'] = $value['description']."".(isset($value['detail'])?"<br/>".$value['detail']:'')."<br/>".$value['starttime']." - ".$value['endtime'];
            $tmp['type'] = "ida practice";
            $tmp['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
            $tmp['_start'] = $value['appointment_time']/2*$blockWidth;
            $tmp['_width'] = $value['expect_duration']/2*$blockWidth;
            $tmp['_full_position'] = "";
            if($tmp['_start']>75){
                $tmp['_full_position'] = "left";
            }
            if($tmp['_start']<25){
                $tmp['_full_position'] = "right";
            }
            if($k==0){
                $tmp['_full_position'] = $tmp['_full_position']." bottom";
            }
            if(($k+1)==count($timeline)){
                $tmp['_full_position'] = $tmp['_full_position']." top";   
            }
            array_push($timeline[$k]['timeline'], $tmp);
            array_push($timeline[$k]['timeline_real'], $value);
        }

        foreach ($leaveAndBussyList['oldTask'] as $key => $value) {
            array_push($data, $value);
            $tmp = array();
            $tmp['name'] = $value['SRID'].", ".$value['contract_no'];
            // $tmp['link'] = "http://case.flgupload.com/pages/dashboard/#casedetail/".$value['ticket_sid'];
            $tmp['full_name'] = $value['SRID'].", ".$value['contract_no']."<br/>".$value['request_no']."<br/>".$value['customer_company_name']."<br/>Appointment ".$value['tooltip_appointment_time']." - ".$value['tooltip_expect_time']."<br/>Timestamp ".$value['tooltip_start_time']." - ".$value['tooltip_close_job_time'];
            $tmp['type'] = "ida practice";
            $tmp['rang'] = 'rang-'.ceil(($value['number_hour']/2));
            $tmp['_start'] = $value['appointment_h']/2*$blockWidth;
            $tmp['_width'] = $value['number_hour']/2*$blockWidth;
            $tmp['_full_position'] = "";
            if($tmp['_start']>75){
                $tmp['_full_position'] = "left";
            }
            if($tmp['_start']<25){
                $tmp['_full_position'] = "right";
            }
            if($k==0){
                $tmp['_full_position'] = $tmp['_full_position']." bottom";
            }
            if(($k+1)==count($timeline)){
                $tmp['_full_position'] = $tmp['_full_position']." top";   
            }
            array_push($timeline[$k]['timeline'], $tmp);
            array_push($timeline[$k]['timeline_real'], $value);
        }
        $taskLength = count($timeline[$k]['timeline']);
        if($taskLength>1){
            foreach ($timeline[$k]['timeline'] as $key => $value) {
                $timeline[$k]['timeline'][$key]['_height'] = 100/$taskLength;
                $timeline[$k]['timeline'][$key]['_top'] = $key*100/$taskLength;
            }
        }

    }

    $moreSystemView = new MoreSystemView();
    $response = array();
    $response["error"] = false;
    $response["template"] = $moreSystemView->templatePortfolio($timeline);
    $response['data'] = $timeline;
    echoRespnse(200, $response);
});

/* END template*/

/* WS SELECT INSERT UPDATE DELETE*/
$app->post('/saveRemedyPassword', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $remedy_user = $app->request->post('remedy_user');
    $remedy_password = $app->request->post('remedy_password');

    $userModel = new UserModel();
    // $data = $userModel->getProfile($email);
    $data = $userModel->saveRemedyPassword($email,$remedy_user, $remedy_password);

    $moreSystemView = new MoreSystemView($email);

    $response = array();
    $response["error"] = false;
    // $response["template"] = $moreSystemView->templateSyncRemedy($data);
    $response['message'] = "Done";
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/saveBugReport', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $subject_bug = $app->request->post('subject_bug');
    
    $obj = new BugReportModel();
    
    $data = $obj->saveBugReport($email, $subject_bug);
    $response = array();
    $response["error"] = false;
    $response['message'] = "Done";
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/loadBadge',function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $seen_notification_sid = $app->request->post('seen_notification_sid');

    $db = new DbHandler();
    $dataTask = $db->getTasksWorking($email);
    $response = array();
    $response['tasks'] = array();
    foreach ($dataTask as $key => $value) {
        $tmp = array();
        $serviceTypeObj = new ServiceType();
        $serviceTypeList = $serviceTypeObj->listServiceStatus($value['service_type']);
        for($i=0;$i<=600;$i+=100){
            if($value['tasks_log_status']=="400" && !$value['is_account']){
                $value['tasks_log_status'] = 600;
            }
            if($value['tasks_log_status']<=$i){
                $next = ($i/100 )+1;
                for($j=$next;$j<=6;$j++){
                    $checkServiceStatusRequired = $serviceTypeObj->checkServiceStatusRequired($j, $serviceTypeList);
                    if($j==5 && $value['is_number_one']==false){
                    }else{
                        if($checkServiceStatusRequired['result']){
                            $tmp["subject_type"] = $checkServiceStatusRequired['subject'];
                            $j+=100;
                            $i+=1000;
                        }
                    }
                }
            }
        }
        if(isset($tmp['subject_type'])){
            array_push($response["tasks"], $tmp);
        }       
    }
    $dataNotification = $db->getNotifications($email,$seen_notification_sid);
    $dataUnsigned = $db->getUnsigned($email);
    $response["error"] = false;
    $response['dataUnsigned'] = $dataUnsigned;
    $response['dataNotification'] = $dataNotification;
    $response['dataTodolist'] = $response["tasks"];
    $response['dataTask'] = $dataTask;
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });



/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>