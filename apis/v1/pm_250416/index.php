<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/PreventiveMaintenanceModel.php';
require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
 
// ดึงข้อมูล serial ของ ticket
$app->post('/select/serial_ticket', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_sid'));
 
    // reading post params
    $ticket_sid = $app->request->post('ticket_sid');


    $pmModel = new PreventiveMaintenanceModel();
    $serial = $pmModel->selectSerialTicket($ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['serial'] = $serial;

    // echo json response
    echoRespnse(200, $response);
});
 
// ดึงข้อมูลเพื่อแสดงว่า ticket ให้ทำ PM อะไรบ้าง
$app->post('/select/to_do_pm', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_sid'));
 
    // reading post params
    $ticket_sid = $app->request->post('ticket_sid');

    $pmModel = new PreventiveMaintenanceModel();
    $data = $pmModel->selectToDoPM($ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['todppm'] = $data;
    
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/insert/do_serial_pm', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_serial_sid','task_sid','status','email'));
 
    // reading post params
    $ticket_serial_sid = $app->request->post('ticket_serial_sid');
    $task_sid = $app->request->post('task_sid');
    $status = $app->request->post('status');
    $email = $app->request->post('email');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('ticket_serial_sid'=>$ticket_serial_sid,'task_sid'=>$task_sid,'status'=>$status,'create_by'=>$email);

    $result = $pmModel->insertDoSerialPM($param);


    $response = array();
    $response['result'] = $result;
    
    // echo json response
    echoRespnse(200, $response);
});



$app->post('/insert/input_action', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('task_sid','problem','email'));
 
    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email);

    $result = $pmModel->insertInputAction($param);


    $response = array();
    $response['result'] = $result;
    
    // echo json response
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>