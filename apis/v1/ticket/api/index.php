<?php

require_once '../../include/TicketModel.php';
require_once '../../include/CaseModel.php';
require_once "../../include/TokenModel.php";
require_once "../../include/ProjectModel.php";
require_once "../../include/RoleModel.php";
require_once __dir__."/../../../include/WebServiceToRemedy/VSpaceToKBank.php";
require_once __dir__."/../../../core/constant.php";
require_once __dir__."/../../../include/ServiceRequestLog.php";

$index = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$data);

        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();
        $info = $tokenObj->info($data['token'], $data['email']);
        if(isset($info['role_sid'])){
            $permission = $tokenObj->permissionTicket($info['role_sid']);
            $search_refer_remedy_hd = isset($data['search_refer_remedy_hd'])?$data['search_refer_remedy_hd']:'';
            $res = $caseModel->tickets($permission, $data, $data['email'], $data['search'], $info['role_sid'], $search_refer_remedy_hd);
            // while(count($res)>0){
            //     $data['dataStart'] += $data['dataLimit'];
                // $dataAll = array_merge($dataAll, $res);
                // $res = $caseModel->tickets($permission, $data, $data['email'], $data['search']);
            // }

            $caseType = $caseModel->ticketType($permission, $data, $data['email'], $data['search']);
        }else{
            $res = array();
        }
        $response = array();


        $response['data'] = $res;
        $response['type'] = $caseType;
        $response['request'] = ($data);
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$updateTicketOwner = function () use($app){
    parse_str($app->request()->getBody(),$req);
    $tokenObj = new TokenModel();
    $caseModel = new CaseModel();
      $info = $tokenObj->info($req['token'], $req['email']);
          if(isset($info['role_sid'])){
    $result = false;
    $rowAffect = 0;
    $checkAuthOwner = $caseModel->checkAuthorizeForUpdate($req['ticket_sid']);
    $checkAuthSenior = $caseModel->checkAUTHSernior($req['email']);
    if ($checkAuthSenior == 2) {
      if (!empty($req['new_owner']) || $req['new_owner'] != null) {
          $rowAffect = $caseModel->updateOwner($req['ticket_sid'], $req['new_owner'], $req['submitter']);
          $log = $caseModel->insertOwnerLog($req['ticket_sid'],$req['new_owner'],$req['submitter']);
        }
        $response = array();
        $response['message']  = $rowAffect>0?'Update Completed':'Nothing Change';
        $response['error'] = 'false';
        echoRespnse(200, $response);

    }elseif ($checkAuthOwner['user_type'] == 1 || $checkAuthOwner['owner'] == $req['email']){
          //implement check permission ?
          if (!empty($req['new_owner']) || $req['new_owner'] != null) {
          $rowAffect = $caseModel->updateOwner($req['ticket_sid'], $req['new_owner'], $req['submitter']);
          $log = $caseModel->insertOwnerLog($req['ticket_sid'],$req['new_owner'],$req['submitter']);
          // implement add log change owner
          }
    $response = array();
    $response['message']  = $rowAffect>0?'Update Completed':'Nothing Change';
    $response['error'] = 'false';
    echoRespnse(200, $response);
   }else{
    $response['error'] = 'true';
    $response['message'] = ERROR_AUTH_1_MSG;
    echoRespnse(200, $response);
  }
}
};

$detail = function() use ($app){

    parse_str($app->request()->getBody(),$request);

    $tokenObj = new TokenModel();
    $caseModel = new CaseModel();
    $obj = new ProjectModel();
    $rObj = new RoleModel();
    $contactPeople = array();
    $response = array();

    // check token
    $info = $tokenObj->info($request['token'], $request['email']);
    if(isset($info['role_sid'])){
        $data = $obj->ticketDetail($request['ticket_sid'],$info['email']);
        $response['permission'] = $data['permission'] = $rObj->permissionTicketWithRole($data, $request);
        $tasks = $caseModel->taskOfTicketQuery($request['ticket_sid']);
        if(isset($data['contract_no']) && isset($data['end_user'])){
            $contactPeople = $obj->contactPeople($data['contract_no'], trim($data['end_user']), $request['ticket_sid']);
        }
    }


    $response['data'] = $data;
    $response['tasks'] = $tasks;
    $response['contactPeople'] = $contactPeople;
    $response['request'] = $request;
    echoRespnse(200, $response);
};

$importTicketServiceRequest = function () use($app){
    $request = json_decode($app->request()->getBody() ,true);
    $tokenModel = new TokenModel();
    $info = $tokenModel->info($request['token'], $request['email']);
    $response = [];
    $import_fail = [];
    $duplicateTicket = array("data"=>[], "error_type"=>"Duplicate ticket");
    $missingRequiredField = array("data"=>[], "error_type"=>"Missing required fields");
    $assignedGroupNotFound = array("data"=>[], "error_type"=>"Assigned group not found");
    $otherError = array("data"=>[], "error_type"=>"Something went wrong");
    $response['import_success'] = [];
    if(!isset($info['role_sid'])){
        // invalid token
        $response['message'] = 'Invalid token';
        $response['error'] = true;
        echoRespnse(401, $response);
        return;
    }
    // loop process import ticket request

    foreach ($request['data'] as $ticket){
        $serviceRequestLog = new ServiceRequestLog();
        if(!$serviceRequestLog->initData($ticket)){
            array_push($missingRequiredField['data'], $ticket);
            continue;
        }
        if(!$serviceRequestLog->setTeamAndOwner()){
            array_push($assignedGroupNotFound['data'], $ticket);
            continue;
        }
        // new CaseModel object from Service Request
        $caseModel = $serviceRequestLog->mapDataToTicketObject();
        if($caseModel->isDuplicateTicket()){
            array_push($duplicateTicket['data'], $ticket);
            continue;
        }
        // insert new ticket
        $success = $caseModel->insertRequestTicket();
        // insert log import ticket
        $serviceRequestLog->no_ticket = $caseModel->caseNo;
        $serviceRequestLog->insertLog();
        if($success){
            array_push($response['import_success'], $ticket);
        }else{
            array_push($otherError['data'], $ticket);
        }

    }
    count($duplicateTicket['data']) && array_push($import_fail, $duplicateTicket);
    count($missingRequiredField['data']) && array_push($import_fail, $missingRequiredField);
    count($assignedGroupNotFound['data']) && array_push($import_fail, $assignedGroupNotFound);
    count($otherError['data']) && array_push($import_fail, $otherError);
    count($import_fail) && $response['import_fail'] = $import_fail;
    echoRespnse(200, $response);
};

$getFileAttach = function () use($app){
    //parse_str($app->request()->getBody(),$request);
    $request['email'] = $app->request()->post('email');
    $request['token'] = $app->request()->post('token');
    $request['sid'] = $app->request()->post('sid');

    $tokenObj = new TokenModel();
    $caseModel = new CaseModel();
    $caseModel->ticket_sid = $request['sid'];
    $rObj = new RoleModel();
    $info = $tokenObj->info($request['token'], $request['email']);
    if(isset($info['role_sid'])){
        $ticketStatusDetail = $caseModel->getTicketStatusDetailFromSid($request['sid']);
        $role = $rObj->permissionTicketWithRole($caseModel->getTicketDetail(['create_by', 'owner']), $request);
        if($role['download_attach_file']){
            $file = __dir__."/../../../../".$ticketStatusDetail['path_file_attach'];
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
};

$create = function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $request['email'] = $email;
    $request['token'] = $token;

    $response = array();
    // $response['res'] = $res;
    $response['request'] = $request;
    echoRespnse(200, $response);
};
$edit = function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $request['email'] = $email;
    $request['token'] = $token;

    $response = array();
    // $response['res'] = $res;
    $response['request'] = $request;
    echoRespnse(200, $response);
};
$updateSla = function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $request['email'] = $email;
    $request['token'] = $token;

    $response = array();
    // $response['res'] = $res;
    $response['request'] = $request;
    echoRespnse(200, $response);
};

$cManHours = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();

        $obj = new ProjectModel();
        $res = array();
        $info = $tokenObj->info($request['token'], $request['email']);
        if(isset($info['role_sid'])){
            $data = $caseModel->cManHours($request['ticket_sid'], $request['new_man_hours'], $info['email']);

            $data = $obj->ticketDetail($request['ticket_sid'],$info['email']);
            $tasks = $caseModel->taskOfTicketQuery($request['ticket_sid']);
        }

        $response = array();
        $response['data'] = $data;
        $response['tasks'] = $tasks;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$updateTicketWorklogToKBank = function() use ($app){
    $vSpaceToKBank = new VSpaceToKBank();
    echoRespnse(200, "test");
};

$cEndUser = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);
        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();

        $obj = new ProjectModel();
        $res = array();
        $info = $tokenObj->info($request['token'], $request['email']);
        if(isset($info['role_sid'])){
            $data = $caseModel->cEndUser(
                $request['ticket_sid'],
                $info['email'],
                $request['eu_name'],
                $request['eu_email'],
                $request['eu_mobile'],
                $request['eu_phone'],
                $request['eu_company']
                );

            $data = $obj->ticketDetail($request['ticket_sid'],$info['email']);
            $tasks = $caseModel->taskOfTicketQuery($request['ticket_sid']);
        }

        $response = array();
        $response['data'] = $data;
        $response['tasks'] = $tasks;
        $response['request'] = $request;
        echoRespnse(200, $response);

    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$sosUpdateStatus = function() use ($app){
 try{
        parse_str($app->request()->getBody(),$request);
        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();

        $caseModel->sosUpdateStatus($request);

        $res = array();

        $response = array();
        // $response['data'] = $data;
        // $response['tasks'] = $tasks;
        $response['request'] = $request;
        echoRespnse(200, $response);

    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$cEditNote = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $caseModel = new CaseModel();
        $data = $caseModel->cEditNote($request['email'], $request['ticket_sid'], $request['note']);

        $response = array();
        $response['request'] = $request;
        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$cEditAddress = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $caseModel = new CaseModel();
        $data = $caseModel->cEditAddress($request['email'], $request['ticket_sid'], $request['address']);

        $response = array();
        $response['request'] = $request;
        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$notificationDetail = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $caseModel = new CaseModel();
        $data = $caseModel->notificationDetail($request['sid']);

        $response = array();
        $response['request'] = $request;
        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
