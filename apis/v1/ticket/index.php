<?php
ob_start('ob_gzhandler');
error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/TicketModel.php';
require_once '../../include/CaseModel.php';
require_once "../../include/TokenModel.php";
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development'
));
foreach (glob("api/*.php") as $filename){require_once ($filename);}
$app->post('/', $index);
$app->post('/detail',$detail);
$app->post('/create',$create);
$app->post('/cManHours', $cManHours);
$app->post('/cEndUser', $cEndUser);
$app->post('/sosUpdateStatus', $sosUpdateStatus);
$app->post('/cEditNote', $cEditNote);
$app->post('/cEditAddress', $cEditAddress);
$app->post('/notificationDetail', $notificationDetail);
$app->post('/updateTicketOwner', $updateTicketOwner);
$app->post('/getFileAttach', $getFileAttach);
$app->post('/importTicketServiceRequest', $importTicketServiceRequest);
function mw1() {
    return "This is middleware!";
};

$app->post('/followup', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $date = $app->request->post('date');
    $source = $app->request->post('source');

    $obj = new CaseModel();
    $result = $obj->followup($email,$tasks_sid,$date,$source);

    $response = array();
    $response["error"] = false;
    $response['data'] = $result;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/update_spare_part', function() use ($app){
    verifyRequiredParams(array('token','email','part_number_defective','part_serial_defective','part_number','quantity','sparepart_sid','tasks_sid'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $part_number_defective = $app->request->post('part_number_defective');
    $part_serial_defective = $app->request->post('part_serial_defective');
    $part_number = $app->request->post('part_number');
    $part_serial = $app->request->post('part_serial');
    $description = $app->request->post('description');
    $quantity = $app->request->post('quantity');
    $sparepart_sid = $app->request->post('sparepart_sid');
    $tasks_sid = $app->request->post('tasks_sid');

    $obj = new CaseModel();
    $result = $obj->updateSparePart($email,$part_number_defective,$part_serial_defective,$part_number,$part_serial,$description,$quantity,$sparepart_sid,$tasks_sid);
    
    $response = array();
    $response["error"] = false;
    $response['data'] = $result;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/deleteSparePart', function() use ($app){
    verifyRequiredParams(array('token','email','sparepart_sid'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $sparepart_sid = $app->request->post('sparepart_sid');
    $obj = new CaseModel();
    $result = $obj->deleteSparePart($email,$sparepart_sid);
    
    $response = array();
    $response["error"] = false;
    $response['data'] = $result;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/task_request',function() use ($app){
    
    verifyRequiredParams(array('token','email','task_request','expect_time','tasks_sid'));

    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $task_request = $app->request->post('task_request');
    $expect_time = $app->request->post('expect_time');
    $tasks_sid = $app->request->post('tasks_sid');
    $task_request_sid = $app->request->post('task_request_sid');
    $ticketObj = new TicketModel();
    $task_request_sid = $ticketObj->inputTaskRequest($email,$task_request,$expect_time,$tasks_sid,$task_request_sid);
    
    $response = array();
    
    $response["error"] = false;
    $response['data'] = array('task_request_sid'=>$task_request_sid);

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/get_task_request', function() use ($app){
    verifyRequiredParams(array('task_request_sid','email'));
    $task_request_sid = $app->request->post('task_request_sid');

    $ticketObj = new TicketModel();
    $data = $ticketObj->getTaskRequest($task_request_sid);
    
    $response = array();
    
    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
    
});

$app->post('/deleteTaskRequest', function() use ($app){
    verifyRequiredParams(array('task_request_sid','email'));
    $task_request_sid = $app->request->post('task_request_sid');
    $email = $app->request->post('email');

    $ticketObj = new TicketModel();
    $data = $ticketObj->deleteTaskRequest($task_request_sid,$email);
    
    $response = array();
    
    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
    
});

$app->post('/editendser',function() use ($app){
    
	verifyRequiredParams(array('pk','name','value'));

    $pk = $app->request->post('pk');
    $name = $app->request->post('name');
    $value = $app->request->post('value');

    $ticketObj = new TicketModel();
    $res = $ticketObj->editendser($pk,$name,$value);
    $response = array();
    
    $response["error"] = false;
    $response['data'] = $res;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/delete_signature_customer', function() use ($app){
    verifyRequiredParams(array('signature_customer_sid'));

    $signature_customer_sid = $app->request->post('signature_customer_sid');

    $ticketObj = new TicketModel();
    $res = $ticketObj->deleteSignatureCustomer($signature_customer_sid);
    $response = array();
    
    $response["error"] = false;
    $response['data'] = $res;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/enduser_servicereport',function() use ($app){
    verifyRequiredParams(array('tasks_sid'));
    $tasks_sid = $app->request->post('tasks_sid');

    $obj = new CaseModel();
    $data = $obj->requestEndUserServiceReport($tasks_sid);

    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/subject_service_report',function() use ($app){
    verifyRequiredParams(array('tasks_sid'));
    $tasks_sid = $app->request->post('tasks_sid');

    $obj = new CaseModel();
    $data = $obj->requestSubjectServiceReport($tasks_sid);

    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/editSubjectServiceReport', function() use ($app){
    verifyRequiredParams(array('tasks_sid'));
    $tasks_sid = $app->request->post('tasks_sid');
    $email = $app->request->post('email');
    $subject_service_report = $app->request->post('subject_service_report');
    $service_type = $app->request->post('service_type');

    $obj = new CaseModel();
    $data = $obj->editSubjectServiceReport($tasks_sid,$email,$subject_service_report, $service_type);

    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/editEndUserServiceReport', function() use ($app){
    verifyRequiredParams(array('tasks_sid'));
    $tasks_sid = $app->request->post('tasks_sid');

    $email = $app->request->post('email');
    $end_user_contact_name_service_report = $app->request->post('end_user_contact_name_service_report');
    $end_user_email_service_report = $app->request->post('end_user_email_service_report');
    $end_user_mobile_service_report = $app->request->post('end_user_mobile_service_report');
    $end_user_phone_service_report = $app->request->post('end_user_phone_service_report');
    $end_user_company_name_service_report = $app->request->post('end_user_company_name_service_report');

    $obj = new CaseModel();
    $data = $obj->editEndUserServiceReport($tasks_sid,$email,$end_user_contact_name_service_report,$end_user_email_service_report,$end_user_mobile_service_report,$end_user_phone_service_report,$end_user_company_name_service_report);
    
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>