<?php
require '../.././libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development'
));
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');
foreach (glob("api/*.php") as $filename){require_once ($filename);}
$app->post('/', $index);
$app->post('/checkEvaluation',$checkEvaluation);
$app->post('/insertLogSendMailCussat',$insertLogSendMailCussat);
$app->post('/getAnswer',$getAnswer);
$app->post('/insertCussat',$insertCussat);
$app->post('/sendMailCusSat',$sendMailCusSat);
$app->post('/sendMailThankCusSat',$sendMailThankCusSat);



function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
