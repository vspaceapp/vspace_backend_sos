<?php
// require_once "../../include/MickModel.php";
require_once "../../include/customer_satisfactionModel.php";
require_once '../../include/TokenModel.php';

function uatWebService($url , $data){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_SSL_VERIFYPEER => 0,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      return $response;
    }
}

//require_once  __dir__."/../../../../crontab/Sla.php";
$index = function () use ($app) {
    $cusSat = new customer_satisfactionModel();
    parse_str($app->request()->getBody(),$request);
    $cusSat->initData($request);
    $cusSat->createCustomerSatisfaction();
    $response = array();
    $response['request'] = $request;
    $response['PutData'] = $cusSat;
    $response['error'] = false;

    // $response['data'] = $data;
    echoRespnse(200, $response);
};

$insertCussat  = function () use ($app){
  try {
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');
    $data = $app->request()->post('data');
    $ticket_sid = $app->request()->post('ticket_sid');
    $evaluate_by = $app->request()->post('evaluate_by');
    $type = $app->request()->post('type');
    if (!empty($type) && !empty($ticket_sid) && !empty($data) && !empty($evaluate_by) && !empty($email)){
            $res =  json_decode($data,true);
            $satisfaction_result = $res['result'];
            $other_reason =  empty($res['textOther']) ? NULL : $res['textOther'];
            $answer =empty($res['ans']) ? NULL : $res['ans'];
            $objCusSat = new customer_satisfactionModel();
            $resultEnable = $objCusSat->checkEnableButton($ticket_sid);
            if ($resultEnable == 'false') {
                if ($type == 'web' || $type == 'mobile') {
                  $tokenObj = new TokenModel();
                  $info = $tokenObj->info($token,$email);
                    if(isset($info['role_sid'])){
                      if (($satisfaction_result == 'พอใจ' && empty($answer)) || ($satisfaction_result == 'ไม่พอใจ' && !empty($answer))) {
                        $resultInsertHeader = $objCusSat->insertCussatHeader($ticket_sid,$evaluate_by,$email,$satisfaction_result,$type);
                        $response['result_insert_header'] = $resultInsertHeader;
                          if ($resultInsertHeader == 'Insert Completed' && $satisfaction_result == 'ไม่พอใจ') {
                              foreach ($answer as $key => $value) {
                                  if ($value != 4) {
                                      $other = NULL;
                                  }else {
                                      $other =$other_reason ;
                                  }
                                $resultInsertDetail = $objCusSat->insertCussatDetail($ticket_sid,$value,$other);
                              }
                              $response['result_insert_detail'] = empty($resultInsertDetail) ? 'No Detail' : $resultInsertDetail;
                          }
                          echoRespnse(200, $response);
                      }else {
                        $response["error"] = true;
                        $response["message"] = 'Bad Input';
                        echoRespnse(400, $response);
                      }
                    }else{
                       $response["error"] = true;
                       $response["message"] = 'You have Unauthorized';
                       echoRespnse(401, $response);
                    }
                }else if($type == 'mail'){
                  if (($satisfaction_result == 'พอใจ' && empty($answer)) || ($satisfaction_result == 'ไม่พอใจ' && !empty($answer))) {
                    $resultInsertHeader = $objCusSat->insertCussatHeader($ticket_sid,$evaluate_by,$email,$satisfaction_result,$type);
                    $response['result_insert_header'] = $resultInsertHeader;
                      if ($resultInsertHeader == 'Insert Completed' && $satisfaction_result == 'ไม่พอใจ') {
                          foreach ($answer as $key => $value) {
                              if ($value != 4) {
                                  $other = NULL;
                              }else {
                                  $other =$other_reason ;
                              }
                            $resultInsertDetail = $objCusSat->insertCussatDetail($ticket_sid,$value,$other);
                          }
                          $response['result_insert_detail'] = empty($resultInsertDetail) ? 'No Detail' : $resultInsertDetail;
                      }
                      echoRespnse(200, $response);
                  }else {
                    $response["error"] = true;
                    $response["message"] = 'Bad Input';
                    echoRespnse(400, $response);
                  }
                }else{
                  $response["error"] = true;
                  $response["message"] = 'Bad request type';
                  echoRespnse(400, $response);
                }
          }else{
            $response["error"] = true;
            $response["message"] = 'Duplicate Ticket';
            echoRespnse(200, $response);

          }
      }else {
        $response["error"] = true;
        $response["message"] = 'You have not require field';
        echoRespnse(400, $response);
      }
    }catch(Exception $e){
        echoRespnse(500, $response['message']=$e);
        print_r($e);
    }
};

$checkEvaluation = function () use ($app) {
  try {
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');
    $type = $app->request()->post('type');
    $ticket_no = $app->request()->post('ticket_sid');
        if ($type == 'web' || $type == 'mobile') {
            if (!empty($email) && !empty($token) && !empty($ticket_no)){
                    $tokenObj = new TokenModel();
                    $info = $tokenObj->info($token,$email);
                    if(isset($info['role_sid'])){
                        $objCusSat = new customer_satisfactionModel();
                        ##Check ว่าเคยประเมินหรือยัง
                        $resultEnable = $objCusSat->checkEnableButton($ticket_no);
                        ##นับจำนวนครั้งที่ส่ง
                        $resultAmount = $objCusSat->countSendCussat($ticket_no);
                        $response['button_enable'] = $resultEnable;
                        $response['send_amount'] = $resultAmount;
                        echoRespnse(200, $response);

                    }else{
                       $response["error"] = true;
                       $response["message"] = 'You have Unauthorized';
                       echoRespnse(401, $response);
                    }
              }else {
                $response["error"] = true;
                $response["message"] = 'You have not require field';
                echoRespnse(400, $response);
              }
        }elseif ($type == 'mail') {
            $objCusSat = new customer_satisfactionModel();
            ##Check ว่าเคยประเมินหรือยัง
            $resultEnable = $objCusSat->checkEnableButton($ticket_no);
            ##นับจำนวนครั้งที่ส่ง
            $resultAmount = $objCusSat->countSendCussat($ticket_no);
            $response['button_enable'] = $resultEnable;
            $response['send_amount'] = $resultAmount;
            echoRespnse(200, $response);
        }else {
          $response["error"] = true;
          $response["message"] = 'Bad request type';
          echoRespnse(400, $response);
        }

    }catch(Exception $e){
        echoRespnse(500, $response['message']=$e);
    }
};

  $insertLogSendMailCussat = function () use ($app) {
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      $ticket_no = $app->request()->post('ticket_sid');
      $send_by = $app->request()->post('send_by');
      $send_to = $app->request()->post('send_to');

      if (!empty($email) && !empty($token) && !empty($ticket_no) && !empty($send_by) && !empty($send_to)){
              $tokenObj = new TokenModel();
              $info = $tokenObj->info($token,$email);
              if(isset($info['role_sid'])){
                  $objCusSat = new customer_satisfactionModel();
                  $resultInsert = $objCusSat->insertLogSendMailCussat($ticket_no,$send_by,$send_to);
                  $response['result_insert'] = $resultInsert;
                  echoRespnse(200, $response);

              }else{
                 $response["error"] = true;
                 $response["message"] = 'You have Unauthorized';
                 echoRespnse(401, $response);
              }
        }else {
          $response["error"] = true;
          $response["message"] = 'You have not require field';
          echoRespnse(400, $response);
        }
      }catch(Exception $e){
          echoRespnse(500, $response['message']=$e);
      }
  };

  $getAnswer = function () use ($app) {
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      $type = $app->request()->post('type');
      if (!empty($email) && !empty($type)){
          if ($type == 'web' || $type == 'mobile') {
                $tokenObj = new TokenModel();
                $info = $tokenObj->info($token,$email);
                  if(isset($info['role_sid'])){
                      $objCusSat = new customer_satisfactionModel();
                      $resultAnswer = $objCusSat->getAnswerByType();
                      $response['Answer'] = $resultAnswer;
                      echoRespnse(200, $response);
                  }else{
                     $response["error"] = true;
                     $response["message"] = 'You have Unauthorized';
                     echoRespnse(401, $response);
                  }
          }elseif ($type == 'mail') {
              $objCusSat = new customer_satisfactionModel();
              $resultAnswer = $objCusSat->getAnswerByType();
              $response['Answer'] = $resultAnswer;
              echoRespnse(200, $response);
         }
        }else {
          $response["error"] = true;
          $response["message"] = 'You have not require field';
          echoRespnse(400, $response);
        }
      }catch(Exception $e){
          echoRespnse(500, $response['message']=$e);
      }
  };


  $sendMailCusSat = function () use ($app) {
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      $ticket_sid = $app->request()->post('ticket_sid');
      $send_to = $app->request()->post('send_to');
      if (!empty($email) && !empty($token) && !empty($ticket_sid) && !empty($send_to)){
      $objCusSat = new customer_satisfactionModel();
      $info = $objCusSat->getTicketInfoByTicketsid($ticket_sid);
      $url = $objCusSat->genURL($email,$ticket_sid,$send_to);
      $mailto = $send_to;
      $subject = 'vSpace Customer Satisfaction Survey '.$info['subject'].' '.$info['refer_remedy_hd'];
      $message = '
                    <html>
                    <head>
                    </head>
                    <body style="max-width:500px">
                        <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                            <h1 style="color:#FFFFFF">
                            vSpace
                            </h1>
                        </div>
                        <div style="color:#222222; background-color:#FFFFFF; padding:20px 10px; text-align:left;" >
                              <div>
                                  ขอขอบคุณ ที่ท่านไว้วางใจใช้บริการของเรา<br>
                                  เพื่อพัฒนาการบริการของเรากรุณาทำแบบประเมินพึงพอใจของ<br>
                                  Ticket No. :'.$info['refer_remedy_hd'].'<br>
                                  Subject : '.$info['subject'].'<br>
                                  <p><a href="'.$url.'">คลิกที่นี่เพื่อทำการประเมินความพึงพอใจ<br>Please Click Here for Customer Satisfaction Survey</a></p>
                              </div>
                            <br><br><br>
                        </div>
                        <div style="background-color:#FFFFFF; padding:20px 10px; text-align:left; ">
                            <div style="color:red; font-size:14px;">
                            *E-Mail นี้เป็นระบบอัตโนมัติกรุณาอย่าตอบกลับ
                            </div>
                        </div>
                        <div style="background-color:rgb(0,188,212); color:#FFFFFF; padding:10px 10px;">
                            <div style="font-weight:bold; font-size:16px;">
                              Best Regards
                            </div>
                            <div style="font-weight:bold; font-size:14px; padding:2px 2px;">
                              vSpace Team
                            </div>
                            <div style="font-size:12px;">
                              G-ABLE CO., LTD. <BR>
                              Panjathani Tower 15th Floor, Nonsi Road<BR>
                              Chong Nonsi, Yannawa, Bangkok 10120 THAILAND<BR>
                              Tel. 02-781-9333 Ext.4477
                            </div>
                        </div>
                    </body>
                    </html>';
      // a random hash will be necessary to send mixed content
      $separator = md5(time());

      // carriage return type (RFC)
      $eol = "\r\n";

      // main header (multipart mandatory)
      $headers = "From:vSpace Customer Satisfaction Survey<noreply@vspace.in.th>" . $eol;
      $headers .= "MIME-Version: 1.0" . $eol;
      $headers .= "Content-Type: multipart/mixed;charset=utf-8; boundary=\"" . $separator . "\"" . $eol;
      $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
      $headers .= "This is a MIME encoded message." . $eol;

      // message
      $body = "--" . $separator . $eol;
      $body .= "Content-Type: text/html; charset=\"UTF-8\"" . $eol;
      $body .= "Content-Transfer-Encoding: 8 bit" . $eol;
      $body .= $message. $eol;


      $data['mailto'] = $mailto;
      $data['subject'] = $subject;
      $data['body'] = $body;
      $data['headers'] = $headers;
      $data['sender'] = "-f noreply@vspace.in.th";
      // print_r($data);

      // SEND Mail Thank you
        $url = 'https://sosuat.vspace.in.th/apis/v1/service_send_mail/sendMail';
        $send = uatWebService($url,$data);
        // print_r($send);
      // mail($mailto, $subject, $body, $headers, "-f noreply@vspace.in.th");
      // if (mail($mailto, $subject, $body, $headers, "-f noreply@vspace.in.th")) {
          $response["error"] = false;
          $response["message"] = 'Mail Send';
          echoRespnse(200, $response);
      // } else {
      //     $response["error"] = true;
      //     $response["message"] = 'Mail Not Send';
      //     echoRespnse(400, $response);
      // }
      }else {
        $response["error"] = true;
        $response["message"] = 'You have not require field';
        echoRespnse(400, $response);
      }
  }catch(Exception $e){
      echoRespnse(500, $response['message']=$e);
      print_r($e);
  }
};


$sendMailThankCusSat = function () use ($app) {
  try {
    $email = $app->request()->post('email');
    $ticket_sid = $app->request()->post('ticket_sid');
    $send_to = $app->request()->post('send_to');
        if (!empty($email) && !empty($ticket_sid) && !empty($send_to)){
        $objCusSat = new customer_satisfactionModel();
        $info = $objCusSat->getTicketInfoByTicketsid($ticket_sid);
        $mailto = $send_to;
        $subject = 'Thank you for Customer Satisfaction Survey from.'.$info['subject'].' '.$info['refer_remedy_hd'];
        $message = '
                      <html>
                      <head>
                      </head>
                      <body style="max-width:500px">
                          <div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
                              <h1 style="color:#FFFFFF">
                              vSpace
                              </h1>
                          </div>
                          <div style="color:#222222; background-color:#FFFFFF; padding:20px 10px; text-align:left; text-align: center;" >
                                <div style ="font-size:24px;">
                                    <b>ขอบคุณสำหรับการประเมิน</b>
                                </div>
                                    <br>
                                <div style ="font-size:16px;">
                                    เราได้รับผลการประเมินของ<br>Ticket No. : '.$info['refer_remedy_hd'].'<br>Subject : '.$info['subject'].'<br>
                                    จากคุณเรียบร้อยแล้ว ขอบคุณที่ใช้บริการ
                                </div>
                          </div>
                          <div style="background-color:#FFFFFF; padding:20px 10px; text-align:left; ">
                              <div style="color:red; font-size:14px;">
                              *E-Mail นี้เป็นระบบอัตโนมัติกรุณาอย่าตอบกลับ
                              </div>
                          </div>
                          <div style="background-color:rgb(0,188,212); color:#FFFFFF; padding:10px 10px;">
                              <div style="font-weight:bold; font-size:16px;">
                                Best Regards
                              </div>
                              <div style="font-weight:bold; font-size:14px; padding:2px 2px;">
                                vSpace Team
                              </div>
                              <div style="font-size:12px;">
                                G-ABLE CO., LTD. <BR>
                                Panjathani Tower 15th Floor, Nonsi Road<BR>
                                Chong Nonsi, Yannawa, Bangkok 10120 THAILAND<BR>
                                Tel. 02-781-9333 Ext.4477
                              </div>
                          </div>
                      </body>
                      </html>';



        // a random hash will be necessary to send mixed content
        $separator = md5(time());

        // carriage return type (RFC)
        $eol = "\r\n";
        //
        // main header (multipart mandatory)
        $headers = "From:vSpace Customer Satisfaction Survey<noreply@vspace.in.th>" . $eol;
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed;  charset=utf-8';boundary=\"" . $separator . "\"" . $eol;
        $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
        $headers .= "This is a MIME encoded message." . $eol;

        // message
        $body = "--" . $separator . $eol;
        $body .= "Content-Type: text/html; charset=\"UTF-8\"" . $eol;
        $body .= "Content-Transfer-Encoding: 8 bit" . $eol;
        $body .= $message. $eol;

        $data['mailto'] = $mailto;
        $data['subject'] = $subject;
        $data['body'] = $body;
        $data['headers'] = $headers;
        $data['sender'] = "-f noreply@vspace.in.th";

        //SEND Mail
        $url = 'https://sosuat.vspace.in.th/apis/v1/service_send_mail/sendMail';
        $send = uatWebService($url,$data);
    //     if (mail($mailto, $subject, $body, $headers, "-f noreply@vspace.in.th")) {
            $response["error"] = false;
            $response["message"] = 'Mail Send';
            echoRespnse(200, $response);
    //     } else {
    //         $response["error"] = true;
    //         $response["message"] = 'Mail Not Send';
    //         echoRespnse(400, $response);
    //     }
      }else {
        $response["error"] = true;
        $response["message"] = 'You have not require field';
        echoRespnse(400, $response);
      }
  }catch(Exception $e){
      echoRespnse(500, $response['message']=$e);
      print_r($e);
  }


};

?>
