<?php
require_once "../../include/WebserviceModel.php";


$getTicketDetailDaily = function() use ($app) {
	try{
		$input = file_get_contents('php://input');
		$data = json_decode($input, true);
		if (!empty($data['token']) || $data['token'] != Null ) {
			if ($data['token'] == '3a8b3ec40f77886e5402ce076215341d') {
			$yesterday =  date('Y-m-d', strtotime("-1day"));
			$obj = new WebServiceModel();
			$result = $obj->getTicketdata($yesterday.' 00:00:00',$yesterday.' 23:59:59');
			if (empty($result)) {
				$response["error"] = false;
				$response["message"] = 'Record Not Found ';
				$response["data"] = $result;
				echoRespnse(200, $response);
			}else{
				$response["error"] = false;
				$response["message"] = 'Get Data Success';
				$response["data"] = $result;
				echoRespnse(200, $response);
			}
			}else {
			$response["error"] = true;
			$response["message"] = 'Unauthorized unknow token';
			echoRespnse(401, $response);
			}
		}else {
			$response["error"] = true;
			$response["message"] = 'Not found token';
			echoRespnse(400, $response);
		}
	}catch(Exception $e){
		echo $e;
		echoRespnse(500, $response['message']=$e);
	}
};

$getSLAdata = function() use ($app) {
	try{
		$input = file_get_contents('php://input');
		$data = json_decode($input, true);
		if (!empty($data['token']) || $data['token'] != Null ) {
			if ($data['token'] == '3a8b3ec40f77886e5402ce076215341d') {
			$yesterday =  date('Y-m-d', strtotime("-1day"));
			$obj = new WebServiceModel();
			$result = $obj->getSLAdata($yesterday.' 00:00:00',$yesterday.' 23:59:59');
			if (empty($result)) {
				$response["error"] = false;
				$response["message"] = 'Record Not Found ';
				$response["data"] = $result;
				echoRespnse(200, $response);
			}else{
				$response["error"] = false;
				$response["message"] = 'Get Data Success';
				$response["data"] = $result;
				echoRespnse(200, $response);
			}
			}else {
			$response["error"] = true;
			$response["message"] = 'Unauthorized unknow token';
			echoRespnse(401, $response);
			}
		}else {
			$response["error"] = true;
			$response["message"] = 'Not found token';
			echoRespnse(400, $response);
		}
	}catch(Exception $e){
		echo $e;
		echoRespnse(500, $response['message']=$e);
	}
};

$getTicketStatusdata = function() use ($app) {
	try{
		$input = file_get_contents('php://input');
		$data = json_decode($input, true);
		if (!empty($data['token']) || $data['token'] != Null ) {
			if ($data['token'] == '3a8b3ec40f77886e5402ce076215341d') {
			$yesterday =  date('Y-m-d', strtotime("-1day"));
			$obj = new WebServiceModel();
			$result = $obj->getTicketStatusdata($yesterday.' 00:00:00',$yesterday.' 23:59:59');
				if (empty($result)) {
					$response["error"] = false;
					$response["message"] = 'Record Not Found ';
					$response["data"] = $result;
					echoRespnse(200, $response);
				}else{
					$response["error"] = false;
					$response["message"] = 'Get Data Success';
					$response["data"] = $result;
					echoRespnse(200, $response);
				}
			}else {
			$response["error"] = true;
			$response["message"] = 'Unauthorized unknow token';
			echoRespnse(401, $response);
			}
		}else {
			$response["error"] = true;
			$response["message"] = 'Not found token';
			echoRespnse(400, $response);
		}
	}catch(Exception $e){
		echo $e;
		echoRespnse(500, $response['message']=$e);
	}
};

$getTicketOwnerdata = function() use ($app) {
	try{
		$input = file_get_contents('php://input');
		$data = json_decode($input, true);
		if (!empty($data['token']) || $data['token'] != Null ) {
			if ($data['token'] == '3a8b3ec40f77886e5402ce076215341d') {
			$yesterday =  date('Y-m-d', strtotime("-1day"));
			$obj = new WebServiceModel();
			$result = $obj->getTicketOwnerdata($yesterday.' 00:00:00',$yesterday.' 23:59:59');
				if (empty($result)) {
					$response["error"] = false;
					$response["message"] = 'Record Not Found ';
					$response["data"] = $result;
					echoRespnse(200, $response);
				}else {
					$response["error"] = false;
					$response["message"] = 'Get Data Success';
					$response["data"] = $result;
					echoRespnse(200, $response);
				}
			}else {
			$response["error"] = true;
			$response["message"] = 'Unauthorized unknow token';
			echoRespnse(401, $response);
			}
		}else {
			$response["error"] = true;
			$response["message"] = 'Token Not Found';
			echoRespnse(400, $response);
		}
	}catch(Exception $e){
		echo $e;
		echoRespnse(500, $response['message']=$e);
	}
};

?>
