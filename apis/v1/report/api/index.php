<?php
require_once "../../include/ServiceReportModel.php";
require_once "../../include/ExportExcel.php";
// require "../../include/PHPMailer/PHPMailerAutoload.php";
require "../../include/ReportModel.php";
require_once __dir__."/../../../include/RoleModel.php";
require_once __dir__."/../../../include/TokenModel.php";
ini_set("memory_limit","-1");
$index = function ($secondChar, $start, $end, $group_name) use ($app) {
	try{
        parse_str($app->request()->getBody(),$request);

        // print_r($secondChar);
        // echo "<pre>";
        // print_r($start);
        // echo "</pre>";
        // echo "<pre>";
        // print_r($end);
        // echo "</pre>";
        // exit();

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;
        $obj = new ServiceReportModel();
        $data = $obj->reportSr($secondChar,$start." 00:00:00",$end." 23:59:00", str_replace("_","/",$group_name));
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exportReportSr($data, $secondChar, $start, $end, $group_name);
		// echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

// $SendEmailRawDataSOSDaily = function () use ($app) {
// 	$this->RawDataSOSDaily()
// };

$getCusSatReport = function () use ($app){
	try {
    $start = $app->request->post('start');
    $end = $app->request->post('end');
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');

    $objReport = new ReportModel();
    $roleModel = new RoleModel();
    $roleModel->email = $email;

				if ((!empty($email) || !empty($token)) && (!empty($start) && !empty($end))){
					$tokenObj = new TokenModel();
					$info = $tokenObj->info($token, $email);
							if(isset($info['role_sid'])) {
								$can_view_report =	$roleModel->checkServicesPermission('permission_view_report');
									if ($can_view_report) {
											$startdatetime = $start.' 00:00:00';
											$enddatetime = $end.' 23:59:59';
											$result = $objReport->getCusSatData($startdatetime,$enddatetime);
											if (count($result)>0) {
													$response["error"] = false;
													$response["message"] = 'Search Success';
													$response['data'] = $result;
													echoRespnse(200, $response);
											}else{
													$response["error"] = false;
													$response["message"] = 'No Data';
													$response['data'] = $result;
													echoRespnse(200, $response);

											}
									}else{
										 $response["error"] = true;
						         $response["message"] = 'You have not permission ';
										 echoRespnse(403, $response);
									}
							}else{
								$response["error"] = true;
								$response["message"] = 'You have Unauthorized';
								echoRespnse(401, $response);
							}

				}else {
					$response["error"] = true;
					$response["message"] = 'You have not require field';
					echoRespnse(400, $response);
				}
	} catch (Exception $e) {
		print_r($e);
		  echoRespnse(500, $response['message']=$e);
	}
};

$getProductCatReport = function () use ($app){
	try {
    $start = $app->request->post('start');
    $end = $app->request->post('end');
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');

    $objReport = new ReportModel();
    $roleModel = new RoleModel();
    $roleModel->email = $email;

				if ((!empty($email) || !empty($token)) && (!empty($start) && !empty($end))){
					$tokenObj = new TokenModel();
					$info = $tokenObj->info($token, $email);
							if(isset($info['role_sid'])) {
								$can_view_report =	$roleModel->checkServicesPermission('permission_view_report');
									if ($can_view_report) {
											$startdatetime = $start.' 00:00:00';
											$enddatetime = $end.' 23:59:59';
											$result = $objReport->getProductCatReport($startdatetime,$enddatetime);
											if (count($result)>0) {
													$response["error"] = false;
													$response["message"] = 'Search Success';
													$response['data'] = $result;
													echoRespnse(200, $response);
											}else{
													$response["error"] = false;
													$response["message"] = 'No Data';
													$response['data'] = $result;
													echoRespnse(200, $response);

											}
									}else{
										 $response["error"] = true;
						         $response["message"] = 'You have not permission ';
										 echoRespnse(403, $response);
									}
							}else{
								$response["error"] = true;
								$response["message"] = 'You have Unauthorized';
								echoRespnse(401, $response);
							}

				}else {
					$response["error"] = true;
					$response["message"] = 'You have not require field';
					echoRespnse(400, $response);
				}
	} catch (Exception $e) {
		print_r($e);
		  echoRespnse(500, $response['message']=$e);
	}
};

$getTimeReport = function () use ($app){
	try {
    $start = $app->request->post('start');
    $end = $app->request->post('end');
		$status = $app->request()->post('status');
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');

    $objReport = new ReportModel();
    $roleModel = new RoleModel();
    $roleModel->email = $email;

				if ((!empty($email) || !empty($token)) && (!empty($start) && !empty($end))){
					$tokenObj = new TokenModel();
					$info = $tokenObj->info($token, $email);
							if(isset($info['role_sid'])) {
								$can_view_report =	$roleModel->checkServicesPermission('permission_view_report');
									if ($can_view_report) {
											$startdatetime = $start.' 00:00:00';
											$enddatetime = $end.' 23:59:59';
											$result = $objReport->getTimeReport($startdatetime,$enddatetime,$status);
											if (count($result)>0) {
													$response["error"] = false;
													$response["message"] = 'Search Success';
													$response['data'] = $result;
													echoRespnse(200, $response);
											}else{
													$response["error"] = false;
													$response["message"] = 'No Data';
													$response['data'] = $result;
													echoRespnse(200, $response);

											}
									}else{
										 $response["error"] = true;
						         $response["message"] = 'You have not permission ';
										 echoRespnse(403, $response);
									}
							}else{
								$response["error"] = true;
								$response["message"] = 'You have Unauthorized';
								echoRespnse(401, $response);
							}

				}else {
					$response["error"] = true;
					$response["message"] = 'You have not require field';
					echoRespnse(400, $response);
				}
	} catch (Exception $e) {
		print_r($e);
		  echoRespnse(500, $response['message']=$e);
	}
};



$getCaseData = function () use ($app){
	try {
    $start = $app->request->post('start');
    $end = $app->request->post('end');
    $status = $app->request->post('status');
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');

    $objReport = new ReportModel();
    $roleModel = new RoleModel();
    $roleModel->email = $email;

				if ((!empty($email) || !empty($token)) && (!empty($start) && !empty($end))){
					$startdatetime = $start.' 00:00:00';
			    $enddatetime = $end.' 23:59:59';
					$tokenObj = new TokenModel();
					$info = $tokenObj->info($token, $email);
							if(isset($info['role_sid'])) {
								$can_view_report =	$roleModel->checkServicesPermission('permission_view_report');
									if ($can_view_report) {
											$result = $objReport->getCaseData($startdatetime,$enddatetime,$status);
											if (count($result)>0) {
													$response["error"] = false;
													$response["message"] = 'Search Success';
													$response['data'] = $result;
													echoRespnse(200, $response);
											}else{
													$response["error"] = false;
													$response["message"] = 'No Data';
													$response['data'] = $result;
													echoRespnse(200, $response);

											}
									}else{
										 $response["error"] = true;
						         $response["message"] = 'You have not permission ';
										 echoRespnse(403, $response);
									}
							}else{
								$response["error"] = true;
								$response["message"] = 'You have Unauthorized';
								echoRespnse(401, $response);
							}

				}else {
					$response["error"] = true;
					$response["message"] = 'You have not require field';
					echoRespnse(400, $response);
				}
	} catch (Exception $e) {
		  echoRespnse(500, $response['message']=$e);
	}
};

$getSumTeamTicket = function () use ($app){
	$objReport = new ReportModel();
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');
    $roleModel = new RoleModel();
    $roleModel->email = $email;
    $tokenObj = new TokenModel();
    $info = $tokenObj->info($token, $email);
    if(!(isset($info['role_sid']) && $roleModel->checkServicesPermission('permission_view_report'))){
        $response["error"] = true;
        $response["message"] = 'Invalid token or permission not granted';
        $response['data'] = [];
    }else{
        $result = $objReport->getSumTeamTicket();
        if (count($result)>0) {
            $response["error"] = false;
            $response["message"] = 'Search Success';
            $response['data'] = $result;
        }else{
            $response["error"] = false;
            $response["message"] = 'No Data';
            $response['data'] = [];
        }
	}
	// echo json response
	echoRespnse(200, $response);
};

$getSumticketPerDaySplitHour = function () use ($app){
		$date = $app->request->post('datesearch');
		$objReport = new ReportModel();
		$email = $app->request()->post('email');
		$token = $app->request()->post('token');
		$roleModel = new RoleModel();
		$roleModel->email = $email;
		$tokenObj = new TokenModel();
		$info = $tokenObj->info($token, $email);
		if(!(isset($info['role_sid']) && $roleModel->checkServicesPermission('permission_view_report'))){
			$response["error"] = true;
			$response["message"] = 'Invalid token or permission not granted';
			$response['data'] = [];
		}else{
            $result = $objReport->getSumticketPerDaySplitHour($date);
            if (count($result)>0) {
                $serv1 = array();
                $serv2 = array();
                $serv3 = array();

                $serv1[$result['0']['DATE']] = 'Severity-1';
                $serv1['06-00'] = $result['0']['serv1_6'];
                $serv1['07-00'] = $result['0']['serv1_7'];
                $serv1['08-00'] = $result['0']['serv1_8'];
                $serv1['09-00'] = $result['0']['serv1_9'];
                $serv1['10-00'] = $result['0']['serv1_10'];
                $serv1['11-00'] = $result['0']['serv1_11'];
                $serv1['12-00'] = $result['0']['serv1_12'];
                $serv1['13-00'] = $result['0']['serv1_13'];
                $serv1['14-00'] = $result['0']['serv1_14'];
                $serv1['15-00'] = $result['0']['serv1_15'];
                $serv1['16-00'] = $result['0']['serv1_16'];
                $serv1['17-00'] = $result['0']['serv1_17'];
                $serv1['18-00'] = $result['0']['serv1_18'];
                $serv1['19-00'] = $result['0']['serv1_19'];
                $serv1['20-00'] = $result['0']['serv1_20'];
                $serv1['21-00'] = $result['0']['serv1_21'];
                $serv1['22-00'] = $result['0']['serv1_22'];
                $serv1['23-00'] = $result['0']['serv1_23'];

                $serv2[$result['0']['DATE']] = 'Severity-2';
                $serv2['06-00'] = $result['0']['serv2_6'];
                $serv2['07-00'] = $result['0']['serv2_7'];
                $serv2['08-00'] = $result['0']['serv2_8'];
                $serv2['09-00'] = $result['0']['serv2_9'];
                $serv2['10-00'] = $result['0']['serv2_10'];
                $serv2['11-00'] = $result['0']['serv2_11'];
                $serv2['12-00'] = $result['0']['serv2_12'];
                $serv2['13-00'] = $result['0']['serv2_13'];
                $serv2['14-00'] = $result['0']['serv2_14'];
                $serv2['15-00'] = $result['0']['serv2_15'];
                $serv2['16-00'] = $result['0']['serv2_16'];
                $serv2['17-00'] = $result['0']['serv2_17'];
                $serv2['18-00'] = $result['0']['serv2_18'];
                $serv2['19-00'] = $result['0']['serv2_19'];
                $serv2['20-00'] = $result['0']['serv2_20'];
                $serv2['21-00'] = $result['0']['serv2_21'];
                $serv2['22-00'] = $result['0']['serv2_22'];
                $serv2['23-00'] = $result['0']['serv2_23'];

                $serv3[$result['0']['DATE']] = 'Severity-3';
                $serv3['06-00'] = $result['0']['serv3_6'];
                $serv3['07-00'] = $result['0']['serv3_7'];
                $serv3['08-00'] = $result['0']['serv3_8'];
                $serv3['09-00'] = $result['0']['serv3_9'];
                $serv3['10-00'] = $result['0']['serv3_10'];
                $serv3['11-00'] = $result['0']['serv3_11'];
                $serv3['12-00'] = $result['0']['serv3_12'];
                $serv3['13-00'] = $result['0']['serv3_13'];
                $serv3['14-00'] = $result['0']['serv3_14'];
                $serv3['15-00'] = $result['0']['serv3_15'];
                $serv3['16-00'] = $result['0']['serv3_16'];
                $serv3['17-00'] = $result['0']['serv3_17'];
                $serv3['18-00'] = $result['0']['serv3_18'];
                $serv3['19-00'] = $result['0']['serv3_19'];
                $serv3['20-00'] = $result['0']['serv3_20'];
                $serv3['21-00'] = $result['0']['serv3_21'];
                $serv3['22-00'] = $result['0']['serv3_22'];
                $serv3['23-00'] = $result['0']['serv3_23'];

                $sum[$result['0']['DATE']] = 'Total SUM';
                $sum['06-00'] = $result['0']['sum_tk6'];
                $sum['07-00'] = $result['0']['sum_tk7'];
                $sum['08-00'] = $result['0']['sum_tk8'];
                $sum['09-00'] = $result['0']['sum_tk9'];
                $sum['10-00'] = $result['0']['sum_tk10'];
                $sum['11-00'] = $result['0']['sum_tk11'];
                $sum['12-00'] = $result['0']['sum_tk12'];
                $sum['13-00'] = $result['0']['sum_tk13'];
                $sum['14-00'] = $result['0']['sum_tk14'];
                $sum['15-00'] = $result['0']['sum_tk15'];
                $sum['16-00'] = $result['0']['sum_tk16'];
                $sum['17-00'] = $result['0']['sum_tk17'];
                $sum['18-00'] = $result['0']['sum_tk18'];
                $sum['19-00'] = $result['0']['sum_tk19'];
                $sum['20-00'] = $result['0']['sum_tk20'];
                $sum['21-00'] = $result['0']['sum_tk21'];
                $sum['22-00'] = $result['0']['sum_tk22'];
                $sum['23-00'] = $result['0']['sum_tk23'];

                $response['data']=array();

                array_push($response['data'],$serv1,$serv2,$serv3,$sum);
                $response["error"] = false;
                $response["message"] = 'Search Success';
            }else{
                $response["error"] = false;
                $response["message"] = 'No Data';
                $response['data'] = [];
            }
		}
		echoRespnse(200, $response);
};

$RawDataSOSDaily = function () use ($app) {
	$startdatetime = date( "Y-m-d", strtotime( "-1day" ) )." 00:00:00";
	$enddatetime = date( "Y-m-d", strtotime( "-1day" ) )." 23:59:59";
$objReportExcel = new ReportExcelModel();
$dataTicket = $objReportExcel->getRawDataTicketDaily($startdatetime, $enddatetime);
$dataServiceReport = $objReportExcel->getRawDataServiceReportDaily($startdatetime, $enddatetime);
require_once '../../../apis/include/PHPExcel/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') . " Create new PHPExcel object\n";
$objPHPExcel = new PHPExcel();

// Set properties
echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("vSpace Team")
							 ->setLastModifiedBy("vSpace Team")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


							 $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('00B33C');
							 $objPHPExcel->getActiveSheet()->getStyle('I1:M1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('FFFF00');
							 $objPHPExcel->getActiveSheet()->getStyle('N1:W1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('00B33C');
							 $objPHPExcel->getActiveSheet()->getStyle('X1:AB1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('CCE6FF');
							 $objPHPExcel->getActiveSheet()->getStyle('AC1:AE1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('00B33C');
							 $objPHPExcel->getActiveSheet()->getStyle('AF1:AH1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('FFBF80');
							 $objPHPExcel->getActiveSheet()->getStyle('AI1:AK1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('BFBFBF');
							 $objPHPExcel->getActiveSheet()->getStyle('AL1:AX1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('00B33C');
							 $objPHPExcel->getActiveSheet()->getStyle('A1:AX1')->getFont()->setBold( true );
							 $objPHPExcel->getActiveSheet()->getStyle('A1:AX1')->getAlignment()->applyFromArray(array('horizontal'
							 => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
							 $objPHPExcel->getActiveSheet()->getDefaultColumnDimension('A1:AX1')->setWidth('18');
							 $objPHPExcel->getActiveSheet()->freezePane('E2');
							 $objPHPExcel->getActiveSheet()->setAutoFilter('A1:AX1');

// Add some data
echo date('H:i:s') . " Add some data\n";
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No.Ticket(Remedy)')
            ->setCellValue('B1', 'Status')
            ->setCellValue('C1', 'Create Ticket Date')
						->setCellValue('D1', 'Last Update Ticket Date')
						->setCellValue('E1', 'Create Ticket User')
            ->setCellValue('F1', 'Subject')
						->setCellValue('G1', 'Description')
						->setCellValue('H1', 'Team')
						->setCellValue('I1', 'End User ID')
						->setCellValue('J1', 'End User Name')
						->setCellValue('K1', 'End User Phone')
						->setCellValue('L1', 'End User Mobile')
						->setCellValue('M1', 'End User Email')
						->setCellValue('N1', 'Case Type')
						->setCellValue('O1', 'Reported Source')
						->setCellValue('P1', 'Urgency')
						->setCellValue('Q1', 'Updated By')
						->setCellValue('R1', 'SLA Remedy')
						->setCellValue('S1', 'Number SR.')
						->setCellValue('T1', 'Department')
						->setCellValue('U1', 'Organization Title')
						->setCellValue('V1', 'Functional Title')
						->setCellValue('W1', 'Asset ID')
						->setCellValue('X1', 'Incident Location')
						->setCellValue('Y1', 'Incident Address')
						->setCellValue('Z1', 'Location ID')
						->setCellValue('AA1', 'Location Type')
						->setCellValue('AB1', 'Location Area')
						->setCellValue('AC1', 'Branch Code')
						->setCellValue('AD1', 'Grade')
						->setCellValue('AE1', 'Vendor')
						->setCellValue('AF1', 'Operational Categorization Tier 1')
						->setCellValue('AG1', 'Operational Categorization Tier 2')
						->setCellValue('AH1', 'Operational Categorization Tier 3')
						->setCellValue('AI1', 'Product Categorization Tier 1')
						->setCellValue('AJ1', 'Product Categorization Tier 2')
						->setCellValue('AK1', 'Product Categorization Tier 3')
						->setCellValue('AL1', 'Product Categorization Product Name')
						->setCellValue('AM1', 'Product Categorization Model Version')
						->setCellValue('AN1', 'Product Categorization Manufacturer')
						->setCellValue('AO1', 'Product Categorization Application Tier')
						->setCellValue('AP1', 'Product Support Period')
						->setCellValue('AQ1', 'Owner Group')
						->setCellValue('AR1', 'Incident Type')
						->setCellValue('AS1', 'Status Reason')
						->setCellValue('AT1', 'Resolution')
						->setCellValue('AU1', 'Severity')
						->setCellValue('AV1', 'Impact')
						->setCellValue('AW1', 'Owner Ticket')
						->setCellValue('AX1', 'Last Worklog');

	$i = '2';

foreach ($dataTicket as $key => $value)
{
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value["refer_remedy_hd"]);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value["status"]);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value["create_datetime"]);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value["update_datetime"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value["create_by"]);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $value["subject"]);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $value["description"]);
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $value["team"]);
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value["refer_remedy_employee_contract_id"]);
	$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $value["end_user_contact_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $value["end_user_phone"]);
	$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $value["end_user_mobile"]);
	$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $value["end_user_email"]);
	$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $value["case_type"]);
	$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $value["reported_source"]);
	$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $value["urgency"]);
	$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $value["updated_by"]);
	$objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $value["sla_remedy"]);
	$objPHPExcel->getActiveSheet()->setCellValue('S' . $i, $value["number_sr"]);
	$objPHPExcel->getActiveSheet()->setCellValue('T' . $i, $value["department"]);
	$objPHPExcel->getActiveSheet()->setCellValue('U' . $i, $value["organization_title"]);
	$objPHPExcel->getActiveSheet()->setCellValue('V' . $i, $value["functional_title"]);
	$objPHPExcel->getActiveSheet()->setCellValue('W' . $i, $value["asset_id"]);
	$objPHPExcel->getActiveSheet()->setCellValue('X' . $i, $value["incident_location"]);
	$objPHPExcel->getActiveSheet()->setCellValue('Y' . $i, $value["incident_address"]);
	$objPHPExcel->getActiveSheet()->setCellValue('Z' . $i, $value["location_id"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AA' . $i, $value["location_type"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AB' . $i, $value["location_area"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AC' . $i, $value["branch_code"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AD' . $i, $value["grade"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AE' . $i, $value["vendor"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AF' . $i, $value["operational_categorization_tier_1"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AG' . $i, $value["operational_categorization_tier_2"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AH' . $i, $value["operational_categorization_tier_3"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AI' . $i, $value["product_categorization_tier_1"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AJ' . $i, $value["product_categorization_tier_2"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AK' . $i, $value["product_categorization_tier_3"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AL' . $i, $value["product_categorization_product_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AM' . $i, $value["product_categorization_model_version"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AN' . $i, $value["product_categorization_manufacturer"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AO' . $i, $value["product_categorization_application_tier"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AP' . $i, $value["product_support_period"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AQ' . $i, $value["owner_group"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AR' . $i, $value["incident_type"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AS' . $i, $value["status_reason"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AT' . $i, $value["solution_detail"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AU' . $i, $value["severity"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AV' . $i, $value["impact"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AW' . $i, $value["owner"]);
	$objPHPExcel->getActiveSheet()->setCellValue('AX' . $i, $value["worklog"]);

	$i++;
}

// Rename sheet
echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('INCIDENT');

$sheet2 = $objPHPExcel->createSheet();

							 $sheet2->getStyle('A1:L1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('00B33C');
							 $sheet2->getStyle('M1:P1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('FFFF00');
							 $sheet2->getStyle('Q1:V1')->getFill()
							 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('00B33C');
							 $sheet2->getStyle('A1:V1')->getFont()->setBold( true );
							 $sheet2->getStyle('A1:V1')->getAlignment()->applyFromArray(array('horizontal'
							 => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
							 $sheet2->getDefaultColumnDimension('A1:V1')->setWidth('18');
							 $sheet2->freezePane('E2');
							 $sheet2->setAutoFilter('A1:V1');

 $sheet2
            ->setCellValue('A1', 'No.Ticket(Remedy)')
            ->setCellValue('B1', 'Service Report No.')
            ->setCellValue('C1', 'Appointment')
						->setCellValue('D1', 'Expect Finish')
						->setCellValue('E1', 'Started Datetime')
            ->setCellValue('F1', 'Closed Datetime')
						->setCellValue('G1', 'Engineer')
						->setCellValue('H1', 'Subject Service Report')
						->setCellValue('I1', 'Create By')
						->setCellValue('J1', 'Create Datetime')
						->setCellValue('K1', 'Associate Engineer')
						->setCellValue('L1', 'Service Type')
						->setCellValue('M1', 'End User Name')
						->setCellValue('N1', 'End User Phone')
						->setCellValue('O1', 'End User Mobile')
						->setCellValue('P1', 'End User Email')
						->setCellValue('Q1', 'Service Report Address')
						->setCellValue('R1', 'File Name Signated')
						->setCellValue('S1', 'Expect Duration')
						->setCellValue('T1', 'Updated By')
						->setCellValue('U1', 'Updated Datetime')
						->setCellValue('V1', 'Path Service Report');

	$i = '2';

foreach ($dataServiceReport as $key => $value)
{
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value["refer_remedy_hd"]);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value["no_task"]);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value["appointment"]);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value["expect_finish"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value["started_datetime"]);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $value["closed_datetime"]);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $value["engineer"]);
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $value["subject_service_report"]);
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value["create_by"]);
	$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $value["create_datetime"]);
	$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $value["associate_engineer"]);
	$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $value["service_type"]);
	$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $value["end_user_contact_name_service_report"]);
	$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $value["end_user_phone_service_report"]);
	$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $value["end_user_mobile_service_report"]);
	$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $value["end_user_email_service_report"]);
	$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $value["service_report_address"]);
	$objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $value["file_name_signated"]);
	$objPHPExcel->getActiveSheet()->setCellValue('S' . $i, $value["expect_duration"]);
	$objPHPExcel->getActiveSheet()->setCellValue('T' . $i, $value["updated_by"]);
	$objPHPExcel->getActiveSheet()->setCellValue('U' . $i, $value["updated_datetime"]);
	$objPHPExcel->getActiveSheet()->setCellValue('V' . $i, $value["path_service_report"]);

	$i++;
}

echo date('H:i:s') . " Rename sheet\n";
$sheet2->setTitle('SERVICE REPORT');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Save Excel 2007 file
$date = date( "Y_m_d", strtotime( "-1day" ) );
echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$pathfile = '../../../report/excel/';
$strFileName = $pathfile."RawData_SOS_$date.xlsx";
$objWriter->save($strFileName);


// Echo memory peak usage
echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
echo date('H:i:s') . " Done writing file.\r\n";
};

	$sendRawDataSOSDaily = function () use ($app) {


						$date = date( "Y_m_d", strtotime( "-1day" ) );

						$filename = "RawData_SOS_$date.xlsx";
						// $filename = "RawData_SOS_2018_04_03.xlsx";

    $path = '../../../report/excel';
    $file = $path . "/" . $filename;


    $mailto = 'thanapong.a@g-able.com, preecha.du@kasikornbank.com, nantisak.b@kasikornbank.com, nattaya.tan@kbtg.tech';
    $subject = 'Daily Raw Data report  ประจำวันที่ '.date( "d/m/Y", strtotime( "-1day" ) );
    $message = '
				<html>
				<head>
				</head>
				<body style="max-width:500px">
						<div style="background-color:rgb(0,188,212); text-align:left; padding:20px 10px;">
								<h1 style="color:#FFFFFF">
								 vSpace
								</h1>
						</div>
						<div style="color:#222222; background-color:#FFFFFF; padding:20px 10px; text-align:left;" >
								<div>เรียน ทุกท่าน</div>
								<div style="padding:0px 10px;">
									นำส่ง  Raw Data Report  ประจำวันที่ '.date( "d/m/Y", strtotime( "-1day" ) ).'
								</div>
								<br><br><br>
						</div>
						<div style="background-color:#FFFFFF; padding:20px 10px; text-align:left; ">
								<div style="color:red; font-size:14px;">
								*E-Mail นี้เป็นระบบอัตโนมัติกรุณาอย่าตอบกลับ
								</div>
						</div>
						<div style="background-color:rgb(0,188,212); color:#FFFFFF; padding:10px 10px;">
								<div style="font-weight:bold; font-size:16px;">
									Best Regards
								</div>
								<div style="font-weight:bold; font-size:14px; padding:2px 2px;">
									vSpace Team
								</div>
								<div style="font-size:12px;">
									G-ABLE CO., LTD. <BR>
									CDG House 10th Floor, Nanglinchi Road<BR>
									Chong Nonsi, Yannawa, Bangkok 10120 THAILAND<BR>
									Tel. (+66) 678-0478 Ext.4476
								</div>
						</div>
				</body>
				</html>';



    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // carriage return type (RFC)
    $eol = "\r\n";

    // main header (multipart mandatory)
    $headers = "From:NoReply<noreply@vspace.in.th>" . $eol;
		$headers = "Cc:nuntaporn.t@g-able.com, rapee.s@g-able.com, anichaya.s@g-able.com, surawach.s@firstlogic.co.th,
								sarinaphat.t@firstlogic.co.th, nanthawat.s@firstlogic.co.th, supakorn.l@firstlogic.co.th" . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
    $headers .= "This is a MIME encoded message." . $eol;

    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/html; charset=\"UTF-8\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8 bit" . $eol;
    $body .= $message. $eol;

		$content = chunk_split(base64_encode(file_get_contents($file)));
		$body .= "--".$separator."\n";
		$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n";
		$body .= "Content-Transfer-Encoding: base64 \n";
		$body .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
		$body .= $content."\n\n";

    //SEND Mail
    if (mail($mailto, $subject, $body, $headers, "-f noreply@vspace.in.th")) {
        echo "mail send ... OK"; // or use booleans here
    } else {
        echo "mail send ... ERROR!";
        print_r( error_get_last() );
    }

};


?>
