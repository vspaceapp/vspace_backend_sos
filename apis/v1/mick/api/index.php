<?php
require_once "../../include/MickModel.php";
//require_once  __dir__."/../../../../crontab/Sla.php";
$index = function () use ($app) {
    parse_str($app->request()->getBody(),$request);
    $response = array();
    $response['request'] = $request;
    // $response['data'] = $data;
    echoRespnse(200, $response);
};

$create = function () use ($app){
    parse_str($app->request()->getBody(),$request);

    $model = new MickModel();

    $name = $model->firstFunc($request);

    $response = array();
    $response['request'] = $request;
    $response['is'] = "create";
    $response['data'] = $name;

    echoRespnse(200, $response);
};

$remedyToVspaceCreateWorklog = function () use($app){
    parse_str($app->request()->getBody(),$request);
    $mickModel = new MickModel();
    $res = $mickModel->testCallSoapSosWorklog(
        $request['ticket_id']
        , $request['Detail_Description']
        , $request['Attachment_Name']
        , base64_decode($request['Attachment_Data'])
        , $request['Attachment_OrigSize']
        , $request['Attachment_2']
        , $request['Attachment_3']
        , $request['Submitter_Email']
    );
    $response['data'] = $res;
    $response['request'] = $request;
    echoRespnse(200, $response);
};

$getAllUser = function () use ($app){
    parse_str($app->request()->getBody(), $request);
    $model = new MickModel();
    $result = $model->getUserByName($request);
    $respone = array();
    $respone['data'] = $result;
    $respone['request'] = $request;
    echoRespnse(200, $respone);
};

$runSla = function () use ($app){
    parse_str($app->request()->getBody(), $request);
    $slaCron = new Sla();
    $response = array();
    $response['error'] = false;
    echoRespnse('200', $response);
};

$updateticket = function () use ($app){
    parse_str($app->request()->getBody(), $request);
    $mickModel = new MickModel();

};

$getBlob = function () use ($app){
  parse_str($app->request()->getBody(), $request);
  $model = new MickModel();
  $result = $model->getBlob();
  $res = array();
  $res['data'] = base64_encode($result['attachment_data']);
  echoRespnse(200, $res);
};

$testWSKBankCreateIncidentCase = function () use ($app){
  parse_str($app->request()->getBody(), $request);
  $model = new MickModel();
  $result = $model->createIncidentCase();
  $res = array();
  $res['data'] = $result;
  echoRespnse(200, $res);
};

$testWSKBankUpdateIncidentCase = function () use ($app){
  //request->post('userName');

  parse_str($app->request()->getBody(), $request);
    $userName = $request['userName'];
    $password = $request['password'];
    $Incident_Number = isset($request['Incident_Number'])?$request['Incident_Number']:null;
    $Status = isset($request['Status'])?$request['Status']:null;
    $Status_Reason = isset($request['Status_Reason'])?$request['Status_Reason']:null;
    $z1D_Status_Reason = isset($request['z1D_Status_Reason'])?$request['z1D_Status_Reason']:null;
    $Assigned_Support_Company = isset($request['Assigned_Support_Company'])?$request['Assigned_Support_Company']:null;
    $Assigned_Support_Organization = isset($request['Assigned_Support_Organization'])?$request['Assigned_Support_Organization']:null;
    $Assigned_Group = isset($request['Assigned_Group'])?$request['Assigned_Group']:null;
    $Assignee = isset($request['Assignee'])?$request['Assignee']:null;
    $Assigned_Group_ID = isset($request['Assigned_Group_ID'])?$request['Assigned_Group_ID']:null;
    $Assignee_Login_ID = isset($request['Assignee_Login_ID'])?$request['Assignee_Login_ID']:null;
    $z1D_Note = isset($request['z1D_Note'])?$request['z1D_Note']:null;
    $Resolution_Category = isset($request['Resolution_Category'])?$request['Resolution_Category']:null;
    $Resolution_Category_Tier_2 = isset($request['Resolution_Category_Tier_2'])?$request['Resolution_Category_Tier_2']:null;
    $Resolution_Category_Tier_3 = isset($request['Resolution_Category_Tier_3'])?$request['Resolution_Category_Tier_3']:null;
    $Resolution = isset($request['Resolution'])?$request['Resolution']:null;
    $z1D_Action = isset($request['z1D_Action'])?$request['z1D_Action']:null;
    $z1D_Char01 = isset($request['z1D_Char01'])?$request['z1D_Char01']:null;

  $model = new MickModel();
  $result = $model->updateIncidentCase(
    $userName, $password, $Incident_Number, $Status, $Status_Reason, $z1D_Status_Reason,
    $Assigned_Support_Company, $Assigned_Support_Organization,$Assigned_Group,
    $Assignee,$Assigned_Group_ID,$Assignee_Login_ID,$z1D_Note,$Resolution_Category,
    $Resolution_Category_Tier_2,$Resolution_Category_Tier_3,$Resolution,$z1D_Action,$z1D_Char01
  );
  $res = array();
  $res['data'] = $result;
  $res['request'] = $request;
  echoRespnse(200, $res);
};

$testGetTicketDetail = function () use ($app){
    require_once __dir__.'/../../include/ProjectModel.php';
    $request = $app->request();
    $refer_remedy_hd = $request->post('refer_remedy_hd');
    $projectModel = new ProjectModel();
    $res = array();
    $res['data'] = $projectModel->ticketDetail($refer_remedy_hd);
    $res['error'] = false;
    echoRespnse(200, $res);
};



$findSlaRule = function () use ($app){
//    require_once __dir__.'/../../../include/SlaModel.php';
//    $slaModel = new SlaModel();
//    $data = $slaModel->listTicketStatusZero();
//    $allSla = array();
//    foreach ($data as $key => $value){
//
//        try{
//            $sla = $slaModel->findSlaRule($value);
//            array_push($allSla, $sla);
//        }catch (Exception $e){
//            echo "error ".$e->getMessage();
//            echo "<br/>";
//        }
//    }
//    $request = $app->request();
//    $res['data'] = $data;
//    $res['sla'] = $allSla;
    echoRespnse(200, "");
};

$quickTest  = function () use ($app){
    //$data = $app->request->post('para1');
    require_once __dir__."/../../../include/db_connect.php";
    $dbConnect = new DbConnect();
    $db = $dbConnect->connect();
    $data['data1'] = "data1";
    $data['data2'] = "data2";
    try{
        $sql = "INSERT INTO response_remedy (response_data, create_datetime, other, create_by)
                        VALUE (:response_data, NOW(), :other, :create_by)";
        $stmt = $db->prepare($sql);
        $data = json_encode($data);
        $stmt->execute(
            array(
                ":response_data"=>$data,
                ":other"=>"test",
                ":create_by"=>"test"
            )
        );
        echoRespnse(200, $data);
    }catch(Exception $e){
        echo $e->getMessage();
    }

};

$testWSKBankCreateUpdateWorklog = function () use ($app){
//  $data = $app->request->post('z2AF_Work_Log01_attachmentData');
//  //echo $data;
//  // $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
//
//  $decoded = base64_decode($data);
//  $file = 'img_base64.jpg';
//  file_put_contents($file, $decoded);
//  if (file_exists($file)) {
//      header('Content-Description: File Transfer');
//      header('Content-Type: application/octet-stream');
//      header('Content-Disposition: attachment; filename="'.basename($file).'"');
//      header('Expires: 0');
//      header('Cache-Control: must-revalidate');
//      header('Pragma: public');
//      header('Content-Length: ' . filesize($file));
//      //readfile($file);
//      echo $file;
//      //exit;
//  }
  //echoRespnse(200, $app->request->post('z2AF_Work_Log01_attachmentData'));
  // echo '<img src="'.$data.'">';
  //echoRespnse(200, $app->request->post('z2AF_Work_Log01_attachmentData'));
   parse_str($app->request()->getBody(), $request);
   $userName = $request['userName'];
   $password = $request['password'];

   $Incident_Number = isset($request['Incident_Number'])?$request['Incident_Number']:null;
   $Summary = isset($request['Summary'])?$request['Summary']:null;
   $Note = isset($request['Note'])?$request['Note']:null;
   $Work_Info_Type = isset($request['Work_Info_Type'])?$request['Work_Info_Type']:null;
   $z2AF_Work_Log01_attachmentName = isset($request['z2AF_Work_Log01_attachmentName'])?$request['z2AF_Work_Log01_attachmentName']:null;
   $z2AF_Work_Log01_attachmentData = isset($request['z2AF_Work_Log01_attachmentData'])?$request['z2AF_Work_Log01_attachmentData']:null;
   $z2AF_Work_Log01_attachmentOrigSize = isset($request['z2AF_Work_Log01_attachmentOrigSize'])?$request['z2AF_Work_Log01_attachmentOrigSize']:null;
   $z2AF_Work_Log02_attachmentName = isset($request['z2AF_Work_Log02_attachmentName'])?$request['z2AF_Work_Log02_attachmentName']:null;
   $z2AF_Work_Log02_attachmentData = isset($request['z2AF_Work_Log02_attachmentData'])?$request['z2AF_Work_Log02_attachmentData']:null;
   $z2AF_Work_Log02_attachmentOrigSize = isset($request['z2AF_Work_Log02_attachmentOrigSize'])?$request['z2AF_Work_Log02_attachmentOrigSize']:null;
   $z2AF_Work_Log03_attachmentName = isset($request['z2AF_Work_Log03_attachmentName'])?$request['z2AF_Work_Log03_attachmentName']:null;
   $z2AF_Work_Log03_attachmentData = isset($request['z2AF_Work_Log03_attachmentData'])?$request['z2AF_Work_Log03_attachmentData']:null;
   $z2AF_Work_Log03_attachmentOrigSize = isset($request['z2AF_Work_Log03_attachmentOrigSize'])?$request['z2AF_Work_Log03_attachmentOrigSize']:null;
   $Secure_Work_Log = isset($request['Secure_Work_Log'])?$request['Secure_Work_Log']:null;
   $View_Access = isset($request['View_Access'])?$request['View_Access']:null;

   $model = new MickModel();
//    $result = $model->createUpdateWorklog(
//        "sos.vspace",
//        "password",
//        "INC000000037438",
//        "test summary",
//        "test send worklog",
//        "General Information",
//        "",
//        "",
//        "",
//        "",
//        "",
//        "",
//        "",
//        "",
//        "",
//        "Yes",
//        "Internal"
//    );

   $result = $model->createUpdateWorklog(
     $userName,
     $password,
     $Incident_Number,
     $Summary,
     $Note,
     $Work_Info_Type,
     $z2AF_Work_Log01_attachmentName,
     $z2AF_Work_Log01_attachmentData,
     $z2AF_Work_Log01_attachmentOrigSize,
     $z2AF_Work_Log02_attachmentName,
     $z2AF_Work_Log02_attachmentData,
     $z2AF_Work_Log02_attachmentOrigSize,
     $z2AF_Work_Log03_attachmentName,
     $z2AF_Work_Log03_attachmentData,
     $z2AF_Work_Log03_attachmentOrigSize,
     $Secure_Work_Log,
     $View_Access
   );
   $res = array();
   $res['data'] = $result;
   echoRespnse(200, $res);
};

?>
