<?php
ob_start('ob_gzhandler');
error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ProjectModel.php';
require_once '../../include/PlanningModel.php';
require_once '../../include/ERequestModel.php';
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development'
));
foreach (glob("api/*.php") as $filename){require_once ($filename);}

$app->post('/', $index);
$app->post('/detail', $detail);
$app->post('/create', $create);
$app->post('/checkPermissionCompany', $checkPermissionCompany);
$app->post('/checkSale', $checkSale);
$app->post('/changeContract', $changeContract);
$app->post('/editManHour', $editManHour);
$app->post('/projectSavePlan', $projectSavePlan);
$app->post('/projectSendPlan', $projectSendPlan);
$app->post('/projectPlanOpenedCase', $projectPlanOpenedCase);
$app->post('/editPeriod', $editPeriod);
$app->post('/deleteProject', $deleteProject);
$app->post('/closeProject', $closeProject);
$app->post('/editAddress', $editAddress);
$app->post('/changeOwner', $changeOwner);
$app->post('/projectEditMgrOwnner', $projectEditMgrOwnner);
$app->post('/approvePlan', $approvePlan);
$app->post('/rejectPlan', $rejectPlan);

$app->post('/project/:sid', function($project_sid) use ($app){
    $obj = new ProjectModel();
    $data = $obj->projectData($project_sid);
    $response["error"] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

$app->post('/plan_created', function() use ($app){
    $response = array();
    $response['req']['data'] = $app->request->post('data');
    $response['req']['email'] = $app->request->post('email');
    $data = "";
    $obj = new ProjectModel();
    $data = $obj->planCreate($response['req']['data'], $response['req']['email']);
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});
$app->get('/executeUpdateManDays', function() use ($app){
    $obj = new ProjectModel();
    $data = $obj->executeUpdateManDays();
    $response["error"] = false;
    $response["data"] = $data;
    // echoRespnse(200, $response);
});
$app->post('/lists/:type',function($type) use ($app){
    // verifyRequiredParams(array('day'));
    $search = $app->request->post('search');
    $email = $app->request->post('email');
    $owner = $app->request->post('owner');
    $limit = $app->request->post('limit');
    $obj = new ProjectModel();

    $data = $obj->lists($owner, $type,$search, $email,$limit);
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/fromeServiceRequest', function(){

    $obj = new ERequestModel();
    $data = $obj->listeRequest();
    $response = array();

    $response['error'] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});


$app->post('/listCase', function() use ($app){
    $contract_no = $app->request->post('contract_no');
    $project_sid = $app->request->post('project_sid');

    $obj = new ProjectModel();
    $data = $obj->listCaseOfContract($contract_no);
    $engineer = array();
    $timesTotal = 0;
    foreach ($data as $key => $value) {
        foreach ($value['task'] as $k => $v) {
            foreach ($v['timestamp'] as $kk => $vv) {
                $timesTotal++;
                if(!in_array($vv['engineer'], $engineer)){
                    // $temp = array();
                    $temp = array('engineer'=>$vv['engineer'],'use_minutes'=>$vv['use_minutes'],'thainame'=>$vv['thainame'],'pic'=>$vv['pic'],
                        'times'=>1,
                        'taxi'=>($vv['taxi_fare_1']+$vv['taxi_fare_2'])
                        );
                    if(!repeatEngineer($engineer,$vv['engineer'])){
                        array_push($engineer, $temp);
                    }else{
                        $engineer = updateUseMinute($engineer, $temp['engineer'], $vv['use_minutes'], $vv);
                    }
                }
            }
        }
    }
    $project_detail = $obj->projectDetail($project_sid);

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    $project_detail['timesTotal'] = $timesTotal;
    $response['project_detail'] = $project_detail;
    $response['engineer'] = $engineer;
    echoRespnse(200, $response);
});
function repeatEngineer($engineerArray, $engineer){
    foreach ($engineerArray as $key => $value) {
        if($value['engineer']==$engineer){
            return true;
        }
    }
    return false;
}
function updateUseMinute($engineerArray, $engineer, $useMinutes, $data){
    foreach ($engineerArray as $key => $value) {
        if($value['engineer']==$engineer){
            $engineerArray[$key]['use_minutes']+= $useMinutes;
            $engineerArray[$key]['times']++;
            $engineerArray[$key]['taxi'] +=($data['taxi_fare_1']+$data['taxi_fare_2']);
        }
    }
    return $engineerArray;
}

$app->post('/engineer', function() use ($app){
    $team = $app->request->post('team');
    $email = $app->request->post('email');
    if($email=="sirirat.c@firstlogic.co.th"){
        $team .= ",4";
    }
    if($team!=""){
        $team .= ",8,4";
    }else{
        $team .= "8,4";
    }

    $obj = new PlanningModel();
    if($team!="")
        $data = $obj->engineer($team);
    else
        $data = $obj->engineer();

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/ProjectCreate',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $data = json_decode($app->request->post('data') ,true);
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $obj = new ProjectModel();
    $obj->createProject1($data,$email);

    $response['error'] = false;
    $response['data'] = $data;
    $response['message'] = "Done";
    echoRespnse(200,$response);
});


$app->post('/CreateProject1',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $data = $app->request->post('data');
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $obj = new ProjectModel();
    $obj->createProject1($data,$email);

    $response['error'] = false;
    $response['data'] = $data;
    $response['message'] = "Done";
    echoRespnse(200,$response);
});
$app->post('/createProject', function() use ($app){
    verifyRequiredParams(array('email','token'));
    $data = $app->request->post('data');
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $obj = new ProjectModel();
    $obj->createProject($data,$email);

    $response['error'] = false;
    $response['data'] = $data;
    echoRespnse(200,$response);
});
$app->post('/addproject', function() use ($app){

    verifyRequiredParams(array('email','contract','company','project_name'));
    $email = $app->request->post('email');
    $contract = $app->request->post('contract');
    $company = $app->request->post('company');
    $project_name = $app->request->post('project_name');
    $vendor = $app->request->post('vendor');
    $project_type = $app->request->post('project_type');
    // $owner = $app->request->post('owner');
    // $start = $app->request->post('start');
    // $end = $app->request->post('end');
    // $man_day = $app->request->post('man_day');
    $project_sid = $app->request->post('project_sid');
    $data_status = $app->request->post('data_status');
    $prime_contract = $app->request->post('prime_contract');
    $end_user = $app->request->post('end_user');
    // $scope = $app->request->post('scope');
    $projectOwner = $app->request->post('projectOwner');

    // print_r($projectOnwer);

    $obj = new ProjectModel();
    $data = $obj->executeProject($email,$contract,$company,$project_name,$vendor,$project_type,$project_sid, $data_status, $prime_contract, $end_user,$projectOwner);


    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/setprojecttype', function() use ($app){
    verifyRequiredParams(array('project_sid', 'email', 'project_type'));
    $project_sid = $app->request->post('project_sid');
    $project_type = $app->request->post('project_type');
    $email = $app->request->post('email');


    $obj = new ProjectModel();
    $data =$obj->setProjectType($project_sid, $project_type, $email);

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/setmandayprojectimplement',function() use ($app){
    verifyRequiredParams(array('project_sid','engineer','man_day','start_project','end_project','email'));
    $project_sid = $app->request->post('project_sid');
    $engineer = $app->request->post('engineer');
    $man_day = $app->request->post('man_day');
    $start_project = $app->request->post('start_project');
    $end_project = $app->request->post('end_project');
    $email = $app->request->post('email');


    $obj = new ProjectModel();
    $data =$obj->setMandayProjectImplement($project_sid, $engineer, $man_day, $start_project, $end_project, $email);

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/plan', function() use ($app){
    verifyRequiredParams(array('project_sid','contract_no'));

    $project_sid = $app->request->post('project_sid');
    $contract_no = $app->request->post('contract_no');

    $obj = new ProjectModel();
    $data = $obj->plan($project_sid, $contract_no);

    $response["error"] = false;
    $response["data"] = $data;

    echoRespnse(200, $response);
});

$app->post('/planProject', function() use ($app){
    verifyRequiredParams(array('project_sid','contract_no'));

    $project_sid = $app->request->post('project_sid');
    $contract_no = $app->request->post('contract_no');

    $obj = new ProjectModel();
    $data = $obj->planProject($project_sid, $contract_no);

    $response["error"] = false;
    $response["data"] = $data;

    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;

//     verifyRequiredParams(array('gcm_registration_id'));

//     $gcm_registration_id = $app->request->put('gcm_registration_id');

//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);

//     echoRespnse(200, $response);
// });


/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
