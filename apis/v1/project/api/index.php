<?php
require_once "../../include/TokenModel.php";
require_once '../../include/ProjectModel.php';
require_once '../../include/ProjectModifyModel.php';
// require_once "../../include/"
$index = function () use ($app) {

    parse_str($app->request()->getBody(),$request);
    $projectModel = new ProjectModel();
    $tokenObj = new TokenModel();

    $data = array();

    $info = $tokenObj->info($request['token'], $request['email']);
    if(isset($info['role_sid'])){
        $permission = $tokenObj->permissionProject($info['role_sid']);
        $data = $projectModel->projects($permission, $request['email'], $request['status'], $request['search'], $request);
    }else{
        $res = array();
    }

    $response = array();
    $response['request'] = $request;
    $response['data'] = $data;
    echoRespnse(200, $response);
};
$detail = function () use ($app) {
    parse_str($app->request()->getBody(),$request);

    $projectModel = new ProjectModel();
    $data = array();
    if(isset($request['project_sid'])){
      $data = $projectModel->projectDetail2($request['project_sid'], $request['email']);
    }
    $response = array();
    $response['request'] = $request;
    $response['data'] = $data;
    echoRespnse(200, $response);
};
$create = function () use ($app) {
    parse_str($app->request()->getBody(),$data);

    $response = array();
    $response['request'] = $data;
    // $response['res'] = $res;
    echoRespnse(200, $response);
};
$editManHour = function () use ($app) {
    parse_str($app->request()->getBody(), $request);

    $response = array();
    $response['request'] = $request;
    $projectModel = new ProjectModel();
    $projectModel->editManHour($request['project_sid'], $request['man_hours'], $request['email']);
    echoRespnse(200, $response);
};

$projectSavePlan = function () use ($app) {
    parse_str($app->request()->getBody(), $request);

    $response = array();
    $response['request'] = $request;
    $projectModel = new ProjectModel();
    $projectModel->projectSavePlan($request);
    echoRespnse(200, $response);
};

$projectSendPlan = function () use ($app) {
    parse_str($app->request()->getBody(), $request);

    $response = array();
    $response['request'] = $request;
    $projectModel = new ProjectModel();
    $projectModel->projectSendPlan($request);
    echoRespnse(200, $response);
};

$projectPlanOpenedCase = function () use ($app) {
    parse_str($app->request()->getBody(), $request);

    $response = array();
    $response['request'] = $request;
    $projectModel = new ProjectModel();
    $projectModel->projectPlanOpenedCase($request);
    echoRespnse(200, $response);
};


$checkPermissionCompany = function() use ($app){
    parse_str($app->request()->getBody(),$request);

    $tokenObj = new TokenModel();

    $info = $tokenObj->info($request['token'], $request['email']);
    if(isset($info['email']) && $info['email']!=""){
        $empno = $tokenObj->getEmpno($info['email']);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://flgupload.com/checkPermissionDummyContract.php?empno=".$empno,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: fe796863-62fd-609f-af1f-6456240f4a78"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $data = json_decode($response, true);
            // print_r($data);
            $response = array();
            $response['data'] = $data;
            // $response['res'] = $res;
            echoRespnse(200, $response);
        }
    }else{
        $response = array();
        echoRespnse(404, $response);
    }
};

$checkSale = function() use ($app){
    parse_str($app->request()->getBody(),$request);

    if(isset($request['company'])){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://flgupload.com/CheckSale.php?company=".$request['company'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: e03227e6-3397-8667-b8b8-4455c84029b2"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $data = json_decode($response, true);
            // print_r($data);
            $response = array();
            $response['data'] = $data;
            // $response['res'] = $res;
            echoRespnse(200, $response);
        }
    }
};

$changeContract = function() use ($app){
    parse_str($app->request()->getBody(),$request);

    $response = array();
    $response['request'] = $request;


    $projectModel = new ProjectModel();
    if(isset($request['email']) && isset($request['project_sid']) && isset($request['newContract']) && isset($request['projectName']) &&
    isset($request['enduser']) && isset($request['enduser_address']) && isset($request['customer'])){

      $projectModel->addNewContractToProjectContractQuery($request['email'], $request['project_sid']);
      $response['data'] = $projectModel->updateContractToProjectQuery(
          $request['email'],
          $request['project_sid'],
          $request['newContract'],
          $request['projectName'],
          $request['enduser'],
          $request['enduser_address'],
          $request['customer']
          );
    }else{
      $response['data'] = array('status'=>0,'message'=>'ข้อมูลไม่ครบ');
    }

    // $response['data'] = $data;
    echoRespnse(200, $response);
};

$editPeriod = function() use ($app){
    parse_str($app->request()->getBody(), $request);
    $projectModifyModel = new ProjectModifyModel();
    $data = $projectModifyModel->editPeriod($request);

    $response = array();
    $response['error'] = false;
    $response['request'] = $request;
    $response['data'] = $data;
    echoRespnse(200, $response);
};
$editAddress = function() use ($app){
    parse_str($app->request()->getBody(), $request);
    $projectModifyModel = new ProjectModifyModel();
    $projectModifyModel->editAddress($request);

    $response = array();
    $response['error'] = false;
    $response['request'] = $request;
    echoRespnse(200, $response);
};

$deleteProject = function() use ($app){
    parse_str($app->request()->getBody(), $request);

    $projectModel = new ProjectModel();
    $message = $projectModel->deleteProject($request);

    $response = array();
    $response['error'] = false;
    $response['request'] = $request;

    $response['message'] = $message;

    echoRespnse(200, $response);
};

$closeProject = function() use ($app){
    parse_str($app->request()->getBody(), $request);

    $projectModifyModel = new ProjectModifyModel();
    $message = $projectModifyModel->closeProject($request['project_sid'], $request['email'], $request['remark_close_project']);

    $response = array();
    $response['error'] = false;
    $response['request'] = $request;

    $response['message'] = $message;

    echoRespnse(200, $response);
};

$changeOwner = function() use ($app){
    parse_str($app->request()->getBody(), $request);

    $response = array();
    $response['error'] = false;
    $response['request'] = $request;

    $email = $request['email'];
    $token = $request['token'];

    $project_sid = $request['project_sid'];
    $new_staff_email = $request['new_staff_email'];

    $obj = new ProjectModifyModel();
    $response['data'] = $obj->changeOwner($email, $project_sid, $new_staff_email);

    echoRespnse(200, $response);
};

$projectEditMgrOwnner = function() use ($app){
    parse_str($app->request()->getBody(), $request);

    $response = array();
    $response['error'] = false;
    $response['request'] = $request;

    $email = $request['email'];
    $token = $request['token'];

    $project_sid = $request['project_sid'];
    $new_staff_email = $request['new_staff_email'];

    $obj = new ProjectModifyModel();
    $response['data'] = $obj->changeMgrOwner($email, $project_sid, $new_staff_email);

    echoRespnse(200, $response);
};

$approvePlan = function() use ($app){
    parse_str($app->request()->getBody(), $request);
    $projectModifyModel = new ProjectModifyModel();
    $message = $projectModifyModel->approvePlan($request['project_sid'], $request['email']);
    $response = array();
    $response['error'] = false;
    $response['request'] = $request;
    $response['message'] = $message;
    echoRespnse(200, $response);
};

$rejectPlan = function() use ($app){
    parse_str($app->request()->getBody(), $request);

    $projectModifyModel = new ProjectModifyModel();
    $message = $projectModifyModel->rejectPlan($request['project_sid'], $request['email'], $request['reason_reject_plan']);
    $response = array();
    $response['error'] = false;
    $response['request'] = $request;
    $response['message'] = $message;

    echoRespnse(200, $response);
};
?>
