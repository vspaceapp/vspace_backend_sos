<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/CaseModel.php';
require_once '../../include/GenNoCaseSR.php';
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/listCaseType', function() use ($app){

    $response['error'] = false;

    $response['data'] = array(
        // array('name'=>'Incident','href'=>'CaseInputSerialContract'),
        // array('name'=>'Question','href'=>'CaseInputSerialContract'),
        array('name'=>'Request','href'=>'CaseInputSerialContract'),
        array('name'=>'Implement', 'href'=>'CaseProject'),
        array('name'=>'Install','href'=>'CaseProject'),
        array('name'=>'Preventive Maintenance','href'=>'CaseInputSerialContract'),
        array('name'=>'Learning','href'=>'CaseProject')
        );
    echoRespnse(200, $response);
});

$app->post('/create', function() use ($app){
    verifyRequiredParams(array('email','subject','case_type'));
    $email = $app->request->post('email');
    // $token = $app->request->post('token');
    // $requester_name = $app->request->post('requester_name');
    // $requester_phone = $app->request->post('requester_phone');
    // $requester_email = $app->request->post('requester_email');
    // $requester_mobile = $app->request->post('requester_mobile');
    // $requester_company = $app->request->post('requester_company');
    // $end_user_name = $app->request->post('end_user_name');
    // $end_user_phone = $app->request->post('end_user_phone');
    // $end_user_email = $app->request->post('end_user_email');
    // $end_user_mobile = $app->request->post('end_user_mobile');
    // $end_user_company = $app->request->post('end_user_company');
    // $serial = $app->request->post('serial');
    // $contract = $app->request->post('contract');
    // $prime_contract = $app->request->post('prime_contract');
    // $note = $app->request->post('note');
    $subject = $app->request->post('subject');
    // $description = $app->request->post('description');
    // $case_company = $app->request->post('case_company');
    // $case_address = $app->request->post('case_address');
    $case_type = $app->request->post('case_type');
    // $urgency = $app->request->post('urgency');
    // $email = $app->request->post('email');

    $response = array();

    $obj = new CaseModel();
    // $data = $obj->executeCreateCaseTest($subject, $description, $case_company, $case_address, $case_type, $urgency, $serial, $contract, $prime_contract, $note,$requester_name,$requester_phone,$requester_email,$requester_mobile,$requester_company,$end_user_name,$end_user_phone,$end_user_email,$end_user_mobile,$end_user_company,$email);
    $data = $obj->testCreateCase($subject,$case_type,$email);
    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/changeAppointment',function() use ($app){
    
    verifyRequiredParams(array('email','task_sid','changeAppointmentDate','changeAppointmentTime'));
    $email = $app->request->post('email');
    $task_sid = $app->request->post('task_sid');
    $changeAppointmentDate = $app->request->post('changeAppointmentDate');
    $changeAppointmentTime = $app->request->post('changeAppointmentTime');
    $token = $app->request->post('token');

    $obj = new CaseModel();
    $data = $obj->changeAppointment($email, $changeAppointmentDate, $changeAppointmentTime, $task_sid);
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    $n = 0;
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $field = str_replace("_", " ", $field);
            $error_fields .= '- ' .ucfirst($field) . '<br/>';
            $n++;
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s)<br/>' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>