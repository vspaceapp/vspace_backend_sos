<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/FCM.php';
require_once '../../include/SendMail.php';
require_once '../../include/db_handler.php';
require_once '../../include/ProjectModel.php';
require_once '../../include/CaseModel.php';
require_once '../../include/UserModel.php';
require_once '../../include/PlanningModel.php';
require_once '../../include/LocationModel.php';
require_once '../../include/ServiceType.php';
require_once '../../include/TokenModel.php';
require_once '../../include/SlaModel.php';
require_once '../../include/BugReportModel.php';
require_once '../../view/MoreSystemView.php';
require_once '../../view/MoreSystemViewKaew.php';
require_once '../../view/SparePartView.php';
require_once '../../view/ServiceReportCreateView.php';
require_once '../../include/ReportEngineerModel.php';

require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/reportEngineerService',function() use ($app){
    
    $by = $app->request->post('by');
    $token = $app->request->post('token');
    $email_engineer = $app->request->post('email_engineer');
    $problem = $app->request->post('type_report');
    $solution = $app->request->post('solution');
    $other = $app->request->post('other');

    $obj = new ReportEngineerModel();    
    $data = $obj->addReport( $email_engineer, $problem, $solution, $other, $by);

    $response = array();
    $response["error"] = false;
    // $response["template"] = $template;
    // $response["data"] = $data;
    echoRespnse(200, $response);
});


$app->post('/loadReportEngineerService',function() use ($app){
    
    $by = $app->request->post('by');
    $token = $app->request->post('token');
    $email_engineer = $app->request->post('email_engineer');

    $obj = new ReportEngineerModel();    
    $data = $obj->selectReport( $email_engineer);

    $response = array();
    $response["error"] = false;
    // $response["template"] = $template;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });



/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>