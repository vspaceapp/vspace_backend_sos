<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ProjectModel.php';
require_once '../../include/CaseModel.php';
require_once '../../include/PlanningModel.php';
require_once '../../include/FunctionImplement.php';
require_once '../../include/GenNoCaseSR.php';

require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
$myProject = array();
$app = new \Slim\Slim();

$app->post('/lists',function() use ($app){
    
    // verifyRequiredParams(array('day'));
    $email = $app->request->post('email');
    $obj = new CaseModel();
    $data = $obj->lists('Install', $email);
    
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/project',function() use ($app){
    global $myProject;
    verifyRequiredParams(array('email'));
    $project_sid = $app->request->post('project_sid');
    $email = $app->request->post('email');
    
    $response["error"] = false;
    implementList($email);
    
    // $data = $project_sid;
    $response['data'] = $myProject;
    // echo json response
    echoRespnse(200, $response);
});

//สำหรับสร้าง เคส และ เอสอาร์ ไม่ใช่อาร์สยามนะ 
//1 validate
//2 create case
//3 create sr
$app->post('/createCaseAndSr', function() use ($app){
    verifyRequiredParams(array('contract_no','email','subject','end_user_contact_name','end_user_site','end_user_email','type_service','appointment_date','appointment_time','expect_time'));
    $project = $app->request->post('contract_no'); // ควรเช็คก่อน
    $appointment_time = $app->request->post('appointment_time'); 
    $appointment_date = $app->request->post('appointment_date');
    $engineer = $app->request->post('engineer');
    $subject = $app->request->post('subject');
    $description = $app->request->post('description');
    $end_user_site = $app->request->post('end_user_site');
    $end_user_contact_name = $app->request->post('end_user_contact_name');
    $end_user_phone = $app->request->post('end_user_phone');
    $end_user_mobile = $app->request->post('end_user_mobile');
    $end_user_email = $app->request->post('end_user_email');
    $expect_time = $app->request->post('expect_time');
    $email = $app->request->post('email');
    $type_service = $app->request->post('type_service');
    $case_company = $app->request->post('case_company');
    
    $end_user_company_name = $app->request->post('end_user_company_name');

    $subject_service_report = $app->request->post('subject_service_report');

    $obj = new CaseModel();
    
    $obj->executeCreateCaseInstall($project,$appointment_time,$appointment_date,$engineer,$subject,$description,$end_user_site,$end_user_contact_name,$end_user_phone,$end_user_mobile,$end_user_email,$expect_time,$email,$type_service,$case_company,$subject_service_report,$end_user_company_name);

    $response["error"] = false;
    echoRespnse(200, $response);
});

$app->post('/createtask', function() use ($app){
    verifyRequiredParams(array('appointment_date','appointment_time','engineer','expect_time','type_service'));
    
    $appointment_date = $app->request->post('appointment_date'); // ควรเช็คก่อน
    $appointment_time = $app->request->post('appointment_time'); 
    $engineer = $app->request->post('engineer');
    $expect_time = $app->request->post('expect_time');
    $type_service = $app->request->post('type_service');
    $ticket_sid = $app->request->post('ticket_sid');
    $email = $app->request->post('email');

    $obj = new CaseModel();
    
    $obj->executeCreateTaskInstall($appointment_date,$appointment_time,$engineer,$expect_time,$type_service,$ticket_sid, $email);

    $response["error"] = false;
    echoRespnse(200, $response);
});


$app->post('/lists',function() use ($app){
    // verifyRequiredParams(array('day'));
    // $day = $app->request->post('day');
    $obj = new ProjectModel();
    $data = $obj->lists();
    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/engineer', function() use ($app){
    $team = $app->request->post('team');

    $obj = new PlanningModel();
    if($team!="")
        $data = $obj->engineer($team);
    else
        $data = $obj->engineer();

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/addcase', function() use ($app){

    verifyRequiredParams(array('data', 'email'));
    $data = $app->request->post('data');
    $email = $app->request->post('email');
    $data = json_decode($data, true);

    $obj = new ProjectModel();

    foreach ($data as $key => $value) {
        $obj->addProject($value, $email);
    }
    // $obj = new PlanningModel();
    // $data = $obj->engineer();

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/setprojecttype', function() use ($app){
    verifyRequiredParams(array('project_sid', 'email', 'project_type'));
    $project_sid = $app->request->post('project_sid');
    $project_type = $app->request->post('project_type');
    $email = $app->request->post('email');

    
    $obj = new ProjectModel();
    $data =$obj->setProjectType($project_sid, $project_type, $email);

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/setmandayprojectimplement',function() use ($app){
    verifyRequiredParams(array('project_sid','engineer','man_day','start_project','end_project','email'));
    $project_sid = $app->request->post('project_sid');
    $engineer = $app->request->post('engineer');
    $man_day = $app->request->post('man_day');
    $start_project = $app->request->post('start_project');
    $end_project = $app->request->post('end_project');
    $email = $app->request->post('email');
    

    $obj = new ProjectModel();
    $data =$obj->setMandayProjectImplement($project_sid, $engineer, $man_day, $start_project, $end_project, $email);

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $field = str_replace("_", " ", $field);
            $error_fields .= ucfirst($field) . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>