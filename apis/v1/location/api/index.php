<?php
require_once "../../include/TokenModel.php";
require_once "../../include/Location_Model.php";
require_once __dir__."/../../../include/RoleModel.php";

$index = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new TokenModel();

        $info = $obj->info($request['token'], $request['email']);

        $response = array();
        $response['request'] = $request;
        $roleModel = new RoleModel();
        $roleModel->email = $request['email'];
        $can_view_map =	$roleModel->checkServicesPermission('permission_view_map');
        if(isset($info['role_sid']) && $can_view_map){
            $permission = $obj->permissionTask($info['role_sid']);

            $data = $obj->userInRole($permission);
            $response['data'] = $data;
        }

        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
