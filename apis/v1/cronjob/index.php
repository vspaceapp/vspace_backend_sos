<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/CaseModel.php';
require_once '../../include/GenNoCaseSR.php';
require '../.././libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
 
$app->get('/importsr/:sequence_id', function($sequence_id) use ($app) {


	$cronjob = new CaseModel();

	$data = $cronjob->importSr($sequence_id);

	$response = array();
	$response['error'] = false;
	// $response['data'] = $data;

	echoResponse(200, $response);

});

$app->get('/importcase/:sequence_id', function($sequence_id) use ($app) {


	$cronjob = new CaseModel();

	$data = $cronjob->importCase($sequence_id);

	$response = array();
	$response['error'] = false;
	$response['data'] = $data;
	
	echoResponse(200, $response);

});

function echoResponse($status_code,$response){
	$app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
$app->run();
?>