<?php
require_once "../../include/SettingTipGroupModel.php";

$index = function () use ($app) {
  try{
      parse_str($app->request()->getBody(),$request);

      $response['error'] = false;
      $response['request'] = $request;
      $settingModel = new SettingTipGroupModel();
      $data = $settingModel->selectData();

      $response['data'] = $data;
      $response['description'] = "ส่งข้อมูล email, token เข้ามา";
      echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e);
  }
};
$tipGroup = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $settingModel = new SettingTipGroupModel();
    $data = $settingModel->selectData();

    $response['data'] = $data;
    $response['description'] = "ส่งข้อมูล email, token เข้ามา";
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$addTipGroup = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;

    $settingModel = new SettingTipGroupModel();
    $data = $settingModel->addData($request);
    $response['data'] = $data;
    $response['description'] = "ส่งข้อมูล email, token, type, item, product, group_name, service_mgr และ mgr (service_mgr และ mgr ส่งมาเป็นรูปแบบของ email) ";
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$updateTipGroup = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $settingModel = new SettingTipGroupModel();
    $data = $settingModel->editData($request);

    $response['data'] = $data;
    $response['description'] = "ส่งข้อมูล email, token, type, item, product, group_name, service_mgr, mgr และ sid ของข้อมูลที่ต้องการแก้ไข  (service_mgr และ mgr ส่งมาเป็นรูปแบบของ email) ";
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$deleteTipGroup = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $settingModel = new SettingTipGroupModel();
    $data = $settingModel->deleteData($request);

    $response['error'] = false;
    $response['request'] = $request;
    $response['data'] = $data;

    $response['description'] = "ส่งข้อมูล email, token และ sid ของข้อมูลที่ต้องการลบ ";

    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
?>
