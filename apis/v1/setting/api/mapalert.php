<?php
require_once "../../include/SettingMapAlertModel.php";
$mapAlert = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;

    $obj = new SettingMapAlertModel();
    $response['data'] = $obj->selectData();
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$addMapAlert = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $obj = new SettingMapAlertModel();
    $data = $obj->addData($request);
    $response['data'] = $data;
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$updateMapAlert = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $obj = new SettingMapAlertModel();
    $response['data'] = $obj->editData($request);
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$deleteMapAlert = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;

    $obj = new SettingMapAlertModel();
    $response['data'] = $obj->deleteData($request);
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};

?>
