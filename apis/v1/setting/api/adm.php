<?php
require_once "../../include/SettingAdmModel.php";

$admApi = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $obj = new SettingAdmModel();
    $data = $obj->selectData();
    $response['data'] = $data;
    $response['description'] = "ส่งข้อมูล email, token เข้ามา";
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$addAdmApi = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $obj = new SettingAdmModel();
    $response['data'] = $obj->addData($request);

    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$updateAdm = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);
    $obj = new SettingAdmModel();
    $response['data'] = $obj->editData($request);
    $response['error'] = false;
    $response['request'] = $request;
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$deleteAdm = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);
    $obj = new SettingAdmModel();
    $response['data'] = $obj->deleteData($request);
    $response['error'] = false;
    $response['request'] = $request;
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
?>
