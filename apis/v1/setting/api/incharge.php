<?php
require_once "../../include/SettingInchargeModel.php";
$incharge = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $obj = new SettingInchargeModel();
    $response['data'] = $obj->selectData();
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$addIncharge = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);

    $response['error'] = false;
    $response['request'] = $request;
    $obj = new SettingInchargeModel();
    $response['data'] = $obj->addData($request);
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$updateIncharge = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);
    $obj = new SettingInchargeModel();
    $response['data'] = $obj->editData($request);

    $response['error'] = false;
    $response['request'] = $request;
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
$deleteIncharge = function() use ($app){
  try{
    parse_str($app->request()->getBody(),$request);
    $obj = new SettingInchargeModel();
    $response['data'] = $obj->deleteData($request);
    
    $response['error'] = false;
    $response['request'] = $request;
    echoRespnse(200, $response);
  }catch(Exception $e){
    echo $e;
    echoRespnse(500, $response['message']=$e->getMessage());
  }
};
?>
