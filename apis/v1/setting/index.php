<?php
ob_start('ob_gzhandler');
error_reporting(-1);
ini_set('display_errors', 'On');

require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development'
));
foreach (glob("api/*.php") as $filename){require_once ($filename);}

$app->post('/', $index);
$app->post('/tipGroup', $tipGroup);
$app->post('/addTipGroup', $addTipGroup);
$app->post('/updateTipGroup', $updateTipGroup);
$app->post('/deleteTipGroup', $deleteTipGroup);

$app->post('/adm', $admApi);
$app->post('/addAdm', $addAdmApi);
$app->post('/updateAdm', $updateAdm);
$app->post('/deleteAdm', $deleteAdm);

$app->post('/incharge', $incharge);
$app->post('/addIncharge', $addIncharge);
$app->post('/updateIncharge', $updateIncharge);
$app->post('/deleteIncharge', $deleteIncharge);

$app->post('/alert', $alert);
$app->post('/addAlert', $addAlert);
$app->post('/updateAlert', $updateAlert);
$app->post('/deleteAlert', $deleteAlert);

$app->post('/mapAlert', $mapAlert);
$app->post('/addMapAlert', $addMapAlert);
$app->post('/updateMapAlert', $updateMapAlert);
$app->post('/deleteMapAlert', $deleteMapAlert);
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
