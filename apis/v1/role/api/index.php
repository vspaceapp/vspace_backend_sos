<?php
require_once "../../include/RoleModel.php";
$roleUpdateDetail = function () use ($app) {
	try{
        parse_str($app->request()->getBody(),$request);

        $obj = new RoleModel();
        $obj->updateRoleDetail($request['role_sid'], $request['key'], $request['value'], $request['email']);
        
        $response = array();
        $response['error'] = false;
        $response['request'] = $request;
        
		echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};