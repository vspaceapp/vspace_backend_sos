<?php
require_once "../../include/CalendarModel.php";

$index = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new CalendarModel();

        $data = $obj->index($request['email'], $request['start'], $request['to']);
        $response = array();
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
