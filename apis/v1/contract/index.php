<?php

error_reporting(-1);
ini_set('display_errors', 'On');
// require_once '../../include/PreventiveMaintenanceModel.php';
require_once '../../include/db_handler.php';
require '../../libs/Slim/Slim.php';
require_once "../../include/ProjectModel.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
 
// ดึงข้อมูล serial ของ ticket
$app->post('/find_contract', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('serial','email'));
 
    // reading post params
    $serial = $app->request->post('serial');
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    //FIND CONTACT FROM contract
    // $obj = new ProjectModel();
    // $contactPeople = $obj->contactPeople($data['contract_no'],"");


    $response = array();
    $response['error'] = false;
    include "../../libs/simple_html_dom.php";
    /*
    OLD CODE FROM flgupload.com
    
    $url = "https://mobileapp.g-able.com/webservice/rest/EbizServices/getContractWarrantyJSON?user=SSS&company=FIRST&serialNo=".$serial;
    */

    $url = "http://flgupload.com/contractInfoEbiz.php?serial=".$serial;

    $data = file_get_html($url)->plaintext;
    $response['data'] = json_decode($data);
    
    // $response['contactPeople'] = $contactPeople;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/find_serial_info', function() use ($app){
    verifyRequiredParams(array('contract','email'));

    $contract = $app->request->post('contract');
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $response = array();
    $response['error'] = false;
    include "../../libs/simple_html_dom.php";
    /*
    OLD CODE FROM flgupload.com
    
    $url = "https://mobileapp.g-able.com/webservice/rest/EbizServices/getContractInfoJSON?user=SSS&company=FIRST&contractNo=".$contract;
    */
    $url = "http://flgupload.com/contractInfoEbiz.php?serial=".$contract;

    $data = file_get_html($url)->plaintext;
    $response['data'] = json_decode($data);

    echoRespnse(200, $response);
});
$app->post('/find_contract_info', function() use ($app){
    verifyRequiredParams(array('contract','email'));

    $contract = $app->request->post('contract');
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $response = array();
    $response['error'] = false;
    include "../../libs/simple_html_dom.php";
    $contract = trim($contract);
    $contract = strtoupper($contract);
    /*
    OLD CODE FROM flgupload.com
    
    $url = "https://mobileapp.g-able.com/webservice/rest/EbizServices/getContractInfoJSON?user=SSS&company=FIRST&contractNo=".$contract;
    */
    $url = "http://flgupload.com/contractInfoEbiz.php?contract=".$contract;

    $data = file_get_html($url)->plaintext;
    $response['data'] = json_decode($data);


    //FIND CONTACT FROM contract
    $obj = new ProjectModel();
    $contactPeople = $obj->contactPeople($contract,"");
    $response['contactPeople'] = $contactPeople;

    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>