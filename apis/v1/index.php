<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require_once '../include/db_handler.php';
require_once '../include/ServiceType.php';
require_once '../include/ProjectModel.php';
require_once '../include/FunctionImplement.php';
require_once '../include/RoleModel.php';
// require_once '../send_notification/ios/send.php';
// require_once '../send_notification/andriod/send.php';

require '.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
$todolist = array();
$myProject = array();

// User login
// $app->post('/user/login', function() use ($app) {
//     // check for required params
//     verifyRequiredParams(array('email', 'password'));
 
//     // reading post params
//     $email = $app->request->post('email');
//     $password = $app->request->post('password');
 
//     // validating email address
//     validateEmail($email);
 
//     $db = new DbHandler();
//     $response = $db->checklogin($email, $password);
 
//     // echo json response
//     echoRespnse(200, $response);
// });

$app->post('/sendMailAssignedCase', function() use ($app){
    $ticket_sid = $app->request->post('ticket_sid');
    require_once '../include/SendMail.php'; 
    $obj = new SendMail();
    $obj->noticeEngineerCaseAssigned($ticket_sid);

    $response = array();
    // $response['ticket_sid'] = $ticket_sid;
    echoRespnse(200, $response);

});
 
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
$app->post('/question1', function() use ($app){
    $number = $app->request->post('number');
    sort($number);
    $response = array();
    $response['result'] = $number;
    echoRespnse(200, $response);

});
$app->post('/question2', function() use ($app){
    $teams = $app->request->post('teams');
    $rounds = fixtures($teams);
    $response = array();
    $response['table'] = $rounds;
    echoRespnse(200, $response);
});
function fixtures($teams){
    $teamsLength = count($teams);
    $totalMatch = array();
    for($i=0;$i<$teamsLength;$i++){
        $round = array();
        for ($j=0; $j < $teamsLength; $j++) { 
            if($i!=$j){
                $tmp = array();
                $tmp['team_1_home_name'] = $teams[$i];
                $tmp['team_2_away_name'] = $teams[$j];
                array_push($round,$tmp);
            }
        }
        array_push($totalMatch, $round);
    }
    $rounds = array();
    $r = 1;
    $roundsTotal = ($teamsLength-1)*2;
    $mem = array();
    while ( $r <= $roundsTotal) {
        $tmp = array();
        $teamHasMatch = array();
        for ($i=0; $i < count($totalMatch) ; $i++) { 
            for ($j=0; $j < count($totalMatch[$i]); $j++) { 
                if(!in_array($totalMatch[$i][$j]['team_1_home_name'], $teamHasMatch) && !in_array($totalMatch[$i][$j]['team_2_away_name'], $teamHasMatch)){
                    $concatName = $totalMatch[$i][$j]['team_1_home_name'].$totalMatch[$i][$j]['team_2_away_name'];
                    if(!in_array($concatName, $mem)){
                        array_push($tmp,$totalMatch[$i][$j]);
                        array_push($mem, $concatName);
                        array_push($teamHasMatch, $totalMatch[$i][$j]['team_1_home_name']);
                        array_push($teamHasMatch, $totalMatch[$i][$j]['team_2_away_name']);
                    }   
                }
            }
        }
        array_push($rounds, array('round'=>$r,'team'=>$tmp));
        $r++;
    }
    return ($rounds);
}

$app->post('/question222', function() use ($app){
    $teams = $app->request->post('teams');
    $numberRounds = (count($teams)-1);
    $matches = count($teams)/2;
    $rounds = array();
    for ($i=0; $i < $numberRounds; $i++) { 
        $temp = array('round'=>($i+1),'team'=>genMatch($teams, $i));
        array_push($rounds, $temp);
    }
    
    $response = array();
    $response['table'] = $rounds;
    echoRespnse(200, $response);
});
function genMatch($teams, $index){
    $teamsLength = count($teams);
    $round = array();
    $loops = $teamsLength+$index;
    $indexAway = -1;
    // $indexAwayForNext = -1;
    $added = array();
    for ($i=0; $i < $teamsLength; $i+=2) {
        $run = ($i+$index)%$teamsLength;
        $indexHome = $run;
        $indexAway = ($run+1)%$teamsLength;
        array_push($added,  $indexHome);
        array_push($added,  $indexAway);
        $temp = array('team_1_home_name'=>$teams[$indexHome], 'team_2_away_name'=>$teams[$indexAway]);
        array_push($round, $temp);
    }
    return $round;
}
function genMatchOld($teams, $index){
    $teamsLength = count($teams);
    $round = array();
    $loops = $teamsLength+$index;
    $indexAway = -1;
    $indexAwayForNext = -1;
    $added = array();
    for ($i=0; $i < $teamsLength/2; $i++) {
        $indexHome = $i;
        $indexAway = ($i+1)%$teamsLength;
        // if($i<$teamsLength || 1){
        // if($index<5){
        // if(($indexAwayForNext+1)>13){
        //     $indexHome = ($indexAwayForNext+2)%$teamsLength;
        //     $indexAway = ($index+$indexHome+2)%$teamsLength;
        //     $indexAwayForNext = $indexAway;
        // }else{
        //     $indexHome = ($indexAwayForNext+1)%$teamsLength;
        //     if($index+$indexHome+1>13){
        //         $indexAway = ($index+$indexHome+2)%$teamsLength;
        //     }else{
        //         $indexAway = ($index+$indexHome+1)%$teamsLength;
        //         $indexAwayForNext = $indexAway;
        //     }
        // }
        // }else if($index<10){
        //     $indexHome = ($indexAway+2)%$teamsLength;
        //     $indexAway = ($index+$indexHome+2)%$teamsLength;
        // }else if($index<150){
        //     $indexHome = ($indexAway+3)%$teamsLength;
        //     $indexAway = ($index+$indexHome+3)%$teamsLength;
        // }
        // }else{
        //     $indexHome = ($i+1)%$teamsLength;
        //     $indexAway = $i%$teamsLength;
        // }
        array_push($added,  $indexHome);
        array_push($added,  $indexAway);
        $temp = array('team_1_home_name'=>$teams[$indexHome], 'team_2_away_name'=>$teams[$indexAway]);
        array_push($round, $temp);
    }
    return $round;
}
$app->post('/question3', function() use ($app){


    $position9 = array(
        0=>array('position'=>'1','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>''))),
        1=>array('position'=>'2','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>'')))
    );
    $position10 = array(
        0=>array('position'=>'3','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>''))),
        1=>array('position'=>'4','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>'')))
    );
    $position11 = array(
        0=>array('position'=>'5','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>''))),
        1=>array('position'=>'6','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>'')))
    );
    $position12 = array(
        0=>array('position'=>'7','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>''))),
        1=>array('position'=>'8','before'=>array(0=>array('position'=>'','bofore'=>''),1=>array('position'=>'','bofore'=>'')))
    );
    $position13 = array(
        0=>array('position'=>'9','before'=>$position9),
        1=>array('position'=>'10','before'=>$position10)
        );
    $position14 = array(
        0=>array('position'=>'11','before'=>$position11),
        1=>array('position'=>'12','before'=>$position12)
        );
    $before = array(
        0=>array('position'=>'13','before'=>$position13),
        1=>array('position'=>'14','before'=>$position14)
    );
    $chart = array('position'=>'15','before'=>$before);
    $response = array();
    $response['chart'] = $chart;
    echoRespnse(200, $response);
});

$app->post('/findEmployee', function() use ($app){
    global $app;
 
    
    $sql = $app->request->put('sql');
    
    $sql = str_replace("update", "", $sql);
    $sql = str_replace("delete", "", $sql);

    $sql = str_replace("UPDATE", "", $sql);
    $sql = str_replace("DELETE", "", $sql);

    $db = new DbHandler();
    $response = $db->findEmployee($sql);
 
    echoRespnse(200, $response);

});

$app->post('/sendNotificationToEmployee', function() use ($app){
    global $app;
    $sql = $app->request->put('sql');
    $message = $app->request->put('message');
 
    $db = new DbHandler();
    $response['data'] = $db->findEmployee($sql);
    foreach ($response['data'] as $key => $value) {
        if($value['device_platform']=="Android"){
            sendAndroid($value['gcm_registration_id'],$message);
        }else{
            sendNotificationIOS($value['gcm_registration_id'],$message,'case.flgupload.com');
        }
    }
    $response['message'] = $message;
    echoRespnse(200, $response);
});

$app->post('/test1', function() use ($app){
    $db = new DbHandler();
    $data = $db->updateEngineerToTask();
    echo "<pre>";
    print_r($data);
    echo "</pre>";
});
$app->post('/user/:id', function($user_id) use ($app) {
    global $app;
 
    verifyRequiredParams(array('gcm_registration_id'));
 
    $gcm_registration_id = $app->request->put('gcm_registration_id');
 
    $db = new DbHandler();
    $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
    echoRespnse(200, $response);
});
 
 $app->post('/taskscompleted', function() {
    global $app;
    global $todolist;
    global $myProject;

    verifyRequiredParams(array('email','status'));
    $email = $app->request->put('email');
    
    $status = $app->request->put('status');
    $role = $app->request->put('role');
    $start = $app->request->put('start');
    $end = $app->request->put('end');

    $start = explode("-", $start);
    $start = $start[0]."-".((strlen($start[1])==1)?"0".$start[1]:$start[1])."-".((strlen($start[2])==1)?"0".$start[2]:$start[2]);

    $end = explode("-", $end);
    $end = $end[0]."-".((strlen($end[1])==1)?"0".$end[1]:$end[1])."-".((strlen($end[2])==1)?"0".$end[2]:$end[2]);
    $response = array();
    $db = new DbHandler();
 
    // fetching all user tasks from tasks_log
    $result = $db->getTasksCompleted($email, $start, $end);
 
    $response["error"] = false;
    $response["tasks"] = array();
    $response["todolist"] = array();
    $response["events"] = array();
    // pushing single chat room into array
    // while ($chat_room = $result->fetch_assoc()) {
    foreach ($result as $key => $value) {
        # code...
        $tmp = array();
        $tmp["tasks_sid"] = $value["tasks_sid"];
        $tmp["name"] = $value["subject"];
        $tmp["created_at"] = $value["last_create_datetime"];
        $tmp["engineer"] = $value['engineer_email'];
        $tmp["tasks_log_status"] = $value['tasks_log_status'];
        $tmp["type"] = "task";
        $tmp["appointment"] = $value['appointment'];
        $tmp["expect_finish"] = $value['expect_finish'];
        $tmp["end_user_company_name"] = $value['end_user_company_name'];
        $tmp["end_user_site"] = $value['end_user_site'];
        $tmp["ticket_sid"] = $value['ticket_sid'];
        $tmp["no_ticket"] = $value['no_ticket'];
        $tmp["no_task"] = $value["no_task"];
        // $tmp["onsite_in"] = $value["onsite_in"];
        $tmp["service_type"] = $value["service_type"];
        $tmp["service_type_name"] = ucfirst($value["service_type_name"]);
        $tmp["service_type_icon"] = ($value["service_type_icon"]);
        $tmp["appointment_datetime"] = $value['appointment_datetime'];
        $tmp["expect_datetime"] = $value['expect_datetime'];

        $serviceTypeObj = new ServiceType();
        $serviceTypeList = $serviceTypeObj->listServiceStatus($value['service_type']);
        $tmp['service_type_list'] = $serviceTypeList;
        array_push($response["tasks"], $tmp);

        $event = array();
        if($value['appointment_type']=="2"){
            $event['eventName'] = $value['subject_service_report'];//$value['no_task'].", ".$value['subject_service_report'].($value['contract_no']?(", ".$value['contract_no']):"").(($tmp['end_user_company_name'])?(", ".$tmp['end_user_company_name']):'').(($tmp['end_user_site'])?(", ".$tmp['end_user_site']):'').", Appointment ".$value['appointment_datetime_df']."(".$value['expect_duration']."), Timestamp ".$value['start_task_timestamp_df']." - ".$value['closed_task_timestamp_df'];
        }else{
            $event['eventName'] = $value['no_task'].", ".$value['subject_service_report'].($value['contract_no']?(", ".$value['contract_no']):"").(($tmp['end_user_company_name'])?(", ".$tmp['end_user_company_name']):'').(($tmp['end_user_site'])?(", ".$tmp['end_user_site']):'').", Appointment ".$value['appointment_datetime_df']."(".$value['expect_duration']."), Timestamp ".$value['start_task_timestamp_df']." - ".$value['closed_task_timestamp_df'];            
        }

        $event['calendar'] = "Task";
        $event['color'] = "orange";

        $appointment = explode(" ", $value['appointment_datetime']);
        $appointment = explode("-", $appointment[0]);

        $event['day'] = $appointment[2];
        $event['month'] = $appointment[1]-1;
        $event['year'] = $appointment[0];  
        $event['link'] = "/pages/dashboard/#casedetail/".$value['ticket_sid'];
        array_push($response['events'], $event);
    }

    require_once '../include/PlanningModel.php';
    $planningDayView = new PlanningModel();
    $leaveAndBussyList = $planningDayView->leaveAndBussyList($email, $start, $end);
    // print_r($leaveAndBussyList);
    foreach ($leaveAndBussyList['oldTask'] as $key => $value) {

        $tmp = array();
        $tmp["tasks_sid"] = "";//$value["tasks_sid"];
        $tmp["name"] = $value["subject"];
        $tmp["created_at"] = "";//$value["last_create_datetime"];
        $tmp["engineer"] = "";//$value['engineer_email'];
        $tmp["tasks_log_status"] = "";//$value['tasks_log_status'];
        $tmp["type"] = "task";
        $tmp["appointment"] = $value['appointment'];
        $tmp["expect_finish"] = "";//$value['expect_finish'];
        $tmp["end_user_company_name"] = $value['customer_company_name'];
        $tmp["end_user_site"] = "";//$value['end_user_site'];
        $tmp["ticket_sid"] = "";//$value['ticket_sid'];
        $tmp["no_ticket"] = $value['request_no'];
        $tmp["no_task"] = $value["SRID"];
        // $tmp["onsite_in"] = $value["onsite_in"];
        $tmp["service_type"] = "";//$value["service_type"];
        $tmp["service_type_name"] = "";//ucfirst($value["service_type_name"]);
        $tmp["service_type_icon"] = "";//($value["service_type_icon"]);
        $tmp["appointment_datetime"] = $value['appointment'];
        $tmp["expect_datetime"] = $value['appointment'];

        $serviceTypeObj = new ServiceType();
        $serviceTypeList = "";//$serviceTypeObj->listServiceStatus($value['service_type']);
        $tmp['service_type_list'] = "";//$serviceTypeList;
        array_push($response["tasks"], $tmp);

        $event = array();
        $event['eventName'] = $tmp['no_task'].", ".$tmp['no_ticket'].", ".$value['contract_no'].", ".$tmp['end_user_company_name'].", ".$tmp['end_user_site'];
        $event['calendar'] = "Task";
        $event['color'] = "orange";
        $event['link'] = 'http://flgupload.com/FLG/index.php?r=SRS/default/editManageService&stepIndex=1&id={%22no%22:%22'.$tmp['no_task'].'%22}';
        $appointment = explode(" ", $value['appointment']);
        $appointment = explode("-", $appointment[0]);

        $event['day'] = $appointment[2];
        $event['month'] = $appointment[1]-1;
        $event['year'] = $appointment[0];  
        array_push($response['events'], $event);
    }

    foreach ($leaveAndBussyList['leave'] as $key => $value) {
         $tmp = array();
         $tmp = $value;
        $tmp["tasks_sid"] = $value["description"];
        $tmp["name"] = $value["group_master"];
        $tmp["created_at"] = "";//$value["last_create_datetime"];
        $tmp["engineer"] = "";//$value['engineer_email'];
        $tmp["tasks_log_status"] = "";//$value['tasks_log_status'];
        $tmp["type"] = "task";
        $tmp["appointment"] = $value['date_busy'];
        $tmp["expect_finish"] = "";//$value['expect_finish'];
        $tmp["end_user_company_name"] = $value['description'];//$value['customer_company_name'];
        $tmp["end_user_site"] = "";//$value['end_user_site'];
        $tmp["ticket_sid"] = "";//$value['ticket_sid'];
        $tmp["no_ticket"] = "";//$value['detail'];
        $tmp["no_task"] = $value['description'];//$value["SRID"];
        // $tmp["onsite_in"] = $value["onsite_in"];
        $tmp["service_type"] = "";//$value["service_type"];
        $tmp["service_type_name"] = "";//ucfirst($value["service_type_name"]);
        $tmp["service_type_icon"] = "";//($value["service_type_icon"]);
        $tmp["appointment_datetime"] = $value['date_busy'];
        $tmp["expect_datetime"] = $value['date_busy'];

        $serviceTypeObj = new ServiceType();
        $serviceTypeList = "";//$serviceTypeObj->listServiceStatus($value['service_type']);
        $tmp['service_type_list'] = "";//$serviceTypeList;
        array_push($response["tasks"], $tmp);

         $event = array();
        $event['eventName'] = $value['description']." ".$value['detail']." ".$value['busy_start_time']." - ".$value['busy_end_time'];
        $event['calendar'] = "BussyList";
        $event['color'] = "green";

        // $appointment = explode(" ", $value['date_busy']);
        $appointment = explode("-", $value['date_busy']);

        $event['day'] = $appointment[2];
        $event['month'] = $appointment[1]-1;
        $event['year'] = $appointment[0];  
        array_push($response['events'], $event);

    }

    //มัสคอมเม้นออก standby7x24
    // $standby7x24 = $planningDayView->standby7x24Calendar($email,$start,$end);
    // foreach ($standby7x24 as $key => $value) {
    //     $event = array();
    //     $event['eventName'] = "Standby 7x24 ".$value['start_time_df']." - ".$value['time_end_df'];
    //     $event['calendar'] = "7x24";
    //     $event['color'] = "blue";
    //     $appointment = explode("-", $value['standby_date']);

    //     $event['day'] = $appointment[2];
    //     $event['month'] = $appointment[1]-1;
    //     $event['year'] = $appointment[0];  
    //     array_push($response['events'], $event);
    // }
    $response['standby7x24'] = array();//$standby7x24;
    // }
    echoRespnse(200, $response);
});

 $app->post('/taskscompleteds', function() {
    global $app;
    global $todolist;
    global $myProject;

    verifyRequiredParams(array('email','status'));
    $email = $app->request->put('email');
    
    $status = $app->request->put('status');
    $role = $app->request->put('role');
    $views = $app->request->put('views');

    $views = explode(" ", $views);

    // $start = explode("-", $start);
    // $start = $start[0]."-".((strlen($start[1])==1)?"0".$start[1]:$start[1])."-".((strlen($start[2])==1)?"0".$start[2]:$start[2]);

    // $end = explode("-", $end);
    // $end = $end[0]."-".((strlen($end[1])==1)?"0".$end[1]:$end[1])."-".((strlen($end[2])==1)?"0".$end[2]:$end[2]);
    $start = $views[3].'-'.'08'.'-'.$views[2];
    // $end = $views[3]+'-08-'+($views[2]+1);
    // $start = $views[3].'-'.'08'.'-'.$views[2];
    $end = $start;
    $response = array();
    $db = new DbHandler();
 
    // fetching all user tasks from tasks_log
    $result = $db->getTasksCompletedApp($email, $start);
 
    $response["error"] = false;
    $response["tasks"] = array();
    $response["todolist"] = array();
    // pushing single chat room into array
    // while ($chat_room = $result->fetch_assoc()) {
    foreach ($result as $key => $value) {
        # code...
        $tmp = array();
        $tmp["tasks_sid"] = $value["tasks_sid"];
        $tmp["name"] = $value["subject"];
        $tmp["created_at"] = $value["last_create_datetime"];
        $tmp["engineer"] = $value['engineer_email'];
        $tmp["tasks_log_status"] = $value['tasks_log_status'];
        $tmp["type"] = "task";
        $tmp["appointment"] = $value['appointment'];
        $tmp["expect_finish"] = $value['expect_finish'];
        $tmp["end_user_company_name"] = $value['end_user_company_name'];
        $tmp["end_user_site"] = $value['end_user_site'];
        $tmp["ticket_sid"] = $value['ticket_sid'];
        $tmp["no_ticket"] = $value['no_ticket'];
        $tmp["no_task"] = $value["no_task"];
        // $tmp["onsite_in"] = $value["onsite_in"];
        $tmp["service_type"] = $value["service_type"];
        $tmp["service_type_name"] = ucfirst($value["service_type_name"]);
        $tmp["service_type_icon"] = ($value["service_type_icon"]);
        $tmp["appointment_datetime"] = $value['appointment_datetime'];
        $tmp["expect_datetime"] = $value['expect_datetime'];

        $serviceTypeObj = new ServiceType();
        $serviceTypeList = $serviceTypeObj->listServiceStatus($value['service_type']);
        $tmp['service_type_list'] = $serviceTypeList;
        array_push($response["tasks"], $tmp);    
    }

    require_once '../include/PlanningModel.php';
    $planningDayView = new PlanningModel();
    $leaveAndBussyList = $planningDayView->leaveAndBussyList($email, $start, $end);
    // print_r($leaveAndBussyList);
    foreach ($leaveAndBussyList['oldTask'] as $key => $value) {

        $tmp = array();
        $tmp["tasks_sid"] = "";//$value["tasks_sid"];
        $tmp["name"] = $value["subject"];
        $tmp["created_at"] = "";//$value["last_create_datetime"];
        $tmp["engineer"] = "";//$value['engineer_email'];
        $tmp["tasks_log_status"] = "";//$value['tasks_log_status'];
        $tmp["type"] = "task";
        $tmp["appointment"] = $value['appointment'];
        $tmp["expect_finish"] = "";//$value['expect_finish'];
        $tmp["end_user_company_name"] = $value['customer_company_name'];
        $tmp["end_user_site"] = "";//$value['end_user_site'];
        $tmp["ticket_sid"] = "";//$value['ticket_sid'];
        $tmp["no_ticket"] = $value['request_no'];
        $tmp["no_task"] = $value["SRID"];
        // $tmp["onsite_in"] = $value["onsite_in"];
        $tmp["service_type"] = "";//$value["service_type"];
        $tmp["service_type_name"] = "";//ucfirst($value["service_type_name"]);
        $tmp["service_type_icon"] = "";//($value["service_type_icon"]);
        $tmp["appointment_datetime"] = $value['appointment'];
        $tmp["expect_datetime"] = $value['appointment'];

        $serviceTypeObj = new ServiceType();
        $serviceTypeList = "";//$serviceTypeObj->listServiceStatus($value['service_type']);
        $tmp['service_type_list'] = "";//$serviceTypeList;
        array_push($response["tasks"], $tmp);
    }

    foreach ($leaveAndBussyList['leave'] as $key => $value) {
         $tmp = array();
         $tmp = $value;
        $tmp["tasks_sid"] = $value["description"];
        $tmp["name"] = $value["group_master"];
        $tmp["created_at"] = "";//$value["last_create_datetime"];
        $tmp["engineer"] = "";//$value['engineer_email'];
        $tmp["tasks_log_status"] = "";//$value['tasks_log_status'];
        $tmp["type"] = "task";
        $tmp["appointment"] = $value['date_busy'];
        $tmp["expect_finish"] = "";//$value['expect_finish'];
        $tmp["end_user_company_name"] = $value['description'];//$value['customer_company_name'];
        $tmp["end_user_site"] = "";//$value['end_user_site'];
        $tmp["ticket_sid"] = "";//$value['ticket_sid'];
        $tmp["no_ticket"] = "";//$value['detail'];
        $tmp["no_task"] = $value['description'];//$value["SRID"];
        // $tmp["onsite_in"] = $value["onsite_in"];
        $tmp["service_type"] = "";//$value["service_type"];
        $tmp["service_type_name"] = "";//ucfirst($value["service_type_name"]);
        $tmp["service_type_icon"] = "";//($value["service_type_icon"]);
        $tmp["appointment_datetime"] = $value['date_busy'];
        $tmp["expect_datetime"] = $value['date_busy'];

        $serviceTypeObj = new ServiceType();
        $serviceTypeList = "";//$serviceTypeObj->listServiceStatus($value['service_type']);
        $tmp['service_type_list'] = "";//$serviceTypeList;
        array_push($response["tasks"], $tmp);
    }
    $standby7x24 = $planningDayView->standby7x24Calendar($email,$start,$end);
    foreach ($standby7x24 as $key => $value) {
        $event = array();
        $event['eventName'] = "Standby 7x24 ".$value['start_time_df']." - ".$value['time_end_df'];
        $event['calendar'] = "7x24";
        $event['color'] = "blue";
        $appointment = explode("-", $value['standby_date']);

        $event['day'] = $appointment[2];
        $event['month'] = $appointment[1]-1;
        $event['year'] = $appointment[0];  
        array_push($response['events'], $event);
    }
    // }
    echoRespnse(200, $response);
});

/* * *
 * fetching all chat rooms
 */
$app->post('/tasks', function() {
	global $app;
    global $todolist;
    global $myProject;

	verifyRequiredParams(array('email'));
    $email = $app->request->put('email');
    
    $status = $app->request->put('status');
    $role = $app->request->put('role');
    if($app->request->put('view_sid')){
        $view_sid = $app->request->put('view_sid');
    }else{
        $view_sid = "1";
    }
    $db = new DbHandler();
    $data = $db->getSegmentMobile($email, $view_sid);
    $response = array();
    $response["error"] = false;
    $response["tasks"] = array();
    $response["unsigned"] = array();
    $response["todolist"] = array();
    $response["feeds"] = array();
    $response["notifications"] = array();
    $response["raw"] = $data;

    // fetching all user tasks from tasks_log
    if(strpos(" ".$data['detail'], "TaskSR")){
        $result = $db->getTasksWorking($email);
        $response['result'] = $response;
        foreach ($result as $key => $value) {
            $tmp = array();
            $tmp["tasks_sid"] = $value["tasks_sid"];
            $tmp["name"] = $value["subject"];
            $tmp["created_at"] = $value["last_create_datetime"];
            $tmp["engineer"] = $value['engineer_email'];
            $tmp["tasks_log_status"] = $value['tasks_log_status'];
            $tmp["type"] = "task";
            $tmp["appointment"] = $value['appointment'];
            $tmp["end_user_company_name"] = $value['end_user_company_name'];
            if($value['case_type']=='Preventive Maintenance'){
                if($value['end_user_company_name_service_report']!="")
                    $tmp["end_user_site"] = $value['end_user_company_name_service_report'];
                else
                    $tmp["end_user_site"] = $value['end_user_site'];    
            }else{
                $tmp["end_user_site"] = $value['end_user_site'];
            }
            $tmp["ticket_sid"] = $value['ticket_sid'];
            $tmp["no_ticket"] = $value['no_ticket'];
            $tmp["no_task"] = $value["no_task"];
            $tmp["onsite_in"] = $value["onsite_in"];
            $tmp["service_type"] = $value["service_type"];
            $tmp["service_type_name"] = ucfirst($value["service_type_name"]);
            $tmp["service_type_icon"] = ($value["service_type_icon"]);
            $tmp["end_user"] = $value['end_user'];
            $tmp["end_user_site"] = $value['end_user_site'];
            $serviceTypeObj = new ServiceType();
            $serviceTypeList = $serviceTypeObj->listServiceStatus($value['service_type']);
            $tmp['service_type_list'] = $serviceTypeList;
            $tmp['link'] = "#task/".$value['ticket_sid']."/".$value['tasks_sid']."/right";
            $tmp['pic'] = '../img/icon/'.$value['service_type'].'.png';
            $tmp['is_account'] = $value['is_account'];
            for($i=0;$i<=600;$i+=100){
                if($value['tasks_log_status']=="400" && !$value['is_account']){
                    $value['tasks_log_status'] = 600;
                }
                if($value['tasks_log_status']<=$i){
                    $next = ($i/100 )+1;
                    for($j=$next;$j<=6;$j++){
                        $checkServiceStatusRequired = $serviceTypeObj->checkServiceStatusRequired($j, $serviceTypeList);
                        if($j==5 && $value['is_number_one']==false){
                        }else{
                            if($checkServiceStatusRequired['result']){
                                $tmp["subject_type"] = $checkServiceStatusRequired['subject'];
                                $j+=100;
                                $i+=1000;
                            }
                        }
                    }
                }
            }
            if(isset($tmp['subject_type'])){
                array_push($response["tasks"], $tmp);
            }       
        }
    }

    if(strpos(" ".$data['detail'], "Unsigned")){
        $unsigned = $db->getUnsigned($email);
        $response['tasks'] = $unsigned;
    }
    if(strpos(" ".$data['detail'], "WaitCustomerSigned")){
        $unsigned = $db->getUnsigned($email);
        $response['tasks'] = $unsigned;   
    }
    if(strpos(" ".$data['detail'], "Feeds")){
        $feeds = $db->getFeeds($email);
        foreach ($feeds as $key => $value) {
            array_push($response['tasks'], $value);
        }
    }
    if(strpos(" ".$data['detail'],"Notifications")){
        $notifications = $db->getNotifications($email);
        foreach ($notifications as $key => $value) {
            array_push($response['notifications'], $value);
        }
    }
    if($view_sid==""){
        $result = $db->getTasksWorking($email);
        foreach ($result as $key => $value) {
            $tmp = array();
            $tmp["tasks_sid"] = $value["tasks_sid"];
            $tmp["name"] = $value["subject"];
            $tmp["created_at"] = $value["last_create_datetime"];
            $tmp["engineer"] = $value['engineer_email'];
            $tmp["tasks_log_status"] = $value['tasks_log_status'];
            $tmp["type"] = "task";
            $tmp["appointment"] = $value['appointment'];
            $tmp["end_user_company_name"] = $value['end_user_company_name'];
            if($value['case_type']=='Preventive Maintenance'){
                if($value['end_user_company_name_service_report']!="")
                    $tmp["end_user_site"] = $value['end_user_company_name_service_report'];
                else
                    $tmp["end_user_site"] = $value['end_user_site'];    
            }else{
                $tmp["end_user_site"] = $value['end_user_site'];
            }
            $tmp["ticket_sid"] = $value['ticket_sid'];
            $tmp["no_ticket"] = $value['no_ticket'];
            $tmp["no_task"] = $value["no_task"];
            $tmp["onsite_in"] = $value["onsite_in"];
            $tmp["service_type"] = $value["service_type"];
            $tmp["service_type_name"] = ucfirst($value["service_type_name"]);
            $tmp["service_type_icon"] = ($value["service_type_icon"]);
            $tmp["end_user"] = $value['end_user'];
            $tmp["end_user_site"] = $value['end_user_site'];
            $serviceTypeObj = new ServiceType();
            $serviceTypeList = $serviceTypeObj->listServiceStatus($value['service_type']);
            $tmp['service_type_list'] = $serviceTypeList;
            $tmp['link'] = "#task/".$value['ticket_sid']."/".$value['tasks_sid']."/right";
            $tmp['pic'] = '../img/icon/'.$value['service_type'].'.png';
            $tmp['is_account'] = $value['is_account'];

            for($i=0;$i<=600;$i+=100){
                if($value['tasks_log_status']<=$i){
                    $next = ($i/100 )+1;
                    for($j=$next;$j<=6;$j++){
                        $checkServiceStatusRequired = $serviceTypeObj->checkServiceStatusRequired($j, $serviceTypeList);
                        if($j==5 && $value['is_number_one']==false){
                        }else{
                            if($checkServiceStatusRequired['result']){
                                $tmp["subject_type"] = $checkServiceStatusRequired['subject'];
                                $j+=100;
                                $i+=1000;
                            }
                        }
                    }
                }
            }
            if(isset($tmp['subject_type'])){
                array_push($response["tasks"], $tmp);
            }       
        }
    }
    
    echoRespnse(200, $response);
});
$app->post('/segment', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->put('email');
    $db = new DbHandler();
    $data = $db->getSegmentMobile($email);
    $response["error"] = false;
    $response["tasks"] = array();
    $response["unsigned"] = array();
    $response["todolist"] = array();
    $response["feeds"] = array();
    $response["raw"] = $data;

    echoRespnse(200, $response);
});

$app->post('/insert/input_action', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('task_sid','email'));
 
    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');
    $pm_service = $app->request->post('pm_service');

    $model = new DbHandler();
    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email, 'pm_service'=>$pm_service);
    $result = $model->insertInputAction($param, $email);


    $response = array();
    $response['error'] = false;
    $response['result'] = $result;
    
    // echo json response
    echoRespnse(200, $response);
});

/**
 * Fetching single detail_task including
 *  */
$app->post('/task_detail/:id', function($tasks_sid) use ($app) {
    global $app;
    $db = new DbHandler();
    
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');

    if($app->request->post('view')){
        $view = $app->request->post('view');
    }else{
        $view = 1;
    }
    if($view==2){
        $result = $db->getTaskDetailUnsigned($tasks_sid, $email);
    }else{
        $result = $db->getTaskDetail($tasks_sid, $email);
    }

    // print_r($result);
    $response["error"] = false;
    $response["messages"] = array();
    $response['task_detail'] = array();
    
    // $i = 0;
    // looping through result and preparing tasks array
    // while ($chat_room = $result->fetch_assoc()) {
        // adding chat room node
    foreach ($result as $key => $task_detail) {
        if ($key == 0) {
            $tmp = array();
            $tmp["chat_room_id"] = $task_detail["tasks_sid"];
            $tmp["tasks_sid"] = $task_detail["tasks_sid"];
            $tmp["ticket_sid"] = $task_detail["ticket_sid"];
            $tmp["name"] = $task_detail["subject"];
            $tmp["created_at"] = $task_detail["create_datetime"];
            $tmp["ticket_description"] = $task_detail["ticket_description"];
            $tmp["ticket_project_name"] = $task_detail["ticket_project_name"];
            $tmp["ticket_serial"] = $task_detail["ticket_serial"];
            $tmp["ticket_description"] = $task_detail["ticket_description"];
            $tmp["ticket_contract"] = $task_detail["ticket_contract"];
            $tmp["ticket_end_user_company_name"] = $task_detail["ticket_end_user_company_name"];
            $tmp["ticket_end_user_site"] = $task_detail["ticket_end_user_site"];
            $tmp["ticket_end_user_contact_name"] = $task_detail["ticket_end_user_contact_name"];
            $tmp["ticket_end_user_phone"] = $task_detail["ticket_end_user_phone"];
            $tmp["ticket_end_user_mobile"] = $task_detail["ticket_end_user_mobile"];
            $tmp["ticket_end_user_email"] = $task_detail["ticket_end_user_email"];
            $tmp["ticket_case_type"] = $task_detail["ticket_case_type"];
            $tmp["task_status_tmp"] = $task_detail['status'];
            $tmp["appointment"] = $task_detail['appointment'];
            $tmp['appointment_YmdHi'] = $task_detail['appointment_YmdHi'];
            $tmp["expect_finish"] = $task_detail['expect_finish'];
            $tmp["expect_duration"] = $task_detail['expect_duration'];

            $tmp["travel_by"] = $task_detail['travel_by'];
            $tmp["action_completed"] = $task_detail['action_completed'];
            $tmp["is_number_one"] = $task_detail['is_number_one'];
            $tmp["pm_serial"] = $task_detail['pm_serial'];
            $tmp['task_request'] = $task_detail['task_request'];
            $tmp['sparepart'] = $task_detail['sparepart'];
            $tmp['input_action'] = $task_detail['input_action'];
            $tmp['subject_service_report'] = ($task_detail['subject_service_report']!="")?$task_detail['subject_service_report']:$task_detail["subject"];
            $tmp['end_user_contact_name_service_report'] = $task_detail['end_user_contact_name_service_report'];
            $tmp['end_user_phone_service_report'] = $task_detail['end_user_phone_service_report'];
            $tmp['end_user_mobile_service_report'] = $task_detail['end_user_mobile_service_report'];
            $tmp['end_user_email_service_report'] = $task_detail['end_user_email_service_report'];
            $tmp['end_user_company_name_service_report'] = $task_detail['end_user_company_name_service_report'];
            $tmp['no_ticket'] = $task_detail['no_ticket'];
            $tmp['no_task'] = $task_detail['no_task'];
            $tmp['serial_in_ticket'] = $task_detail['serial_in_ticket'];
            $tmp['end_user_site'] = $task_detail['ticket_end_user_site'];
            $tmp['end_user'] = $task_detail['end_user'];
            if(isset($task_detail['summary_pm'])){
                $tmp['summary_pm'] = $task_detail['summary_pm'];
            }

            $tmp["service_type"] = $task_detail["service_type"];
	        $tmp["service_type_name"] = ucfirst($task_detail["service_type_name"]);
	        $tmp["service_type_icon"] = ($task_detail["service_type_icon"]);
            $tmp["is_follow_up"] = $task_detail['is_follow_up'];
            $tmp["customer_signated"] = $task_detail['customer_signated'];
            if($view==2){
                if($task_detail['customer_signated']!="1"){
                    $tmp["resend_signed"] = "1";
                }else{
                    $tmp["resend_signed"] = false;
                }
            }
	        $serviceTypeObj = new ServiceType();
	        $serviceTypeList = $serviceTypeObj->listServiceStatus($task_detail['service_type']);
	        $tmp['service_type_list'] = $serviceTypeList;
            $tmp['is_account'] = $task_detail['is_account'];

            if(!$task_detail['is_account'] && $task_detail['status']=="400"){
                $task_detail['status'] = "500";
            }

            $isRequiredTaskTimeStamp = false;
	        for($i=0;$i<=600;$i+=100){
        		if($task_detail['status']<=$i){
        			$next = ($i/100 )+1;
        			for($j=$next;$j<=6;$j++){
        				$checkServiceStatusRequired = $serviceTypeObj->checkServiceStatusRequired($j, $serviceTypeList);
        				if($checkServiceStatusRequired['result']){
                            $isRequiredTaskTimeStamp = true;
	        				$tmp["service_type_type"] = $checkServiceStatusRequired['service_type'];
	        				$tmp["subject_type"] = $checkServiceStatusRequired['subject'];
	        				$tmp["descript_status"] = $checkServiceStatusRequired['descript_status'];
	        				$tmp["btnRight"] = $checkServiceStatusRequired['btn_right'];
	        				$tmp["btnLeft"] = $checkServiceStatusRequired['btn_left'];
	        				$tmp["task_status"] = $checkServiceStatusRequired['task_status'];
	        				$j+=100;
	        				$i+=1000;
	        			}
        			}
        		}
        	}
        	if(!isset($tmp['subject_type'])){
        		$tmp["service_type_type"] = "";
				$tmp["subject_type"] = "";
				$tmp["descript_status"] = "";
				$tmp["btnRight"] = "";
				$tmp["btnLeft"] = "";
				$tmp["task_status"] = "";
        	}
            if($task_detail['status']=="600" || !$isRequiredTaskTimeStamp){
                $tmp["service_type_type"] = "";
                $tmp["subject_type"] = "Completed";
                $tmp["descript_status"] = "Completed";
                $tmp["btnRight"] = "";
                $tmp["btnLeft"] = "";
                $tmp["task_status"] = "";   
            }
            $response['task_detail'] = $tmp;
        }
 
        if (isset($task_detail['tasks_sid']) && $task_detail['tasks_sid'] != "") {
            // message node
            $cmt = array();
            $cmt["message"] = $task_detail["subject"];
            $cmt["message_id"] = $task_detail["tasks_sid"];
            $cmt["created_at"] = $task_detail["create_datetime"];
 
            // user node
            $user = array();
            $user['user_id'] = $task_detail['tasks_sid'];
            $user['username'] = $task_detail['subject'];
            $cmt['user'] = $user;
 
            array_push($response["messages"], $cmt);
        }

    }
    // }
 
    echoRespnse(200, $response);
});

// function dashboard(){
// }

function projmgt($email){
    global $todolist;
    // echo "projmgt";
    $obj = new ProjectModel();
    $newprojects = $obj->listnewprojects();
    foreach ($newprojects as $key => $value) {
        $tmp = array();
        $tmp["detail"] = "<span class='glyphicon glyphicon-tasks'></span> Project: ".$value['project']."<br/><i class='fa fa-fw fa-institution'></i> Company: ".$value['customer'];
        $tmp["sid"] = $value["sid"];
        $tmp["project_sid"] = $value["project_sid"];
        $tmp["primary"] = "<i class='fa fa-fw fa-edit'></i> Contract: ".$value["contract"];
        $tmp["subject_type"] = "<i class='fa fa-flag-o'></i> Please Set Type Project (New Project)";
        // $tmp["option"] = '<div class="row"><div class="col-md-6">* Project Type? <div class="dropdownlist_type"></div></div><div class="col-md-6"></div></div>';
        $tmp["option"] = '<div class="row" style="margin-top:10px;"><div class="col-md-6"></div><div class="col-md-6"><button style="width:100%; webkit-user-select: none;" class="btnSubmit">Click Me For Submit</button></div></div>';
        // $tmp["option"] .= '<div class="dropdownlist_engineer"></div>';
        $tmp["goto"] = "gotoProjectManagement('".$value['sid']."')";
        $tmp["javascriptFunctionOption"] = 'var $data = {project_sid: "'.$value['project_sid'].'", contract: "'.$value['contract'].'", company: "'.$value['customer'].'", name: "'.$value['name'].'", vendor: "'.$value['vendor'].'", owner:"'.$value['owner'].'",man_days:"'.$value['man_days'].'",project_start:"'.$value['project_start'].'",project_end:"'.$value['project_end'].'",project_type:"'.$value['project_type'].'",end_user:"'.$value['end_user'].'",prime_contract:"'.$value['prime_contract'].'",scope:"'.$value['scope'].'"};genButtonToManageProject($li,$data);';
        array_push($todolist, $tmp);
    }
}

function projImplement($email){
    global $todolist;
    // echo "projmgt";
    $obj = new ProjectModel();
    $newprojects = $obj->listProjectImplementWaitSetManday();
    foreach ($newprojects as $key => $value) {
        $tmp = array();
        $tmp["detail"] = "<span class='glyphicon glyphicon-tasks'></span> Project: ".$value['project']."<br/><i class='fa fa-fw fa-institution'></i> Company: ".$value['customer'];
        $tmp["sid"] = $value["sid"];
        $tmp["project_sid"] = $value["project_sid"];
        $tmp["primary"] = "<i class='fa fa-fw fa-edit'></i> Contract: ".$value["contract"];
        $tmp["subject_type"] = "<i class='fa fa-fw fa-edit'></i> Section Manager, Please Set Man Days<br/>(<i class='fa fa-flag-o'></i> Project Imeplement)";
        $tmp["option"] = '<div class="row" style="margin-top:10px;"><div class="col-md-6"></div><div class="col-md-6"><button style="width:100%; webkit-user-select: none;" class="btnSubmit">Click Me For Submit</button></div></div>';

        $tmp["goto"] = "gotoProjectManagement('".$value['sid']."')";
        // $tmp["javascriptFunctionOption"] = 'genDropdownProjectImplement($li);';
        $tmp["javascriptFunctionOption"] = 'var $data = {project_sid: "'.$value['project_sid'].'", contract: "'.$value['contract'].'", company: "'.$value['customer'].'", name: "'.$value['name'].'", vendor: "'.$value['vendor'].'", owner:"'.$value['owner'].'",man_days:"'.$value['man_days'].'",project_start:"'.$value['project_start'].'",project_end:"'.$value['project_end'].'",project_type:"'.$value['project_type'].'",end_user:"'.$value['end_user'].'",prime_contract:"'.$value['prime_contract'].'",scope:"'.$value['scope'].'"};genButtonToManageProject($li,$data);';
        array_push($todolist, $tmp);
    }
}

function projInstall($email){
    global $todolist;
    // echo "projmgt";
    $obj = new ProjectModel();
    $newprojects = $obj->listProjectInstallWaitSetManday();
    foreach ($newprojects as $key => $value) {
        $tmp = array();
        $tmp["detail"] = "<span class='glyphicon glyphicon-tasks'></span> Project: ".$value['project']."<br/><i class='fa fa-fw fa-institution'></i> Company: ".$value['customer'];
        $tmp["sid"] = $value["sid"];
        $tmp["project_sid"] = $value["project_sid"];
        $tmp["primary"] = "<i class='fa fa-fw fa-edit'></i> Contract: ".$value["contract"];
        $tmp["subject_type"] = "<i class='fa fa-fw fa-edit'></i> Section Manager, Please Set Man Days<br/>(<i class='fa fa-fw fa-download'></i> Project Install )";
        // $tmp["option"] = '<div class="dropdownlist_type"></div>';
        // $tmp["option"] = '<div class="dropdownlist_mandays"></div>';
        $tmp["option"] = '<div class="row" style="margin-top:10px;"><div class="col-md-6"></div><div class="col-md-6"><button style="width:100%; webkit-user-select: none;" class="btnSubmit">Click Me For Submit</button></div></div>';
        $tmp["goto"] = "gotoProjectManagement('".$value['sid']."')";
        // $tmp["javascriptFunctionOption"] = 'genDropdownProjectInstall($li);';
        $tmp["javascriptFunctionOption"] = 'var $data = {project_sid: "'.$value['project_sid'].'", contract: "'.$value['contract'].'", company: "'.$value['customer'].'", name: "'.$value['name'].'", vendor: "'.$value['vendor'].'", owner:"'.$value['owner'].'",man_days:"'.$value['man_days'].'",project_start:"'.$value['project_start'].'",project_end:"'.$value['project_end'].'",project_type:"'.$value['project_type'].'",end_user:"'.$value['end_user'].'",prime_contract:"'.$value['prime_contract'].'",scope:"'.$value['scope'].'"};genButtonToManageProject($li,$data);';
        array_push($todolist, $tmp);
    }
}

$app->post('/timestampSr', function(){
    global $app;
    $db = new DbHandler();
 
    verifyRequiredParams(array('email', 'task_status','taxi_fare'));
 
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $follow_up = 0;
    if($app->request->post('follow_up')){
        $follow_up = $app->request->post('follow_up');
    }
    $task_status = $app->request->post('task_status');
    $tasks_sid = $app->request->post('tasks_sid');

    $taxi_fare = $app->request->post('taxi_fare');
    $taxi_fare_stang = $app->request->post('taxi_fare_stang');

    $lat = $app->request->post('lat');
    $lng = $app->request->post('lng');

    if($app->request->post('signature')){
        $signature = $app->request->post('signature');
    }else{
        $signature = '';
    }
    if($task_status!="-1"){
        if($task_status=="0"){
            $task_status+=100;
        }
        $new_status = $task_status+100;
    }else{
        $new_status = "-1";
    }
    if($new_status==500 && $signature==''){
        fopen("http://vspace.in.th/api/sendmail/sendEmailAfterClosedToCustomer/".$tasks_sid."?email=".$email, "r");
    }
    $response = $db->addNewStatusTaskLog($email, $tasks_sid, $new_status, $email, $taxi_fare, $lat, $lng, $follow_up, $signature);
    $response["error"] = false;
    $response['request'] = array($task_status,$tasks_sid,$taxi_fare,$taxi_fare_stang,$lat,$lng);
    $response['current_status'] = $new_status;
    echoRespnse(200, $response);
});

$app->post('/checkpoint', function() {
    global $app;
    $db = new DbHandler();
 
    verifyRequiredParams(array('token','email', 'current_status','taxi_fare'));
  
    $tasks_sid = $app->request->post('tasks_sid');
    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $message = $app->request->post('current_status');
    $taxi_fare = $app->request->post('taxi_fare');
    $lat = $app->request->post('lat');
    $lng = $app->request->post('lng');
    if($message!="-1"){
        $new_status = $message+100;
    }else{
        $new_status = "-1";
    }
    if($new_status==500){
        echo fopen("http://vspace.in.th/api/sendmail/sendEmailAfterClosedToCustomer/".$tasks_sid."?email=".$email, "r");
    }
    $response = $db->addNewStatusTaskLog($email, $tasks_sid, $new_status, $email, $taxi_fare, $lat, $lng);
    $response["error"] = false;
    echoRespnse(200, $response);
});

/**
 * Messaging in a chat room
 * Will send push notification using Topic Messaging
 *  */
$app->post('/chat_rooms/:id/message', function($chat_room_id) {
    global $app;
    $db = new DbHandler();
 
    verifyRequiredParams(array('email', 'message','taxi_fare'));
 
    $email = $app->request->post('email');
    $message = $app->request->post('message');
 	$taxi_fare = $app->request->post('taxi_fare');
    $lat = $app->request->post('lat');
    $lng = $app->request->post('lng');
    if($message!="-1"){
 		$new_status = $message+100;
 	}else{
 		$new_status = "-1";
 	}
	$response = $db->addNewStatusTaskLog($email, $chat_room_id, $new_status, $email, $taxi_fare, $lat, $lng);
 		
    // $response = $db->addMessage($user_id, $chat_room_id, $message);
 
    // if ($response['error'] == false) {
    //     require_once __DIR__ . '/../libs/gcm/gcm.php';
    //     require_once __DIR__ . '/../libs/gcm/push.php';
    //     $gcm = new GCM();
    //     $push = new Push();
 
    //     // get the user using userid
    //     $user = $db->getUser($user_id);
 
    //     $data = array();
    //     $data['user'] = $user;
    //     $data['message'] = $response['message'];
    //     $data['chat_room_id'] = $chat_room_id;
 
    //     $push->setTitle("Google Cloud Messaging");
    //     $push->setIsBackground(FALSE);
    //     $push->setFlag(PUSH_FLAG_CHATROOM);
    //     $push->setData($data);
         
    //     // echo json_encode($push->getPush());exit;
 
    //     // sending push message to a topic
    //     $gcm->sendToTopic('topic_' . $chat_room_id, $push->getPush());
 
    //     $response['user'] = $user;
    //     $response['error'] = false;
    // }
    $response["error"] = false;
    echoRespnse(200, $response);
});
 
//when customer signature
$app->post('/receivesignature',function() use ($app) {
    $db = new DbHandler();
    $response = array();
    verifyRequiredParams(array('email', 'tasks_sid'));
    $email = $app->request->post('email');
    $tasks_sid = $app->request->post('tasks_sid');
    // $latitude = $app->request->post('latitude');
    // $longitude = $app->request->post('longitude');

    $base64 = $app->request->post('base64');

    $new_status = 500;
    $response = $db->addNewStatusTaskLog($email, $tasks_sid, $new_status, $email);
    
    $data = base64_decode($base64);
    $file = uniqid() .'_'.$tasks_sid. '.png';
    $file_path = '../signature_customer/'.$file;
    $success = file_put_contents($file_path, $data);

    $db->customer_signature_task($email, $tasks_sid, $file);

    $response["error"] = false;
    echoRespnse(200, $response);
});
 
/**
 * Sending push notification to a single user
 * We use user's gcm registration id to send the message
 * * */
$app->post('/users/:id/message', function($to_user_id) {
    global $app;
    $db = new DbHandler();
 
    verifyRequiredParams(array('message'));
 
    $from_user_id = $app->request->post('user_id');
    $message = $app->request->post('message');
 	$response['error'] = false;
    // $response = $db->addMessage($from_user_id, $to_user_id, $message);
 				$tmp = array();
                $tmp['message_id'] = '1';
                $tmp['chat_room_id'] = '1';
                $tmp['message'] = "12345";
                $tmp['created_at'] = '';
                $response['message'] = $tmp;


    if ($response['error'] == false) {
        require_once __DIR__ . '/../libs/gcm/gcm.php';
        require_once __DIR__ . '/../libs/gcm/push.php';
        $gcm = new GCM();
        $push = new Push();
 
        // $user = $db->getUser($to_user_id);
 		$user = array(
     	"user_id" => "1",
    	"name"=> "Mas",
    	"email"=> "admin@androidhive.info",
    	"gcm_registration_id"=> "dNxCgLd48Q4:APA91bHFj8wNIiiHNeceH8xcEO7mjVtFzd_NqSkBSjoHAW5o5YJmvKM1ua4CtElC6diax2C7G2pKxwmK_qlDX0BypRQ1eJfReT2UknqjWxCyCciZiFeUKSSCC88zcUY_GSeFXHoctSm-",
    	"created_at"=> "2016-02-12 15:13:57"
    	);

        $data = array();
        $data['user'] = $user;
        $data['message'] = $response['message'];
        $data['image'] = '';
        // $data['image'] = 'http://www.androidhive.info/wp-content/uploads/2016/01/Air-1.png';
 
        $push->setTitle("Super service");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_USER);
        $push->setData($data);
 
        // sending push message to single user
        $gcm->send($user['gcm_registration_id'], $push->getPush());
 
        $response['user'] = $user;
        $response['error'] = false;
    }
 
    echoRespnse(200, $response);
});
 
 
/**
 * Sending push notification to multiple users
 * We use gcm registration ids to send notification message
 * At max you can send message to 1000 recipients
 * * */
$app->post('/users/message', function() use ($app) {
 
    $response = array();
    verifyRequiredParams(array('user_id', 'to', 'message'));
 
    require_once __DIR__ . '/../libs/gcm/gcm.php';
    require_once __DIR__ . '/../libs/gcm/push.php';
 
    $db = new DbHandler();
 
    $user_id = $app->request->post('user_id');
    $to_user_ids = array_filter(explode(',', $app->request->post('to')));
    $message = $app->request->post('message');
 
    $user = $db->getUser($user_id);
    $users = $db->getUsers($to_user_ids);
 
    $registration_ids = array();
 
    // preparing gcm registration ids array
    foreach ($users as $u) {
        array_push($registration_ids, $u['gcm_registration_id']);
    }
 
    // insert messages in db
    // send push to multiple users
    $gcm = new GCM();
    $push = new Push();
 
    // creating tmp message, skipping database insertion
    $msg = array();
    $msg['message'] = $message;
    $msg['message_id'] = '';
    $msg['chat_room_id'] = '';
    $msg['created_at'] = date('Y-m-d G:i:s');
 
    $data = array();
    $data['user'] = $user;
    $data['message'] = $msg;
    $data['image'] = '';
 
    $push->setTitle("Google Cloud Messaging");
    $push->setIsBackground(FALSE);
    $push->setFlag(PUSH_FLAG_USER);
    $push->setData($data);
 
    // sending push message to multiple users
    $gcm->sendMultiple($registration_ids, $push->getPush());
 
    $response['error'] = false;
 
    echoRespnse(200, $response);
});
 
$app->post('/users/send_to_all', function() use ($app) {
 
    $response = array();
    verifyRequiredParams(array('user_id', 'message'));
 
    require_once __DIR__ . '/../libs/gcm/gcm.php';
    require_once __DIR__ . '/../libs/gcm/push.php';
 
    $db = new DbHandler();
 
    $user_id = $app->request->post('user_id');
    $message = $app->request->post('message');
 
    // require_once __DIR__ . '/../libs/gcm/gcm.php';
    // require_once __DIR__ . '/../libs/gcm/push.php';

    $gcm = new GCM();
    $push = new Push();
 
    // get the user using userid
    // $user = $db->getUser($user_id);
     $user = array(
     	"user_id" => "1",
    	"name"=> "AndroidHive",
    	"email"=> "admin@androidhive.info",
    	"gcm_registration_id"=> "chXAxyWP2AY:APA91bFvd_G_uhkVCkXShJ4rBxug4-pc2bVHYdCAoNQmrcCvCc0r-gCZh0hZrmVlK09k9-fZyZRT603uiO2hoZWjxfxQP8BbTD3Fk0BcARF3861om5DnR64HJg89CqFKYVMFHsFgpVI1",
    	"created_at"=> "2016-02-12 15:13:57"
    	);
    // creating tmp message, skipping database insertion
    $msg = array();
    $msg['message'] = $message;
    $msg['message_id'] = '';
    $msg['chat_room_id'] = '';
    $msg['created_at'] = date('Y-m-d G:i:s');
 
    $data = array();
    $data['user'] = $user;
    $data['message'] = $msg;
    $data['image'] = 'http://www.androidhive.info/wp-content/uploads/2016/01/Air-1.png';
 
    $push->setTitle("Super service");
    $push->setIsBackground(FALSE);
    $push->setFlag(PUSH_FLAG_USER);
    $push->setData($data);
 
    // sending message to topic `global`
    // On the device every user should subscribe to `global` topic
    $gcm->sendToTopic('global', $push->getPush());
 
    $response['user'] = $user;
    $response['error'] = false;
 
    echoRespnse(200, $response);
});
 
 

/**
 * Fetching single chat room including all the chat messages
 *  */
$app->get('/chat_rooms/:id', function($chat_room_id) {
    global $app;
    $db = new DbHandler();
 
    $result = $db->getChatRoom($chat_room_id);
 
    $response["error"] = false;
    $response["messages"] = array();
    $response['chat_room'] = array();
 
    $i = 0;
    // looping through result and preparing tasks array
    // while ($chat_room = $result->fetch_assoc()) {
        // adding chat room node
    foreach ($result as $key => $chat_room) {
        if ($key == 0) {
            $tmp = array();
            $tmp["chat_room_id"] = $chat_room["chat_room_id"];
            $tmp["name"] = $chat_room["name"];
            $tmp["created_at"] = $chat_room["chat_room_created_at"];
            $response['chat_room'] = $tmp;
        }
 
        if ($chat_room['user_id'] != "") {
            // message node
            $cmt = array();
            $cmt["message"] = $chat_room["message"];
            $cmt["message_id"] = $chat_room["message_id"];
            $cmt["created_at"] = $chat_room["created_at"];
 
            // user node
            $user = array();
            $user['user_id'] = $chat_room['user_id'];
            $user['username'] = $chat_room['username'];
            $cmt['user'] = $user;
 
            array_push($response["messages"], $cmt);
        }

    }
    // }
 
    echoRespnse(200, $response);
});

$app->post('/location',function() use ($app) {
 
    $response = array();
    verifyRequiredParams(array('email', 'latitude','longitude'));
    $email = $app->request->post('email');
    $latitude = $app->request->post('latitude');
    $longitude = $app->request->post('longitude');

    $db = new DbHandler();
    $db->sendLocation($email, $latitude, $longitude);

    $response["error"] = false;
    echoRespnse(200, $response);
});

function base64_to_jpeg($base64_string, $output_file) {
    $ifp = fopen($output_file, "wb"); 

    // $data = explode(',', $base64_string);

    fwrite($ifp, $base64_string); 
    fclose($ifp); 

    return $output_file; 
}
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>