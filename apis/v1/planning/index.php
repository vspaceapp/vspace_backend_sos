<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/PlanningModel.php';
require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->post('/clearOldStandby',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $start_date = $app->request->post('start_date');
    $obj = new PlanningModel($email);
    $res = $obj->clearOldStandby($start_date);

    $response["error"] = false;
    $response["start_date"] = $start_date;
    $response["res"] = $res;
    echoRespnse(200, $response);
});
$app->post('/saveStandby',function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');
    $obj = new PlanningModel($email);
    $res = $obj->saveStandby($data);

    $response["error"] = false;
    $response["data"] = $data;
    $response["res"] = $res;
    echoRespnse(200, $response);
});
$app->post('/timeline', function() use ($app){
    verifyRequiredParams(array('email','token','month'));
    $email = $app->request->post('email');
    $viewEmail = $app->request->post('viewEmail');
    $token = $app->request->post('token');
    $month = $app->request->post('month');
    $year = $app->request->post('year');

    if(($month-1)<1 && 0){
        $monthStart = 12 - ($month);
    }else{
        $monthStart = $month;
    }
    if(($month+1)>12){
        $monthEnd = ($month+1)%12;
    }else{
        $monthEnd = ($month+1);
    }

    if(strlen($monthStart)<2){
        $monthStart = "0".$monthStart;
    }
    if(strlen($monthEnd)<2){
        $monthEnd = "0".$monthEnd;
    }

    $start = $year."-".($monthStart)."-01";
    if($month==12){
        $end = ($year+1)."-".($monthEnd)."-01";
    }else{
        $end = $year."-".($monthEnd)."-01";
    }
    $db = new DbHandler();

    $data = array();

    $obj = new PlanningModel();
    $timeline = $obj->sheduleTimeline($start, $end);
    $leave = array();
    $blockWidth = 8.33;
    foreach ($timeline as $k => $v) {
        if(!isset($timeline[$k]['timeline'])){
            $timeline[$k]['timeline'] = array();
        }
        $timeline[$k]['timeline_real'] = array();
        $timeline[$k]['day'] = $v['date_w'].", ".$v['day'];
        $task = $db->getTasksCompleted($viewEmail, $v['date_Ymd'], $v['next_date_Ymd']);
        foreach ($task as $key => $value) {
            array_push($timeline[$k]['timeline_real'], $value);
            $tmp = array();
            $tmp['name'] = $value['no_task'].", ".$value['contract_no'];//."<br/>Taxi (".$value['taxi_fare_in'].",".$value['taxi_fare_out'].")";
            $tmp['link'] = "/pages/dashboard/#casedetail/".$value['ticket_sid'];
            $tmp['full_name'] = $value['no_task']."<br/>".$value['contract_no']." ".$value['no_ticket']."<br/>".$value['subject']."<br/>".(($value['end_user'])?$value['end_user']:$value['case_company'])."<br/>".$value['appointment_datetime_df']."(".$value['expect_duration'].")<br/>".$value['start_task_timestamp_df']." - ".$value['closed_task_timestamp_df'].(($value['closed_task_timestamp_df'])?"<br/>Taxi (".$value['taxi_fare_in'].",".$value['taxi_fare_out'].")":"");
            $tmp['type'] = "ida practice";
            $tmp['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
            $tmp['_start'] = $value['appointment_time']/2*$blockWidth;
            $tmp['_full_position'] = "";
            if($tmp['_start']>75){
                $tmp['_full_position'] = "left";
            }
            if($tmp['_start']<25){
                $tmp['_full_position'] = "right";
            }
            if($k==0){
                $tmp['_full_position'] = $tmp['_full_position']." bottom";
            }
            if(($k+1)==count($timeline)){
                $tmp['_full_position'] = $tmp['_full_position']." top";
            }
            //Bug Preventive Maintenance
            if($value['expect_duration']<=0){
                $value['expect_duration'] = 2;
            }
            $tmp['_width'] = $value['expect_duration']/2*$blockWidth;
            if($tmp['_start']+$tmp['_width']>100){
                $tmpContinue = array();
                $tmpContinue['_width'] = ($tmp['_start']+$tmp['_width'])-100;
                $tmp['_width'] = 100-$tmp['_start'];
                if(isset($timeline[$k+1])){
                    $tmpContinue['name'] = $value['no_task']." ".$value['contract_no'];
                    $tmpContinue['full_name'] = $value['no_task']."<br/>".$value['contract_no']." ".$value['no_ticket']."<br/>".$value['subject']."<br/>".$value['appointment_datetime_df']."(".$value['expect_duration'].")<br/>".$value['start_task_timestamp_df']." - ".$value['closed_task_timestamp_df'];
                    $tmpContinue['link'] = "/pages/dashboard/#casedetail/".$value['ticket_sid'];
                    $tmpContinue['type'] = "ida practice";
                    $tmpContinue['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
                    $tmpContinue['_start'] = 0;
                    $tmpContinue['_full_position'] = "";
                    if($tmpContinue['_start']<25){
                        $tmpContinue['_full_position'] = "right";
                    }
                    if($k==0){
                        $tmpContinue['_full_position'] = $tmpContinue['_full_position']." bottom";
                    }
                    if(($k+1)==count($timeline)){
                        $tmpContinue['_full_position'] = $tmpContinue['_full_position']." top";
                    }

                    $tmpContinue['_width'] = $tmpContinue['_width'];///2*$blockWidth;
                    $timeline[$k+1]['timeline'] = array();
                    array_push($timeline[$k+1]['timeline'], $tmpContinue);
                }
            }
            array_push($timeline[$k]['timeline'], $tmp);
        }

        $leaveAndBussyList = $obj->leaveAndBussyList($viewEmail, $v['date_Ymd'], $v['date_Ymd']);
        foreach ($leaveAndBussyList['leave'] as $key => $value) {
            array_push($leave, $value);
            $tmp = array();
            $tmp['name'] = $value['description'];
            // $tmp['link'] = "http://case.flgupload.com/pages/dashboard/#casedetail/".$value['ticket_sid'];
            $tmp['full_name'] = $value['description']."".(isset($value['detail'])?"<br/>".$value['detail']:'')."<br/>".$value['starttime']." - ".$value['endtime'];
            $tmp['type'] = "ida practice";
            $tmp['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
            $tmp['_start'] = $value['appointment_time']/2*$blockWidth;
            $tmp['_width'] = $value['expect_duration']/2*$blockWidth;
            $tmp['_full_position'] = "";
            if($tmp['_start']>75){
                $tmp['_full_position'] = "left";
            }
            if($tmp['_start']<25){
                $tmp['_full_position'] = "right";
            }
            if($k==0){
                $tmp['_full_position'] = $tmp['_full_position']." bottom";
            }
            if(($k+1)==count($timeline)){
                $tmp['_full_position'] = $tmp['_full_position']." top";
            }
            array_push($timeline[$k]['timeline'], $tmp);
            array_push($timeline[$k]['timeline_real'], $value);
        }

        foreach ($leaveAndBussyList['oldTask'] as $key => $value) {
     	    array_push($data, $value);
     	    $tmp = array();
            $tmp['name'] = $value['SRID'].", ".$value['contract_no'];
            // $tmp['link'] = "http://case.flgupload.com/pages/dashboard/#casedetail/".$value['ticket_sid'];
            $tmp['full_name'] = $value['SRID'].", ".$value['contract_no']."<br/>".$value['request_no']."<br/>".$value['customer_company_name']."<br/>Appointment ".$value['tooltip_appointment_time']." - ".$value['tooltip_expect_time']."<br/>Timestamp ".$value['tooltip_start_time']." - ".$value['tooltip_close_job_time'];
            $tmp['type'] = "ida practice";
            $tmp['rang'] = 'rang-'.ceil(($value['number_hour']/2));
            $tmp['_start'] = $value['appointment_h']/2*$blockWidth;
            $tmp['_width'] = $value['number_hour']/2*$blockWidth;
            $tmp['_full_position'] = "";
            if($tmp['_start']>75){
                $tmp['_full_position'] = "left";
            }
            if($tmp['_start']<25){
                $tmp['_full_position'] = "right";
            }
            if($k==0){
                $tmp['_full_position'] = $tmp['_full_position']." bottom";
            }
            if(($k+1)==count($timeline)){
                $tmp['_full_position'] = $tmp['_full_position']." top";
            }
            array_push($timeline[$k]['timeline'], $tmp);
            array_push($timeline[$k]['timeline_real'], $value);
    	}
        $taskLength = count($timeline[$k]['timeline']);
        if($taskLength>1){
            foreach ($timeline[$k]['timeline'] as $key => $value) {
                $timeline[$k]['timeline'][$key]['_height'] = 100/$taskLength;
                $timeline[$k]['timeline'][$key]['_top'] = $key*100/$taskLength;
            }
        }

    }

    // $data = array();
    $response["error"] = false;
    $response["data"] = $data;
    if($email==$viewEmail){
        $response['timeline'] = $timeline;
    }else{
        $response['timeline'] = array();
    }
    $response['leave']= $leave;
    echoRespnse(200, $response);
});


$app->post('/timesheet', function() use ($app){
    verifyRequiredParams(array('email','token','month'));
    $email = $app->request->post('email');
    $viewEmail = $app->request->post('viewEmail');
    $token = $app->request->post('token');
    $month = $app->request->post('month');
    $year = $app->request->post('year');

    if(($month-1)<1 && 0){
        $monthStart = 12 - ($month);
    }else{
        $monthStart = $month;
    }
    if(($month+1)>12){
        $monthEnd = ($month+1)%12;
    }else{
        $monthEnd = ($month+1);
    }

    if(strlen($monthStart)<2){
        $monthStart = "0".$monthStart;
    }
    if(strlen($monthEnd)<2){
        $monthEnd = "0".$monthEnd;
    }

    $start = $year."-".($monthStart)."-01";
    if($month==12){
        $end = ($year+1)."-".($monthEnd)."-01";
    }else{
        $end = $year."-".($monthEnd)."-01";
    }
    $db = new DbHandler();

    $data = array();

    $obj = new PlanningModel();
    $timeline = $obj->sheduleTimeline($start, $end, 1);
    $leave = array();
    $blockWidth = 8.33;
    foreach ($timeline as $k => $v) {
        if(!isset($timeline[$k]['timeline'])){
            $timeline[$k]['timeline'] = array();
        }
        // $timeline[$k]['timeline_real'] = array();
        // $timeline[$k]['day'] = $v['date_w'].", ".$v['day'];
        $task = $db->getTasksCompleted($viewEmail, $v['date_Ymd'], $v['next_date_Ymd']);
        foreach ($task as $key => $value) {
            // array_push($timeline[$k]['timeline_real'], $value);
            $tmp = array();
            $tmp['service_no'] = $value['no_task'];
            $tmp['subject'] = $value['subject'];
            $tmp['case_no'] = $value['no_ticket'];
            $tmp['startTime'] = $value['appointment_datetime'];
            $tmp['endTime'] = $value['expect_finish_datetime'];
            $tmp['contract_no'] = $value['contract_no'];
            $tmp['ot'] = $value['overtime_expect'];
            $tmp['type'] = "service";
            array_push($timeline[$k]['timeline'], $tmp);
        }

        $leaveAndBussyList = $obj->leaveAndBussyList($viewEmail, $v['date_Ymd'], $v['date_Ymd']);
        foreach ($leaveAndBussyList['leave'] as $key => $value) {
            array_push($leave, $value);
            $tmp = array();
            $tmp['subject'] = $value['description'];
            $tmp['service_no'] = "";

            $tmp['case_no'] = "";
            $tmp['startTime'] = $value['starttime'];
            $tmp['endTime'] = $value['endtime'];
            $tmp['detail'] = $value["detail"];
            $tmp['contract_no'] = "";
            $tmp['ot'] = "";
            $tmp['type'] = $value['group_master'];
            // $tmp['link'] = "http://case.flgupload.com/pages/dashboard/#casedetail/".$value['ticket_sid'];
            // $tmp['full_name'] = $value['description']."".(isset($value['detail'])?"<br/>".$value['detail']:'')."<br/>".$value['starttime']." - ".$value['endtime'];
            // $tmp['type'] = "ida practice";
            // $tmp['rang'] = 'rang-'.ceil(($value['expect_duration']/2));
            // $tmp['_start'] = $value['appointment_time']/2*$blockWidth;
            // $tmp['_width'] = $value['expect_duration']/2*$blockWidth;
            // $tmp['_full_position'] = "";
            // if($tmp['_start']>75){
            //     $tmp['_full_position'] = "left";
            // }
            // if($tmp['_start']<25){
            //     $tmp['_full_position'] = "right";
            // }
            // if($k==0){
            //     $tmp['_full_position'] = $tmp['_full_position']." bottom";
            // }
            // if(($k+1)==count($timeline)){
            //     $tmp['_full_position'] = $tmp['_full_position']." top";
            // }
            if(is_array($timeline[$k]['timeline'])){
                array_push($timeline[$k]['timeline'], $tmp);
                if(isset($timeline[$k]['timeline_real']) && is_array($timeline[$k]['timeline_real'])){
                    array_push($timeline[$k]['timeline_real'], $value);
                }
            }
        }

        // foreach ($leaveAndBussyList['oldTask'] as $key => $value) {
        //     array_push($data, $value);
        //     $tmp = array();
        //     $tmp['name'] = $value['SRID'].", ".$value['contract_no'];
        //     // $tmp['link'] = "http://case.flgupload.com/pages/dashboard/#casedetail/".$value['ticket_sid'];
        //     $tmp['full_name'] = $value['SRID'].", ".$value['contract_no']."<br/>".$value['request_no']."<br/>".$value['customer_company_name']."<br/>Appointment ".$value['tooltip_appointment_time']." - ".$value['tooltip_expect_time']."<br/>Timestamp ".$value['tooltip_start_time']." - ".$value['tooltip_close_job_time'];
        //     $tmp['type'] = "ida practice";
        //     $tmp['rang'] = 'rang-'.ceil(($value['number_hour']/2));
        //     $tmp['_start'] = $value['appointment_h']/2*$blockWidth;
        //     $tmp['_width'] = $value['number_hour']/2*$blockWidth;
        //     $tmp['_full_position'] = "";
        //     if($tmp['_start']>75){
        //         $tmp['_full_position'] = "left";
        //     }
        //     if($tmp['_start']<25){
        //         $tmp['_full_position'] = "right";
        //     }
        //     if($k==0){
        //         $tmp['_full_position'] = $tmp['_full_position']." bottom";
        //     }
        //     if(($k+1)==count($timeline)){
        //         $tmp['_full_position'] = $tmp['_full_position']." top";
        //     }
        //     array_push($timeline[$k]['timeline'], $tmp);
        //     array_push($timeline[$k]['timeline_real'], $value);
        // }
        // $taskLength = count($timeline[$k]['timeline']);
        // if($taskLength>1){
        //     foreach ($timeline[$k]['timeline'] as $key => $value) {
        //         $timeline[$k]['timeline'][$key]['_height'] = 100/$taskLength;
        //         $timeline[$k]['timeline'][$key]['_top'] = $key*100/$taskLength;
        //     }
        // }

    }

    // $data = array();
    $response["error"] = false;
    $response["data"] = $data;
    if($email==$viewEmail || 1){
        $response['timeline'] = $timeline;
    }else{
        $response['timeline'] = array();
    }
    // $response['leave']= $leave;
    echoRespnse(200, $response);
});

$app->post('/leave', function() use ($app){
    verifyRequiredParams(array('email','token','month'));
    $email = $app->request->post('email');
    $viewEmail = $app->request->post('viewEmail');
    $token = $app->request->post('token');
    $month = $app->request->post('month');
    $year = $app->request->post('year');

    if(($month-1)<1 && 0){
        $monthStart = 12 - ($month);
    }else{
        $monthStart = $month;
    }
    if(($month+1)>12){
        $monthEnd = ($month+1)%12;
    }else{
        $monthEnd = ($month+1);
    }

    if(strlen($monthStart)<2){
        $monthStart = "0".$monthStart;
    }
    if(strlen($monthEnd)<2){
        $monthEnd = "0".$monthEnd;
    }

    $start = $year."-".($monthStart)."-01";
    if($month==12){
        $end = ($year+1)."-".($monthEnd)."-01";
    }else{
        $end = $year."-".($monthEnd)."-01";
    }
    $db = new DbHandler();

    $data = array();

    $obj = new PlanningModel();
    $timeline = $obj->sheduleTimeline($start, $end, 1);
    $leave = array();
    $leave_log = array();
    $blockWidth = 8.33;
    foreach ($timeline as $k => $v) {

        $leaveAndBussyList = $obj->leaveAndBussyList($viewEmail, $v['date_Ymd'], $v['date_Ymd']);
        foreach ($leaveAndBussyList['leave'] as $key => $value) {
            if($value['group_master']=="leave"){
                array_push($leave_log, $value);
                $tmp = array();
                $tmp['date_leave'] = $value['date_busy'];
                // $tmp['subject'] = "";
                // $tmp['type'] = $value["detail"];
                $tmp['name'] = $value['name'];
                $tmp['detail'] = $value["detail"];
                $tmp['start_time'] = $value['busy_start_time'];
                $tmp['end_time'] = $value['busy_end_time'];
                array_push($leave, $tmp);
            }
        }
    }

    // $data = array();
    $response["error"] = false;
    $response["data"] = $data;
    // if($email==$viewEmail || 1){
    //     $response['timeline'] = $timeline;
    // }else{
    //     $response['timeline'] = array();
    // }
    $response['leave']= $leave;
    // $response['leave_log']= $leave_log;
    echoRespnse(200, $response);
});


$app->post('/generate7x24', function() use ($app){
    verifyRequiredParams(array('email','start','end'));
    $email = $app->request->post('email');
    $start = $app->request->post('start');
    $end = $app->request->post('end');
    $engineer_no = $app->request->post('engineer_no');
    $continue_holiday = $app->request->post('continue_holiday');
    $switch_call = $app->request->post('switch_call');
    $engineer_no_holiday = $app->request->post('engineer_no_holiday');

    $obj = new PlanningModel();
    $data = $obj->generate7x24($start, $end, $engineer_no,$switch_call, $continue_holiday,$engineer_no_holiday);
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/buddy', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');
    $obj = new PlanningModel();
    $data = $obj->buddy();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});
$app->post('/standby', function() use ($app){

    $planning = new PlanningModel();
    $ace = $planning->standbyAce();
    $s7x24 =$planning->standby7x24();
    $standbyOtherProduct =$planning->standbyOtherProduct();

    $response["error"] = false;
    $response['standby7x24'] = $s7x24;
    $response['ace'] = $ace;
    $response['otherProduct'] = $standbyOtherProduct;

    // echo json response
    echoRespnse(200, $response);

});
$app->post('/planning',function() use ($app){

	verifyRequiredParams(array('day'));
    $day = $app->request->post('day');

    $planningDayView = new PlanningModel();
    $planningDayViewData = $planningDayView->planningDayView($day);
    $response = array();

    $response["error"] = false;
    $response['data'] = $planningDayViewData;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/sheduleStandby', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');

    $planningObj = new PlanningModel();
    $data = $planningObj->sheduleStandby();
    $standby = $planningObj->standby7x24();

    $response["error"] = false;
    $response["data"] = $data;
    $response["standby"] = $standby;
    echoRespnse(200,$response);
});

$app->post('/sheduleStandbyReact', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');

    $planningObj = new PlanningModel();
    $data = $planningObj->sheduleStandbyReact();
    $standby = $planningObj->standby7x24();

    $response["error"] = false;
    $response["data"] = $data;
    $response["standby"] = $standby;
    echoRespnse(200,$response);
});

$app->post('/sheduleStandbyForCallcenter', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');

    $planningObj = new PlanningModel();
    $data = $planningObj->sheduleStandbyForCallcenter();
    $standby = $planningObj->standby7x24();

    $response["error"] = false;
    $response["data"] = $data;
    $response["standby"] = $standby;
    echoRespnse(200,$response);
});




$app->post('/whereEmployee', function() use ($app){
    $planningDayView = new PlanningModel();
    $data = $planningDayView->whereEmployee();
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/get_data',function() use ($app){
    verifyRequiredParams(array('subject_sid'));
    $subject_sid = $app->request->post('subject_sid');

    $actionplanModel = new ActionPlanModel();
    $actionPlanView = $actionplanModel->selectActionPlanBySubject( $subject_sid );
    $response = array();

    $response["error"] = false;
    $response['data'] = $actionPlanView;

    // echo json response
    echoRespnse(200, $response);
});


// ดึงข้อมูล serial ของ ticket
$app->post('/select/actionplan', function() use ($app) {
    // check for required params
    // verifyRequiredParams(array('ticket_sid'));

    // reading post params
    // $ticket_sid = $app->request->post('ticket_sid');


    $actionplanModel = new ActionPlanModel();
    $actionPlan = $actionplanModel->selectActionPlanSubject();


    $response = array();

    $response["error"] = false;
    $response['data'] = $actionPlan;

    // echo json response
    echoRespnse(200, $response);
});

// ดึงข้อมูลเพื่อแสดงว่า ticket ให้ทำ PM อะไรบ้าง
$app->post('/save', function() use ($app) {
    // check for required params
    // verifyRequiredParams(array('description'));

    $response = array();
    $response["error"] = false;
    // reading post params
    $description = $app->request->post('description');
    $cause = $app->request->post('cause');
    $solution = $app->request->post('solution');
    $expect = $app->request->post('expect');
    $jsonText = $app->request->post('jsonText');
    $email = $app->request->post('email');
    $action_plan_subject_sid = $app->request->post('action_plan_subject_sid');

    $actionplanModel = new ActionPlanModel();
    if($description!=""){

        if($action_plan_subject_sid>0){
            $action_plan_subject_sid = $actionplanModel->updateActionPlanSubject($description,$cause,$solution,$expect, $email, $action_plan_subject_sid);
        }else {
            $action_plan_subject_sid = $actionplanModel->saveActionPlanSubject($description,$cause,$solution,$expect, $email);
        }

        if($jsonText!=""){
            $json = json_decode($jsonText);
            foreach ($json as $key => $value) {
                $actionplanModel->saveActionPlanDetail(
                    $action_plan_subject_sid,
                    $value->duration,
                    $value->action,
                    $value->command,
                    $value->objective,
                    $value->expectedResult,
                    $value->responsibility,
                    "",
                    $value->status,
                    ($key+1)
                );
                // echo "action_plan_subject_sid ".$action_plan_subject_sid;
            }
        }
    }
    $response['json'] = $json;
    $response['action_plan_subject_sid'] = $action_plan_subject_sid;
    // echo json response
    echoRespnse(200, $response);
});



$app->post('/insert/input_action', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('task_sid','problem','solution','recommend','email'));

    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email);

    $result = $pmModel->insertInputAction($param);


    $response = array();
    $response['result'] = $result;

    // echo json response
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;

//     verifyRequiredParams(array('gcm_registration_id'));

//     $gcm_registration_id = $app->request->put('gcm_registration_id');

//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);

//     echoRespnse(200, $response);
// });


/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
