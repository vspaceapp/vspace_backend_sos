<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ActionPlanModel.php';
require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/deleteit',function() use ($app){
    verifyRequiredParams(array('subject_sid'));
    $subject_sid = $app->request->post('subject_sid');

    $actionplanModel = new ActionPlanModel();
    $actionPlanView = $actionplanModel->deleteActionplan( $subject_sid );
    $response = array();
    
    $response["error"] = false;
    $response['data'] = $actionPlanView;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/get_data',function() use ($app){
    verifyRequiredParams(array('subject_sid'));
    $subject_sid = $app->request->post('subject_sid');

    $actionplanModel = new ActionPlanModel();
    $actionPlanView = $actionplanModel->selectActionPlanBySubject( $subject_sid );
    $response = array();
    
    $response["error"] = false;
    $response['data'] = $actionPlanView;

    // echo json response
    echoRespnse(200, $response);
});



$app->post('/select/actionplan', function() use ($app) {
    // check for required params
    // verifyRequiredParams(array('ticket_sid'));
 
    // reading post params
    // $ticket_sid = $app->request->post('ticket_sid');


    $actionplanModel = new ActionPlanModel();
    $actionPlan = $actionplanModel->selectActionPlanSubject();


    $response = array();
    
    $response["error"] = false;
    $response['data'] = $actionPlan;

    // echo json response
    echoRespnse(200, $response);
});
 

$app->post('/save', function() use ($app) {
    // check for required params
    // verifyRequiredParams(array('description'));
    
    $response = array();
    $response["error"] = false;
    // reading post params
    $data = $app->request->post('data');
    $email = $app->request->post('email');
    $action_plan_subject_sid = $app->request->post('action_plan_subject_sid');
    $data = json_decode($data, true);
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    // $description = $app->request->post('description');
    // $cause = $app->request->post('cause');
    // $solution = $app->request->post('solution');
    // $expect = $app->request->post('expect');
    // $jsonText = $app->request->post('jsonText');
    // 

        $actionplanModel = new ActionPlanModel();
        if($action_plan_subject_sid>0){
            $action_plan_subject_sid = $actionplanModel->updateActionPlanSubject(
                $data['description'],
                $data['cause'],
                $data['solution'],
                $data['expect'], 
                $email, 
                $action_plan_subject_sid);
        }else {
            if($data['description']!=""){
                $action_plan_subject_sid = $actionplanModel->saveActionPlanSubject(
                    $data['description'],
                    $data['cause'],
                    $data['solution'],
                    $data['expect'], $email);
            }
        }

 
            foreach ($data['dataSheet'] as $key => $value) {
                $task_sid = isset($value['task_sid'])?$value['task_sid']:"";
                // print_r($value);
                $data['dataSheet'][$key]['task_sid'] = $task_sid = $actionplanModel->saveActionPlanTask(
                    $value['taskName'],
                    $action_plan_subject_sid,
                    $task_sid, 
                    $email
                );

                foreach ($value['data'] as $k => $v) {
                    $actionplanModel->saveActionPlanDetail(
                        $task_sid, 
                        $v[0],
                        $v[1],
                        $v[2],
                        $v[3],
                        $v[4],
                        $v[5],
                        $v[6],
                        $v[7],
                        ($k+1)
                    );
                }
            }
  
    $response['data'] = json_encode($data);
    $response['action_plan_subject_sid'] = $action_plan_subject_sid;
    // echo json response
    echoRespnse(200, $response);
});



$app->post('/insert/input_action', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('task_sid','problem','solution','recommend','email'));
 
    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email);

    $result = $pmModel->insertInputAction($param);


    $response = array();
    $response['result'] = $result;
    
    // echo json response
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>