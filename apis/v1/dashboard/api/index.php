<?php
require_once '../../include/DashboardModel.php';
require_once '../../include/TokenModel.php';
require_once __dir__.'/../../../include/RoleModel.php';


$index = function () use ($app) {
  try{
      parse_str($app->request()->getBody(),$request);
      $data = array();
      $response = array();
      $obj = new DashboardModel();
      if(isset($request['email']) && $request['token']){
        $response['data'] = $obj->dashboardWeb($request);
      }
      $response['request'] = $request;
      echoRespnse(200, $response);
  }catch(Exception $e){
      echo $e;
      echoRespnse(500, $response['message']=$e);
  }
};
$getDashboard = function () use ($app) {
  try{
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');
        if (!empty($email) && !empty($token)){
          $tokenObj = new TokenModel();
          $info = $tokenObj->info($token,$email);
          if(isset($info['role_sid'])){
                $roleModel = new RoleModel();
                $roleModel->email = $email;
                $can_view_dashboard =  $roleModel->checkServicesPermission('permission_view_dashboard');
                if ($can_view_dashboard) {
                    $objDashboard = new DashboardModel();
                  $response = array();
                  $res = array();
                  $data = $objDashboard->getQuantityTicket();
                  $sum_status = $objDashboard->getSumStatusTicket();
                  $serv1 = array();
                  $serv2 = array();
                  $serv3 = array();
                  $sumtk = array();
                  foreach ($data as $key => $value) {
                    $i = 6;
                    foreach ($value as $key => $val) {
                      if ($i == 24) {
                        $i = 6;
                        if ($key == 'serv1_'.$i) {
                            array_push($serv1, intval($val));
                        }
                        if ($key == 'serv2_'.$i) {
                            array_push($serv2, intval($val));
                        }
                        if ($key == 'serv3_'.$i) {
                            array_push($serv3, intval($val));
                        }
                        if ($key == 'sum_tk'.$i) {
                            array_push($sumtk, intval($val));
                        }
                        $i++;
                      }else{
                        if ($key == 'serv1_'.$i) {
                            array_push($serv1, intval($val));
                        }
                        if ($key == 'serv2_'.$i) {
                            array_push($serv2, intval($val));
                        }
                        if ($key == 'serv3_'.$i) {
                            array_push($serv3, intval($val));
                        }
                        if ($key == 'sum_tk'.$i) {
                            array_push($sumtk, intval($val));
                        }
                        $i++;
                      }
                    }
                  }
                  $response["error"] = false;
                  $response["message"] = 'Successfully';
                  $response['serv1'] = $serv1;
                  $response['serv2'] = $serv2;
                  $response['serv3'] = $serv3;
                  $response['sumtk'] = $sumtk;
                  $response['sum_status'] = $sum_status;
                  echoRespnse(200, $response);
                }else{
                  $response["error"] = true;
                  $response["message"] = 'You have not permission ';
                  echoRespnse(403, $response);
                }
            }else{
              $response["error"] = true;
              $response["message"] = 'You have Unauthorized';
              echoRespnse(401, $response);
            }
      }else {
        $response["error"] = true;
        $response["message"] = 'You have not require field';
        echoRespnse(400, $response);
      }
  }catch(Exception $e){
      echoRespnse(500, $response['message']=$e);
  }
};

$getDashboardTeam = function () use ($app) {
  try {
    $email = $app->request()->post('email');
    $token = $app->request()->post('token');

    if (!empty($email) && !empty($token)){
            $tokenObj = new TokenModel();
            $info = $tokenObj->info($token,$email);
            if(isset($info['role_sid'])){
                $roleModel = new RoleModel();
                $roleModel->email = $email;
                $can_view_dashboard  = $roleModel->checkServicesPermission('permission_view_dashboard');
                    if ($can_view_dashboard) {
                          $objDashboard = new DashboardModel();
                           $team = array();
                           $assign = array();
                           $in_progress = array();
                           $pending = array();

                           $data = $objDashboard->getTicketTeam();
                           foreach ($data as $key => $value) {
                                   array_push($team,$value['team']);
                                   array_push($assign,intval($value['Assigned']));
                                   array_push($in_progress,intval($value['In_progress']));
                                   array_push($pending,intval($value['Pending']));
                             }
                           $response["error"] = false;
                             $response["message"] = 'Successfully';
                           $response['team'] = $team;
                           $response['assign'] = $assign;
                           $response['in_progress'] = $in_progress;
                           $response['pending'] = $pending;
                           echoRespnse(200, $response);
                    }else{
                      $response["error"] = true;
                      $response["message"] = 'You have not permission ';
                      echoRespnse(403, $response);
                    }
            }else{
               $response["error"] = true;
               $response["message"] = 'You have Unauthorized';
               echoRespnse(401, $response);
            }
      }else {
        $response["error"] = true;
        $response["message"] = 'You have not require field';
        echoRespnse(400, $response);
      }
    }catch(Exception $e){
        echoRespnse(500, $response['message']=$e);
    }
};

  $getDashboardAlertSLA = function () use ($app) {
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      if (!empty($email) && !empty($token)){
              $tokenObj = new TokenModel();
              $info = $tokenObj->info($token,$email);
              if(isset($info['role_sid'])){
                  $roleModel = new RoleModel();
                  $roleModel->email = $email;
                  $can_view_dashboard  = $roleModel->checkServicesPermission('permission_view_dashboard');
                      if ($can_view_dashboard) {
                            $objDashboard = new DashboardModel();
                            $data = $objDashboard->getAlertSLA();
                          if ($data) {
                            $response["error"] = false;
                            $response["message"] = 'Successfully';
                            $response['data'] = $data;
                            echoRespnse(200, $response);
                          }else {
                            $response["error"] = false;
                            $response["message"] = 'Successfully';
                            $response['data'] = null;
                            echoRespnse(200, $response);
                          }

                      }else{
                        $response["error"] = true;
                        $response["message"] = 'You have not permission ';
                        echoRespnse(403, $response);
                      }
              }else{
                 $response["error"] = true;
                 $response["message"] = 'You have Unauthorized';
                 echoRespnse(401, $response);
              }
        }else {
          $response["error"] = true;
          $response["message"] = 'You have not require field';
          echoRespnse(400, $response);
        }
      }catch(Exception $e){
          echoRespnse(500, $response['message']=$e);
      }
  };

  $getDashboardTeamSC = function () use ($app){
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      if (!empty($email) && !empty($token)){
              $tokenObj = new TokenModel();
              $info = $tokenObj->info($token,$email);
              if(isset($info['role_sid'])){
                  $roleModel = new RoleModel();
                  $roleModel->email = $email;
                  $can_view_dashboard  = $roleModel->checkServicesPermission('permission_view_dashboard');
                      if ($can_view_dashboard) {
                            $objDashboard = new DashboardModel();
                            $data = $objDashboard->getTicketTeamSC();
                            $name = array();
                            $assign = array();
                            $in_progress = array();
                            $pending = array();


                             foreach ($data as $key => $value) {
                                     array_push($name,$value['name']);
                                     array_push($assign,intval($value['Assigned']));
                                     array_push($in_progress,intval($value['In_progress']));
                                     array_push($pending,intval($value['Pending']));
                             }
                             $response["error"] = false;
                             $response["message"] = 'Successfully';
                             $response['name'] = $name;
                             $response['Assigned'] = $assign;
                             $response['In_progress'] = $in_progress;
                             $response['Pending'] = $pending;
                             echoRespnse(200, $response);
                      }else{
                        $response["error"] = true;
                        $response["message"] = 'You have not permission ';
                        echoRespnse(403, $response);
                      }
              }else{
                 $response["error"] = true;
                 $response["message"] = 'You have Unauthorized';
                 echoRespnse(401, $response);
              }
        }else {
          $response["error"] = true;
          $response["message"] = 'You have not require field';
          echoRespnse(400, $response);
        }
      }catch(Exception $e){
          echoRespnse(500, $response['message']=$e);
      }
  };

  $getDashboardCountTeamSLA = function () use ($app){
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      if (!empty($email) && !empty($token)){
              $tokenObj = new TokenModel();
              $info = $tokenObj->info($token,$email);
              if(isset($info['role_sid'])){
                  $roleModel = new RoleModel();
                  $roleModel->email = $email;
                  $can_view_dashboard  = $roleModel->checkServicesPermission('permission_view_dashboard');
                      if ($can_view_dashboard) {
                            $objDashboard = new DashboardModel();
                            $data = $objDashboard->getSumAlertSLATeam();
                            $team = array();
                            $ticket = array();
                             foreach ($data as $key => $value) {
                                     array_push($team,$value['team']);
                                     array_push($ticket,intval($value['ticket']));
                             }
                             $response["error"] = false;
                             $response["message"] = 'Successfully';
                             $response['team'] = $team;
                             $response['ticket'] = $ticket;
                             echoRespnse(200, $response);
                      }else{
                        $response["error"] = true;
                        $response["message"] = 'You have not permission ';
                        echoRespnse(403, $response);
                      }
              }else{
                 $response["error"] = true;
                 $response["message"] = 'You have Unauthorized';
                 echoRespnse(401, $response);
              }
        }else {
          $response["error"] = true;
          $response["message"] = 'You have not require field';
          echoRespnse(400, $response);
        }
      }catch(Exception $e){
          echoRespnse(500, $response['message']=$e);
      }
  };

  $getDashboardSeverity1 = function () use ($app) {
    try {
      $email = $app->request()->post('email');
      $token = $app->request()->post('token');
      if (!empty($email) && !empty($token)){
              $tokenObj = new TokenModel();
              $info = $tokenObj->info($token,$email);
              if(isset($info['role_sid'])){
                  $roleModel = new RoleModel();
                  $roleModel->email = $email;
                  $can_view_dashboard  = $roleModel->checkServicesPermission('permission_view_dashboard');
                      if ($can_view_dashboard) {
                            $objDashboard = new DashboardModel();
                            $data = $objDashboard->getSeverityTicket();
                          if ($data) {
                            $response["error"] = false;
                            $response["message"] = 'Successfully';
                            $response['data'] = $data;
                            echoRespnse(200, $response);
                          }else {
                            $response["error"] = false;
                            $response["message"] = 'Successfully';
                            $response['data'] = null;
                            echoRespnse(200, $response);
                          }

                      }else{
                        $response["error"] = true;
                        $response["message"] = 'You have not permission ';
                        echoRespnse(403, $response);
                      }
              }else{
                 $response["error"] = true;
                 $response["message"] = 'You have Unauthorized';
                 echoRespnse(401, $response);
              }
        }else {
          $response["error"] = true;
          $response["message"] = 'You have not require field';
          echoRespnse(400, $response);
        }
      }catch(Exception $e){
          echoRespnse(500, $response['message']=$e);
      }
  };


?>
