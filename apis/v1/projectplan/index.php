<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ProjectModel.php';
require_once '../../include/PlanningModel.php';
require_once '../../include/ERequestModel.php';
require_once '../../include/GenNoCaseSR.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/historyAppointment', function() use ($app){

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $windowHeight = $app->request->post('windowHeight');
    $start = $app->request->post('start');
    $end = $app->request->post('end');
    $staff = $app->request->post('staff');

    $year = "2017";
    $month = "01";
    require_once '../../include/CaseModel.php';
    // if($app->request->post('year')){
    //     $year = $app->request->post('year');
    // }
    // if($app->request->post('month')){
    //     $month = $app->request->post('month');
    // }

    $obj = new CaseModel($email);

    $filter = array('year'=>$year,'month'=>$month, 'start'=>$start, 'end'=>$end,'staff'=>$staff);
    $data = $obj->taxiRequest($email, $filter);

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/ticketCreate', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $data = $app->request->post('data');
    // $obj = new CaseModel();
    // $data = $obj->receiveCasemanagementCreateCases($storage,$email);
    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/ticketDetail', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $ticket_sid = $app->request->post('ticket_sid');
    $obj = new ProjectModel();

    $data = $obj->ticketDetail($ticket_sid,$email);
    $canAssignTo = $obj->listUserCanAddProject($email);

    $response = array();
    $response["error"] = false;
    $response['canAssignTo'] = $canAssignTo;
    $response["data"] = $data;
    echoRespnse(200, $response);
    
});
$app->post('/tasks', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $ticket_sid = $app->request->post('ticket_sid');

    $obj = new ProjectModel($email);
    $data = $obj->listTaskOfCase($ticket_sid);
    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});
$app->post('/projectlist', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $order_by = $app->request->post('order_by');
    $search = $app->request->post('search');
    $windowHeight = $app->request->post('windowHeight');

    require_once '../../view/ProjectManagementView.php';
    require_once '../../include/db_handler.php';
    require_once '../../include/CaseModel.php';
    $response = array();
    $data = array();
    $t = array();
    $appointment = array();
    $db = new DbHandler();
    $obj = new ProjectModel($email);
    $caseModel = new CaseModel($email);
    
    $permission_role = $obj->permission_role;
    $canAssignTo = $obj->listUserCanAddProject($email);
    if($app->request->post('selectedIndex')){
        $selectedIndex = $app->request->post('selectedIndex');
        if($selectedIndex==1){
            $filter_case = "'Incident','Request','Question','Implement','Install','Preventive Maintenance'";
            $options = array(
                    'case_type'=>$filter_case,
                    'sort'=>'DESC'
            );
            $sort = array('name'=>'T.create_datetime','type'=>'DESC');
            if($permission_role['permission_ticket']==""){
                $permission_role['permission_ticket'] = "0";
            }
            $t = $obj->ticketItem($email,"1,2,3,4,7,8,9,10", $permission_role['permission_ticket'], $search);
        }else if($selectedIndex==2){
            $appointment = $db->listTaskWorking($email, $search);
        }else{
            $data = $obj->listProject($email, $token, $order_by, $search);
        }
    }else{
        $appointment = $db->listTaskWorking($email, $search);
        $data = $obj->listProject($email, $token, $order_by, $search);
        $filter_case = "'Incident','Request','Question','Implement','Install','Preventive Maintenance'";
        $options = array(
                'case_type'=>$filter_case,
                'sort'=>'DESC'
        );
        $sort = array('name'=>'T.create_datetime','type'=>'DESC');
        if($permission_role['permission_ticket']==""){
            $permission_role['permission_ticket'] = "0";
        }
        $t = $obj->ticketItem($email,"1,2,3,4,7,8,9,10", $permission_role['permission_ticket'], $search);
    }
    
    $response["error"] = false;
    $response['data'] = $data;
    $response['permission_role'] = $permission_role;
    $response['canAssignTo'] = $canAssignTo;
    $response['a'] = $appointment;
    $response['t'] = $t;
    
    echoRespnse(200, $response);
});

$app->post('/addChecklist', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $new_checklist = $app->request->post('new_checklist');
    $ticket_sid = $app->request->post('ticket_sid');
    $unit = $app->request->post('unit');
    $within = $app->request->post('within');
    $obj = new ProjectModel($email);
    $checklist = $obj->createChecklist($email, $ticket_sid, $new_checklist, $within, $unit);
    $response = array();
    $response["error"] = false;
    $response['checklist'] = $checklist;
    echoRespnse(200, $response);
});

$app->post('/removeChecklist', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $ticket_sid = $app->request->post('ticket_sid');
    $checklist_sid = $app->request->post('checklist_sid');

    $obj = new ProjectModel($email);
    $checklist = $obj->deleteChecklist($email,$ticket_sid, $checklist_sid);

    $response = array();
    $response["error"] = false;
    $response['checklist'] = $checklist;
    echoRespnse(200, $response);
});

$app->post('/updateChecklist', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $ticket_sid = $app->request->post('ticket_sid');
    $checklist_sid = $app->request->post('checklist_sid');
    $new_checklist = $app->request->post('new_checklist');
    $unit = $app->request->post('unit');
    $within = $app->request->post('within');

    $obj = new ProjectModel($email);
    $checklist = $obj->updateChecklist($email, $ticket_sid, $new_checklist, $checklist_sid,$within, $unit);

    $response = array();
    $response["error"] = false;
    $response['checklist'] = $checklist;
    echoRespnse(200, $response);
});

$app->post('/doChecklist', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $checklist_sid = $app->request->post('checklist_sid');
    $value = $app->request->post('value');

    $obj = new ProjectModel($email);
    $request = $obj->doChecklist($email, $checklist_sid, $value);
    $response = array();
    $response["error"] = false;
    $response["data"] = array('checklist_sid'=>$checklist_sid,'value'=>$value, 'result'=>$request);
    echoRespnse(200, $response);
});


$app->post('/projectdetail', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $project_sid = $app->request->post('project_sid');
    $contract_no = "";
    // $search = $app->request->post('search');
    $tabName = $app->request->post('tabName');

    require_once '../../view/ProjectManagementView.php';
    
    $obj = new ProjectModel();
    $listUserCanAddProject = $obj->listUserCanAddProject($email);

    $project_detail = $obj->projectDetail($project_sid,$email, $token);

    $dataTypeCase = $obj->listCaseOfProjectByType($project_detail['project_detail']['project_sid']);

    foreach ($dataTypeCase as $key => $value) {
        $dataTypeCase[$key]['sid'] = $key+1;
        $dataTypeCase[$key]['case'] = $obj->listCaseOfProject($project_detail['project_detail']['project_sid'], $value['type']);
    }
    // $data = $obj->listCaseOfProject($project_detail['project_detail']['project_owner_sid'], );
    
    $engineer = array();
    $timesTotal = 0;
    // foreach ($data as $key => $value) {
    //     foreach ($value['task'] as $k => $v) {
    //         foreach ($v['timestamp'] as $kk => $vv) {
    //             $timesTotal++;
    //             $cal['use_hours'] = floor(intval($vv['use_minutes']) / 60);
    //             $cal['use_minutes_mod'] = (intval($vv['use_minutes']) % 60);
    //             $cal['use_minutes_mod'] = (intval($cal['use_minutes_mod'] + "") == 1) ? "0" + ($cal['use_minutes_mod']) : $cal['use_minutes_mod'];
                    
    //             if(!in_array($vv['engineer'], $engineer)){
    //                 // $temp = array();

    //                 $temp = array(
    //                     'engineer'=>$vv['engineer'],
    //                     'use_minutes'=>$vv['use_minutes'],
    //                     'thainame'=>$vv['thainame'],
    //                     'engname'=>$vv['engname'],
    //                     // 'mobile'=>$vv['mobile'],
    //                     'pic'=>$vv['pic'], 
    //                     'times'=>1,
    //                     'taxi'=>($vv['taxi_fare_1']+$vv['taxi_fare_2']),
    //                     'use_hours'=>$cal['use_hours'],
    //                     'use_minutes_mod'=>$cal['use_minutes_mod']
    //                     );
    //                 if(!repeatEngineer($engineer,$vv['engineer'])){
    //                     array_push($engineer, $temp);
    //                 }else{
    //                     $engineer = updateUseMinute($engineer, $temp['engineer'], $vv['use_minutes'], $vv);
    //                 }
    //             }
    //         }
    //     }
    // }

    $response = array();
    $response["error"] = false;

    // $viewObj = new ProjectManagementView($email);

    $response['data'] = $dataTypeCase;
    $project_detail['timesTotal'] = $timesTotal;
    $response['project_detail'] = $project_detail;
    $response['engineer'] = $engineer;
    $response['listUserCanAddProject'] = $listUserCanAddProject;
    $response['dataTypeCase'] = $dataTypeCase;
    echoRespnse(200, $response);
});

function repeatEngineer($engineerArray, $engineer){
    foreach ($engineerArray as $key => $value) {
        if($value['engineer']==$engineer){
            return true;
        }
    }
    return false;
}
function updateUseMinute($engineerArray, $engineer, $useMinutes, $data){
    foreach ($engineerArray as $key => $value) {
        if($value['engineer']==$engineer){
            $engineerArray[$key]['use_minutes']+= $useMinutes;
            $engineerArray[$key]['times']++;
            $engineerArray[$key]['taxi'] +=($data['taxi_fare_1']+$data['taxi_fare_2']);
        }
    }
    return $engineerArray;
}
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    $n = 0;
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $field = str_replace("_", " ", $field);
            $error_fields .= '- ' .ucfirst($field) . '<br/>';
            $n++;
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s)<br/>' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>