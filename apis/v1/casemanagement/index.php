<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/CaseModel.php';
require_once '../../include/GenNoCaseSR.php';
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->post('/createCases', function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $storage = $app->request->post('storage');

    $obj = new CaseModel();
    $data = $obj->receiveCasemanagementCreateCases($storage,$email);
    // print_r($storage);
    $response["error"] = false;
    $response['data'] = $storage;
    $response['message'] = "Done";
    // echo "<script>window.location='';</script>";
	echoRespnse(200, $response);
});

$app->post('/createCasesProjectPlan', function() use ($app){
    verifyRequiredParams(array('email','token'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $storage = json_decode($app->request->post('storage') ,true);

    require_once __dir__.'/../../include/ProjectModel.php';
    $obj = new CaseModel();
    $objProjectModel = new ProjectModel();


    if(isset($storage['project_sid'])){
        $dataProject = $objProjectModel->projectDetail2($storage['project_sid']);

        $storage['enduser_address'] = $dataProject['end_user_address'];
        $storage['enduser_case'] = $dataProject['end_user'];
        $storage['site_area'] = '';
        $storage['project_owner_sid'] = $dataProject['owner'][0]['owner_sid'];

        $storage['contract_no'] = $dataProject['contract'];

    }else{
        $storage['project_owner_sid'] = 0;
    }

    $data = $obj->receiveCasemanagementCreateCases($storage,$email);
    $response['data_res']['ticket_sid'] = $data['ticket_sid'];

    $dataTicket = $objProjectModel->ticketDetail($data['ticket_sid'], $email);
    if(isset($dataTicket['no_ticket'])){
        $dataTicket['ticket_sid'] = $data['ticket_sid'];
        $dataTicket['case_no'] = $dataTicket['no_ticket'];
        // print_r($storage);

        $response['storage'] = $storage;
        $response['data_res'] = $dataTicket;
    }
    $response["error"] = false;
    $response['data'] = $storage;
    $response['ticket_sid'] = $data['ticket_sid'];
    $response['message'] = "Done";
    // echo "<script>window.location='';</script>";
    echoRespnse(200, $response);
});

$app->post('/createCase', function() use ($app){
    verifyRequiredParams(array('email','requester_name','requester_email','end_user_name','end_user_email','contract','subject','case_type','urgency'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $requester_name = $app->request->post('requester_name');
    $requester_phone = $app->request->post('requester_phone');
    $requester_email = $app->request->post('requester_email');
    $requester_mobile = $app->request->post('requester_mobile');
    $requester_company = $app->request->post('requester_company');
    $end_user_name = $app->request->post('end_user_name');
    $end_user_phone = $app->request->post('end_user_phone');
    $end_user_email = $app->request->post('end_user_email');
    $end_user_mobile = $app->request->post('end_user_mobile');
    $end_user_company = $app->request->post('end_user_company');
    $serial = $app->request->post('serial');
    $contract = $app->request->post('contract');
    $prime_contract = $app->request->post('prime_contract');
    $note = $app->request->post('note');
    $subject = $app->request->post('subject');
    $description = $app->request->post('description');
    $case_company = $app->request->post('case_company');
    $case_address = $app->request->post('case_address');
    $case_type = $app->request->post('case_type');
    $urgency = $app->request->post('urgency');
    $owner = $app->request->post('owner');

    $response = array();

    $obj = new CaseModel();
    $data = $obj->executeCreateCase($subject, $description, $case_company, $case_address, $case_type, $urgency, $serial, $contract, $prime_contract, $note,$requester_name,$requester_phone,$requester_email,$requester_mobile,$requester_company,$end_user_name,$end_user_phone,$end_user_email,$end_user_mobile,$end_user_company,$email, $owner);

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response

    echoRespnse(200, $response);
});

$app->post('/changeAppointment',function() use ($app){

    verifyRequiredParams(array('email','task_sid','changeAppointmentDate','changeAppointmentTime'));
    $email = $app->request->post('email');
    $task_sid = $app->request->post('task_sid');
    $changeAppointmentDate = $app->request->post('changeAppointmentDate');
    $changeAppointmentTime = $app->request->post('changeAppointmentTime');
    $token = $app->request->post('token');

    $obj = new CaseModel();
    $data = $obj->changeAppointment($email, $changeAppointmentDate, $changeAppointmentTime, $task_sid);
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});
$app->post('/changeExpectTime',function() use ($app){

    verifyRequiredParams(array('email','task_sid','expect_time'));
    $email = $app->request->post('email');
    $task_sid = $app->request->post('task_sid');
    $expect_time = $app->request->post('expect_time');
    $token = $app->request->post('token');

    $obj = new CaseModel();
    $data = $obj->changeExpectTime($email, $expect_time, $task_sid);
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/listServiceReport',function() use ($app){

    verifyRequiredParams(array('ticket_sid'));
    $ticket_sid = $app->request->post('ticket_sid');

    $obj = new CaseModel();
    $data = $obj->listServiceReport($ticket_sid);
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    $n = 0;
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $field = str_replace("_", " ", $field);
            $error_fields .= '- ' .ucfirst($field) . '<br/>';
            $n++;
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s)<br/>' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
