<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/PreventiveMaintenanceModel.php';
require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
 
// ดึงข้อมูล serial ของ ticket
$app->post('/select/serial_ticket', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_sid'));
 
    // reading post params
    $ticket_sid = $app->request->post('ticket_sid');


    $pmModel = new PreventiveMaintenanceModel();
    $serial = $pmModel->selectSerialTicket($ticket_sid);


    $response = array();
    $response['error'] = false;
    
    $response['serial'] = $serial;
    $response['data'] = array(
            "data"=>"data"
        );
    // echo json response
    echoRespnse(200, $response);
});
 
// ดึงข้อมูลเพื่อแสดงว่า ticket ให้ทำ PM อะไรบ้าง
$app->post('/select/to_do_pm', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_sid'));
 
    // reading post params
    $ticket_sid = $app->request->post('ticket_sid');

    $pmModel = new PreventiveMaintenanceModel();
    $data = $pmModel->selectToDoPM($ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['todppm'] = $data;
    
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/insert/do_serial_pm', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_serial_sid','task_sid','status','email'));
 
    // reading post params
    $ticket_serial_sid = $app->request->post('ticket_serial_sid');
    $task_sid = $app->request->post('task_sid');
    $status = $app->request->post('status');
    $email = $app->request->post('email');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('ticket_serial_sid'=>$ticket_serial_sid,'task_sid'=>$task_sid,'status'=>$status,'create_by'=>$email);

    $result = $pmModel->insertDoSerialPM($param);


    $response = array();
    $response['error'] = false;
    $response['result'] = $result;
    
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/insert/input_action', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('task_sid','email'));
 
    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');
    $pm_service = $app->request->post('pm_service');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email, 'pm_service'=>$pm_service);

    $result = $pmModel->insertInputAction($param);


    $response = array();
    $response['error'] = false;
    $response['result'] = $result;
    
    // echo json response
    echoRespnse(200, $response);
});

/*****START CREATE CASE PM****/
$app->post('/createcasepm', function() use ($app) {

        require_once '../../include/db_connect.php';
        $connect = new DbConnect();
        $db = $connect->connect();
        $email = $app->request->post('email');
        $due_datetime = $app->request->post('due_datetime');

        $subject = $app->request->post('subject');
        $contract_no = $app->request->post('contract_no');
        $description = $app->request->post('description');
        $end_user_company_name = $app->request->post('end_user_company_name');
        $end_user_site = $app->request->post('end_user_site');
        $end_user_contact_name = $app->request->post('end_user_contact_name');
        $end_user_phone = $app->request->post('end_user_phone');
        $end_user_mobile = $app->request->post('end_user_mobile');
        $end_user_email = $app->request->post('end_user_email');

        $todoPmArray = json_decode($app->request->post('todoPmArray'), true);
        // print_r($todoPmArray);
        $pm = array();
        foreach ($todoPmArray as $key => $value) {
            $pm["pm".($key+1)]['name'] = $value['name'];
            $pm["pm".($key+1)]['value'] = $value['to_do'];
        }

    
        $serialArray = json_decode($app->request->post('serialArray'));
        $listSelectEngineer = json_decode($app->request->post('listSelectEngineer'));

        
        include "/home/flguploa/domains/flgupload.com/public_html/".END_POINT_APIS."/include/OpenPM.php";

        $dataSaveTicket = array(
            ':no_ticket'=>genTicketNo($db),
            ':via'=>'pmsystem',
            ':create_by'=>$listSelectEngineer[0],
            ':subject'=>vPost($subject),
            ':description'=>vPost($description),
            ':contract_no'=>vPost($contract_no),
            ':prime_contract'=>'',
            ':project_name'=>vPost(""),
            ':serial_no'=>join(',',$serialArray),
            ':team'=>'SSS',
            ':requester_full_name'=>'',
            ':requester_company_name'=>'',
            ':requester_phone'=>'',
            ':requester_mobile'=>'',
            ':requester_email'=>'',
            ':end_user_company_name'=>vPost($end_user_company_name),
            ':end_user_site'=>vPost($end_user_site),
            ':end_user_contact_name'=>vPost($end_user_contact_name),
            ':end_user_phone'=>vPost($end_user_phone),
            ':end_user_mobile'=>vPost($end_user_mobile),
            ':end_user_email'=>vPost($end_user_email)
        );
        $owner = vPost($listSelectEngineer[0]);
        // exit();
        $ticketSid = saveTicketManual($dataSaveTicket,$db);///////////(1) ///// saveTicket

        $dataSaveTicketLog = array(
            ':ticket_sid'=>$ticketSid,
            ':owner'=>$owner,
            ':status'=>'1',
            ':subject'=>vPost($subject),
            ':description'=>vPost($description),
            ':contract_no'=>vPost($contract_no),
            ':prime_contract'=>'',
            ':project_name'=>vPost(""),
            ':serial_no'=>join(',',$serialArray),
            ':team'=>'SSS',
            ':requester_full_name'=>'',
            ':requester_company_name'=>'',
            ':requester_phone'=>'',
            ':requester_mobile'=>'',
            ':requester_email'=>'',
            ':end_user_company_name'=>vPost($end_user_company_name),
            ':end_user_site'=>vPost($end_user_site),
            ':end_user_contact_name'=>vPost($end_user_contact_name),
            ':end_user_phone'=>vPost($end_user_phone),
            ':end_user_mobile'=>vPost($end_user_mobile),
            ':end_user_email'=>vPost($end_user_email),
            ':case_type'=>'Preventive Maintenance',
            ':source'=>'pmsystem',
            ':note'=>'',
            ':urgency'=>'Normal',
            ':create_by'=>vPost($email),
            ':due_datetime'=>$due_datetime
        );
        
        echo "<pre>";
        print_r($dataSaveTicket);
        echo "</pre>";

        echo "<pre>";
        print_r($dataSaveTicketLog);
        echo "</pre>";

        saveTicketLog($dataSaveTicketLog, $db);////////////////// (2) save to ticket log
        saveTicketSerial($ticketSid,$serialArray,$db);////////////////// (3) save ticket serial
        saveTasksManual($ticketSid,$listSelectEngineer,$due_datetime, $db, $pm); ////////////// (4) save task

});
/*****END/

/*****START CREATE TASK PM****/
$app->post('/createtaskpm', function() use ($app) {

        require_once '../../include/db_connect.php';
        $connect = new DbConnect();
        $db = $connect->connect();
        $email = $app->request->post('email');
        $appointment = $app->request->post('appointment');
        $ticket_sid = $app->request->post('ticket_sid');
        $listSelectEngineer = json_decode($app->request->post('engineerAssign'), true);

        
        include "/home/flguploa/domains/flgupload.com/public_html/".END_POINT_APIS."/include/OpenPM.php";
        $pm = array();
        saveTasksManual($ticket_sid,$listSelectEngineer,$appointment, $db, $pm); ////////////// (4) save task

});
/*****END CREATE TASK/ ***/

$app->post('/sendoutlook/:id', function($sr_sid){
    global $app;
    
    $obj = new PreventiveMaintenanceModel();
    $data = $obj->selectInformationTask($sr_sid);
    print_r($data);
    require_once '../../include/Outlook.php';
    $outlook = new Outlook();
  
    foreach ($data as $key => $value) {
        $data = array();
        $data['method'] = "ADD";
        $data['attendee'] = $value['engineer'];
        $data['end'] = $value['appointment'];
        $data['start'] = $value['endtime'];
        $data['location'] = $value['end_user_company_name'];
        $data['organizer'] = $value['subject'];
        $data['UID'] = $value['tasks_sid'];
        $data['title'] = $value['end_user_company_name'];
        $data['summary'] = $value['subject']." ".$value['serial_no']." ".$value['end_user_company_name']." ".$value['end_user_site']." ".$value['end_user_contact_name']." ".$value['end_user_phone']." ".$value['end_user_email'];
        $data['from'] = "flguploa@flgupload.com";
        $data['to'] = $value['engineer'];
        // $data['to'] = "autsakorn.t@firstlogic.co.th";

        $outlook->sendEvent($data);
    }

});

$app->get('/cronjobSendOutlookPM',function(){
    global $app;

    $obj = new PreventiveMaintenanceModel();
    $data = $obj->listCasePM();
    $temp = array();
    foreach ($data as $key => $value) {
        if($value['last_status']>=0 && $value['last_status']<200){
            array_push($temp, $value);
            sendOutlook($value['tasks_sid']);
        }
    }
    echo "<pre>";
    print_r($temp);
    echo "</pre>";
});

function sendOutlook($sr_sid){
    $obj = new PreventiveMaintenanceModel();
    $data = $obj->selectInformationTask($sr_sid);
    // print_r($data);
    require_once '../../include/Outlook.php';
    $outlook = new Outlook();
    
    foreach ($data as $key => $value) {

        $isSentOutlook = $outlook->isSentOutlook($sr_sid, $value['engineer']);
        if(!$isSentOutlook){
            $data = array();
            $data['method'] = "ADD";
            $data['attendee'] = $value['engineer'];
            $data['end'] = $value['appointment'];
            $data['start'] = $value['endtime'];
            $data['location'] = $value['end_user_company_name'];
            $data['organizer'] = $value['subject'];
            $data['UID'] = $value['tasks_sid'];
            $data['title'] = $value['end_user_company_name'];
            $data['summary'] = $value['no_ticket']." ".$value['no_task']." Subject:".$value['subject']." ".$value['end_user_company_name']." ".$value['end_user_site']." ".$value['end_user_contact_name']." ".$value['end_user_phone']." ".$value['end_user_email'];
            $data['from'] = "flguploa@flgupload.com";
            $data['to'] = $value['engineer'];
            // $data['to'] = "autsakorn.t@firstlogic.co.th";

            $outlook->sendEvent($data, $sr_sid);
        }
    }
}
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });



/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>