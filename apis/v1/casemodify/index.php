<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/CaseModifyModel.php';
require_once '../../include/GenNoCaseSR.php';
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();

$app->post('/subject', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');
    $new_subject = $app->request->post('new_subject');
    $ticket_sid = $app->request->post('ticket_sid');

    $obj = new CaseModifyModel($email);
    $data = $obj->modifySubject($email, $new_subject, $ticket_sid);

    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});


$app->post('/removeCase', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $ticket_sid = $app->request->post('ticket_sid');
    $data = array();
    $obj = new CaseModifyModel();
    $message = $obj->removeCase($email, $ticket_sid);
    $response = array();
    $response['error'] = false;
    $response['message'] = $message;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/endusercontact', function() use ($app){
    verifyRequiredParams(array('email'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $name  = $app->request->post('name');
    $mobile  = $app->request->post('mobile');
    $phone  = $app->request->post('phone');
    $end_user_email  = $app->request->post('end_user_email');
    $company  = $app->request->post('company');
    $ticket_sid  = $app->request->post('ticket_sid');

    $obj = new CaseModifyModel();
    // $permission_role = $obj->permission_role;
    $data = $obj->modifyContactUser($email, $name, $mobile, $phone, $end_user_email, $company, $ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});


$app->post('/requestercontact', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $name  = $app->request->post('name');
    $mobile  = $app->request->post('mobile');
    $phone  = $app->request->post('phone');
    $requester_email  = $app->request->post('requester_email');
    $company  = $app->request->post('company');
    $ticket_sid  = $app->request->post('ticket_sid');

    $obj = new CaseModifyModel();
    // $permission_role = $obj->permission_role;
    $data = $obj->modifyRequester($email, $name, $mobile, $phone, $requester_email, $company, $ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});


$app->post('/description', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $new_description  = $app->request->post('new_description');
    $ticket_sid  = $app->request->post('ticket_sid');

    $obj = new CaseModifyModel();
    // $permission_role = $obj->permission_role;
    $data = $obj->modifyDescription($email, $new_description, $ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});



$app->post('/owner', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $new_owner  = $app->request->post('new_owner');
    $ticket_sid  = $app->request->post('ticket_sid');

    $obj = new CaseModifyModel();
    // $permission_role = $obj->permission_role;
    $data = $obj->modifyOwner($email, $new_owner, $ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/mandays', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $man_days  = $app->request->post('man_days');
    $ticket_sid  = $app->request->post('ticket_sid');

    $obj = new CaseModifyModel();
    // $permission_role = $obj->permission_role;
    $data = $obj->modifyMandays($email, $man_days, $ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['message'] = false;
    // $response['permission_role'] = $permission_role;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

// $app->post('/edit', function() use ($app){
//     verifyRequiredParams(array('email'));
//     $email = $app->request->post('email');
//     $token = $app->request->post('token');
//     $case_type_sid  = $app->request->post('case_type_sid');
//     $case_type_name  = $app->request->post('case_type_name');

//     $obj = new CaseTypeModel($email);
//     // $permission_role = $obj->permission_role;
//     $data = $obj->editCasetype($case_type_sid, $case_type_name);


//     $response = array();
//     $response['error'] = false;
//     $response['message'] = false;
//     // $response['permission_role'] = $permission_role;
//     $response['data'] = $data;
//     echoRespnse(200, $response);
// });

// $app->post('/delete', function() use ($app){
//     verifyRequiredParams(array('email'));
//     $email = $app->request->post('email');
//     $token = $app->request->post('token');
//     $case_type_sid  = $app->request->post('case_type_sid');

//     $obj = new CaseTypeModel($email);
//     // $permission_role = $obj->permission_role;
//     $data = $obj->deleteCasetype($case_type_sid);


//     $response = array();
//     $response['error'] = false;
//     $response['message'] = false;
//     // $response['permission_role'] = $permission_role;
//     $response['data'] = $data;
//     echoRespnse(200, $response);
// });

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    $n = 0;
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $field = str_replace("_", " ", $field);
            $error_fields .= '- ' .ucfirst($field) . '<br/>';
            $n++;
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s)<br/>' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>