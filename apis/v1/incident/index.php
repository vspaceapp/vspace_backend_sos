<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ServiceReportModel.php';
require_once '../../include/CaseModel.php';
require_once '../../include/ProjectModel.php';
require_once '../../include/GenNoCaseSR.php';
require_once '../../include/PlanningModel.php';
require_once '../../include/RoleModel.php';
require_once '../../send_notification/ios/send.php';
require_once '../../send_notification/andriod/send.php';
require_once '../../view/ActivityCase.php';
require_once __dir__.'/../../include/WebServiceToRemedy/VSpaceToKBank.php';
require_once __dir__."/../../core/constant.php";
require_once __dir__."/../../core/config.php";
// require_once '../../include/db_handler.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->post('/viewOracleSr', function() use ($app){
    $email = "";
    $token = "";
    $ticket_sid = "";
    $spare_sid = "";
    $data = array();

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    if($app->request->post('email')){
        $email = $app->request->post('email');
    }
    if($app->request->post('token')){
        $token = $app->request->post('token');
    }
    if($app->request->post('ticket_sid')){
        $ticket_sid = $app->request->post('ticket_sid');
    }
    if($app->request->post('spare_sid')){
        $spare_sid = $app->request->post('spare_sid');
    }

    $obj = new CaseModel();
    $data = $obj->viewOracleSr($email, $spare_sid, $token, $ticket_sid);
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/ListOracleSr', function() use ($app){
    $email = "";
    $token = "";
    $ticket_sid = "";
    $data = array();

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    if($app->request->post('email')){
        $email = $app->request->post('email');
    }
    if($app->request->post('token')){
        $token = $app->request->post('token');
    }
    if($app->request->post('ticket_sid')){
        $ticket_sid = $app->request->post('ticket_sid');
    }
    if($email!=""){
        $obj = new CaseModel();
        $data = $obj->listOracleSr($email, $ticket_sid, $token);
        $response['data'] = $data;
    }

    echoRespnse(200, $response);
});
$app->post('/CreatePart', function() use ($app){

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;

    $obj = new CaseModel();
    $obj->createPart($email, $data, $token);
    echoRespnse(200, $response);
});

$app->post('/EditPart', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;

    $obj = new CaseModel();
    $obj->editPart($email, $data, $token);
    echoRespnse(200, $response);
});

$app->post('/EditSpareTask', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;

    $obj = new CaseModel();
    $obj->editSpareTask($email, $data, $token);
    echoRespnse(200, $response);
});

$app->post('/sendNotification', function() use ($app){
    $to = $app->request->post('to');
    $message = $app->request->post('message');
    $notification_sid = 0;

    if($app->request->post('notification_sid')){
        $notification_sid = $app->request->post('notification_sid');
    }
    $ticket_sid = 0;
    if($app->request->post('ticket_sid')){
        $ticket_sid = $app->request->post('ticket_sid');
    }
    $tasks_sid = 0;
    if($app->request->post('tasks_sid')){
        $tasks_sid = $app->request->post('tasks_sid');
    }
    $project_sid = 0;
    if($app->request->post('project_sid')){
        $project_sid = $app->request->post('project_sid');
    }


    // $to, $message
    $obj = new CaseModel();
    $obj->sendNoti($to, $message, $notification_sid,$ticket_sid,$tasks_sid,$project_sid);
    $response = array();
    $response["error"] = false;
    $response['notification_sid'] = $notification_sid;
    echoRespnse(200, $response);
});

$app->post('/lists',function() use ($app){
    // verifyRequiredParams(array('day'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $sort = array();
    if($app->request->post('sort')){
        $sort = $app->request->post('sort');
    }
    if($app->request->post('status_view'))
        $type_view = $app->request->post('status_view');
    else
        $type_view = "";

    $owner_type = "";
    if($app->request->post('owner')){
        $owner_type = $app->request->post('owner');
    }
    $filter_case = "";
    if($app->request->post('filter_case')){
        $filter_case = $app->request->post('filter_case');
    }

    if($filter_case!=""){
        $filter_case_string = "";
        $filter_case = explode(",", $filter_case);
        foreach ($filter_case as $key => $value) {
            if($filter_case_string!=""){
                $filter_case_string .= ",";
            }
            if($value=="pm"){
                $filter_case_string .= "'Preventive Maintenance'";
            }else{
                $filter_case_string .= "'".ucfirst($value)."'";
            }
        }
        $filter_case = $filter_case_string;
    }else{
        $filter_case = "'Incident','Request','Question','Implement','Install','Preventive Maintenance'";
    }

    $obj = new CaseModel();
    $search = "";
    if($app->request->post("search")){
        $search = $app->request->post('search');
    }

    $options = array(
        'case_type'=>$filter_case,
        'sort'=>'DESC'
    );

    if($owner_type=="myself"){
        $owner = $email;
    }else if($owner_type=="group"){
        $owner = $owner_type;
    }else{
        $owner = "";
    }
    $data = $obj->lists('Incident',"",$options, $email,$search,$type_view, $owner,$sort);
    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    $response['sort'] = $sort;

    require_once '../../include/CaseTypeModel.php';
    $obj = new CaseTypeModel($email);
    $dataProjectParse = $obj->getProjectParse($email);
    $caseType = array();
    foreach ($dataProjectParse as $key => $value) {
        foreach ($value['case_type'] as $k => $v) {
            $temp = array();
            $temp['name'] = $v['name'];
            $temp['detail'] = $v['name'];
            $temp['project'] = $v['type'];
            array_push($caseType, $temp);
        }
    }
    $response['caseType'] = $caseType;
    // echo json response
    echoRespnse(200, $response);

});

$app->post('/case_detail', function() use ($app){
	$ticket_sid = $app->request->post('ticket_sid');
    $email = "";
    if($app->request->post('email')){
        $email = $app->request->post('email');
    }
    $token = "";
    if($app->request->post('token')){
        $token = $app->request->post('token');

    }
    $obj = new CaseModel();
    $data = $obj->getCaseDetail($ticket_sid, $email, $token);
    if($data['case_type']=="Incident" && $data['sla_remedy']){

    	$data['sla_stamp'] = $data['sla'];
    	// $data['response'] = ($data['status']<2)?1:0;
    	// $data['wa'] = ($data['status']<3)?1:0;
    	// $data['resolve'] = ($data['status']<5 && $data['status']!="6" && $data['status']!="7")?1:0;
        $slaRemedy = explode(",", $data['sla_remedy']);

        $slaRemedySubUse1 = array();
        foreach ($slaRemedy as $key => $value) {
            $slaSub = explode("||", $value);
                if(count($slaSub)>1){

                $slaRemedySubUse2 = array();
                foreach ($slaSub as $k => $v) {
                    $slaRemedySub = explode("=>", $v);
                    if($slaRemedySub[0]=="SLA_Name"){
                        $slaRemedySub[1] = str_replace("OneStop_","",$slaRemedySub[1]);
                        $slaRemedySub[1] = str_replace("_2012","",$slaRemedySub[1]);
                    }

                    if($slaRemedySub[0]=="SLA_Name"){
                        $slaRemedySub[1] = str_replace("FIRSTAccountOr7x24_","",$slaRemedySub[1]);
                        $slaRemedySub[1] = str_replace("_2011","",$slaRemedySub[1]);
                        $slaRemedySub[1] = str_replace("_2014","",$slaRemedySub[1]);
                    }

                    if($slaRemedySub[0]=="SLA_Due_Date"){
                    	$slaDueDate = explode("T", $slaRemedySub[1]);
                        $datetime = explode("-", $slaDueDate[0]);
                        $datetime = ($datetime[0]-543)."/".$datetime[1]."/".$datetime[2];

                    	$slaRemedySub[1] = $datetime." ".str_replace("+07:00", "", $slaDueDate[1]);
                    }
                    $slaRemedySubUse2[$k] =  array($slaRemedySub[0],$slaRemedySub[1]);
                }

                array_push(
                    $slaRemedySubUse1,
                    array(
                        $slaRemedySubUse2[0][0]=>$slaRemedySubUse2[0][1],
                        $slaRemedySubUse2[1][0]=>$slaRemedySubUse2[1][1],
                        $slaRemedySubUse2[2][0]=>$slaRemedySubUse2[2][1],
                        $slaRemedySubUse2[3][0]=>$slaRemedySubUse2[3][1]
                    )
                );
            }
        }

        if(count($slaRemedySubUse1)>0){
            function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    		    $sort_col = array();
    		    foreach ($arr as $key=> $row) {
    		        $sort_col[$key] = $row[$col];
    		    }
    		    array_multisort($sort_col, $dir, $arr);
    		}

    		array_sort_by_column($slaRemedySubUse1, 'SLA_Due_Date');

            $data['sla_remedy_use'] = array();
            $data['sla_remedy_use'] = $slaRemedySubUse1;
        }
    }
	$response = array();
	$response["error"] = false;
	$response["data"] = $data;

    $activityCase = new ActivityCase($email);

    $response['html_case_activity'] = $activityCase->htmlActivityCase(
        (isset($data['sla'])?$data['sla']:array()),
        (isset($data['expect_pending_finish'])?$data['expect_pending_finish']:""),
        $data['work_log_remedy'],
        $ticket_sid,
        $data['service_report']);

    $response['html_case_service'] = $activityCase->htmlServiceReport($ticket_sid,$data['service_report'],$email);
	echoRespnse(200,$response);
});

$app->post('/updateworklog', function() use ($app){
    verifyRequiredParams(array('worklog','email','ticket_sid'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $worklog = $app->request->post('worklog');
    $ticket_sid = $app->request->post('ticket_sid');
    $obj = new CaseModel();
    $data = $obj->initailUpdateWorklog($ticket_sid, $worklog, $email);

    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);
});
$app->post('/lists_remind_sla',function() use ($app){
    // verifyRequiredParams(array('day'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $obj = new CaseModel();
    $options = array(
        'sort'=>'DESC'
    );
    $data = $obj->remindSLA('Incident',"",$options);
    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);
});

//$updateTicketStatus = function() use($app){
//    verifyRequiredParams(array('ticket_sid','case_status_sid' , 'email', 'token'));
//    $ticket_sid = $app->request->post('ticket_sid');
//    $work_log = $app->request->post('worklog');
//    $email = $app->request->post('email');
//    $token = $app->request->post('token');
//    $obj = new CaseModel();
//
//    if($app->request->post('datetime')){
//        $datetime_pending = $app->request->post('datetime');
//        if(strlen($datetime_pending)<17){
//            $expect_pending_to_remedy = $expect_pending = ($datetime_pending.":00");
//        }else{
//            $expect_pending_to_remedy = $expect_pending = $datetime_pending;
//        }
//    }else{
//        $expect_pending = "0000-00-00 00:00:00";
//        $expect_pending_to_remedy = "";
//    }
//    $solution = "";
//    $solution_detail = "";
//    if($app->request->post('solution')){
//        $solution = $app->request->post('solution');
//    }
//    if($app->request->post('solution_detail')){
//        $solution_detail = $app->request->post('solution_detail');
//    }
//
//};

$app->post('/updateSLA', function() use ($app){
    verifyRequiredParams(array('case_status_sid','ticket_sid','email','token', 'worklog', 'assigned_group'));
    $vSpaceToKBank = new VSpaceToKBank();
    $request = $app->request();
    $work_log = $request->post('worklog');
    $case_status_sid = $request->post('case_status_sid');
    $ticket_sid = $request->post('ticket_sid');
    $email = $request->post('email');
    $token = $request->post('token');
    $loadCaseModel = new CaseModel;
    $roleModel = new RoleModel();
    $roleModel->email = $email;
    $dataRole = $roleModel->getRoleByEmail();
    $UpdateAuth = $loadCaseModel->checkAuthorizeForUpdate($ticket_sid);
    $obj = new CaseModel();
    $obj->ticket_sid = $ticket_sid;

    if($obj->checkCanResolveTicket() == 0 && $dataRole['can_resolve_immediately'] == 0 && $case_status_sid == 5){
        return echoErrorMessageResponse(200, ERROR_REQUIRES_REPORT_CLOSING);
        // end process update sla
    }
    if ($email == $UpdateAuth['owner']) {
        if($request->post('datetime')){
            $datetime_pending = $request->post('datetime');
            if(strlen($datetime_pending)<17){
                $expect_pending = ($datetime_pending.":00");
            }else{
                $expect_pending = $datetime_pending;
            }
        }else{
            $expect_pending = "0000-00-00 00:00:00";
        }
        $solution = getRequestParamsOrDefault($request->post('solution'),"");
        $solution_detail = getRequestParamsOrDefault($request->post('solution_detail'),"");
        $status_reason = getRequestParamsOrDefault($request->post('status_reason'));
        $serial_no = getRequestParamsOrDefault($request->post('serial_no'), null);
        $resolution_product_category_tier_1 = getRequestParamsOrDefault($request->post('resolution_product_category_tier_1'), null);
        $resolution_product_category_tier_2 = getRequestParamsOrDefault($request->post('resolution_product_category_tier_2'), null);
        $resolution_product_category_tier_3 = getRequestParamsOrDefault($request->post('resolution_product_category_tier_3'), null);
        $additional_status = getRequestParamsOrDefault($request->post('additional_status'),null);

        $data = $obj->initailUpdateSlaAndWorklog(
            $ticket_sid, $work_log, $case_status_sid, $email,
            $expect_pending, $solution, $solution_detail,0,
            1 , $status_reason, $serial_no, $resolution_product_category_tier_1, $resolution_product_category_tier_2,
            $resolution_product_category_tier_3, $additional_status
        );
        $isRequest_information = getRequestParamsOrDefault($request->post('request_information'), false);
        $isRequest_close_change_group = getRequestParamsOrDefault($request->post('request_close_change_group'), false);
        $ticketDetail = $obj->getTicketDetail(['refer_remedy_hd', 'case_type']);
        $res_remedy_update_status = null;
        $res_remedy_update_worklog = null;
        if($ticketDetail['case_type']=='Incident' && ENV == 'production'){
            if(in_array($case_status_sid, array(2,5,4))){
                // send update status to remedy
                $params = array(
                    'incident_number' => $ticketDetail['refer_remedy_hd'],
                    'status' => ($isRequest_information || $isRequest_close_change_group)?
                        $obj->selectStatusName(1):
                        $obj->selectStatusName($case_status_sid),
                    'status_reason' => $request->post('status_reason'),
                    'z1d_note' => $request->post('z1d_note'),
                    'z1d_action' => $request->post('z1d_action' ),
                    'z1d_char01' => $request->post('z1d_char01'),
                    'resolution' => $request->post('solution_detail'),
                    'resolution_category' => null,
                    'resolution_category_tier_2' => null,
                    'resolution_category_tier_3' => null
                );
                $params = $vSpaceToKBank->prepareDataUpdateTicket(
                    $params,
                    $request->post('assigned_group')
                );
                $res_remedy_update_status = $vSpaceToKBank->updateIncidentCase($params, $email);
            }
            $res_remedy_update_worklog = $vSpaceToKBank->createUpdateWorklog($email,$ticketDetail['refer_remedy_hd'],"",$work_log);
        }
        //  update chane owner(team end point => incident co)
        if($isRequest_information || $isRequest_close_change_group){
            //  nanthawat.s.m+sos.vspace.01@gmail.com => อีเมล์กลางทีม incident co
            $obj->updateOwner($ticket_sid, "nanthawat.s.m+sos.vspace.01@gmail.com", $email);
        }
        $response['result_update_remdy'] = array("update_status_success"=>$res_remedy_update_status, "update_worklog_success"=>$res_remedy_update_worklog);
        $response["error"] = false;
        $response['message'] = 'Update Completed';
        echoRespnse(200, $response);
        }else {
            $response["error"] = true;
            $response["message"] = ERROR_AUTH_1_MSG;
            echoRespnse(200, $response);
        }


});

$app->post('/updateSLAWeb', function() use ($app){
    verifyRequiredParams(array('worklog','case_status_sid','ticket_sid','email','token'));
    $work_log = $app->request->post('worklog');
    $case_status_sid = $app->request->post('case_status_sid');
    $ticket_sid = $app->request->post('ticket_sid');
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $data = "";
    // if($email!="autsakorn.t@firstlogic.co.th"){
        $obj = new CaseModel();
        if($app->request->post('datetime')){
            $expect_pending_to_remedy = $expect_pending = $app->request->post('datetime').":00";
        }else{
            $expect_pending = "0000-00-00 00:00:00";
            $expect_pending_to_remedy = "";
        }

        $solution = "";
        $solution_detail = "";

        if($app->request->post('solution')){
            $solution = $app->request->post('solution');
        }

        if($app->request->post('solution_detail')){
            $solution_detail = $app->request->post('solution_detail');
        }
        $status_reason = getRequestParamsOrDefault($app->request->post('status_reason'));
        $data = $obj->initailUpdateSlaAndWorklog($ticket_sid, $work_log, $case_status_sid, $email,$expect_pending, $solution, $solution_detail, $status_reason);

        $obj->updateStatusCaseToRemedy($ticket_sid, $email, $work_log,$expect_pending_to_remedy, $case_status_sid,$solution,$solution_detail);
    // }

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/createtask', function() use ($app){
    verifyRequiredParams(array('appointment_date','appointment_time','engineer','expect_time','type_service','subject_service_report'));

    $appointment_date = $app->request->post('appointment_date'); // ควรเช็คก่อน
    $appointment_time = $app->request->post('appointment_time');
    $engineer = $app->request->post('engineer');
    $expect_time = $app->request->post('expect_time');
    $type_service = $app->request->post('type_service');
    $ticket_sid = $app->request->post('ticket_sid');
    $email = $app->request->post('email');

    $subject_service_report = ($app->request->post('subject_service_report'))?$app->request->post('subject_service_report'):"-";
    $end_user_contact_name_service_report = ($app->request->post('end_user_contact_name_service_report'))?$app->request->post('end_user_contact_name_service_report'):"-";
    $end_user_phone_service_report = ($app->request->post('end_user_phone_service_report'))?$app->request->post('end_user_phone_service_report'):"-";
    $end_user_mobile_service_report = ($app->request->post('end_user_mobile_service_report'))?$app->request->post('end_user_mobile_service_report'):"-";
    $end_user_email_service_report = ($app->request->post('end_user_email_service_report'))?$app->request->post('end_user_email_service_report'):"-";
    $end_user_company_name_service_report = ($app->request->post('end_user_company_name_service_report'))?$app->request->post('end_user_company_name_service_report'):"-";

    $obj = new CaseModel();

    if($ticket_sid!=""){
       $obj->createSrIncident($ticket_sid,$appointment_date,$appointment_time,$expect_time,$engineer, $email,$type_service,$subject_service_report,$end_user_contact_name_service_report,$end_user_phone_service_report,$end_user_mobile_service_report,$end_user_email_service_report,$end_user_company_name_service_report
        );
    }

    $response["error"] = false;
    echoRespnse(200, $response);
});

$app->post('/createServiceReportGeneral', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $ticket_sid = "1";

    $data = $app->request->post('data');
    // print_r($data);
    $obj = new CaseModel();
    // if($ticket_sid!=""){
    $data = $obj->createServiceReportGeneral($email,$data);
    // }
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});
$app->post('/createServiceReport',function() use ($app){
	verifyRequiredParams(array('email','ticket_sid','token'));
	$token = $app->request->post('token');
	$email = $app->request->post('email');
	$ticket_sid = $app->request->post('ticket_sid');

	$data = $app->request->post('data');
	// print_r($data);
	$obj = new CaseModel();
	if($ticket_sid!=""){
		$data = $obj->createServiceReport($ticket_sid,$email,$data);
	}
	$response["error"] = false;
	$response["data"] = $data;
    echoRespnse(200, $response);
});


$app->post('/createServiceReportReact',function() use ($app){
    verifyRequiredParams(array('email','ticket_sid','token'));
    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $ticket_sid = $app->request->post('ticket_sid');
    $data = json_decode($app->request->post('data'), true);
    $obj = new CaseModel();
    $authCheck = $obj->checkAuthorizeForUpdate($ticket_sid);
    if($authCheck['owner'] != $email){
        $response['error'] = true;
        $response['message'] = ERROR_AUTH_1_MSG;
        echoRespnse(200, $response);
        return;
    }
    
    if($ticket_sid!=""){
        $obj->createServiceReportReact($ticket_sid,$email,$data);
    }

    require_once '../../include/ProjectModel.php';
    $objProjectModel = new ProjectModel();

    $response["error"] = false;
    $response["message"] = "Create Completed";
    $response["data"] = $data;
    $response["task"] = $objProjectModel->listTaskOfCase($ticket_sid);

    echoRespnse(200, $response);
});

$app->post('/createServiceReport2',function() use ($app){
    verifyRequiredParams(array('email','ticket_sid','token'));
    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $ticket_sid = $app->request->post('ticket_sid');

    $data = $app->request->post('data');
    // print_r($data);
    $obj = new CaseModel();
    if($ticket_sid!=""){
        $data = $obj->createServiceReport2($ticket_sid,$email,$data);
    }
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

// Mobile 11/11/2016
$app->post('/createServiceReport3', function() use ($app){
    verifyRequiredParams(array('email','ticket_sid','token'));
    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $ticket_sid = $app->request->post('ticket_sid');

    $data = $app->request->post('data');
    $obj = new CaseModel();
    if($ticket_sid!=""){
        $data = $obj->createServiceReport3($ticket_sid, $email, $data);
    }
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200,$response);
});

$app->post('/engineer', function() use ($app){
    $team = $app->request->post('team');

    $obj = new PlanningModel();
    if($team!="")
        $data = $obj->engineer($team);
    else
        $data = $obj->engineer();

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/addproject', function() use ($app){

    verifyRequiredParams(array('data', 'email'));
    $data = $app->request->post('data');
    $email = $app->request->post('email');
    $data = json_decode($data, true);

    $obj = new ProjectModel();

    foreach ($data as $key => $value) {
        $obj->addProject($value, $email);
    }
    // $obj = new PlanningModel();
    // $data = $obj->engineer();

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/setprojecttype', function() use ($app){
    verifyRequiredParams(array('project_sid', 'email', 'project_type'));
    $project_sid = $app->request->post('project_sid');
    $project_type = $app->request->post('project_type');
    $email = $app->request->post('email');


    $obj = new ProjectModel();
    $data =$obj->setProjectType($project_sid, $project_type, $email);

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/setmandayprojectimplement',function() use ($app){
    verifyRequiredParams(array('project_sid','engineer','man_day','start_project','end_project','email'));
    $project_sid = $app->request->post('project_sid');
    $engineer = $app->request->post('engineer');
    $man_day = $app->request->post('man_day');
    $start_project = $app->request->post('start_project');
    $end_project = $app->request->post('end_project');
    $email = $app->request->post('email');


    $obj = new ProjectModel();
    $data =$obj->setMandayProjectImplement($project_sid, $engineer, $man_day, $start_project, $end_project, $email);

    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;

//     verifyRequiredParams(array('gcm_registration_id'));

//     $gcm_registration_id = $app->request->put('gcm_registration_id');

//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);

//     echoRespnse(200, $response);
// });


/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function getRequestParamsOrDefault($value, $default = null){
    return isset($value)?$value:$default;
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

function echoErrorMessageResponse($status_code, $message){
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    $response["error"] = true;
    $response['message'] = $message;
    echo json_encode($response);
}

$app->run();
?>
