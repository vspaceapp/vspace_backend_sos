<?php
require_once "../../include/TokenModel.php";
require_once "../../include/InventoryModel.php";

$index = function () use ($app) {
    
    parse_str($app->request()->getBody(),$request);

    $response = array();
    $response['request'] = $request;

    $obj = new InventoryModel();
    $data = $obj->index();

    $response['data'] = $data;
    echoRespnse(200, $response);
};
$detail = function () use ($app) {
    parse_str($app->request()->getBody(),$data);

    $response = array();
    $response['request'] = $data;
    // $response['res'] = $res;
    echoRespnse(200, $response);
};
$addInventory = function () use ($app) {
    parse_str($app->request()->getBody(),$request);
    $response = array();
    $response['request'] = $request;

    $obj = new InventoryModel();
    $res = $obj->addInventory($request);
    if($res){
        $response['res'] = $res;
    }
    echoRespnse(200, $response);
};

$cti = function() use ($app){
    parse_str($app->request()->getBody(),$data);
    $obj = new InventoryModel();
    $data = $obj->cti();

    $response = array();
    $response['request'] = $data;
    $response['data'] = $data;
    echoRespnse(200, $response);
};

$ciInformation = function() use ($app){
    parse_str($app->request()->getBody(),$request);
    
    $response = array();
    $response['request'] = $request;

    $obj = new InventoryModel();
    $data = $obj->ciInformation($request);
    $response['data'] = $data;
    echoRespnse(200, $response);
};

$findItemLikeThis = function() use ($app){
    parse_str($app->request()->getBody(),$request);
    
    $response = array();
    $response['request'] = $request;

    $obj = new InventoryModel();
    $data = $obj->findItemLikeThis($request);
    
    $response['data'] = $data;
    echoRespnse(200, $response);
};
// $item = function() use ($app){
//     parse_str($app->request()->getBody(),$data);

//     $response = array();
//     $response['request'] = $data;
//     // $response['res'] = $res;
//     echoRespnse(200, $response);
// };
?>