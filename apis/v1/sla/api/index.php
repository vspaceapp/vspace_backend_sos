<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once "../../include/SlaModel.php";
require_once "../../include/AlertModel.php";
$index = function () use ($app) {
	try{
        parse_str($app->request()->getBody(),$request);

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;

		echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$startCreateSlaRule = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;

        $obj = new SlaModel();
        $team = $obj->teamOfEmployee();

        if(isset($request['sla_sid'])){
            $data = $obj->slaRuleDetail($request['sla_sid']);
            $response['data'] = $data;
        }
        // $enduser = $obj->enduserOfTicket();

        $response['team'] = $team;

        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$enduserCompanyForSlaRule = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;

        $obj = new SlaModel();
        $enduser = $obj->enduserOfTicket();

        // $response['team'] = $team;
        $response['enduser'] = $enduser;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$generateSlaRule = function () use ($app) {
    try {
        parse_str($app->request()->getBody(), $request);

        $data = $request['data'];
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        $obj = new SlaModel();
        $team = $obj->generateSlaRule($data, $request['email']);

        $response = array();
        $response['error'] = false;

        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$slaRule = function () use ($app) {
    try {
        parse_str($app->request()->getBody(), $request);

        $obj = new SlaModel();
        $data = $obj->slaRule();

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$slaRuleDetail = function () use ($app) {
    try {
        parse_str($app->request()->getBody(), $request);

        $obj = new SlaModel();
        $data = $obj->slaRuleDetail($request['sla_sid']);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$editSlaRule = function () use ($app) {
    try {
        parse_str($app->request()->getBody(), $request);

        $data = $request['data'];

        // print_r($data);
        $obj = new SlaModel();
        $data = $obj->editSlaRule($data, $request['email']);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        // echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$deleteSlaRule = function () use ($app) {
    try {
        parse_str($app->request()->getBody(), $request);

        $data = $request['data'];

        $obj = new SlaModel();
        $data = $obj->deleteSlaRule($data, $request['email']);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        // echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$slaSimulator = function () use ($app){
    try {
        parse_str($app->request()->getBody(), $request);

        $obj = new SlaModel();
        $data = $obj->findSlaRule($request);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        // echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$loadNotification = function () use ($app){
    try {
        parse_str($app->request()->getBody(), $request);

        $obj = new AlertModel();
        $data = $obj->loadNotificationGroup($request);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data['data'];

        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        // echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$loadNotificationDetail = function () use ($app){
	try{
		parse_str($app->request()->getBody(), $request);

		$response = array();
		$response['error'] = false;
		$response['request'] = $request;
		$obj = new AlertModel();
		$data = $obj->loadNotificationDetail($request);
		$response['data'] = $data;
		echoRespnse(200, $response);
	}catch(Exception $e){
		echoRespnse(500, $response['message']=$e->getMessage());
	}
};
$alertSimulator = function () use ($app){
    try {
        parse_str($app->request()->getBody(), $request);

        $obj = new SlaModel();
        $data = $obj->findAlertRule($request);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data[0];
        $response['adm'] = $data[1];
        $response['service_mgr'] = $data[2];
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        // echo $e;
        // print_r($e);
        echoRespnse(500, $response['message']=$e->getMessage());
    }
};
$updateAlert = function () use ($app){
	try{
		parse_str($app->request()->getBody(), $request);
		$obj = new AlertModel();
		$data = $obj->updateAlertByColumnValueSid($request);
		$response = array();
		$response['error'] = false;
		$response['request'] = $request;
		$response['data'] = $data;
		echoRespnse(200, $response);
	}catch(Exception $e){
		echoRespnse(500, $response['message']=$e->getMessage());
	}
};
$addAlert = function () use ($app){
	try{
		parse_str($app->request()->getBody(), $request);
		$obj = new AlertModel();
		$data = $obj->addAlert($request);
		$response = array();
		$response['error'] = false;
		$response['request'] = $request;
		$response['data'] = $data;
		echoRespnse(200, $response);
	}catch(Exception $e){
		echoRespnse(500, $response['message']=$e->getMessage());
	}
};
?>
