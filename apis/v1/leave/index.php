<?php

error_reporting(-1);
ini_set('display_errors', 'On');
// require_once '../../include/PreventiveMaintenanceModel.php';
require_once '../../include/db_handler.php';
require_once '../../include/LeaveModel.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
 

$app->post('/loadLeave', function() use ($app){
    verifyRequiredParams(array('token','email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    
    if($email!=""){
        $obj = new LeaveModel();
        $data = $obj->loadLeave($email, $token);
    }
    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});

$app->post('/processBeforeLeave', function() use ($app){
    verifyRequiredParams(array('token','email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');

    if($email!=""){
        $obj = new LeaveModel();
        $p = $obj->processBeforeLeave($email, $token, $data);
    }
    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    $response['p'] = $p;
    echoRespnse(200, $response);
});
$app->post('/processLeave', function() use ($app){
    verifyRequiredParams(array('token','email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $data = $app->request->post('data');
    sleep(2);
    if($email!=""){
        $obj = new LeaveModel();
        $p = $obj->processLeave($email, $token, $data);
    }
    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    // $response['p'] = $p;
    echoRespnse(200, $response);
});
$app->post('/processLeave2', function() use ($app){
    verifyRequiredParams(array('token','email'));

    parse_str($app->request()->getBody(), $request);
    $obj = new LeaveModel();
    $p = $obj->processLeave($request['email'], $request['token'], $request);


    $response = array();
    $response['error'] = false;
    $response['request'] = $request;
    // $response['p'] = $p;
    echoRespnse(200, $response);
});

$app->post('/typeLeave', function() use ($app){
    verifyRequiredParams(array('token','email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $type = $app->request->post('type');

    if($email!=""){
        $obj = new LeaveModel();
        $data = $obj->typeLeave($email, $token, $type);
    }
    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>