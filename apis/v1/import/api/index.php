<?php
require_once "../../include/SlaModel.php";
require_once "../../include/ImportExcelCTI.php";
require_once "../../include/ImportIncharge.php";
require_once "../../include/ImportExcelAlertSmsAndEmail.php";
$index = function () use ($app) {
	try{
        parse_str($app->request()->getBody(),$request);

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;
        
		echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$importCTI = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;
        
        $obj = new ImportExcelTIPGroup();
        $obj->readExcelFile();
        // $enduser = $obj->enduserOfTicket();

        
        // $response['enduser'] = $enduser;
        // echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$importIncharge = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;
        
        $obj = new ImportIncharge();
        $obj->readExcelFile();
        // $enduser = $obj->enduserOfTicket();
        // $response['enduser'] = $enduser;
        // echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$alertSmsAndEmail = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);
        $obj = new ImportExcelAlertSmsAndEmail();
        $obj->import();

        $response = array();
        $response['error'] = false;
        $response['request'] = $request;
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$uploadNotification = function () use ($app){
    try{
        // parse_str($app->request()->getBody(),$request);
        // $obj = new ImportExcelAlertSmsAndEmail();
        // $obj->import();

        $email = $app->request->post('email');
        $token = $app->request->post('token');
        // $base64 = $app->request->post('file');
        // $data = array('base64'=>$base64);

        if(isset($_FILES['file'])){
            $file = $_FILES['file'];
        }else{
            $file = array();
        }
        $obj = new ImportExcelAlertSmsAndEmail();
        $data = $obj->uploadFile($email, $token, $file);

        // print_r($data);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;
        $response['request'] = $file;
        echoRespnse(200, $response);

    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$importNotification = function () use ($app){
    try{
        parse_str($app->request()->getBody(),$request);
        $response = array();
        $obj = new ImportExcelAlertSmsAndEmail();
        $obj->importData($request['file']);
        
        $response['request'] = $request;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
?>