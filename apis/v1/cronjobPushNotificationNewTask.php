<?php
    // error_reporting(-1);
    // ini_set('display_errors', 'On');

    require_once '/home/flguploa/domains/flgupload.com/public_html/apis/include/db_connect.php';
    // opening db connection
    $db = new DbConnect();
    $conn = $db->connect();
    
    $sql = "SELECT TKL.*,T.*,TL.*,DATE_FORMAT(NOW(),'%d/%m/%y %H:%i') current_datetime, U.gcm_registration_id, 
            NOW() current, DATE_FORMAT(appointment,'%d/%m/%y %H:%i') appointment 
            FROM tasks_log TL 
            LEFT JOIN user U ON TL.engineer = U.email 
            LEFT JOIN tasks T ON TL.tasks_sid = T.sid 
            LEFT JOIN ticket_log TKL ON T.ticket_sid = TKL.sid 
                WHERE 1 
                AND TKL.sid = (SELECT MAX(sid) FROM ticket_log WHERE TKL.sid = sid)  
                AND TL.sid = (SELECT MAX(sid) FROM tasks_log WHERE TL.tasks_sid = tasks_sid AND TL.engineer = engineer ) AND TL.status = '0' 
            GROUP BY TL.tasks_sid,TL.engineer ";
    $q = $conn->prepare($sql);
    $q->execute();
    $r = $q->fetchAll();


    echo "<pre>";
    print_r($r);
    echo "</pre>";
    // exit();

    foreach ($r as $key => $value) {
        $message = "Please response!";//"Accept or Reject? onsite ".$value['end_user_site']." at ".$value['appointment'];
        $response['error'] = false;
        $tmp = array();
        $tmp['message_id'] = $key+1;
        $tmp['chat_room_id'] = $key+1;
        $tmp['message'] = $message;
        $tmp['created_at'] = $value['current'];
        $response['message'] = $tmp;

        if ($response['error'] == false) {
            require_once '/home/flguploa/domains/flgupload.com/public_html/apis/libs/gcm/gcm.php';
            require_once '/home/flguploa/domains/flgupload.com/public_html/apis/libs/gcm/push.php';
            $gcm = new GCM();
            $push = new Push();
     
            // $user = $db->getUser($to_user_id);
            $user = array(
                "user_id" => $key,
                "name"=> $value['engineer'].' '.$value['end_user_site'],
                "email"=> $value['engineer'],
                "gcm_registration_id"=> $value['gcm_registration_id'],
                "created_at"=> $value['current_datetime']
            );

            $data = array();
            $data['user'] = $user;
            $data['message'] = $response['message'];
            $data['image'] = '';
            // $data['image'] = 'http://www.androidhive.info/wp-content/uploads/2016/01/Air-1.png';
     
            $push->setTitle("Accept or Reject?");
            $push->setIsBackground(FALSE);
            $push->setFlag(PUSH_FLAG_USER);
            $push->setData($data);
     
            // sending push message to single user
            $gcm->send($user['gcm_registration_id'], $push->getPush());
     
            $response['user'] = $user;
            $response['error'] = false;
        }
    }
?>