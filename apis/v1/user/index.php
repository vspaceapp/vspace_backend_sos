<?php
ob_start('ob_gzhandler');
error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/FCM.php';
require_once '../../include/SendMail.php';
// require_once '../../include/db_handler.php';
require_once '../../include/UserModel.php';
require_once '../../include/LocationModel.php';
// require_once '../../include/SendEmailvSpace.php';
require_once '../../include/TokenModel.php';
require_once '../../include/WSCdgModel.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development'
));
// $app->contentType('application/json');
$app->post('/updateRegisterReact', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $code_version = $app->request->post('code_version');
    $device_platform = $app->request->post('device_platform');
    $device_version = $app->request->post('device_version');
    $registrationId = $app->request->post('registrationId');

    $obj = new UserModel();
    $obj->updateRegisterReact($email, $token, $code_version, $device_platform, $device_version, $registrationId);
});

$app->post('/validate', function() use ($app){
    // $app->withHeader('Content-Type', 'application/json');
    // $app->contentType('application/json');
    $data = $app->request->getBody();
    // $json = json_decode($data, true);
    print_r($data);
    // // echo gettype($json);
    // $data = json_decode(html_entity_decode($app->request->getBody()),true);
    // // print_r($data);
    // // echo $data[''];
    $response = array();
    $response['data'] = ($data);
    echoRespnse(200, $response);
});


$urlCdgWebService = "http://www2.cdg.co.th/wcfvspace/ServiceVspace.svc/rest/";

function cdgWebService($url , $data, $token=""){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
        "postman-token: e2bbdf0d-849f-7ad9-4d8f-1c028a9b779d",
        "token: ".$token.""
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      return $response;
    }
}

$app->post('/CheckUserAuthenticationTokenByEmpNo', function() use ($app, $urlCdgWebService){
    $userID = $app->request->post('UserID');
    $groupID = $app->request->post('GroupID');
    $data = array('UserID'=>$userID,'GroupID'=>$groupID);

    $url = $urlCdgWebService."CheckUserAuthenticationTokenByEmpNo";
    $res = cdgWebService($url, $data);
    $response = array();
    $response["error"] = true;
    $response["data"] = json_decode($res);
    echoRespnse(200, $response);
});

$app->get('/GetEmployee_F', function() use ($app, $urlCdgWebService){
	$userID = $app->request->get('UserID');

    $CompID = $app->request->get('CompID');
    $groupID = $app->request->get('GroupID');
    $data = array('UserID'=>$userID,'GroupID'=>$groupID);

    $url = $urlCdgWebService."CheckUserAuthenticationTokenByEmpNo";
    $res = cdgWebService($url, $data);
	$token = json_decode($res, true);

	$data = array('CompID'=>$CompID,'GroupID'=>$groupID);
    $url = $urlCdgWebService."GetEmployee_F";
    $res = cdgWebService($url, $data, $token['Result']);
    $response = array();
    $response["error"] = true;
    $response["token"] = $token;
    $response["data"] = json_decode($res);
    echoRespnse(200, $response);

    $model  = new WSCdgModel();
    $data = json_decode($res, true);
    // $length = count($data['Result']);
 	// for($i=0;$i<$length;$i++){
 		// print_r($data['Result'][$i]);
 		// $update = $model->selectFromWS($data['Result'][$i]);
 	// }
    // echoRespnse(200, $response);
});


$app->get('/Get_ContandPreContLite_F', function() use ($app, $urlCdgWebService){
	$userID = $app->request->get('UserID');

    $CompID = $app->request->get('CompID');
    $groupID = $app->request->get('GroupID');
    $data = array('UserID'=>$userID,'GroupID'=>$groupID);

 	$url = $urlCdgWebService."CheckUserAuthenticationTokenByEmpNo";
    $res = cdgWebService($url, $data);
	$token = json_decode($res, true);

	$data = array('GroupID'=>$groupID, 'CompID'=>$CompID);
    $url = $urlCdgWebService."Get_ContandPreContLite_F";
    $res = cdgWebService($url, $data, $token['Result']);
    $response = array();
    $response["error"] = true;
    // $response["token"] = $token;
    $response["data"] = json_decode($res);
    echoRespnse(200, $response);
});




$app->post('/Leave_GetLeaveTran_F', function() use ($app,$urlCdgWebService){
    $EmpNo = $app->request->post('EmpNo');
    $LvTranYear = $app->request->post('LvTranYear');
    $groupID = $app->request->post('GroupID');
    $data = array('EmpNo'=>$EmpNo, 'LvTranYear'=>$LvTranYear , 'GroupID'=>$groupID);

    $url = $urlCdgWebService."GetEmployee_F";
    $res = cdgWebService($url, $data, "c3a8143c-1a93-4d51-ac9f-d37f6bcb4378");
    $response = array();
    $response["error"] = true;
    $response["data"] = json_decode($res);
    echoRespnse(200, $response);
});

$app->post('/GetEmpTrainingSchedule', function() use ($app, $urlCdgWebService){
    $EmpNo = $app->request->post('EmpNo');
    $groupID = $app->request->post('GroupID');
    $data = array('EmpNo'=>$EmpNo, 'GroupID'=>$groupID);

    $url = $urlCdgWebService."GetEmpTrainingSchedule";
    $res = cdgWebService($url, $data, "c3a8143c-1a93-4d51-ac9f-d37f6bcb4378");
    $response = array();
    $response["error"] = true;
    $response["data"] = json_decode($res);
    echoRespnse(200, $response);
});


$app->post('/listUserCanAddTask', function() use ($app){
    verifyRequiredParams(array('email','token'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $contract_no = $app->request->post('contract_no');
    $search = $app->request->post('search');

    $userModel = new UserModel();
    $data = $userModel->listUserCanAddTask($email,$search);

    $response = array();
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});

$app->post('/users', function() use ($app){
    // verifyRequiredParams(array('?'))
    $response = array();
    $response["error"] = true;
    if($app->request->post('email')){
        $sort = $app->request->post('sort');
        $userModel = new UserModel();
        $email = $app->request->post('email');
        $response["error"] = false;
        $search = $app->request->post('search');

        $data = $userModel->users($email, $sort['column'], $sort['type'], $search);
        $response['data'] = $data;
    }

    echoRespnse(200, $response);
});

$app->post('/updateUser', function() use ($app){
    // verifyRequiredParams(array('?'))
    $response = array();
    $response["error"] = true;
    if($app->request->post('email')){
        $email = $app->request->post('email');
        $userModel = new UserModel();
        $token = $app->request->post('token');
        $response["error"] = false;
        $data = $app->request->post('data');
        $userModel->updateUser($email, $data);
        $response['data'] = $data;
    }

    echoRespnse(200, $response);
});

$app->get('/userShowData', function() use ($app){
    $model = new UserModel();
    $data = $model->selectUser();
    print_r($data);
    echo '<img src="'.( $data['company_logo_base64'] ).'"/>';
});

$app->post('/updateProfile', function() use ($app){
    $token = $app->request->post('token');
    $email = $app->request->post('email');

    $mobile = $app->request->post('mobile');
    $name = $app->request->post('name');
    $company = $app->request->post('company');
    $company_logo = $app->request->post('company_logo');
    $picture_profile = $app->request->post('picture_profile');

    $model = new UserModel();

    if(isset($_FILES['file'])){
        $file = $_FILES['file'];
        // require_once '../../include/PictureControl.php';
    }else{
        $file = array();
    }

    if(isset($_FILES['file_picture_profile'])){
        $file_picture_profile = $_FILES['file_picture_profile'];
    }else{
        $file_picture_profile = array();
    }
    $data = array(
        'email'=>$email,
        'mobile'=>$mobile,
        'company'=>$company,
        'company_logo'=>$company_logo,
        'picture_profile'=>$picture_profile,
        'name'=>$name
    );

    $model->updateProfile($file, $email, $token, $data, $file_picture_profile);

    $response = array();

    $response['token'] = $token;
    $response['file'] = $file;
    echoRespnse(200, $response);
});


$app->post('/header', function() use ($app){
    verifyRequiredParams(array('token'));
    $token = $app->request->post('token');
    $email = $app->request->post('email');
    $remedyProductCategory = $app->request->post("remedy_resolution_product_category");
    $remedyProductCategoryRequest = $app->request()->post("remedy_resolution_product_category_request");
    require_once '../../include/ProjectModel.php';
    $obj = new ProjectModel($email);
    $canAssignTo = $obj->listUserCanAddProject($email);
    $listUserCanAddTask = $obj->listUserCanAddTask($email);
    $response = array();
    $userModel = new UserModel();
    $userSID = $userModel->getInformationUserViaToken(array('token'=>$token,'email'=>$email));
    $remedyProduct = $userModel->getRemedyCategoryByType($remedyProductCategory);
    $remedyProductRequest = $userModel->getRemedyCategoryByType($remedyProductCategoryRequest);
    if($userSID){
        $userInfo = $userModel->getInformationUser(array('sid'=>$userSID));
        $response['data'] = $userInfo;
    }else{
        $response['data'] = array();
    }
    $response['remedy_resolution_product_category_request'] = $remedyProductRequest;
    $response['remedy_resolution_product_category'] = $remedyProduct;
    $response['my_staff'] = $canAssignTo;
    $response['listUserCanAddTask'] = $listUserCanAddTask;
    $response['token'] = $token;
    echoRespnse(200, $response);
});
$app->post('/confirmForgotPasswordReact', function() use ($app){
    $data = json_decode($app->request->post('data'), true);
    $obj = new UserModel();
    $res = $obj->confirmForgotPassword($data);

    $response = array();
    if($res){
        $response['error'] = false;
        $obj->updatePassword($data);
        $response['message'] = "Your password has been changed successfully!";
    }else{
        $response['error'] = true;
        $response['message'] = "Not Found OTP";
    }
    $response['data'] = $res;
    echoRespnse(200, $response);
});
$app->post('/confirmForgotPassword', function() use ($app){
    $data = $app->request->post('data');
    $obj = new UserModel();
    $res = $obj->confirmForgotPassword($data);

    $response = array();
    if($res){
        $response['error'] = false;
        $obj->updatePassword($data);
        $response['message'] = "Your password has been changed successfully!";
    }else{
        $response['error'] = true;
        $response['message'] = "Not Found OTP";
    }
    $response['data'] = $res;
    echoRespnse(200, $response);
});
$app->post('/forgotPasswordReact', function() use ($app){
    $data = json_decode($app->request->post('data'), true);

    $obj = new UserModel();
    $data = $obj->forgotPassword($data);

    $response = array();
    $response['message'] = "";
    if(!$data){
        $response['error'] = true;
        $response['message'] = "Not Found Email";
    }else{
        $response['error'] = false;
    }
    $data['otp'] = '';
    $data['password'] = '';
    $data['gcm_registration_id'] = '';
    $data['remedy_password'] = '';
    $data['path_signature'] = '';
    $data['picture_profile'] = '';

    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/forgotPassword', function() use ($app){
    $data = $app->request->post('data');

    $obj = new UserModel();
    $data = $obj->forgotPassword($data);

    $response = array();
    $response['message'] = "";
    if(!$data){
        $response['error'] = true;
        $response['message'] = "Not Found Email";
    }else{
        $response['error'] = false;
    }
    $response['data'] = $data;
    echoRespnse(200, $response);
});
$app->post('/signup', function() use ($app){
    $data = $app->request->post('data');
    $obj = new UserModel();
    $data = $obj->signUp($data);

    $response = array();

    $response['error'] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
    // print_r($data);
});

$app->post('/signupReact', function() use ($app){
    $data = json_decode($app->request->post('data'), true);
    $obj = new UserModel();
    $data = $obj->signUp($data);

    $response = array();

    $response['error'] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
    // print_r($data);
});
$app->post('/comfirmOtpReact', function() use ($app){
    $data = json_decode($app->request->post('data'), true);
    $obj = new UserModel();
    $res = $obj->comfirmOtp($data);

    $response = array();
    $response['message'] = "Confirm Success!";
    if(!$res){
        $data = array();
        $response['message'] = "OTP Incorrect!";
    }
    $response['error'] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});



$app->post('/comfirmOtp', function() use ($app){
    $data = $app->request->post('data');
    $obj = new UserModel();
    $res = $obj->comfirmOtp($data);

    $response = array();
    $response['message'] = "Confirm Success!";
    if(!$res){
        $data = array();
        $response['message'] = "OTP Incorrect!";
    }
    $response['error'] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});
// define('PUSH_FLAG_CHATROOM', 1);
// define('PUSH_FLAG_USER', 2);
$app->post('/sendLocation', function() use ($app){
    $email = $app->request->post('email');
    $latitude = $app->request->post('latitude');
    $longitude = $app->request->post('longitude');
    $altitude = $app->request->post('altitude');
    $accuracy = $app->request->post('accuracy');
    $altitudeAccuracy = $app->request->post('altitudeAccuracy');
    $heading = $app->request->post('heading');
    $speed = $app->request->post('speed');
    $code_version = $app->request->post('code_version');
    $device_platform = $app->request->post('device_platform');
    $device_version = $app->request->post('device_version');
    $registrationId = $app->request->post('registrationId');

    if($email!="autsakorn.t@firstlogic.co.th" || 1){
        $db = new LocationModel();
        $response = $db->sendLocation($email,$latitude,$longitude,$altitude,$accuracy,$altitudeAccuracy,$heading,$speed,$code_version,$device_platform,$device_version,$registrationId);
    }else{
        $response = array();
    }
    // echo json response
    echoRespnse(200, $response);
});
$app->post('/Profile', function() use ($app){

    $email = $app->request->post('email');
    $emailView = $app->request->post('emailView');

    $obj = new UserModel();
    $data = $obj->getProfile($emailView);

    $response = array();

    $response['error'] = false;
    $response['data'] = $data;
    $response['can_gen_new_signature'] = 0;
    if($data['emailaddr']==$email){
        $response['can_gen_new_signature'] = 1;
    }
    echoRespnse(200, $response);
});
$app->post('/listLocationUser', function() use ($app){
    $email = $app->request->post('email');
    $db = new LocationModel();
    $data = $db->selectAllUserLocation($email);
    $response = array();

    $response['error'] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

// $app->post('/login', function() use ($app){

// });
$app->post('/login', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('email', 'password'));

    // reading post params
    $email = $app->request->post('email');
    $password = $app->request->post('password');

    $registration_id = $app->request->post('registration_id');
    $device_model = $app->request->post('device_model');
    $device_platform = $app->request->post('device_platform');
    $device_version = $app->request->post('device_version');

    // validating email address
    // validateEmail($email);

    $db = new DbHandler();
    // $response = array();
    $response = $db->checklogin($email, $password,$registration_id,$device_model,$device_platform,$device_version);
    // $response = array();
    // $response['error'] = false;
    // $response['status'] = 200;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/update_register_fcm', function() use ($app){
    verifyRequiredParams(array('Token','Email'));
    $token = $app->request->post('Token');
    $email = $app->request->post('Email');

    $response = array();
    $obj = new UserModel();
    $obj->updateRegisterFCM($token, $email);

    $response['error'] = false;
    $response['token'] = $token;

    echoRespnse(200, $response);
});

$app->post('/send', function() use ($app){
    require_once '../../libs/gcm/gcm.php';
    require_once '../../libs/gcm/push.php';
        $gcm = new GCM();
        $push = new Push();
        $user = array(
                "user_id" => "1",
                "name"=> "Mas",
                "email"=> "admin@androidhive.info",
                "gcm_registration_id"=> "dyBTqY8yncY:APA91bGvVM76pibKxK9XbFq8T1iBclV8NWPsjmSBBDnGnOHJvaOrZYXit99yUxtTyuHgA55-bMHk1LWLRrG76985krU82LcfWYKKfEjHlfoP6_thSOI5odflUPHylbSOfAW8GCYAgMN_",
                "created_at"=> "2016-02-12 15:13:57"
                );

        $data = array();
        $data['user'] = $user;
        $data['message'] = "TEST";
        $data['image'] = '';
        $push->setTitle("Super service");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_USER);
        $push->setData($data);
        echo $gcm->send($user['gcm_registration_id'], $push->getPush());
});

$app->post('/register', function() use ($app){
    verifyRequiredParams(array('Token'));
    $token = $app->request->post('Token');

    $response = array();
    $fcm = new FCM();
    $fcm->addToken($token);

    $response['error'] = false;
    $response['token'] = $token;


    echoRespnse(200, $response);
});

// ดึงข้อมูล serial ของ ticket
$app->post('/select/serial_ticket', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_sid'));

    // reading post params
    $ticket_sid = $app->request->post('ticket_sid');


    $pmModel = new PreventiveMaintenanceModel();
    $serial = $pmModel->selectSerialTicket($ticket_sid);


    $response = array();
    $response['error'] = false;

    $response['serial'] = $serial;
    $response['data'] = array(
            "data"=>"data"
        );
    // echo json response
    echoRespnse(200, $response);
});

// ดึงข้อมูลเพื่อแสดงว่า ticket ให้ทำ PM อะไรบ้าง
$app->post('/select/to_do_pm', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_sid'));

    // reading post params
    $ticket_sid = $app->request->post('ticket_sid');

    $pmModel = new PreventiveMaintenanceModel();
    $data = $pmModel->selectToDoPM($ticket_sid);


    $response = array();
    $response['error'] = false;
    $response['todppm'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

$app->post('/insert/do_serial_pm', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('ticket_serial_sid','task_sid','status','email'));

    // reading post params
    $ticket_serial_sid = $app->request->post('ticket_serial_sid');
    $task_sid = $app->request->post('task_sid');
    $status = $app->request->post('status');
    $email = $app->request->post('email');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('ticket_serial_sid'=>$ticket_serial_sid,'task_sid'=>$task_sid,'status'=>$status,'create_by'=>$email);

    $result = $pmModel->insertDoSerialPM($param);


    $response = array();
    $response['error'] = false;
    $response['result'] = $result;

    // echo json response
    echoRespnse(200, $response);
});



$app->post('/insert/input_action', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('task_sid','email'));

    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');
    $pm_service = $app->request->post('pm_service');

    $pmModel = new PreventiveMaintenanceModel();

    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email, 'pm_service'=>$pm_service);

    $result = $pmModel->insertInputAction($param);


    $response = array();
    $response['error'] = false;
    $response['result'] = $result;

    // echo json response
    echoRespnse(200, $response);
});

/*****START CREATE CASE PM****/
$app->post('/createcasepm', function() use ($app) {

        require_once '../../include/db_connect.php';
        $connect = new DbConnect();
        $db = $connect->connect();
        $email = $app->request->post('email');
        $due_datetime = $app->request->post('due_datetime');

        $subject = $app->request->post('subject');
        $contract_no = $app->request->post('contract_no');
        $description = $app->request->post('description');
        $end_user_company_name = $app->request->post('end_user_company_name');
        $end_user_site = $app->request->post('end_user_site');
        $end_user_contact_name = $app->request->post('end_user_contact_name');
        $end_user_phone = $app->request->post('end_user_phone');
        $end_user_mobile = $app->request->post('end_user_mobile');
        $end_user_email = $app->request->post('end_user_email');

        $todoPmArray = json_decode($app->request->post('todoPmArray'), true);
        // print_r($todoPmArray);
        $pm = array();
        foreach ($todoPmArray as $key => $value) {
            $pm["pm".($key+1)]['name'] = $value['name'];
            $pm["pm".($key+1)]['value'] = $value['to_do'];
        }


        $serialArray = json_decode($app->request->post('serialArray'));
        $listSelectEngineer = json_decode($app->request->post('listSelectEngineer'));


        include "/home/flguploa/domains/flgupload.com/public_html/".END_POINT_APIS."/include/OpenPM.php";

        $dataSaveTicket = array(
            ':no_ticket'=>genTicketNo($db),
            ':via'=>'pmsystem',
            ':create_by'=>$listSelectEngineer[0]
        );
        $owner = vPost($listSelectEngineer[0]);
        // exit();
        $ticketSid = saveTicketManual($dataSaveTicket,$db);///////////(1) ///// saveTicket

        $dataSaveTicketLog = array(
            ':ticket_sid'=>$ticketSid,
            ':owner'=>$owner,
            ':status'=>'1',
            ':subject'=>vPost($subject),
            ':description'=>vPost($description),
            ':contract_no'=>vPost($contract_no),
            ':prime_contract'=>'',
            ':project_name'=>vPost(""),
            ':serial_no'=>join(',',$serialArray),
            ':team'=>'SSS',
            ':requester_full_name'=>'',
            ':requester_company_name'=>'',
            ':requester_phone'=>'',
            ':requester_mobile'=>'',
            ':requester_email'=>'',
            ':end_user_company_name'=>vPost($end_user_company_name),
            ':end_user_site'=>vPost($end_user_site),
            ':end_user_contact_name'=>vPost($end_user_contact_name),
            ':end_user_phone'=>vPost($end_user_phone),
            ':end_user_mobile'=>vPost($end_user_mobile),
            ':end_user_email'=>vPost($end_user_email),
            ':case_type'=>'Preventive Maintenance',
            ':source'=>'pmsystem',
            ':note'=>'',
            ':urgency'=>'Normal',
            ':create_by'=>vPost($email),
            ':due_datetime'=>$due_datetime
        );

        echo "<pre>";
        print_r($dataSaveTicket);
        echo "</pre>";

        echo "<pre>";
        print_r($dataSaveTicketLog);
        echo "</pre>";

        saveTicketLog($dataSaveTicketLog, $db);////////////////// (2) save to ticket log
        saveTicketSerial($ticketSid,$serialArray,$db);////////////////// (3) save ticket serial
        saveTasksManual($ticketSid,$listSelectEngineer,$due_datetime, $db, $pm); ////////////// (4) save task

});
/*****END/

/*****START CREATE TASK PM****/
$app->post('/createtaskpm', function() use ($app) {

        require_once '../../include/db_connect.php';
        $connect = new DbConnect();
        $db = $connect->connect();
        $email = $app->request->post('email');
        $appointment = $app->request->post('appointment');
        $ticket_sid = $app->request->post('ticket_sid');
        $listSelectEngineer = json_decode($app->request->post('engineerAssign'), true);


        include "/home/flguploa/domains/flgupload.com/public_html/".END_POINT_APIS."/include/OpenPM.php";
        $pm = array();
        saveTasksManual($ticket_sid,$listSelectEngineer,$appointment, $db, $pm); ////////////// (4) save task

});
/*****END CREATE TASK/ ***/

$app->post('/sendoutlook/:id', function($sr_sid){
    global $app;

    $obj = new PreventiveMaintenanceModel();
    $data = $obj->selectInformationTask($sr_sid);
    print_r($data);
    require_once '../../include/Outlook.php';
    $outlook = new Outlook();

    foreach ($data as $key => $value) {
        $data = array();
        $data['method'] = "ADD";
        $data['attendee'] = $value['engineer'];
        $data['end'] = $value['appointment'];
        $data['start'] = $value['endtime'];
        $data['location'] = $value['end_user_company_name'];
        $data['organizer'] = $value['subject'];
        $data['UID'] = $value['tasks_sid'];
        $data['title'] = $value['end_user_company_name'];
        $data['summary'] = $value['subject']." ".$value['serial_no']." ".$value['end_user_company_name']." ".$value['end_user_site']." ".$value['end_user_contact_name']." ".$value['end_user_phone']." ".$value['end_user_email'];
        $data['from'] = "flguploa@flgupload.com";
        $data['to'] = $value['engineer'];
        // $data['to'] = "autsakorn.t@firstlogic.co.th";

        $outlook->sendEvent($data);
    }

});

/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;

//     verifyRequiredParams(array('gcm_registration_id'));

//     $gcm_registration_id = $app->request->put('gcm_registration_id');

//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);

//     echoRespnse(200, $response);
// });



/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
