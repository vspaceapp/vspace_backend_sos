<?php
ob_start('ob_gzhandler');
error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ServiceReportModel.php';
// require_once '../../include/CaseModel.php';
// require_once '../../include/GenNoCaseSR.php';
require_once '../../include/db_handler.php';
require_once '../../include/UserModel.php';
require_once '../../view/ServiceReportView.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'mode' => 'development'
));
foreach (glob("api/*.php") as $filename){require_once ($filename);}
$app->post('/', $index);
$app->post('/detail',$detail);
$app->post('/create',$create);
$app->post('/edit',$edit);
$app->post('/editContactAfterClose', $editContactAfterClose);
$app->post('/timestampSr', $timestampSr);
$app->post('/input_action', $input_action);
$app->post('/notification', $notification);
$app->post('/delete', $delete);
$app->get('/ExpressReport/:name/:c1/:c2', $expressReport);
$app->post('/ServiceReportEditAddress',$ServiceReportEditAddress);


$app->post('/appointment', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');

    $db = new DbHandler();
    $result = $db->getTaskDetail($tasks_sid, $email);

    $response = array();
    $response["error"] = false;
    if(isset($result[0])){
        $response["data"] = $result[0];
    }else{
        $response["data"] = false;
    }
    echoRespnse(200, $response);
});

$app->post('/templateServiceReportUnsigned', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $servicereportUnsignedSort = $app->request->post('servicereportUnsignedSort');

    $data = array();
    $db = new DbHandler();
    $data = $db->getUnsigned($email,$servicereportUnsignedSort);


    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    $response['template'] = templateServiceReportUnsigned($data);
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/listSR',function() use ($app){
    // verifyRequiredParams(array('day'));
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $obj = new ServiceReportModel();
    $data = $obj->loadAllServiceReport();


    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);

});

$app->post('/dataSr', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $obj = new ServiceReportModel();
    $data = $obj->dataSr($tasks_sid, $email, $token, $ticket_sid);
    if(strpos(":", $data['expect_duration']>0)){

    }else{
        $data['expect_duration'] = $data['expect_duration'].":00";
    }

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;

    $backTo = "case_detail/".$ticket_sid."//3";

    $response['html_app'] = genHtmlAppServiceReport($data,$backTo);

    $response['html_app_change_appointment'] = "";
    echoRespnse(200, $response);
});

$app->post('/dataSr2', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $obj = new ServiceReportModel();


    $data = $obj->dataSr($tasks_sid, $email, $token, $ticket_sid);
    if(strpos(":", $data['expect_duration']>0)){

    }else{
        $data['expect_duration'] = $data['expect_duration'].":00";
    }

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    $backTo = "CaseDetail/CaseWip/left/".$ticket_sid."/";

    $view = new ServiceReportView($email, $ticket_sid, $tasks_sid);
    $response['html_app'] = $view->genHtmlAppServiceReport($data,$backTo);

    $response['html_app_change_appointment'] = "";
    echoRespnse(200, $response);
});

$app->post('/editSrEngineer', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $case_change_engineer = array();
    if($app->request->post('case_change_engineer')){
        $case_change_engineer = $app->request->post('case_change_engineer');
    }

    $step = 1;
    if($app->request->post('step')){
        $step = $app->request->post('step');
    }

    $obj = new ServiceReportModel();
    $data = $obj->dataSr($tasks_sid, $email, $token, $ticket_sid);

    $notEngineer = array();
    if($data['associate_engineer']){
        $associate_engineer = explode(",", $data['associate_engineer']);
        $notEngineer = $associate_engineer;
    }
    array_push($notEngineer, $data['engineer']);

    $userModel = new UserModel();
    $listUserCanAddTask = $userModel->listUserCanAddTask($email,"", $notEngineer);

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    $response['listUserCanAddTask'] = $listUserCanAddTask;
    $response['notEngineer'] = $notEngineer;

    $view = new ServiceReportView($email, $ticket_sid, $tasks_sid);
    if($step==1){
        $response['html_app'] = $view->genHtmlAppEditSrEngineer($listUserCanAddTask, $ticket_sid, $tasks_sid);
    }else{
        $response['html_app'] = $view->genHtmlAppEditSrEngineerConfirm($data, $ticket_sid, $tasks_sid, $case_change_engineer);
    }
    echoRespnse(200, $response);
});

$app->post('/changeToEmail', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $step = 1;
    if($app->request->post('step')){
        $step = $app->request->post('step');
    }
    $changeToEmail = "";

    $data = array();
    if($app->request->post('changeToEmail')){
        $changeToEmail = $app->request->post('changeToEmail');
    }
    $obj = new ServiceReportModel();
    $obj->changeToEmail($tasks_sid, $changeToEmail, $email, $token);
    $response = array();
    $response['error'] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

$app->post('/editAssociateEngineer', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $step = 1;
    if($app->request->post('step')){
        $step = $app->request->post('step');
    }

    $case_change_engineer = array();
    if($app->request->post('case_change_engineer')){
        $case_change_engineer = $app->request->post('case_change_engineer');
    }

    $obj = new ServiceReportModel();
    $data = $obj->dataSr($tasks_sid, $email, $token, $ticket_sid);

    $notEngineer = array();
    array_push($notEngineer, $data['engineer']);

    $userModel = new UserModel();
    $listUserCanAddTask = $userModel->listUserCanAddTask($email,"", $notEngineer);

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    // $response['listUserCanAddTask'] = $listUserCanAddTask;
    $view = new ServiceReportView($email, $ticket_sid, $tasks_sid);
    $response['html_app'] = $view->genHtmlEditAssociateEngineer($data, $case_change_engineer);
    echoRespnse(200, $response);
});

$app->post('/addAssociateEngineer', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $step = 1;
    if($app->request->post('step')){
        $step = $app->request->post('step');
    }
    $case_change_engineer = array();
    if($app->request->post('case_change_engineer')){
        $case_change_engineer = $app->request->post('case_change_engineer');
    }

    $obj = new ServiceReportModel();
    $data = $obj->dataSr($tasks_sid, $email, $token, $ticket_sid);

    $notEngineer = array();
    if($data['associate_engineer']){
        $associate_engineer = explode(",", $data['associate_engineer']);
        $notEngineer = $associate_engineer;
    }
    array_push($notEngineer, $data['engineer']);

    $userModel = new UserModel();
    $listUserCanAddTask = $userModel->listUserCanAddTask($email,"", $notEngineer);

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    $response['listUserCanAddTask'] = $listUserCanAddTask;
    $view = new ServiceReportView($email, $ticket_sid, $tasks_sid);
    if($step=="1"){
        $response['html_app'] = $view->genHtmlAddAssociateEngineer($listUserCanAddTask, $ticket_sid, $tasks_sid);
    }else if($step=="2"){
        $response['html_app'] = $view->genHtmlAddAssociateEngineerComfirm($listUserCanAddTask, $ticket_sid, $tasks_sid, $case_change_engineer);
    }
    echoRespnse(200, $response);
});

$app->post('/addAssociateEngineerConfirmed', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $step = 1;
    if($app->request->post('step')){
        $step = $app->request->post('step');
    }
    $case_change_engineer = array();
    if($app->request->post('case_change_engineer')){
        $case_change_engineer = $app->request->post('case_change_engineer');
    }

    $obj = new ServiceReportModel();
    $data = $obj->addAssociateEngineer($tasks_sid, $email, $token, $ticket_sid, $case_change_engineer);

    $response = array();
    $response['error'] = false;
    $response['data'] = $data;
    $response['case_change_engineer'] = $case_change_engineer;
    echoRespnse(200, $response);
});
$app->post('/removeEngineerConfirm',function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_sid = $app->request->post('tasks_sid');
    $ticket_sid = $app->request->post('ticket_sid');

    $code_version = $app->request->post('code_version');
    $platform = $app->request->post('platform');

    $step = 1;
    if($app->request->post('step')){
        $step = $app->request->post('step');
    }

    $emailRemove = $app->request->post('emailRemove');

    $obj = new ServiceReportModel();
    $data = $obj->removeEngineerConfirm($tasks_sid, $email, $token, $ticket_sid, $emailRemove);

    $response = array();
    $response['error'] = false;
    // $response['data'] = $data;
    $response['emailRemove'] = $emailRemove;
    echoRespnse(200, $response);
});

$app->post('/EditTimestamp', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $tasks_log_sid = $app->request->post('tasks_log_sid');
    $date = $app->request->post('date');
    $time = $app->request->post('time');

    $obj = new  ServiceReportModel();
    $obj->editTimestamp($email, $tasks_log_sid, $date, $time);
    $response = array();
    $response["error"] = false;
    // $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/EditTimeAppointment', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $task_sid = $app->request->post('task_sid');
    $date = $app->request->post('date');
    $time = $app->request->post('time');


    $obj = new  ServiceReportModel();
    $obj->editAppointment($email, $task_sid, $date, $time);
    $response = array();
    $response["error"] = false;
    // $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/EditTimeAppointmentMobile', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $task_sid = $app->request->post('task_sid');
    $datetime = $app->request->post('datetime');
    // $time = $app->request->post('time');

    $obj = new  ServiceReportModel();
    $obj->editAppointmentMobile($email, $task_sid, $datetime);
    $response = array();
    $response["error"] = false;
    // $response['data'] = $data;
    // echo json response
    echoRespnse(200, $response);
});

//ADD 11/11/2016
$app->post('/EditTimeAppointmentMobile1', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $task_sid = $app->request->post('task_sid');
    $datetime = $app->request->post('datetime');

    $expect_duration = "";
    if($app->request->post('expect_duration')){
        $expect_duration = $app->request->post('expect_duration');
    }
    // $time = $app->request->post('time');

    $obj = new  ServiceReportModel();
    $return = $obj->editAppointmentMobile1($email, $task_sid, $datetime,$expect_duration);
    $response = array();
    $response["error"] = false;
    $response['data'] = $datetime;
    $response['return'] = $return;
    $response['rand'] = rand(0,10000);
    // echo json response
    echoRespnse(200, $response);
});


$app->post('/listEngineer', function() use ($app){
    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $engineer = array();
    $engineerList = array("name"=>"Teerawut","location"=>"","lat"=>"2","lon"=>"2.9","OT"=>"10","balance"=>"9");
    array_push($engineer, $engineerList);
    $engineerList = array("name"=>"Autsakron","location"=>"","lat"=>"3","lon"=>"2.5","OT"=>"20","balance"=>"3");
    array_push($engineer, $engineerList);
    $engineerList = array("name"=>"Visa","location"=>"","lat"=>"4","lon"=>"2.2","OT"=>"15","balance"=>"1");
    array_push($engineer, $engineerList);
    $engineerList = array("name"=>"Navares","location"=>"","lat"=>"5","lon"=>"3.2","OT"=>"2","balance"=>"5");
    array_push($engineer, $engineerList);
    $objModel = new UserModel();
    $data = $objModel->listUserCanAddTask($email);
    foreach ($data as $key => $value) {
        $data[$key]['name'] = $value['thainame'];
        $data[$key]['location'] = "";
        $data[$key]['factor'] = array();
        $location = $objModel->getLastLocationEmployee($value['email']);
        $data[$key]['lat'] = $location['latitude'];
        $data[$key]['lon'] = $location['longitude'];
        $data[$key]['grade'] = $key;
        // if($key>5){
            $data[$key]['OT'] = $key;
        // }else{
        //     $data[$key]['ot'] = "20";
        // }
        // if($key>10){
            $data[$key]['balance'] = $key+1;
        // }else{
        //     $data[$key]['balance'] = "20";
        // }
    }
    $response = array();
    $response["error"] = false;
    $response['data'] = $data;
    echoRespnse(200, $response);
});
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;

//     verifyRequiredParams(array('gcm_registration_id'));

//     $gcm_registration_id = $app->request->put('gcm_registration_id');

//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);

//     echoRespnse(200, $response);
// });

 function genHtmlAppServiceReport($data,$backTo){
        $html = '<header class="bar bar-nav">
              <a class="icon icon-left-nav pull-left" href="#'.$backTo.'"></a>
            <div class="" style="width: 210px"><h1 class="title">'.$data['subject_service_report'].'</h1></div>
          </header>
          <div class="content">
              <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider cell-block">
                    <div>Info</div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Subject <span class="pull-right">'.$data['subject_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service No. <span class="pull-right">'.$data['no_task'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service Type <span class="pull-right">'.$data['service_type_name'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service Status <span class="pull-right">'.$data['status_name'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Service Report (PDF) <span class="pull-right">'.(($data['pdf_report']!="-")?'<a href="'.$data['pdf_report'].'">PDF</a>':'-').'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Taxi total <span class="pull-right">'.$data['taxi_fare_total'].'</span></p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider">Engineer</li>
                    <li class="table-view-cell ">'.
                        (($data['last_status']<'500')?'<a class="navigate-right" href="#EditSrEngineer/'.$data['ticket_sid'].'/'.$data['sid'].'/right/1">':'').
                        '<img class="media-object pull-left" src="'.((isset($data['engineer_info']['employee_pic']))?($data['engineer_info']['employee_pic']):'').'">
                            <div class="media-body"><p>'.((isset($data['engineer_info']['thainame']))?$data['engineer_info']['thainame']:'').'<br/><span class="">'.(isset($data['engineer_info']['email'])?$data['engineer_info']['email']:'').'</span><br/>'.(isset($data['engineer_info']['mobile'])?$data['engineer_info']['mobile']:'').'</p></div>'
                        .($data['last_status']<'500'?'</a>':'').
                    '</li>
                </ul>
            </div>

            <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider">Associate Engineer</li>';
                    if(count($data['associate_engineer_info'])>0){
                        foreach ($data['associate_engineer_info'] as $key => $value) {
                        $html .= ('<li class="table-view-cell ">'.
                            ($data['last_status']<'500'?'<a data-pic="'.(isset($value['employee_pic'])?$value['employee_pic']:'').'" data-name="'.(isset($value['thainame'])?$value['thainame']:'').'" data-email="'.$value['email'].'" data-mobile="'.(isset($value['mobile'])?$value['mobile']:'').'"  class="navigate-right" href="#EditAssociateEngineer/'.$data['ticket_sid'].'/'.$data['sid'].'/right/1">':'').
                            '<img class="media-object pull-left" src="'.(isset($value['employee_pic'])?$value['employee_pic']:'').'">
                                <div class="media-body"><p>'.(isset($value['thainame'])?$value['thainame']:'').'<br/><span class="">'.$value['email'].'</span><br/>'.(isset($value['mobile'])?$value['mobile']:'').'</p></div>'.
                            ($data['last_status']<'500'?'</a>':'').
                            '</li>');
                        }
                    }else{
                        $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>-</p></div></li>';
                    }
                    if($data['last_status']<'500'){
                        $html .= '<li class="table-view-cell cell-block"><div style=""><div style="float:right;text-align: center;width: 100px;"><a href="#AddAssociateEngineer/'.$data['ticket_sid'].'/'.$data['sid'].'/right/1"><p><span class="btn btn-positive btn-outlined btn-xs circle icon icon-plus"></span> <br/><small>Add Engineer</small></p></a></div></div></li>';
                    }
            $html .= '</ul>
            </div>

            <div class="card">
                <ul class="table-view">
                    <li class="table-view-divider">Appointment</li>
                    <li class="table-view-cell media cell-block ">
                        '.(($data['can_change_appointment'] && 1==2)?('
                            <a href="#EditTimeAppointment/{{data.ticket_sid}}/{{data.tasks_sid}}/{{data.appointment}}/{{view}}" class="navigate-right">
                              <div class="media-body">
                                <p>Start <span class="pull-right">'.$data['appointment'].'</span></p>
                              </div>
                            </a>
                        '):('
                          <div class="media-body">
                            <p>Start <span class="pull-right">'.$data['appointment'].'</span></p>
                          </div>
                        ')).'
                    </li>
                    <li class="table-view-cell media cell-block">
                      <div class="media-body">
                        <p>Expect Finish
                            <span class="pull-right">
                            '.$data['expect_finish'].'
                            </span>
                        </p>
                      </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                        <p>Expect Overtime
                            <span class="pull-right">
                            '.$data['overtime_expect'].'
                            </span>
                        </p>
                      </div>
                    </li>
                </ul>
              </div>
              <div class="card">
                  <ul class="table-view">
                    <li class="table-view-cell table-view-cell-divider">
                      '.(($data['can_change_end_user'] && 1==2)?('
                        <a href="#SrEditEndUser/{{data.ticket_sid}}/{{data.tasks_sid}}/{{view}}" class="navigate-right">
                          <p>End User Service Report </p>
                        </a>
                      '):('
                          <p>End User Service Report </p>
                      ')).'
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Name <span class="pull-right">'.$data['end_user_contact_name_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                          <p>Email <span class="pull-right">'.$data['end_user_email_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                            <p>Phone/Mobile<span class="pull-right">'.$data['end_user_phone_service_report'].'/ '.$data['end_user_mobile_service_report'].'</span></p>
                        </div>
                    </li>
                    <li class="table-view-cell media cell-block">
                        <div class="media-body">
                            <p>Company <span class="pull-right">'.$data['end_user_company_name_service_report'].'</span></p>
                        </div>
                    </li>
                  </ul>
              </div>
          </div>';
        return $html;
 }
function genHtmlAppEditSrEngineer($data, $ticket_sid, $tasks_sid){
    $html = '<header class="bar bar-nav">
              <a class="icon icon-left-nav pull-left" href="#service_report/'.$ticket_sid.'/'.$tasks_sid.'/left/other"></a>
            <div class="" style="width: 210px"><h1 class="title">Change Engineer</h1></div>
          </header>';
    $html .= '<div class="content list-engineer"><ul class="table-view">';
        if(count($data)>0){
            foreach ($data as $key => $value) {
                $html .= '<li class="table-view-cell cell-block_"><a class="navigate-right" data-name="'.$value['thainame'].'" data-email="'.$value['emailaddr'].'" data-mobile="'.$value['mobile'].'" data-pic="'.$value['pic_employee'].'" href="#EditSrEngineer/'.$ticket_sid.'/'.$tasks_sid.'/right/2"><img class="media-object pull-left" src="'.$value['pic_employee'].'" /><div class="media-body"><p>'.$value['thainame'].'<br/><span class="">'.$value['emailaddr'].'</span><br/>'.$value['mobile'].'</p></div></a></li>';
            }
        }else{
            $html .= '<li class="table-view-cell cell-block"><div><p>Not found employee that you can add task</p></div></li>';
        }
    $html .= '</div></ul>';
    return $html;
}

function genHtmlAppEditSrEngineerConfirm($data, $ticket_sid, $tasks_sid, $case_change_engineer){
    $html = '<header class="bar bar-nav">
              <a class="icon icon-left-nav pull-left" href="#EditSrEngineer/'.$ticket_sid.'/'.$tasks_sid.'/left/1"></a>
            <div class="" style="width: 210px"><h1 class="title">Confirm Change Engineer</h1></div>
          </header>';
    $html .= '<div class="content">';
    $html .= '<ul class="table-view">';
    $html .= '<li class="table-view-divider cell-block"><div>From Engineer</div></li>';
    $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$data['engineer_info']['employee_pic'].'" /><div class="media-body"><p>'.$data['engineer_info']['thainame'].'<br/>'.$data['engineer_info']['emailaddr'].'<br/>'.$data['engineer_info']['mobile'].'</p></div></li>';
    $html .= '<li class="table-view-divider cell-block"><div>To Engineer</div></li>';
    if(isset($case_change_engineer['name'])){
        // $html .= '<div>'.$case_change_engineer['name'].'</div>';
        $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$case_change_engineer['pic'].'" /><div class="media-body"><p>'.$case_change_engineer['name'].'<br/><span class="change_to">'.$case_change_engineer['email'].'</span><br/>'.$case_change_engineer['mobile'].'</p></div></li>';
        $html .= '<li class="table-view-cell cell-block"><div><button id="btnConfirmChangeEngineer" data-change-to="'.$case_change_engineer['email'].'" class="btn btn-block btn-positive">Done</button></div></li>';
    }
    $html .= '</ul>';
    $html .= '</div>';
    return $html;
}

function genHtmlAddAssociateEngineer($data, $ticket_sid, $tasks_sid){
    $html = '<header class="bar bar-nav">
              <a class="icon icon-left-nav pull-left" href="#service_report/'.$ticket_sid.'/'.$tasks_sid.'/left/other"></a>
              <a class="icon icon-right-nav pull-right" href="#AddAssociateEngineer/'.$ticket_sid.'/'.$tasks_sid.'/right/2"></a>
            <div class="" style="width: 210px"><h1 class="title">Add Associate Engineer</h1></div>
          </header>';
    $html .= '<div class="content list-engineer"><ul class="table-view">';
        if(count($data)>0){
            foreach ($data as $key => $value) {
                $html .= '<li class="table-view-cell cell-block_"><img class="media-object pull-left" src="'.$value['pic_employee'].'" /><div class="media-body"><p>'.$value['thainame'].'<br/><span class="">'.$value['emailaddr'].'</span><br/>'.$value['mobile'].'</p></div><div data-pic="'.$value['pic_employee'].'" data-email="'.$value['email'].'" data-name="'.$value['thainame'].'" data-mobile="'.$value['mobile'].'" class="toggle"><div class="toggle-handle"></div></div></li>';
            }
        }else{
            $html .= '<li class="table-view-cell cell-block"><div><p>Not found employee that you can add task</p></div></li>';
        }
    $html .= '</ul></div>';
    return $html;
}
function genHtmlAddAssociateEngineerComfirm($data, $ticket_sid, $tasks_sid, $case_change_engineer){
    $html = '<header class="bar bar-nav">
              <a class="icon icon-left-nav pull-left" href="#AddAssociateEngineer/'.$ticket_sid.'/'.$tasks_sid.'/left/3"></a>
            <div class="" style="width: 210px"><h1 class="title">Confirm Add Associate Engineer</h1></div>
          </header>';
    $html .= '<div class="content">';
    $html .= '<ul class="table-view">';
    // $html .= '<li class="table-view-divider cell-block"><div>From Engineer</div></li>';
    // $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$data['engineer_info']['employee_pic'].'" /><div class="media-body"><p>'.$data['engineer_info']['thainame'].'<br/>'.$data['engineer_info']['emailaddr'].'<br/>'.$data['engineer_info']['mobile'].'</p></div></li>';
    $html .= '<li class="table-view-divider cell-block"><div>New Associate Engineer</div></li>';
    if(count($case_change_engineer)>0){
        foreach ($case_change_engineer as $key => $value) {
            $html .= '<li class="table-view-cell cell-block"><img class="media-object pull-left" src="'.$value['pic'].'" /><div class="media-body"><p>'.$value['name'].'<br/><span class="change_to">'.$value['email'].'</span><br/>'.$value['mobile'].'</p></div></li>';
        }
        $html .= '<li class="table-view-cell cell-block"><div><button id="btnConfirmAddAssociateEngineer" class="btn btn-block btn-positive">Done</button></div></li>';
    }else{
        $html .= '<li class="table-view-cell cell-block"><div style="text-align:center;"><p>Please choose Engineer</p></div></li>';
    }

    $html .= '</ul>';
    $html .= '</div>';
    return $html;
}

function genHtmlEditAssociateEngineer($data, $case_change_engineer){
    $html = '<header class="bar bar-nav">
              <a class="icon icon-left-nav pull-left" href="#service_report/'.$data['ticket_sid'].'/'.$data['sid'].'/left/other"></a>
            <div class="" style="width: 210px"><h1 class="title">Remove Engineer</h1></div>
          </header>';
    $html .= "<div class='content'><div class='card'><ul class='table-view'>";
    if(isset($case_change_engineer['email']) && $case_change_engineer['email']!=""){
        $html .= "<li class='table-view-divider cell-block'><div><p>Click Done for remove Engineer</p></div></li><li class='table-view-cell'><img class='media-object pull-left' src='".$case_change_engineer['pic']."' /><div class='media-body'><p>".$case_change_engineer['name']."<br/>".$case_change_engineer['email']."<br/>".$case_change_engineer['mobile']."</p></div></li>";
        $html .= "<li class='table-view-cell cell-block'><div><button id='btnRemoveEngineerConfirm' data-email='".$case_change_engineer['email']."' class='btn btn-block btn-positive'>Done</button></div></li>";
    }else{
        $html .= "<li class='table-view-cell cell-block'>-</li>";
    }
    $html .= "</ul></div></div>";
    return $html;
}

function templateServiceReportUnsigned($data){
    $html = '<div class="content-wrapper"><section class="content">';
    $html .= '<ul class="table-view"><li class="table-view-cell cell-block"><div><p>Service Report Unsigned<span class="pull-right">
        Sort By:
        <button class="btn btn-primary sort-by" data-sort-by="appointment">Appointment</button>
        <button class="btn btn-primary sort-by" data-sort-by="end_user_contact_name_service_report">End User</button>
        <button class="btn btn-primary sort-by" data-sort-by="engineer">Engineer</button>
        </span></p></div></li></ul>';
    $html .= '<ul class="table-view">';
    $html .= '<li class="table-view-cell cell-block"><div><p><span class="pull-right">'.count($data).' Rows.</span></p></div></li>';
    foreach ($data as $key => $value) {
        $html .= '<li class="table-view-cell cell-block">
                    <img class="media-object pull-left" src="'.$value['pic'].'">
                    <div class="media-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p>'.$value['subject_service_report'].'</p>
                                <p>'.$value['end_user_company_name'].'</p>
                                <p>'.$value['no_task'].'<span data="no_ticket" class="pull-right">'.$value['no_ticket'].'</span></p>
                                <p>Appointment <span data="appointment" class="pull-right">'.$value['appointment'].'</span></p>
                                <p>'.$value['engineer_thainame'].'</p>
                            </div>
                            <div class="col-md-6">
                                <p><span class="pull-right-">'.$value['end_user_contact_name_service_report'].'</span></p>
                                <p>'.$value['end_user_email_service_report'].'</p>
                                <p>'.$value['end_user_mobile_service_report'].'<span data="" class="pull-right">'.$value['end_user_phone_service_report'].'</span></p>
                                <p><span data="" class="pull-right-">'.$value['end_user_company_name_service_report'].'</span></p>
                                <p>
                                    <span class="pull-right"><button class="resend_signed btn btn-warning btn- btn-outlined" data-tasks-sid="'.$value['sid'].'" >Resend Email To Customer Sign</button>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>';
    }
    $html .= '</ul></section></div>';
    return $html;
}
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}



$app->run();
?>
