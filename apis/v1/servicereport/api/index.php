<?php
require_once __dir__."/../../../include/TokenModel.php";
require_once __dir__.'/../../../include/CaseModel.php';
require_once __dir__."/../../../include/ServiceReportModel.php";
require_once __dir__."/../../../include/db_handler.php";
require_once __dir__."/../../../include/SendMail.php";
// require_once "../../include/UserModel.php";
$index = function () use ($app) {
    try{
        parse_str($app->request()->getBody(),$request);

        $data = array();

        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();
        $serviceModel = new ServiceReportModel();
        $db = new DbHandler();

        $response = array();
        $info = $tokenObj->info($request['token'], $request['email']);
        if(isset($info['role_sid'])){
            $permission = $tokenObj->permissionTicket($info['role_sid']);
            $data = $serviceModel->servicereports($permission, $request);
            $caseType = $caseModel->ticketType($permission, $request, $request['email'], $request['search']);

            $dataFindNoti['search'] = $request['search'];
            $dataFindNoti['dataStart'] = 0;
            $dataFindNoti['dataLimit'] = 5;

            $dataNoti = $db->notifications($request['email'],0, $dataFindNoti);
            $response['dataNoti'] = $dataNoti;


            $dataFindCase['search'] = $request['search'];
            $dataFindCase['status'] = '1';
            $dataFindCase['dataStart'] = '0';
            $dataFindCase['dataLimit'] = '5';
            $response['dataCase'] = $caseModel->tickets($permission, $dataFindCase, $request['email'], $dataFindCase['search']);

            if(isset($request['registrationId']) && strlen($request['registrationId'])>10){
                $tokenObj->updateRegisterOnlyReact($request['email'], $request['registrationId']);
            }

        }else{
            $res = array();
        }


        $response['request'] = ($request);
        $response['data'] = $data;

        if($caseType){
            $response['type'] = $caseType;
        }
        echoRespnse(200, $response);
    }catch(Exception $e){
        echoRespnse(500, $response['message']="error");
    }
};

$detail = function () use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $data = array();
        // print_r($request);
        $tokenObj = new TokenModel();
        // $caseModel = new CaseModel();
        $serviceModel = new ServiceReportModel();
        $info = $tokenObj->info($request['authen']['token'], $request['authen']['email']);
        if(isset($info['role_sid'])){
            $data = $serviceModel->serviceReportDetail($request);
        }else{
            $data = array();
        }
        $response = array();
        $response['request'] = ($request);
        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};
$create = function () use ($app){

};
$edit = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $data = array();
        // print_r($request);
        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();
        $serviceModel = new ServiceReportModel();
        $info = $tokenObj->info($request['authen']['token'], $request['authen']['email']);
        if(isset($info['role_sid'])){
            // $data = array('info'=>$info);
            $serviceModel->editServiceReport($request['authen'], $request['data']);

            $serviceModel->deletedEngieerFromTaskLogQuery($request['data'], $request['authen']);
            $serviceModel->addEngineerToTaskLogQuery($request['data'], $request['authen']);
            $serviceModel->addAssoEngineerToTaskLog($request['data'], $request['authen']);
            $serviceModel->deleteAssoEngineer($request['data'], $request['authen']);
            $serviceModel->addEngineerToTasksEngineer($request['data'], $request['authen']);
            $serviceModel->addAssoEngineer($request['data'], $request['authen']);

        }else{
            $data = array();
        }
        $response = array();
        $response['request'] = ($request);
        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$editContactAfterClose = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $data = array();
        // print_r($request);
        $tokenObj = new TokenModel();
        $caseModel = new CaseModel();
        $serviceModel = new ServiceReportModel();
        $info = $tokenObj->info($request['authen']['token'], $request['authen']['email']);
        if(isset($info['role_sid'])){
            $serviceModel->editContactAfterClose($request['data'], $request['tasks_sid']);
        }else{
            $data = array();
        }
        $response = array();
        $response['request'] = ($request);
        $response['data'] = $data;
        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$timestampSr = function() use ($app){
    $db = new DbHandler();
    $sendMail = new sendMail();
    verifyRequiredParams(array('email', 'task_status','taxi_fare'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $task_status = $app->request->post('task_status');
    $tasks_sid = $app->request->post('tasks_sid');

    $taxi_fare = $app->request->post('taxi_fare');
    $taxi_fare_stang = $app->request->post('taxi_fare_stang');

    $lat = $app->request->post('lat');
    $lng = $app->request->post('lng');

    $follow_up = 0;
    if($app->request->post('follow_up') && $app->request->post('follow_up_date')){
        $follow_up_date = $app->request->post('follow_up_date');
        $follow_up_time = $app->request->post('follow_up_time');

        $follow_up = $app->request->post('follow_up');
        if(($follow_up!="false") && $follow_up_date!="0000-00-00 00:00:00"){
            $follow_up = 1;

            $case = new CaseModel();
            $case->updateTicketStatusFollowup($tasks_sid, $follow_up_date, $follow_up_time, $follow_up);
        }

    }

    if($app->request->post('signature')){
        $signature = $app->request->post('signature');
    }else{
        $signature = '';
    }
    if($task_status!="-1"){
        if($task_status=="0"){
            $task_status+=100;
        }
        $new_status = $task_status+100;
    }else{
        $new_status = "-1";
    }
    if($new_status==500 && $signature==''){
        //fopen(__dir__."/../../../../api/sendmail/sendEmailAfterClosedToCustomer/".$tasks_sid."?email=".$email, "r");
         $sendMail->sendEmailAfterClosedToCustomer($tasks_sid, $email);
    }
    $response = $db->addNewStatusTaskLog($email, $tasks_sid, $new_status, $email, $taxi_fare, $lat, $lng, $follow_up, $signature);
    $response["error"] = false;
    $response['request'] = array($task_status,$tasks_sid,$taxi_fare,$taxi_fare_stang,$lat,$lng);
    $response['current_status'] = $new_status;

    echoRespnse(200, $response);
};

$input_action =  function() use ($app){
    verifyRequiredParams(array('task_sid','email'));

    // reading post params
    $task_sid = $app->request->post('task_sid');
    $problem = $app->request->post('problem');
    $solution = $app->request->post('solution');
    $recommend = $app->request->post('recommend');
    $email = $app->request->post('email');
    $pm_service = $app->request->post('pm_service');
    $serial_no = $app->request->post('serial_no');
    $ticket_sid = $app->request->post('ticket_sid');

    $caseModel = new CaseModel();
    $caseModel->setSerialNumber($serial_no);
    $caseModel->setTicketSid($ticket_sid);
    $caseModel->updateSerialNumber();
    $model = new DbHandler();
    $param = array('task_sid'=>$task_sid,'problem'=>$problem,'solution'=>$solution,'recommend'=>$recommend, 'create_by'=>$email, 'pm_service'=>$pm_service);
    $result = $model->insertInputAction($param, $email);

    $response = array();
    $response['error'] = false;
    $response['result'] = $result;

    // echo json response
    echoRespnse(200, $response);
};

$notification = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $db = new DbHandler();
        $data = $db->notifications($request['email'],0, $request);

        $response = array();
        $response['error'] = false;
        $response['data'] = $data;

        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$delete = function() use ($app){
    try{
        parse_str($app->request()->getBody(),$request);

        $db = new ServiceReportModel();
        $caseModel = new CaseModel();
        $data = $db->deleteService($request);
        $caseModel->updateTicketNumberSr($request['ticket_sid']);
        $response = array();
        $response['error'] = false;
        $response['data'] = $data;

        echoRespnse(200, $response);
    }catch(Exception $e){
        echo $e;
        echoRespnse(500, $response['message']=$e);
    }
};

$expressReport = function($name,$c1,$c2) use ($app){
    echo 'หางานของคุณ'.$name."<br/>ตั้งแต่ 2017-04-01 00:00:00 จนถึงปัจจุบัน ที่ Contract ไม่ขึ้นต้นด้วย ".$c1." และ ".$c2;


        $db = new ServiceReportModel();
        $data = $db->findExpressReport($name, $c1, $c2);

        if(count($data)>0){
            echo "<table>";
            echo "<tr><td>#</td><td>Sr No.</td><td>HD</td><td>Subject HD</td><td>End User</td><td>Contract No</td></tr>";
            foreach ($data as $key => $value) {
                echo "<tr><td>".($key+1)."</td><td>".$value['no_task']."</td><td>".$value['no_ticket']."</td><td>".$value['subject']."</td><td>".$value['end_user']."</td><td>".$value['contract_no']."</td></tr>";
            }
            echo "</table>";
        }else{
            echo "<div>ไม่พบข้อมูล</div>";
        }

};

$ServiceReportEditAddress = function() use ($app){
  try{
      parse_str($app->request()->getBody(),$request);

      $db = new ServiceReportModel();
      $message = $db->serviceReportEditAddress($request);
      $response = array();
      $response['error'] = false;
      $response['request'] = $request;
      $response['message'] = $message;

      echoRespnse(200, $response);
  }catch(Exception $e){
      echo $e;
      echoRespnse(500, $response['message']=$e);
  }
};
