<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../../include/ProjectModifyModel.php';
require '../.././libs/Slim/Slim.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
\Slim\Slim::registerAutoloader();
 
$app = new \Slim\Slim();
$app->post('/changestaff', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $project_owner_sid = $app->request->post('project_owner_sid');
    $project_sid = $app->request->post('project_sid');
    $new_staff_email = $app->request->post('new_staff_email');

    $obj = new ProjectModifyModel();
    $data = $obj->addStaffAgain($email, $project_owner_sid, $project_sid, $new_staff_email);
    $data = $obj->deleteStaff($email, $project_owner_sid);
    $response["error"] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

$app->post('/addStaff', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $project_sid = $app->request->post('project_sid');
    $new_staff_email = $app->request->post('new_staff_email');

    $obj = new ProjectModifyModel();
    $data = $obj->addStaff($email, $project_sid, $new_staff_email);
    $response["error"] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

$app->post('/deleteStaff', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');

    $project_sid = $app->request->post('project_sid');
    $del_staff_email = $app->request->post('staff_email');

    $obj = new ProjectModifyModel();
    $data = $obj->deleteStaffReact($email, $del_staff_email, $project_sid);
    $response["error"] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});


$app->post('/addProjectContact', function() use ($app){

	$project_sid = $app->request->post('project_sid');
	$email = $app->request->post('email');
	$token = $app->request->post('token');
	$end_user_name =$app->request->post('end_user_name');
	$end_user_email =$app->request->post('end_user_email');
	$end_user_mobile =$app->request->post('end_user_mobile');
	$end_user_phone =$app->request->post('end_user_phone');
	$end_user_company =$app->request->post('end_user_company');

    $obj = new ProjectModifyModel();
    $obj->addProjectContact($project_sid,$email, $token,$end_user_name, $end_user_email,$end_user_mobile,$end_user_phone,$end_user_company);

	$response["error"] = false;
    $response['data'] = array(
    	'project_sid'=>$project_sid,
    	'email'=>$email,
    	'token'=>$token,
    	'end_user_name'=>$end_user_name,
    	'end_user_email'=>$end_user_email,
    	);

    echoRespnse(200, $response);
});

$app->post('/updateProjectContact',function() use ($app){
    $project_contact_sid = $app->request->post('project_contact_sid');
    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $end_user_name = $app->request->post('end_user_name');
    $end_user_email = $app->request->post('end_user_email');
    $end_user_mobile = $app->request->post('end_user_mobile');
    $end_user_phone = $app->request->post('end_user_phone');
    $end_user_company = $app->request->post('end_user_company');
   
   $obj = new ProjectModifyModel();
   $obj ->updateProjectContact($project_contact_sid,$end_user_name,$end_user_email,$end_user_mobile,$end_user_phone,$end_user_company,$email,$token);

   $response["error"] = false;
   // $response['data'] = array(
   //      'project_contact_sid' => $project_contact_sid,
   //      'end_user_name' => $end_user_name,
   //      'end_user_email' => $end_user_email,
   //      'end_user_mobile' => $end_user_mobile,
   //      'end_user_phone' => $end_user_phone,
   //      'end_user_company' => $end_user_company,
   //      'email' => $email,
   //      'token' => $token,
   //      );
   echoRespnse(200, $response);
});



$app->post('/changemanday', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $project_owner_sid = $app->request->post('project_owner_sid');
    $man_days_new = $app->request->post('man_days_new');

    $obj = new ProjectModifyModel();
    $data = $obj->changeManday($project_owner_sid, $email, $man_days_new);
    $response["error"] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

$app->post('/deleteproject', function() use ($app){
    verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $project_sid = $app->request->post('project_sid');

    $obj = new ProjectModifyModel();
    $data = $obj->deleteProject($project_sid, $email);
    $response["error"] = false;
    $response['data'] = $data;

    echoRespnse(200, $response);
});

$app->post('/closeProject', function() use ($app){
     verifyRequiredParams(array('email'));

    $email = $app->request->post('email');
    $token = $app->request->post('token');
    $project_sid = $app->request->post('project_sid');
    $remark = $app->request->post('remark_close_project');
    $obj = new ProjectModifyModel();
    $message = $data = $obj->closeProject($project_sid, $email, $remark);
    $response["error"] = false;
    $response['data'] = $data;
    $response['message'] = $message;    
    echoRespnse(200, $response);
});

$app->post('/plan_created', function() use ($app){
    $response = array();
    $response['req']['data'] = $app->request->post('data');
    $response['req']['email'] = $app->request->post('email');
    $data = "";
    $obj = new ProjectModel();
    $data = $obj->planCreate($response['req']['data'], $response['req']['email']);
    $response["error"] = false;
    $response["data"] = $data;
    echoRespnse(200, $response);
});
$app->post('/lists/:type',function($type) use ($app){
    // verifyRequiredParams(array('day'));
    $search = $app->request->post('search');
    $email = $app->request->post('email');
    $owner = $app->request->post('owner');
    $limit = $app->request->post('limit');
    $obj = new ProjectModel();

    $data = $obj->lists($owner, $type,$search, $email,$limit);
    $response = array();

    $response["error"] = false;
    $response['data'] = $data;

    // echo json response
    echoRespnse(200, $response);
});

/* * *
 * Updating user
 *  we use this url to update user's gcm registration id
 */
// $app->post('/user/:id', function($user_id) use ($app) {
//     global $app;
 
//     verifyRequiredParams(array('gcm_registration_id'));
 
//     $gcm_registration_id = $app->request->put('gcm_registration_id');
 
//     $db = new DbHandler();
//     $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
//     echoRespnse(200, $response);
// });
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>