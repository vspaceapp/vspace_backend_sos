<?php
ob_start();

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require_once("../nusoap-0.9.5/lib/nusoap.php");
header("Content-Type: text/html; charset=utf-8");

$server = new soap_server();
$server->configureWSDL("gable_worklog", "urn:gable_worklog", "http://vspace.in.th/soap/gable_worklog/index.php");
$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;

$server->register("gable_worklog", 
	array(
		"case_id"=>"xsd:string",
		"Detail_Description"=>"xsd:string",
		"Description"=>"xsd:string",
		"Submitter_Email"=>"xsd:string",
		"Client_Type"=>"xsd:string"
		),
    array("return" => "xsd:string"),
    "urn:gable_worklog",
    "urn:gable_worklog#getTest",
    "rpc",
    "encoded",
    "gable_worklog");

if ( !isset( $HTTP_RAW_POST_DATA ) ) $HTTP_RAW_POST_DATA =file_get_contents( 'php://input' );
$server->service($HTTP_RAW_POST_DATA);

// $server->service($HTTP_RAW_POST_DATA);

function gable_worklog(
	$case_id,
	$Detail_Description,
	$Description,
	$Submitter_Email,
	$Client_Type
) {	

	require_once "/home/vlogic/public_html/apis/core/config.php";
	$servername = "127.0.0.1";
	$username = DB_USERNAME;
	$password = DB_PASSWORD;
	$dbname = DB_NAME;

	// $servername = "127.0.0.1";
	// $username = "vspace";
	// $password = "v@dm1n";
	// $dbname = "vspace_cases";

	try {
	    $conn = new PDO("mysql:host=$servername;port=5006;dbname=$dbname;charset=utf8", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $conn->beginTransaction();
	    $stmt = $conn->prepare("INSERT INTO gable_worklog 
	    	(case_id, detail_description, description,submitter_email, create_datetime, client_type) 
	    VALUES (:case_id, :detail_description,:description,:submitter_email, NOW(), :client_type) ");

	    $stmt->bindParam(':case_id', $case_id);
	    $stmt->bindParam(':detail_description', $Detail_Description);
	    $stmt->bindParam(':description', $Description);
	    $stmt->bindParam(':submitter_email',$Submitter_Email);
	    $stmt->bindParam(':client_type', $Client_Type);
	    
	    $stmt->execute();
		$conn->commit();

		if($Client_Type=="9"){
			require_once "/home/vlogic/public_html/apis/include/CaseModel.php";
			$caseModel = new CaseModel();

			$ticket_sid = $caseModel->getTicketSidFromRemedyInc($case_id);
			$caseModel->initailUpdateSlaAndWorklog($ticket_sid, $Detail_Description, "8", $Submitter_Email,"","","","1", 2);

		}


	    $message = "Updated successfully";
	    // try{

	    // }catch(PDOException $e){
	    // 	$e->getMessage();
	    // }
	    return $message;
	}
	catch(PDOException $e){
	    return "Error: " . $e->getMessage();
	}
	$conn = null;
}
?>