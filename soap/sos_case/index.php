<?php
$time_pre_soap = microtime(true);
require_once __dir__."/../../apis/core/config.php";
error_reporting(0);
ini_set('display_errors', 0);

require_once __dir__."/../../apis/core/constant.php";
require_once("../nusoap-0.9.5/lib/nusoap.php");
require_once ("../../apis/include/CaseModel.php");
require_once ("../../apis/core/config.php");
require_once __dir__."/../../apis/include/SlaModel.php";
header("Content-Type: text/html; charset=utf-8");

$server = new soap_server();
$server->configureWSDL("create_case", "urn:create_case", SOAP_WS_URL_UPDATE_TICKET);
//$server->configureWSDL("create_case", "urn:create_case", "http://localhost/vspace_backend_sos/soap/sos_case/index.php");

$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;

$server->register("create",
    array(
        "ticket_id"=>"xsd:string",
        "company"=>"xsd:string",
        "customer"=>"xsd:string",
        "contract"=>"xsd:string",
        "summary"=>"xsd:string",
        "notes"=>"xsd:string",
        "reported_source"=>"xsd:string",
        "impact"=>"xsd:string",
        "urgency"=>"xsd:string",
        "priority"=>"xsd:string",
        "incident_type"=>"xsd:string",
        "assigned_group"=>"xsd:string",
        "assignee"=>"xsd:string",
        "vendor_group"=>"xsd:string",
        "vendor_ticket_number"=>"xsd:string",
        "status"=>"xsd:string",
        "status_reason"=>"xsd:string",
        "resolution"=>"xsd:string",
        "employee_contract_id"=>"xsd:string",
        "employee_first_name"=>"xsd:string",
        "employee_last_name"=>"xsd:string",
        "employee_phone_number"=>"xsd:string",
        "employee_mobile_number"=>"xsd:string",
        "employee_email"=>"xsd:string",
        "department"=>"xsd:string",
        "organization_title"=>"xsd:string",
        "functional_title"=>"xsd:string",
        "asset_id"=>"xsd:string",
        "incident_location"=>"xsd:string",
        "incident_address"=>"xsd:string",
        "location_id"=>"xsd:string",
        "location_type"=>"xsd:string",
        "location_area"=>"xsd:string",
        "branch_code"=>"xsd:string",
        "grade"=>"xsd:string",
        "vendor"=>"xsd:string",
        "operational_categorization_tier_1"=>"xsd:string",
        "operational_categorization_tier_2"=>"xsd:string",
        "operational_categorization_tier_3"=>"xsd:string",
        "product_categorization_tier_1"=>"xsd:string",
        "product_categorization_tier_2"=>"xsd:string",
        "product_categorization_tier_3"=>"xsd:string",
        "product_categorization_product_name"=>"xsd:string",
        "product_categorization_model_version"=>"xsd:string",
        "product_categorization_manufacturer"=>"xsd:string",
        "product_categorization_application_tier"=>"xsd:string",
        "product_domain"=>"xsd:string",
        "product_support_period"=>"xsd:string",
        "target_sla"=>"xsd:string",
        "reported_date"=>"xsd:string",
        "responded_date"=>"xsd:string",
        "submitter"=>"xsd:string",
        "submit_date"=>"xsd:string",
        "owner_group"=>"xsd:string",
        "last_work_log"=>"xsd:string"
        ),
    array("return" => "xsd:string"),
    "urn:create_hd",
    "urn:create_hd#getTest",
    "rpc",
    "encoded",
    "Just a Create");

if (!isset( $HTTP_RAW_POST_DATA )) {
    $HTTP_RAW_POST_DATA =file_get_contents( 'php://input' );
}
$server->service($HTTP_RAW_POST_DATA);

// $server->service($HTTP_RAW_POST_DATA);

// require_once "../../crontab/Sla.php";
// $file = fopen("http://vspace.com/crontab/Sla.php","r");

function create(
    $ticket_id,
    $company,
    $customer,
    $contract,
    $summary,
    $notes,
    $reported_source,
    $impact,
    $urgency,
    $priority,
    $incident_type,
    $assigned_group,
    $assignee,
    $vendor_group,
    $vendor_ticket_number,
    $status,
    $status_reason,
    $resolution,
    $employee_contract_id,
    $employee_first_name,
    $employee_last_name,
    $employee_phone_number,
    $employee_mobile_number,
    $employee_email,
    $department,
    $organization_title,
    $functional_title,
    $asset_id,
    $incident_location,
    $incident_address,
    $location_id,
    $location_type,
    $location_area,
    $branch_code,
    $grade,
    $vendor,
    $operational_categorization_tier_1,
    $operational_categorization_tier_2,
    $operational_categorization_tier_3,
    $product_categorization_tier_1,
    $product_categorization_tier_2,
    $product_categorization_tier_3,
    $product_categorization_product_name,
    $product_categorization_model_version,
    $product_categorization_manufacturer,
    $product_categorization_application_tier,
    $product_domain,
    $product_support_period,
    $target_sla,
    $reported_date,
    $responded_date,
    $submitter,
    $submit_date,
    $owner_group,
    $last_work_log
) {
    try {
        //$conn = new PDO("mysql:host=".DB_HOST.";port=5006;dbname=".DB_NAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
        //$conn = new PDO("mysql:host=".DB_HOST.";port=3306;dbname=".DB_NAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
        $conn = new PDO("mysql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $slaModel = new SlaModel();
        $severity = $slaModel->genPriority($urgency, $impact);

// insert ticket log
        $sql = "INSERT INTO ticket_sos (
			ticket_id,
			create_datetime,
			company,
			customer,
			contract,
			summary,
			notes,
			reported_source,
			impact,
			urgency,
			priority,
			incident_type,
			assigned_group,
			assignee,
			vendor_group,
			vendor_ticket_number,
			status,
			status_reason,
			resolution,
			employee_contract_id,
			employee_first_name,
			employee_last_name,
			employee_phone_number,
			employee_mobile_number,
			employee_email,
			department,
			organization_title,
			functional_title,
			asset_id,
			incident_location,
			incident_address,
			location_id,
			location_type,
			location_area,
			branch_code,
			grade,
			vendor,
			operational_categorization_tier_1,
			operational_categorization_tier_2,
			operational_categorization_tier_3,
			product_categorization_tier_1,
			product_categorization_tier_2,
			product_categorization_tier_3,
			product_categorization_product_name,
			product_categorization_model_version,
			product_categorization_manufacturer,
			product_categorization_application_tier,
			product_domain,
			product_support_period,
			target_sla,
			reported_date,
			responded_date,
			submitter,
			submit_date,
			owner_group,
			last_work_log,
			severity)
		VALUES (
			:ticket_id,
			NOW(),
			:company,
			:customer,
			:contract,
			:summary,
			:notes,
			:reported_source,
			:impact,
			:urgency,
			:priority,
			:incident_type,
			:assigned_group,
			:assignee,
			:vendor_group,
			:vendor_ticket_number,
			:status,
			:status_reason,
			:resolution,
			:employee_contract_id,
			:employee_first_name,
			:employee_last_name,
			:employee_phone_number,
			:employee_mobile_number,
			:employee_email,
			:department,
			:organization_title,
			:functional_title,
			:asset_id,
			:incident_location,
			:incident_address,
			:location_id,
			:location_type,
			:location_area,
			:branch_code,
			:grade,
			:vendor,
			:operational_categorization_tier_1,
			:operational_categorization_tier_2,
			:operational_categorization_tier_3,
			:product_categorization_tier_1,
			:product_categorization_tier_2,
			:product_categorization_tier_3,
			:product_categorization_product_name,
			:product_categorization_model_version,
			:product_categorization_manufacturer,
			:product_categorization_application_tier,
			:product_domain,
			:product_support_period,
			:target_sla,
			:reported_date,
			:responded_date,
			:submitter,
			:submit_date,
			:owner_group,
			:last_work_log,
		  :severity);";
        $q = $conn->prepare($sql);
        $q->execute(array(
          ':ticket_id'=>$ticket_id,
          ':company'=>$company,
          ':customer'=>$customer,
          ':contract'=>$contract,
          ':summary'=>$summary,
          ':notes'=>$notes,
          ':reported_source'=>$reported_source,
          ':impact'=>$impact,
          ':urgency'=>$urgency,
          ':priority'=>$priority,
          ':incident_type'=>$incident_type,
          ':assigned_group'=>$assigned_group,
          ':assignee'=>$assignee,
          ':vendor_group'=>$vendor_group,
          ':vendor_ticket_number'=>$vendor_ticket_number,
          ':status'=>$status,
          ':status_reason'=>$status_reason,
          ':resolution'=>$resolution,
          ':employee_contract_id'=>$employee_contract_id,
          ':employee_first_name'=>$employee_first_name,
          ':employee_last_name'=>$employee_last_name,
          ':employee_phone_number'=>$employee_phone_number,
          ':employee_mobile_number'=>$employee_mobile_number,
          ':employee_email'=>$employee_email,
          ':department'=>$department,
          ':organization_title'=>$organization_title,
          ':functional_title'=>$functional_title,
          ':asset_id'=>$asset_id,
          ':incident_location'=>$incident_location,
          ':incident_address'=>$incident_address,
          ':location_id'=>$location_id,
          ':location_type'=>$location_type,
          ':location_area'=>$location_area,
          ':branch_code'=>$branch_code,
          ':grade'=>$grade,
          ':vendor'=>$vendor,
          ':operational_categorization_tier_1'=>$operational_categorization_tier_1,
          ':operational_categorization_tier_2'=>$operational_categorization_tier_2,
          ':operational_categorization_tier_3'=>$operational_categorization_tier_3,
          ':product_categorization_tier_1'=>$product_categorization_tier_1,
          ':product_categorization_tier_2'=>$product_categorization_tier_2,
          ':product_categorization_tier_3'=>$product_categorization_tier_3,
          ':product_categorization_product_name'=>$product_categorization_product_name,
          ':product_categorization_model_version'=>$product_categorization_model_version,
          ':product_categorization_manufacturer'=>$product_categorization_manufacturer,
          ':product_categorization_application_tier'=>$product_categorization_application_tier,
          ':product_domain'=>$product_domain,
          ':product_support_period'=>$product_support_period,
          ':target_sla'=>$target_sla,
          ':reported_date'=>$reported_date,
          ':responded_date'=>$responded_date,
          ':submitter'=>$submitter,
          ':submit_date'=>$submit_date,
          ':owner_group'=>$owner_group,
          ':last_work_log'=>$last_work_log,
          ':severity'=>$severity
        ));

        //
       // // UnMap  variable table ticket , ticket sos
          // $no_ticket = "Unmap_no_ticket";
          // $no_project_ticket  = "Unmap_no_project_ticket ";
          // $first_owner  = "Unmap_first_owner ";
          // $update_datetime  = "Unmap_update_datetime ";
          // $project_sid  = "Unmap_project_sid ";
          // $project_owner_sid  = "Unmap_project_owner_sid ";
          // $receive_mail_datetime = "Unmap_receive_mail_datetime";
          // $refer_remedy_create_time = "Unmap_refer_remedy_create_time";
          // $due_datetime = "Unmap_due_datetime";
          // $prime_contract = "Unmap_prime_contract";
          // $project_name = "Unmap_project_name";
          // $serial_no = "Unmap_serial_no";
          // $requester_company_name = "Unmap_requester_company_name";
          // $requester_email = "Unmap_requester_email";
          // $end_user_site = "Unmap_end_user_site";
          // $case_type = "Unmap_case_type";
          // $case_company = "Unmap_case_company";
          // $source = "Unmap_source";
          // $note = "Unmap_note";
          // $sla_remedy = "Unmap_sla_remedy";
          // $update_sla_remedy_datetime = "Unmap_update_sla_remedy_datetime";
          // $updated_auto_closed_datetime = "Unmap_updated_auto_closed_datetime";
          // $check_standby = "Unmap_check_standby";
          // $man_days = "Unmap_man_days";
          // $site_area = "Unmap_site_area";
          // $send_mail_open_ticket = "Unmap_send_mail_open_ticket";
          // $send_email_open_ticket_datetime = "Unmap_send_email_open_ticket_datetime";
          // $taxi_fare_total = "Unmap_taxi_fare_total";
          // $update_taxi_fare = "Unmap_update_taxi_fare";
          // $number_sr = "Unmap_number_sr";
          // $updated_number_sr = "Unmap_updated_number_sr";
       //

// map field
//
  // set params
        $assigneeUpdate = getAssigneeUpdate($ticket_id, $conn);
      $no_project_ticket = ""; //not use
      $refer_remedy_hd = $ticket_id;

      $team = $assigneeUpdate['team'];
      $owner = $assigneeUpdate['email'];
      $first_owner = ""; //not use
      $status = getStatusIdFromName($status, $conn);
      $update_datetime = "";
      $project_sid = ""; // not use
      $project_owner_sid = ""; // not use
      $via = "remedy kbank";
      $create_by = $submitter;
      $receive_mail_datetime = "";
      $refer_remedy_assigned_time = $submit_date;
      $refer_remedy_create_time = "";
      $refer_remedy_report_by = $submitter;
      $subject = $summary;
      $due_datetime = "";
      $description = $notes;
      //$contract_no = $contract; // not use?
        $contract_no="";
      $prime_contract = ""; //not use
      $project_name = ""; //not use
      $serial_no = $asset_id;


      $requester_full_name = "";
      $requester_company_name  = "";
      $requester_phone = "";
      $requester_mobile = "";
      $requester_email = "";

      $end_user_company_name = $company;
      $end_user  = $company;
      $end_user_site = $incident_address;
      $end_user_contact_name =  $employee_first_name." ".$employee_last_name;
      $end_user_phone = $employee_phone_number;
      $end_user_mobile = $employee_mobile_number;
      //$employee_email = $employee_email;
      $case_type = "Incident";
      $case_company = $company;
      $source = $reported_source;
      $note = $notes;
      $urgency = $urgency;
      $updated_by = $submitter;
      $sla_remedy = "";
      $update_sla_remedy_datetime = "";
      $updated_auto_closed_datetime = "";
      $check_standby = ""; //not use
      $man_days = "";
      $site_area = "";
      $send_mail_open_ticket = "";
      $send_email_open_ticket_datetime = "";
      $taxi_fare_total = ""; // not use
      $update_taxi_fare = ""; //not use
      $number_sr = "";
      $updated_number_sr = "";
      $department = $department;
      $refer_remedy_employee_contract_id  = $employee_contract_id;

    if(!isset($ticket_id)){
        return "Required field ticket_id";
    }else if(strlen($ticket_id)<15){
        return "Ticket number must contain 15 characters.";
    }
    else if(!isset($status) || $status ==''){
        return "Required field status";
    }else if($status != 1){ // 1 Assigned
        return "Status must be value 'Assigned'";
    }
    else{
        $r = insertOrUpdateTicket(
            $no_project_ticket,
            $refer_remedy_hd,
            $owner,
            $first_owner,
            $status,
            $update_datetime,
            $project_sid,
            $project_owner_sid,
            $via,
            $create_by,
            $receive_mail_datetime,
            $refer_remedy_assigned_time,
            $refer_remedy_create_time,
            $refer_remedy_report_by,
            $subject,
            $due_datetime,
            $description,
            $contract_no,
            $prime_contract,
            $project_name,
            $serial_no,
            $team,
            $requester_full_name,
            $requester_company_name,
            $requester_phone,
            $requester_mobile,
            $requester_email,
            $end_user_company_name,
            $end_user,
            $end_user_site,
            $end_user_contact_name,
            $end_user_phone,
            $end_user_mobile,
            $employee_email,
            $case_type,
            $case_company,
            $source,
            $note,
            $urgency,
            $updated_by,
            $sla_remedy,
            $update_sla_remedy_datetime,
            $updated_auto_closed_datetime,
            $check_standby,
            $man_days,
            $site_area,
            $send_mail_open_ticket,
            $send_email_open_ticket_datetime,
            $taxi_fare_total,
            $update_taxi_fare,
            $number_sr,
            $updated_number_sr,
            $department,
            $organization_title,
            $functional_title,
            $asset_id,
            $incident_location,
            $incident_address,
            $location_id,
            $location_type,
            $location_area,
            $branch_code,
            $grade,
            $vendor,
            $operational_categorization_tier_1,
            $operational_categorization_tier_2,
            $operational_categorization_tier_3,
            $product_categorization_tier_1,
            $product_categorization_tier_2,
            $product_categorization_tier_3,
            $product_categorization_product_name,
            $product_categorization_model_version,
            $product_categorization_manufacturer,
            $product_categorization_application_tier,
            $product_domain,
            $product_support_period,
            $target_sla,
            $reported_date,
            $responded_date,
            $submit_date,
            $owner_group,
            $reported_source,
            $incident_type,
            $vendor_group,
            $vendor_ticket_number,
            $status_reason,
            $resolution,
            $last_work_log,
            $impact,
            $severity,
            $refer_remedy_employee_contract_id, /// end field column
            $conn // db conect
        );
        return $r;
    }

    } catch (PDOException $e) {
        return json_encode(array('error'=>"true", 'message'=>"Something went wrong" ));
    }
}


function insertOrUpdateTicket(
    $no_project_ticket,
    $refer_remedy_hd,
    $owner,
    $first_owner,
    $status,
    $update_datetime,
    $project_sid,
    $project_owner_sid,
    $via,
    $create_by,
    $receive_mail_datetime,
    $refer_remedy_assigned_time,
    $refer_remedy_create_time,
    $refer_remedy_report_by,
    $subject,
    $due_datetime,
    $description,
    $contract_no,
    $prime_contract,
    $project_name,
    $serial_no,
    $team,
    $requester_full_name,
    $requester_company_name,
    $requester_phone,
    $requester_mobile,
    $requester_email,
    $end_user_company_name,
    $end_user,
    $end_user_site,
    $end_user_contact_name,
    $end_user_phone,
    $end_user_mobile,
    $employee_email,
    $case_type,
    $case_company,
    $source,
    $note,
    $urgency,
    $updated_by,
    $sla_remedy,
    $update_sla_remedy_datetime,
    $updated_auto_closed_datetime,
    $check_standby,
    $man_days,
    $site_area,
    $send_mail_open_ticket,
    $send_email_open_ticket_datetime,
    $taxi_fare_total,
    $update_taxi_fare,
    $number_sr,
    $updated_number_sr,
    $department,
    $organization_title,
    $functional_title,
    $asset_id,
    $incident_location,
    $incident_address,
    $location_id,
    $location_type,
    $location_area,
    $branch_code,
    $grade,
    $vendor,
    $operational_categorization_tier_1,
    $operational_categorization_tier_2,
    $operational_categorization_tier_3,
    $product_categorization_tier_1,
    $product_categorization_tier_2,
    $product_categorization_tier_3,
    $product_categorization_product_name,
    $product_categorization_model_version,
    $product_categorization_manufacturer,
    $product_categorization_application_tier,
    $product_domain,
    $product_support_period,
    $target_sla,
    $reported_date,
    $responded_date,
    $submit_date,
    $owner_group,
    $reported_source,
    $incident_type,
    $vendor_group,
    $vendor_ticket_number,
    $status_reason,
    $resolution,
  	$last_work_log,
  	$impact,
  	$severity,
    $refer_remedy_employee_contract_id, // end field column
    $conn // db connect
) {
    try {
        $caseModel = new CaseModel("", $refer_remedy_hd,"");
        if($caseModel->getStatusTicket() == 6){
            return "Unable to update closed ticket.";
        }
        $conn->beginTransaction();
        $no_ticket = $caseModel->genNumberTicket($conn);
        $rowsBeforeInsert = getRowTicket($conn);
        $sql =
        "INSERT INTO
			`ticket`(
				   `no_ticket`
				 , `no_project_ticket`
				 , `refer_remedy_hd`
				 , `owner`
				 , `first_owner`
				 , `status`
				 , `create_datetime`
				 , `update_datetime`
				 , `project_sid`
				 , `project_owner_sid`
				 , `via`
				 , `create_by`
				 , `receive_mail_datetime`
				 , `refer_remedy_assigned_time`
				 , `refer_remedy_create_time`
				 , `refer_remedy_report_by`
				 , `subject`
				 , `due_datetime`
				 , `description`
				 , `contract_no`
				 , `prime_contract`
				 , `project_name`
				 , `serial_no`
				 , `team`
				 , `requester_full_name`
				 , `requester_company_name`
				 , `requester_phone`
				 , `requester_mobile`
				 , `requester_email`
				 , `end_user_company_name`
				 , `end_user`
				 , `end_user_site`
				 , `end_user_contact_name`
				 , `end_user_phone`
				 , `end_user_mobile`
				 , `end_user_email`
				 , `case_type`
				 , `case_company`
				 , `source`
				 , `note`
				 , `urgency`
				 , `updated_by`
				 , `sla_remedy`
				 , `update_sla_remedy_datetime`
				 , `updated_auto_closed_datetime`
				 , `check_standby`
				 , `man_days`
				 , `site_area`
				 , `send_mail_open_ticket`
				 , `send_email_open_ticket_datetime`
				 , `taxi_fare_total`
				 , `update_taxi_fare`
				 , `number_sr`
				 , `updated_number_sr`
				 , `department`
				 , `organization_title`
				 , `functional_title`
				 , `asset_id`
				 , `incident_location`
				 , `incident_address`
				 , `location_id`
				 , `location_type`
				 , `location_area`
				 , `branch_code`
				 , `grade`
				 , `vendor`
				 , `operational_categorization_tier_1`
				 , `operational_categorization_tier_2`
				 , `operational_categorization_tier_3`
				 , `product_categorization_tier_1`
				 , `product_categorization_tier_2`
				 , `product_categorization_tier_3`
				 , `product_categorization_product_name`
				 , `product_categorization_model_version`
				 , `product_categorization_manufacturer`
				 , `product_categorization_application_tier`
				 , `product_domain`
				 , `product_support_period`
				 , `target_sla`
				 , `reported_date`
				 , `responded_date`
				 , `submit_date`
				 , `owner_group`
				 , `reported_source`
				 , `incident_type`
				 , `vendor_group`
				 , `vendor_ticket_number`
				 , `status_reason`
				 , `resolution`
				 , `last_work_log`
				 , `impact`
				 , `severity`
                 , `refer_remedy_employee_contract_id`
                 , `gen_sla`)
		VALUES (
									:no_ticket,
									:no_project_ticket,
									:refer_remedy_hd,
									:owner,
									:first_owner,
									:status,
									NOW(),
									NOW(),
									:project_sid,
									:project_owner_sid,
									:via,
									:create_by,
									:receive_mail_datetime,
									:refer_remedy_assigned_time,
									:refer_remedy_create_time,
									:refer_remedy_report_by,
									:subject,
									:due_datetime,
									:description,
									:contract_no,
									:prime_contract,
									:project_name,
									:serial_no,
									:team,
									:requester_full_name,
									:requester_company_name,
									:requester_phone,
									:requester_mobile,
									:requester_email,
									:end_user_company_name,
									:end_user,
									:end_user_site,
									:end_user_contact_name,
									:end_user_phone,
									:end_user_mobile,
									:end_user_email,
									:case_type,
									:case_company,
									:source,
									:note,
									:urgency,
									:updated_by,
									:sla_remedy,
									:update_sla_remedy_datetime,
									:updated_auto_closed_datetime,
									:check_standby,
									:man_days,
									:site_area,
									:send_mail_open_ticket,
									:send_email_open_ticket_datetime,
									:taxi_fare_total,
									:update_taxi_fare,
									:number_sr,
									:updated_number_sr,
									:department,
									:organization_title,
									:functional_title,
									:asset_id,
									:incident_location,
									:incident_address,
									:location_id,
									:location_type,
									:location_area,
									:branch_code,
									:grade,
									:vendor,
									:operational_categorization_tier_1,
									:operational_categorization_tier_2,
									:operational_categorization_tier_3,
									:product_categorization_tier_1,
									:product_categorization_tier_2,
									:product_categorization_tier_3,
									:product_categorization_product_name,
									:product_categorization_model_version,
									:product_categorization_manufacturer,
									:product_categorization_application_tier,
									:product_domain,
									:product_support_period,
									:target_sla,
									:reported_date,
									:responded_date,
									:submit_date,
									:owner_group,
									:reported_source,
									:incident_type,
									:vendor_group,
									:vendor_ticket_number,
									:status_reason,
									:resolution,
									:last_work_log,
									:impact,
									:severity,
                                    :refer_remedy_employee_contract_id,
                                    0
								)
		ON DUPLICATE KEY UPDATE
					no_project_ticket=:no_project_ticket,
					owner=:owner,
					first_owner=:first_owner,
					status=:status,
					update_datetime=NOW(),
					project_sid=:project_sid,
					project_owner_sid=:project_owner_sid,
					via=:via,
					create_by=:create_by,
					receive_mail_datetime=:receive_mail_datetime,
					refer_remedy_assigned_time=:refer_remedy_assigned_time,
					refer_remedy_create_time=:refer_remedy_create_time,
					refer_remedy_report_by=:refer_remedy_report_by,
					subject=:subject,
					due_datetime=:due_datetime,
					description=:description,
					contract_no=:contract_no,
					prime_contract=:prime_contract,
					project_name=:project_name,
					serial_no=:serial_no,
					team=:team,
					requester_full_name=:requester_full_name,
					requester_company_name=:requester_company_name,
					requester_phone=:requester_phone,
					requester_mobile=:requester_mobile,
					requester_email=:requester_email,
					end_user_company_name=:end_user_company_name,
					end_user=:end_user,
					end_user_site=:end_user_site,
					end_user_contact_name=:end_user_contact_name,
					end_user_phone=:end_user_phone,
					end_user_mobile=:end_user_mobile,
					end_user_email=:end_user_email,
					case_type=:case_type,
					case_company=:case_company,
					source=:source,
					note=:note,
					urgency=:urgency,
					updated_by=:updated_by,
					sla_remedy=:sla_remedy,
					update_sla_remedy_datetime=:update_sla_remedy_datetime,
					updated_auto_closed_datetime=:updated_auto_closed_datetime,
					check_standby=:check_standby,
					man_days=:man_days,
					site_area=:site_area,
					send_mail_open_ticket=:send_mail_open_ticket,
					send_email_open_ticket_datetime=:send_email_open_ticket_datetime,
					taxi_fare_total=:taxi_fare_total,
					update_taxi_fare=:update_taxi_fare,
					number_sr=:number_sr,
					updated_number_sr=:updated_number_sr,
					department=:department,
					organization_title=:organization_title,
					functional_title=:functional_title,
					asset_id=:asset_id,
					incident_location=:incident_location,
					incident_address=:incident_address,
					location_id=:location_id,
					location_type=:location_type,
					location_area=:location_area,
					branch_code=:branch_code,
					grade=:grade,
					vendor=:vendor,
					operational_categorization_tier_1=:operational_categorization_tier_1,
					operational_categorization_tier_2=:operational_categorization_tier_2,
					operational_categorization_tier_3=:operational_categorization_tier_3,
					product_categorization_tier_1=:product_categorization_tier_1,
					product_categorization_tier_2=:product_categorization_tier_2,
					product_categorization_tier_3=:product_categorization_tier_3,
					product_categorization_product_name=:product_categorization_product_name,
					product_categorization_model_version=:product_categorization_model_version,
					product_categorization_manufacturer=:product_categorization_manufacturer,
					product_categorization_application_tier=:product_categorization_application_tier,
					product_domain=:product_domain,
					product_support_period=:product_support_period,
					target_sla=:target_sla,
					reported_date=:reported_date,
					responded_date=:responded_date,
					submit_date=:submit_date,
					owner_group=:owner_group,
					reported_source=:reported_source,
					incident_type=:incident_type,
					vendor_group=:vendor_group,
					vendor_ticket_number=:vendor_ticket_number,
					status_reason=:status_reason,
					resolution=:resolution,
					last_work_log=:last_work_log,
					impact=:impact,
					severity=:severity,
                    refer_remedy_employee_contract_id=:refer_remedy_employee_contract_id,
                    gen_sla=0
					";

        $q = $conn->prepare($sql);
        $r = $q->execute(array(
            ':no_ticket'=>$no_ticket,
            ':no_project_ticket'=>$no_project_ticket,
            ':refer_remedy_hd'=>$refer_remedy_hd,
            ':owner'=>$owner,
            ':first_owner'=>$first_owner,
            ':status'=>$status,
            ':update_datetime'=>$update_datetime,
            ':project_sid'=>$project_sid,
            ':project_owner_sid'=>$project_owner_sid,
            ':via'=>$via,
            ':create_by'=>$create_by,
            ':receive_mail_datetime'=>$receive_mail_datetime,
            ':refer_remedy_assigned_time'=>$refer_remedy_assigned_time,
            ':refer_remedy_create_time'=>$refer_remedy_create_time,
            ':refer_remedy_report_by'=>$refer_remedy_report_by,
            ':subject'=>$subject,
            ':due_datetime'=>$due_datetime,
            ':description'=>$description,
            ':contract_no'=>$contract_no,
            ':prime_contract'=>$prime_contract,
            ':project_name'=>$project_name,
            ':serial_no'=>$serial_no,
            ':team'=>$team,
            ':requester_full_name'=>$requester_full_name,
            ':requester_company_name'=>$requester_company_name,
            ':requester_phone'=>$requester_phone,
            ':requester_mobile'=>$requester_mobile,
            ':requester_email'=>$requester_email,
            ':end_user_company_name'=>$end_user_company_name,
            ':end_user'=>$end_user,
            ':end_user_site'=>$end_user_site,
            ':end_user_contact_name'=>$end_user_contact_name,
            ':end_user_phone'=>$end_user_phone,
            ':end_user_mobile'=>$end_user_mobile,
            ':end_user_email'=>$employee_email,
            ':case_type'=>$case_type,
            ':case_company'=>$case_company,
            ':source'=>$source,
            ':note'=>$note,
            ':urgency'=>$urgency,
            ':updated_by'=>$updated_by,
            ':sla_remedy'=>$sla_remedy,
            ':update_sla_remedy_datetime'=>$update_sla_remedy_datetime,
            ':updated_auto_closed_datetime'=>$updated_auto_closed_datetime,
            ':check_standby'=>$check_standby,
            ':man_days'=>$man_days,
            ':site_area'=>$site_area,
            ':send_mail_open_ticket'=>$send_mail_open_ticket,
            ':send_email_open_ticket_datetime'=>$send_email_open_ticket_datetime,
            ':taxi_fare_total'=>$taxi_fare_total,
            ':update_taxi_fare'=>$update_taxi_fare,
            ':number_sr'=>$number_sr,
            ':updated_number_sr'=>$updated_number_sr,
            ':department'=>$department,
            ':organization_title'=>$organization_title,
            ':functional_title'=>$functional_title,
            ':asset_id'=>$asset_id,
            ':incident_location'=>$incident_location,
            ':incident_address'=>$incident_address,
            ':location_id'=>$location_id,
            ':location_type'=>$location_type,
            ':location_area'=>$location_area,
            ':branch_code'=>$branch_code,
            ':grade'=>$grade,
            ':vendor'=>$vendor,
            ':operational_categorization_tier_1'=>$operational_categorization_tier_1,
            ':operational_categorization_tier_2'=>$operational_categorization_tier_2,
            ':operational_categorization_tier_3'=>$operational_categorization_tier_3,
            ':product_categorization_tier_1'=>$product_categorization_tier_1,
            ':product_categorization_tier_2'=>$product_categorization_tier_2,
            ':product_categorization_tier_3'=>$product_categorization_tier_3,
            ':product_categorization_product_name'=>$product_categorization_product_name,
            ':product_categorization_model_version'=>$product_categorization_model_version,
            ':product_categorization_manufacturer'=>$product_categorization_manufacturer,
            ':product_categorization_application_tier'=>$product_categorization_application_tier,
            ':product_domain'=>$product_domain,
            ':product_support_period'=>$product_support_period,
            ':target_sla'=>$target_sla,
            ':reported_date'=>$reported_date,
            ':responded_date'=>$responded_date,
            ':submit_date'=>$submit_date,
            ':owner_group'=>$owner_group,
            ':reported_source'=>$reported_source,
            ':incident_type'=>$incident_type,
            ':vendor_group'=>$vendor_group,
            ':vendor_ticket_number'=>$vendor_ticket_number,
            ':status_reason'=>$status_reason,
            ':resolution'=>$resolution,
            ':last_work_log'=>$last_work_log,
            ':impact'=>$impact,
            ':severity'=>$severity,
            ':refer_remedy_employee_contract_id'=>$refer_remedy_employee_contract_id
        ));
        $check = $rowsBeforeInsert - getRowTicket($conn);
        $conn->commit();
        if ($check == 0) {
            return "Update Complete";
        } elseif ($check < 0) {
            return "Insert Complete";
        }
    } catch (PDOException $e) {
        $conn->rollBack();
        return json_encode(array('error'=>"true", 'message'=>"Something went wrong" ));
    }
}

function calSeverityCase($priority){
  switch ($priority) {
    case 'Critical':
      return "Severity-1";
      break;
    case 'High':
        return "Severity-2";
        break;
    case 'Medium':
          return "Severity-3";
          break;
    case 'Low':
            return "Severity-3";
            break;
    default:
      return "none";
      break;
  }
}

function getStatusIdFromName($statusName, $conn){
    try{
        $sql = "SELECT sid FROM case_status WHERE name_return_to_remedy = :name_return_to_remedy";
        $q = $conn->prepare($sql);
        $q->execute(array(
            ':name_return_to_remedy' => $statusName
        ));
        $r = $q->fetch();
        return $r['sid'];
    }catch (Exception $e){
        return "";
    }
}

function getDefaultOwner($role_sid, $conn){
    try{
        $sql = "SELECT U.email, R.name FROM user U 
                JOIN role R ON R.sid = U.role_sid
                WHERE R.sid = :role_sid and U.user_type = 1
                ORDER BY U.sid ASC limit 0,1;";
        $q = $conn->prepare($sql);
        $q->execute(array(':role_sid'=>$role_sid));
        $r = $q->fetch();
        return $r;
    }catch (Exception $e){
        return null;
    }
}

function getRowTicket($conn)
{
    try {
        $sql =  "SHOW TABLE STATUS FROM ".DB_NAME." like 'ticket'";
        $q = $conn->prepare($sql);
        $q->execute();
        $r = $q->fetch();
        return $r['Rows'];
    } catch (PDOException $e) {
        return "Error : " . $e->getMessage();
    }
}

function getTicketByTicketNumber($refer_remedy_hd, $conn){
    $sql = "select * from ticket where refer_remedy_hd = :refer_remedy_hd";
    $q = $conn->prepare($sql);
    $q->execute(array(':refer_remedy_hd' => $refer_remedy_hd));
    $r = $q->fetch();
    return $r;
}

function getAssigneeUpdate($refer_remedy_hd, $conn){
        $owner = getDefaultOwner(DEFAULT_ROLE_SID_ASSIGNED_TICKET, $conn);
        return array('team'=>$owner['name'], 'email'=>$owner['email']);
}


