<?php
ob_start();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once __dir__."/../../apis/core/constant.php";
require_once("../nusoap-0.9.5/lib/nusoap.php");
require_once __dir__."/../../apis/core/config.php";
header("Content-Type: text/html; charset=utf-8");

$server = new soap_server();
$server->configureWSDL("sos_worklog", "urn:sos_worklog", SOAP_WS_URL_UPDATE_WORKLOG);
$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;

$server->register("sos_worklog", 
	array(
		"ticket_id"=>"xsd:string",
		"Detail_Description"=>"xsd:string",
		"Attachment_Name"=>"xsd:string",
		"Attachment_Data"=>"xsd:base64Binary",
		"Attachment_OrigSize"=>"xsd:int",
		"Attachment_2"=>"xsd:string",
		"Attachment_3"=>"xsd:string",
		"Submitter_Email"=>"xsd:string"
		),
    array("return" => "xsd:string"),
    "urn:sos_worklog",
    "urn:sos_worklog#getTest",
    "rpc",
    "encoded",
    "sos_worklog");

if ( !isset( $HTTP_RAW_POST_DATA ) ) $HTTP_RAW_POST_DATA =file_get_contents( 'php://input' );
$server->service($HTTP_RAW_POST_DATA);

// $server->service($HTTP_RAW_POST_DATA);


function sos_worklog(
	$ticket_id,
	$Detail_Description,
	$Attachment_Name,
	$Attachment_Data,
	$Attachment_OrigSize,
	$Attachment_2,
	$Attachment_3,
	$Submitter_Email
) {
    $ticket_sid = getTicketSid($ticket_id);
    if(isset($ticket_sid)){
        try {
            $conn = getConnection();
            //$conn->beginTransaction();
            $stmt = $conn->prepare("INSERT INTO gable_worklog 
	    	(case_id, detail_description, description,submitter_email, create_datetime, client_type, 
	    	attachment, attachment_data, attachment_orig_size, 
	    	attachment_2, attachment_3) 
	    VALUES (:case_id, :detail_description,'',:submitter_email, NOW(), '', :attachment,'', :attachment_orig_size, :attachment_2, :attachment_3) ");

            $stmt->bindParam(':case_id', $ticket_id);
            $stmt->bindParam(':detail_description', $Detail_Description);
            $stmt->bindParam(':attachment',$Attachment_Name);
            $stmt->bindParam(':attachment_orig_size',$Attachment_OrigSize);
            $stmt->bindParam(':attachment_2',$Attachment_2);
            $stmt->bindParam(':attachment_3',$Attachment_3);
            $stmt->bindParam(':submitter_email',$Submitter_Email);

            $stmt->execute();
            $path = "";
            $idMd5 = md5($conn->lastInsertId());

            // debug log file attach
//            try{
//                $sql = "UPDATE gable_worklog SET log_data_attach = :log_data_attach, attachment_data = :attachment_data WHERE sid = :sid";
//                $stmt = $conn->prepare($sql);
//                $stmt->execute(array(
//                    ":log_data_attach"=>$Attachment_Data,
//                    ":attachment_data"=>$Attachment_Data,
//                    ":sid"=>$conn->lastInsertId()
//                ));
//            }catch (Exception $e){
//
//            }



            if(strlen($Attachment_Name)>0 && strlen($Attachment_Data)>0){
                //$path = base64_to_file($Attachment_Data, $idMd5."_".$Attachment_Name);
                $path = rawDataToFile($Attachment_Data, $idMd5."_".$Attachment_Name);
            }


            insertWorklog($ticket_sid,
                $Detail_Description,
                $Attachment_Name,
                $Attachment_Data,
                $Attachment_OrigSize,
                $Attachment_2,
                $Attachment_3,
                $Submitter_Email,
                $path,
                $conn);



            //$conn->commit();
            $message = "Updated successfully";
            return $message;
        }catch(Exception $e){
            //$conn->rollBack();
            return $e->getMessage();
        }


    }else{
        return "Error not found ticket ID ".$ticket_id.".";
    }

}

function insertWorklog(
    $ticket_id,
    $Detail_Description,
    $Attachment_Name,
    $Attachment_Data,
    $Attachment_OrigSize,
    $Attachment_2,
    $Attachment_3,
    $Submitter_Email,
    $path_file_attach,
    $conn){
    $sql = "INSERT INTO ticket_status (ticket_sid, send_to_ccd, case_status_sid, create_datetime, create_by, data_from,
data_status, update_datetime, update_by, worklog, path_file_attach, original_file_name, size_file_attach) VALUE (:ticket_sid, :send_to_ccd, :case_status_sid, NOW(), :create_by, :data_from,
:data_status, NOW(), :update_by, :worklog, :path_file_attach, :original_file_name, :size_file_attach)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':ticket_sid'=>$ticket_id,
        ':send_to_ccd'=>1,
        ':case_status_sid'=>8,
        ':create_by' => $Submitter_Email,
        ':data_from' => 'remedy',
        ':data_status'=> 8,
        ':update_by'=>$Submitter_Email,
        ':worklog'=>$Detail_Description,
        ':path_file_attach'=>$path_file_attach,
        ':original_file_name'=>$Attachment_Name,
        ':size_file_attach'=>$Attachment_OrigSize));
}

function getTicketSid($refer_remedy_hd){
    try{
        $conn = getConnection();
        $sql = "SELECT sid FROM ticket where refer_remedy_hd = :refer_remedy_hd";
        $stmt =  $conn->prepare($sql);
        $stmt->execute(array(':refer_remedy_hd'=>$refer_remedy_hd));
        $r = $stmt->fetch();
        return $r['sid'];
    }catch (Exception $e){
        return $e->getMessage();
    }
}

function getConnection (){

    $servername = "127.0.0.1";
    $username = DB_USERNAME;
    $password = DB_PASSWORD;
    $dbname = DB_NAME;
    $conn = new PDO("mysql:host=$servername;port=5006;dbname=$dbname;charset=utf8", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
}

function base64_to_file($base64_string, $fileName) {
    try{
        // open the output file for writing
        $date = date('Y_m_d', time());
        $dateSplit = explode('_',$date);
        $pathFile = "uploads/file_attach/".$dateSplit[0]."/".$dateSplit[1];
        $fullPath = __dir__."/../../".$pathFile;
        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }
        $fullPath = $fullPath."/".$fileName;
        $fullPath2 = __dir__."/../../".$pathFile."/not_base64_decode_".$fileName;

        $ifp = fopen( $fullPath, 'wb' );
        $data = explode( ',', $base64_string );
        $data = isset($data[1])?$data[1]:$data[0];
        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data));
        // clean up the file resource
        fclose($ifp);

        // test
        $ifp = fopen( $fullPath2, 'wb' );
        fwrite($ifp, $base64_string);
        fclose($ifp);
        // end test
        return $pathFile."/".$fileName;
    }catch (Exception $e){
        return "";
    }
}

function rawDataToFile($data, $fileName){
    try{
        $date = date('Y_m_d', time());
        $dateSplit = explode('_',$date);
        $pathFolder = "uploads/file_attach/".$dateSplit[0]."/".$dateSplit[1];
        $fullPath = __dir__."/../../".$pathFolder;
        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }
        $fullPath = $fullPath."/".$fileName;
        $ifp = fopen( $fullPath, 'wb' );
        fwrite($ifp, $data);
        fclose($ifp);
        return $pathFolder."/".$fileName;
    }catch (Exception $e){
        return "";
    }
}

?>