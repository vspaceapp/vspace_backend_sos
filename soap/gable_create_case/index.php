<?php
ob_start();

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require_once("../nusoap-0.9.5/lib/nusoap.php");
header("Content-Type: text/html; charset=utf-8");

$server = new soap_server();
$server->configureWSDL("create_case", "urn:create_case");
$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;

$server->register("create", 
	array(
		"case_id"=>"xsd:string",
		"Assigned_Group"=>"xsd:string",
		"Assigned_Support_Company"=>"xsd:string",
		"Assigned_Support_Organization"=>"xsd:string",
		"Assignee"=>"xsd:string",
		"Assignee_Email"=>"xsd:string",
		"Impact"=>"xsd:string",
		"Product_Categorization_Tier_1"=>"xsd:string",
		"Product_Categorization_Tier_2"=>"xsd:string",
		"Product_Categorization_Tier_3"=>"xsd:string",
		"Reported_Source"=>"xsd:string",
		"Service_Type"=>"xsd:string",
		"Status"=>"xsd:string",
		"Action"=>"xsd:string",
		"Create_Request"=>"xsd:string",
		"Summary"=>"xsd:string",
		"Notes"=>"xsd:string",
		"Urgency"=>"xsd:string",
		"Work_Info_Summary"=>"xsd:string",
		"Work_Info_Notes"=>"xsd:string",
		"Work_Info_Type"=>"xsd:string",
		"Work_Info_Source"=>"xsd:string",
		"Work_Info_Locked"=>"xsd:string",
		"Work_Info_View_Access"=>"xsd:string",
		"Customer_Company"=>"xsd:string",
		"Incident_For"=>"xsd:string",
		"Incident_Type"=>"xsd:string",
		"Requester_Full_Name"=>"xsd:string",
		"Requester_Company"=>"xsd:string",
		"Requester_Site"=>"xsd:string",
		"Requester_Code"=>"xsd:string",
		"Requester_Group"=>"xsd:string",
		"Requester_Phone"=>"xsd:string",
		"Requester_Mobile"=>"xsd:string",
		"Requester_Email"=>"xsd:string",
		"Requester_Type"=>"xsd:string",
		"End_User_Full_Name"=>"xsd:string",
		"End_User_Company"=>"xsd:string",
		"End_User_Site"=>"xsd:string",
		"End_User_Code"=>"xsd:string",
		"End_User_Group"=>"xsd:string",
		"End_User_Phone"=>"xsd:string",
		"End_User_Mobile"=>"xsd:string",
		"End_User_Email"=>"xsd:string",
		"Contract_No"=>"xsd:string",
		"Serial"=>"xsd:string",
		"Prime_Contract"=>"xsd:string",
		"Project_Name"=>"xsd:string",
		"Product_Description"=>"xsd:string",
		"Asset_Description"=>"xsd:string",
		"Expire_Warranty"=>"xsd:string",
		"Period_of_Service"=>"xsd:string",
		"Service_Type__c"=>"xsd:string",
		"Add_On_Service"=>"xsd:string",
		"Last_Work_Info"=>"xsd:string",
		"Submitter_Full_Name"=>"xsd:string",
		"Submitter_Email"=>"xsd:string"
		),
    array("return" => "xsd:string"),
    "urn:create_hd",
    "urn:create_hd#getTest",
    "rpc",
    "encoded",
    "Just a Create");

if ( !isset( $HTTP_RAW_POST_DATA ) ) $HTTP_RAW_POST_DATA =file_get_contents( 'php://input' );
$server->service($HTTP_RAW_POST_DATA);

// $server->service($HTTP_RAW_POST_DATA);

function create(
	$case_id,
	$Assigned_Group,
	$Assigned_Support_Company,
	$Assigned_Support_Organization,
	$Assignee,
	$Assignee_Email,
	$Impact,
	$Product_Categorization_Tier_1,
	$Product_Categorization_Tier_2,
	$Product_Categorization_Tier_3,
	$Reported_Source,
	$Service_Type,
	$Status,
	$Action,
	$Create_Request,
	$Summary,
	$Notes,
	$Urgency,
	$Work_Info_Summary,
	$Work_Info_Notes,
	$Work_Info_Type,
	$Work_Info_Source,
	$Work_Info_Locked,
	$Work_Info_View_Access,
	$Customer_Company,
	$Incident_For,
	$Incident_Type,
	$Requester_Full_Name,
	$Requester_Company,
	$Requester_Site,
	$Requester_Code,
	$Requester_Group,
	$Requester_Phone,
	$Requester_Mobile,
	$Requester_Email,
	$Requester_Type,
	$End_User_Full_Name,
	$End_User_Company,
	$End_User_Site,
	$End_User_Code,
	$End_User_Group,
	$End_User_Phone,
	$End_User_Mobile,
	$End_User_Email,
	$Contract_No,
	$Serial,
	$Prime_Contract,
	$Project_Name,
	$Product_Description,
	$Asset_Description,
	$Expire_Warranty,
	$Period_of_Service,
	$Service_Type__c,
	$Add_On_Service,
	$Last_Work_Info,
	$Submitter_Full_Name,
	$Submitter_Email
) {	

	
	$servername = "localhost";
	$username = "vspace";
	$password = "v@dm1n";
	$dbname = "vspace_cases";

	try {
	    $conn = new PDO("mysql:host=$servername;port=5006;dbname=$dbname;charset=utf8", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    
	    $stmt = $conn->prepare("INSERT INTO test_create_case 
	    	(case_id, status, subject, incharge, case_type, create_by, urgency,
	    	requester_name,requester_email,requester_mobile, requester_phone,requester_company_name,requester_site,
	    	requester_code,requester_group,requester_type,
	    	enduser_site,enduser_name,
	    	end_user_full_name,end_user_company,end_user_site,end_user_code,end_user_group,end_user_phone,end_user_mobile,end_user_email,
	    	contract_no,serial_no,description,create_time,prime_contract,work_log, create_datetime,
	    	assigned_group,assigned_support_company,assigned_support_organization,impact,product_categorization_tier_1,product_categorization_tier_2,product_categorization_tier_3,reported_source,service_type,action,create_request,work_info_summary,work_info_notes,work_info_type,work_info_source,work_info_locked,work_info_view_access,incident_for,project_name,product_description,asset_description,expire_warranty,period_of_service,service_type__c,add_on_service,	last_work_info,submitter_full_name,submitter_email,assignee_email
	    	) 
	    VALUES (:case_id, :status, :subject, :incharge, :case_type, :create_by, :urgency,:requester_name,:requester_email,:requester_mobile, :requester_phone,:requester_company_name,:requester_site,
	    	:requester_code,:requester_group,:requester_type,
	    	:enduser_site,:enduser_name,
	    	:end_user_full_name,:end_user_company,:end_user_site,:end_user_code,:end_user_group,:end_user_phone,:end_user_mobile,:end_user_email,
	    	:contract_no,:serial_no,:description,NOW(),:prime_contract,:work_log, NOW(),
	    	:assigned_group,:assigned_support_company,:assigned_support_organization,:impact,:product_categorization_tier_1,:product_categorization_tier_2,:product_categorization_tier_3,:reported_source,:service_type,:action,:create_request,:work_info_summary,:work_info_notes,:work_info_type,:work_info_source,:work_info_locked,:work_info_view_access,:incident_for,:project_name,:product_description,:asset_description,:expire_warranty,:period_of_service,:service_type__c,:add_on_service,	:last_work_info,:submitter_full_name,:submitter_email,:assignee_email) ");

	    
	    $stmt->bindParam(':case_id', $case_id);
	    $stmt->bindParam(':status', $Status);
	    $stmt->bindParam(':subject', $Summary);
	    $stmt->bindParam(':incharge', $Assignee);
	    $stmt->bindParam(':case_type', $Incident_Type);
	    $stmt->bindParam(':create_by', $Submitter_Email);
	    $stmt->bindParam(':urgency', $Urgency);

	    $stmt->bindParam(':requester_name', $Requester_Full_Name);
	    $stmt->bindParam(':requester_email', $Requester_Email);
	    $stmt->bindParam(':requester_mobile', $Requester_Mobile);
	    $stmt->bindParam(':requester_phone', $Requester_Phone);
	    $stmt->bindParam(':requester_company_name', $Requester_Company);
	    $stmt->bindParam(':requester_site', $Requester_Site);
	    $stmt->bindParam(':requester_code', $Requester_Code);
	    $stmt->bindParam(':requester_group', $Requester_Group);
	    $stmt->bindParam(':requester_type', $Requester_Type);

	   
	    $stmt->bindParam(':enduser_site', $End_User_Site);
	    $stmt->bindParam(':enduser_name', $Customer_Company);

	    $stmt->bindParam(':end_user_full_name', $End_User_Full_Name);
	    $stmt->bindParam(':end_user_company', $End_User_Company);
	    $stmt->bindParam(':end_user_site', $End_User_Site);
	    $stmt->bindParam(':end_user_code', $End_User_Code);
	    $stmt->bindParam(':end_user_group', $End_User_Group);
	    $stmt->bindParam(':end_user_phone', $End_User_Phone);
	    $stmt->bindParam(':end_user_mobile', $End_User_Mobile);
	    $stmt->bindParam(':end_user_email', $End_User_Email);

	    $stmt->bindParam(':contract_no', $Contract_No);
	    
	    $stmt->bindParam(':serial_no', $Serial);
	    $stmt->bindParam(':description', $Notes);

	    $stmt->bindParam(':prime_contract', $Prime_Contract);
	    $stmt->bindParam(':work_log', $Last_Work_Info);


	    $stmt->bindParam(':assigned_group', $Assigned_Group);
	    $stmt->bindParam(':assigned_support_company', $Assigned_Support_Company);
	    $stmt->bindParam(':assigned_support_organization',$Assigned_Support_Organization);

	    $stmt->bindParam(':impact',$Impact);
	    $stmt->bindParam(':product_categorization_tier_1',$Product_Categorization_Tier_1);
	    $stmt->bindParam(':product_categorization_tier_2',$Product_Categorization_Tier_2);
	    $stmt->bindParam(':product_categorization_tier_3',$Product_Categorization_Tier_3);
	    $stmt->bindParam(':reported_source',$Reported_Source);
	    $stmt->bindParam(':service_type',$Service_Type);
	    $stmt->bindParam(':action',$Action);
	    $stmt->bindParam(':create_request',$Create_Request);
	    $stmt->bindParam(':work_info_summary',$Work_Info_Summary);
	    $stmt->bindParam(':work_info_notes',$Work_Info_Notes);
	    $stmt->bindParam(':work_info_type',$Work_Info_Type);
	    $stmt->bindParam(':work_info_source',$Work_Info_Source);
	    $stmt->bindParam(':work_info_locked',$Work_Info_Locked);
	    $stmt->bindParam(':work_info_view_access',$Work_Info_View_Access);
	    $stmt->bindParam(':incident_for',$Incident_For);
	    $stmt->bindParam(':project_name',$Project_Name);
	    $stmt->bindParam(':product_description',$Product_Description);
	    $stmt->bindParam(':asset_description',$Asset_Description);
	    $stmt->bindParam(':expire_warranty',$Expire_Warranty);
	    $stmt->bindParam(':period_of_service',$Period_of_Service);
	    $stmt->bindParam(':service_type__c',$Service_Type__c);
	    $stmt->bindParam(':add_on_service',$Add_On_Service);
	    $stmt->bindParam(':last_work_info',$Last_Work_Info);
	    $stmt->bindParam(':submitter_full_name',$Submitter_Full_Name);
	    $stmt->bindParam(':submitter_email',$Submitter_Email);

	    $stmt->bindParam(':assignee_email', $Assignee_Email);

	    $stmt->execute();

	    try{
	    	$sql = "SELECT sid FROM ticket WHERE refer_remedy_hd = :refer_remedy_hd ";
	    	$q = $conn->prepare($sql);
	    	$q->execute(array(':refer_remedy_hd'=>$case_id));
	    	$r = $q->fetch();


	    	if($status=="Assigned"){
	    		$status = "1";
	    	}else if($status=="In Progress"){
				$status = "2";
	    	}else if($status=="Pending"){
	    		$status = "4";
	    	}else if($status=="Resolved"){
	    		$status = "5";
	    	}else if($status=="Closed"){
	    		$status = "6";
	    	}else if($status=="Cancelled"){
	    		$status = "6";
	    	}else if($status=="Completed"){
	    		$status = "5";
	    	}else{
	    		$status = "2";
	    	}

	    	$owner = strtolower($owner);
	    	
	    	if(isset($r['sid']) && $r['sid']>0){

	    		$sql = "UPDATE ticket SET status = :status, owner = :owner, update_datetime = NOW() WHERE sid = :sid ";
	    		$q = $conn->prepare($sql);
	    		$q->execute(
	    			array(
	    				':status'=>$Status,
	    				':owner'=>$Assignee_Email,
	    				':sid'=>$r['sid']
	    				)
	    			);
	    		$message = "Updated successfully";
	    	}else{
	    		require_once '../../apis/include/GenNoCaseSR.php';
	    		$message = "New records created successfully";
	    		$newNoTicket = genTicketNo($conn);

	    		$sql = "INSERT INTO ticket 
	    		(create_datetime,
	    		no_ticket,
	    		refer_remedy_hd,
	    		owner,
	    		status,
	    		via,
	    		subject,
	    		description,
	    		contract_no,
	    		serial_no,
	    		requester_full_name,
	    		requester_email,
	    		requester_mobile,
	    		requester_phone,
	    		requester_company_name,
	    		end_user,
	    		end_user_site,
	    		end_user_contact_name,
	    		end_user_email,
	    		end_user_mobile,
	    		end_user_phone,
	    		end_user_company_name,
	    		case_type,
	    		urgency,
	    		create_by
	    		) 
	    		VALUES 
	    		(NOW(),
	    		:no_ticket,
	    		:refer_remedy_hd,
	    		:owner,
	    		:status,
	    		'remedy_sos',
	    		:subject,
	    		:description,
	    		:contract_no,
	    		:serial_no,
	    		:requester_full_name,
	    		:requester_email,
	    		:requester_mobile,
	    		:requester_phone,
	    		:requester_company_name,
	    		:end_user,
	    		:end_user_site,
	    		:end_user_contact_name,
	    		:end_user_email,
	    		:end_user_mobile,
	    		:end_user_phone,
	    		:end_user_company_name,
	    		:case_type,
	    		:urgency,
	    		:create_by
	    		) ";
	    		$q = $conn->prepare($sql);
	    		$q->execute(
	    			array(
	    				':no_ticket'=>$newNoTicket,
	    				':refer_remedy_hd'=>$case_id,
			    		':owner'=>$Assignee_Email,
			    		':status'=>$Status,
			    		':subject'=>$Summary,
			    		':description'=>$Notes,
			    		':contract_no'=>$Contract_No,
			    		':serial_no'=>$Serial,
			    		':requester_full_name'=>$Requester_Full_Name,
			    		':requester_email'=>$Requester_Email,
			    		':requester_mobile'=>$Requester_Mobile,
			    		':requester_phone'=>$Requester_Phone,
			    		':requester_company_name'=>$Requester_Company,
			    		':end_user'=>$Customer_Company,
			    		':end_user_site'=>$End_User_Site,
			    		':end_user_contact_name'=>$End_User_Full_Name,
			    		':end_user_email'=>$End_User_Email,
			    		':end_user_mobile'=>$End_User_Mobile,
			    		':end_user_phone'=>$End_User_Phone,
			    		':end_user_company_name'=>$End_User_Company,
			    		':case_type'=>$Incident_Type,
			    		':urgency'=>$Urgency,
			    		':create_by'=>$Submitter_Email
	    				)
	    			);
	    	}


	    }catch(PDOException $e){
	    	$e->getMessage();
	    }
	    return $message;
	}
	catch(PDOException $e){
	    return "Error: " . $e->getMessage();
	}
	$conn = null;
}
?>