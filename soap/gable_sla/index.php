<?php
ob_start();

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require_once("../nusoap-0.9.5/lib/nusoap.php");
header("Content-Type: text/html; charset=utf-8");

$server = new soap_server();
$server->configureWSDL("gable_sla", "urn:gable_sla", "http://vspace.in.th/soap/gable_sla/index.php");
$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;

$server->register("gable_sla", 
	array(
		"case_id"=>"xsd:string",
		"Sla_Type"=>"xsd:string",
		"ParentStopTime__c"=>"xsd:dateTime",
		"Submitter_Email"=>"xsd:string",
		"expect_pending"=>"xsd:dateTime",
		"Client_Type"=>"xsd:string"
		),
    array("return" => "xsd:string"),
    "urn:gable_sla",
    "urn:gable_sla#getTest",
    "rpc",
    "encoded",
    "gable_sla");

if ( !isset( $HTTP_RAW_POST_DATA ) ) $HTTP_RAW_POST_DATA =file_get_contents( 'php://input' );
$server->service($HTTP_RAW_POST_DATA);

// $server->service($HTTP_RAW_POST_DATA);

function gable_sla(
	$case_id,
	$Sla_Type,
	$ParentStopTime__c,
	$Submitter_Email,
	$expect_pending,
	$Client_Type
) {	
	require_once "/home/vlogic/public_html/apis/core/config.php";
	$servername = "127.0.0.1";
	$username = DB_USERNAME;
	$password = DB_PASSWORD;
	$dbname = DB_NAME;

	try {
	    $conn = new PDO("mysql:host=$servername;port=5006;dbname=$dbname;charset=utf8", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $conn->beginTransaction();
	    $stmt = $conn->prepare("INSERT INTO gable_sla 
	    	(case_id, sla_type, parentstoptime__c, create_datetime, submitter_email, expect_pending, client_type) 
	    VALUES (:case_id, :status, :parentstoptime__c, NOW(), :submitter_email, :expect_pending, :client_type) ");

	    
	    $stmt->bindParam(':case_id', $case_id);
	    $stmt->bindParam(':status', $Sla_Type);
	    $stmt->bindParam(':parentstoptime__c', $ParentStopTime__c);
	    $stmt->bindParam(':submitter_email',$Submitter_Email);
	    $stmt->bindParam(':expect_pending', $expect_pending);
	    $stmt->bindParam(':client_type', $Client_Type);
	    
	    $stmt->execute();
		$conn->commit();


		// $conn->beginTransaction();
		// $sql = "SELECT * FROM test_create_case WHERE case_id = :case_id ORDER BY sid DESC LIMIT 0,1";
		// $q = $con->prepare($sql);
		// $q->execute(array(':case_id'=>$case_id));
		// $r = $q->fetch();
		// $Client_Type = $r['client_type'];
		// $conn->commit();


		if($Client_Type=="9"){

			require_once "/home/vlogic/public_html/apis/include/CaseModel.php";
			$caseModel = new CaseModel();

			$ticket_sid = $caseModel->getTicketSidFromRemedyInc($case_id);

			// print_r($ticket_sid);

			$Sla_Type = str_replace('"', '', $Sla_Type);


			$case_status_sid["Response"] = "2";
			$case_status_sid["WA"] = "3";
			$case_status_sid["Pending"] = "4";
			$case_status_sid["Resolution"] = "5";
			$case_status_sid["Onsite"] = "7";

			if($expect_pending){
				$expect_pending = str_replace("T", " ", $expect_pending);
				$expect_pending = str_replace("+07:00", "", $expect_pending);
			}else{
				$expect_pending = "";
			}
			if(isset($case_status_sid[$Sla_Type])){
														//ticket_sid, worklog, case_status_sid, 			email 			, expect_pending, solution, solution_detail, send_to_ccd, is_return_remedy
				$caseModel->initailUpdateSlaAndWorklog($ticket_sid, $Sla_Type, $case_status_sid[$Sla_Type], $Submitter_Email, $expect_pending, "", "" , "0", 2);
			}
		}

	    $message = "Updated successfully";
	    // try{


	    // }catch(PDOException $e){
	    // 	$e->getMessage();
	    // }
	    return $message;
	}
	catch(PDOException $e){
	    return "Error: " . $e->getMessage();
	}
	$conn = null;
}
?>