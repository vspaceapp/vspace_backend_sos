<?php
//error_reporting(1);
//ini_set('display_errors', 'On');
if(!isset($_GET['task']) || $_GET['task']=="" ){
  echo "Bad request";
  exit();
}else{
    include_once __dir__ . "/../../../apis/include/db_connect.php";

    $obj = new DbConnect();
    $db = $obj->connect();
    $sql = "SELECT DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') now ";
    $q = $db->prepare($sql);
    $q->execute();
    $r = $q->fetch();
    $datetime = $r['now'];

    if (isset($_GET['engineer']) && $_GET['engineer'] != "") {
        $obj->setTable($_GET['engineer']);
    }
    $sql = "SELECT T.*,CST.file_name,
      (
        SELECT subject 
        FROM " . $obj->table_ticket . " ticket 
        WHERE ticket.sid = T.ticket_sid ORDER BY ticket.sid DESC LIMIT 0,1
      ) 
      subject,T.subject_service_report, 
      (
        SELECT solution 
        FROM " . $obj->table_task_input_action . " TIA 
        WHERE TIA.task_sid = T.sid 
        ORDER BY TIA.sid DESC LIMIT 0,1 
      ) 
      action_solution , 
      (
        SELECT CONCAT(thainame,' ', DATE_FORMAT(TIA.create_datetime,'%d.%m.%Y %H:%i')) 
        FROM " . $obj->table_task_input_action . " TIA 
        LEFT JOIN employee E ON TIA.create_by = E.emailaddr 
        WHERE TIA.task_sid = T.sid 
        ORDER BY TIA.sid 
        DESC LIMIT 0,1 ) create_by   
    FROM " . $obj->table_tasks . " T 
    LEFT JOIN " . $obj->table_customer_signature_task . " CST ON T.sid = CST.task_sid 
    WHERE T.sid = :task ";
    $q = $db->prepare($sql);
    $q->execute(array(':task' => $_GET['task']));
    $r = $q->fetch();
    if (!isset($r['sid'])) {
        echo "No result";
        exit();
    }
    if ($r['file_name'] != "" || $r['customer_signated']) {
        echo "Signed";
        exit();
    }
    //print_r($r);
    $data = $r;

}?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Signature Pad</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link rel="stylesheet" href="css/signature-pad.css">
  <link rel="stylesheet" type="text/css" href="css/pageslider.css">
  <script type="text/javascript">
    // var _gaq = _gaq || [];
    // _gaq.push(['_setAccount', 'UA-39365077-1']);
    // _gaq.push(['_trackPageview']);

    // (function() {
    //   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    //   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    // })();
  </script>

  <style type="text/css">
    .m-signature-pad--header {
      left: 20px;
      right: 20px;
      bottom: 4px;
      height: 28px;
    }
    .m-signature-pad--body{
      top: 50px;
    }
    @media screen and (max-height: 320px){
      .m-signature-pad--header .description {
          font-size: 1em;
          /*margin-top: 1em;*/
      }
      .m-signature-pad--header .description {
          color: #C3C3C3;
          text-align: center;
          font-size: 1.2em;
          /*margin-top: 1.8em;*/
      }
    }
    @media screen and (max-width: 1024px) {
        .m-signature-pad {
            margin: 0% !important;
        }
    }
    .m-signature-pad--header .description {
      padding: 20px;
      font-size: 14px;
    }
  </style>
</head>
<body>

<script type="text/template" id="FormSignature">
 <form method="POST" enctype="multipart/form-data" action="savesignature.php?engineer=<?php echo isset($_GET['engineer'])?$_GET['engineer']:'';?>">
  <div id="signature-pad" class="m-signature-pad">
    <div class="m-signature-pad--header">
      <div class="description">
        <?php
        if(isset($r['subject_service_report']) && $r['subject_service_report']!=""){
          echo "<div><b>Subject: </b>".$r['subject_service_report']."</div>";
        }else{
          echo "<div><b>Subject: </b>".$r['subject']."</div>";
        }
        ?>
      </div>
      
    </div>
    <div class="m-signature-pad--body">
      <canvas></canvas>
    </div>
    <div class="m-signature-pad--footer">
      <div class="description">Sign above</div>
      <button type="button" class="button clear" data-action="clear">Clear</button>
      <button type="button" class="button save" data-action="save">Save</button>
    </div>
   
      <input class="output" name="output" value=""  type="hidden" />
      <input name="task_sid" value="<?php echo $_GET['task'];?>" type="hidden" />
      <input name="engineer" id="engineer" value="<?php echo isset($_GET['engineer'])?$_GET['engineer']:'';?>" type="hidden"  />
  </div>
</form>
</script>
<script type="text/template" id="DetailService">
	<div id="signature-pad" class="m-signature-pad">
	    <div class="m-signature-pad--header">
	      	<div class="description" style="color: #000000;">
	      			<?php
			        if(isset($r['subject_service_report']) && $r['subject_service_report']!=""){
			          echo "<div><b>Subject: </b>".$r['subject_service_report']."</div>";
			        }else{
			          echo "<div><b>Subject: </b>".$r['subject']."</div>";
			        }
			        ?>
			</div>
	      
	    </div>
	    <div class="m-signature-pad--body" style="font-size: 14px;padding: 10px; overflow-y:auto;">
	      <?php echo "<div><b>Action : </b>".$r['action_solution']."</div>";?>
	    </div>
	    <div class="m-signature-pad--footer">
	      <div type="button" class="button clear" data-action="clear"><?php echo $r['create_by'];?></div>
	      <a href="/pages/signaturepad/customer/sign.php?task=<?php echo $_GET['task'];?>&engineer=<?php echo isset($_GET['engineer'])?$_GET['engineer']:'';?>" type="button" class="button save" data-action="save" >
	      	<button type="button" class="button save" data-action="save">Next</button>
	      </a>
	    </div>
      
  </div>
</script>
  
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="js/signature_pad.js"></script>

  <script src="lib/router.js"></script>
  <script src="lib/pageslider.js"></script>
  <script src="lib/handlebars.js"></script>

  <script src="js/Flow.js"></script>

  <script src="js/app.js"></script>

  <script type="text/javascript">
    var datetime = '<?php echo $datetime;?>';
    var sr = '<?php echo $data['no_task'];?>';
    // $(function(){
      
    // });
  </script>

</body>
</html>
