<?php if(!isset($_GET['task']) || $_GET['task']=="" || !isset($_GET['engineer']) ){
  echo "Bad request";
  exit();
}else{
    include_once __dir__ . "/../../../apis/include/db_connect.php";
    $obj = new DbConnect();
    $db = $obj->connect();
    $sql = "SELECT DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') now ";
    $q = $db->prepare($sql);
    $q->execute();
    $r = $q->fetch();
    $datetime = $r['now'];

    if($_GET['engineer']!=""){
      $obj->setTable($_GET['engineer']);
    }

    $sql = "SELECT T.*,
      CST.file_name,(SELECT subject FROM ".$obj->table_ticket." ticket WHERE ticket.sid = T.ticket_sid ORDER BY ticket.sid DESC LIMIT 0,1) subject,
      T.subject_service_report, (SELECT solution FROM ".$obj->table_task_input_action." TIA WHERE TIA.task_sid = T.sid ORDER BY TIA.sid DESC LIMIT 0,1 ) action_solution  
    FROM ".$obj->table_tasks." T 
    LEFT JOIN ".$obj->table_customer_signature_task." CST ON T.sid = CST.task_sid 
    WHERE T.sid = :task ";
    $q = $db->prepare($sql);
    $q->execute(array(':task'=>$_GET['task']));
    $r = $q->fetch();
    if(!isset($r['sid'])){
      echo "No result";
      exit();
    }
    if($r['file_name']!="" && isset($r['file_name'])){
      echo "Signed";
      exit();
    }
    // print_r($r);
    $data = $r;
}?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Signature Pad</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link rel="stylesheet" href="css/signature-pad.css">

  <script type="text/javascript">
    // var _gaq = _gaq || [];
    // _gaq.push(['_setAccount', 'UA-39365077-1']);
    // _gaq.push(['_trackPageview']);

    // (function() {
    //   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    //   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    // })();
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <style type="text/css">
    .m-signature-pad--header {
      left: 20px;
      right: 20px;
      bottom: 4px;
      height: 28px;
    }
    .m-signature-pad--body{
      top: 50px;
    }
    @media screen and (max-height: 320px){
      .m-signature-pad--header .description {
          font-size: 1em;
          /*margin-top: 1em;*/
      }
      .m-signature-pad--header .description {
          /*color: #C3C3C3;*/
          color: #222222;
          text-align: center;
          font-size: 1.2em;
          /*margin-top: 1.8em;*/
      }
    }
      @media screen and (max-width: 1024px) {
        .m-signature-pad {
            margin: 0% !important;
        }
    }
    .m-signature-pad--header .description {
      padding: 20px;
      font-size: 14px;
    }
  </style>
</head>
<body >
 <form method="POST" enctype="multipart/form-data" action="savesignature.php?engineer=<?php echo isset($_GET['engineer'])?$_GET['engineer']:'';?>">
  <div id="signature-pad" class="m-signature-pad">
    <div class="m-signature-pad--header">
      <div class="description">
        <?php
        if(isset($r['subject_service_report']) && $r['subject_service_report']!=""){
          echo "<div><b>Subject: </b>".$r['subject_service_report']."</div>";
        }else{
          echo "<div><b>Subject: </b>".$r['subject']."</div>";
        }
        ?>
      </div>
      
    </div>
    <div class="m-signature-pad--body">
      <canvas></canvas>
    </div>
    <div class="m-signature-pad--footer">
      <div class="description">Sign above</div>
      <button type="button" class="button clear" data-action="clear">Clear</button>
      <button type="button" class="button save" data-action="save">Save</button>
    </div>
   
      <input class="output" name="output" value=""  type="hidden" />
      <input name="task_sid" value="<?php echo $_GET['task'];?>" type="hidden" />
      <input name="engineer" id="engineer" value="<?php echo isset($_GET['engineer'])?$_GET['engineer']:'';?>" type="hidden"  />
  </div>
</form>
  
  <script type="text/javascript">
    var datetime = '<?php echo $datetime;?>';
    var sr = '<?php echo $data['no_task'];?>';
    $(function(){
      if(localStorage.getItem("email")){
        $("#engineer").val(localStorage.getItem("email"));
      }else{
        $("#engineer").val('');
      }

          $("button[data-action='save']").click(function(){
                if (signaturePad.isEmpty()) {
                    alert("Please provide signature first.");
                } else {
                    console.log(signaturePad.toDataURL());
                            // var c = document.getElementById("pad");
                            var toDataURL = canvas.toDataURL("image/png");
                            console.log(toDataURL);
                            $(".output").val(toDataURL);
                            $("form").submit();

                    // $.ajax({
                    //     type: "POST",
                    //     url: "http://api.flgupload.com/sign/savesignature/100",
                    //     data:{
                    //         signature:JSON.stringify(toDataURL)
                    //     },
                    //     success:function(res){
                    //         console.log(res);
                    //     }
                    // });
                }
        });
    });
  </script>
  <script src="js/signature_pad.js"></script>

  <script src="js/appSign.js"></script>
</body>
</html>
