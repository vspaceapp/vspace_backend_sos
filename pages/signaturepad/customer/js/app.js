var wrapper,clearButton,saveButton,canvas,signaturePad;

function initBg() {

    var c = wrapper.querySelector("canvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = '#f4f4f6';
    ctx.font = window.innerWidth / 20 + "px Cordia New bold italic";
    ctx.fillText("สำหรับ " + sr, window.innerWidth / 30, window.innerHeight / 8);
    ctx.fillText("" + datetime, window.innerWidth / 30, window.innerHeight / 3);
}
// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    initBg();
}
// saveButton.addEventListener("click", function (event) {
//     if (signaturePad.isEmpty()) {
//         alert("Please provide signature first.");
//     } else {
//         console.log(signaturePad.toDataURL());
//         $.ajax({
//             type: "POST",
//             url: "http://api.flgupload.com/sign/savesignature/100",
//             data:{
//                 signature:"123"
//             },
//             success:function(res){
//                 console.log(res);
//             }
//         });
//     }
// });
function runRouter() {
    DetailService.prototype.template = Handlebars.compile($("#DetailService").html());
    FormSignature.prototype.template = Handlebars.compile($("#FormSignature").html());

    var slider = new PageSlider($('body'));
    router.addRoute('', function() {
        slider.slidePageFrom(new DetailService().$el, 'left');
    });
    router.addRoute('sign', function() {
        window.location = window.location.href;
        slider.slidePageFrom(new FormSignature().$el, 'right');


        wrapper = document.getElementById("signature-pad"),
        clearButton = wrapper.querySelector("[data-action=clear]"),
        saveButton = wrapper.querySelector("[data-action=save]"),
        canvas = wrapper.querySelector("canvas"),
        signaturePad;

        window.onresize = resizeCanvas;
        resizeCanvas();
        signaturePad = new SignaturePad(canvas);
        signaturePad.minWidth = 3;
        signaturePad.maxWidth = 5;
        initBg();
        $("[data-action=clear]").click(function(event) {
            signaturePad.clear();
            initBg();
        });
        
    });
    /******************* PROFILE END **************************************/
    router.start();
}
$(function() {
    // initBg();
    if (localStorage.getItem("email")) {
        // $("#engineer").val(localStorage.getItem("email"));
    } else {
        // $("#engineer").val('');
    }
    $("button[data-action='save']").click(function() {
        if (signaturePad.isEmpty()) {
            alert("Please provide signature first.");
        } else {
            console.log(signaturePad.toDataURL());
            // var c = document.getElementById("pad");
            var toDataURL = canvas.toDataURL("image/png");
            console.log(toDataURL);
            $(".output").val(toDataURL);
            $("form").submit();
            // $.ajax({
            //     type: "POST",
            //     url: "http://api.flgupload.com/sign/savesignature/100",
            //     data:{
            //         signature:JSON.stringify(toDataURL)
            //     },
            //     success:function(res){
            //         console.log(res);
            //     }
            // });
        }
    });
    runRouter();
});