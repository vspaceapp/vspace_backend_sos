var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;

function initBg(){
    var c = wrapper.querySelector("canvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = '#f0f2f6';
    // ctx.fillStyle = '#998fff';

    if(window.innerWidth>767){

        ctx.font = window.innerWidth / 16 + "px Cordia New bold italic";
        ctx.fillText("สำหรับ "+sr, window.innerWidth / 20, window.innerHeight / 8);
        ctx.fillText(""+datetime, window.innerWidth / 20, window.innerHeight / 3);
    }else{
        
        ctx.font = window.innerWidth / 12 + "px Cordia New bold italic";
        ctx.fillText("สำหรับ "+sr, window.innerWidth / 10, window.innerHeight / 6);
        ctx.fillText(""+datetime, window.innerWidth / 10, window.innerHeight / 3);
    }
}
// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);

    initBg();
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);
signaturePad.minWidth = 3;
signaturePad.maxWidth = 5;

clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
    initBg();
});

// saveButton.addEventListener("click", function (event) {
//     if (signaturePad.isEmpty()) {
//         alert("Please provide signature first.");
//     } else {
//         console.log(signaturePad.toDataURL());
//         $.ajax({
//             type: "POST",
//             url: "http://api.flgupload.com/sign/savesignature/100",
//             data:{
//                 signature:"123"
//             },
//             success:function(res){
//                 console.log(res);
//             }
//         });
//     }
// });

$(function(){
    initBg();

    
});