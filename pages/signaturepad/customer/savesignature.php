<?php
ini_set('display_errors', '0'); ?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SignaturePad</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link rel="stylesheet" href="css/signature-pad.css">
</head>
<style type="text/css">
  h1 {
      text-align: center;
      /*top: 40%;*/
      /*position: absolute;*/
      /*left: 43%;*/
  }
  @media screen and (max-width: 1024px) {
        .m-signature-pad {
            margin: 0% !important;
        }
    }
    .m-signature-pad--header .description {
      padding: 20px;
      font-size: 14px;
    }
</style>
<script type="text/javascript">
  history.pushState(null, null, document.title);
  window.addEventListener('popstate', function () {
      history.pushState(null, null, document.title);
  });
</script>
<body>
<div id="signature-pad" class="m-signature-pad">
    <div class="m-signature-pad--header">
    </div>
    <div class="m-signature-pad--body">
  
<?php
$rootPath = __dir__."/";
              // include "../../../config.php";
              //include $rootPath."apis/include/db_connect.php";
include_once __dir__ ."/../../../apis/include/db_connect.php";
include_once __dir__."/../../../apis/include/db_handler.php";
require_once __dir__."/../../../apis/core/constant.php";
              $obj = new DbConnect();
              $db = $obj->connect();

              $tasks_sid = $_POST['task_sid'];
              if(isset($_GET['engineer'])){
                $engineer = $_GET['engineer'];
              }else{
                $engineer = "";
              }

              $prefix = "";
              if($engineer!=""){
                // $obj->setTable($engineer);
                $prefix = $obj->getTablePrefix($obj->getRoleByEmail($engineer));
              }


              // echo "engineer ".$engineer;
              // if($tasks_sid=="1069"){
              //   exit();
              // }
              if(isSigned($tasks_sid, $db, $obj)){
                    $filteredData = substr($_POST['output'], strpos($_POST['output'], ",") + 1);
                      //Decode the string
                    $unencodedData = base64_decode($filteredData);
                    $file_name_starter = uniqid() .'_'.$tasks_sid;
                    $file = $file_name_starter. '.png';
                      //Save the image
                        $rootPath = __dir__."../../..";

                    file_put_contents(__dir__."/../../../uploads/signature/". $file, $unencodedData);

                    insertCustomerSignatureTask($file, $tasks_sid, $engineer, $db, $obj, $file_name_starter);
                    echo "<h1>Thank you</h1>";


                    // if($engineer!=""){
                    //       echo "<div style='bottom: 0px;position: inherit;'><a href='../../../'>Back to Main</a></div>";                      
                    // }
                    if($prefix=="" && 1==2){ // add 1==2 when 9/3/2017
                      fopen("".HOST_NAME."/pdf.php?task=".$tasks_sid."&email=".$engineer,"r");
                    }else{
                      // fopen("http://vspace.in.th/nodes/pdf/?task=".$tasks_sid."&email=".$engineer,"r");

                      //old fopen("http://node02.vspace.in.th/pdf/?task=".$tasks_sid."&email=".$engineer,"r");
                      //old fopen("http://node03.vspace.in.th/pdf/?task=".$tasks_sid."&email=".$engineer,"r");
                    }
                    // fopen("http://vspace.in.th/api/sendmail/sendEmailAfterSigned/".$tasks_sid."/".$engineer, "r");
              }else{
                  echo "<h1>Signed</h1>";
                  // if($engineer!=""){
                  //         echo "<div style='bottom: 0px;position: inherit;'><a href='../../../../case'>Back to Main</a></div>";                      
                  //   }
              }
		          

              function insertCustomerSignatureTask($file, $task_sid, $engineer, $db, $obj, $file_name_starter) {
                  $sql = "INSERT INTO ".$obj->table_customer_signature_task." (file_name,create_datetime,task_sid) VALUES (:file_name,NOW(),:task_sid) ";
                  $q = $db->prepare($sql);
                  $r = $q->execute(array(
                      ':file_name' => $file,
                      ':task_sid' => $task_sid
                  ));

                  $filePdfName = $file_name_starter.".pdf";

                  $sql = "UPDATE ".$obj->table_tasks." SET customer_signated = '1', file_name_signated = :file_name,path_service_report = :path_service_report WHERE sid = :sid ";
                  $q = $db->prepare($sql);
                  $r = $q->execute(array(':file_name'=>$file,
                    ':path_service_report'=>$filePdfName,
                    ':sid'=>$task_sid
                    ));

                  if($engineer!=""){
                      $sql = "SELECT sid FROM ".$obj->table_tasks_log." WHERE tasks_sid = :tasks_sid AND is_deleted = '0' AND status = '500' ";
                      $q = $db->prepare($sql);
                      $q->execute(array(':tasks_sid'=>$task_sid));
                      $r = $q->fetch();
                      if($r['sid']>0){

                      }else{
//                        // echo $engineer;
//                        $rootPath = "/var/www/html/vlogic/public_html/";
//                        include $rootPath.'apis/include/db_handler.php';
                        $dbH = new DbHandler();
                        $dbH->addNewStatusTaskLog($engineer, $task_sid, '500', $engineer, $taxi_fare ="");
                      }
                  }
              }


              function isSigned($task_sid, $db, $obj){
                  $sql = "SELECT * FROM ".$obj->table_customer_signature_task." WHERE task_sid = :task_sid AND status >= 0 ";
                  $q = $db->prepare($sql);
                  $q->execute(array(':task_sid'=>$task_sid));
                  $r = $q->fetchAll();
                  if(count($r)>0){
                      return false;
                  }
                  return true;
              }
?>


</div>
    <div class="m-signature-pad--footer">
    </div>
   
     
  </div>
</body>
</html>