<?php
if (!function_exists("mb_check_encoding")) {
    echo 'mbstring extension is not enabled';
}
ob_start();
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
ini_set('display_errors', 'On');
if(isset($_GET['task']) && isset($_GET['email'])){
    $task = $_GET['task'];

    $email = $_GET['email'];

include_once __dir__."/../../apis/core/config.php";
include_once __dir__."/../../apis/include/db_handler.php";
include_once __dir__."/../../simple_html_dom.php";

$db_handler = new DbHandler();
$dataPDF = $db_handler->getDataTaskForPDF($task, $email);
 echo "<pre>";
 print_r($dataPDF);
 echo "</pre>";
// exit();

$string = "";
for($i=0;$i<100;$i++){
    $string .= "<p>".$i."</p>";
}
$type = $dataPDF['ticket_case_type'];
$product = "N/A";
$model = "N/A";
$serial = "";

if($dataPDF['subject_service_report']!=""){
    $symtom = $dataPDF['subject_service_report'];
}else{
    $symtom = $dataPDF['subject'];
}

$serviceStatus = "Closed";
if(isset($dataPDF['is_follow_up']) && $dataPDF['is_follow_up']=="1" && $dataPDF['follow_up_date']!="0000-00-00 00:00"){
    $serviceStatus = "Follow Up ";
    $serviceStatus .= "(".$dataPDF['follow_up_date'].")";
}

// if($type=="Preventive Maintenance"){
//     if($dataPDF['end_user_company_name_service_report']!=""){
//         $address = $serviceLocation = $dataPDF['end_user_company_name_service_report'];
//     }else{
//         $address = $serviceLocation = $dataPDF['ticket_end_user_site'];
//     }
// }else{
    $address = $serviceLocation = (isset($dataPDF['service_report_address']) && $dataPDF['service_report_address']!="")?$dataPDF['service_report_address']:$dataPDF['ticket_end_user_site'];
// }
$service_type_name = $dataPDF['service_type_name'];

$taskNo = $dataPDF['no_task'];
$caseNo = $dataPDF['no_ticket'];
$contract = $dataPDF['ticket_contract'];

if($dataPDF['end_user_contact_name_service_report']!="" && $dataPDF['end_user_email_service_report']!=""){
    $contactPerson = $dataPDF['end_user_contact_name_service_report'];
    $tel = ($dataPDF['end_user_mobile_service_report'])?$dataPDF['end_user_mobile_service_report']:$dataPDF['end_user_phone_service_report'];
    $tel .='/ '.$dataPDF['end_user_email_service_report'];
    $company = $dataPDF['end_user_company_name_service_report'];
}else{
    $contactPerson = $dataPDF['ticket_end_user_contact_name'];
    $tel = ($dataPDF['ticket_end_user_mobile'])?$dataPDF['ticket_end_user_mobile']:$dataPDF['ticket_end_user_phone'];
    $tel .= '/ '.$dataPDF['ticket_end_user_email'];
    $company = $dataPDF['ticket_end_user_company_name'];
}

if(isset($dataPDF['case_company']) && $dataPDF['case_company']!=""){
    $company = $dataPDF['case_company'];
}
if(isset($dataPDF['end_user']) && $dataPDF['end_user']!=""){
    $company = $dataPDF['end_user'];
}
if($dataPDF['customer_signature']==""){
    echo "Please customer sign";
    exit();
}

$engineerName = $dataPDF['thainame'];
if($dataPDF['path_signature']!=""){
    $imgEng = '../../uploads/'.$dataPDF['path_signature'];
}else{
    $imgEng = '../../uploads/bg_white.jpg';
}

if(isset($dataPDF['customer_signature']) && $dataPDF['customer_signature']!="" && file_exists('../../uploads/signature/'.$dataPDF['customer_signature'])){
    $imgSign = '../../uploads/signature/'.$dataPDF['customer_signature'];
}else{
    $imgSign = '../../uploads/bg_white.jpg';
}
$associate = "";
foreach ($dataPDF['associate'] as $key => $value) {
    $associate .= $value['thainame'];
    if(count($dataPDF['associate'])-1 != $key ){
        $associate .= ",";
    }
}

$action = "Action: <br/>";

if($dataPDF['type']=="Preventive Maintenance"){
    $action .= "PM Service";
    if($dataPDF['to_do_pm']['to_do_1']=="do"){
        $action .= "<br/>1. ".$dataPDF['to_do_pm']['to_do_name_1'];
    }
    if($dataPDF['to_do_pm']['to_do_2']=="do"){
        $action .= "<br/>2. ".$dataPDF['to_do_pm']['to_do_name_2'];
    }
    if($dataPDF['to_do_pm']['to_do_3']=="do"){
        $action .= "<br/>3. ".$dataPDF['to_do_pm']['to_do_name_3'];
    }
    if($dataPDF['to_do_pm']['to_do_4']=="do"){
        $action .= "<br/>4. ".$dataPDF['to_do_pm']['to_do_name_4'];
    }

    if(count($dataPDF['serial'])>0){
        $action .= "<br/><br/>Serial";
        foreach ($dataPDF['serial'] as $key => $value) {
            $action .= "<br/>".$value['serial_detail'];
        }
    }

    if(isset($dataPDF['input_action']['solution']) && $dataPDF['input_action']['solution']!="" ){
        $action .= "<br/><br/>".$dataPDF['input_action']['solution'];
    }
    if(isset($dataPDF['input_action']['recommend']) && $dataPDF['input_action']['recommend']!=""){
        $action .= "<br/><br/>".$dataPDF['input_action']['recommend'];
    }
}else{
    $action .= $dataPDF['input_action']['solution'];
    if(count($dataPDF['serial'])>0){
        $action .= "<br/><br/>Serial: ";
        foreach ($dataPDF['serial'] as $key => $value) {
            if($key>0){
                $action .= "<br/>";
            }
            $action .= $value['serial_detail'];
        }
    }
    if(isset($dataPDF['input_action']['recommend'])){
        if($dataPDF['input_action']['recommend']!=""){
            $action .= "<br/><br/>Recommend: ".$dataPDF['input_action']['recommend'];
        }
    }
}
$string = "Subject: ".$symtom."<br/><br/>";
// $string .= "Type: ".$type."";//  PRODUCT: ".$product."<br/>";
// $string .= "MODEL: ".$model." SERIAL: - <br/>";
// $string .= "<br/>Service Type: ".$service_type_name."<br/>";
if(isset($dataPDF['ticket_serial']) && $dataPDF['ticket_serial']!=""){
  if(strpos("Information",$dataPDF['ticket_serial'])>0){
    $string .= "<br/>Serial: ".$dataPDF['ticket_serial'].'<br/><br/>';
  }
}

$string .= $action;
// $string .= "<br/><br/>Service Status: ".$serviceStatus;
$string = trim($string);

// border-left:1px solid;border-right:1px solid;
$html = '<div id="wrapper" style="background:url(\'img/water_mark.jpg\');background-repeat: no-repeat; font-size:20px;">
            <div style="float:left; width:70%;">
                <div style="margin:1%;">'.$string.'</div>
            </div>

            <div style="float:left; width:30%;">
                <table style="font-size:20px; width:100%; border-left: 1px solid black;border-bottom: 1px solid black;border-collapse: collapse;">
                    <tr style="">
                        <td colspan="3" style="text-align:center;border: 1px solid;border-collapse: collapse;">TIME STAMP</td>
                    </tr>
                    <tr style="text-align:center;background-color: #f1f1f1;" >
                        <td style="width:33%;text-align:center; border: 1px solid;border-collapse: collapse;">Time</td>
                        <td style="width:33%;text-align:center; border: 1px solid;border-collapse: collapse;">DD/MM/20YY</td>
                        <td style="width:33%;text-align:center; border: 1px solid;border-collapse: collapse;">HH:MM</td>
                    </tr>
                    <tr style="text-align:center;">
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">Started</td>
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">'.$dataPDF['time_log_start']['log_task_date'].'</td>
                        <td style="text-align:center; border-collapse: collapse;">
                        '.$dataPDF['time_log_start']['log_task_time'].'
                        </td>
                    </tr>
                    <tr style="text-align:center;">
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">Finished</td>
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">
                            '.$dataPDF['time_log_end']['log_task_date'].'
                        </td>
                        <td style="text-align:center; border-collapse: collapse;">
                            '.$dataPDF['time_log_end']['log_task_time'].'
                        </td>
                    </tr>
                    <tr style="text-align:center;">
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">Period</td>
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">-</td>
                        <td style="text-align:center; border-right: 1px solid;border-collapse: collapse;">
                            '.$dataPDF['service_period'].'
                        </td>
                    </tr>
                </table>
            </div>';

if(count($dataPDF['replace_part'])>0){
    $sparePart = '<div style="margin:5px;color:#5b5b5b;font-family: \'thsarabunnew\', sans-thsarabunnew;font-size:18px;"><br/>';
    $sparePart .= '<table style="font-size:18px; width:100%; border: 1px solid #999999;border-bottom: 1px solid #999999;border-collapse: collapse;font-family: \'thsarabunnew\', sans-thsarabunnew;"><tr><td style="text-align:center;border-top: 1px solid #999999;color:#5b5b5b;text-align:center;" colspan="6">REPLACE PART</td></tr>';
    $sparePart .= '<tr><td colspan="3" style="text-align:center;border-top: 1px solid #999999;border-right: 1px solid #999999;color:#5b5b5b;text-align:center;">OLD PART</td><td colspan="3" style="border-top: 1px solid #999999;text-align:center;color:#5b5b5b;">NEW PART</td></tr>';
    $sparePart .= '<tr style="background-color: #f1f1f1;"><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">PART</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">SERIAL</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">DESCRIPTION</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">PART</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">SERIAL</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">DESCRIPTION</td></tr>';
    foreach ($dataPDF['replace_part'] as $key => $value) {
        $sparePart .= '<tr><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;">'.$value['part_number_defective'].'</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">'.$value['part_serial_defective'].'</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">'.$value['description'].'</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">'.$value['part_number'].'</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">'.$value['part_serial'].'</td><td style="border-top: 1px solid #999999;border-left: 1px solid #999999;color:#5b5b5b;text-align:center;">'.$value['description'].'</td></tr>';
    }
    $sparePart .= '</table>';
    $sparePart .= '</div>';
}

$html  .= '</div>';

require '../../MPDF54/mpdf.php';

if($associate!=""){
    $marginBottom = '42';
}else{
    $marginBottom = '36';
}
$mpdf = new mPDF('th','A4','0','thsarabunnew',0,0,60,90);
$mpdf->SetFont('thsarabunnew');
// $mpdf->SetAutoFont();
$mpdf->SetDisplayMode('fullpage');

$companyEngineer = "";
$imgPath = "../../img/gable/g-able.jpg";

$companyEngineer = "บริษัท จีเอเบิล จำกัด";
// $mpdf->setDisplayMode('fullpage');
// $mpdf->setAutoFont();
/*if(isset($dataPDF['created_appointment_by']) && isset($dataPDF['ticket_contract']) && $dataPDF['ticket_contract']!="" && (strpos($dataPDF['created_appointment_by'],"firstlogic")>0 || strpos($dataPDF['created_appointment_by'],"g-able")>0 ) ){
    if(substr($dataPDF['ticket_contract'], 0, 2)=="F1"){
        $imgPath = "../../img/service_report_logo.gif";
        $companyEngineer = "First Logic Company Limited";

    }else if(substr($dataPDF['ticket_contract'], 0, 2)=="A1" || substr($dataPDF['ticket_contract'], 0, 2)=="L1" || substr($dataPDF['ticket_contract'], 0, 2)=="M1" || substr($dataPDF['ticket_contract'], 0, 2)=="T1"){
        $imgPath = "../../img/gable/g-able.jpg";

        $companyEngineer = "บริษัท จีเอเบิล จำกัด";
    }else{
        if(isset($dataPDF['company_logo']) && $dataPDF['company_logo']!=""){
            $imgPath = $dataPDF['company_logo'];
        }else{
            $imgPath = "../../img/engineer/bg_white.jpg";
        }
    }
}else if(isset($dataPDF['company_logo']) && $dataPDF['company_logo']!=""){
    $imgPath = $dataPDF['company_logo'];
}else{
    $imgPath = "../../img/engineer/bg_white.jpg";
}*/
// if($dataPDF['comp_refer_ebiz']=="first"){
//     $imgPath = "img/service_report_logo.gif";
// }else if($dataPDF['comp_refer_ebiz']=="cdgm"){
//     $imgPath = "img/mvg/Mverge.png";
// }else if($dataPDF['comp_refer_ebiz']=="tcs"){
//     $imgPath = "img/tcs/TCS.png";
// }else{
//     $imgPath = "img/gable/g-able.jpg";
// }
/// Change 2017/04/26

// $mpdf->setHtmlHeader('<div style=""><div style=" height:60px; ">
// <div style="float:left; width:33.33%; text-align:left;"><img src="'.$imgPath.'" width=60%/></div>
// <div style="float:left; text-align:center; width:33.33%; font-size:40px; font-weight: 700; ">SERVICE REPORT</div>
// <div style="float:left; text-align:right; width:33.33%; "><span style="font-size:24px; font-weight: 700;" >Service Report No.: </span><span style="font-size:24px;" > '.$taskNo.' </span>
// <br/> <span style="font-size:21px; font-weight: 700;" >Request No.:</span><span style="font-size:24px;" > '.$caseNo.' </span></div>
// </div>
// <div style="height:100px;clear:both; margin-top:10px; border-style:solid; border-width: 1px; ">
//     <div style="float:left; width:46%; margin-left:1%; ">
//             <span style="font-size:20px; font-weight: 700;" >Company Name :</span>
//             <span style="font-size:20px;" >'.$company.' </span>
//     </div>

//     <div style="float:left;  width:18%; text-align:left;">
//         <span style="font-size:20px;" >Contract No. :</span>
//         <span style="font-size:20px;" >'.$contract.' </span>
//     </div>

//     <div style="float:left;  width:34%; text-align:right; margin-right:1%;">
//         <span style="font-size:20px;" >Contact Person :</span>
//         <span style="font-size:20px;" margin-right:1%;>'.$contactPerson.' </span>
//     </div>

//     <div style="float:left; text-align:left; width:46%; margin-left:1%; ">
//         <span style="font-size:20px;">Site Address : </span>
//         <span style="font-size:20px;">'.$address.'</span>
//     </div>

//     <div style="float:left; margin-right:1%; width:52%; text-align:left; ">
//         <span style="font-size:20px; ">Tel/E-Mail : '.$tel.'</span>
//     </div>
// </div>
// <div style="clear:both;"></div><div style="margin-top:2px; height:100%; border:1px solid; "></div></div>');


// $mpdf->setHtmlFooter('
//     <img src="'.$imgEng.'" style="position:absolute; float:left; vertical-align: baseline;height:80px; margin-bottom:-80px; margin-left:80px;"/>
//     <img src="'.$imgSign.'" style=" position:absolute; float:right; vertical-align: baseline; width:20%; height:100px; margin-bottom:-80px; margin-right: 5%;"/>
//     <div style="clear:both; border:1px solid; ">
//         <div style="margin-top:-25px;">
//             <div style="float:left; width:49%; margin-left:1%; text-align:left; margin-top:10px; ">

//                 <div style="float:left;width:20%;font-size:20px;text-align: left;margin-top: 15px;">Service engineer : </div>
//                 <div style="float:left; width:70%; text-align:left;" >

//                 </div>

//                 <div style="clear:both;"></div>
//             </div>
//             <div style="float:left;  width:50%; margin-top:10px; ">
//                 <div style="float:left; width:50%; font-size:20px; text-align: right;margin-top: 15px;">Customer Sign : </div>
//                 <div style="float:left; width:50%; text-align:left;" >

//                 </div>
//                 <div style="clear:both;"></div>
//             </div>
//             <div style="clear:both; "></div>
//         </div>
//         <div style="clear:both; "></div>
//         <div>
//             <div style="float:left; width:49%; margin-left:1%; text-align:left;" >
//                 <div style="font-size:20px; margin-left:20%;"><br/>('.$engineerName.')</div>
//             </div>
//             <div style="float:left; width:40%; text-align:right; margin-right:10%; ">
//                 <span style="font-size:20px;"><br/>('.$contactPerson.')</span>
//             </div>
//             <div style="clear:both; "></div>
//         </div>
//         '.(($associate!="")?'<div style="float:left; width:99%; margin-left:1%; font-size:16px;" > Associate engineer : ('.$associate.')</div>':'').
//         '<div style="float:left; width:100%; text-align:center;" >Page {PAGENO}/{nbpg}</div>
//     </div>
//     ');


 $period = '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
                  <tbody>
                    <tr>
                      <th style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 10px 7px 0;" width="30%" align="center">
                        Step
                      </th>
                      <th style="width:40%; font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;" align="center">
                        <small>DD/MM/YYYY</small>
                      </th>
                      <th style="width:30%; font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;" align="center">
                        HH:MM
                      </th>
                    </tr>
                    <tr>
                      <td height="1" style="background: #bebebe;" colspan="4"></td>
                    </tr>
                    <tr>
                      <td height="10" colspan="4"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;" class="article"  align="center">Started
                      </td>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;" align="center"><small>'.$dataPDF['time_log_start']['log_task_date'].'</small></td>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;" align="center">'.$dataPDF['time_log_start']['log_task_time'].'</td>
                    </tr>
                    <tr>
                      <td height="1" colspan="4" style="border-bottom:1px solid #e4e4e4"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;" class="article"  align="center">Finished</td>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;"  align="center"><small>'.$dataPDF['time_log_end']['log_task_date'].'</small></td>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;" align="center">'.$dataPDF['time_log_end']['log_task_time'].'</td>
                    </tr>
                    <tr>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;"  align="center" class="article">Period</td>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;"  align="center"><small>-</small></td>
                      <td style="font-size: 18px; font-family: \'thsarabunnew\', sans-thsarabunnew; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;" align="center">'.$dataPDF['service_period'].'</td>
                    </tr>
                    <tr>
                      <td height="1" colspan="4" style="border-bottom:1px solid #e4e4e4"></td>
                    </tr>
                  </tbody>
                </table>';

$mpdf->SetWatermarkImage('../../img/water_mark.jpg', 0.9, '', array(200, 120) );

$htmlHeader = '<div style="width:100%; background-color:#ffffff; margin:auto;">
            <div style="padding:0% 5%;">

                    <div style="font-family: \'thsarabunnew\', sans-thsarabunnew; width:50%; float:left;color: #5b5b5b;" border="0" cellpadding="0" cellspacing="0" align="left" class="">
                        <div><img src="'.$imgPath.'"  height="48" alt="logo" border="0" /></div>
                    </div>

                    <div style="width:50%;float:left;" border="0" cellpadding="0" cellspacing="0" align="right" class="">
                          <div style="margin-top:60px;font-size: 20px; color: #5b5b5b; font-family:  \'thsarabunnew\', sans-thsarabunnew; line-height: 18px; vertical-align: top; text-align: right;">
                            Service Report No. '.$dataPDF['no_task'].'<br/>
                            <small></small> Request No. '.$dataPDF['no_ticket'].'<br />
                            <small></small> Contract No. '.$contract.'
                          </div>
                    </div>
            </div>
      </div>
      <div style="width:100%; background-color:#ffffff; margin-top:-75px; ">
            <div style="padding:0% 5%;">
                    <div style="font-family: \'thsarabunnew\', sans-thsarabunnew; font-size:41px; width:100%; text-align:center;color: #5b5b5b;" border="0" cellpadding="0" cellspacing="0">
                            SERVICE REPORT
                    </div>
            </div>
      </div>
<div style="width:100%;padding:0% 5%; margin-top:10px;"><div style="padding:0% 5%; margin-top:10px; border-bottom:1px solid #999999;"></div></div>
<div style="width:100%; background-color:#ffffff; margin:auto; border:0px solid #000;">
            <div style="padding:0% 5%;">
                    <div style="font-family: \'thsarabunnew\', sans-thsarabunnew; width:50%; float:left;color: #5b5b5b;" border="0" cellpadding="0" cellspacing="0" align="left" class="">
                        <div style="margin-top:10px;font-size: 18px; "><span>'.$company.'</span><br/>'.$address.'</div>
                    </div>
                    <div style="width:50%;float:left;" border="0" cellpadding="0" cellspacing="0" align="right" class="">
                          <div style="margin-top:10px;font-size: 20px; color: #5b5b5b; font-family:  \'thsarabunnew\', sans-thsarabunnew; line-height: 18px; vertical-align: top; text-align: right;">
                            '.$contactPerson.'<br/>
                            <small></small>'.$tel.'<br />
                          </div>
                    </div>
            </div>
</div>
<div style="width:100%;padding:0% 5%; margin-top:10px;"><div style="padding:0% 5%; margin-top:10px; border-bottom:1px solid #999999;"></div></div>
';

// $mpdf->showWatermarkImage = true;
// $mpdf->watermarkImgBehind = true;
$html = '<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>SERVICE REPORT '.$dataPDF['no_task'].'</title>

</head>

<body>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="robots" content="noindex,nofollow" />
<!-- <meta name="viewport" content="width=device-width; initial-scale=1.0;" /> -->
<style type="text/css">
  @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
  body { margin: 0; padding: 0; background: #fffff; }
  div, p, a, li, td { -webkit-text-size-adjust: none; }
  .ReadMsgBody { width: 100%; background-color: #ffffff; }
  .ExternalClass { width: 100%; background-color: #ffffff; }
  body { width: 100%; height: 100%; background-color: #ffffff; margin: 0; padding: 0; -webkit-font-smoothing: antialiased; }
  html { width: 100%; }
  p { padding: 0 !important; margin-top: 0 !important; margin-right: 0 !important; margin-bottom: 0 !important; margin-left: 0 !important; }
  .visibleMobile { display: none; }
  .hiddenMobile { display: block; }

  @media only screen and (max-width: 600px) {
  body { width: auto !important; }
  table[class=fullTable] { width: 100% !important; clear: both; }
  table[class=fullPadding] { width: 100% !important; clear: both; }
  table[class=col] { width: 100% !important; }
  .erase { display: none; }
  }

  @media only screen and (max-width: 420px) {
  table[class=fullTable] { width: 100% !important; clear: both; }
  table[class=fullPadding] { width: 100% !important; clear: both; }
  table[class=col] { width: 100% !important; clear: both; }
  table[class=col] td { text-align: left !important; }
  .erase { display: none; font-size: 0; max-height: 0; line-height: 0; padding: 0; }
  .visibleMobile { display: block !important; }
  .hiddenMobile { display: none !important; }
  }
</style>

<div style="width:100%;background-color:#ffffff;margin-top:25px;">

        <div style="padding: 5px 5%;">
                <div style="float:left; width:49%">

                    <table style="font-family: \'thsarabunnew\', sans-thsarabunnew; width:100%; float:left;color: #5b5b5b; font-size:18px;border: 1px solid #999999;border-bottom: 1px solid #5b5b5b;border-collapse: collapse;" >
                    <tr >
                            <td style="background-color: #f1f1f1;border-bottom: 1px solid #999999;border-collapse: collapse; width:30%;border-right: 1px solid #999999;">REQUEST TYPE</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse;text-align:left;padding-left:10px;">'.strtoupper($dataPDF['ticket_case_type']).'</td>
                    </tr>
                    <tr >
                            <td style="background-color: #f1f1f1;border-bottom: 1px solid #999999;border-collapse: collapse;width:30%;border-right: 1px solid #999999;">SERVICE TYPE</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse;text-align:left;padding-left:10px;">'.strtoupper($dataPDF['service_type_name']).'</td>
                    </tr>
                    <tr >
                            <td style="background-color: #f1f1f1;border-bottom: 1px solid #999999;border-collapse: collapse;width:30%;border-right: 1px solid #999999;">SERVICE STATUS</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:left;padding-left:10px;">'.strtoupper($serviceStatus).'</td>
                        </tr>
                    </table>
                </div>
                <div style="float:right; width:49%;">
                    <table style="font-family: \'thsarabunnew\', sans-thsarabunnew; width:100%; float:right;color: #5b5b5b; font-size:18px; border: 1px solid #999999;border-bottom: 1px solid #999999;border-collapse: collapse;">
                    <tr style="background-color: #f1f1f1;">
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">SERVICE</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">START</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">FINISHED</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse;text-align:center;">PERIOD</td>
                        </tr>
                    <tr >
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">DATE</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">'.$dataPDF['time_log_start']['log_task_date'].'</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">'.$dataPDF['time_log_end']['log_task_date'].'</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;"></td>
                    </tr>
                    <tr>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">TIME</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">'.$dataPDF['time_log_start']['log_task_time'].'</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse; text-align:center;border-right: 1px solid #999999;">'.$dataPDF['time_log_end']['log_task_time'].'</td>
                            <td style="border-bottom: 1px solid #999999;border-collapse: collapse;text-align:center;">'.$dataPDF['service_period'].'</td>
                            </tr></table>
                </div>
        </div>
</div>

<div style="width:100%;background-color:#ffffff;margin-top:25px;">

        <div style="padding: 5px 5%;">

            <div style="width:100%;font-size: 18px; color: #5b5b5b; font-family: \'thsarabunnew\', sans-thsarabunnew; line-height: 18px; vertical-align: top; text-align: left;">
                <div style="font-family: \'thsarabunnew\', sans-thsarabunnew; width:100%; float:left;color: #5b5b5b;" border="0" cellpadding="0" cellspacing="0" align="left" class="">
                      <p>'.$string.'</p>

                </div>
               <div style="font-family: \'thsarabunnew\', sans-thsarabunnew; width:100%; float:left;color: #5b5b5b;" border="0" cellpadding="0" cellspacing="0" align="left" class="">
                    <p><br/><br/>'.(($dataPDF['is_follow_up']==0)?'Service Status: Closed':'Service Status: Follow Up').'</p>
               </div>
            </div>
        </div>
</div>

<div style="width:100%;background-color:#ffffff;margin-top:25px;">
    <div style="padding: 5px 5%;">
        '.$sparePart.'
    </div>
</div>

<div style="width:100%;padding:0% 5%;"><div style="padding:0% 5%; margin-top:10px;"></div></div>


</body>
</html>';

$htmlFooter = '<div style="width:100%;background-color:#ffffff;margin-top:25px;">
        <div style="padding: 25px 5%;">
            <div style="border:1px solid #999999;">
                <div style="float:left; width:50%;font-size: 18px; color: #5b5b5b; font-family:  \'thsarabunnew\', sans-thsarabunnew; line-height: 18px;">
                    <div style="border-right:1px solid #999999;">
                        <div style="margin:10px;">
                            <div style="text-align:center;">Service engineer</div>
                            <div style="margin-top:10px;padding:0px 30px 0px 0px;text-align:center;border:0px solid #000;">
                                <img src="'.$imgEng.'" height="80px;" />
                            </div>
                            <div style="text-align:center;border:0px solid #000; margin-top:10px;">('.$engineerName.')</div>
                            <div style="text-align:center">'.$dataPDF['engineer_email'].'</div>
                            <div style="text-align:center"><br/>'.(($companyEngineer!="")?$companyEngineer:$dataPDF['comptname']).'</div>
                            '.(($associate!="")?'<div style="margin-top:25px; text-align:center">Associate engineer</div><div style="text-align:center">('.$associate.')</div>':'').'
                        </div>
                    </div>
                </div>

                <div style="float:left; width:50%;font-size: 18px; color: #5b5b5b; font-family:  \'thsarabunnew\', sans-thsarabunnew; line-height: 18px;">
                        <div style="margin:10px;">
                            <div style="text-align:center;">Customer Sign</div>
                            <div style="margin-top:10px;padding:0px 30px 0px 0px; text-align:center; border:0px solid #000;">
                                <img src="'.$imgSign.'" height="80px;" />
                            </div>
                            <div style="text-align:center;border:0px solid #000; margin-top:10px;">('.$contactPerson.')</div>
                            <div style="text-align:center;">'.$tel.'</div>
                        </div>
                </div>
            </div>
            <div style="text-align:center;" >Page {PAGENO}/{nbpg}</div>
        </div>

</div>';
$mpdf->setHtmlHeader($htmlHeader);
 $mpdf->setHtmlFooter($htmlFooter);

// echo $html;
// exit();
$mpdf->writeHTML($html);
// $mpdf->Output();
// exit();
if(isset($dataPDF['file_name_signated']) && $dataPDF['file_name_signated']!=""){
    echo "ok";
    $explodeFileName = explode(".",$dataPDF['file_name_signated']);
    $mpdf->Output(__dir__."/../../pdf/".$explodeFileName[0].".pdf");
}else{
    echo "dataPDF null";
}
exit();
}else{
    echo "Not Found";
}
?>
